﻿using DomainLogic.Interfaces;
using DomainLogic.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainLogic.Models;
using WebApp.UiHelpers.Shared;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Models.Enumerations;
using WebApp.Models.Views;
using WebApp.Models.Views.Default;

namespace WebApp.UiHelpers
{
    public class FeedbackUiHelpers
    {
        [Inject]
        public SectionsSelectListCreater SectionsSelectListCreater { get; set; }
        [Inject]
        public ProductTitlesForSelectListCreater ProductTitlesForSelectListCreater { get; set; }

        [Inject]
        public IFeedbacksService FeedbacksService { get; set; }
        [Inject]
        public IResourceProvider ResourceProvider { get; set; }


        public async Task<Tuple<List<SelectListItem>, List<SelectListItem>, List<SelectListItem>>> GetSelectListsAsync(long? sectionId, bool hasLastSection = true, bool hasSecondModel = true)
        {
            List<SelectListItem> themesForSelectList = GetFeedbackThemesForSelectList(new SelectListItem { Value = "", Text = "--" });
            string otherSection = ResourceProvider.ReadResource("Resources.ViewsRes.OtherSectionSelectListItem");
            List<SelectListItem> sectionsForSelectList = await SectionsSelectListCreater.CreateSelectListAsync(new SelectListItem { Value = "", Text = "--" },
                hasLastSection ? new SelectListItem { Value = "-1", Text = otherSection } : null);
            List<SelectListItem> productTitlesForSelectList;
            if (sectionId != null)
            {
                string anyProduct = ResourceProvider.ReadResource("Resources.ViewsRes.AnyProductSelectListItem");
                productTitlesForSelectList = await ProductTitlesForSelectListCreater.CreateSelectListAsync((long)sectionId, new SelectListItem { Value = "", Text = "--" },
                    hasSecondModel ? new SelectListItem { Value = "-1", Text = anyProduct } : null);
            }
            else
            {
                productTitlesForSelectList = new List<SelectListItem> { new SelectListItem { Value = "", Text = "--" } };
            }
            return Tuple.Create(themesForSelectList, sectionsForSelectList, productTitlesForSelectList);
        }

        public List<SelectListItem> GetFeedbackThemesForSelectList(SelectListItem firstSelectItem = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (firstSelectItem != null)
                list.Add(firstSelectItem);
            foreach (FeedbackThemeEnum s in Enum.GetValues(typeof(FeedbackThemeEnum)))
            {
                list.Add(new SelectListItem
                {
                    Value = s.ToString(),
                    Text = s.GetTitle()
                });
            }
            return list;
        }

        public List<SelectListItem> GetFeedbackStatesForSelectList(SelectListItem firstSelectItem = null)
        {
            var list = new List<SelectListItem>();
            if (firstSelectItem != null)
                list.Add(firstSelectItem);
            foreach (StateEnum s in Enum.GetValues(typeof(StateEnum)))
            {
                list.Add(new SelectListItem
                {
                    Value = s.ToString(),
                    Text = s.GetTitle()
                });
            }
            return list;
        }

        public DropDownMenu<StateEnum> GetDropDownMenuForStates(StateEnum activeState, string stateParamName, Dictionary<string, object> parms)
        {
            var items = new List<DropDownMenuItem<StateEnum>>();
            foreach (StateEnum s in Enum.GetValues(typeof(StateEnum)))
            {
                items.Add(new DropDownMenuItem<StateEnum>
                {
                    Value = s,
                    Text = s.GetTitle()
                });
            }
            var menu = new DropDownMenu<StateEnum>
            {
                ItemParamName = stateParamName,
                Params = parms,
                Items = items
            };
            menu.SetActiveItemByValue(activeState);
            menu.ComputeParams();
            return menu;
        }

        public List<AutocompleteItem> ToAutocompleteData(List<FeedbackTag> tags)
        {
            return tags.Select(x => new AutocompleteItem { id = x.Id, label = x.Title, value = x.Title }).ToList();
        }
    }
}
