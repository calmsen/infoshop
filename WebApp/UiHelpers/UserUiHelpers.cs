﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Constants;
using DomainLogic.Interfaces;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using DomainLogic.Models.Settings;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace WebApp.UiHelpers
{
    public class UserUiHelpers
    {
        [Inject]
        public ITLS TLS { get; set; }
        [Inject]
        public IUsersService UsersService { get; set; }
        [Inject]
        public ISettingsService SettingsService { get; set; }
        [Inject]
        public AppSettings AppSettings { get; set; }

        public List<SelectListItem> GetRolesForSelectList(List<string> roles)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (string r in roles)
            {
                list.Add(new SelectListItem
                {
                    Value = r,
                    Text = GetRoleTitle(r)
                });
            }
            return list;
        }

        public string GetRoleTitle(string role) 
        {
            switch (role)
            {
                case "Administrator": return "Администратор сайта";
                case "Manager": return "Контент менеджер";
                default: return null;
            }
        }


        /// <summary>
        /// Получает текущего пользователя, если пользователь не авторизован, то делается заглушка guest
        /// </summary>
        public async Task<User> GetCurrentUserAsync()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated && !TLS.Contains(TLSKeys.CurrentUser))
            {
                string currentUserName = HttpContext.Current.User.Identity.Name;
                if (!string.IsNullOrEmpty(currentUserName))
                {
                    User user = await UsersService.GetUserByEmailAsync(currentUserName);
                    if (user == null)
                    {
                        user = new User
                        {
                            Email = currentUserName
                        };
                        await UsersService.CreateUserAsync(user);
                        int usersCount = await UsersService.GetNumberOfUsersAsync();
                        if (usersCount == 1)
                        {
                            await SettingsService.CreateDataOnInitApp();
                            await SettingsService.UpdateDataOnCreateAdmin(user);
                            SettingsService.FillSettings(AppSettings);
                        }

                    }
                    TLS.Set(TLSKeys.CurrentUser, user);
                }
            }
            return TLS.Get<User>(TLSKeys.CurrentUser);
        }
    }
}
