﻿using DomainLogic.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.UiHelpers
{
    public class PartnerUiHelpers
    {
        [Inject]
        public IPartnersService PartnersService { get; set; }

        public async Task<List<SelectListItem>> GetPartnersForSelectListAsync(SelectListItem firstSelectItem)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (firstSelectItem != null)
                list.Add(firstSelectItem);
            foreach (Partner p in await PartnersService.GetPartnersAsync())
            {
                list.Add(new SelectListItem
                {
                    Value = p.Id.ToString(),
                    Text = p.Title
                });
            }
            return list;
        }
    }
}
