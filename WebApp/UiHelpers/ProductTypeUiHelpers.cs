﻿using DomainLogic.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.UiHelpers
{
    public class ProductTypeUiHelpers
    {
        [Inject]
        public IProductTypesService ProductTypesService { get; set; }

        public async Task<List<SelectListItem>> GetProductTypesForSelectListAsync(SelectListItem firstSelectItem)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (firstSelectItem != null)
                list.Add(firstSelectItem);
            List<ProductType> types = await ProductTypesService.GetTypesAsync();
            types.Sort((x, y) => x.Title.CompareTo(y.Title));
            foreach (ProductType t in types)
            {
                list.Add(new SelectListItem
                {
                    Value = t.Id.ToString(),
                    Text = t.Title
                });
            }
            return list;
        }
    }
}
