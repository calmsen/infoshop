﻿using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Models.Enumerations;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace WebApp.UiHelpers
{
    public class VacancyUiHelpers
    {
        public List<SelectListItem> GetVacancyStatesForSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (VacancyStateEnum s in Enum.GetValues(typeof(VacancyStateEnum)))
            {
                list.Add(new SelectListItem
                {
                    Value = s.ToString(),
                    Text = s.GetTitle()
                });
            }
            return list;
        }
    }
}
