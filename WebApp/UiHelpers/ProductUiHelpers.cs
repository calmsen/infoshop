﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using DomainLogic.Models;
using WebApp.UiHelpers.Shared;
using DomainLogic.Models.Settings;
using DomainLogic.Infrastructure.Attributes;
using WebApp.Infrastructure.Mvc.Utils;
using WebApp.Models.Views;
using WebApp.Models.Views.Default;

namespace WebApp.UiHelpers
{
    public class ProductUiHelpers
    {
        [Inject]
        public ProductTitlesForSelectListCreater ProductTitlesForSelectListCreater { get; set; }

        [Inject]
        public CookieUtils CookieUtils { get; set; }

        [Inject]
        public ProductsSettings ProductsSettings { get; set; }

        public List<AutocompleteItem> ToAutocompleteData(List<Product> products)
        {
            return products.Select(x => new AutocompleteItem { id = x.Id, label = x.Title, value = x.Title }).ToList();
        }

        public async Task<List<SelectListItem>> GetProductTitlesForSelectListAsync(long? sectionId, SelectListItem firstSelectItem = null, SelectListItem secondSelectItem = null, SelectListItem lastSelectItem = null)
        {
            return await ProductTitlesForSelectListCreater.CreateSelectListAsync(sectionId, firstSelectItem, secondSelectItem, lastSelectItem);
        }
        
        public void FilterAttrsAndGroups(ProductView productView)
        {
            if (productView.Attributes.Count == 0)
                return;

            List<AttributeView> attrs = new List<AttributeView>();
            List<FilterGroupView> groups = new List<FilterGroupView>();
            // отсортируем по группам
            foreach (var group in productView.Section.FiltersGroups)
            {
                bool groupFlag = false;
                foreach (var fs in group.Filters)
                {
                    foreach (AttributeView a in productView.Attributes)
                    {
                        if (a.FilterId != fs.Id)
                            continue;

                        if (ProductsSettings.PlatformFilterIds.Contains(a.FilterId))
                            continue;
                        else if (a.Values.Count == 0)
                            continue;

                        //
                        a.Filter.Title = fs.Title; 
                        //
                        a.FilterSettings = fs;
                        //
                        attrs.Add(a);
                        //
                        if (groupFlag)
                            continue;
                        groups.Add(group);
                        groupFlag = true;
                    }
                }
            }
            productView.Section.FiltersGroups = groups;
            productView.Attributes = attrs;
        }
        
        public CompareProductBtn GetCompareProductBtn(long productId)
        {
            return new CompareProductBtn
            {
                ProductId = productId,
                ProductInComparison = GetProductsInComparison().Any(x => x.ProductId == productId)
            };
        }
        
        public CompareProductsBtn GetCompareProductsBtn(long sectionId)
        {
            return new CompareProductsBtn
            {
                SectionId = sectionId,
                ProductsAmountInComparison = GetProductsInComparison().Where(x => x.SectionId == sectionId).Count()
            };
        }

        public ProductInComparison[] GetProductsInComparison()
        {
            ProductInComparison[] productsInComparison;
            try
            {
                productsInComparison = JsonConvert.DeserializeObject<ProductInComparison[]>(HttpUtility.UrlDecode(CookieUtils.GetCookie("productsInComparison")) ?? "[]");
            }
            catch (Exception)
            {
                productsInComparison = new ProductInComparison[0];
            }
            return productsInComparison;
        }
    }
}
