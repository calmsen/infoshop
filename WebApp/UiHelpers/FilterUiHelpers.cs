﻿using DomainLogic.Interfaces.Services;
using WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Extensions;
using WebApp.Models.Views;
using WebApp.Models.Views.Default;
using DomainLogic.Models.Enumerations;

namespace WebApp.UiHelpers
{
    public class FilterUiHelpers
    {
        [Inject]
        public IFiltersService FiltersService { get; set; }

        public Dictionary<long, FilterForSelectList> FiltersForSelectListsAsMap(List<DmFilter> filters, List<FilterGroup> filtersGroups = null)
        {
            var map = new Dictionary<long, FilterForSelectList>();
            if (filters.Count == 0)
                return map;
            foreach (DmFilter f in filters)
            {
                // добавим первое значение
                List<SelectListItem> list = new List<SelectListItem> { 
                    new SelectListItem {
                        Value = "",
                        Text = "--"
                    }
                };
                // добавим основные значения(только активные)
                FilterSettings fs = null;
                if (filtersGroups != null)
                    fs = filtersGroups.SelectMany(x => x.Filters.Where(y => y.Id == f.Id)).FirstOrDefault();

                foreach (FilterValue v in f.Values)
                {
                    // если настройки значений заполнены и нам не удалось найти активное значение то пропускаем данное значение
                    if (fs != null && fs.Values != null && fs.Values.Count > 0 && !fs.Values.Any(x => x.Id == v.Id && x.Activated))
                        continue;
                    list.Add(new SelectListItem
                    {
                        Value = v.Id.ToString(),
                        Text = v.Title.ToString()
                    });
                }
                list = list.OrderBy(x => x.Text).ToList();
                map.Add(f.Id, new FilterForSelectList
                {
                    Id = f.Id,
                    Title = f.Title,
                    Type = f.Type,
                    List = list,
                    Multiple = f.Multiple,
                    Unit = f.Unit
                });
            }
            return map;
        }

        public List<AutocompleteItem> ToAutocompleteData(List<FilterView> filters)
        {
            return filters.Select(x => new AutocompleteItem {
                id = x.Id,
                label = x.Title + " " + x.GetSectionTitlesAsString(),
                value = x.Title,
                unit = x.Unit,
                unitConverters = x.UnitConverters,
                type = x.Type
            }).ToList();
        }

        public List<SelectListItem> GetFiltersTypesForSelectList(SelectListItem firstSelectItem = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (firstSelectItem != null)
                list.Add(firstSelectItem);
            foreach (FilterTypeEnum s in Enum.GetValues(typeof(FilterTypeEnum)))
            {
                list.Add(new SelectListItem
                {
                    Value = s.ToString(),
                    Text = s.GetTitle()
                });
            }
            return list;
        }

        public async Task<List<SelectListItem>> GetFilterValuesForSelectListAsync(SelectListItem firstSelectItem, string filterTitle, long sectionId)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (firstSelectItem != null)
                list.Add(firstSelectItem);
            foreach (FilterValue v in await FiltersService.GetFilterValuesAsync(filterTitle, sectionId))
            {
                list.Add(new SelectListItem
                {
                    Value = v.Id.ToString(),
                    Text = v.Title 
                });
            }
            return list;
        }
    }
}
