﻿using DomainLogic.Interfaces;
using DomainLogic.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainLogic.Models;
using WebApp.UiHelpers.Shared;
using DomainLogic.Infrastructure.Attributes;
using WebApp.Models.Views.Default;

namespace WebApp.UiHelpers
{
    public class SectionUiHelpers
    {
        [Inject]
        public SectionsSelectListCreater SectionsSelectListCreater { get; set; }

        [Inject]
        public ISectionsService SectionsService { get; set; }

        [Inject]
        public IResourceProvider ResourceProvider { get; set; }

        public List<Breadcrumb> GetBreadcrumbsFromSections(List<Section> sections, long activeSectionId)
        {
            List<Breadcrumb> breadcrumbs = new List<Breadcrumb>();
            breadcrumbs.Add(new Breadcrumb
            {
                Name = null,
                Title = ResourceProvider.ReadResource("Resources.ViewsRes.CatalogBreadcrumbText"),
                Path = ""
            });
            if (activeSectionId > 0)
            {
                AddBreadcrumbs(breadcrumbs, sections, activeSectionId, "");
            }

            return breadcrumbs;
        }

        public async Task<List<SelectListItem>> GetSectionsForSelectListAsync(SelectListItem firstSelectItem = null, SelectListItem lastSelectItem = null)
        {
            return await SectionsSelectListCreater.CreateSelectListAsync(firstSelectItem, lastSelectItem);
        }

        public async Task<List<SelectListItem>> GetSectionsForSelectListAsync(List<long> sectionIds, SelectListItem firstSelectItem = null, SelectListItem lastSelectItem = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (firstSelectItem != null)
                list.Add(firstSelectItem);
            List<Section> sections = await SectionsService.GetSectionsAsync(sectionIds);
            sections.Sort((x, y) => x.Title.CompareTo(y.Title));
            foreach (Section s in sections)
            {
                list.Add(new SelectListItem
                {
                    Value = s.Id.ToString(),
                    Text = s.Title
                });
            }
            if (lastSelectItem != null)
                list.Add(lastSelectItem);

            return list;
        }

        private void AddBreadcrumbs(List<Breadcrumb> breadcrumbs, List<Section> sections, long sectionId, string path)
        {
            Section section = SectionsService.GetSectionByIdFromList(sections, sectionId);
            path = "/" + section.Name;
            if (section.ParentId > 0)
            {
                AddBreadcrumbs(breadcrumbs, sections, section.ParentId, path);
            }
            breadcrumbs.Add(new Breadcrumb
            {
                Name = section.Name,
                Title = section.Title,
                Path = path
            });
        }
    }
}
