﻿using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Models.Enumerations;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace WebApp.UiHelpers
{
    public class FileUiHelpers
    {
        public List<SelectListItem> GetFileFiltersForSelectList(SelectListItem firstSelectItem = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            
            if (firstSelectItem != null)
                list.Add(firstSelectItem);

            foreach (FileFilterEnum s in Enum.GetValues(typeof(FileFilterEnum)))
            {
                list.Add(new SelectListItem
                {
                    Value = s.ToString(),
                    Text = s.GetTitle()
                });
            }
            return list;
        }
    }
}
