﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace WebApp.UiHelpers.Shared
{
    public class ProductTitlesForSelectListCreater
    {
        [Inject]
        public IProductsService ProductsService { get; set; }

        public async Task<List<SelectListItem>> CreateSelectListAsync(long? sectionId, SelectListItem firstSelectItem = null, SelectListItem secondSelectItem = null, SelectListItem lastSelectItem = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (firstSelectItem != null)
                list.Add(firstSelectItem);
            if (secondSelectItem != null)
                list.Add(secondSelectItem);
            if (sectionId == null)
                return list;

            List<ProductTitle> productTitles = await ProductsService.GetProductTitlesAsync((long)sectionId);
            productTitles.ForEach(x => x.Title = Regex.Replace(x.Title, @"[а-яА-Я/]+|\(.+\)", ""));
            productTitles.Sort((x, y) => x.Title.CompareTo(y.Title));
            foreach (ProductTitle v in productTitles)
            {
                list.Add(new SelectListItem
                {
                    Value = v.Id.ToString(),
                    Text = v.Title
                });
            }
            if (lastSelectItem != null)
                list.Add(lastSelectItem);
            return list;
        }
    }
}