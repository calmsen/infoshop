﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace WebApp.UiHelpers.Shared
{
    public class SectionsSelectListCreater
    {
        [Inject]
        public ISectionsService SectionsService { get; set; }

        public async Task<List<SelectListItem>> CreateSelectListAsync(SelectListItem firstSelectItem = null, SelectListItem lastSelectItem = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (firstSelectItem != null)
                list.Add(firstSelectItem);
            List<Section> sections = await SectionsService.GetSectionsWithProductsAsync();
            sections.Sort((x, y) => x.Title.CompareTo(y.Title));
            foreach (Section s in sections)
            {
                list.Add(new SelectListItem
                {
                    Value = s.Id.ToString(),
                    Text = s.Title
                });
            }
            if (lastSelectItem != null)
                list.Add(lastSelectItem);

            return list;
        }
    }
}