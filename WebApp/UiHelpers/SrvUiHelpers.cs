﻿using DomainLogic.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Models.Enumerations;

namespace WebApp.UiHelpers
{
    public class SrvUiHelpers
    {
        [Inject]
        public ISrvsService SrvsService { get; set; }

        public async Task<List<SelectListItem>> GetCitiesForSelectListAsync(string country, SelectListItem firstSelectItem)
        {
            List<string> cities = await SrvsService.GetCitiesForSelectListAsync(country);
            List<SelectListItem> citiesForSelectList = cities.Select(x => new SelectListItem { Value = x, Text = x }).ToList();
            if (firstSelectItem != null)
                citiesForSelectList.Insert(0, firstSelectItem);
            return citiesForSelectList;
        }

        public List<SelectListItem> GetServiceTypesForSelectList(SelectListItem firstSelectItem)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (firstSelectItem != null)
                list.Add(firstSelectItem);
            foreach (ServiceTypeEnum s in Enum.GetValues(typeof(ServiceTypeEnum)))
            {
                if (s == ServiceTypeEnum.Others)
                    continue;
                list.Add(new SelectListItem
                {
                    Value = s.ToString(),
                    Text = s.GetTitle()
                });
            }
            return list;
        }
    }
}
