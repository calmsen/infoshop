﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace WebApp.ValidationAttributes
{
    /// <summary>
    /// Проверяет равно ли значение true
    /// </summary>
    public class MustBeTrueAttribute : ValidationAttribute, IClientValidatable
    {
        /// <summary>
        /// Проверяет валидно ли значение
        /// </summary>
        /// <param name="propertyValue"></param>
        /// <returns></returns>
        public override bool IsValid(object propertyValue)
        {
            return propertyValue != null
                && propertyValue is bool
                && (bool)propertyValue;
        }

        /// <summary>
        /// Получает правила валидации на клиентской стороне
        /// </summary>
        /// <param name="metadata"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationRule
            {
                ErrorMessage = this.ErrorMessage,
                ValidationType = "mustbetrue"
            };
        }
    }
}