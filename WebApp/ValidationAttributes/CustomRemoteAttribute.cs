﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApp.ValidationAttributes
{
    /// <summary>
    /// Custom Remote Attribute for Client an Server validation.
    /// </summary>
    public class CustomRemoteAttribute : RemoteAttribute
    {
        /// <summary>
        /// List of all Controllers on MVC Application
        /// </summary>
        /// <returns></returns>
        private List<Type> GetControllerList()
        {
            return Assembly.GetCallingAssembly().GetTypes().Where(type => type.IsSubclassOf(typeof(Controller))).ToList();
        }

        /// <summary>
        /// Constructor of base class.
        /// </summary>
        protected CustomRemoteAttribute()
        {
        }

        /// <summary>
        /// Constructor of base class.
        /// </summary>
        public CustomRemoteAttribute(string routeName)
            : base(routeName)
        {
        }

        /// <summary>
        /// Constructor of base class.
        /// </summary>
        public CustomRemoteAttribute(string action, string controller)
            : base(action, controller)
        {
        }

        /// <summary>
        /// Constructor of base class.
        /// </summary>
        public CustomRemoteAttribute(string action, string controller, string areaName)
            : base(action, controller, areaName)
        {
        }

        /// <summary>
        /// Overridden IsValid function
        /// </summary>
        /// <param name="value"></param>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // Find the controller passed in constructor
            var controller = GetControllerList().FirstOrDefault(x => x.Name == string.Format("{0}Controller", this.RouteData["controller"]));
            if (controller == null)
            {
                // Default behavior of IsValid when no controller is found.
                return ValidationResult.Success;
            }

            // Find the Method passed in constructor
            var mi = controller.GetMethod(this.RouteData["action"].ToString());
            if (mi == null)
            {
                // Default behavior of IsValid when action not found
                return ValidationResult.Success;
            }
            
            var instance = (ControllerBase)ControllerBuilder.Current.GetControllerFactory().CreateController(HttpContext.Current.Request.RequestContext, new Regex(@"Controller$").Replace(controller.Name, ""));
            instance.ControllerContext = new ControllerContext(new HttpContextWrapper(System.Web.HttpContext.Current), new RouteData(), instance);

            TaskCompletionSource<bool> promise = null;
            

            List<object> args = new List<object>();
            if (string.IsNullOrWhiteSpace(AdditionalFields))
            {
                args.Add(value);
            }
            else
            {
                var miParams = mi.GetParameters();
                for (int i = 0; i < miParams.Length; i++)
                {
                    Type argType = Type.GetType(miParams[i].ParameterType.FullName);
                    string argName = miParams[i].Name;
                    object arg;
                    if (Equals(argType.Name, "Action`1") && Equals(argName, "callback"))
                    {
                        promise = new TaskCompletionSource<bool>();
                        Action<bool> callback = x => promise.SetResult(x);
                        arg = callback;
                    }
                    else
                    {
                        arg = Activator.CreateInstance(argType);
                        string valueAsString = HttpContext.Current.Request.Form[argName];
                        arg = Convert.ChangeType(valueAsString, argType);
                    }                        
                    
                    args.Add(arg);
                }
            }
            
            // invoke the method on the controller with value
            bool result = (mi.Invoke(instance, args.ToArray()) as bool?) ?? promise?.Task.Result ?? false;

            // Return success or the error message string from CustomRemoteAttribute
            return result ? ValidationResult.Success : new ValidationResult(base.ErrorMessageString);
        }
    }
}