﻿using WebApp.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using DomainLogic.Interfaces;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using WebApp.Models.Views.Default;
using DomainLogic.Interfaces.Services;
using DomainLogic.Infrastructure.Providers;
using WebApp.UiHelpers;

namespace WebApp.Controllers
{
    /// <summary>
    /// Базовый контроллер.
    /// </summary>
    public abstract class BaseController : Controller
    {
        [Inject]
        public ITLS TLS { get; set; }
        [Inject]
        public ILogger Logger { get; set; }
        [Inject]
        public IResourceProvider ResourceProvider { get; set; }
        [Inject]
        public IUsersService UsersService { get; set; }
        [Inject]
        public ISettingsService SettingsService { get; set; }
        [Inject]
        public UserUiHelpers UserUiHelpers { get; set; }
        [Inject]
        public Provider<MainLayout> LayoutsProvider { get; set; }

        /// <summary>
        /// Получает форму из сессии. Вызывется после редиректа POST на GET. Реализация паттерн PRG (post get redirect)
        /// </summary>
        /// <typeparam name="T">Тип формы</typeparam>
        /// <returns></returns>
        protected T GetForm<T>() 
        {
            return GetForm<T>(typeof(T).Name);
        }
        /// <summary>
        /// Получает форму из сессии. Вызывется после редиректа POST на GET. Реализация паттерн PRG (post get redirect)
        /// </summary>
        /// <typeparam name="T">Тип формы</typeparam>
        /// <param name="key">Ключ, по которому хранится форма</param>
        /// <returns></returns>
        protected T GetForm<T>(string key)
        {
            if (Session[key] == null) {
                Session.Remove(key);
                return default(T);
            }                

            T form = (T)Session[key];
            Session.Remove(key);
            ModelState.Clear();
            var previousModelState = Session["ModelState"] as ModelStateDictionary;
            Session.Remove("ModelState");
            if (previousModelState != null)
            {
                foreach (KeyValuePair<string, ModelState> kvp in previousModelState)
                    if (!ModelState.ContainsKey(kvp.Key))
                        ModelState.Add(kvp.Key, kvp.Value);
            }
            return form;
        }

        /// <summary>
        /// Реализация патерна PGR (post redirect get). Все POST параметры записываются в сессию, и делается редирект на GET.
        /// </summary> 
        /// <typeparam name="T">Тип формы</typeparam>
        /// <param name="form">Форма</param>
        /// <param name="action">Действие</param>
        /// <param name="controller">Контроллер</param>
        /// <param name="parameters">Дополнительные get параметры</param>
        /// <returns></returns>
        protected RedirectToRouteResult PostRedirectGet<T>(T form, string action, string controller, object parameters = null) 
        { 
            return PostRedirectGet<T>(typeof(T).Name, form, action, controller, parameters);
        }

        /// <summary>
        /// Реализация патерна PGR (post redirect get). Все POST параметры записываются в сессию, и делается редирект на GET.
        /// </summary> 
        /// <typeparam name="T">Тип формы</typeparam>
        /// <param name="key">Ключ, по которому хранится форма</param>
        /// <param name="form">Форма</param>
        /// <param name="action">Действие</param>
        /// <param name="controller">Контроллер</param>
        /// <param name="parameters">Дополнительные get параметры</param>
        /// <returns></returns>
        protected RedirectToRouteResult PostRedirectGet<T>(string key, T form, string action, string controller, object parameters = null)
        {
            Session[key] = form;
            Session["ModelState"] = ModelState;
            return RedirectToAction(action, controller, parameters);
        }
        /// <summary>
        /// Разрешить доступ к странице, если используется http аутинтефикация
        /// </summary>
        /// <returns></returns>
        protected bool AllowAccess()
        {
            if (String.IsNullOrEmpty(Request.Headers["Authorization"]))
            {
                Response.AppendHeader("WWW-Authenticate", "Basic realm=\"My Realm\"");
                Response.StatusCode = 401;
                Response.Write("Страница 401 - требуется авторизация.");
                Response.End();
                Response.Close();
                return false;
            }
            else
            {
                // проверяем логин и пароль
                var cred = System.Text.UTF8Encoding.UTF8
                    .GetString(Convert.FromBase64String(Request.Headers["Authorization"].Substring(6)))
                    .Split(':');
                string userName = cred[0];
                string password = cred[1];
                // собсвенно здесь должна быть сама проверка(запрос в бд к примеру)
                if (!userName.Equals("Admin") || !password.Equals("123456"))
                {
                    Response.StatusCode = 403;
                    Response.Write("Страница 403 - доступ ограниччен.");
                    Response.End();
                    Response.Close();
                    return false;
                }
                else return true;
            }
        }
        /// <summary>
        /// Проверяет достоверность каптчи
        /// </summary>
        /// <param name="captcha"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        protected bool ValidateCaptcha(string captcha, string field = "Captcha")
        {
            if (Session["captcha"] == null)
                return false;
            string sesCaptcha = Session["captcha"].ToString();
            Session.Remove("captcha");

            if (string.IsNullOrWhiteSpace(captcha))
            {
                ModelState.AddModelError(field, ResourceProvider.ReadResource("Resources.ViewsRes.CaptchaRequiredMessage"));
                return false;
            }
            if (!string.Equals(captcha, sesCaptcha))
            {
                ModelState.AddModelError(field, ResourceProvider.ReadResource("Resources.ViewsRes.CaptchaErrorMessage"));
                return false;
            }
            return true;
        }
        /// <summary>
        /// Получает ip адрес пользователя.
        /// </summary>
        /// <returns></returns>
        protected string GetUserIP()
        {
            string ip = null;
            try
            {
                //ip = Request.UserHostAddress
                ip = Request.Headers["X-Forwarded-For"];
                if (string.IsNullOrEmpty(ip) || "unknown".Equals(ip, StringComparison.InvariantCultureIgnoreCase))
                {
                    ip = Request.Headers["Proxy-Client-IP"];
                }
                if (string.IsNullOrEmpty(ip) || "unknown".Equals(ip, StringComparison.InvariantCultureIgnoreCase))
                {
                    ip = Request.Headers["WL-Proxy-Client-IP"];
                }
                if (string.IsNullOrEmpty(ip) || "unknown".Equals(ip, StringComparison.InvariantCultureIgnoreCase))
                {
                    ip = Request.Headers["HTTP_CLIENT_IP"];
                }
                if (string.IsNullOrEmpty(ip) || "unknown".Equals(ip, StringComparison.InvariantCultureIgnoreCase))
                {
                    ip = Request.Headers["HTTP_X_FORWARDED_FOR"];
                }
                if (string.IsNullOrEmpty(ip) || "unknown".Equals(ip, StringComparison.InvariantCultureIgnoreCase))
                {
                    ip = Request.ServerVariables["REMOTE_ADDR"];
                }
                IPAddress address = IPAddress.Parse(ip.Split(',')[0].Trim());
                ip = address.MapToIPv4().ToString();
            }
            catch (Exception)
            {
            }
            return ip;
        }


        protected async Task<ViewModelHolder<T>> CreateViewModelHolderAsync<T>(T viewModel)
            where T : class, new()
        {
            var model = LayoutsProvider.Get<ViewModelHolder<T>>();
            model.ViewModel = viewModel;
            await model.InitAsync(null, 0, false);
            return model;
        }

        protected async Task<MainLayout> CreateMainLayoutAsync()
        {
            var model = LayoutsProvider.Get<MainLayout>();
            await model.InitAsync(null, 0, false);
            return model;
        }
    }
}
