﻿using AutoMapper;
using DataAccess.Infrastructure;
using DomainLogic.Interfaces;
using DataAccess.Migrations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebApp.Areas.Admin;
using WebApp.Areas.Default;
using WebApp.Infrastructure.Mvc;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Mappers;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Services.Shared.Providers;
using WebApp.Infrastructure.Mvc.Localization;
using WebApp.Infrastructure.Mvc.Utils;
using WebApp.Infrastructure.Mvc.ModelBinders;
using DomainLogic.Services.Shared;
using WebApp.Infrastructure.Mvc.Extensions;
using System.Web.Mvc.Html;
using DomainLogic.Models.Settings;
using DomainLogic.Interfaces.Services;
using DomainLogic.Infrastructure;
using DomainLogic.Infrastructure.ServiceContainers;
using DomainLogic.Infrastructure.ServiceContainers.Ninject;

namespace WebApp
{
    // Примечание: Инструкции по включению классического режима IIS6 или IIS7 
    // см. по ссылке http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        // Определим статические сервисы для использования во Views
        public static ReflectorUtils ReflectorUtils;
        public static St0SiteSettings St0SiteSettings;
        public static CommonUtils CommonUtils;
        public static UrlUtils UrlUtils;
        public static LocalizationSettings LocalizationSettings;
        public static IResourceProvider ResourceProvider;

        private static IServiceContainer _serviceContainer;

        /// <summary>
        /// Запускается при старте приложения
        /// </summary>
        protected void Application_Start()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<InfoShopDatabase, Configuration>());
            using (DbContext ctx = new InfoShopDatabase())
            {
                ctx.Database.Initialize(true);
            }

            // --
            _serviceContainer = new ServiceContainer();
            _serviceContainer.Init();
            _serviceContainer.Load(new DataAccess.BindingModule(), new DomainLogic.BindingModule(), new BindingModule());
            _serviceContainer.Load("WebAppBindingModule.xml");

            CommonUtils = _serviceContainer.Get<CommonUtils>();
            UrlUtils = _serviceContainer.Get<UrlUtils>();
            ReflectorUtils = _serviceContainer.Get<ReflectorUtils>();
            St0SiteSettings = _serviceContainer.Get<St0SiteSettings>();
            LocalizationSettings = _serviceContainer.Get<LocalizationSettings>();
            ResourceProvider = _serviceContainer.Get<IResourceProvider>();

            NLogCommandInterceptor.CurrentLogger = _serviceContainer.Get<ILogger>();
            EmailService.EmailSender = _serviceContainer.Get<EmailSender>();
            FieldInfoHtmlHelper.ReflectorUtils = ReflectorUtils;

            UrlExtensions.UrlUtils = UrlUtils;
            EnumExtensions.EnumTitlesProvider = _serviceContainer.Get<EnumTitlesProvider>();
            CollectionExtensions.CollectionUtils = _serviceContainer.Get<CollectionUtils>();
            StringExtensions.StringUtils = _serviceContainer.Get<StringUtils>();

            Mapper.Initialize(x =>
            {
                x.AddProfile(_serviceContainer.Get<WebApp.Infrastructure.AutoMapperDefaultProfile>());
                x.AddProfile(_serviceContainer.Get<DomainLogic.Infrastructure.Mappers.AutoMapperDefaultProfile>());
            });
            _serviceContainer.Get<ProductMapper>().Initialize(x => x.AddProfile<DomainLogic.Infrastructure.Mappers.AutoMapperProductProfile>());
            _serviceContainer.Get<FilterMapper>().Initialize(x => x.AddProfile<DomainLogic.Infrastructure.Mappers.AutoMapperFilterProfile>());
            
            DependencyResolver.SetResolver(new WebAppDependencyResolver(_serviceContainer));
            
            AreaConfig.RegisterArea<AdminAreaRegistration>(RouteTable.Routes, null);
            AreaConfig.RegisterArea<DefaultAreaRegistration>(RouteTable.Routes, null);

            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ModelBinders.Binders.Add(typeof(List<string>), new CommaSeparatedModelBinder());
            ModelBinders.Binders.Add(typeof(List<int>), new CommaSeparatedModelBinder());
            ModelBinders.Binders.Add(typeof(List<long>), new CommaSeparatedModelBinder());
            ModelBinders.Binders.Add(typeof(List<double>), new CommaSeparatedModelBinder());
            ModelBinders.Binders.Add(typeof(DateTime), new DateTimeModelBinder());
            ModelBinders.Binders.Add(typeof(string), new AntyXssAndRemoveSpacesModelBinder(_serviceContainer.Get<HtmlSanitizer>()));

            ModelMetadataProviders.Current = _serviceContainer.Get<LocalizedModelMetadataProvider>();
            ModelValidatorProviders.Providers.Clear();
            ModelValidatorProviders.Providers.Add(_serviceContainer.Get<LocalizedModelValidatorProvider>());

            FillSettings(_serviceContainer.Get<AppSettings>(), _serviceContainer.Get<ISettingsService>());
        }

        /// <summary>
        /// Запускается перед выполнением текущего запроса
        /// </summary>
        protected void Application_BeginRequest()
        {
            string requestUrl = HttpContext.Current.Request.Url.ToString();
            if (HttpContext.Current.Request.FilePath.Equals("/404.aspx"))
                requestUrl = HttpContext.Current.Request.Url.Query.Replace("?404;", "");
            requestUrl = new Regex(@"\/$").Replace(requestUrl, "");

            // --
            CreateCultureInfoIfNeed(requestUrl);
            //
            StartTimer(requestUrl);
            // --
        }
        /// <summary>
        /// Запускается после выполнения текущего запроса
        /// </summary>
        protected void Application_EndRequest()
        {
            string requestUrl = HttpContext.Current.Request.Url.ToString();

            // --
            CreateCultureInfoIfNeed(requestUrl);
            //
            StopTimer(requestUrl);
            // --

            if (HttpContext.Current.Response.StatusCode == 302)
                HttpContext.Current.Response.RedirectLocation = CommonUtils.GetUrlWithCulture(System.Threading.Thread.CurrentThread.CurrentCulture.Name, HttpContext.Current.Response.RedirectLocation);

            /**SetCacheControlHeaders();*/
            _serviceContainer.Get<ITLS>().Clear();
        }
        
        /// <summary>
        /// Создает нужную культуру если нужно. Культура парсится из url
        /// </summary>
        /// <param name="requestUrl"></param>
        private void CreateCultureInfoIfNeed(string requestUrl)
        {
            if (_serviceContainer.Get<ITLS>().Get("CurrentCultureIsSetted") != null)
                return;
            CultureInfo ci = null;
            try
            {
                string locale = requestUrl.Replace("://", "ProtocolSeparator").Split('/')[1];
                ci = new CultureInfo(locale);
            }
            catch (Exception)
            {
                ci = new CultureInfo("ru");
            }
            System.Threading.Thread.CurrentThread.CurrentUICulture = ci;
            System.Threading.Thread.CurrentThread.CurrentCulture = ci;
            _serviceContainer.Get<ITLS>().Set("CurrentCultureIsSetted", true);
        }

        /// <summary>
        /// Запускает таймер выполнения запроса
        /// </summary>
        /// <param name="requestUrl"></param>
        private void StartTimer(string requestUrl)
        {
            if (requestUrl.IndexOf("/Content/", StringComparison.OrdinalIgnoreCase) != -1
               || requestUrl.IndexOf("/Admin", StringComparison.OrdinalIgnoreCase) != -1
               || requestUrl.IndexOf("/elmah", StringComparison.OrdinalIgnoreCase) != -1
               || requestUrl.IndexOf(".aspx") != -1
               || requestUrl.IndexOf(".axd") != -1)
                return;
            //
            _serviceContainer.Get<ITLS>().Set("Stopwatch", Stopwatch.StartNew());
            _serviceContainer.Get<ITLS>().Set("RequestCounter", new RequestCounter { RequestTime = DateTime.Now });
        }

        /// <summary>
        /// Останавливает таймер выполнения запроса. После чего данные сохраняются в таблице RequestCounters
        /// </summary>
        /// <param name="requestUrl"></param>
        private void StopTimer(string requestUrl)
        {
            if (requestUrl.IndexOf("/Content/", StringComparison.OrdinalIgnoreCase) != -1
               || requestUrl.IndexOf("/Admin", StringComparison.OrdinalIgnoreCase) != -1
               || requestUrl.IndexOf("/elmah", StringComparison.OrdinalIgnoreCase) != -1
               || requestUrl.IndexOf(".aspx") != -1
               || requestUrl.IndexOf(".axd") != -1)
                return;
            //
            Stopwatch sw = (Stopwatch)_serviceContainer.Get<ITLS>().Get("Stopwatch");
            RequestCounter rc = (RequestCounter)_serviceContainer.Get<ITLS>().Get("RequestCounter");
            if (sw == null || rc == null)
                return;
            rc.RequestExcevuteTime = sw.Elapsed.Milliseconds;
            rc.RequestUrl = requestUrl;
            // TODO: Переписать на синхронный метод
            //_kernel.Get<IRequestCounterService>().CreateRequestCounterAsync(rc);
        }

        private void FillSettings(AppSettings appSettings, ISettingsService settingsService)
        {
            appSettings.SmtpClient.Host = System.Configuration.ConfigurationManager.AppSettings["SmtpClient.Host"].ToString();
            appSettings.SmtpClient.Port = int.Parse(System.Configuration.ConfigurationManager.AppSettings["SmtpClient.Port"].ToString());
            appSettings.SmtpClient.EnableSsl = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["SmtpClient.EnableSsl"].ToString());
            appSettings.SmtpClient.Credentials.User = System.Configuration.ConfigurationManager.AppSettings["SmtpClient.Credentials.User"].ToString();
            appSettings.SmtpClient.Credentials.Password = System.Configuration.ConfigurationManager.AppSettings["SmtpClient.Credentials.Password"].ToString();

            appSettings.Files.AllowedExts = new List<string>(System.Configuration.ConfigurationManager.AppSettings["Files.AllowedExts"].ToString().Replace(" ", "").Split(','));

            appSettings.St0Site.Root = System.Configuration.ConfigurationManager.AppSettings["St0Site.Root"].ToString();
            appSettings.St0Site.HttpHost = System.Configuration.ConfigurationManager.AppSettings["St0Site.HttpHost"].ToString();

            appSettings.Products.Images.Ftp.Credentials.User = System.Configuration.ConfigurationManager.AppSettings["Products.Images.Ftp.Credentials.User"].ToString();
            appSettings.Products.Images.Ftp.Credentials.Password = System.Configuration.ConfigurationManager.AppSettings["Products.Images.Ftp.Credentials.Password"].ToString();
            appSettings.Products.Images.Ftp.Path = System.Configuration.ConfigurationManager.AppSettings["Products.Images.Ftp.Path"].ToString();

            appSettings.Upload.Root = System.Configuration.ConfigurationManager.AppSettings["Upload.Root"].ToString();

            appSettings.Feedback.Md5HahSolt = System.Configuration.ConfigurationManager.AppSettings["Feedback.Md5HahSolt"].ToString();
            appSettings.Mailing.Md5HahSolt = System.Configuration.ConfigurationManager.AppSettings["Mailing.Md5HahSolt"].ToString();

            settingsService.FillSettings(appSettings);
        }
    }
}
