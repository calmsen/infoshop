﻿using System.Web.Http;
using System.Web.Mvc;

namespace WebApp.Areas.Default
{
    public class DefaultAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Default";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            context.MapRoute(
                name: "CultureDefault",
                url: "{culture}/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                constraints: new { culture = @"ru|en" },
                namespaces: new[] { "WebApp.Areas.Default.Controllers", "WebApp.Controllers" }
            );
            context.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "WebApp.Areas.Default.Controllers", "WebApp.Controllers" }
            );
        }
    }
}