﻿using WebApp.Controllers;
using DomainLogic.Interfaces.Services;
using WebApp.Models;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainLogic.Infrastructure;
using DomainLogic.Models;
using WebApp.Models.Views.Default;

namespace WebApp.Areas.Default.Controllers
{
    public class HomeController : BaseController
    {

        public async Task<ActionResult> Index()
        {
            var model = LayoutsProvider.Get<HomePage>();
            await model.InitAsync();
            return View(model);
        }

        public async Task<ActionResult> About() {
            
            var model = LayoutsProvider.Get<AboutPage>();
            await model.InitAsync();
            return View(model);
        }

        public async Task<ActionResult> CorporateDepartment()
        {
            var model = LayoutsProvider.Get<CorporateDepartmentPage>();
            await model.InitAsync();
            return View(model);
        }        
    }
}