﻿using WebApp.Controllers;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainLogic.Models;
using WebApp.Models.Views.Default;

namespace WebApp.Areas.Default.Controllers
{
    public class NewsController : BaseController
    {
        public async Task<ActionResult> Index(int page = 1)
        {
            var model = LayoutsProvider.Get<NewsListPage>();
            await model.InitAsync(page);
            return View(model);
        }

        public async Task<ActionResult> Show(long id)
        {
            var model = LayoutsProvider.Get<NewsShowPage>();
            await model.InitAsync(id);

            if (model.Article == null)
                return new HttpNotFoundResult();

            return View(model);
        }

    }
}
