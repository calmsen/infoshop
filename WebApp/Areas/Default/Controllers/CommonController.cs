﻿using WebApp.Controllers;
using DomainLogic.Interfaces;
using DomainLogic.Interfaces.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using DomainLogic.Infrastructure;
using DomainLogic.Models;
using DomainLogic.Models.Settings;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Infrastructure.Credentials;

namespace WebApp.Areas.Default.Controllers
{
    /// <summary>
    /// Контроллер, в котором располагаются различные методы. Если методы накапливается много, то они группируются и для них создается уже отдельный контроллер.
    /// </summary>
    public class CommonController : BaseController
    {
        [Inject]
        public ICommonService CommonService { get; set; }
        [Inject]
        public IAddressesService AddressesService { get; set; }
        [Inject]
        public ICaptcha CaptchaGenerator { get; set; }

        public void Captcha() 
        {
            Response.Clear();
            Response.ContentType = "image/jpeg";
            Session["captcha"] = CaptchaGenerator.Generate(Response.OutputStream);
        }

        public async Task<string> GetAddressByRequestIp() 
        {
            string ip = GetUserIP();
            try
            {
                Address address = await AddressesService.GetAddressByIpAsync(ip);
                return JsonConvert.SerializeObject(address);
            }
            catch (Exception) 
            {
                return "{}";
            }                                 
        }

        public async Task<string> GetCityByIp()
        {
            string ip = GetUserIP();
            string city = await CommonService.GetCityByIpAsync(ip);
            return city;
        }

        public ActionResult TestCaptcha() 
        {
            return View();
        }

        [HttpPost]
        public string TestCaptcha(string captcha)
        {
            return string.Equals(Session["captcha"], captcha) ? "OK" : "NO";
        }

        public ActionResult BlurEffectTest()
        {
            return View();
        }
    }
}
