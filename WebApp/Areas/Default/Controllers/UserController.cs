﻿using WebApp.Controllers;
using WebApp.Models;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainLogic.Interfaces.Services;
using DomainLogic.Infrastructure.Attributes;
using WebApp.Infrastructure.Mvc.Utils;
using WebApp.Models.Views.Default;
using DomainLogic.Models;

namespace WebApp.Areas.Default.Controllers
{
    public class UserController : BaseController
    {
        [Inject]
        public UrlUtils UrlUtils { get; set; }

        public ActionResult ChangeFullName()
        {
            return View(new ChangeFullNameModel());
        }

        [HttpPost]
        public async Task<ActionResult> ChangeFullName(ChangeFullNameModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    User currentUser = await UserUiHelpers.GetCurrentUserAsync();
                    currentUser.FullName = model.FullName;
                    await UsersService.UpdateUserAsync(currentUser);

                    var message = new MessageViewModel
                    {
                        PageTitle = Resources.AccountRes.ChangeFullNameMessagePageTitle,
                        Message = Resources.AccountRes.ChangeFullNameMessageMessage
                    };
                    var modelHolder = CreateViewModelHolderAsync(message);
                    return View("Message", modelHolder);
                }
                catch (Exception)
                {

                }
            }
            return View(model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> UnsubscribeByHash(string email, string hash)
        {
            await UsersService.UnsubscribeAsync(email, hash);
            var message = new MessageViewModel
            {
                PageTitle = "Рассылка InfoShop",
                Message = "Рассылка InfoShop отменена."
            };
            var modelHolder = CreateViewModelHolderAsync(message);
            return View("Message", modelHolder);
        }

        public async Task<ActionResult> Unsubscribe()
        {
            string urlReferrer = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : null;
            if (urlReferrer == null || urlReferrer.IndexOf(UrlUtils.GetBaseUrl()) != 0)
                throw new Exception("Доступ запрещен");
            await UsersService.UnsubscribeAsync((await UserUiHelpers.GetCurrentUserAsync()).Id);
            return Redirect(urlReferrer);
        }

        public async Task<ActionResult> Subscribe()
        {
            string urlReferrer = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : null;
            if (urlReferrer == null || urlReferrer.IndexOf(UrlUtils.GetBaseUrl()) != 0)
                throw new Exception("Доступ запрещен");
            await UsersService.SubscribeAsync((await UserUiHelpers.GetCurrentUserAsync()).Id);
            return Redirect(urlReferrer);
        }

        public async Task<ActionResult> Subscription()
        {
            ViewBag.Subscription = (await UserUiHelpers.GetCurrentUserAsync()).Subscription;
            return View();
        }
    }
}
