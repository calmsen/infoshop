﻿using DomainLogic.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WebApp.Controllers;
using Newtonsoft.Json;
using WebApp.UiHelpers;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Models.Settings;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Infrastructure.Extensions;
using WebApp.Infrastructure.Mvc.Extensions;
using WebApp.Models.Filters;
using DomainLogic.Models.Enumerations;
using WebApp.Models.Forms;
using WebApp.Models.Views.Default;

namespace WebApp.Areas.Default.Controllers
{
    /// <summary>
    /// Обратная связь
    /// </summary>
    [Authorize]
    public class FeedbackController : BaseController
    {
        [Inject]
        public IFeedbacksService FeedbacksService { get; set; }
        [Inject]
        public ProductUiHelpers ProductUiHelpers { get; set; }
        [Inject]
        public FeedbackUiHelpers FeedbackUiHelpers { get; set; }
        [Inject]
        public FeedbackSettings FeedbackSettings { get; set; }
        [Inject]
        public CommonUtils CommonUtils { get; set; }


        #region Feedbacks
        [AllowAnonymous]
        public async Task<ActionResult> Index(long? sectionId, long? productId)
        {
            string urlReferrer = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : "";
            User currentUser = await UserUiHelpers.GetCurrentUserAsync();
            var form = new FeedbackForm
            {
                FullName = currentUser?.FullName,
                Email = currentUser?.Email,
                UrlReferer = urlReferrer,
                SectionId = sectionId,
                ProductId = productId,
                IsShowFeedbackMainFormGroups = false,
                IsReadOnlyFullNameInput = !string.IsNullOrEmpty(currentUser?.FullName),
                IsReadOnlyEmailInput = User.Identity.IsAuthenticated,
                IsShowCaptcha = !User.Identity.IsAuthenticated
            };

            var model = LayoutsProvider.Get<FeedbackFormPage>();
            await model.InitAsync(form);
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]        
        public async Task<ActionResult> Index(FeedbackForm form)
        {
            if (Request.Form.AllKeys.Contains("Captcha"))
                ValidateCaptcha(form.Captcha);
            //
            if (!ModelState.IsValid)
            {
                var model = LayoutsProvider.Get<FeedbackFormPage>();
                await model.InitAsync(form);
                return View(model);
            }

            if (form.SectionId == -1)
                form.SectionId = null;
            if (form.ProductId == -1)
                form.ProductId = null;

            form.FullName = form.FullName != null ? form.FullName.Trim() : null;
            form.Email = form.Email.Trim();

            var feedback = form.Map<Feedback>();
            User currentUser = await UserUiHelpers.GetCurrentUserAsync();
            feedback.User = currentUser ?? new User
            {
                FullName = form.FullName,
                Email = form.Email
            };
            await FeedbacksService.CreateFeedbackAsync(feedback);
                        
            SetAuthCookie();
            
            var message = new MessageViewModel
            {
                PageTitle = ResourceProvider.ReadResource("Resources.FeedbackRes.FeedbackFormPageTitle"),
                Message = ResourceProvider.ReadResource("Resources.FeedbackRes.CreateFeedbackSuccessMessage")
            };
            var modelHolder = CreateViewModelHolderAsync(message);
            return View("Message", modelHolder);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> EditImages(FeedbackImagesForm form)
        {
            await FeedbacksService.UpdateFeedbackImagesAsync(form.Map<Feedback>());
            return RedirectToAction("Show", "Feedback", new RouteValueDictionary { { "id", form.Id } } );
        }

        [Authorize(Roles = "Manager")]
        public async Task<ActionResult> List(FeedbackListFilter filter, int page = 1)
        {
            var model = LayoutsProvider.Get<FeedbackListPage>();
            await model.InitAsync(Request.QueryString, filter, page);
            return View(model);
        }
        
        public async Task<ActionResult> My()
        {
            var model = LayoutsProvider.Get<MyFeedbackListPage>();
            await model.InitAsync(User.Identity.Name);
            return View(model);
        }

        [Authorize(Roles = "Manager")]
        public async Task<ActionResult> ChangeState(FeedbackListFilter filter, long id, StateEnum stateForAction)
        {
            await FeedbacksService.ChangeStateAsync(id, stateForAction, (await UserUiHelpers.GetCurrentUserAsync()).Id);
            return RedirectToAction("List", "Feedback", filter.AsRouteValueDictionaryOfFormatedValues());
        }

        [AllowAnonymous]  
        public async Task<ActionResult> Show(long id, string returnUrl = null, string hash = null, string email = null, bool needPickUp = false)
        {
            if (string.IsNullOrEmpty(returnUrl) && Request.UrlReferrer != null && Request.UrlReferrer.LocalPath.IndexOf("/Feedback/List") != -1)
                returnUrl = Request.UrlReferrer.PathAndQuery;

            var currentUser = await UserUiHelpers.GetCurrentUserAsync();
            var model = LayoutsProvider.Get<FeedbackShowPage>();
            await model.InitAsync(id, returnUrl, hash, email, needPickUp);
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<ActionResult> ChangeModel(FeedbackChangeModelForm form)
        {
            await FeedbacksService.ChangeModelAsync(form.SectionId, form.ProductId, form.FeedbackId);
            return RedirectToAction("Show", "Feedback", new RouteValueDictionary { { "id", form.FeedbackId }, { "returnUrl", form.ReturnUrl } });
        }
        #endregion

        #region FeedbackAnswers
        [Authorize(Roles = "Manager")]
        public async Task<ActionResult> Answers(FeedbackAnswerListFilter filter, int page = 1)
        {
            var currentUser = await UserUiHelpers.GetCurrentUserAsync();
            var model = LayoutsProvider.Get<FeedbackAnswerListPage>();
            await model.InitAsync(Request.QueryString, filter, page);
            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> CreateFeedbackAnswer(FeedbackAnswerForm form, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var feedbackAnswer = form.Map<FeedbackAnswer>();
                //
                feedbackAnswer.User = await UserUiHelpers.GetCurrentUserAsync();
                if (form.Hash != null && form.AuthorEmail != null && string.Equals(form.Hash, CommonUtils.GetMd5Hash(form.Feedback.Id + form.AuthorEmail + FeedbackSettings.Md5HahSolt)))
                    feedbackAnswer.User = await UsersService.GetUserByEmailAsync(form.AuthorEmail);

                bool allowChangeState = User.IsInRole("Manager");
                bool needChangeState = false;
                if (allowChangeState)
                    needChangeState = form.Feedback.State != StateEnum.None ? true : false;

                await FeedbacksService.CreateFeedbackAnswerAsync(feedbackAnswer, needChangeState, form.NeedPickUp);
            }
            
            RouteValueDictionary routeValues = new RouteValueDictionary { { "id", form.Feedback.Id }, { "returnUrl", returnUrl } };
            if (form.Hash != null && form.AuthorEmail != null)
            {
                routeValues.Add("hash", form.Hash);
                routeValues.Add("email", form.AuthorEmail);
            }
            return RedirectToAction("Show", "Feedback", routeValues);
        }
        #endregion

        #region FeedbackThemes
        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<ActionResult> ChangeTheme(FeedbackThemeEnum theme, long feedbackId = 0, string returnUrl = null)
        {
            await FeedbacksService.ChangeThemeAsync(theme, feedbackId, (await UserUiHelpers.GetCurrentUserAsync()).Id);
            return RedirectToAction("Show", "Feedback", new RouteValueDictionary { { "id", feedbackId }, { "returnUrl", returnUrl } });
        }
        #endregion

        #region FeedbackInvitings
        [HttpPost]
        public async Task<ActionResult> InviteMessage(FeedbackInviteMessageForm form, string returnUrl = null)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Неправильные данные.");
                await FeedbacksService.InviteMessageAsync(form.FeedbackId, form.Emails.Select(x => x.Value).ToList(), form.Message);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message + " : " + JsonConvert.SerializeObject(form));
            }
            return RedirectToAction("Show", "Feedback", new { id = form.FeedbackId, returnUrl = returnUrl });
        }
        #endregion

        #region FeedbackTags
        [HttpPost]
        public async Task<string> CreateFeedbackTag(FeedbackTagForm form)
        {
            FeedbackTag tag = form.Map<FeedbackTag>();
            await FeedbacksService.CreateFeedbackTagAsync(tag);
            return "{}";
        }

        [HttpPost]
        public async Task<string> DeleteFeedbackTag(long feedbackId, string tagTitle)
        {
            await FeedbacksService.DeleteFeedbackTagAsync(feedbackId, tagTitle);
            return "{}";
        }

        public async Task<string> AutocompleteSearchTags(string term)
        {
            List<FeedbackTag> tags = await FeedbacksService.SearchTagsAsync(term);
            List<AutocompleteItem> data = FeedbackUiHelpers.ToAutocompleteData(tags);
            return JsonConvert.SerializeObject(data);
        }
        #endregion


        [AllowAnonymous]
        public async Task<bool> ValidateEmail(string Email, Action<bool> callback = null)
        {
            bool result;
            try
            {
                // если пользователь авторизован то подразумевается что введенный e-mail это его e-mail и проверка для этого не нужна
                if (User.Identity.IsAuthenticated)
                    result = true;
                // иначе делаем проверку
                if ((await UsersService.GetUserByEmailAsync(Email)) == null)
                    result = true;
                result = false;
            }
            catch
            {
                result = false;
            }
            callback?.Invoke(result);
            return result;
        }

        #region private methods
        private void SetAuthCookie()
        {
            HttpCookie noauth = new HttpCookie("noauth");
            Response.Cookies.Add(noauth);
        }
        
        #endregion  
    }
}
