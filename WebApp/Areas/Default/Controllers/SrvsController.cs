﻿using WebApp.Controllers;
using DomainLogic.Interfaces.Services;
using WebApp.Models;
using System.Web.Mvc;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using WebApp.Models.Views.Default;

namespace WebApp.Areas.Default.Controllers
{
    public class SrvsController : BaseController
    {
        [Inject]
        public ISrvsService SrvsService { get; set; }

        public async Task<ActionResult> Index(string city, string partner)
        {
            var model = LayoutsProvider.Get<ServicesPage>();
            await model.InitAsync(city, partner);
            return View(model);
        }

        public async Task<string> SrvsListAsXml()
        {
            Response.ContentType = "text/xml";
            // выберем все позиции
            var xml = await SrvsService.GetSrvsListAsXmlAsync();
            return xml;
        }
    }
}
