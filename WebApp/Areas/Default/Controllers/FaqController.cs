﻿using WebApp.Controllers;
using DomainLogic.Interfaces.Services;
using WebApp.Models;
using System.Web.Mvc;
using System.Threading.Tasks;
using Newtonsoft.Json;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Models.Enumerations;

namespace WebApp.Areas.Default.Controllers
{
    public class FaqController : BaseController
    {
        [Inject]
        public IFaqService FaqService { get; set; }

        [HttpPost]
        public async Task<string> Index(long? sectionId, long? productId, FeedbackThemeEnum theme = FeedbackThemeEnum.None)
        {
            var faqList = (await FaqService.GetFaqAsync(sectionId, productId, theme))
                .MapList<FaqView>();
            return JsonConvert.SerializeObject(faqList);
        }

    }
}
