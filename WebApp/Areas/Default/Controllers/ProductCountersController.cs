﻿using WebApp.Controllers;
using DomainLogic.Interfaces.Services;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using WebApp.Infrastructure.Mvc.Utils;

namespace WebApp.Areas.Default.Controllers
{
    public class ProductCountersController : BaseController
    {
        [Inject]
        public IProductCountersService ProductCountersService { get; set; }
        [Inject]
        public IProductsService ProductsService { get; set; }
        [Inject]
        public IAddressesService AddressesService { get; set; }
        [Inject]
        public CookieUtils CookieUtils { get; set; }

        [HttpPost]
        public async Task Increase(long productId)
        {
            long? cityId = null;
            if (CookieUtils.ContainsCookie("cityId")) 
            {
                try
                {
                    cityId = Convert.ToInt64(CookieUtils.GetCookie("cityId"));
                }
                catch (Exception){ }
            }
            if (cityId == null)
            {
                string ip = GetUserIP();
                try
                {
                    Address address = await AddressesService.GetAddressByIpAsync(ip);
                    cityId = address.CityId;
                }
                catch (Exception) { }
            }
            await ProductCountersService.AddOrUpdateByProductAsync(productId, cityId);
            
            if (!CookieUtils.ContainsCookie("cityId") && cityId > 0)
                CookieUtils.SetCookie("cityId", cityId.ToString(), 7);
        }

        public async Task UpdateProductCounters() 
        {
            await ProductsService.UpdateProductCountersAsync();
        }
    }
}
