﻿using WebApp.Controllers;
using WebApp.Models;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainLogic.Models;
using WebApp.Models.Views.Default;

namespace WebApp.Areas.Default.Controllers
{
    public class PartnersController : BaseController
    {
        public async Task<ActionResult> Index(int page = 1)
        {
            var model = LayoutsProvider.Get<PartnersListPage>();
            await model.InitAsync(page);
            return View(model);
        }

    }
}
