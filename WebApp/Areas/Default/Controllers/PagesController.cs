﻿using DomainLogic.Models;
using WebApp.Controllers;
using WebApp.Models;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebApp.Models.Views.Default;

namespace WebApp.Areas.Default.Controllers
{
    public class PagesController : BaseController
    {
        public async Task<ActionResult> Index(string pageName)
        {
            var model = LayoutsProvider.Get<PageDetail>();
            await model.InitAsync(pageName);

            if (model.Page == null)
                return new HttpNotFoundResult();

            return View(model);
        }

    }
}
