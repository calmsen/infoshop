﻿using WebApp.Controllers;
using WebApp.Models;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainLogic.Models;
using WebApp.Models.Views.Default;

namespace WebApp.Areas.Default.Controllers
{
    public class SupportArticlesController : BaseController
    {
        public async Task<ActionResult> Index(int page = 1)
        {
            var model = LayoutsProvider.Get<SupportArticlesListPage>();
            await model.InitAsync(page);
            return View(model);
        }
        
        public async Task<ActionResult> Show(long id)
        {
            var model = LayoutsProvider.Get<SupportArticleDetailPage>();
            await model.InitAsync(id);

            if (model.Article == null)
                return new HttpNotFoundResult();

            return View(model);
        }

    }
}
