﻿using WebApp.Controllers;
using WebApp.Models;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainLogic.Models;
using WebApp.Models.Views.Default;

namespace WebApp.Areas.Default.Controllers
{
    public class PostsController : BaseController
    {
        public async Task<ActionResult> Index(int page = 1)
        {
            var model = LayoutsProvider.Get<PostsListPage>();
            await model.InitAsync(page);
            return View(model);
        }

        public async Task<ActionResult> Show(long id)
        {
            var model = LayoutsProvider.Get<PostsShowPage>();
            await model.InitAsync(id);

            if (model.Post == null)
                return new HttpNotFoundResult();

            return View(model);
        }

    }
}
