﻿using WebApp.Controllers;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebApp.Models.Views.Default;
using WebApp.Models.Forms;

namespace WebApp.Areas.Default.Controllers
{
    public class VacanciesController : BaseController
    {
        public ActionResult Index()
        {
            return RedirectToAction("Career");
        }

        public async Task<ActionResult> Career()
        {
            var model = LayoutsProvider.Get<CareerPage>();
            await model.InitCareerPageAsync();
            return View(model);
        }

        public async Task<ActionResult> HowToGetJob(bool isCreatedResume = false)
        {
            var model = LayoutsProvider.Get<HowToGetJobPage>();
            model.ResumeForm = GetForm<ResumeForm>() ?? new ResumeForm();
            model.IsCreatedResume = isCreatedResume;

            await model.InitHowToGetJobPageAsync();
            return View(model);
        }

        public ActionResult Show(int id)
        {
            var model = LayoutsProvider.Get<VacancyShowPage>();
            if (model.Vacancy == null)
                return new HttpNotFoundResult();
            return View(model);
        }
    }
}
