﻿using WebApp.Controllers;
using DomainLogic.Interfaces.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Models.Enumerations;
using WebApp.Models.Views.Default;
using WebApp.Models.Views;

namespace WebApp.Areas.Default.Controllers
{
    public class FilesController : BaseController
    {
        [Inject]
        public IFilesService FilesService { get; set; }

        public async Task<ActionResult> Index(long? sectionId, long? productId, FileFilterEnum filters = FileFilterEnum.None)
        {
            var model = LayoutsProvider.Get<FilesPage>();
            await model.InitAsync(sectionId, productId, filters);
            return View(model);
        }

        [HttpPost]
        public async Task<string> FilesJson(FileFilterEnum filters, long productId)
        {
            List<DmFile> files = await FilesService.GetFilesAsync(filters, productId);
            var model = new FilesData
            {
                Files = files.MapList<FileView>(),
            };
            return JsonConvert.SerializeObject(model);
        }
    }
}
