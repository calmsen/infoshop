﻿using WebApp.Controllers;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Extensions;
using WebApp.Models.Forms;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Interfaces.Services;

namespace WebApp.Areas.Default.Controllers
{
    public class ResumesController : BaseController
    {
        [Inject]
        public IResumesService ResumesService { get; set; }

        [HttpPost]
        public async Task<ActionResult> Index(ResumeForm form)
        {
            try
            {
                ValidateCaptcha(form.Captcha);

                if (!ModelState.IsValid)
                    throw new Exception();

                var resume = form.Map<Resume>();
                await ResumesService.CreateResumeAsync(resume);
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);

                return PostRedirectGet(form, "HowToGetJob", "Vacancies");
            }

            return RedirectToAction("HowToGetJob", "Vacancies", new { isCreatedResume = true });
        }
    }
}
