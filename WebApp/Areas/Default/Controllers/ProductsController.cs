﻿using WebApp.Controllers;
using DomainLogic.Interfaces.Services;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Attributes;
using WebApp.Infrastructure.Mvc.Utils;
using WebApp.Models.Views.Default;

namespace WebApp.Areas.Default.Controllers
{
    public class ProductsController : BaseController
    {
        [Inject]
        public IProductsService ProductsService { get; set; }
        [Inject]
        public UrlUtils UrlUtils { get; set; }
        [Inject]
        public CrawlerUtils CrawlerUtils { get; set; }

        public async Task<ActionResult> Index(string sectionName = null, int page = 1, bool archive = false, string sortType = "Title", string groupType = null, int selectType = 0, string viewType = null, List<double> price = null)
        {
            var model = LayoutsProvider.Get<ProductListPage>();
            await model.InitAsync(Request.QueryString, sectionName, page, archive, sortType, groupType, selectType, viewType, price);
            return View(model);
        }

        public async Task<ActionResult> Catalog()
        {
            var model = LayoutsProvider.Get<CatalogPage>();
            await model.InitAsync();
            return View(model);
        }

        public async Task<ActionResult> Show(long productId, bool archive = false)
        {
            var model = LayoutsProvider.Get<ProductShowPage>();
            await model.InitAsync(productId, archive);

            if (model.Product == null)
                return new HttpNotFoundResult();

            return View(model);
        }
        
        public async Task<ActionResult> Search(string query, int page = 1)
        {
            var model = LayoutsProvider.Get<SearchProductsPage>();
            await model.InitAsync(query, page);
            return View(model);
        }

        public async Task<ActionResult> Compare(long sectionId)
        {
            var model = LayoutsProvider.Get<CompareProductsPage>();
            await model.InitAsync(sectionId);
            return View(model);
        }

        public async Task<string> InfoShopProductsAsXml(long? sectionId)
        {
            Response.ContentType = "text/xml";
            string xml = await ProductsService.GetProductsAsXmlAsync(sectionId ?? 0);
            return xml;
        }

        public async Task<ActionResult> InfoShopProductsInExcel(long? sectionId)
        {
            if (Request.UrlReferrer == null || Request.UrlReferrer.ToString().IndexOf(UrlUtils.GetBaseUrl()) != 0)
            {
                if (CrawlerUtils.IsCrawler())
                    return new HttpNotFoundResult();
                else
                    return new HttpStatusCodeResult(403, "Доступ запрещен");
            }
            byte[] excelAsBytes = await ProductsService.GetProductsInExcelAsync(sectionId ?? 0);
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=InfoShopProducts.xlsx");
            Response.BinaryWrite(excelAsBytes);
            return new EmptyResult();
        }

        public async Task ProductImagesAsZipArchive(long id)
        {
            byte[] zipAsBytes = await ProductsService.GetProductImagesAsZipAsync(id);

            Response.ClearContent();
            Response.Clear();
            Response.AddHeader("Content-Disposition", "inline; filename=" + id + "-images.zip");
            Response.ContentType = "application/zip";
            Response.BinaryWrite(zipAsBytes);
            Response.End();
            Response.Close();
        }

        public async Task<ActionResult> Presentation(long sectionId) 
        {
            if (Request.UrlReferrer == null || Request.UrlReferrer.ToString().IndexOf(UrlUtils.GetBaseUrl()) != 0)
            {
                if (CrawlerUtils.IsCrawler())
                    return new HttpNotFoundResult();
                else
                    return new HttpStatusCodeResult(403, "Доступ запрещен");
            }
            byte[] bs = await ProductsService.GenerateProductsPresentationAsync(sectionId);
            return File(bs, "application/pdf", "InfoShopPresentation.pdf");
        }
    }
}
