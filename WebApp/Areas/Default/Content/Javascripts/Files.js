﻿/**
 * Модуль содержит свойства и методы по работе с файлами
 * @author Ruslan Rakhmankulov
 */
define("Default/Files", ["jquery", "Default/MainLayout", "SelectControl", "SmartFormatMootools", "jqueryTmpl", "text!Default/CommonTmpl.htm"], function ($, MainLayout, SelectControl) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Files() {
        this.init.apply(this, arguments);
    }
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(Files.prototype = new MainLayout(), {
        /**
        * Все свойства defaults являются переопределяемые через объект options.
        */
        defaults: {
            denyAjax: false  // состояние, которое запрещает повторную отправку запроса аякс
            , resources: {} // ресурсы локализации
            , files: []
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(true, this, MainLayout.prototype.defaults, this.defaults, options);
            MainLayout.prototype.init.apply(this, arguments);
        }
        /**
         * Установим события для списка на главной странице 'Файлы'
         */
        , setFilesListEvents: function () {
            $("#search-files-query").on("blur", function (event) {
                $("#search-files-form").submit();
            });
            //
            if (this.files.length > 0)
                this.showFilesHolder(this.files);
            //
            this.initSectionSelectControl("SectionId", "ProductId");
            //
            $("#ProductId").on("change", function (event) {
                $("#InfoShop-files-wraper").empty();
                if ($("#Filters").closest(".form-group").is(":hidden"))
                    $("#search-files-form").submit();
                else if ($("#Filters").val() !== "")
                    $("#search-files-form").submit();
            });
            $("#Filters").on("change", function (event) {
                if ($("#ProductId").val() !== "")
                    $("#search-files-form").submit();
            });
        }

        /**
         * Инициализация селекта раздела
         * @param {Number} sectionId - ид элемента раздела
         * @param {Number} productId - ид элемента товара
         * @param {Boolean} secondSelectItem - получать ли второй элемент "Не важно"
         */
        , initSectionSelectControl: function (sectionId, productId) {
            $("#" + sectionId).on("change", $.proxy(this.sectionIdSelectOnChange, this))
            .selectControl({
                baseUrl: this.baseUrl
                , dependentSelectElem: $("#" + productId)
                , dependentSelectUri: "Admin/Products/ProductTitlesForSelectList"
                , dependentSelectParam: "sectionId"
            });
        }
        /**
         * Обработчик события change на элементе sectionIdSelect
         * @param {JQueryEventObject} event
         */
        , sectionIdSelectOnChange: function (event) {
            $("#InfoShop-files-wraper").empty();
            $("#Filters").val("");
            var sectionId = parseInt($("#SectionId").val());

            $("#Filters").closest(".form-group").show();
        }
        /**
         * Установка события на элемент filesSelect 
         */
        , setFilesEvents: function () {
            $("#Filters").on("change", $.proxy(this.filesSelectOnChange, this));
        }
        /**
         * Обработчик события change на элементе filesSelect
         * @param {JQueryEventObject} event
         */
        , filesSelectOnChange: function (event) {
            this.getFiles(event.currentTarget.value, $(event.currentTarget).closest("form").find("#productId").val());
        }
        /**
         * Получим название фильтра
         * @param {String} filtersAsArrayOfInt 
         * @returns {String}
         */
        , getFileFiltersAsString: function (filtersAsArrayOfInt) {
            var filterList = [];
            for (var i = 0; i < filtersAsArrayOfInt.length; i++) {
                switch (filtersAsArrayOfInt[i]) {
                    case 1: filterList.push("Фильтр 1");
                        break;
                    case 2: filterList.push("Фильтр 2");
                        break;
                    case 8: filterList.push("Фильтр 3");
                        break;
                    case 16: filterList.push("Фильтр 4");
                        break;
                    default: continue;
                }
            }

            return filterList.join(", ");

        }
        /**
         * Получим jquery элемент filesHolder посредством jqueryTmpl
         * @param {Array} Files
         * @returns {jQueryObject}
         */
        , getFilesHolder: function (Files) {
            for (var i = 0; i < Files.length; i++) {
                Files[i].FileFiltersAsString = this.getFileFiltersAsString(Files[i].Filters);
            }
            return $("#FilesHolder").tmpl({
                Files: Files
                , baseUrl: this.baseUrl
                , resources: this.resources
                , Smart: Smart
                , st0SiteHttpHost: this.st0SiteHttpHost
            });
        }
        /**
         * Отобразим элемент filesHolder в dom дереве
         * @param {Array} Files
         */
        , showFilesHolder: function (Files) {
            console.log(Files)
            $("#InfoShop-files-wraper").html(this.getFilesHolder(Files));
        }
        /**
         * Обработчик события success на получения данных о файлах
         * @param {Object} json
         */
        , getFilesOnSuccess: function (json) {
            this.showFilesHolder(json.Files);
            this.denyAjax = false;
        }
        /**
         * Обработчик события error на получения данных о файлах
         * @param {jQueryXhr} xhr
         * @param {String} status
         */
        , getFilesOnError: function (xhr, status) {
            this.denyAjax = false;
        }
        /**
         * Получим данные по файлам из удаленного сервера
         * @param {String} filters
         * @param {Number} productId
         */
        , getFiles: function (filters, productId) {
            if (this.denyjax) {
                return;
            }
            this.denyAjax = true;

            $("#Filters").find("option[value='']").text("Все файлы");

            $.ajax({
                url: this.baseUrl + this.culture + "/Files/FilesJson"
                , data: "filters=" + filters + "&productId=" + productId
                , type: "POST"
                , dataType: "json"
                , context: this
                , success: this.getFilesOnSuccess
                , error: this.getFilesOnError
            });
        }
        
    });
    return Files;
});