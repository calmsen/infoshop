﻿/**
 * Модуль содержит свойства и методы по работе с Резюме    
 * @author Ruslan Rakhmankulov
 */
define("Default/Resumes", ["jquery", "common", "globalize", "tinymce", "jqueryUnobtrusive", "ajaxfileupload", "jqueryToastmessage", "jqueryAlerts"], function ($, common, Globalize, tinymce) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Resumes() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(Resumes.prototype, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            baseUrl: common.baseUrl
            , culture: common.culture || "ru"
            , denyAjax: false // состояние, которое запрещает повторную отправку запроса аякс
            , denySubmit: false // состояние, которое запрещает повторную отправку формы
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(this, this.defaults, options);
            Globalize.culture(this.culture);
        }
        /**
         * Установим события на форму Resume
         */
        , setResumeFormEvents: function () {
            $("#save-resume-btn").on("click", $.proxy(this.saveResumeBtnOnClick, this));
            $("#resumeDoc").on("change", $.proxy(this.resumeDocFileOnChange, this));

        }
        /**
         * Обработчик события click на кнопке submit формы Resume
         * @param {JQueryEventObject} event
         */
        , saveResumeBtnOnClick: function (event) {
            event.preventDefault();
            var resumesObj = this;
            $(function () {
                if (resumesObj.denySubmit)
                    return;
                var formIsValid = $("#resume-form").valid();
                if (formIsValid) {
                    resumesObj.denySubmit = true;
                    $("#resume-form").submit();
                }
                return;
            });
            
        }
        /**
         * Обработчик события change на input-е file
         * @param {JQueryEventObject} event
         */
        , resumeDocFileOnChange: function (event) {
            this.loadResumeDoc();
        }
        /**
         * Отправим запрос на загрузку файла на удаленный сервер
         */
        , loadResumeDoc: function () {
            var file = $("#resumeDoc").val();
            var ext = file.substring(file.lastIndexOf(".")).toLowerCase();
            if (ext != ".doc" && ext != ".docx" && ext != ".rtf") {
                message.error("Файл должен быть с разрешением doc или docx.");
                return;
            }
            //
            $("#resumeDoc").hide();
            $("#loading").show();
            var resumesObj = this;

            $.ajaxFileUpload({
                url: this.baseUrl + "Admin/Files/LoadFile?folder=Resumes"
                , fileElementId: "resumeDoc"
                , type: "POST"
                , dataType: "json"
                , success: function (json) {
                    $("#ResumeDocLink").val(json.fileName);
                    $("#loading").hide();
                    $("#resumeDoc").hide();
                    $("#load-on-success").show();
                    $("form").validate().element("#ResumeDocLink");
                }
                , error: function (xhr, status) {
                    message.error(xhr.responseText);
                    $("#loading").hide();
                    $("#resumeDoc").show();
                }
            });
        }
    });
    return Resumes;
});