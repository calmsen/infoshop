﻿/**
 * Модуль содержит свойства и методы по работе с аккаунтом
 * @author Ruslan Rakhmankulov
 */
define("Default/Account", ["jquery", "common", "Default/MainLayout", "globalize", "jqueryUnobtrusive"], function ($, common, MainLayout, Globalize) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Account() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(Account.prototype = new MainLayout(), {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
             culture: common.culture || "ru"
            , denySubmit: false // состояние, которое запрещает повторную отправку формы
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(true, this, MainLayout.prototype.defaults, this.defaults, options);
            MainLayout.prototype.init.apply(this, arguments);
            Globalize.culture(this.culture);
        }
        /**
         * Установим события на форму 
         */
        , setFormEvents: function () {
            $("#submit-btn").on("click", $.proxy(this.submitBtnOnClick, this));
        }
        /**
         * Обработчик события click на кнопке submit формы 
         * @param {JQueryEventObject} event
         */
        , submitBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            var formIsValid = $("#form").valid();
            if (formIsValid) {
                this.denySubmit = true;
                $("#form").submit();
            }
            return;
        }
    });
    return Account;
});