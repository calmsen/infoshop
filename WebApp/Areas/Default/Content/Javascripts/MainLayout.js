﻿/**
 * Модуль содержит свойства и методы по работе с Main Layout
 * @author Ruslan Rakhmankulov
 */
define("Default/MainLayout", ["jquery", "common", "globalize", "bootstrap", "bootstrapHoverDropdown", "uiToTop"], function ($, common, Globalize) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function MainLayout() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var mainLayoutIsInit = false;
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(MainLayout.prototype, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            baseUrl: common.baseUrl
            , culture: common.culture || "ru"
            , st0SiteHttpHost: common.st0SiteHttpHost
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(this, this.defaults, options);
            Globalize.culture(this.culture);

            //
            if (!mainLayoutIsInit) {
                new MainLayout.NavbarSubMenu();
                new MainLayout.SiteHeader()
                    .setHeaderElementsEvents();

                mainLayoutIsInit = true;
            }
        }
    });
    /*
     * Определим другие модули:
     */
    $.extend(MainLayout, {
        /**
         * Модуль содержит свойства и методы по работе с Шапкой сайта
         * @author Ruslan Rakhmankulov
         */
        SiteHeader: (function ($) {
            /**
             * Конструктор является прокси для метода init
             * @constructor
             */
            function SiteHeader() {
                this.init.apply(this, arguments);
            }
            /**
             * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
             * Расширяем прототип с помощью jQuery.extend
             */
            $.extend(SiteHeader.prototype, {
                /**
                 * Все свойства defaults являются переопределяемые через объект options.
                 */
                defaults: {
                    baseUrl: common.baseUrl
                    , st0SiteHttpHost: common.st0SiteHttpHost
                    , culture: common.culture || "ru"
                    , currentUrl: location.href
                }
                /**
                 * Инициализиуем экземпляр класса
                 * @param {Object} options
                 */
                , init: function (options) {
                    $.extend(true, this, this.defaults, options);
                }
                /**
                 * Установка событий на шапку сайта
                 */
                , setHeaderElementsEvents: function () {
                    $(".js-set-culture-btn").on("click", $.proxy(this.setCultureBtnOnClick, this))
                }
                /**
                 * Обработчик события click на элементе setCultureBtn
                 * @param {JQueryEventObject} event
                 */
                , setCultureBtnOnClick: function (event) {
                    var culture = $(event.currentTarget).data("culture");
                    this.setPageCulture(culture);
                }
                /**
                 * Установим новую культуру в зависимости от текущей культуры
                 * @param {String} culture - текущая культура
                 */
                , setPageCulture: function (culture) {
                    var url = this.currentUrl.replace("/" + this.culture, "").replace(/\/$/, "");
                    if (culture != "ru") {
                        url = url.replace("://", "ProtocolSeparator");
                        var urlParts = url.split("/");
                        urlParts.splice(1, 0, culture);
                        url = urlParts.join("/");
                        url = url.replace("ProtocolSeparator", "://");
                    }
                    location.href = url.replace(/\/$/, "");
                }
            });
            return SiteHeader;
        }($, common))
        /**
         * Модуль содержит свойства и методы для очистки форм
         * @author Ruslan Rakhmankulov
         */
        , ClearForm: (function ($) {
            /**
             * Конструктор является прокси для метода init
             * @constructor
             */
            function ClearForm() {
                this.init.apply(this, arguments);
            }
            /**
             * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
             * Расширяем прототип с помощью jQuery.extend
             */
            $.extend(ClearForm.prototype, {
                /**
                 * Все свойства defaults являются переопределяемые через объект options.
                 */
                defaults: {
                    element: $()
                }
                /**
                 * Инициализиуем экземпляр класса
                 * @param {Object} options
                 */
                , init: function (options) {
                    $.extend(true, this, this.defaults, options);
                    this.setBtnEvents();
                }
                /**
                 * Обработка формы перед отправкой формы -
                 * очистка ненужных полей, установка правильных значение для полей ввода чисел,
                 * приведение значение к перечисление значений через запятую
                 */
                , clear: function () {
                    this.element.find(".form-group").each(function () {
                        var fieldset = $(this);
                        var type = fieldset.data("type");
                        if (type == "select") {
                            fieldset.find("select").filter(function () {
                                return this.value == "NaN";
                            }).removeAttr("name");
                        } else if (type == "checkbox") {
                            var name = fieldset.find("input:first").attr("name");
                            var checkboxs = fieldset.find("input:checked");
                            if (checkboxs.length > 0) {
                                var value = checkboxs.get().map(function (el) {
                                    return el.value;
                                });
                                value = value.join(",");
                                checkboxs.removeAttr("name");
                                $("<input/>").attr({ type: "hidden", name: name, value: value }).appendTo(fieldset);
                            }

                        } else if (type == "range" || type == "number") {
                            var inputs = fieldset.find("input").filter(function () {
                                return this.value == "" || this.value == "0" || isNaN(parseFloat(this.value));
                            });
                            if (inputs.length <= 1) {
                                var inputs = fieldset.find("input");
                                var name = inputs.attr("name");
                                var value = inputs.get().map(function (el) {
                                    return (el.value != "" ? parseFloat(el.value) : "0");
                                }).join(",");
                                $("<input/>").attr({ type: "hidden", name: name, value: value }).appendTo(fieldset);
                            }
                            inputs.removeAttr("name");
                        }
                    });
                }
                /**
                 * Обработчик события click на кнопке submit формы
                 * @param {JQueryEventObject} event
                 */
                , btnOnClick: function (event) {
                    //event.preventDefault();
                    this.clear();
                    //this.element.submit();
                }
                /**
                 * Установка событий на кнопку submit формы
                 */
                , setBtnEvents: function () {
                    this.element.find("button").on("click", $.proxy(this.btnOnClick, this));
                }
            });
            return ClearForm;
        }($))
        /**
         * Модуль содержит свойства и методы для работы с подменю
         * @author Ruslan Rakhmankulov
         */
        , NavbarSubMenu: (function ($, common) {
            /**
             * Конструктор является прокси для метода init
             * @constructor
             */
            function NavbarSubMenu() {
                this.init.apply(this, arguments);
            }
            /**
             * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
             * Расширяем прототип с помощью jQuery.extend
             */
            $.extend(NavbarSubMenu.prototype, {
                /**
                 * Все свойства defaults являются переопределяемые через объект options.
                 */
                defaults: {
                    baseUrl: common.baseUrl
                    , st0SiteHttpHost: common.st0SiteHttpHost
                }
                /**
                 * Инициализиуем экземпляр класса
                 * @param {Object} options
                 */
                , init: function (options) {
                    $.extend(true, this, this.defaults, options);
                    $(".js-navbar-list-group-item").on("mouseenter", $.proxy(this.listGroupItemOnMouseenter, this));
                    $(".js-navbar-list-group").on("mouseleave", $.proxy(this.listGroupOnMouseleave, this));
                }
                /**
                 * Установка отображаемой миниатюры для верхнего раздела
                 */
                , setThumbnailImage: function (imageId, sectionId) {
                    $("#navbar-thumbnail-image-" + sectionId).attr("src", this.st0SiteHttpHost + "Images/Sections/normal/" + imageId + ".jpg");
                }
                /**
                 * Обработчик события mouseenter на элементе listGroupItem
                 * @param {JQueryEventObject} event
                 */
                , listGroupItemOnMouseenter: function (event) {
                    var imageId = $(event.currentTarget).data("mainImageId");
                    var sectionId = $(event.currentTarget).data("parentSectionId");
                    this.setThumbnailImage(imageId, sectionId);
                }
                /**
                 * Обработчик события mouseleave на элементе listGroupItem
                 * @param {JQueryEventObject} event
                 */
                , listGroupOnMouseleave: function (event) {
                    var imageId = $(event.currentTarget).data("mainImageId");
                    var sectionId = $(event.currentTarget).data("sectionId");
                    this.setThumbnailImage(imageId, sectionId);
                }
            });
            return NavbarSubMenu;
        }($, common))
        /**
         * Модуль содержит свойства и методы для снятия установленных радиочекбоксов
         * @author Ruslan Rakhmankulov
         */
        , RadioboxUnchecked: (function ($) {
            /**
             * Конструктор является прокси для метода init
             * @constructor
             */
            function RadioboxUnchecked() {
                this.init.apply(this, arguments);
            }
            /**
             * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
             * Расширяем прототип с помощью jQuery.extend
             */
            $.extend(RadioboxUnchecked.prototype, {
                /**
                 * Все свойства defaults являются переопределяемые через объект options.
                 */
                defaults: {
                }
                /**
                 * Инициализиуем экземпляр класса
                 * @param {Object} options
                 */
                , init: function (options) {
                    $.extend(true, this, this.defaults, options);
                    this.setRadioboxEvents();
                }
                /**
                 * Установка событий для радиобоксов
                 */
                , setRadioboxEvents: function () {
                    $("[type='radio']")
                        .on("mousedown", $.proxy(this.radioOnMousedown, this));
                }
                /**
                 * Обработчик события mousedown на элементе radio
                 * @param {JQueryEventObject} event
                 */
                , radioOnMousedown: function (event) {
                    $(event.currentTarget).data("checked", event.currentTarget.checked);
                    setTimeout(function () {
                        if ($(event.currentTarget).data("checked")) {
                            event.currentTarget.checked = false;
                            fireEvent(event.currentTarget, "change");
                        }
                    }, 300);
                }
            });
            return RadioboxUnchecked;
        }($))
        /**
         * Модуль содержит свойства и методы для автоматической отправки формы
         * @author Ruslan Rakhmankulov
         */
        , AutoSubmit: (function ($) {
            /**
             * Конструктор является прокси для метода init
             * @constructor
             */
            function AutoSubmit() {
                this.init.apply(this, arguments);
            }
            /**
             * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
             * Расширяем прототип с помощью jQuery.extend
             */
            $.extend(AutoSubmit.prototype, {
                /**
                 * Все свойства defaults являются переопределяемые через объект options.
                 */
                defaults: {
                    timer: null
                    , interval: 3000
                    , element: $()
                    , clearFormObj: null
                    , currentInitialElement: null
                    , needSubmitStraight: false
                }
                /**
                 * Инициализиуем экземпляр класса
                 * @param {Object} options
                 */
                , init: function (options) {
                    $.extend(true, this, this.defaults, options);
                    var obj = this;
                    this.element.find("input[type='text']").on("input", function (event) {
                        obj.createAutosubmitProcessingElem(event.currentTarget);
                        obj.element.submit();
                    });
                    this.element.find("select, input").on("change", function (event) {
                        obj.createAutosubmitProcessingElem(event.currentTarget);
                        obj.element.submit();
                    });
                    this.element.on("submit", $.proxy(this.formOnSubmit, this));
                }
                /**
                 * Создаем элемент processing
                 */
                , createAutosubmitProcessingElem: function (inputElem) {
                    $(".autosubmit-processing")
                            .stop().remove();
                    this.currentInitialElement = $("<span/>", { "class": "autosubmit-processing" });
                    var formGroupHolder = $(inputElem).closest(".form-group-holder");
                    formGroupHolder.find(".autosubmit-processing-holder").append(this.currentInitialElement);
                    this.needSubmitStraight = formGroupHolder.data("needSubmitStraight");
                }
                /**
                 * Обработчик события submit на форме
                 * @param {JQueryEventObject} event
                 */
                , formOnSubmit: function (event) {
                    if (this.needSubmitStraight) {
                        if (this.clearFormObj != null)
                            this.clearFormObj.clear();
                        return;
                    }

                    event.preventDefault();
                    var obj = this;
                    if (this.currentInitialElement == null) {
                        if (this.timer)
                            clearTimeout(this.timer);
                        this.timer = setTimeout(function () {
                            if (obj.clearFormObj != null)
                                obj.clearFormObj.clear();
                            event.currentTarget.submit();
                        }, this.interval)
                    } else {
                        this.currentInitialElement
                            .animate(
                                { 'width': '100%' },
                                this.interval,
                                'easeInCirc',
                                function () {
                                    if (obj.clearFormObj != null)
                                        obj.clearFormObj.clear();
                                    event.currentTarget.submit();
                                }
                            );
                    }
                }
            });
            return AutoSubmit;
        }($))
    });
    return MainLayout;
});