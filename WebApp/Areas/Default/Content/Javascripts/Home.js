﻿/**
 * Модуль содержит свойства и методы по работе с Home Page
 * @author Ruslan Rakhmankulov
 */
define("Default/Home", ["jquery", "Default/MainLayout", "jcarousel"], function ($, MainLayout) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Home() {
        this.init.apply(this, arguments);
    }
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(Home.prototype = new MainLayout(), {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(true, this, MainLayout.prototype.defaults, this.defaults, options);
            MainLayout.prototype.init.apply(this, arguments);
        }
    });
    return Home;
});