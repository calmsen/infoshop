﻿/**
 * Модуль содержит свойства и методы по работе с Обратной связью
 * @author Ruslan Rakhmankulov
 */
define("Default/Feedback", ["jquery", "common", "Default/MainLayout", "Admin/ImagesV2", "SelectControl", "jqueryUi", "jqueryUnobtrusive", "jqueryTmpl", "text!Default/CommonTmpl.htm"], function ($, common, MainLayout, Images, SelectControl) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Feedback() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(Feedback.prototype = new MainLayout(), {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            denyAjax: false // состояние, которое запрещает повторную отправку запроса аякс
            , resources: {} // ресурсы локализации
            , currentUrl: location.href
            , denySubmit: false // состояние, которое запрещает повторную отправку формы
            , validationAttributes: {} // атрибуты валидации для jquery unobtrusive
            , faqCache: {}
            , showFeedbackMainFormGroups: false
            , setFeedbackTagsEventsDone: false
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(true, this, MainLayout.prototype.defaults, this.defaults, options);
            MainLayout.prototype.init.apply(this, arguments);
        }
        /**
         * Установим события на форму FeedbackChangeThemeForm
         */
        , setFeedbackChangeThemeFormEvents: function () {
            $("#feedbackChangeThemeForm").on("click", $.proxy(this.feedbackChangeThemeSumitBtnOnClick, this));
        }
        /**
         * Установим события на форму FeedbackChangeModelForm
         */
        , setFeedbackChangeModelFormEvents: function () {
            $("#feedbackChangeModelSubmitBtn").on("click", $.proxy(this.feedbackChangeModelSumitBtnOnClick, this));
            this.initSectionSelectControl("SectionId", "ProductId", false);
        }
        /**
         * Установим события на форму Feedback
         */
        , setFeedbackFormEvents: function () {
            $("#feedback-submit-btn").on("click", $.proxy(this.feedbackSubmitBtnOnClick, this));
            this.initSectionSelectControl("SectionId", "ProductId", true);
            $("#ProductId").on("change", $.proxy(this.productIdSelectOnChange, this));
            $("#Theme").on("change", $.proxy(this.themeSelectOnChange, this));
        }
        /**
         * Установим события для страницы Feedback Detailt
         */
        , setFeedbackDetailEvents: function () {
            if (!this.setFeedbackTagsEventsDone) {
                this.setFeedbackTagsEvents();
                this.setFeedbackTagsEventsDone = true;
            }
            this.setFaqItemsEvent();
            this.setFeedbackAnswerFormEvents();
            this.setFeedbackChangeModelFormEvents();
            this.setFeedbackChangeThemeFormEvents();
            this.setFeedbackInviteMessageFormEvents();
        }
        /**
         * Установим события и инициализируем плагина для списка Feedback
         */
        , setFeedbackListEvents: function () {
            feedbackObj = this;
            // инициализируем плагин автокомплит на поле Search для поиска тегов
            $("#Search").autocomplete({
                //source: this.baseUrl + "Feedback/AutocompleteSearchTags"
                source: function (request, response) {
                    var term = request.term.split(",").pop().trim();
                    if (term.length < 2)
                        return;
                    $.ajax({
                        url: feedbackObj.baseUrl + "Feedback/AutocompleteSearchTags",
                        type: "GET",
                        dataType: "json",
                        cache: false,
                        data: {
                            term: request.term.split(",").pop().trim()
                        },
                        success: function (data) {                        
                            response(data);
                        }
                    });
                }
                , minLength: 2
                , focus: function (event, ui) {
                    feedbackObj.setSearchValue(event, ui);
                }
                , select: function (event, ui) {
                    feedbackObj.setSearchValue(event, ui);
                }
            });
            // -- -- --
            if (!this.setFeedbackTagsEventsDone) {
                this.setFeedbackTagsEvents();
                this.setFeedbackTagsEventsDone = true;
            }
            //
            this.initSectionSelectControl("SectionId", "ProductId", true);
            $("#ProductId").on("change", $.proxy(this.productIdSelectOnChange, this));
        }
        /**
         * Установим события на форму Ответа на сообщение (FeedbackAnswer)
         */
        , setFeedbackAnswerFormEvents: function () {
            $("#feedback-answer-submit-btn").on("click", $.proxy(this.feedbackAnswerSubmitBtnOnClick, this));
        }
        /**
         * Инициализация селекта раздела
         * @param {Number} sectionId - ид элемента раздела
         * @param {Number} productId - ид элемента товара
         * @param {Boolean} secondSelectItem - получать ли второй элемент "Не важно"
         */
        , initSectionSelectControl: function (sectionId, productId, secondSelectItem) {
            $("#" + sectionId).on("change", $.proxy(this.sectionIdSelectOnChange, this))
            .selectControl({
                baseUrl: this.baseUrl
                , dependentSelectElem: $("#" + productId)
                , dependentSelectUri: this.culture + "/Admin/Products/ProductTitlesForSelectList"
                , dependentSelectParam: "sectionId"
                , dependentSelectData: {
                    secondSelectItem: secondSelectItem
                }
            });
        }
        /**
         * Обработчик события click на кнопке submit формы Feedback
         * @param {JQueryEventObject} event
         */
        , feedbackSubmitBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            var formIsValid = $("#feedback-form").valid();
            if (formIsValid) {
                this.denySubmit = true;
                $("#feedback-form").submit();
            }
            return;
        }
        /**
         * Обработчик события click на кнопке submit формы FeedbackChangeThemeForm
         * @param {JQueryEventObject} event
         */
        , feedbackChangeThemeSubmitBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            var formIsValid = $("#feedbackChangeThemeForm").valid();
            if (formIsValid) {
                this.denySubmit = true;
                $("#feedbackChangeThemeForm").submit();
            }
            return;
        }
        /**
         * Обработчик события click на кнопке submit формы FeedbackChangeModelForm
         * @param {JQueryEventObject} event
         */
        , feedbackChangeModelSubmitBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            var formIsValid = $("#feedbackChangeModelForm").valid();
            if (formIsValid) {
                this.denySubmit = true;
                $("#feedbackChangeModelForm").submit();
            }
            return;
        }
        /**
         * Обработаем и установим значение найденное значение
         * @param {JQueryEventObject} event
         * @param {Object} ui - ui содержит item элемент
         */
        , setSearchValue: function (event, ui) {
            var value = $("#Search").val();
            if (value.indexOf(",") != -1)
                value = value.replace(/,[^,]*$/, ", " + ui.item.value)
            else
                value = ui.item.value;
            $("#Search").val(value);
            event.preventDefault();
        }
        /**
         * Обработчик события click на кнопке submit формы FeedbackAnswer
         * @param {JQueryEventObject} event
         */
        , feedbackAnswerSubmitBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            var formIsValid = $("#feedback-answer-form").valid();
            if (formIsValid) {
                this.denySubmit = true;
                $("#feedback-answer-form").submit();
            }
            return;
        }
        /**
         * Установим события на форму Приглашения к обсуждению (FeedbackInvite)
         */
        , setFeedbackInviteMessageFormEvents: function () {
            $("#feedback-invite-message-submit-btn").on("click", $.proxy(this.feedbackInviteMessageSubmitBtnOnClick, this));
            $("#show-invite-messge-form-btn").on("click", $.proxy(this.showInviteMessageFormBtnOnClick, this));
            $("#Emails").on("input", $.proxy(this.inviteMessageEmailsInputOnInput, this));
        }
        /**
         * Обработчик события click на кнопке submit формы FeedbackInvite
         * @param {JQueryEventObject} event
         */
        , feedbackInviteMessageSubmitBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            if ($("#Emails").val().trim().length == 0) {
                message.error("Укажите электронный адрес получателя");
                return;
            }
            if ($("#Emails_0__Value").length == 0) {
                message.error("Электронный адрес получателя указан неправильно");
                return;
            }
            
            var formIsValid = $("#feedback-invite-message-form").valid();
            if (formIsValid) {
                this.denySubmit = true;
                $("#feedback-invite-message-form").submit();
            }
            return;
        }
        /**
         * Обработчик события click на кнопке showInviteMessageFormBtn - показать или скрыть форму FeedbackInvite
         * @param {JQueryEventObject} event
         */
        , showInviteMessageFormBtnOnClick: function (event) {
            if ($("#invite-message-form").is(":visible")) {
                $("#invite-message-form").hide();
            } else {
                $("#invite-message-form").show();
            }
        }
        /**
         * Получим jquery элемент email посредством jqueryTmpl
         * @param {Number} valueIndex - порядковый номер email-a
         * @param {String} value - email
         * @returns {jQueryObject}
         */
        , getInviteMessageEmailValue: function (valueIndex, value) {
            return $("#feedbackInviteMessageEmailValue").tmpl({
                valueIndex: valueIndex
                , value: value
                , validationAttributes: this.validationAttributes
            });
        }
        /**
         * Отобразим email в dom дереве
         * @param {Number} valueIndex - порядковый номер email-a
         * @param {String} value - email
         */
        , showInviteMessageEmailValue: function (valueIndex, value) {
            this.getInviteMessageEmailValue(valueIndex, value).appendTo("#emails-holder");
        }
        /**
         * Отобразим введенные email-ы после обработки значения
         * @param {String} values - email-ы через запятую
         */
        , showAllInviteMessageEmailValues: function (values) {
            $("#emails-holder").empty();
            var valuesAsArray = values.split(",")
            for (var i in valuesAsArray) {
                if (valuesAsArray[i].indexOf("@") == -1 || valuesAsArray[i].indexOf(".") == -1)
                    continue;
                this.showInviteMessageEmailValue(i, valuesAsArray[i].trim());
            }
        }
        /**
         * Обработчик события input на элементе ввода email-а (inviteMessageEmailsInput)
         * @param {JQueryEventObject} event
         */
        , inviteMessageEmailsInputOnInput: function (event) {
            this.showAllInviteMessageEmailValues(event.currentTarget.value);
        }
        /**
         * Отправим запрос на добавление тега на удаленный сервер
         * @param {Number} feedbackId
         */
        , createFeedbackTag: function (feedbackId) {
            if (this.denyAjax)
                return;
            var tag = $("#create-feedback-tag-input-" + feedbackId).val().trim();
            if (tag == "")
                return;
            if ($("#feedback-tag-form-" + feedbackId + " .js-feedback-tag-title:contains('" + tag + "')").length > 0)
                return;
            this.denyAjax = true;
            $.ajax({
                url: this.baseUrl + "Feedback/CreateFeedbackTag"
                , data: {
                    title: tag
                    , feedbackId: feedbackId
                }
                , type: "POST"
                , dataType: "json"
                , context: this
                , success: function () {
                    this.showFeedbackTag(feedbackId, tag);
                    this.denyAjax = false;
                }
                , error: function (status, xhr) {
                    this.denyAjax = false;
                }
            });
        }
        /**
         * Отобразим тег в dom дереве
         * @param {Number} feedbackId
         * @param {String} tag
         */
        , showFeedbackTag: function (feedbackId, tag) {
            this.getFeedbackTag(feedbackId, tag).appendTo("#feedback-tags-" + feedbackId);
            this.setFeedbackTagEvents("#feedback-tag-form-" + feedbackId + " .js-feedback-tag-title:contains('" + tag + "')");
            $("#create-feedback-tag-input-" + feedbackId).val("");
        }
        /**
         * Получим jquery элемент tag посредством jqueryTmpl
         * @param {Number} feedbackId
         * @param {String} tag
         * @returns {jQueryObject}
         */
        , getFeedbackTag: function (feedbackId, tag) {
            return $("#feedbackTagTmpl").tmpl({
                feedbackId: feedbackId
                , tag: tag
            })
        }
        /**
         * Установим события на тег
         * @param {String} feedbackTagRemoveBtnSelector - css селектор для поиска кнопок для удаления тега
         */
        , setFeedbackTagEvents: function (feedbackTagRemoveBtnSelector) {
            $(feedbackTagRemoveBtnSelector)
                .closest(".js-feedback-tag")
                .find(".js-delete-feedback-tag-btn")
                .on("click", $.proxy(this.deleteFeedbackTagBtnOnClick, this));
        }
        /**
         * Обработчик события click на кнопку для добавления тега
         * @param {JQueryEventObject} event
         */
        , createFeedbackTagBtnOnClick: function (event) {
            var feedbackId = $(event.currentTarget).closest(".js-feedback-tag-form").data("feedbackId");
            this.createFeedbackTag(feedbackId);
        }
        /**
         * Отправим запрос на удаление тега на удаленный сервер
         * @param {Number} feedbackId
         * @param {String} tag
         */
        , deleteFeedbackTag: function (feedbackId, tag) {
            if (this.denyAjax)
                return;
            this.denyAjax = true;
            $.ajax({
                url: this.baseUrl + "Feedback/DeleteFeedbackTag"
                , data: {
                    feedbackId: feedbackId
                    , tagTitle: tag
                }
                , type: "POST"
                , dataType: "json"
                , context: this
                , success: function () {
                    this.removeFeedbackTag(feedbackId, tag);
                    this.denyAjax = false;
                }
                , error: function () {
                    this.denyAjax = false;
                }
            });

        }
        /**
         * Удаления тега из dom дерева
         * @param {Number} feedbackId
         * @param {String} tag
         */
        , removeFeedbackTag: function (feedbackId, tag) {
            $("#feedback-tag-form-" + feedbackId + " .js-feedback-tag-title:contains('" + tag + "')")
                .closest(".js-feedback-tag")
                .remove();
        }
        /**
         * Обработчик события click на кнопку для удаления тега
         * @param {JQueryEventObject} event
         */
        , deleteFeedbackTagBtnOnClick: function (event) {
            var feedbackId = $(event.currentTarget).closest(".js-feedback-tag-form").data("feedbackId");
            var tagTitle = $(event.currentTarget).closest(".js-feedback-tag").data("tagTitle");
            this.deleteFeedbackTag(feedbackId, tagTitle);
        }
        /**
         * Установим события на списки тегов
         */
        , setFeedbackTagsEvents: function () {
            $(".js-create-feedback-tag-btn").on("click", $.proxy(this.createFeedbackTagBtnOnClick, this));
            $(".js-create-feedback-tag-input").autocomplete({
                source: this.baseUrl + "Feedback/AutocompleteSearchTags"
                , minLength: 2
            });
            this.setFeedbackTagEvents(".js-delete-feedback-tag-btn");
        }
        /**
         * Установим состояния по умолчанию
         */
        , resetContextStates: function () {
            $("#Theme").val(-1);
            $("#feedbackFaqList").empty();
            $("#feedbackMainFormGroups").hide();
            $("#serialNumberPrompt").hide();
        }
        /**
         * Обработчик события change на селекте SectionId
         * @param {JQueryEventObject} event
         */
        , sectionIdSelectOnChange: function (event) {
            this.resetContextStates();

            var sectionId = $("#SectionId").val() || 0;
            
            if (sectionId >= 0)
                $("#ProductId").closest(".form-group").show();
            else
                $("#ProductId").closest(".form-group").hide();

            if (sectionId == -1)
                $("#Theme").closest(".form-group").show();
            else
                $("#Theme").closest(".form-group").hide();
        }
        /**
         * Обработчик события change на селекте ProductId
         * @param {JQueryEventObject} event
         */
        , productIdSelectOnChange: function (event) {
            this.resetContextStates();

            var productId = $("#ProductId").val() || 0;

            if (productId > 0 || productId == -1)
                $("#Theme").closest(".form-group").show();
            else 
                $("#Theme").closest(".form-group").hide();
        }
        /**
         * Обработчик события change на селекте Theme
         * @param {JQueryEventObject} event
         */
        , themeSelectOnChange: function (event) {
            var theme = $("#Theme").val() || "";

            this.showOrHideFaqList(theme);

            if (theme !== "") {
                $("#feedbackMainFormGroups").show();
            } else {
                $("#feedbackMainFormGroups").hide();
            }
            if (theme == "ServiceQuestions") {
                $("#serialNumberPrompt").show();
            } else {
                $("#serialNumberPrompt").hide();
            }
        }
        /**
         * Отобразим или скроем список вопросов и ответов
         * @param {String} theme 
         */
        , showOrHideFaqList: function (theme) {
            if (theme === "")
                $("#faqHolder").hide();

            var sectionId = $("#SectionId").val() || 0;
            var productId = $("#ProductId").val() || 0;
            

            this.getFaqData(sectionId, productId, theme).wait(function (faqList) {
                this.showFaqItems(faqList);

                if (faqList.length > 0)
                    $("#faqHolder").show();
                else 
                    $("#faqHolder").hide();
            }, function () {
                message.error("Произошла ошибка на сервере.");                
            }, this);
        }
        /**
         * Получим данные о вопросов и ответов с удаленного сервера
         * @param {Number} sectionId
         * @param {Number} productId
         * @param {String} theme
         */
        , getFaqData: function (sectionId, productId, theme) {
            var cacheKey = sectionId + "_" + productId + "_" + theme;
            if (this.faqCache[cacheKey] === undefined)
                this.faqCache[cacheKey] = $.ajax({
                    url: this.baseUrl + "Faq"
                    , data: {
                        sectionId: sectionId == 0 ? "" : sectionId
                        , productId: productId == 0 ? "" : productId
                        , theme: theme
                    }
                    , type: "post"
                    , dataType: "json"
                });
            return this.faqCache[cacheKey];
        }
        /**
        * Получим список jquery элементов faqItem посредством jqueryTmpl
        * @param {Array} faqList
        * @returns {jQueryObject}
        */
        , getFaqItems: function (faqList) {
            return $("#feedbackFormFaqItem").tmpl(faqList);
        }
        /**
         * Установим события на список элементов faqItem
         */
        , setFaqItemsEvent: function () {
            $(".js-faq-item-link").on("click", function (event) {
                $(event.currentTarget).closest(".js-faq-item").find(".js-faq-item-content").toggle();
            });
            if ($("#feedback-answer-form").length > 0) {
                $(".js-faq-copy-answer, .js-faq-item-answer").on("click", function (event) {
                    var answer = $(event.currentTarget).closest(".js-faq-item").find(".js-faq-item-answer").text();
                    var message = $("#Message").val();
                    if (message)
                        message += "\r\n";
                    $("#Message").val(message + answer);
                });
            }
        }
        /**
         * Отобразим список элементов faqItem в dom дереве
         */
        , showFaqItems: function (faqList) {
            $("#feedbackFaqList").empty();
            this.getFaqItems(faqList)
                .appendTo("#feedbackFaqList");
            this.setFaqItemsEvent();
        }
    });
    return Feedback;
});