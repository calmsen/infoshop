﻿/**
 * Модуль содержит свойства и методы по работе с СЦ
 * @author Ruslan Rakhmankulov
 */
define("Default/Srvs", ["jquery", "Default/MainLayout"], function ($, MainLayout) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Srvs() {
        this.init.apply(this, arguments);
    }
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(Srvs.prototype = new MainLayout(), {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(true, this, MainLayout.prototype.defaults, this.defaults, options);
            MainLayout.prototype.init.apply(this, arguments);
        }
        /**
         * Установим события на список СЦ
         */
        , setSrvsListEvents: function () {
            $("#city").on("change", function () {
                var cityEl = $(this);
                var partnerEl = $("#partner");
                if (cityEl.val().length != 0) {
                    cityEl.closest(".form-group").removeClass("has-error");
                    if (partnerEl.val().length != 0) {
                        partnerEl.closest("form").submit();
                    } else {
                        partnerEl.closest(".form-group").addClass("has-error");
                    }
                } else {
                    cityEl.closest(".form-group").addClass("has-error");
                }
            });
            $("#partner").on("change", function () {
                var partnerEl = $(this);
                var cityEl = $("#city");
                if (partnerEl.val().length != 0) {
                    partnerEl.closest(".form-group").removeClass("has-error");
                    if (cityEl.val().length != 0) {
                        cityEl.closest("form").submit();
                    } else {
                        cityEl.closest(".form-group").addClass("has-error");
                    }
                } else {
                    partnerEl.closest(".form-group").addClass("has-error");
                }
            });

        }
        
    });
    return Srvs;
});