﻿/**
 * Модуль содержит свойства и методы по работе с Товарами
 * @author Ruslan Rakhmankulov
 */
define("Default/Products", ["jquery", "common", "Default/MainLayout", "Default/Files", "shadowbox", "globalize", "Default/BlurEffectImage", "SelectControl", "jqueryTmpl", "text!Default/CommonTmpl.htm", "jqueryToastmessage", "SmartFormatMootools", "jqueryValidateGlobalize", "bootstrapMultiselect", "cookie"], function ($, common, MainLayout, Files, Shadowbox, Globalize, BlurEffectImage, SelectControl) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Products() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(Products.prototype = new MainLayout(), {
        defaults: {
            baseUrl: common.baseUrl
            , st0SiteHttpHost: common.st0SiteHttpHost
            , culture: common.culture || "ru"
            , denyAjax: false // состояние, которое запрещает повторную отправку запроса аякс
            , resources: {} // ресурсы локализации
            , ProductCodesByApprUpgToWin10: []
            , files: []
        }
        , init: function (options) {
            $.extend(true, this, MainLayout.prototype.defaults, this.defaults, options);
            MainLayout.prototype.init.apply(this, arguments);
            Globalize.culture(this.culture);
        }
        /**
         * Установим события для списка товаров
         */
        , setProductsListEvent: function () {
            //
            new MainLayout.RadioboxUnchecked();
            //
            this.setMultiselectEvents();
            //
            $("#show-filters-btn").on("click", $.proxy(this.showFiltersBtnOnClick, this));
            $("#hide-filters-btn").on("click", $.proxy(this.hideFiltersBtnOnClick, this));
            //   
            new MainLayout.AutoSubmit({
                element: $("#filters")
                , clearFormObj: new MainLayout.ClearForm({
                    element: $("#filters")
                })
            });
            //
            new MainLayout.AutoSubmit({
                element: $("#sortTypeForm")
                , clearFormObj: new MainLayout.ClearForm({
                    element: $("#sortTypeForm")
                })
            });
            //
            this.setParamsForCancelFormEvents();
            //
            this.setCompareProductEvents();
        }
        /**
         * Установим события на карточку товара
         */
        , setDetailProductEvents: function () {
            this.setBlurEffectImageHolderStyles();
            this.increaseCounter();
            // инициализация модуля Файлы
            var filesObj = new Files({
                baseUrl: this.baseUrl
                , st0SiteHttpHost: this.st0SiteHttpHost
                , resources: this.resources
                , culture: this.culture
            });
            //
            if (this.files.length)
                filesObj.showFilesHolder(this.files);
            // -- -- --
            filesObj.setFilesEvents();
            //
            this.setCompareProductEvents();
            //
            this.setFaqEvents();
        }
        /**
         * Расчитаем стили для затемнения экранов, относительно десктопных экранов
         */
        , calcBlurEffectImageHolderStyles: function (width, left, top, k) {
            return [
                Math.round(width * k, 2)
                , Math.round(left * k, 2)
                , Math.round(top * k, 2)
            ]
        }
        /**
         * Установим стили для затемнения экранов
         */
        , setBlurEffectImageHolderStyles: function () {
            var obj = this;
            $(".blur-effect-wrap").each(function (i) {
                var el = $(this);
                var styles = el.find("h4").text().replace(/\[style\]|\[\/style\]|s*/g, "").replace(/;$/, "").split(";");
                var width, left, top;
                for (var j in styles) {
                    if (styles[j].indexOf("width") >= 0) {
                        width = parseInt(styles[j].replace("width:", ""));
                    }
                    else if (styles[j].indexOf("left") >= 0) {
                        left = parseInt(styles[j].replace("left:", ""));
                    }
                    else if (styles[j].indexOf("top") >= 0) {
                        top = parseInt(styles[j].replace("top:", ""));
                    }
                }

                var desctopDims = obj.calcBlurEffectImageHolderStyles(width, left, top, 878 / 1078);
                var tabletDims = obj.calcBlurEffectImageHolderStyles(width, left, top, 658 / 1078);
                var phoneDims = obj.calcBlurEffectImageHolderStyles(width, left, top, 358 / 1078);
                el
                    .addClass("blur-effect-wrap-" + i)
                    .prepend('<style>' +
                        '.blur-effect-wrap-' + i + ' .blur-effect-image-holder {' +
                            'width: ' + width + 'px;' +
                            'top: ' + top + 'px;' +
                            'left: ' + left + 'px;' +
                        '}' +
                        '@media (max-width: 1199px) {' +
                            '.blur-effect-wrap-' + i + ' .blur-effect-image-holder {' +
                                'width: ' + desctopDims[0] + 'px;' +
                                'top: ' + desctopDims[2] + 'px;' +
                                'left: ' + desctopDims[1] + 'px;' +
                            '}'+
                        '}' +
                        '@media (max-width: 991px) {' +
                            '.blur-effect-wrap-' + i + ' .blur-effect-image-holder {' +
                                'width: ' + tabletDims[0] + 'px;' +
                                'top: ' + tabletDims[2] + 'px;' +
                                'left: ' + tabletDims[1] + 'px;' +
                            '}' +
                        '}' +
                        '@media (max-width: 767px) {' +
                            '.blur-effect-wrap-' + i + ' .blur-effect-image-holder {' +
                                'width: ' + phoneDims[0] + 'px;' +
                                'top: ' + phoneDims[2] + 'px;' +
                                'left: ' + phoneDims[1] + 'px;' +
                            '}' +
                        '}' +
                    '</style>')

            })
            .css("opacity", 1);
            $(".blur-effect-slider").blurEffectImage();
        }
        /**
         * Увеличим счетчик на удаленном сервере
         */
        , increaseCounter: function () {
            $.ajax({
                url: this.baseUrl + "ProductCounters/Increase"
                , data: "productId=" + this.productId
                , type: "POST"
            });
        }
        /**
         * Покажем фильтры 
         */
        , showFilters: function () {
            $("#filters").show();
            $("#show-filters-btn").hide();
            $("#hide-filters-btn").show();
        }
        /**
         * Скроем фильтры
         */
        , hideFilters: function () {
            $("#filters").hide();
            $("#hide-filters-btn").hide();
            $("#show-filters-btn").show();
        }
        /**
         * Обработчик события click на кнопке showFiltersBtn
         * @param {JQueryEventObject} event
         */
        , showFiltersBtnOnClick: function (event) {
            this.showFilters();
        }
        /**
         * Обработчик события click на кнопке hideFiltersBtn
         * @param {JQueryEventObject} event
         */
        , hideFiltersBtnOnClick: function () {
            this.hideFilters();
        }
        /**
         * Установка событий на Multiselect чекбоксы
         */
        , setMultiselectEvents: function () {
            var obj = this;
            $(".multiselect-container").on("click", function (event) {
                event.stopPropagation();
            });
            $(".multiselect-container input").on("change", function (event) {
                var buttonText = $(event.currentTarget).closest(".multiselect-container").find("input:checked")
                    .map(function () {
                        return $(this).data("title");
                    })
                    .get().join(",");;
                $(event.currentTarget).closest(".multiselect-holder").find(".multiselect-selected-text").text(buttonText);
            });
            /*$(".multiselect-holder select").multiselect({
                buttonWidth: "100%",
                buttonText: function (options, select) {
                    var labels = [];
                    options.each(function () {
                        labels.push($(this).text());
                    });
                    if (labels.length == 0)
                        return "--";
                    return labels.join(", ");
                },
                maxHeight: 200,
                onChange: function (option, checked, select) { }
            });*/
        }
        /**
         * Установка событий для формы по отмене фильтров
         */
        , setParamsForCancelFormEvents: function () {
            $(".js-delete-filter-param-btn").on("click", function (event) {
                $(event.currentTarget).closest(".js-filter-param").remove();
                $("#filterParamsForCancelForm").submit();
            });
            new MainLayout.AutoSubmit({
                element: $("#filterParamsForCancelForm")
                , clearFormObj: new MainLayout.ClearForm({
                    element: $("#filterParamsForCancelForm")
                })
            });
        }
        /**
         * Работа с обновлением до win 10
         */
        /**
         * Установка событий на элемент checkUpgradeToWindows10Inp
         */
        , setUpgradeToWindows10Events: function () {
            $("#checkUpgradeToWindows10Inp")
                .on("blur", $.proxy(this.checkUpgradeToWindows10InpOnChange, this))
                .on("keypress", $.proxy(this.checkUpgradeToWindows10InpOnEnter, this))
                .on("input", $.proxy(this.checkUpgradeToWindows10InpOnInput, this));
        }
        /**
         * Проверим устройсво на возможность обновления
         * @param {String} query 
         */
        , checkUpgradeToWindows10: function (query) {
            if (/^\d+$/.test(query)) {
                for (var i in this.ProductCodesByApprUpgToWin10) {
                    if (this.ProductCodesByApprUpgToWin10[i].Id == query) {
                        if (this.ProductCodesByApprUpgToWin10[i].Upgrade)
                            this.showUpgradeToWindowsSuccessMessage();
                        else
                            this.showUpgradeToWindowsErrorMessage();
                        return;
                    }
                }
            } else {
                for (var i in this.ProductCodesByApprUpgToWin10) {
                    if (this.ProductCodesByApprUpgToWin10[i].Title == query) {
                        if (this.ProductCodesByApprUpgToWin10[i].Upgrade)
                            this.showUpgradeToWindowsSuccessMessage();
                        else
                            this.showUpgradeToWindowsErrorMessage();
                        return;
                    }
                }
            }
            this.showUpgradeToWindowsWarningMessage();
        }
        /**
         * Отобразим сообщение об ошибке
         */
        , showUpgradeToWindowsErrorMessage: function () {
            $("#upgradeToWindowsWarningMessage").hide();
            $("#upgradeToWindowsSuccessMessage").hide();
            $("#upgradeToWindowsErrorMessage").show();
        }
        /**
         * Отобразим сообщение об успехе
         */
        , showUpgradeToWindowsSuccessMessage: function () {
            $("#upgradeToWindowsWarningMessage").hide();
            $("#upgradeToWindowsErrorMessage").hide();
            $("#upgradeToWindowsSuccessMessage").show();
        }
        /**
         * Отобразим сообщение о предупреждении
         */
        , showUpgradeToWindowsWarningMessage: function () {
            $("#upgradeToWindowsErrorMessage").hide();
            $("#upgradeToWindowsSuccessMessage").hide();
            $("#upgradeToWindowsWarningMessage").show();
        }
        /**
         * Скроем все сообщение
         */
        , hideAllMessage: function () {
            $("#upgradeToWindowsErrorMessage").hide();
            $("#upgradeToWindowsSuccessMessage").hide();
            $("#upgradeToWindowsWarningMessage").hide();
        }
        /**
         * Обработчик события change на элементе checkUpgradeToWindows10Inp
         * @param {JQueryEventObject} event
         */
        , checkUpgradeToWindows10InpOnChange: function (event) {
            var query = event.currentTarget.value.trim();
            if (query == "")
                return;
            this.checkUpgradeToWindows10(event.currentTarget.value.trim());
        }
        /**
         * Обработчик события enter на элементе checkUpgradeToWindows10Inp
         * @param {JQueryEventObject} event
         */
        , checkUpgradeToWindows10InpOnEnter: function (event) {
            if (event.keyCode != 13)
                return;
            var query = event.currentTarget.value.trim();
            if (query == "")
                return;
            this.checkUpgradeToWindows10(event.currentTarget.value.trim());
        }
        /**
         * Обработчик события input на элементе checkUpgradeToWindows10Inp
         * @param {JQueryEventObject} event
         */
        , checkUpgradeToWindows10InpOnInput: function (event) {
            this.hideAllMessage();
        }
        // Работа с сравнениями товаров
        /**
         * Установим количество товаров в сравнении на кнопке compareProductsAmount 
         */
        , setCompareProductsAmount: function (amount) {
            if (amount < 0)
                amount = 0;
            $("#compareProductsAmount").text(Smart.format(this.resources.ProductsRes.CompareProductsLink, { "0": amount, "1": amount }));
        }
        /**
         * Покажем или скроем кнопку compareProductsBtn
         * @param {Number} amount
         */
        , showOrHideCompareProductsBtn: function (amount) {
            if (amount > 0)
                $("#compareProductsBtn").removeClass("hidden");
            else
                $("#compareProductsBtn").addClass("hidden");
        }
        /**
         * Подстроим отображаемую информацию на странице, в случае если товары удаляли или добавляли на других страницах
         * @param {Number} amount
         */
        , adjustCompareProductsInfo: function (amount) {
            var productsInComparison = JSON.parse($.cookie("productsInComparison") || "[]");
            // активируем кнопки
            for (var i in productsInComparison) {
                this.activeCompareProductBtn(productsInComparison[i].productId);
            }
            // деактивируем кнопки
            var obj = this;
            $(".js-compare-product-btn.active").each(function () {
                var productId = $(this).closest(".js-product-holder").data("productId");
                var productIsFinded = false;
                for (var i in productsInComparison) {
                    if (productsInComparison[i].productId == productId) {
                        productIsFinded = true;
                        break;
                    }
                }
                if (!productIsFinded)
                    obj.deactiveCompareProductBtn(productId)

            });
            //
            this.setCompareProductsAmount(amount);
            //
            this.showOrHideCompareProductsBtn(amount);
        }
        /**
         * Сделаем активной кнопку compareProductBtn 
         * @param {Number} productId
         */
        , activeCompareProductBtn: function (productId) {
            var title = this.resources.ProductsRes.RemoveProductFromComparisionLink;
            $("#compareProductBtn" + productId)
                .addClass("active")
                .attr("title", title);
            $("#compareProductBtnText" + productId).text(title);
        }
        /**
         * Уберем активность с кнопки compareProductBtn 
         * @param {Number} productId
         */
        , deactiveCompareProductBtn: function (productId) {
            var title = this.resources.ProductsRes.AddProductToComparisionLink;
            $("#compareProductBtn" + productId)
                .removeClass("active")
                .attr("title", title);
            $("#compareProductBtnText" + productId).text(title);
        }
        /**
         * Добавить товар в сравнение
         * @param {Number} productId
         */
        , addProductToComparison: function (productId) {
            var sectionId = $("#productHolder" + productId).data("sectionId");
            var amount = this.countProductsInComparison(sectionId);
            // 
            this.adjustCompareProductsInfo(amount);
            //
            if (this.productInComparisonExists(productId))
                return;
            
            if (amount >= 4) {
                message.warning("Нельзя добавить больше 4 элементов в сравнение.");
                return;
            }
            var productsInComparison = JSON.parse($.cookie("productsInComparison") || "[]");
            productsInComparison.push({ productId: productId, sectionId: sectionId });
            $.cookie("productsInComparison", JSON.stringify(productsInComparison), { path: '/' });
            //
            this.activeCompareProductBtn(productId);
            //
            this.setCompareProductsAmount(++amount);
            //
            this.showOrHideCompareProductsBtn(amount);
        }
        /**
         * Удалить товар из сравнения
         * @param {Number} productId
         */
        , removeProductFromComparison: function (productId) {
            var sectionId = $("#productHolder" + productId).data("sectionId");
            var amount = this.countProductsInComparison(sectionId);
            // 
            this.adjustCompareProductsInfo(amount);
            //
            var productsInComparison = JSON.parse($.cookie("productsInComparison") || "[]");
            for (var i in productsInComparison)
                if (productsInComparison[i].productId == productId)
                    productsInComparison.splice(i, 1);
            $.cookie("productsInComparison", JSON.stringify(productsInComparison), { path: '/' });
            //
            this.deactiveCompareProductBtn(productId);
            //
            this.setCompareProductsAmount(--amount);
            //
            this.showOrHideCompareProductsBtn(amount);
            //
            this.removeProductFromComparisonInComparePage(productId);
        }
        /**
         * Удалить товар из сравнения на странице сравнения товаров
         * @param {Number} productId
         */
        , removeProductFromComparisonInComparePage: function (productId) {
            var compareProductCol = $("#compareProductBtn" + productId).closest(".js-compare-product-col");
            if (compareProductCol.length > 0) {
                compareProductCol.remove();
                $(".js-product-attribute-" + productId).remove();
                if ($(".js-compare-product-col").length == 0)
                    $("#compareProductAttributes").html(this.resources.ProductsRes.CompareProductsListEmpty);
            }
            //
            $(".different").each(function () {
                var different = false;
                var value = undefined;
                var el = $(this);
                el.find(".js-product-attribute span").each(function () {
                    if (value === undefined) {
                        value = this.textContent;
                        return true;
                    }
                    if (value != this.textContent) {
                        different = true;
                        return false;
                    }
                });
                if (!different)
                    el.removeClass("different").addClass("similar");
            });
        }
        /**
         * Проверить есть ли товар в сравнение
         * @param {Number} productId
         * @returns {Boolean}
         */
        , productInComparisonExists: function(productId) {
            var productsInComparison = JSON.parse($.cookie("productsInComparison") || "[]");
            for (var i in productsInComparison)
                if (productsInComparison[i].productId == productId)
                    return true;
            return false;
        }
        /**
         * Посчитаем количество товаров в сравнении для данного раздела
         * @param {Number} sectionId
         */
        , countProductsInComparison: function (sectionId) {
            var count = 0;
            var productsInComparison = JSON.parse($.cookie("productsInComparison") || "[]");
            for (var i in productsInComparison)
                if (productsInComparison[i].sectionId == sectionId)
                    count++;
            return count;
        }
        /**
         * Обработчик события click на кнопке compareProductBtn
         * @param {JQueryEventObject} event
         */
        , compareProductBtnOnClick: function (event) {
            var productId = $(event.currentTarget).closest(".js-product-holder").data("productId");
            if ($(event.currentTarget).is(".active"))
                this.removeProductFromComparison(productId);
            else
                this.addProductToComparison(productId);
        }
        /**
         * Установка событий на элементы для сравнения товаров
         */
        , setCompareProductEvents: function () {
            $(".js-compare-product-btn").on("click", $.proxy(this.compareProductBtnOnClick, this));
        }
        // /Работа с сравнениями товаров
        // Работа с вопросами и ответами
        /**
         * Покажем или скроем список вопросов и ответов
         * @param {DomObject} linkEl
         */
        , showOrHideFaqThemeItem: function (linkEl) {
            $(linkEl).closest(".js-faq-theme-item").find(".js-faq-list").toggle();
        }
        /**
         * Обработчик события click на ссылке faqThemeItemLink
         * @param {JQueryEventObject} event
         */
        , faqThemeItemLinkOnClick: function (event) {
            this.showOrHideFaqThemeItem(event.currentTarget);
        }
        /**
         * Покажем или скроем содержимое вопроса и ответа
         * @param {DomObject} linkEl
         */
        , showOrHideFaqItemContent: function (linkEl) {
            $(linkEl).closest(".js-faq-item").find(".js-faq-item-content").toggle();
        }
        /**
         * Обработчик события click на ссылке faqItemLink
         * @param {JQueryEventObject} event
         */
        , faqItemLinkOnClick: function (event) {
            this.showOrHideFaqItemContent(event.currentTarget);
        }
        /**
         * Установим события на вопросы и ответы
         */
        , setFaqEvents: function () {
            $(".js-faq-theme-item-link").on("click", $.proxy(this.faqThemeItemLinkOnClick, this))
            $(".js-faq-item-link").on("click", $.proxy(this.faqItemLinkOnClick, this))
        }
        // /Работа с вопросами и ответами
    });
    
    return Products;
});