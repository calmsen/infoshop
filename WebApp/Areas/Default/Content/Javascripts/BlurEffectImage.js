﻿/**
 * Модуль содержит свойства и методы по работе с эффктом blur на картинке    
 * @author Ruslan Rakhmankulov
 */
define("Default/BlurEffectImage", ["jquery", "addJqueryPlugin"], function ($, addJqueryPlugin) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function BlurEffectImage() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var doc = $(document);
    var win = $(window);
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(BlurEffectImage.prototype, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            element: null
            , blurEffectImageHolder: null
            , blurEffectContainer: null
            , blurEffectImage: null
            // -- -- --  
            , sliderDefaultCurrentPosition: "50%"
            , elementWidth: 0
            , blurEffectSliderIsGrabbed: false
            , sliderMinLeftPosition: 0
            , sliderMaxLeftPosition: 0
            , sliderCurrentPosition: 0
            , blurEffectContainerMaxWidth: 0            
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(this, this.defaults, options);

            this.blurEffectImageHolder = this.blurEffectImageHolder || $(this.element).closest(".blur-effect-image-holder");
            this.blurEffectContainer = this.blurEffectContainer || this.blurEffectImageHolder.find(".blur-effect-container");
            this.blurEffectImage = this.blurEffectImage || this.blurEffectImageHolder.find(".blur-effect-image");
            this.blurEffectContainer.css("background-image", "url(" + this.blurEffectImage.attr("src") + ")");
            this.elementWidth = this.elementWidth || this.element.outerWidth();
            this.setStates();
            this.setSliderEvents();
        }
        /**
         * Установим события для ползунка
         */
        , setSliderEvents: function () {
            this.blurEffectImageHolder.add(this.blurEffectImageHolder.find("*"))
                .bind("dragover", function (event) {
                    event.preventDefault();
                    return false;
                })
                .bind("drop", function (event) {
                    event.preventDefault();
                    return false;
                })
                .mousedown(function (event) {
                    event.preventDefault();
                    return false;
                });
            //
            this.element.on("mousedown", $.proxy(this.blurEffectSliderOnMouseDown, this));
            doc.on("mouseup", $.proxy(this.documentOnMouseUp, this))
            doc.on("mousemove", $.proxy(this.documentOnMouseMove, this))
            win.on("resize", $.proxy(this.windowOnResize, this))
        }
        /**
         * Обработчик события mousedown на ползунке
         * @param {JQueryEventObject} event
         */
        , blurEffectSliderOnMouseDown: function (event) {
            this.grabSlider();
        }
        /**
         * Обработчик события mouseup на всем документе
         * @param {JQueryEventObject} event
         */
        , documentOnMouseUp: function (event) {
            this.releaseSlider();
        }
        /**
         * Обработчик события mousemove на документе
         * @param {JQueryEventObject} event
         */
        , documentOnMouseMove: function (event) {
            //if (event.which === 1)
            //    this.dragSlider(event.pageX);
            //else 
            //    this.blurEffectSliderIsGrabbed = false;
            this.dragSlider(event.pageX);
        }
        /**
         * Обработчик события resize на окне браузера
         * @param {JQueryEventObject} event
         */
        , windowOnResize: function (event) {
            this.setStates();
        }
        /**
         * Захватываем ползунок
         */
        , grabSlider: function () {
            this.blurEffectSliderIsGrabbed = true;
        }
        /**
         * Отпускаем ползунок
         */
        , releaseSlider: function () {
            this.blurEffectSliderIsGrabbed = false;
        }
        /**
         * Тащим ползунок
         * @param {Number} pageX координата мыши
         */
        , dragSlider: function (pageX) {
            if (!this.blurEffectSliderIsGrabbed)
                return;

            this.sliderCurrentPosition = pageX;

            if (this.sliderCurrentPosition < this.sliderMinLeftPosition)
                this.sliderCurrentPosition = this.sliderMinLeftPosition;
            if (this.sliderCurrentPosition > this.sliderMaxLeftPosition)
                this.sliderCurrentPosition = this.sliderMaxLeftPosition;

            this.element.offset({ left: this.sliderCurrentPosition });
            //
            this.setBlurEffectContainerWidth();
        }
        /**
         * Устанавливаем ширину контейнера с blur эффектом, относительно текущей позици ползунка
         */
        , setBlurEffectContainerWidth: function () {
            var width = this.sliderCurrentPosition - this.sliderMinLeftPosition;

            if (width < 0)
                width = 0;
            if (width > this.blurEffectContainerMaxWidth)
                width = this.blurEffectContainerMaxWidth;

            this.blurEffectContainer.width(width);
        }
        /**
         * Устанавливаем состояния ползунка и других элементов в состояния по умолчанию
         */
        , setStates: function () {
            this.element.css("left", this.sliderDefaultCurrentPosition);
            this.blurEffectContainer.css("width", this.sliderDefaultCurrentPosition);
            this.sliderMinLeftPosition = this.blurEffectImageHolder.offset().left - this.elementWidth / 2;
            this.sliderMaxLeftPosition = this.blurEffectImageHolder.offset().left + this.blurEffectImageHolder.innerWidth() - this.elementWidth / 2;
            this.sliderCurrentPosition = this.element.offset().left;
            this.blurEffectContainerMaxWidth = this.blurEffectImage.width();
        }
    });

    // создаем jquery plugin
    addJqueryPlugin(BlurEffectImage, "blurEffectImage");

    return BlurEffectImage;
});