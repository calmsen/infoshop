﻿using DomainLogic.Interfaces.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Net;
using WebApp.UiHelpers;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Models.Enumerations;
using WebApp.Models.Forms;
using WebApp.Models.Views.Admin;
using WebApp.Models.Views;

namespace WebApp.Areas.Admin.Controllers
{
    public class FilesController : AdminBaseController
    {
        [Inject]
        public IFilesService FilesService { get; set; }
        [Inject]
        public IProductsService ProductsService { get; set; }
        [Inject]
        public ProductUiHelpers ProductUiHelpers { get; set; }
        [Inject]
        public FileUiHelpers FileUiHelpers { get; set; }
        [Inject]
        public SectionUiHelpers SectionUiHelpers { get; set; }
        
        public async Task<ActionResult> Index(long sectionId = 0, long productId = 0)
        {
            if (productId > 0)
            {
                Product product = await ProductsService.GetProductByIdAsync(productId);
                if (product.SectionId != sectionId)
                    sectionId = product.SectionId;
            }

            var files = (productId > 0 ? await FilesService.GetFilesAsync(productId) : new List<DmFile>())
                .MapList<FileView>();
            List<SelectListItem> sectionsForSelectList = await SectionUiHelpers.GetSectionsForSelectListAsync(new SelectListItem { Value = "", Text = "Выберите раздел" });
            List<SelectListItem> productTitlesForSelectList = await ProductUiHelpers.GetProductTitlesForSelectListAsync(sectionId, new SelectListItem { Value = "", Text = "Выберите модель" });
            
            return View(new FilesAdminPage
            {
                Files = files,
                ProductId = productId,
                SectionId = sectionId,
                SectionsForSelectList = sectionsForSelectList,
                ProductTitlesForSelectList = productTitlesForSelectList
            });
        }
        public async Task<ActionResult> EditFile(long id, long productId, string redirectToUrl = null)
        {
            if (string.IsNullOrEmpty(redirectToUrl) && Request.UrlReferrer != null)
                redirectToUrl = Request.UrlReferrer.ToString();

            var form = GetForm<FileForm>() ?? (await FilesService.GetFileByIdAsync(id))?.Map<FileForm>();
            if (form == null) throw new Exception();
            List<SelectListItem> fileFiltersForSelectList = FileUiHelpers.GetFileFiltersForSelectList();
            
            return View(new EditFileAdminPage
            {
                FileForm = form,
                FileFiltersForSelectList = fileFiltersForSelectList,
                ProductId = productId,
                RedirectToUrl = redirectToUrl
            });
        }

        [HttpPost]
        public async Task<ActionResult> EditFile(FileForm form, long productId, string redirectToUrl = null)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Неправильные данные");
                var file = form.Map<DmFile>();
                await FilesService.EditFileAsync(file, productId);
            }
            catch (Exception)
            {
                return PostRedirectGet(form, "EditFile", "Files", new { id = form.Id, productId = productId, redirectToUrl = redirectToUrl });
            }
            if (!string.IsNullOrEmpty(redirectToUrl))
                return Redirect(redirectToUrl);
            else 
                return RedirectToAction("Index", "Files");
        }

        public ActionResult CreateFile(long productId, string redirectToUrl = null)
        {
            if (string.IsNullOrEmpty(redirectToUrl) && Request.UrlReferrer != null)
                redirectToUrl = Request.UrlReferrer.ToString();

            var form = GetForm<FileForm>() ?? new FileForm();
            List<SelectListItem> fileFiltersForSelectList = FileUiHelpers.GetFileFiltersForSelectList();
            if (form.Filters == null || form.Filters.Count == 0)
                form.Filters = new List<FileFilterEnum> { 
                    FileFilterEnum.Filter_1
                };
            
            return View(new CreateFileAdminPage
            {
                FileForm = form,
                FileFiltersForSelectList = fileFiltersForSelectList,
                ProductId = productId,
                RedirectToUrl = redirectToUrl
            });
        }

        [HttpPost]
        public async Task<ActionResult> CreateFile(FileForm form, long productId, string redirectToUrl = null)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Неправильные данные");

                DmFile file = form.Map<DmFile>();
                await FilesService.CreateFileAsync(file, productId);
            }
            catch (Exception)
            {
                return PostRedirectGet(form, "CreateFile", "Files", new { productId = productId, redirectToUrl = redirectToUrl });
            }
            if (!string.IsNullOrEmpty(redirectToUrl))
                return Redirect(redirectToUrl);
            else 
                return RedirectToAction("Index", "Files");
        }

        [HttpPost]
        public async Task<ActionResult> DeleteFile(long id, string redirectToUrl = null)
        {
            DmFile file = await FilesService.GetFileByIdAsync(id);
            await FilesService.DeleteFileAsync(file);
            if (!string.IsNullOrEmpty(redirectToUrl))
                return Redirect(redirectToUrl);
            else 
                return RedirectToAction("Index", "Files");
        }

        public long GetFileSize(string ftpPath)
        {
            FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(new Uri(ftpPath));
            request.Method = WebRequestMethods.Ftp.GetFileSize;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            long size = response.ContentLength;
            response.Close();
            return size;
        }

        [HttpPost]
        public async Task<string> LoadFile(HttpPostedFileBase file, string folder)
        {
            int fileSize = (int)file.InputStream.Length;
            string fileExt = Path.GetExtension(file.FileName);
            string fileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + fileExt;
            await FilesService.LoadFileAsync(file.InputStream, fileName, folder);
            return JsonConvert.SerializeObject(new { fileName, fileSize, fileExt });
        }
    }
}
;