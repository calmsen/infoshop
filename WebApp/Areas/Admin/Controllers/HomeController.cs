﻿using WebApp.Infrastructure;
using WebApp.Infrastructure.Mvc;
using System;
using System.Data;
using System.Web.Mvc;

namespace WebApp.Areas.Admin.Controllers
{
    public class HomeController : AdminBaseController
    {
        //
        // GET: /Admin/Home/

        public ActionResult Index()
        {
            return RedirectToAction("Index", "Products");
        }
        
    }
}
