﻿using AutoMapper;
using DomainLogic.Interfaces.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Net;
using WebApp.UiHelpers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Models.Settings;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Models.Enumerations;
using DomainLogic.Infrastructure.Constants;
using WebApp.Models.Views;
using WebApp.Models.Views.Admin;
using WebApp.Models.Forms;
using WebApp.Models.Views.Default;

namespace WebApp.Areas.Admin.Controllers
{
    public class ProductsController : AdminBaseController
    {
        [Inject]
        public ISectionsService SectionsService { get; set; }
        [Inject]
        public IProductsService ProductsService { get; set; }
        [Inject]
        public IFiltersService FiltersService { get; set; }  
        [Inject]
        public IImagesService ImagesService { get; set; }
        [Inject]
        public IProductSnapshotsService ProductSnapshotsService { get; set; }
        [Inject]
        public ProductUiHelpers ProductUiHelpers { get; set; }
        [Inject]
        public FilterUiHelpers FilterUiHelpers { get; set; }
        [Inject]
        public PartnerUiHelpers PartnerUiHelpers { get; set; }
        [Inject]
        public ProductTypeUiHelpers ProductTypeUiHelpers { get; set; }
        [Inject]
        public ImagesLoaderSettings ImagesLoaderSettings { get; set; }

        #region Products
        public async Task<ActionResult> Index(long sectionId = 0, List<AllowableFilterEnum?> filter = null, bool needShowUnactiveProducts = false, string sort = null, string query = "", int page = 1)
        {
            query = query.Trim(); 
            if (new Regex(@"^\d+$").IsMatch(query))
            {
                // сбросим текущее значение sectionId
                sectionId = 0;
            }

            List<AllowableFilterEnum> processedFilter = new List<AllowableFilterEnum>();
            if (filter != null)
                foreach (AllowableFilterEnum? f in filter)
                    if (f != null)
                        processedFilter.Add((AllowableFilterEnum)f);
            
            // удалим дубликаты
            processedFilter = processedFilter.Distinct().ToList();
            // -- -- --
            var section = sectionId > 0 
                ? (await SectionsService.GetSectionByIdAsync(sectionId))?.Map<SectionView>() 
                : null;
            List<Section> allSections = await SectionsService.GetSectionsAsync();
            var sectionsWC = SectionsService.SectionsToTreeFromList(allSections, 0)
                .MapList<SectionView>();
            // подготовим параметры для SectionsMenu
            var paramsForSectionsMenu = new Dictionary<string, object>();
            AddCommonParams(paramsForSectionsMenu, processedFilter, query);
            var sectionsMenu = new SectionsMenu
            {
                Section = section,
                SectionsWC = sectionsWC,
                Action = "Index",
                Controller = "Products",
                Params = paramsForSectionsMenu
            };
            // подготовим параметры для Pagination            
            var paramsForPagination = new Dictionary<string, object>();
            if (sectionId > 0)
                paramsForPagination.Add("sectionId", sectionId);
            AddCommonParams(paramsForPagination, processedFilter, query);
            // ! при изменении PageItemsAmount нужно изменить redirectToAction в методе Create
            var pagination = new Pagination
            {
                PageItemsAmount = 50,
                CurrentPage = page,
                RouteValues = paramsForPagination
            };
            pagination.RouteValues.Add("controller", "Products");
            pagination.RouteValues.Add("action", "Index");
            // -- -- -- 
            // получим список товаров
            List<ProductView> products = (await ProductsService.GetProductsForAdminAsync(pagination, section?.PathOfSections ?? 0, processedFilter, needShowUnactiveProducts, query, sort))
                .MapList<ProductView>();
            foreach (ProductView p in products)
                foreach(Section s in allSections) 
                    if (p.SectionId == s.Id)
                    {
                        p.Section = s.Map<SectionView>();
                        break;
                    }
            // -- -- --
            var model = new ProductsListAdminPage
            {
                Products = products,
                SectionsMenu = sectionsMenu,
                Pagination = pagination,
                Section = section,
                NoPhoto = ImagesLoaderSettings.NoPhoto,
                Filter = processedFilter,
                NeedShowUnactiveProducts = needShowUnactiveProducts,
                Sort = sort
            };
            return View(model);
        }

        public async Task<ActionResult> Edit(long id, string redirectToUrl = null, long snapshotId = 0, bool toList = false)
        {
            if (string.IsNullOrEmpty(redirectToUrl) && Request.UrlReferrer != null)
                redirectToUrl = Request.UrlReferrer.ToString();

            var form = GetForm<ProductForm>();
            if (form == null)
            {
                var product = (await ProductsService.GetProductByIdAsync(id))?
                    .Map<ProductForm>();
                if (product == null)
                    throw new Exception();
                
                // если смотрим снимок, то перепишем своства товара
                if (snapshotId > 0)
                {
                    await RedefinePropsBySnapshot(form, snapshotId);
                }
                // -- -- --
                if (form.Description == null)
                    form.Description = new DescriptionForm();
                //// добавим пустые значения 
                //for (int i = 0; i < form.Attributes.Count; i++)
                //{
                //    if (form.Attributes[i].Values.Count == 0)
                //    {
                //        form.Attributes[i].Values.Add(new ValueSimpleForm());
                //    }
                //}
                // -- -- --
            }
            
            
            var model = await ProductEditModelFactoryAsync(form, redirectToUrl, toList);
            // получим снимки данного товара
            model.ProductSnapshots = (await ProductSnapshotsService.GetProductSnapshotsAsync(id)).MapList<ProductSnapshotView>();
            // -- -- --
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(ProductForm form, string redirectToUrl = null, bool toList = false)
        {                        
            Product product = null;
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Не правильные данные.");
                product = form.Map<Product>();
                await ProductsService.EditProductAsync(product, (await UserUiHelpers.GetCurrentUserAsync()).Id);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return PostRedirectGet(form, "Edit", "Products", new { id = form.Id, redirectToUrl });
            }
            if (toList)
            {
                return RedirectToAction("Edit", "Products", new { id = form.Id, redirectToUrl, toList });
            }
            else
            {
                if (!string.IsNullOrEmpty(redirectToUrl))
                    return Redirect(redirectToUrl);
                else
                {
                    int page = await ProductsService.GetPageForProductAsync(product);
                    return RedirectToAction("Index", "Products", new { sectionId = product.SectionId, page });
                }
            }
        }

        public async Task<ActionResult> Create(long sectionId = 0, string redirectToUrl = null, bool toList = false)
        {
            if (string.IsNullOrEmpty(redirectToUrl) && Request.UrlReferrer != null)
                redirectToUrl = Request.UrlReferrer.ToString();

            var form = GetForm<ProductForm>() ?? new ProductForm
            {
                SectionId = sectionId
            };

            return View(await ProductEditModelFactoryAsync(form, redirectToUrl, toList));
        }

        [HttpPost]
        public async Task<ActionResult> Create(ProductForm form, string redirectToUrl = null, bool toList = false)
        {
            Product product = null; ;
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Не правильные данные.");
                product = form.Map<Product>();
                await ProductsService.CreateProductAsync(product, (await UserUiHelpers.GetCurrentUserAsync()).Id);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return PostRedirectGet(form, "Create", "Products", new { redirectToUrl });
            }
            if (toList)
            {
                return RedirectToAction("Edit", "Products", new { id = product.Id, redirectToUrl, toList });
            }
            else
            {
                if (!string.IsNullOrEmpty(redirectToUrl))
                    return Redirect(redirectToUrl);
                else
                {
                    int page = await ProductsService.GetPageForProductAsync(product);
                    return RedirectToAction("Index", "Products", new { sectionId = product.SectionId, page });
                }
            }
                
        }

        [HttpPost]
        public async Task<ActionResult> Delete(long id, string redirectToUrl = null)
        {
            await ProductsService.DeleteProductAsync(id);
            if (!string.IsNullOrEmpty(redirectToUrl))
                return Redirect(redirectToUrl);
            else
                return RedirectToAction("Index", "Products");
        }

        public async Task<ActionResult> Copy(long productId) {
            Product product = await ProductsService.CopyProductAsync(productId, (await UserUiHelpers.GetCurrentUserAsync()).Id);
            return RedirectToAction("Edit", "Products", new { id = product.Id });
        }

        public async Task<ActionResult> CopyAttributes(long id, long fromProductId, string redirectToUrl = null)
        {
            await ProductsService.CopyAttributesAsync(id, fromProductId);
            return RedirectToAction("Edit", "Products", new { id, redirectToUrl });
        }

        public async Task<ActionResult> CopyImages(long id, long fromProductId, string redirectToUrl = null)
        {
            Product product = await ProductsService.CopyImagesAsync(id, fromProductId);           

            return RedirectToAction("Edit", "Products", new { id, redirectToUrl });
        }

        public async Task<string> AutocompleteSearchProducts(string term)
        {
            List<Product> products = await ProductsService.SearchProductsAsync(term);
            List<AutocompleteItem> data = ProductUiHelpers.ToAutocompleteData(products);
            return JsonConvert.SerializeObject(data);
        }
        #endregion 

        [HttpPost]
        public async Task<string> LoadSertificate(string url)
        {
            string fileName = Path.GetFileName(url);
            string fileNameWe = Path.GetFileNameWithoutExtension(url);
            await ImagesService.UploadImageAsync(url, fileName, "Sertificates", false);

            return JsonConvert.SerializeObject(new { Sertificate = fileNameWe });
        }

        public long GetFileSize(string ftpPath)
        {
            var request = WebRequest.Create(new Uri(ftpPath));
            request.Method = WebRequestMethods.Ftp.GetFileSize;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            long size = response.ContentLength;
            response.Close();
            return size;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<string> ProductTitlesForSelectList(long? sectionId, bool? secondSelectItem)
        {
            SelectListItem secondSelectItemObj = null;
            if (secondSelectItem == true)
            {
                string anyProduct = ResourceProvider.ReadResource("Resources.ViewsRes.AnyProductSelectListItem");
                secondSelectItemObj = new SelectListItem { Value = "-1", Text = anyProduct };
            }
            var productTitles = await ProductUiHelpers.GetProductTitlesForSelectListAsync(sectionId, new SelectListItem { Value = "", Text = "--" }, secondSelectItemObj);
            return JsonConvert.SerializeObject(productTitles);
        }

        #region private methods
        private void AddCommonParams(Dictionary<string, object> parameters, List<AllowableFilterEnum> filter, string query)
        {
            if (filter != null && filter.Count() > 0)
            {
                for (int i = 0; i < filter.Count(); i++)
                    parameters.Add("filter[" + i + "]", filter[i]);
            }
            if (!string.IsNullOrEmpty(query))
                parameters.Add("query", query);
        }

        private List<long> GetFilterIds(List<FilterGroup> filtersGroups)
        {
            return filtersGroups.SelectMany(x => x.Filters)
                .Select(x => x.Id)
                .ToList();
        }

        private List<long> GetAnotherFilterIds(List<long> formFilterIds, List<long> filterIds)
        {
            return formFilterIds
                .Where(x => !filterIds.Any(y => y == x))
                .Select(x => x)
                .ToList();
        }

        private void AddAnotherFilterGroup(List<long> anotherFilterIds, List<FilterGroup> filtersGroups)
        {
            filtersGroups.Add(new FilterGroup
            {
                Id = long.MaxValue,
                Title = "Не привязанные к шаблону значения",
                Filters = anotherFilterIds.Select(x => new FilterSettings { Id = x }).ToList()
            });
        }

        private void AddUnfilledFilters(ProductForm form, Dictionary<long, FilterForSelectList> filtersForSelectLists)
        {
            // заполним характеристики, которые не были указаны в нашей форме
            foreach (KeyValuePair<long, FilterForSelectList> kv in filtersForSelectLists)
            {
                if (form.Attributes.Any(x => x.Filter.Id == kv.Key))
                    continue;
                form.Attributes.Add(new AttributeSimpleForm
                {
                    Filter = new FilterSimpleForm {
                        Id = kv.Key,
                    }
                });
            }

        }

        private async Task RedefinePropsBySnapshot(ProductForm form, long snapshotId)
        {
            // очистим список с атрибутами
            form.Attributes.Clear();
            // -- -- --
            ProductSnapshot productSnapshot = await ProductSnapshotsService.GetProductSnapshotAsync(snapshotId);
            ProductInfoForSnapshot snapshot = JsonConvert.DeserializeObject<ProductInfoForSnapshot>(productSnapshot.Snapshot);

            List<long> filterIds = snapshot.Attributes.Select(x => x.FilterId).ToList();
            var snapshotFilterSimpleForms = (await FiltersService.GetFiltersByIdsAsync(filterIds))
                .MapList<FilterSimpleForm>();
            // удалим атрубуты для которых нет фильтров
            for (int i = 0; i < snapshot.Attributes.Count(); i++)
            {
                if (!snapshotFilterSimpleForms.Any(x => x.Id == snapshot.Attributes[i].FilterId))
                {
                    snapshot.Attributes.RemoveAt(i);
                    i--;
                }
            }
            TLS.Set(TLSKeys.SnapshotFilterSimpleFormsForMapper, snapshotFilterSimpleForms);
            Mapper.Map(snapshot, form);
            TLS.Set(TLSKeys.SnapshotFilterSimpleFormsForMapper, null);
        }

        private async Task<ProductEditAdminPage> ProductEditModelFactoryAsync(ProductForm form, string redirectToUrl, bool toList)
        {
            List<Section> allSections = await SectionsService.GetSectionsAsync();
            List<List<SectionView>> sectionsAsPathSiblings = SectionsService.GetSectionsAsPathSiblingsFromList(allSections, form.SectionId)
                .MapList<List<SectionView>>();
            Section activeSection = SectionsService.GetSectionByIdFromList(allSections, form.SectionId);
            List<FilterGroup> filtersGroups = activeSection?.FiltersGroups ?? new List<FilterGroup>();
            List<long> filterIds = GetFilterIds(filtersGroups);
            List<long> anotherFilterIds = GetAnotherFilterIds(form.Attributes.Select(x => x.Filter.Id).ToList(), filterIds);
            filterIds.AddRange(anotherFilterIds);
            List<DmFilter> filters = await FiltersService.GetFiltersByIdsAsync(filterIds);
            AddAnotherFilterGroup(anotherFilterIds, filtersGroups);
            Dictionary<long, FilterForSelectList> filtersForSelectLists = FilterUiHelpers.FiltersForSelectListsAsMap(filters, filtersGroups);
            // возмем данные для представления фильтров и заполним характеристики, которые не были указаны в нашей форме
            AddUnfilledFilters(form, filtersForSelectLists);
            //
            List<SelectListItem> partnersForSelectList = await PartnerUiHelpers.GetPartnersForSelectListAsync(new SelectListItem { Value = "0", Text = "Выберите партнера" });
            // получим данные для выбора изделия
            List<SelectListItem> typesForSelectList = await ProductTypeUiHelpers.GetProductTypesForSelectListAsync(new SelectListItem { Value = "", Text = "Выберите изделие" });

            return new ProductEditAdminPage
            {
                ProductForm = form,
                Sections = allSections.MapList<SectionView>(),
                SectionsAsPathSiblings = sectionsAsPathSiblings,
                FiltersForSelectLists = filtersForSelectLists,
                TypesForSelectList = typesForSelectList,
                FiltersGroups = filtersGroups.MapList<FilterGroupView>(),
                RedirectToUrl = redirectToUrl,
                PartnersForSelectList = partnersForSelectList,
                NoPhoto = ImagesLoaderSettings.NoPhoto,
                ToList = toList
            };
        }
        #endregion
    }
}
;