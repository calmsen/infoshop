﻿using DomainLogic.Interfaces.Services;
using WebApp.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Extensions;
using WebApp.Models.Views.Admin;
using WebApp.Models.Views;

namespace WebApp.Areas.Admin.Controllers
{
    public class CountersController : AdminBaseController
    {
        [Inject]
        public IRequestCounterService RequestCounterService { get; set; }
        public async Task<ActionResult> Index(int page = 1)
        {
            var pagination = new Pagination
            {
                PageItemsAmount = 50,
                CurrentPage = page
            };
            pagination.RouteValues.Add("controller", "Counters");
            pagination.RouteValues.Add("action", "Index");
            //
            List<RequestCounter> list = await RequestCounterService.GetRequestCounterListAsync(pagination);
            return View(new RequestCounterAdminPage
            {
                RequestCounterList = list.MapList<RequestCounterView>(),
                Pagination = pagination
            });
        }

    }
}
