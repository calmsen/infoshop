﻿using DomainLogic.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WebApp.UiHelpers;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Models.Enumerations;
using WebApp.Models.Forms;
using WebApp.Models.Views.Admin;
using WebApp.Models.Views;

namespace WebApp.Areas.Admin.Controllers
{
    public class SrvsController : AdminBaseController
    {
        [Inject]
        public ISrvsService SrvsService { get; set; }
        [Inject]
        public SrvUiHelpers SrvUiHelpers { get; set; }

        public async Task<ActionResult> Index(string city, ServiceTypeEnum type = ServiceTypeEnum.Service1)
        {
            List<string> citiesForSelectList = await SrvsService.GetCitiesForSelectListAsync();

            if (city == null && citiesForSelectList?.Count > 0)
                city = citiesForSelectList[0].Substring(0, citiesForSelectList[0].IndexOf("<!>"));

            var services = (await SrvsService.GetServicesAsync(city, type))
                .MapList<ServiceView>();

            var model = new ServicesListAdminPage
            {
                Services = services,
                CitiesForSelectList = citiesForSelectList,
                ActiveCity = city,
                ActiveType = type
            };
            return View(model);
        }

        public async Task<ActionResult> Edit(long id, string redirectToUrl = null)
        {
            if (string.IsNullOrEmpty(redirectToUrl) && Request.UrlReferrer != null)
                redirectToUrl = Request.UrlReferrer.ToString();

            var form = GetForm<ServiceForm>() ?? (await SrvsService.GetServiceAsync(id))?.Map<ServiceForm>();
            if (form == null) throw new Exception();
            List<SelectListItem> ServiceTypes = SrvUiHelpers.GetServiceTypesForSelectList(null);
            var model = new ServiceEditAdminPage
            {
                ServiceForm = form,
                ServiceTypes = ServiceTypes,
                RedirectToUrl = redirectToUrl
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(ServiceForm form, string redirectToUrl = null)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Неправильные данные");

                var service = form.Map< Service>();
                await SrvsService.EditServiceAsync(service);
            }
            catch (Exception)
            {
                return PostRedirectGet(form, "Edit", "Srvs", new { id = form.Id, redirectToUrl });
            }

            if (!string.IsNullOrEmpty(redirectToUrl))
                return Redirect(redirectToUrl);
            else
                return RedirectToAction("Index", "Srvs");
        }

        public ActionResult Create(string redirectToUrl = null)
        {
            if (string.IsNullOrEmpty(redirectToUrl) && Request.UrlReferrer != null)
                redirectToUrl = Request.UrlReferrer.ToString();

            ServiceForm form = GetForm<ServiceForm>() ?? new ServiceForm();

            List<SelectListItem> ServiceTypes = SrvUiHelpers.GetServiceTypesForSelectList(null);
            var model = new ServiceCreateAdminPage
            {
                ServiceForm = form,
                ServiceTypes = ServiceTypes,
                RedirectToUrl = redirectToUrl
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(ServiceForm form, string redirectToUrl = null)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Неправильные данные");

                var service = form.Map<Service>();
                await SrvsService.CreateServiceAsync(service);
            }
            catch 
            {
                return PostRedirectGet(form, "Create", "Srvs", new { redirectToUrl });
            }

            if (!string.IsNullOrEmpty(redirectToUrl))
                return Redirect(redirectToUrl);
            else
                return RedirectToAction("Index", "Srvs");
        }

        [HttpPost]
        public async Task<ActionResult> Delete(long id, string redirectToUrl = null)
        {
            await SrvsService.DeleteServiceAsync(new Service
            {
                Id = id
            });
            if (!string.IsNullOrEmpty(redirectToUrl))
                return Redirect(redirectToUrl);
            else
                return RedirectToAction("Index", "Srvs");
        }

        public async Task ServiceFillTable(ServiceTypeEnum type)
        {
            await SrvsService.ServiceFillTableAsync(type);
        }

        [HttpPost]
        public async Task AddServices(string fileName, ServiceTypeEnum type) 
        {
            await SrvsService.CreateServicesFromFileAsync(fileName, type);
        }
    }
}
