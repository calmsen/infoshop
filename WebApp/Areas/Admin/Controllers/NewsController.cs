﻿using DomainLogic.Interfaces.Services;
using WebApp.UiHelpers;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Extensions;
using WebApp.Models.Forms;
using WebApp.Models.Views.Admin;
using WebApp.Models.Views;

namespace WebApp.Areas.Admin.Controllers
{
    public class NewsController : AdminBaseController
    {
        [Inject]
        public INewsService NewsService { get; set; }
        [Inject]
        public IProductsService ProductsService { get; set; }
        [Inject]
        public ProductUiHelpers ProductUiHelpers { get; set; }
        [Inject]
        public SectionUiHelpers SectionUiHelpers { get; set; }

        public async Task<ActionResult> Index(int page = 1)
        {
            var pagination = new Pagination
            {
                CurrentPage = page,
                PageItemsAmount = 50
            };
            pagination.RouteValues.Add("controller", "News");
            pagination.RouteValues.Add("action", "Index");
            //
            List<Article> news = await NewsService.GetNewsAsync(pagination, null, new Sort { ColumnName = "Position" });

            var model = new NewsListAdminPage
            {
                News = news.MapList<ArticleView>(),
                Pagination = pagination
            };
            return View(model);
        }

        public async Task<ActionResult> Edit(long id)
        {
            var form = GetForm<ArticleForm>();
            if (form == null)
            {
                var article = (await NewsService.GetArticleAsync(id))?
                    .Map<ArticleForm>();
                if (article == null)
                    return new HttpNotFoundResult();
                form = article;
                if (form.ProductIds.Count > 0)
                {
                    form.SectionId = (await ProductsService.GetProductByIdAsync(form.ProductIds[0].Value)).SectionId;
                }
            }
            List<SelectListItem> sectionsForSelectList = await SectionUiHelpers.GetSectionsForSelectListAsync(new SelectListItem { Value = "", Text = "--" });
            List<SelectListItem> productTitlesForSelectList;
            if (form.SectionId > 0)
                productTitlesForSelectList = await ProductUiHelpers.GetProductTitlesForSelectListAsync((long)form.SectionId, new SelectListItem { Value = "", Text = "--" });
            else
                productTitlesForSelectList = new List<SelectListItem> { new SelectListItem { Value = "", Text = "--" } };

            var model = new ArticleEditAdminPage
            {
                ArticleForm = form,
                SectionsForSelectList = sectionsForSelectList,
                ProductTitlesForSelectList = productTitlesForSelectList
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(ArticleForm form)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Не правильные данные.");

                var article = form.Map<Article>();
                await NewsService.EditArticleAsync(article);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return PostRedirectGet(form, "Edit", "News", new { id = form.Id });
            }

            return RedirectToAction("Index", "News");
        }

        public async Task<ActionResult> Create()
        {
            var form = GetForm<ArticleForm>() ?? new ArticleForm
            {
                Description = new ArticleDescriptionForm()
            };
            List<SelectListItem> sectionsForSelectList = await SectionUiHelpers.GetSectionsForSelectListAsync(new SelectListItem { Value = "", Text = "--" });
            List<SelectListItem> productTitlesForSelectList;
            if (form.SectionId > 0)
                productTitlesForSelectList = await ProductUiHelpers.GetProductTitlesForSelectListAsync(form.SectionId, new SelectListItem { Value = "", Text = "--" });
            else
                productTitlesForSelectList = new List<SelectListItem> { new SelectListItem { Value = "", Text = "--" } };

            var model = new ArticleCreateAdminPage
            {
                ArticleForm = form,
                SectionsForSelectList = sectionsForSelectList,
                ProductTitlesForSelectList = productTitlesForSelectList
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(ArticleForm form)
        {
            
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Не правильные данные.");

                var article = form.Map<Article>();
                await NewsService.CreateArticleAsync(article);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return PostRedirectGet(form, "Create", "News");
            }

            return RedirectToAction("Index", "News");
        }

        [HttpPost]
        public async Task<ActionResult> Delete(long id)
        {
            await NewsService.DeleteArticleAsync(new Article
            {
                Id = id
            });
            return RedirectToAction("Index", "News");
        }
    }
}
