﻿using DomainLogic.Interfaces.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Linq;
using System;
using System.IO;
using System.Threading.Tasks;
using DomainLogic.Infrastructure;
using DomainLogic.Models;
using DomainLogic.Services.Shared.Helpers;
using DomainLogic.Models.Settings;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Credentials;
using DomainLogic.Interfaces.ImageInterfaces;

namespace WebApp.Areas.Admin.Controllers
{
    public class ImagesController : AdminBaseController
    {
        [Inject]
        public IImagesService Ims { get; set; }
        [Inject]
        public ProductsSettings ProductsSettings { get; set; }

        [Inject]
        public ImagesHelper ImagesHelper { get; set; }

        public async Task<ActionResult> ImageManagerSearch(string type, string query = "", int page = 1) 
        {
            ViewBag.Folder = type;
            ViewBag.Query = query;
            ViewBag.CurrentPage = page;
            return View(await Ims.SearchAsync(type, query, page));
        }

        [HttpPost]
        public async Task<ActionResult> ImageManagerUpload(HttpPostedFileBase file, string type)
        {
            if (file != null)
            {
                CheckFile(file);
                IImage image = (IImage)(await Ims.UploadImageAsync(type, file.InputStream));
                image.Title = Path.GetFileNameWithoutExtension(file.FileName);
                await Ims.UpdateImageAsync(type, image);
            }
            return RedirectToAction("ImageManagerSearch", "Images", new { type = type });
        }

        public async Task<string> Edit(long id, string type)
        {
            return JsonConvert.SerializeObject(await Ims.GetImageByIdAsync(type, id));
        }

        [HttpPost]
        public async Task<string> Edit(string type) 
        {
            IImage image = (IImage)Activator.CreateInstance(ImagesHelper.GetType(type));
            image.Id = Convert.ToInt64(Request.Form["Id"]);
            image.Title = Request.Form["Title"];
            image.ExternalLink = Request.Form["ExternalLink"];
            image.Width = Convert.ToInt32(Request.Form["Width"]);
            image.Height = Convert.ToInt32(Request.Form["Height"]);
            await Ims.UpdateImageAsync(type, image);
            return "{}";
        }

        [HttpPost]
        public async Task<string> LoadImage(HttpPostedFileBase file, string type)
        {
            CheckFile(file);

            IImage image = (IImage) (await Ims.UploadImageAsync(type, file.InputStream));
            image.Title = Path.GetFileNameWithoutExtension(file.FileName);
            await Ims.UpdateImageAsync(type, image);

            return JsonConvert.SerializeObject(image);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<string> LoadImages(HttpPostedFileBase[] file, string type)
        {
            foreach(HttpPostedFileBase f in file)
                CheckFile(f);

            IList<object> images = await Ims.UploadImagesAsync(type, file.Select(x => x.InputStream).ToArray());
            for (int i = 0; i < images.Count;  i++)
            {
                ((IImage)images[i]).Title = Path.GetFileNameWithoutExtension(file[i].FileName);
                 await Ims.UpdateImageAsync(type, images[i]);
            }
            return JsonConvert.SerializeObject(images);
        }

        [HttpPost]
        public async Task<string> LoadImageFromUrlOrFtp(string url, string type)
        {
            object image = await Ims.UploadImageAsync(type, url);
            return JsonConvert.SerializeObject(image);
        }

        [HttpPost]
        public async Task<string> LoadImageFromUrl(string url, string type)
        {
            object image = await Ims.UploadImageAsync(type, (HttpCredential)null, url);
            return JsonConvert.SerializeObject(image);
        }

        [HttpPost]
        public async Task<string> LoadImagesFromUrl(string url, string type)
        {
            IList<object> images = await Ims.UploadImagesAsync(type, new HttpCredential { }, url);
            return JsonConvert.SerializeObject(images);
        }

        [HttpPost]
        public async Task<string> LoadImageFromFtp(string ftpPath, string type)
        {
            object image = await Ims.UploadImageAsync(type, (FtpCredential)null, ftpPath);
            return JsonConvert.SerializeObject(image);
        }

        [HttpPost]
        public async Task<string> LoadImagesFromFtp(string ftpPath, string type)
        {
            IList<object> images = await Ims.UploadImagesAsync(type, new FtpCredential(ProductsSettings.Images.Ftp.Credentials.User, ProductsSettings.Images.Ftp.Credentials.Password), ftpPath);
            return JsonConvert.SerializeObject(images);
        }

        [HttpPost]
        public string ReloadImage(HttpPostedFileBase file, long id, string type, int dimension)
        {
            CheckFile(file);

            Ims.ReloadImage(type, file.InputStream, id, dimension);
            return "{}";
        }
                
        private void CheckFile(HttpPostedFileBase file) 
        { 
            if (!new string[] { "image/jpeg", "image/pjpeg", "image/png", "image/gif", "image/bmp" }.Any(x => x == file.ContentType.ToLower()))
                throw new Exception("Файл не является изображением.");
        }
    }
}
