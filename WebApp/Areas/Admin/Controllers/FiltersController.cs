﻿using DomainLogic.Interfaces.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebApp.UiHelpers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Extensions;
using WebApp.Models.Views;
using WebApp.Models.Views.Admin;
using WebApp.Models.Views.Default;
using WebApp.Models.Forms;

namespace WebApp.Areas.Admin.Controllers
{
    public class FiltersController : AdminBaseController
    {
        [Inject]
        public ISectionsService SectionsService { get; set; }
        [Inject]
        public IFiltersService FiltersService { get; set; }
        [Inject]
        public FilterUiHelpers FilterUiHelpers { get; set; }

        public async Task<ActionResult> Index(long sectionId = 0, string query = "", int page = 1)
        {
            query = query.Trim();
            var filters = new List<FilterView>();
            if (new Regex(@"^\d+$").IsMatch(query))
            {
                // сбросим текущее значение sectionId
                sectionId = 0;
                // найдем фильтр по Ид
                long filterId = Convert.ToInt64(query);
                var filter = (await FiltersService.GetFilterByIdAsync(filterId))?.Map<FilterView>();
                if (filter != null)
                {
                    filters.Add(filter);
                }
            }
            //
            var parameters = new Dictionary<string, object>();
            if (sectionId > 0)
                parameters.Add("sectionId", sectionId);

            var pagination = new Pagination
            {
                CurrentPage = page,
                PageItemsAmount = 50,
                RouteValues = parameters
            };
            pagination.RouteValues.Add("controller", "Filters");
            pagination.RouteValues.Add("action", "Index");
            //
            SectionView section = null;
            if (sectionId > 0)
                section = (await SectionsService.GetSectionByIdAsync(sectionId))?.Map<SectionView>();
            List<Section> allSections = await SectionsService.GetSectionsAsync();            
            var sectionsWC = SectionsService.SectionsToTreeFromList(allSections, 0)
                .MapList<SectionView>();
            // подготовим параметры для SectionsMenu
            var paramsForSectionsMenu = new Dictionary<string, object>();
            if (!string.IsNullOrEmpty(query))
                paramsForSectionsMenu.Add("query", query);
            // -- -- --
            var sectionsMenu = new SectionsMenu
            {
                Section = section,
                SectionsWC = sectionsWC,
                Action = "Index",
                Controller = "Filters",
                Params = paramsForSectionsMenu
            };
            if (filters.Count == 0)
            {
                filters = (await FiltersService.GetFiltersAsync(pagination, sectionId > 0 ? section.PathOfSections : 0, query))
                    .MapList<FilterView>();
            }
            // установим соответсвие фильтров и разделов
            await SetFilterSectionsAsync(filters);

            var model = new FiltersListAdminPage
            {
                Filters = filters,
                Pagination = pagination,
                SectionsMenu = sectionsMenu,
                Section = section,
                RedirectToSectionId = section != null ? section.Id : 0
            };
            return View(model);
        }

        public async Task<string> AutocompleteSearchFilters(string term)
        {
            var filters = (await FiltersService.SearchFiltersAsync(term)).MapList<FilterView>();
            // установим соответсвие фильтров и разделов
            await SetFilterSectionsAsync(filters);
            List<AutocompleteItem> data = FilterUiHelpers.ToAutocompleteData(filters);
            return JsonConvert.SerializeObject(data);
        }

        public async Task<ActionResult> Edit(long id, string redirectToUrl = null)
        {
            if (string.IsNullOrEmpty(redirectToUrl) && Request.UrlReferrer != null)
                redirectToUrl = Request.UrlReferrer.ToString();

            var form = GetForm<FilterForm>();
            List<SectionTitle> sections = null;
            if (form == null)
            {
                form = (await FiltersService.GetFilterByIdAsync(id))?.Map<FilterForm>();
                if (form == null) throw new Exception();
                sections = await SectionsService.GetSectionTitlesAsync(new List<long> { form.Id });
            }
            List<SelectListItem> filtersTypes = FilterUiHelpers.GetFiltersTypesForSelectList();
            var model = new FilterEditAdminPage
            {
                FilterForm = form,
                FiltersTypes = filtersTypes,
                Sections = sections,
                RedirectToUrl = redirectToUrl
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(FilterForm form, string redirectToUrl = null)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Не удалось сохранить модфицировать фильтр.");

                var filter = form.Map<DmFilter>();
                await FiltersService.EditFilterAsync(filter);
            }
            catch (Exception)
            {
                return PostRedirectGet(form, "Edit", "Filters", new { id = form.Id, redirectToUrl = redirectToUrl });
            }
            if (!string.IsNullOrEmpty(redirectToUrl))
                return Redirect(redirectToUrl);
            else 
                return RedirectToAction("Index", "Filters");
        }

        public ActionResult Create(string redirectToUrl = null)
        {
            if (string.IsNullOrEmpty(redirectToUrl) && Request.UrlReferrer != null)
                redirectToUrl = Request.UrlReferrer.ToString();

            var form = GetForm<FilterForm>() ?? new FilterForm();

            List<SelectListItem> filtersTypes = FilterUiHelpers.GetFiltersTypesForSelectList();
            var model = new FilterCreateAdminPage
            {
                FilterForm = form,
                FiltersTypes = filtersTypes,
                RedirectToUrl = redirectToUrl
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(FilterForm form, string redirectToUrl = null)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Не удалось добавить фильтр.");

                var filter = form.Map<DmFilter>();
                await FiltersService.CreateFilterAsync(filter);
            }
            catch (Exception)
            {
                return PostRedirectGet(form, "Create", "Filters", new { redirectToUrl = redirectToUrl });
            }

            if (!string.IsNullOrEmpty(redirectToUrl))
                return Redirect(redirectToUrl);
            else
                return RedirectToAction("Index", "Filters");
        }

        [HttpPost]
        public async Task<ActionResult> Delete(long id, string redirectToUrl = null)
        {
            await FiltersService.DeleteFilterAsync(id);
            if (!string.IsNullOrEmpty(redirectToUrl))
                return Redirect(redirectToUrl);
            else
                return RedirectToAction("Index", "Filters");
        }

        public async Task<ActionResult> Copy(long filterId)
        {
            DmFilter filter = await FiltersService.CopyFilterAsync(filterId);
            return RedirectToAction("Edit", "Filters", new { id = filter.Id });
        }

        [HttpPost]
        public async Task MergeBindedValues(long valueId, long mainValueId, bool allowExtendedFunctions, List<long> sectionIds)
        {
            if (allowExtendedFunctions && sectionIds.Count == 0)
                return;
            if (!allowExtendedFunctions && sectionIds.Count > 0)
                sectionIds.Clear();
            await FiltersService.MergeBindedValuesAsync(valueId, mainValueId, sectionIds);
        }

        [HttpPost]
        public async Task DeleteBindedValues(long valueId, bool allowExtendedFunctions, List<long> sectionIds)
        {
            if (allowExtendedFunctions && sectionIds.Count == 0)
                return;
            if (!allowExtendedFunctions && sectionIds.Count > 0)
                sectionIds.Clear();
            await FiltersService.DeleteBindedValuesAsync(valueId, sectionIds);
        }

        [HttpPost]
        public async Task<string> CopyBindedValues(long valueId, bool allowExtendedFunctions, List<long> sectionIds)
        {
            if (allowExtendedFunctions && sectionIds.Count == 0)
                throw new Exception("Укажите разделы для копирования значений.");
            if (!allowExtendedFunctions && sectionIds.Count > 0)
                sectionIds.Clear();

            return JsonConvert.SerializeObject(await FiltersService.CopyBindedValuesAsync(valueId, sectionIds));
        }

        [HttpPost]
        public async Task<string> AddFilterValue(long filterId, string value, string valueIn, double valueAsDouble)
        {
            return JsonConvert.SerializeObject(await FiltersService.GetOrCreateFilterValueAsync(filterId, value, valueIn, valueAsDouble));
        }

        /**
         * For ProductForm
         */
        public async Task<string> ForSelectLists(long sectionId, List<long> formFilterIds)
        {
            Section section = await SectionsService.GetSectionByIdAsync(sectionId);
            var filtersGroups = section?.FiltersGroups ?? new List<FilterGroup>();
            List<long> filterIds = GetFilterIds(filtersGroups);
            List<long> anotherFilterIds = GetAnotherFilterIds(formFilterIds, filterIds);
            filterIds.AddRange(anotherFilterIds);
            List<DmFilter> filters = await FiltersService.GetFiltersByIdsAsync(filterIds);
            AddAnotherFilterGroup(anotherFilterIds, filtersGroups);
            Dictionary<long, FilterForSelectList> filtersForSelectLists = FilterUiHelpers.FiltersForSelectListsAsMap(filters, filtersGroups);

            return JsonConvert.SerializeObject(new
            {
                filters = filtersForSelectLists,
                filtersGroups
            });
        }

        public async Task<bool> ValidateMultiple(bool Multiple, long Id, Action<bool> callback = null)
        {
            bool result;
            try
            {
                result = Multiple || Id == 0 || !(await FiltersService.CheckProductsValuesMultipleAsync(Id));
            }
            catch
            {
                result = false;
            } 
            callback?.Invoke(result);
            return result;
        }
        public TaskCompletionSource<bool> ValidateMultiplePromise { get; set; }

        /**
         * For SectionForm
         */
        public async Task<string> Values(long filterId)
        {
            List<FilterValue> values = await FiltersService.GetFilterValuesAsync(filterId);
            return JsonConvert.SerializeObject(values);
        }

        /**
         * For PaidRepairForm
         */
        [AllowAnonymous]
        [HttpPost]
        public string ValuesForPaidRepair(long sectionId) 
        {
            return JsonConvert.SerializeObject(FilterUiHelpers.GetFilterValuesForSelectListAsync(new SelectListItem { Value = "", Text = "--" }, "Модель", sectionId));
        }
        
        private async Task SetFilterSectionsAsync(List<FilterView> filters)
        {
            List<SectionTitle> sectionTitles = await SectionsService.GetSectionTitlesAsync(filters.Select(x => x.Id).ToList());
            foreach (FilterView f in filters)
            {
                foreach (SectionTitle s in sectionTitles)
                {
                    if (f.Id == s.FilterId)
                        f.Sections.Add(s);
                }
            }
        }

        private List<long> GetFilterIds(List<FilterGroup> filtersGroups)
        {
            return filtersGroups.SelectMany(x => x.Filters)
                .Select(x => x.Id)
                .ToList();
        }

        private List<long> GetAnotherFilterIds(List<long> formFilterIds, List<long> filterIds)
        {
            return formFilterIds
                .Where(x => !filterIds.Any(y => y == x))
                .Select(x => x)
                .ToList();
        }
        private void AddAnotherFilterGroup(List<long> anotherFilterIds, List<FilterGroup> filtersGroups)
        {
            filtersGroups.Add(new FilterGroup
            {
                Id = long.MaxValue,
                Title = "Не привязанные к шаблону значения",
                Filters = anotherFilterIds.Select(x => new FilterSettings { Id = x }).ToList()
            });
        }
    }
    
}
