﻿using DomainLogic.Interfaces.Services;
using WebApp.Models;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainLogic.Infrastructure.Attributes;
using WebApp.Models.Forms;

namespace WebApp.Areas.Admin.Controllers
{
    public class MailingController : AdminBaseController
    {
        [Inject]
        public ICommonService CommonService { get; private set; }

        public ActionResult Index(bool isReady = false)
        {
            ViewBag.IsReady = isReady;
            return View(new MailingForm());
        }

        [HttpPost]
        public async Task<ActionResult> Index(MailingForm form)
        {
            await CommonService.SendMailAsync(form.Subject, form.Body, form.DoTest);
            return RedirectToAction("Index", "Mailing", new { isReady = true });
        }
    }
}
