﻿using AutoMapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DomainLogic.Interfaces.Services;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Extensions;
using WebApp.Models.Forms;
using WebApp.Models.Views;
using WebApp.Models.Views.Admin;

namespace WebApp.Areas.Admin.Controllers
{
    public class PostsController : AdminBaseController
    {
        [Inject]
        public IPostsService PostsService { get; set; }
        [Inject]
        public IProductsService ProductsService { get; set; }

        public async Task<ActionResult> Index(int page = 1)
        {
            var pagination = new Pagination
            {
                CurrentPage = page,
                PageItemsAmount = 50
            };
            pagination.RouteValues.Add("controller", "Posts");
            pagination.RouteValues.Add("action", "Index");
            var posts = (await PostsService.GetPostsAsync(pagination))
                .MapList<PostView>();

            var model = new PostsListAdminPage
            {
                Posts = posts,
                Pagination = pagination
            };
            return View(model);
        }

        public async Task<ActionResult> Edit(long id)
        {
            List<Product> products;
            PostForm form = GetForm<PostForm>();
            if (form == null)
            {
                Post post = await PostsService.GetPostByIdAsync(id);
                if (post == null)
                    throw new Exception();
                form = post.Map<PostForm>();
                products = post.ProductsToPosts.Select(x => x.Product).ToList();
            }
            else
            {
                products = await ProductsService.GetProductsByIdsAsync(form.ProductIds);
            }

            if (form.Description == null)
                form.Description = new PostDescriptionForm();

            var model = new PostEditAdminPage
            {
                PostForm = form,
                Products = products.MapList<ProductView>()
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(PostForm form)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Неправильные данные");

                Post post = await PostsService.GetPostByIdAsync(form.Id);
                Mapper.Map(form, post);
                post.ProductsToPosts = (await ProductsService.GetProductsByIdsAsync(form.ProductIds)).Select(x => new ProductToPost { Product = x }).ToList();
                await PostsService.EditPostAsync(post);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return PostRedirectGet(form, "Edit", "Posts", new { id = form.Id });
            }

            return RedirectToAction("Index", "Posts");
        }

        public async Task<ActionResult> Create()
        {
            List<Product> products;
            PostForm form = GetForm<PostForm>();
            if (form == null)
            {
                products = new List<Product>();
                form = new PostForm();
            }
            else
            {
                products = await ProductsService.GetProductsByIdsAsync(form.ProductIds);
            }
            if (form.Description == null)
                form.Description = new PostDescriptionForm();
            var model = new PostCreateAdminPage
            {
                PostForm = form,
                Products = products.MapList<ProductView>()
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(PostForm form)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Неправильные данные");

                var post = form.Map<Post>();
                post.ProductsToPosts = (await ProductsService.GetProductsByIdsAsync(form.ProductIds)).Select(x => new ProductToPost { Product = x }).ToList();
                await PostsService.CreatePostAsync(post);
            }
            catch (Exception ex)
            {
                Logger.Error("Не удалось создать пост: " + JsonConvert.SerializeObject(form) + "\n" + ex.Message);
                ModelState.AddModelError("", ex.Message);
                return PostRedirectGet(form, "Create", "Posts");
            }

            return RedirectToAction("Index", "Posts");
        }
        public async Task<ActionResult> Delete(long id)
        {
            await PostsService.DeletePostAsync(new Post
            {
                Id = id
            });
            return RedirectToAction("Index", "Posts");
        }
    }
}
