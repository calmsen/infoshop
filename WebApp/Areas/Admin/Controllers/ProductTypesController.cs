﻿using DomainLogic.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Extensions;
using WebApp.Models.Forms;

namespace WebApp.Areas.Admin.Controllers
{
    public class ProductTypesController : AdminBaseController
    {
        [Inject]
        public IProductTypesService ProductTypesService { get; set; }
        [Inject]
        public ISectionsService SectionsService { get; set; }

        public async Task<ActionResult> Index()
        {
            List<ProductType> types = await ProductTypesService.GetTypesAsync();
            return View(types);
        }

        public async Task<ActionResult> Edit(long id)
        {
            var form = GetForm<ProductTypeForm>() ?? (await ProductTypesService.GetTypeByIdAsync(id))?.Map<ProductTypeForm>();
            if (form == null) throw new Exception();
            return View(form);
        }
        [HttpPost]
        public async Task<ActionResult> Edit(ProductTypeForm form)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Не правильные данные.");

                var type = form.Map<ProductType>();
                await ProductTypesService.EditTypeAsync(type);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return PostRedirectGet(form, "Edit", "ProductType", new { id = form.Id });
            }

            return RedirectToAction("Index", "ProductTypes");
        }

        public ActionResult Create()
        {
            var form = GetForm<ProductTypeForm>() ?? new ProductTypeForm();
            return View(form);
        }

        [HttpPost]
        public async Task<ActionResult> Create(ProductTypeForm form)
        {
            
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Не правильные данные.");

                var type = form.Map<ProductType>();
                await ProductTypesService.CreateTypeAsync(type);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return PostRedirectGet(form, "Create", "ProductTypes");
            }

            return RedirectToAction("Index", "ProductTypes");
        }

        [HttpPost]
        public async Task<ActionResult> Delete(long id)
        {
            await ProductTypesService.DeleteTypeAsync(new ProductType
            {
                Id = id
            });
            return RedirectToAction("Index", "ProductTypes");
        }
    }
}
