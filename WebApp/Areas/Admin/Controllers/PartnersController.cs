﻿using System;
using System.Web.Mvc;
using DomainLogic.Interfaces.Services;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Extensions;
using WebApp.Models.Forms;
using WebApp.Models.Views.Admin;
using WebApp.Models.Views;

namespace WebApp.Areas.Admin.Controllers
{
    /// <summary>
    /// Контроллер для работы с партнерами
    /// </summary>
    public class PartnersController : AdminBaseController
    {
        /// <summary>
        /// Сервис для работы с партнерами
        /// </summary>
        [Inject]
        public IPartnersService PtS { get; set; }

        /// <summary>
        /// Получает список партнеров для указанной страницы
        /// </summary>
        /// <param name="page">Запрашиваемая страница</param>
        /// <returns></returns>
        public async Task<ActionResult> Index(int page = 1)
        {
            var pagination = new Pagination
            {
                CurrentPage = page,
                PageItemsAmount = 36
            };

            pagination.RouteValues.Add("controller", "Partners");
            pagination.RouteValues.Add("action", "Index");
            var partners = (await PtS.GetPartnersAsync(pagination))
                .MapList<PartnerView>();

            var model = new PartnersListAdminPage
            {
                Partners = partners,
                Pagination = pagination
            };
            return View(model);
        }

        /// <summary>
        /// Получает форму для редактирования партнера
        /// </summary>
        /// <param name="id"></param>
        /// <param name="redirectToUrl"></param>
        /// <returns></returns>
        public async Task<ActionResult> Edit(long id, string redirectToUrl = null)
        {
            if (string.IsNullOrEmpty(redirectToUrl) && Request.UrlReferrer != null)
                redirectToUrl = Request.UrlReferrer.ToString();

            var form = GetForm<PartnerForm>() ?? (await PtS.GetPartnerByIdAsync(id))?.Map<PartnerForm>();
            if (form == null) throw new Exception();
            var model = new PartnerEditAdminPage
            {
                PartnerForm = form,
                RedirectToUrl = redirectToUrl
            };
            return View(model);
        }

        /// <summary>
        /// Обновляет информацию о партнере
        /// </summary>
        /// <param name="form"></param> 
        /// <param name="redirectToUrl"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Edit(PartnerForm form, string redirectToUrl = null)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Неправильные данные");

                var partner = form.Map<Partner>();
                await PtS.EditPartnerAsync(partner);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return PostRedirectGet(form, "Edit", "Partners", new { id = form.Id, redirectToUrl });
            }

            if (!string.IsNullOrEmpty(redirectToUrl))
                return Redirect(redirectToUrl);
            else
                return RedirectToAction("Index", "Partners");
        }

        /// <summary>
        /// Получает форму для создания партнера
        /// </summary>               
        /// <param name="redirectToUrl"></param>
        /// <returns></returns>
        public ActionResult Create(string redirectToUrl = null)
        {
            if (string.IsNullOrEmpty(redirectToUrl) && Request.UrlReferrer != null)
                redirectToUrl = Request.UrlReferrer.ToString();

            var form = GetForm<PartnerForm>() ?? new PartnerForm();

            var model = new PartnerCreateAdminPage
            {
                PartnerForm = form,
                RedirectToUrl = redirectToUrl
            };
            return View(model);
        }

        /// <summary>
        /// Создает партнера
        /// </summary>
        /// <param name="form"></param>
        /// <param name="redirectToUrl"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Create(PartnerForm form, string redirectToUrl = null)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Неправильные данные");

                var partner = form.Map<Partner>();
                await PtS.CreatePartnerAsync(partner);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return PostRedirectGet(form, "Create", "Partners", new { redirectToUrl });
            }

            if (!string.IsNullOrEmpty(redirectToUrl))
                return Redirect(redirectToUrl);
            else
                return RedirectToAction("Index", "Partners");
        }

        /// <summary>
        /// Удаляет партнера
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Delete(long id, string redirectToUrl = null)
        {
            await PtS.DeletePartnerAsync(new Partner
            {
                Id = id
            });
            if (!string.IsNullOrEmpty(redirectToUrl))
                return Redirect(redirectToUrl);
            else
                return RedirectToAction("Index", "Partners");
        }

    }
}
