﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace WebApp.Areas.Admin.Controllers
{
    public class OptionsController : AdminBaseController
    {
        [Inject]
        public IOptionsService OptionsService { get; set; }

        public async Task<ActionResult> Index()
        {
            List<Option> options = await OptionsService.GetOptions();
            options = options.Where(x => !x.IsSystem).ToList();
            return View(options);
        }

        [HttpPost]
        public async Task Update(string name, string value)
        {
            try
            {
                var option = await OptionsService.GetOptionByNameAsync(name);
                option.Value = value;
                await OptionsService.UpdateOptionAsync(option);
            }
            catch (Exception ex)
            {
                Logger.Error("Не удалось обновить опцию настроек. \n" + ex.Message);
                throw ex;
            }
        }
    }
}
