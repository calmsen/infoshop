﻿using DomainLogic.Interfaces.Services;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Extensions;
using WebApp.Models.Forms;
using WebApp.Models.Views.Admin;
using WebApp.Models.Views;

namespace WebApp.Areas.Admin.Controllers
{
    public class SupportArticlesController : AdminBaseController
    {
        [Inject]
        public ISupportArticlesService SupportArticlesService { get; set; }

        public async Task<ActionResult> Index(int page = 1)
        {
            var pagination = new Pagination
            {
                CurrentPage = page
            };
            pagination.RouteValues.Add("controller", "SupportArticles");
            pagination.RouteValues.Add("action", "Index");
            var articles = (await SupportArticlesService.GetArticlesAsync(pagination))
                .MapList<SupportArticleView>();

            var model = new SupportArticlesListAdminPage
            {
                Articles = articles,
                Pagination = pagination
            };
            return View(model);
        }

        public async Task<ActionResult> Edit(long id)
        {
            var form = GetForm<SupportArticleForm>() ?? (await SupportArticlesService.GetArticleAsync(id))?.Map<SupportArticleForm>();
            if (form == null) throw new Exception();
            var model = new SupportArticleEditAdminPage
            {
                ArticleForm = form
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(SupportArticleForm form)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Пришли плохие данные с клиента.");

                var article = form.Map<SupportArticle>();
                await SupportArticlesService.EditArticleAsync(article);
            }
            catch (Exception ex)
            {
                Logger.Error("Не удалось создать новость." + ex.Message);
                ModelState.AddModelError("", ex.Message);
                return PostRedirectGet(form, "Edit", "SupportArticles", new { id = form.Id });
            }

            return RedirectToAction("Index", "SupportArticles");
        }

        public ActionResult Create()
        {
            var form = GetForm<SupportArticleForm>() ?? new SupportArticleForm
            {
                Description = new SupportArticleDescriptionForm()
            };
            var model = new SupportArticleCreateAdminPage
            {
                ArticleForm = form
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(SupportArticleForm form)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Пришли плохие данные с клиента.");

                var article = form.Map<SupportArticle>();
                await SupportArticlesService.CreateArticleAsync(article);
            }
            catch (Exception ex)
            {
                Logger.Error("Не удалось отредактировать новость: " + ex.Message);
                ModelState.AddModelError("", ex.Message);
                return PostRedirectGet(form, "Create", "SupportArticles");
            }

            return RedirectToAction("Index", "SupportArticles");
        }

        [HttpPost]
        public async Task<ActionResult> Delete(long id)
        {
            await SupportArticlesService.DeleteArticleAsync(new SupportArticle
            {
                Id = id
            });
            return RedirectToAction("Index", "SupportArticles");
        }
    }
}
