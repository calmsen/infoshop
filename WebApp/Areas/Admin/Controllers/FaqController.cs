﻿using DomainLogic.Interfaces.Services;
using WebApp.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WebApp.UiHelpers;
using System.Threading.Tasks;
using DomainLogic.Infrastructure;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Models.Enumerations;
using WebApp.Models.Forms;
using WebApp.Models.Views.Admin;

namespace WebApp.Areas.Admin.Controllers
{
    public class FaqController : AdminBaseController
    {

        [Inject]
        public IFaqService FaqService { get; set; }
        [Inject]
        public IFeedbacksService FeedbacksService { get; set; }
        [Inject]
        public IProductsService ProductsService { get; set; }
        [Inject]
        public FeedbackUiHelpers FeedbackUiHelpers { get; set; }
        [Inject]
        public ProductUiHelpers ProductUiHelpers { get; set; }
        [Inject]
        public SectionUiHelpers SectionUiHelpers { get; set; }
        
        public async Task<ActionResult> Index(long? sectionId, long? productId)
        {
            List<Faq> faq = await FaqService.GetFaqAsync(sectionId, productId, FeedbackThemeEnum.None, null);
            List<SelectListItem> sectionsForSelectList = await SectionUiHelpers.GetSectionsForSelectListAsync(new SelectListItem { Value = "", Text = "Выберите раздел" });
            List<SelectListItem> productTitlesForSelectList = await ProductUiHelpers.GetProductTitlesForSelectListAsync(sectionId, new SelectListItem { Value = "", Text = "Выберите модель" });
            var model = new FaqAdminPage
            {
                Faq = faq.MapList<FaqView>(),
                SectionId = sectionId,
                ProductId = productId,
                SectionsForSelectList = sectionsForSelectList,
                ProductTitlesForSelectList = productTitlesForSelectList
            };
            return View(model);
        }

        public async Task<ActionResult> EditFaq(long id, string redirectToUrl = null)
        {
            if (string.IsNullOrEmpty(redirectToUrl) && Request.UrlReferrer != null)
                redirectToUrl = Request.UrlReferrer.ToString();
            FaqForm form = GetForm<FaqForm>();
            if (form == null)
            {
                Faq faq = await FaqService.GetFaqByIdAsync(id);
                if (faq == null)
                    throw new Exception();
                form = faq.Map<FaqForm>();
            }
            List<SelectListItem> themesForSelectList = FeedbackUiHelpers.GetFeedbackThemesForSelectList(new SelectListItem { Value = "", Text = "--" });
            List<SelectListItem> sectionsForSelectList = await SectionUiHelpers.GetSectionsForSelectListAsync(new SelectListItem { Value = "", Text = "--" });
            List<SelectListItem> productTitlesForSelectList;
            if (form.SectionId > 0)
                productTitlesForSelectList = await ProductUiHelpers.GetProductTitlesForSelectListAsync((long)form.SectionId, new SelectListItem { Value = "", Text = "--" });
            else
                productTitlesForSelectList = new List<SelectListItem> { new SelectListItem { Value = "", Text = "--" } };

            var model = new EditFaqAdminPage
            {
                FaqForm = form,
                ThemesForSelectList = themesForSelectList,
                SectionsForSelectList = sectionsForSelectList,
                ProductTitlesForSelectList = productTitlesForSelectList,
                RedirectToUrl = redirectToUrl
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> EditFaq(FaqForm form, string redirectToUrl = null)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Неправильные данные");

                Faq faq = form.Map<Faq>();
                await FaqService.EditFaqAsync(faq);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return PostRedirectGet(form, "EditFaq", "Faq", new { id = form.Id, redirectToUrl = redirectToUrl });
            }
            if (!string.IsNullOrEmpty(redirectToUrl))
                return Redirect(redirectToUrl);
            else
                return RedirectToAction("Index", "Faq");
        }

        public async Task<ActionResult> CreateFaq(long? sectionId, long? productId, long feedbackAnswer = 0, string redirectToUrl = null)
        {
            if (string.IsNullOrEmpty(redirectToUrl) && Request.UrlReferrer != null)
                redirectToUrl = Request.UrlReferrer.ToString();

            FaqForm form = GetForm<FaqForm>();
            if (form == null)
            {
                if (feedbackAnswer > 0)
                {
                    FeedbackAnswer answer = await FeedbacksService.GetFeedbackAnswerAsync(feedbackAnswer);
                    form = answer.Map<FaqForm>();
                }
                else
                {
                    form = new FaqForm
                    {
                        SectionId = sectionId
                    };
                    if (productId != null) 
                    {
                        Product product = await ProductsService.GetProductByIdAsync((long)productId);
                        form.ProductIds.Add(new ProductId {
                            Value = product.Id,
                            Title = product.Title
                        });
                    }
                }
            }
            List<SelectListItem> themesForSelectList = FeedbackUiHelpers.GetFeedbackThemesForSelectList(new SelectListItem { Value = "", Text = "--" });
            List<SelectListItem> sectionsForSelectList = await SectionUiHelpers.GetSectionsForSelectListAsync(new SelectListItem { Value = "", Text = "--" });
            List<SelectListItem> productTitlesForSelectList;
            if (form.SectionId > 0)
                productTitlesForSelectList = await ProductUiHelpers.GetProductTitlesForSelectListAsync(form.SectionId, new SelectListItem { Value = "", Text = "--" });
            else
                productTitlesForSelectList = new List<SelectListItem> { new SelectListItem { Value = "", Text = "--" } };
            var model = new CreateFaqAdminPage
            {
                FaqForm = form,
                ThemesForSelectList = themesForSelectList,
                SectionsForSelectList = sectionsForSelectList,
                ProductTitlesForSelectList = productTitlesForSelectList,
                RedirectToUrl = redirectToUrl
            };
            return View(model);
        }
        [HttpPost]
        public async Task<ActionResult> CreateFaq(FaqForm form, string redirectToUrl = null)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Неправильные данные");

                Faq faq = form.Map<Faq>();
                await FaqService.CreateFaqAsync(faq);

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return PostRedirectGet(form, "CreateFaq", "Faq", new { redirectToUrl = redirectToUrl });
            }
            if (!string.IsNullOrEmpty(redirectToUrl))
                return Redirect(redirectToUrl);
            else 
                return RedirectToAction("Index", "Faq");
        }

        [HttpPost]
        public async Task<ActionResult> DeleteFaq(long id, string redirectToUrl = null)
        {
            await FaqService.DeleteFaqAsync(new Faq
            {
                Id = id
            });
            if (!string.IsNullOrEmpty(redirectToUrl))
                return Redirect(redirectToUrl);
            else 
                return RedirectToAction("Index", "Faq");
        }
    }
}
;