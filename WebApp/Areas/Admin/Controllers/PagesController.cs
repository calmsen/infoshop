﻿using AutoMapper;
using Newtonsoft.Json;
using System;
using System.Web.Mvc;
using DomainLogic.Interfaces.Services;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Extensions;
using WebApp.Models.Forms;
using WebApp.Models.Views.Admin;
using WebApp.Models.Views;

namespace WebApp.Areas.Admin.Controllers
{
    public class PagesController : AdminBaseController
    {

        [Inject]
        public IPagesService PsS { get; set; }

        public async Task<ActionResult> Index(int page = 1)
        {
            var pagination = new Pagination
            {
                CurrentPage = page,
                PageItemsAmount = 50
            };
            pagination.RouteValues.Add("controller", "Posts");
            pagination.RouteValues.Add("action", "Index");
            var pages = (await PsS.GetPagesAsync(pagination))
                .MapList<PageView>();

            return View(new PagesListAdminPage
            {
                Pages = pages,
                Pagination = pagination
            });
        }

        public async Task<ActionResult> Edit(long id)
        {
            var form = GetForm<PageForm>() ?? (await PsS.GetPageByIdAsync(id))?.Map<PageForm>();
            if (form == null) throw new Exception();
            if (form.Description == null)
                form.Description = new PageDescriptionForm();

            return View(new PageEditAdminPage
            {
                PageForm = form
            });
        }

        [HttpPost]
        public async Task<ActionResult> Edit(PageForm form)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Неправильные данные");

                Page page = await PsS.GetPageByIdAsync(form.Id);
                Mapper.Map(form, page);
                await PsS.EditPageAsync(page);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return PostRedirectGet(form, "Edit", "Pages", new { id = form.Id });
            }

            return RedirectToAction("Index", "Pages");
        }
        public ActionResult Create()
        {
            var form = GetForm<PageForm>() ?? new PageForm();
            if (form.Description == null)
                form.Description = new PageDescriptionForm();

            return View(new PageCreateAdminPage
            {
                PageForm = form
            });
        }

        [HttpPost]
        public async Task<ActionResult> Create(PageForm form)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Неправильные данные");

                var page = form.Map<Page>();
                await PsS.CreatePageAsync(page);
            }
            catch (Exception ex)
            {
                Logger.Error("Не удалось создать пост: " + JsonConvert.SerializeObject(form) + "\n" + ex.Message);
                ModelState.AddModelError("", ex.Message);
                return PostRedirectGet(form, "Create", "Pages");
            }

            return RedirectToAction("Index", "Pages");
        }

        [HttpPost]
        public async Task<ActionResult> Delete(long id)
        {
            await PsS.DeletePageAsync(new Page
            {
                Id = id
            });
            return RedirectToAction("Index", "Pages");
        }
    }
}
