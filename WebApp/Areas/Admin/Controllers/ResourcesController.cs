﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DomainLogic.Interfaces.Services;
using System.Globalization;
using System.Reflection;
using Resources;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Extensions;
using WebApp.Infrastructure.Mvc.Utils;
using WebApp.Models.Views.Admin;

namespace WebApp.Areas.Admin.Controllers
{
    public class ResourcesController : AdminBaseController
    {
        /// <summary>
        /// Сервис для работы с ресурсами
        /// </summary>
        [Inject]
        public IResourcesService ResourcesService { get; set; }
        [Inject]
        public ResourceUtils ResourceUtils { get; set; }

        public ActionResult Index(string classKey = "ViewsRes")
        {
            List<SelectListItem> classKeysForSelectList = GetClassKeysForSelectList();
            if (!classKeysForSelectList.Any(x => x.Value == classKey))
                classKey = "ViewsRes";
            string startResourceKey = $"Resources.{classKey}.";
            List<Resource> resources = ResourcesService.GetResources().Where(x => x.Key.StartsWith(startResourceKey)).ToList();
            Dictionary<string, string> resourceDict = ResourceUtils.ReadResources(classKey, new CultureInfo("ru"));
            Dictionary<string, string> resourceDictIn = ResourceUtils.ReadResources(classKey, new CultureInfo("en"));
            foreach (KeyValuePair<string, string> kv in resourceDict)
            {
                string endResourceKey = "." + kv.Key;
                if (!resources.Any(x => x.Key.EndsWith(endResourceKey)))
                {
                    try
                    {
                        resources.Add(new Resource
                        {
                            Key = startResourceKey + kv.Key,
                            Value = kv.Value,
                            ValueIn = resourceDictIn[kv.Key]
                        });
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("ResourcesController.Index: " + ex.Message);
                    }
                }
            }
            return View(new ResourcesListAdminPage
            {
                Resources = resources,
                ClassKey = classKey,
                ClassKeysForSelectList = classKeysForSelectList
            });
        }
        
        [HttpPost]
        public async Task CreateOrUpdate(Resource resource)
        {
            try
            {
                await ResourcesService.CreateOrUpdateResourceAsync(resource);
            }
            catch (Exception ex)
            {
                Logger.Error("Не удалось обновить ресурc: " + JsonConvert.SerializeObject(resource) + "\n" + ex.Message);
                throw ex;
            }
        }

        private List<SelectListItem> GetClassKeysForSelectList()
        {
            return GetTypesInNamespace(typeof(ViewsRes).Assembly, "Resources").Select(x => new SelectListItem {
                Text = x.Name.TrimEnd("Res"),
                Value = x.Name
            }).ToList();
        }

        private Type[] GetTypesInNamespace(Assembly assembly, string nameSpace)
        {
            return assembly.GetTypes().Where(t => String.Equals(t.Namespace, nameSpace, StringComparison.Ordinal)).ToArray();
        }
    }
}
