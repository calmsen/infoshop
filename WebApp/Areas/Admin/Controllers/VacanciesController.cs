﻿using DomainLogic.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WebApp.UiHelpers;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Extensions;
using WebApp.Models.Forms;
using WebApp.Models.Views.Admin;
using WebApp.Models.Views;

namespace WebApp.Areas.Admin.Controllers
{
    public class VacanciesController : AdminBaseController
    {
        [Inject]
        public IVacanciesService VacanciesService { get; set; }
        [Inject]
        public VacancyUiHelpers VacancyUiHelpers { get; set; }

        public async Task<ActionResult> Index()
        {
            List<Vacancy> vacancies = await VacanciesService.GetVacanciesAsync();

            var model = new VacanciesListAdminPage
            {
                Vacancies = vacancies.MapList<VacancyView>()
            };
            return View(model);
        }

        public async Task<ActionResult> Edit(int id)
        {
            var form = GetForm<VacancyForm>() ?? (await VacanciesService.GetVacancyAsync(id))?.Map<VacancyForm>();
            if (form == null) throw new Exception();
            List<SelectListItem> vacancyStates = VacancyUiHelpers.GetVacancyStatesForSelectList();
            var model = new VacancyEditAdminPage
            {
                VacancyForm = form,
                VacancyStates = vacancyStates
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(VacancyForm form)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception();

                var vacancy = form.Map<Vacancy>();
                await VacanciesService.EditVacancyAsync(vacancy);
            }
            catch (Exception)
            {
                return PostRedirectGet(form, "Edit", "Vacancies", new { id = form.Id });
            }

            return RedirectToAction("Index", "Vacancies");
        }
        public ActionResult Create()
        {
            var form = GetForm<VacancyForm>() ?? new VacancyForm();

            List<SelectListItem> vacancyStates = VacancyUiHelpers.GetVacancyStatesForSelectList();
            var model = new VacancyCreateAdminPage
            {
                VacancyForm = form,
                VacancyStates = vacancyStates
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(VacancyForm form)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception();

                var vacancy = form.Map<Vacancy>();
                await VacanciesService.CreateVacancyAsync(vacancy);
            }
            catch (Exception)
            {
                return PostRedirectGet(form, "Create", "Vacancies");
            }

            return RedirectToAction("Index", "Vacancies");
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            await VacanciesService.DeleteVacancyAsync(new Vacancy
            {
                Id = id
            });
            return RedirectToAction("Index", "Vacancies");
        }
    }
}
