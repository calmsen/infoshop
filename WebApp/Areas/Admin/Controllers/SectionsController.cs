﻿using DomainLogic.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using WebApp.UiHelpers;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Extensions;
using WebApp.Models.Forms;
using WebApp.Models.Views;
using WebApp.Models.Views.Admin;

namespace WebApp.Areas.Admin.Controllers
{
    public class SectionsController : AdminBaseController
    {
        [Inject]
        public ISectionsService SectionsService { get; set; }
        [Inject]
        public IProductsService ProductsService { get; set; }
        [Inject]
        public IFiltersService FiltersService { get; set; }
        [Inject]
        public FilterUiHelpers FilterUiHelpers { get; set; }

        public async Task<ActionResult> Index(long sectionId = 0, string query = "", int page = 1)
        {
            query = query.Trim();
            List<Section> allSections = await SectionsService.GetSectionsAsync();
            var sectionsWC = SectionsService.SectionsToTreeFromList(allSections, 0)
                .MapList<SectionView>();
            List<Section> sections = new List<Section>();
            if (!string.IsNullOrEmpty(query))
            {
                // сбросим текущий раздел
                sectionId = 0;

                if (new Regex(@"^\d+$").IsMatch(query))
                {
                    long sId = Convert.ToInt64(query);
                    Section s = allSections.FirstOrDefault(x => x.Id == sId);
                    if (s != null)
                    {
                        SectionsService.MapSectionChildsFromList(allSections, s);
                        sections.Add(s);
                    }
                }
                else
                {
                    foreach (Section s in allSections.Where(x => x.Title.ToLower().Contains(query.ToLower())))
                    {
                        SectionsService.MapSectionChildsFromList(allSections, s);
                        sections.Add(s);
                    }
                }
            }
            else
            {
                sections = SectionsService.SectionsToTreeFromList(allSections, sectionId);
            }

            var section = SectionsService.GetSectionByIdFromList(allSections, sectionId)?
                .Map<SectionView>();
            // подготовим параметры для SectionsMenu
            var paramsForSectionsMenu = new Dictionary<string, object>();
            if (!string.IsNullOrEmpty(query))
                paramsForSectionsMenu.Add("query", query);
            // -- -- --
            var sectionsMenu = new SectionsMenu
            {
                Section = section,
                SectionsWC = sectionsWC,
                Action = "Index",
                Controller = "Sections",
                Params = paramsForSectionsMenu
            };

            //
            var parameters = new Dictionary<string, object>();
            if (sectionId > 0)
                parameters.Add("sectionId", sectionId);
            var pagination = new Pagination
            {
                PageItemsAmount = 30,
                CurrentPage = page,
                RouteValues = parameters
            };
            pagination.RouteValues.Add("controller", "Sections");
            pagination.RouteValues.Add("action", "Index");
            pagination.ItemsAmount = sections
                .Count();
            pagination.Refresh();
            sections = sections
                .OrderBy(x => x.Position)
                .Skip(pagination.Offset)
                .Take(pagination.PageItemsAmount)
                .ToList();

            var model = new SectionsListAdminPage
            {
                SectionsMenu = sectionsMenu,
                Sections = sections.MapList<SectionView>(),
                Pagination = pagination,
                Section = section != null ? section.Map<SectionView>() : null
            };
            return View(model);
        }

        public async Task<ActionResult> Edit(long id, string redirectToUrl = null, bool toList = false)
        {
            if (string.IsNullOrEmpty(redirectToUrl) && Request.UrlReferrer != null)
                redirectToUrl = Request.UrlReferrer.ToString();

            var filters = new List<FilterView>();
            var form = GetForm<SectionForm>();
            if (form == null)
            {
                form = (await SectionsService.GetSectionByIdAsync(id, true))?
                    .Map<SectionForm>();
                if (form == null)
                    throw new Exception();
                filters = (await FiltersService.GetFiltersForSectionAsync(form.Id))
                    .MapList<FilterView>();
            }
            else
            {
                List<long> filtersIds = form.FiltersGroups.SelectMany(x => x.Filters.Select(y => y.Id).ToList()).ToList();
                filters = (await FiltersService.GetFiltersByIdsAsync(filtersIds))
                    .MapList<FilterView>();
            }
            List<Section> allSections = await SectionsService.GetSectionsAsync();
            var sectionsAsPathSiblings = SectionsService.GetSectionsAsPathSiblingsFromList(allSections, id)
                .MapList<List<SectionView>>();
            //
            List<SelectListItem> filtersTypes = FilterUiHelpers.GetFiltersTypesForSelectList();

            var model = new SectionEditAdminPage
            {
                SectionForm = form,
                Sections = allSections.MapList<SectionView>(),
                SectionsAsPathSiblings = sectionsAsPathSiblings,
                Filters = filters,
                RedirectToUrl = redirectToUrl,
                ToList = toList,
                FiltersTypes = filtersTypes
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(SectionForm form, string redirectToUrl = null, bool toList = false)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Неправильные данные.");

                var section = form.Map<Section>();
                await SectionsService.EditSectionAsync(section);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return PostRedirectGet(form, "Edit", "Sections", new { id = form.Id, redirectToUrl });
            }
            if (toList)
            {
                return RedirectToAction("Edit", "Sections", new { id = form.Id, redirectToUrl, toList });
            }
            else
            {
                if (!string.IsNullOrEmpty(redirectToUrl))
                    return Redirect(redirectToUrl);
                else
                    return RedirectToAction("Index", "Sections");
            }            
        }

        public async Task<ActionResult> Create(string redirectToUrl = null, bool toList = false)
        {
            if (string.IsNullOrEmpty(redirectToUrl) && Request.UrlReferrer != null)
                redirectToUrl = Request.UrlReferrer.ToString();

            var filters = new List<FilterView>();
            var form = GetForm<SectionForm>();
            if (form == null)
            {
                form = new SectionForm();
            }
            else
            {
                // удалим пустые фильтры
                for (int i = 0; i < form.FiltersGroups.Count; i++)
                    for (int j = 0; j < form.FiltersGroups[i].Filters.Count; j++)
                        if (form.FiltersGroups[i].Filters[j].Id == 0) 
                        {
                            form.FiltersGroups[i].Filters.RemoveAt(j);
                            j--;
                        }
                filters = (await FiltersService.GetFiltersByIdsAsync(form.FiltersGroups.Select(x => x.Filters.Select(y => y.Id).ToList()).FirstOrDefault()))
                    .MapList<FilterView>();
            }
            List<Section> allSections = await SectionsService.GetSectionsAsync();
            var sectionsAsPathSiblings = SectionsService.GetSectionsAsPathSiblingsFromList(allSections, form.ParentId)
                .MapList<List<SectionView>>();
            //
            List<SelectListItem> filtersTypes = FilterUiHelpers.GetFiltersTypesForSelectList();

            var model = new SectionCreateAdminPage
            {
                SectionForm = form,
                Sections = allSections.MapList<SectionView>(),
                SectionsAsPathSiblings = sectionsAsPathSiblings,
                Filters = filters,
                RedirectToUrl = redirectToUrl,
                ToList = toList,
                FiltersTypes = filtersTypes
            };   
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(SectionForm form, string redirectToUrl = null, bool toList = false)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Неправильные данные.");

                var section = form.Map<Section>();
                await SectionsService.CreateSectionAsync(section);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return PostRedirectGet(form, "Create", "Sections", new { redirectToUrl });
            }

            if (toList)
            {
                return RedirectToAction("Edit", "Sections", new { id = form.Id, redirectToUrl, toList });
            }
            else
            {
                if (!string.IsNullOrEmpty(redirectToUrl))
                    return Redirect(redirectToUrl);
                else
                    return RedirectToAction("Index", "Sections");
            }            
        }

        [HttpPost]
        public async Task<ActionResult> Delete(long id, string redirectToUrl = null)
        {
            await SectionsService.DeleteSectionAsync(id);
            if (!string.IsNullOrEmpty(redirectToUrl))
                return Redirect(redirectToUrl);
            else
                return RedirectToAction("Index", "Sections");
        }
        
        public async Task<ActionResult> UnbindProducts(long id)
        {
            string redirectToUrl = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : null;

            await SectionsService.UnbindProductsAsync(id);

            if (!string.IsNullOrEmpty(redirectToUrl))
                return Redirect(redirectToUrl);
            else
                return RedirectToAction("Index", "Sections");
        }

        public async Task<string> SaveSectionsPositions(List<Section> sections)
        {
            await SectionsService.SaveSectionsPositionsAsync(sections);
            return "Ok";
        }
    }
}
