﻿using WebApp.Models;
using WebApp.UiHelpers;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using DomainLogic.Infrastructure;
using DomainLogic.Models;
using WebApp.Models.Views.Admin;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class UsersController : AdminBaseController
    {

        public async Task<ActionResult> Index (string email = null) 
        {
            if (email == null)
                return View(new UserInfoPage {
                    Email = email
                });

            User user = await UsersService.GetUserByEmailAsync(email);
            if (user == null)
                return View(new UserInfoPage {
                    Email = email
                });
            //
            List<string> userRoles = await UsersService.GetRolesForUserAsync(user.Email);
            //
            List<string> allRoles = UsersService.GetAllRoles();
            List<SelectListItem> rolesForSelectList = UserUiHelpers.GetRolesForSelectList(allRoles);

            return View(new UserInfoPage { 
                Email = email,
                User = user,
                UserRoles = userRoles,
                RolesForSelectList = rolesForSelectList
            });
        }

        [HttpPost]
        public async Task<ActionResult> AddRole(string email, string role)
        {
            await UsersService.AddRoleForUserAsync(email, role);
            return RedirectToAction("Index", "Users", new { email = email });
        }
        
        public async Task<ActionResult> RemoveRole(string email, string role)
        {
            await UsersService.RemoveRoleFromUserAsync(email, role);
            return RedirectToAction("Index", "Users", new { email = email });
        }
    }
}
