﻿/**
 * Модуль содержит свойства и методы по работе с главной картинкой
 * @author Ruslan Rakhmankulov
 */
define("Admin/MainImage", ["jquery", "common", "globalize", "jqueryUi", "jqueryUnobtrusive", "ajaxfileupload", "jqueryTmpl", "text!Admin/CommonTmpl.htm", "jqueryToastmessage"], function ($, common, Globalize) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function MainImage() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(MainImage.prototype, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            baseUrl: common.baseUrl
            , st0SiteHttpHost: common.st0SiteHttpHost
            , culture: common.culture || "ru"
            , denyAjax: false // состояние, которое запрещает повторную отправку запроса аякс
            , folder: ""
            , mainImageId: "MainImageId"
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(this, this.defaults, options);
            Globalize.culture(this.culture);
        }
        /**
         * Обработчик события click на кнопке removeImageBtn
         * @param {JQueryEventObject} event
         */
        , removeImageBtnOnClick: function (event) {
            this.removeImage();
        }
        /**
         * Удаление картинки из dom дерева
         */
        , removeImage: function () {
            $("#" + this.mainImageId + "-wrap .js-mi-image-holder").hide();
            $("#" + this.mainImageId + "-wrap .js-mi-image").removeAttr("src");
            $("#" + this.mainImageId).val("0")
            $("#" + this.mainImageId + "-wrap .js-mi-add-image-holder").show();
            $("form").validate().element("#" + this.mainImageId);
        }
        /**
         * Обработчик события change на элементе file input
         * @param {JQueryEventObject} event
         */
        , fileOnChange: function (event) {
            this.loadImage();
        }
        /**
         * Загрузка картинки на удаленный сервер
         */
        , loadImage: function () {
            $("#" + this.mainImageId + "-wrap .js-mi-add-image-btn").hide();
            $("#" + this.mainImageId + "-wrap .js-mi-file").hide();
            $("#" + this.mainImageId + "-wrap .js-mi-loading").show();

            var obj = this;

            $.ajaxFileUpload({
                url: this.baseUrl + "Admin/Images/LoadImage"
                , data: "type=" + this.folder
                , fileElementId: this.mainImageId + "-file"
                , type: "POST"
                , dataType: "json"
                , success: function (image) {
                    $("#" + obj.mainImageId + "-wrap .js-mi-add-image-holder").hide();
                    $("#" + obj.mainImageId + "-wrap .js-mi-add-image-btn").show();
                    $("#" + obj.mainImageId + "-wrap .js-mi-file").show();
                    $("#" + obj.mainImageId + "-wrap .js-mi-loading").hide();

                    $("#" + obj.mainImageId).val(image.Id);
                    $("#" + obj.mainImageId + "-wrap .js-open-image-settings-btn").data("imageId", image.Id);
                    $("#" + obj.mainImageId + "-wrap .js-mi-image").attr("src", obj.st0SiteHttpHost + "Images/" + obj.folder + "/normal/" + image.Id + ".jpg");
                    $("#" + obj.mainImageId + "-wrap .js-mi-image-holder").show();
                    $("form").validate().element("#" + obj.mainImageId);
                }
                , error: function (xhr, status) {
                    message.error(xhr.responseText);

                    $("#" + obj.mainImageId + "-wrap .js-mi-add-image-btn").show();
                    $("#" + obj.mainImageId + "-wrap .js-mi-file").show();
                    $("#" + obj.mainImageId + "-wrap .js-mi-loading").hide();
                }
            });
        }
        /**
         * Установка событий на картинку
         */
        , setMainImageFormGroupEvents: function () {
            $("#" + this.mainImageId + "-wrap .js-mi-remove-image-btn").on("click", $.proxy(this.removeImageBtnOnClick, this));
            $("#" + this.mainImageId + "-wrap .js-mi-file").on("change", $.proxy(this.fileOnChange, this));
        }
    });

    // проиницилизируем модули
    $("[data-module='mainImage']").each(function () {
        var el = $(this);
        new MainImage({
            folder: el.data("folder")
            , mainImageId: el.data("mainImageId")
        })
        .setMainImageFormGroupEvents();
    });

    return MainImage;
});