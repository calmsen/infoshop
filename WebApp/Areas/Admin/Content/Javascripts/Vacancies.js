﻿/**
 * Модуль содержит свойства и методы по работе с вакансиями
 * @author Ruslan Rakhmankulov
 */
define("Admin/Vacancies", ["jquery", "common", "globalize", "jqueryUnobtrusive", "jqueryToastmessage", "jqueryAlerts"], function ($, common, Globalize) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Vacancies() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(Vacancies.prototype, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            baseUrl: common.baseUrl
            , culture: common.culture || "ru"
            , denyAjax: false // состояние, которое запрещает повторную отправку запроса аякс
            , denySubmit: false // состояние, которое запрещает повторную отправку формы
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(this, this.defaults, options);
            Globalize.culture(this.culture);
            // установка событий на форму
            $("#save-vacancy-btn").on("click", $.proxy(this.saveVacancyBtnOnClick, this));
            $("#delete-vacancy-btn").on("click", $.proxy(this.deleteVacancyBtnOnClick, this));
        }

        /**
         * Обработчик события click на кнопке saveVacancyBtn
         * @param {JQueryEventObject} event
         */
        , saveVacancyBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return false;
            var formIsValid = $("#vacancy-form").valid();
            if (formIsValid) {
                this.denySubmit = true;
                $("#vacancy-form").submit();
            }
            return false;
        }
        /**
         * Обработчик события click на кнопке deleteVacancyBtn
         * @param {JQueryEventObject} event
         */
        , deleteVacancyBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            $.alerts.confirm("Вы действительно хотите удалить вакансию?", $.noop, function (r) {
                if (r) {
                    this.denySubmit = true;
                    var action = $(event.currentTarget).data("href");
                    $(event.currentTarget).closest("form").attr("action", action).submit();
                }
            });
        }
    });
    return Vacancies;
});