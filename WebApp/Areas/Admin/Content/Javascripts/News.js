﻿/**
 * Модуль содержит свойства и методы по работе с новостями
 * @author Ruslan Rakhmankulov
 */
define("Admin/News", ["jquery", "common", "globalize", "tinymce", "SelectControl", "Admin/ImagesV2", "jqueryUnobtrusive", "jqueryTmpl", "text!Admin/CommonTmpl.htm", "jqueryToastmessage", "jqueryAlerts"], function ($, common, Globalize, tinymce, SelectControl) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function News() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(News.prototype, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            baseUrl: common.baseUrl
            , culture: common.culture || "ru"
            , denySubmit: false // состояние, которое запрещает повторную отправку формы
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(this, this.defaults, options);
            Globalize.culture(this.culture);
            // установка событий на форму 
            $("#save-article-btn").on("click", $.proxy(this.saveArticleBtnOnClick, this));
            $("#delete-article-btn").on("click", $.proxy(this.deleteArticleBtnOnClick, this));

            common.tinymce({
                plugin_preview_width: 710,
                folder: "News"
            });

            //
            this.initSectionSelectControl("SectionId", "ProductIds");
            //
            this.setArticleProductBindingEvents();
        }

        /**
         * Обработчик события click на кнопке submit формы
         * @param {JQueryEventObject} event
         */
        , saveArticleBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            var formIsValid = $("#article-form").valid();
            if (formIsValid) {
                this.denySubmit = true;
                $("#article-form").submit();
            }
            return;
        }
        /**
         * Обработчик события click на ссылке deleteArticleBtn
         * @param {JQueryEventObject} event
         */
        , deleteArticleBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            $.alerts.confirm("Вы действительно хотите удалить новость?", $.noop, function (r) {
                if (r) {
                    this.denySubmit = true;
                    var action = $(event.currentTarget).data("href");
                    $(event.currentTarget).closest("form").attr("action", action).submit();
                }
            });
        }

        /**
         * Инициализация селекта раздела
         * @param {Number} sectionId - ид элемента раздела
         * @param {Number} productId - ид элемента товара
         */
        , initSectionSelectControl: function (sectionId, productId) {
            new SelectControl({
                element: $("#" + sectionId)
                , baseUrl: this.baseUrl
                , dependentSelectElem: $("#" + productId)
                , dependentSelectUri: "Admin/Products/ProductTitlesForSelectList"
                , dependentSelectParam: "sectionId"
            });
        }
        // --
        // Работа с привязками к товарам
        // --
        /**
         * Устанавливает события на элементы для привязки товаров
         */
        , setArticleProductBindingEvents: function () {
            $("#ProductIds").on("change", $.proxy(this.productIdsSelectOnChange, this));
            $("#productsHolder").on("click", ".js-remove-product-holder-btn", $.proxy(this.removeProductHolderBtnOnClick, this));
        }
        /**
         * Обработчик события change на селекте ProductIds
         * @param {JQueryEventObject} event
         */
        , productIdsSelectOnChange: function (event) {
            var selectEl = $(event.currentTarget);
            var productId = selectEl.val();
            if (productId === "")
                return;
            if ($("#productHolder" + productId).length > 0)
                return;
            var productTitle = selectEl.find("option[value=" + productId + "]").text();
            this.addProductHolderElement(productId, productTitle, $(".js-product-holder").length);
        }

        /**
         * Обработчик события click на кнопке removeProductHolderBtn
         * @param {JQueryEventObject} event
         */
        , removeProductHolderBtnOnClick: function (event) {
            var productId = $(event.currentTarget).closest(".js-product-holder").data("productId");
            this.removeProductHolderElement(productId);
        }
        /**
         * Добавляет элемент productHolder в dom дерево
         * @param {Number} productId идентификатор товара
         * @param {String} productTitle название товара
         * @param {Number} index порядковый номер элемента productHolder
         */
        , addProductHolderElement: function (productId, productTitle, index) {
            $("#articleProductHolderTmpl").tmpl({
                productId: productId
                , productTitle: productTitle
                , index: index
            })
            .appendTo("#productsHolder");
        }
        /**
         * Удаляет элемент productHolder из dom дерева
         */
        , removeProductHolderElement: function (productId) {
            $("#productHolder" + productId).remove();
            this.changeProductHolderPositions();
        }
        /**
         * Изменяет порядковые номера элементов productHolder  
         */
        , changeProductHolderPositions: function () {
            $(".js-product-holder").each(function (i) {
                $(this)
                    .find(".js-product-id")
                        .attr("id", "ProductIds_" + i + "__Value")
                        .attr("name", "ProductIds[" + i + "].Value")
                .end()
                .find(".js-product-title")
                        .attr("id", "ProductIds_" + i + "__Title")
                        .attr("name", "ProductIds[" + i + "].Title");
            });
        }
    });
    return News;
});