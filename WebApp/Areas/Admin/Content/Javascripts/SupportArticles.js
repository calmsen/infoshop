﻿/**
 * Модуль содержит свойства и методы по работе с лентой обновлений
 * @author Ruslan Rakhmankulov
 */
define("Admin/SupportArticles", ["jquery", "common", "globalize", "tinymce", "jqueryUnobtrusive", "jqueryToastmessage", "jqueryAlerts"], function ($, common, Globalize, tinymce) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function SupportArticles() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(SupportArticles.prototype, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            baseUrl: common.baseUrl
            , culture: common.culture || "ru"
            , denyAjax: false // состояние, которое запрещает повторную отправку запроса аякс
            , denySubmit: false // состояние, которое запрещает повторную отправку формы
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(this, this.defaults, options);
            Globalize.culture(this.culture);
            // установка событий на форму
            $("#save-support-article-btn").on("click", $.proxy(this.saveArticleBtnOnClick, this));
            $("#delete-support-article-btn").on("click", $.proxy(this.deleteArticleBtnOnClick, this));

            tinymce.init({
                nowrap: false,
                selector: "#Description_Content, #Description_ContentIn",
                plugins: [
                        "advlist textcolor nonbreaking link image media emoticons directionality preview fullscreen code"
                ],

                toolbar1: "undo redo bold italic underline strikethrough alignleft aligncenter alignright alignjustify bullist numlist outdent indent blockquote forecolor backcolor nonbreaking",
                toolbar2: "styleselect formatselect fontselect fontsizeselect link image media emoticons ltr rtl preview fullscreen code",

                menubar: false,
                toolbar_items_size: 'small'
            });

        }
        /**
         * Обработчик события click на кнопке saveArticleBtn
         * @param {JQueryEventObject} event
         */
        , saveArticleBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            var formIsValid = $("#support-article-form").valid();
            if (formIsValid) {
                this.denySubmit = true;
                $("#support-article-form").submit();
            }
        }
        /**
         * Обработчик события click на кнопке deleteArticleBtn
         * @param {JQueryEventObject} event
         */
        , deleteArticleBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            $.alerts.confirm("Вы действительно хотите удалить новость?", $.noop, function (r) {
                if (r) {
                    this.denySubmit = true;
                    var action = $(event.currentTarget).data("href");
                    $(event.currentTarget).closest("form").attr("action", action).submit();
                }
            });
        }
    });
    return SupportArticles;
});