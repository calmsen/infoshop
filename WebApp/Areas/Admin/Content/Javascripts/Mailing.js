﻿/**
 * Модуль содержит свойства и методы по работе с рассылкой писем
 * @author Ruslan Rakhmankulov
 */
define("Admin/Mailing", ["jquery", "common", "tinymce", "jqueryUnobtrusive"], function ($, common, tinymce) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Mailing() {
        this.init.apply(this, arguments);
    }
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(Mailing.prototype, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            baseUrl: common.baseUrl
            , denySubmit: false // состояние, которое запрещает повторную отправку формы
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(this, this.defaults, options);
            Globalize.culture(this.culture);
            // установка событий на форму 
            $("#mailingSubmitBtn").on("click", $.proxy(this.mailingSubmitBtnOnClick, this));

            common.tinymce({
                plugin_preview_width: 710,
                selector: "#Body"
            }); 
        }

        /**
         * Обработчик события click на кнопке submit формы
         * @param {JQueryEventObject} event
         */
        , mailingSubmitBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            var formEl = $("#mailingForm");
            if (!formEl.valid())
                return;
            this.denySubmit = true;
            formEl.submit();
        }
    });
    return Mailing;
});