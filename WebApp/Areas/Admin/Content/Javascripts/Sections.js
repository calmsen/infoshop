﻿/**
 * Модуль содержит свойства и методы по работе с Разделами
 * @author Ruslan Rakhmankulov
 */
define("Admin/Sections", ["jquery", "common", "globalize", "Admin/MainImage", "Admin/ImagesV2", "jqueryUi", "jqueryUnobtrusive", "jqueryTmpl", "text!Admin/CommonTmpl.htm", "jqueryToastmessage", "jqueryAlerts", "bootstrapMultiselect"], function ($, common, Globalize) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Sections() {
        this.init.apply(this, arguments);
    }

    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(Sections.prototype, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            sections: []
            , baseUrl: common.baseUrl
            , culture: common.culture || "ru"
            , denyAjax: false // состояние, которое запрещает повторную отправку запроса аякс
            , sectionId: 0
            , filtersTypes: []
            , filtersValuesCache: {} // кэш полученных данных о настройках значений фильтров из удаленного сервера
            , denySubmit: false // состояние, которое запрещает повторную отправку формы
        }
        // заглушка $.deferred
        , defStub: {
            done: function (fn) {
                fn();
            }
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(this, this.defaults, options);
            Globalize.culture(this.culture);
        }
        /**
         * устанавим события на список разделов
         */
        , setSectionsListEvents: function () {
            this.makeSectionsListSortable();
        }
        /**
         * устанавим события форму раздела
         */
        , setSectionFormEvents: function () {
            this.setSelectGroupListEvents();
            this.setInputHolderEvents(".js-search-filter", ".js-remove-filter-btn", ".js-settings-filter-btn");
            this.setFilterGroupHolderEvents(".js-add-filter-btn", ".js-edit-filter-group-btn", ".js-save-filter-group-btn", ".js-remove-filter-group-btn");
            $("#add-filter-group-btn").on("click", $.proxy(this.addFilterGroupBtnOnClick, this));
            this.makeFilterGroupHolderInsideSortable();
            $("#save-section-btn").on("click", $.proxy(this.saveSectionBtnOnClick, this));
            $("#delete-section-btn").on("click", $.proxy(this.deleteSectionBtnOnClick, this));
        }
        /**
         * устанавим события на селект разделов
         */
        , setSelectGroupListEvents: function () {
            new Sections.AsPathSiblings({
                sections: this.sections
                , omitSectionId: this.sectionId
                , onChange: function (p, currentSectionId) {
                    $("#ParentId").val(currentSectionId);
                    $("form").validate().element('#ParentId');
                }
            });
        }
        /**
         * устанавим события на фильтры
         * @param {String} searchFilterSelector
         * @param {String} removeFilterSelector
         * @param {String} settingsFilterBtnSelector
         */
        , setInputHolderEvents: function (searchFilterSelector, removeFilterSelector, settingsFilterBtnSelector) {
            var sectionsObj = this;
            $(searchFilterSelector).on("input", $.proxy(this.searchFilterInputOnInput, this));
            $(searchFilterSelector).on("blur", $.proxy(this.searchFilterInputOnBlur, this));
            $(searchFilterSelector).autocomplete({
                source: this.baseUrl + "Admin/Filters/AutocompleteSearchFilters",
                /*source: function (request, response) {поиск значения в наборе результата поиска
                    var el = $(this.element);
                    $.ajax({
                        url: sectionsObj.baseUrl + "Admin/Filters/AutocompleteSearchFilters",
                        type: "GET",
                        dataType: "json",
                        cache: false,
                        data: { term: request.term },
                        success: function (data) {
                            var finded = false;
                            for (var i in data) {
                                if (data[i].value == request.term) {
                                    el.closest(".js-input-group").find("input:hidden").val(data[i].id);
                                    sectionsObj.searchFilterInputValidate(el);
                                    finded = true;
                                    break;
                                }
                            }
                            if (!finded) {
                                el.closest(".js-input-group").find("input:hidden").val("0");
                            }                            
                            response(data);
                        },
                    });
                },*/
                minLength: 2,
                select: function (event, ui) {
                    var el = $(this);
                    el.closest(".js-input-group")
                        .data("filterId", ui.item.id)
                        .data("unit", ui.item.unit)
                        .data("type", ui.item.type)
                        .data("unitConverters", ui.item.unitConverters)
                        .find(".js-filter-id").val(ui.item.id);
                    sectionsObj.searchFilterInputValidate(el);
                }
            });
            //
            
            $(removeFilterSelector).on("click", $.proxy(this.removeFilterBtnOnClick, this));
            $(settingsFilterBtnSelector).on("click", $.proxy(this.settingsFilterBtnOnClick, this));
        }
        /**
         * Обработчик события click на кнопке submit формы
         * @param {JQueryEventObject} event
         */
        , saveSectionBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return false;
            var formIsValid = $("#section-form").valid();
            if (formIsValid) {
                this.denySubmit = true;
                $("#section-form").submit();
            }
            return false;
        }
        /**
         * Обработчик события click на кнопке deleteSectionBtn
         * @param {JQueryEventObject} event
         */
        , deleteSectionBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            $.alerts.confirm("Вы действительно хотите удалить раздел? Все ссылки связанные с этим разделом будут удалены.", $.noop, function (r) {
                if (r) {
                    this.denySubmit = true;
                    var action = $(event.currentTarget).data("href");
                    $(event.currentTarget).closest("form").attr("action", action).submit();
                }
            });
        }
        
        
        // Раздел -> Фильтр -> Настройки фильтра 
        /**
         * Обработчик события click на кнопке settingsFilterBtn
         * @param {JQueryEventObject} event
         */
        , settingsFilterBtnOnClick: function (event) {
            var el = $(event.currentTarget);
            var inputGroupEL = el.closest(".js-input-group");
            var groupIndex = inputGroupEL.data("groupIndex");
            var filterIndex = inputGroupEL.data("filterIndex");
            this.showFilterSettingsHolderInside(groupIndex, filterIndex);
        }
        /**
         * Получим данные о настройках значений фильтров из удаленного сервера
         * @param {Number} filterId
         */
        , getFiltersValuesSettings: function (filterId) {
            // вернем заглушку если данные есть в кэше
            if (this.filtersValuesCache[filterId] != undefined)
                return this.defStub;
            // -- -- --
            return $.ajax({
                url: this.baseUrl + "Admin/Filters/Values"
                , data: "filterId=" + filterId
                , type: "GET"
                , dataType: "json"
                , context: this
                , success: function (values) {
                    this.filtersValuesCache[filterId] = values;
                }
            });
        }
        /**
         * Отобразим элемент filterSettingsHolderInside в dom дереве
         * @param {Number} groupIndex
         * @param {Number} filterIndex
         */
        , showFilterSettingsHolderInside: function (groupIndex, filterIndex) {
            var settingsObj = this;
            var filterId = $("#input-group-" + groupIndex + "-" + filterIndex).data("filterId");
            this.getFiltersValuesSettings(filterId)
                .done(function () {
                    // удалим открытую форму с настройками
                    $(".js-filter-settings-holder-inside").remove();
                    // откроем новую форму с настройками
                    settingsObj.getFilterSettingsHolderInside(groupIndex, filterIndex).appendTo("#filter-settings-holder-" + groupIndex + "-" + filterIndex);
                    settingsObj.setFilterSettingsHolderInsideEvents(groupIndex, filterIndex);
                });
        }
        /**
        * Получим jquery элемент filterSettingsHolderInside посредством jqueryTmpl
        * @param {Number} groupIndex
        * @param {Number} filterIndex
        * @returns {jQueryObject}
        */
        , getFilterSettingsHolderInside: function (groupIndex, filterIndex) {
            // подготовим список значений фильтра
            var filterEl = $("#input-group-" + groupIndex + "-" + filterIndex);
            var filterId = filterEl.data("filterId");
            var filterValuesSettings = $("#filters-values-settings-" + groupIndex + "-" + filterIndex)
                .find(".js-value-settings-id")
                .filter(function () {
                    if ($(this).next().val() == "True")
                        return true;
                })
                .map(function () {
                    return { Id: parseInt(this.value) };
                });
            /* массив filterValuesSettings может содержать не все значения и может содержать удаленные значения. поэтому откорректируем его */
            var temp = [];
            for (var i in this.filtersValuesCache[filterId]) {
                
                var valueSettings = { Id: this.filtersValuesCache[filterId][i].Id, Activated: false, Title: this.filtersValuesCache[filterId][i].Title };
                for (var j in filterValuesSettings) {
                    if (filterValuesSettings[j].Id == this.filtersValuesCache[filterId][i].Id) {
                        valueSettings.Activated = true;
                    }
                }
                // 
                temp.push(valueSettings);
            }
            filterValuesSettings = temp;
            // -- -- --
            var filterType = $("#input-group-" + groupIndex + "-" + filterIndex).data("type");
            var redFilterType = $("#FiltersGroups_" + groupIndex + "__Filters_" + filterIndex + "__Type").val();
            if (redFilterType != "")
                filterType = redFilterType;

            return $("#filterSettingsHolderInsideTmpl").tmpl({
                groupIndex: groupIndex
                , filterIndex: filterIndex
                , unit: $("#input-group-" + groupIndex + "-" + filterIndex).data("unit")
                , unitConverters: $("#input-group-" + groupIndex + "-" + filterIndex).data("unitConverters")
                , title: $("#FiltersGroups_" + groupIndex + "__Filters_" + filterIndex + "__Title").val()
                , titleIn: $("#FiltersGroups_" + groupIndex + "__Filters_" + filterIndex + "__TitleIn").val()
                , type: filterType
                , unitConverterIdForCatalog: $("#FiltersGroups_" + groupIndex + "__Filters_" + filterIndex + "__UnitConverterIdForCatalog").val()
                , unitConverterIdForProduct: $("#FiltersGroups_" + groupIndex + "__Filters_" + filterIndex + "__UnitConverterIdForProduct").val()
                , showInGroupSelect: $("#FiltersGroups_" + groupIndex + "__Filters_" + filterIndex + "__ShowInGroupSelect").val() == "True"
                , filterValuesSettings: filterValuesSettings
                , filtersTypes: this.filtersTypes
            });
        }
        /**
        * Установка событий на элемент filterSettingsHolderInside
        * @param {Number} groupIndex
        * @param {Number} filterIndex
        */
        , setFilterSettingsHolderInsideEvents: function (groupIndex, filterIndex) {
            $("#filter-settings-holder-inside-" + groupIndex + "-" + filterIndex)
                .find(".js-cancel-settings-btn").on("click", $.proxy(this.cancelSettingsBtnOnClick, this))
            .end()
                .find(".js-save-settings-btn").on("click", $.proxy(this.saveSettingsBtnOnClick, this));
        }
        /**
         * Обработчик события click на кнопке cancelSettingsBtn
         * @param {JQueryEventObject} event
         */
        , cancelSettingsBtnOnClick: function (event) {
            $(event.currentTarget).closest(".js-filter-settings-holder-inside").remove();
        }
        /**
         * Обработчик события click на кнопке saveSettingsBtn
         * @param {JQueryEventObject} event
         */
        , saveSettingsBtnOnClick: function (event) {
            var el = $(event.currentTarget);
            var inputGroupEL = el.closest(".js-input-group");
            var groupIndex = inputGroupEL.data("groupIndex");
            var filterIndex = inputGroupEL.data("filterIndex");
            this.saveSettings(groupIndex, filterIndex);
        }
        /**
         * Сохраним настройки
         * @param {Number} groupIndex
         * @param {Number} filterIndex
         */
        , saveSettings: function (groupIndex, filterIndex) {
            // сохраняем заголовок и тип
            var title = $("#Settings_Title").val();
            $("#FiltersGroups_" + groupIndex + "__Filters_" + filterIndex + "__Title").val(title);
            var titleIn = $("#Settings_TitleIn").val();
            $("#FiltersGroups_" + groupIndex + "__Filters_" + filterIndex + "__TitleIn").val(titleIn);
            var type = $("#Settings_Type").val();
            $("#FiltersGroups_" + groupIndex + "__Filters_" + filterIndex + "__Type").val(type);
            // -- -- --
            // сохраним конверторы
            if ($("#Settings_UnitConverterIdForCatalog").length > 0) {
                var unitForCatalog = $("#Settings_UnitConverterIdForCatalog").val();
                $("#FiltersGroups_" + groupIndex + "__Filters_" + filterIndex + "__UnitConverterIdForCatalog").val(unitForCatalog);
                var unitForProduct = $("#Settings_UnitConverterIdForProduct").val();
                $("#FiltersGroups_" + groupIndex + "__Filters_" + filterIndex + "__UnitConverterIdForProduct").val(unitForProduct);
            }            
            // -- -- --
            // сохраним значения
            var filterSettingsValues = [];
            $(".js-value-settings:checked").each(function(){
                filterSettingsValues.push({Id: $(this).data("valueId"), Activated: true});
            });
            var valuesHolder = $("#filters-values-settings-" + groupIndex + "-" + filterIndex);
            valuesHolder.empty();
            for(var i in filterSettingsValues) {
                $("<input/>")
                    .attr("type", "hidden")
                    .attr("id", "FiltersGroups_" + groupIndex + "__Filters_" + filterIndex + "_Values_" + i + "__Id")
                    .attr("name", "FiltersGroups[" + groupIndex + "].Filters[" + filterIndex + "].Values[" + i + "].Id")
                    .attr("class", "js-value-settings-id")
                    .val(filterSettingsValues[i].Id).appendTo(valuesHolder)
                $("<input/>")
                    .attr("type", "hidden")
                    .attr("id", "FiltersGroups_" + groupIndex + "__Filters_" + filterIndex + "_Values_" + i + "__Activated")
                    .attr("name", "FiltersGroups[" + groupIndex + "].Filters[" + filterIndex + "].Values[" + i + "].Activated")
                    .attr("class", "js-value-settings-activated")
                    .val("True").appendTo(valuesHolder)
            }
            // -- -- --
            // сохраним ShowInGroupSelect
            var showInGroupSelect = $("#Settings_ShowInGroupSelect").is(":checked") ? "True" : "False";
            $("#FiltersGroups_" + groupIndex + "__Filters_" + filterIndex + "__ShowInGroupSelect").val(showInGroupSelect);
            // -- -- --
            $("#filter-settings-holder-inside-" + groupIndex + "-" + filterIndex).remove();
        }
        // -/ Раздел -> Фильтр -> Настройки фильтра
        // Раздел -> Фильтр -> Поиск, добавление, удаление фильтра 
        /**
         * Обработчик события input на элементе searchFilterInput
         * @param {JQueryEventObject} event
         */
        , searchFilterInputOnInput: function (event) {
            $(event.currentTarget).closest(".js-input-group")
                .find(".js-filter-id").val("0");
        }
        /**
         * Обработчик события blur на элементе searchFilterInput
         * @param {JQueryEventObject} event
         */
        , searchFilterInputOnBlur: function (event) {
            var el = $(event.currentTarget);
            this.searchFilterInputValidate(el);
        }
        /**
         * Проверим элемент searchFilterInput (а вернее его значение) на валидность
         * @param {jQueryObject} el
         */
        , searchFilterInputValidate: function (el) {
            if (el.closest(".js-input-group").find(".js-filter-id").val() == "0") {
                el.css("border-color", "#a94442");
            } else {
                el.css("border-color", "");
            }
        }
        /**
        * Получим jquery элемент inputHolder посредством jqueryTmpl
        * @param {Number} groupIndex
        * @returns {jQueryObject}
        */
        , getInputHolder: function (groupIndex) {
            var filterIndex = parseInt($("#filter-group-holder-" + groupIndex + " .js-input-group:last").data("filterIndex"));
            if (isNaN(filterIndex))
                filterIndex = 0;
            else {
                filterIndex++;
            }
            return $("#inputHolderTmpl").tmpl({
                groupIndex: groupIndex
                , filterIndex: filterIndex
            });
        }
        /**
        * Отобразим элемент inputHolder в dom дереве
        * @param {Number} groupIndex
        */
        , showInputHolder: function (groupIndex) {
            this.getInputHolder(groupIndex).appendTo($("#filter-group-holder-" + groupIndex + " .filter-group-holder-inside:last"));
            this.setInputHolderEvents("#filter-group-holder-" + groupIndex + " .js-search-filter:last",
                "#filter-group-holder-" + groupIndex + " .js-remove-filter-btn:last",
                "#filter-group-holder-" + groupIndex + " .js-settings-filter-btn:last");
            $("#filter-group-holder-" + groupIndex + " .js-search-filter:last").focus();
        }
        /**
         * Обработчик события click на элементе addFilterBtn
         * @param {JQueryEventObject} event
         */
        , addFilterBtnOnClick: function (event) {
            var groupIndex = $(event.target).closest(".js-filter-group-holder").data("groupIndex");
            this.showInputHolder(groupIndex);
        }
        /**
         * Удаление элемента inputHolder
         * @param {String} inputHolderId
         */
        , removeInputHolder: function (inputHolderId) {
            var holder = $("#" + inputHolderId);
            var groupIndex = holder.data("groupIndex");
            holder.remove();

            this.changeInputHoldersPositions(groupIndex);
        }
        /**
         * Обработчик события click на элементе removeFilterBtn
         * @param {JQueryEventObject} event
         */
        , removeFilterBtnOnClick: function (event) {
            var inputHolderId = $(event.currentTarget).closest(".js-input-group").attr("id");
            this.removeInputHolder(inputHolderId);
        }
        // -/ Раздел -> Фильтр -> Поиск, добавление, удаление фильтра
        // Раздел -> Фильтр -> Сортировка фильтра
        /**
         * Изменение позиций элементов inputHolders
         * @param {Number} groupIndex
         */
        , changeInputHoldersPositions: function (groupIndex) {
            // отменим редактирование формы с настройками фильтра если форма была открыта
            $(".js-cancel-settings-btn").click();
            // -- -- --
            $("#filter-group-holder-" + groupIndex).find(".js-input-group").each(function (i) {
                $(this)
                    .attr("id", "input-group-" + groupIndex + "-" + i)
                    .data("filterIndex", i)
                    .find(".js-filter-id")
                        .attr("id", "FiltersGroups_" + groupIndex + "__Filters_" + i + "__Id")
                        .attr("name", "FiltersGroups[" + groupIndex + "].Filters[" + i + "].Id")
                .end()
                    .find(".js-filter-title")
                        .attr("id", "FiltersGroups_" + groupIndex + "__Filters_" + i + "__Title")
                        .attr("name", "FiltersGroups[" + groupIndex + "].Filters[" + i + "].title")
                .end()
                    .find(".js-filter-titleIn")
                        .attr("id", "FiltersGroups_" + groupIndex + "__Filters_" + i + "__TitleIn")
                        .attr("name", "FiltersGroups[" + groupIndex + "].Filters[" + i + "].titleIn")
                .end()
                    .find(".js-filter-type")
                        .attr("id", "FiltersGroups_" + groupIndex + "__Filters_" + i + "__Type")
                        .attr("name", "FiltersGroups[" + groupIndex + "].Filters[" + i + "].Type")
                .end()
                    .find(".js-filter-unitConverterIdForCatalog")
                        .attr("id", "FiltersGroups_" + groupIndex + "__Filters_" + i + "__UnitConverterIdForCatalog")
                        .attr("name", "FiltersGroups[" + groupIndex + "].Filters[" + i + "].UnitConverterIdForCatalog")
                .end()
                    .find(".js-filter-unitConverterIdForProduct")
                        .attr("id", "FiltersGroups_" + groupIndex + "__Filters_" + i + "__UnitConverterIdForProduct")
                        .attr("name", "FiltersGroups[" + groupIndex + "].Filters[" + i + "].UnitConverterIdForProduct")
                .end()
                    .find(".js-filter-showInGroupSelect")
                        .attr("id", "FiltersGroups_" + groupIndex + "__Filters_" + i + "__ShowInGroupSelect")
                        .attr("name", "FiltersGroups[" + groupIndex + "].Filters[" + i + "].ShowInGroupSelect")
                .end()
                    .find(".js-value-settings-id").each(function (k) {
                        $(this)
                            .attr("id", "FiltersGroups_" + groupIndex + "__Filters_" + i + "_Values_" + k + "__Id")
                            .attr("name", "FiltersGroups[" + groupIndex + "].Filters[" + i + "].Values[" + k + "].Id")
                    })
                .end()
                    .find(".js-value-settings-activated").each(function (k) {
                        $(this)
                            .attr("id", "FiltersGroups_" + groupIndex + "__Filters_" + i + "_Activated_" + k + "__Id")
                            .attr("name", "FiltersGroups[" + groupIndex + "].Filters[" + i + "].Activated[" + k + "].Id")
                    })
                .end()
                    .find(".js-filter-settings-holder")
                        .attr("id", "filter-settings-holder-" + groupIndex + "-" + i);
            });
        }
        /**
         * Сделаем элементы внутри элемента filterGroupHolderInside сортируемыми
         */
        , makeFilterGroupHolderInsideSortable: function () {
            var sectionsObj = this;
            $(".js-filter-group-holder-inside").sortable({
                handle: ".js-move-filter-btn"
                , update: function (event, ui) {
                    sectionsObj.changeInputHoldersPositions(ui.item.data("groupIndex"));
                }
            });
        }
        // -/ Раздел -> Фильтр -> Сортировка фильтра
        // Раздел -> Группа фильтров -> Редактирование группы фильтра
        /**
         * Покажем элемент filterGroupInputHolder
         * @param {Number} groupIndex
         */
        , showFilterGroupInputHolder: function (groupIndex) {
            $("#filter-group-holder-" + groupIndex)
                .find(".js-filter-group-header").hide()
            .end()
                .find(".js-filter-group-input-holder").show();
            $("#FiltersGroups_" + groupIndex + "__Title").focus();
            
        }
        /**
         * Обработчик события click на элементе editFilterGroupBtn
         * @param {JQueryEventObject} event
         */
        , editFilterGroupBtnOnClick: function (event) {
            var groupIndex = $(event.target).closest(".js-filter-group-holder").data("groupIndex");
            this.showFilterGroupInputHolder(groupIndex);
        }
        /**
         * Покажем элемент filterGroupHeader
         * @param {Number} groupIndex
         */
        , showFilterGroupHeader: function (groupIndex) {
            $("#filter-group-holder-" + groupIndex)
                .find(".js-filter-group-input-holder").hide()
            .end()
                .find(".js-filter-group-header").show()
        }
        /**
         * Обработчик события click на элементе saveFilterGroupBtn
         * @param {JQueryEventObject} event
         */
        , saveFilterGroupBtnOnClick: function (event) {
            var groupIndex = $(event.target).closest(".js-filter-group-holder").data("groupIndex");
            var value = $("#FiltersGroups_" + groupIndex + "__Title").val();
            if (value.trim().length == 0) {
                $("#FiltersGroups_" + groupIndex + "__Title").focus();
                return;
            }
            $("#filter-group-holder-" + groupIndex)
                .find(".js-filter-group-title").text(value);
            this.showFilterGroupHeader(groupIndex);
        }
        /**
         * Установка событий на элемент filterGroupHolder
         * @param {String} addFilterBtnSelector
         * @param {String} editFilterGroupBtnSelector
         * @param {String} saveFilterGroupBtnSelector
         * @param {String} removeFilterGroupBtnSelector
         */
        , setFilterGroupHolderEvents: function (addFilterBtnSelector, editFilterGroupBtnSelector, saveFilterGroupBtnSelector, removeFilterGroupBtnSelector) {
            $(addFilterBtnSelector).on("focus", $.proxy(this.addFilterBtnOnClick, this));
            $(editFilterGroupBtnSelector).on("click", $.proxy(this.editFilterGroupBtnOnClick, this));
            $(saveFilterGroupBtnSelector).on("click", $.proxy(this.saveFilterGroupBtnOnClick, this));
            $(removeFilterGroupBtnSelector).on("click", $.proxy(this.removeFilterGroupBtnOnClick, this));
        }
        /**
        * Получим jquery элемент filterGroupHolder посредством jqueryTmpl
        * @param {Number} groupIndex
        * @returns {jQueryObject}
        */
        , getFilterGroupHolder: function (groupIndex) {
            return $("#filterGroupHolderTmpl").tmpl({
                groupIndex: groupIndex
                , groupId: new Date().getTime()
                , isAllowLocalization: common.isAllowLocalization
            });
        }
        /**
         * Отобразим элемент filterGroupHolder в dom дереве
         */
        , showFilterGroupHolder: function () {
            var groupIndex = $(".js-filter-group-holder").length;
            this.getFilterGroupHolder(groupIndex).insertBefore("#add-filter-group-btn");
            this.setFilterGroupHolderEvents(".js-add-filter-btn:last", ".js-edit-filter-group-btn:last", ".js-save-filter-group-btn:last", ".js-remove-filter-group-btn:last");
            this.showFilterGroupInputHolder(groupIndex);
        }
        /**
         * Обработчик события click на элементе addFilterGroupBtnO
         * @param {JQueryEventObject} event
         */
        , addFilterGroupBtnOnClick: function (event) {
            this.showFilterGroupHolder();
        }
        /**
         * Удалим элемент filterGroupHolder
         * @param {Number} groupIndex
         */
        , removeFilterGroupHolder: function (groupIndex) {
            $("#filter-group-holder-" + groupIndex).remove();
            var settingsObj = this;
            $(".js-filter-group-holder").each(function (i, v) {
                // изменим позиции групп
                $(this)
                    .data("groupIndex", i)
                    .attr("id", "filter-group-holder-" + i)
                    .find(".js-filter-group-title")
                        .attr("id", "FiltersGroups_" + i + "__Title")
                        .attr("name", "FiltersGroups[" + i + "].Title")
                .end()
                    .find(".js-filter-group-title-en")
                        .attr("id", "FiltersGroups_" + i + "__TitleIn")
                        .attr("name", "FiltersGroups[" + i + "].TitleIn");
                // изменим позиции фильтров в группе
                settingsObj.changeInputHoldersPositions(i);
            });

        }
        /**
         * Обработчик события click на элементе removeFilterGroupBtn
         * @param {JQueryEventObject} event
         */
        , removeFilterGroupBtnOnClick: function (event) {
            var groupIndex = $(event.currentTarget).closest(".js-filter-group-holder").data("groupIndex");
            var sectionsObj = this;
            if (parseInt($("#Id").val()) > 0) {
                $.alerts.confirm("Вы действительно хотите удалить группу фильтров? Все ссылки связанные с этими фильтрами для текущего раздела будут удалены.", $.noop, function (r) {
                    if (r) {
                        sectionsObj.removeFilterGroupHolder(groupIndex);
                    }
                });
            } else {
                this.removeFilterGroupHolder(groupIndex);
            }            
        }
        // -/ Раздел -> Группа фильтров -> Редактирование группы фильтра
        //
        // Разделы -> Сортировка разделов */
        /**
         * Изменим позиции разделов
         * @param {Number} groupIndex
         */
        , changeSectionsListPositions: function (groupIndex) {
            $("#sections-list").find("tr").each(function (i) {
                var el = $(this);
                var pos = i + 1;
                
                var path = el.find(".js-section-path-text").text().replace(/\s/g, "").split(".")
                    .map(function (v,j,arr) { 
                        if (v !== "0" &&  (arr[j + 1] === "0"))
                            return pos;
                        else
                            return v;
                    })
                    .join(".");

                el.data("sectionIndex", i);
                el.find(".js-section-id")
                        .attr("id", "Sections_" + i + "__Id")
                        .attr("name", "Sections[" + i + "].Id");
                el.find(".js-section-position")
                        .attr("id", "Sections_" + i + "__Position")
                        .attr("name", "Sections[" + i + "].Position")
                        .val(pos);
                el.find(".js-section-position-text")
                        .text(pos);
                el.find(".js-section-path-text")
                        .text(path);

            });
        }
        /**
         * Сохраним позиции разделов на удаленном сервере
         */
        , saveSectionsPositions: function () {
            $.ajax({
                url: this.baseUrl + "Admin/Sections/SaveSectionsPositions"
                , data: $("#sections-list-form").serialize()
            });
        }
        /**
         * Сделаем список разделов сортируемым
         */
        , makeSectionsListSortable: function () {
            var sectionsObj = this;
            $("#sections-list").sortable({
                cancel: "input,button,a"
                , cursor: "move"
                , update: function (event, ui) {
                    sectionsObj.changeSectionsListPositions();
                    sectionsObj.saveSectionsPositions();
                }
            });
        }
        // -/ Разделы -> Сортировка разделов
    });
    /*
     * Определим другие модули:
     */
    $.extend(Sections, {
        /**
         * Модуль содержит свойства и методы по работе с выбором пути к разделу
         * @author Ruslan Rakhmankulov
         */
        AsPathSiblings: (function ($) {
            /**
             * Конструктор является прокси для метода init
             * @constructor
             */
            function AsPathSiblings() {
                this.init.apply(this, arguments);
            }
            /**
             * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
             * Расширяем прототип с помощью jQuery.extend
             */
            $.extend(AsPathSiblings.prototype, {
                /**
                 * Все свойства defaults являются переопределяемые через объект options.
                 */
                defaults: {
                    sections: []
                    , omitSectionId: 0
                    , onChange: function (me, currentSectionId) {}
                }
                /**
                 * Инициализиуем экземпляр класса
                 * @param {Object} options
                 */
                , init: function (options) {
                    $.extend(this, this.defaults, options);
                    this.setSelectGroupListEvents();
                }
                /**
                 * Получим jquery элемент selectGroupList посредством jqueryTmpl
                 * @param {Number} sectionId
                 * @param {Array} sectionsAsPathSiblings
                 * @returns {jQueryObject}
                 */
                , getSelectGroupList: function (sectionId, sectionsAsPathSiblings) {
                    return $("#selectGroupList").tmpl({
                        sectionId: sectionId
                        , sectionsAsPathSiblings: sectionsAsPathSiblings
                        , activeSections: this.getActiveSections(sectionsAsPathSiblings, sectionId)
                        , omitSectionId: this.omitSectionId
                        , validationAttributes: this.validationAttributes
                    });
                }
                /**
                 * Установим события на элемент selectGroupList
                 */
                , setSelectGroupListEvents: function () {
                    $(".select-group-item").on("change", $.proxy(this.sectionOnChange, this));
                }
                /**
                 * Отобразим элемент selectGroupList в dom дереве
                 * @param {Number} sectionId
                 */
                , showSelectGroupList: function (sectionId) {
                    var sectionsAsPathSiblings = this.getSectionsAsPathSiblings(sectionId);
                    $(this.getSelectGroupList(sectionId, sectionsAsPathSiblings)).replaceAll("#select-group-list");
                    this.setSelectGroupListEvents();
                }
                /**
                 * Обработчик события click на элементе section
                 * @param {JQueryEventObject} event
                 */
                , sectionOnChange: function (event) {
                    var sectionId = $(event.currentTarget).val();
                    this.showSelectGroupList(sectionId);
                    this.onChange(this, sectionId);
                }
                /**
                 * Получим активные разделы для выбранного раздела
                 * @param {Array} sectionsAsPathSiblings
                 * @param {Number} sectionId
                 * @returns {Array}
                 */
                , getActiveSections: function (sectionsAsPathSiblings, sectionId) {
                    var activeSections = [];
                    for (var i = sectionsAsPathSiblings.length - 1; i >= 0; i--) {
                        for (var s in sectionsAsPathSiblings[i]) {
                            s = sectionsAsPathSiblings[i][s];
                            if (s.Id == sectionId) {
                                activeSections.push(s.Id);
                                sectionId = s.ParentId;
                                break;
                            }
                        }
                    }
                    activeSections.reverse();
                    return activeSections;
                }
                /**
                 * Получим дочерние разделы 
                 * @param {Number} sectionId
                 * @returns {Array}
                 */
                , getSectionChilds: function (sectionId) {
                    var sections = [];
                    for (var i in this.sections) {
                        if (this.sections[i].ParentId == sectionId) {
                            sections.push(this.sections[i]);
                        }
                    }
                    return sections;
                }
                /**
                 * Получим пути по разделу
                 * @param {Number} sectionId
                 * @returns {Array}
                 */
                , getSectionsAsPathSiblings: function (sectionId) {
                    var sectionsList = [];
                    if (sectionId == 0) {
                        var sectioChilds = this.getSectionChilds(0);
                        if (sectioChilds.length > 0)
                            sectionsList.push(sectioChilds);
                        return sectionsList;
                    }
                    section = {
                        Id: sectionId
                    };
                    for (var i = 0; i < 10; i++) {
                        var sectionSiblings = this.getSectionSiblings(section.Id);
                        sectionsList.push(sectionSiblings);
                        if (sectionSiblings[0].ParentId == 0)
                            break;
                        section = this.getSectionById(sectionSiblings[0].ParentId);
                    }
                    sectionsList.reverse();
                    var sectioChilds = this.getSectionChilds(sectionId);
                    if (sectioChilds.length > 0)
                        sectionsList.push(sectioChilds);
                    return sectionsList;
                }
                /**
                 * Получим соседние разделы
                 * @param {Number} sectionId
                 * @returns {Array}
                 */
                , getSectionSiblings: function (sectionId) {
                    var section = this.getSectionById(sectionId);
                    var sections = [];
                    for (var i in this.sections) {
                        if (this.sections[i].ParentId == section.ParentId) {
                            sections.push(this.sections[i]);
                        }
                    }
                    return sections;
                }
                /**
                 * Получим данные по разделу
                 * @param {Number} sectionId
                 * @returns {Object}
                 */
                , getSectionById: function (sectionId) {
                    var section = null;
                    for (var i in this.sections) {
                        if (this.sections[i].Id == sectionId) {
                            section = this.sections[i];
                            break;
                        }
                    }
                    return section;
                }
            });
            return AsPathSiblings;
        }($))
    });
    return Sections;
});