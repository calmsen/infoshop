﻿/**
 * Модуль содержит свойства и методы по работе с Фильтрами
 * @author Ruslan Rakhmankulov
 */
define("Admin/Filters", ["jquery", "common", "globalize", "jqueryUi", "jqueryUnobtrusive", "jqueryTmpl", "text!Admin/CommonTmpl.htm", "jqueryToastmessage", "jqueryAlerts", "fancybox", "bootstrapMultiselect"], function ($, common, Globalize) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Filters() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(Filters.prototype, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            baseUrl: common.baseUrl
            , culture: common.culture || "ru"
            , denyAjax: false // состояние, которое запрещает повторную отправку запроса аякс
            , validationAttributes: {} // атрибуты для валидации по jquery unobtrusive
            , denySubmit: false // состояние, которое запрещает повторную отправку формы 
            , allowExtendedFunctions: false // включим раширенные функции для редактирования значений фильтра
            , sections: [] // разделы к которым привязан фильтр
            , _choosedSectionIds: [] // разделы к которым нужно применить расширенную функцию
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(this, this.defaults, options);
            Globalize.culture(this.culture);
        }

        /**
         * Установим событий и инициализация плагинов для формы 
         */
        , setFilterFormEvents: function () {
            $("#Unit").on("input", $.proxy(this.filterUnitInputOnInput, this));
            $("#Multiple").on("change", $.proxy(this.multipleCheckboxOnChange, this));
            $("#add-value-holder-btn").on("focus", $.proxy(this.addValueHolderBtnOnClick, this));
            $("#show-converter-add-btn").on("click", $.proxy(this.showConverterAddBtnOnClick, this));
            var filtersObj = this;
            $(".js-converters-item").each(function (i) {
                filtersObj.setConvertersItemEvents(i);
            });
            this.setValueHolderEvents(".js-remove-value-holder-btn", ".js-value-as-double", ".js-btn-copy", ".js-btn-cut", ".js-btn-paste");
            this.makeValuesHolderSortable();
            $("#sort-values-btn").on("click", $.proxy(this.sortValueHolders, this));
            $("#save-filter-btn").on("click", $.proxy(this.saveFilterBtnOnClick, this));
            $("#delete-filter-btn").on("click", $.proxy(this.deleteFilterBtnOnClick, this));
            // события для кнопок включения/выключений расширенных функций
            $("#onExtendedFunctionsBtn").on("click", $.proxy(this.onExtendedFunctionsBtnOnClick, this));
            $("#offExtendedFunctionsBtn").on("click", $.proxy(this.offExtendedFunctionsBtnOnClick, this));
        }

        /**
         * Установим событий на спискок фильтров
         */
        , setFiltersListEvents: function () {
        }
        /**
         * Обработчик события click на кнопке submit формы Filter
         * @param {JQueryEventObject} event
         */
        , saveFilterBtnOnClick: function (event) {
            event.preventDefault();
            //if (this.denySubmit)
            //    return;
            var formIsValid = $("#filter-form").valid();
            if (formIsValid) {
                //this.denySubmit = true;
                $("#filter-form").submit();
            }
            return;
        }
        /**
         * Обработчик события click на ссылке deleteFilterBtn
         * @param {JQueryEventObject} event
         */
        , deleteFilterBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            $.alerts.confirm("Вы действительно хотите удалить фильтр? Все ссылки связанные с этим фильтром будут удалены.", $.noop, function (r) {
                if (r) {
                    this.denySubmit = true;
                    var action = $(event.currentTarget).data("href");
                    $(event.currentTarget).closest("form").attr("action", action).submit();
                }
            });
        }
        /* Работа с конверторами */
        /**
         * Обработчик события click на кнопке showConverterAddBtn
         * @param {JQueryEventObject} event
         */
        , showConverterAddBtnOnClick: function (event) {
            this.showConverterEditInside("add", $(".js-converters-item").length, { Id: new Date().getTime(), Unit: "", UnitIn: "", UnitK: "" });
        }
        /**
         * Отобразим элемент converterEditInside в dom дереве
         * @param {String} action
         * @param {Number} converterIndex
         * @param {Object} converter
         */
        , showConverterEditInside: function (action, converterIndex, converter) {
            // удаляем текущею форму и показываем кнопку вызыющую эту форму
            var el = $("#converter-edit-inside");
            if (el.length > 0) {
                if (el.data("action") == "add")
                    $("#show-converter-add-btn").show();
                else {
                    $("#converters-item-" + el.data("converterIndex")).show();
                }
                el.remove();
            }
            //
            if (action == "add")
                $("#show-converter-add-btn").hide();
            else
                $("#converters-item-" + converterIndex).hide();
            this.getConverterEditInside(action, converterIndex, converter).appendTo("#converter-edit");
            this.setConverterEditInsideEvents(action, converterIndex);
        }
        /**
        * Получим jquery элемент converterEditInside посредством jqueryTmpl
        * @param {String} action
        * @param {Number} converterIndex
        * @param {Object} converter
        * @returns {jQueryObject}
        */
        , getConverterEditInside: function (action, converterIndex, converter) {
            return $("#converterEditInsideTmpl").tmpl({
                action: action
                , converterIndex: converterIndex
                , converter: converter
                , isAllowLocalization: common.isAllowLocalization
            });
        }
        /**
         * Установим события на элемент converterEditInside
         * @param {String} action
         * @param {Number} converterIndex
         */
        , setConverterEditInsideEvents: function (action, converterIndex) {
            $("#" + action + "-converter-btn").on("click", $.proxy(this[action + "ConverterBtnOnClick"], this));
            $("#cancel-converter-btn").on("click", $.proxy(this.cancelConverterBtnOnClick, this));
        }
        /**
         * Обработчик события click на ссылке addConverterBtn
         * @param {JQueryEventObject} event
         */
        , addConverterBtnOnClick: function(event) {
            var el = $("#converter-edit-inside");
            this.addConverter(el.data("converterIndex"));
        }
        /**
         * Добавим опцию в селект
         * @param {String} value
         * @param {String} text
         * @param {String} selectId
         */
        , addOptionToSelectEl: function (value, text, selectId) {
            $('<option/>', {
                value: value,
                text: text
            })
            .appendTo("#" + selectId);
        }
        /**
         * Удалим опцию из селекта
         * @param {String} value
         * @param {String} selectId
         */
        , deleteOptionFromSelectEl: function (value, selectId) {
            $("#" + selectId + " option[value=" + value + "]").remove();
        }
        /**
         * Добавим новый конвертер 
         * @param {Number} converterIndex 
         */
        , addConverter: function (converterIndex) {
            var converter = {};
            if (!this.validateConverter(converter))
                return;
            this.showConvertersItem("add", converterIndex, converter);
            this.addOptionToSelectEl(converter.Id, converter.Unit, "UnitConverterIdForCatalog");
            this.addOptionToSelectEl(converter.Id, converter.Unit, "UnitConverterIdForProduct");
        }
        /**
         * Обработчик события click на ссылке editConverterBtn
         * @param {JQueryEventObject} event
         */
        , editConverterBtnOnClick: function (event) {
            var el = $("#converter-edit-inside");
            this.editConverter(el.data("converterIndex"));
        }
        /**
         * Редактируем конвертер 
         * @param {Number} converterIndex 
         */
        , editConverter: function (converterIndex) {
            var converter = {};
            if (!this.validateConverter(converter))
                return;
            this.showConvertersItem("edit", converterIndex, converter);
        }
        /**
         * Проверим конвертер
         * @param converter 
         * @returns {Boolean}
         */
        , validateConverter: function (converter) {
            var id = document.getElementById("Converter.Id").value.trim();
            var unit = document.getElementById("Converter.Unit").value.trim();
            var unitIn = document.getElementById("Converter.UnitIn").value.trim();
            var unitK = document.getElementById("Converter.UnitK").value.trim();
            if (unit == "" || unitK == "" 
                || unitK.indexOf(":") == -1 || !/[0-9]+/.test(unitK.split(":")[0]) || !/[0-9]+/.test(unitK.split(":")[1])) 
                return false;
            converter.Id = id;
            converter.Unit = unit;
            converter.UnitIn = unitIn;
            converter.UnitK = unitK;
            return true;
        }
        /**
         * Отобразим элемент сonvertersItem в dom дереве  
         * @param {String} action
         * @param {Number} converterIndex
         * @param {Object} converter
         */
        , showConvertersItem: function(action, converterIndex, converter) {
            $("#converter-edit-inside").remove();
            var el = this.getConvertersItem(converterIndex, converter);
            var oldEl = $("#converters-item-" + converterIndex);
            
            if(oldEl.length > 0) 
                el.replaceAll(oldEl);
            else 
                el.insertBefore("#show-converter-add-btn");

            this.setConvertersItemEvents(converterIndex);
            if (action == "add")
                $("#show-converter-add-btn").show();
        }
        /**
        * Получим jquery элемент convertersItem посредством jqueryTmpl
        * @param {Number} converterIndex
        * @param {Object} converter
        * @returns {jQueryObject}
        */
        , getConvertersItem: function(converterIndex, converter) {
            return $("#convertersItemTmpl").tmpl({
                converterIndex: converterIndex
                , converter: converter
            });
        }

        /**
         * Установим события на элемент convertersItem
         * @param {Number} converterIndex
         */
        , setConvertersItemEvents: function (converterIndex) {
            $("#show-converter-edit-btn-" + converterIndex).on("click", $.proxy(this.showConverterEditBtnOnClick, this));
            $("#delete-converter-btn-" + converterIndex).on("click", $.proxy(this.deleteConverterBtnOnClick, this));
        }
        /**
         * Обработчик события click на ссылке deleteConverterBtn
         * @param {JQueryEventObject} event
         */
        , deleteConverterBtnOnClick: function (event) {
            var el = $(event.currentTarget).closest(".js-converters-item");
            this.deleteConverter(el.data("converterIndex"));
        }
        /**
         * Удалим конвертер из dom дерева
         * @param {Number} converterIndex
         */
        , deleteConverter: function (converterIndex) {
            //
            var value = $("#UnitConverters_" + converterIndex + "__Id").val();
            this.deleteOptionFromSelectEl(value, "UnitConverterIdForCatalog");
            this.deleteOptionFromSelectEl(value, "UnitConverterIdForProduct");
            $("#converters-item-" + converterIndex).remove();
            //
            $(".js-converters-item").each(function (i) {
                var el = $(this);
                el.attr("id", "converters-item-" + i).data("converterIndex", i);
                el.find(".js-converters-item-id")
                    .attr("id", "UnitConverters_" + i + "__Id")
                    .attr("name", "UnitConverters[" + i + "].Id");
                el.find(".js-converters-item-unit")
                    .attr("id", "UnitConverters_" + i + "__Unit")
                    .attr("name", "UnitConverters[" + i + "].Unit");
                el.find(".js-converters-item-unitIn")
                    .attr("id", "UnitConverters_" + i + "__UnitIn")
                    .attr("name", "UnitConverters[" + i + "].UnitIn");
                el.find(".js-converters-item-unitK")
                    .attr("id", "UnitConverters_" + i + "__UnitK")
                    .attr("name", "UnitConverters[" + i + "].UnitK");
                el.find(".js-show-converter-edit-btn")
                    .attr("id", "show-converter-edit-btn-" + i);
                el.find(".js-delete-converter-btn")
                    .attr("id", "delete-converter-btn-" + i);
            });
        }
        /**
         * Обработчик события click на ссылке showConverterEditBtn
         * @param {JQueryEventObject} event
         */
        , showConverterEditBtnOnClick: function (event) {
            var converterIndex = $(event.currentTarget).closest(".js-converters-item").data("converterIndex");
            var id = document.getElementById("UnitConverters_" + converterIndex + "__Id").value;
            var unit = document.getElementById("UnitConverters_" + converterIndex + "__Unit").value;
            var unitIn = document.getElementById("UnitConverters_" + converterIndex + "__UnitIn").value;
            var unitK = document.getElementById("UnitConverters_" + converterIndex + "__UnitK").value;

            this.showConverterEditInside("edit", converterIndex, { Id: id, Unit: unit, UnitIn: unitIn, UnitK: unitK });
        }
        /**
         * Обработчик события click на ссылке cancelConverterBt
         * @param {JQueryEventObject} event
         */
        , cancelConverterBtnOnClick: function (event) {
            var el = $("#converter-edit-inside");
            this.cancelConverter(el.data("action"), el.data("converterIndex"));
        }
        /**
         * Отменим сохранение конвертера
         * @param {String} action
         * @param {Number} converterIndex
         */
        , cancelConverter: function (action, converterIndex) {
            $("#converter-edit-inside").remove();
            if (action == "add") {
                $("#show-converter-add-btn").show();
            } else {
                $("#converters-item-" + converterIndex).show();
            }
        }
        /**
         * Обработчик события input на ссылке filterUnitInpu
         * @param {JQueryEventObject} event
         */
        , filterUnitInputOnInput: function(event) {
            var value = event.currentTarget.value.trim();
            this.activateOrDeactivateConverters(value != "");
            //
            $("#UnitConverterIdForCatalog option[value=0]").text(value);
            $("#UnitConverterIdForProduct option[value=0]").text(value);
        }
        /**
         * Активируем или деактивируем ковертеры
         * @param {Boolean} activated
         */
        , activateOrDeactivateConverters: function(activated) {
            if (activated) {
                $("#converters").show();
            } else {
                $("#converters").hide();
            }
        }
        /* / Работа с конверторами */
        /* Сортировака значений */
        /**
         * Сделаем значения сортируемыми
         */
        , makeValuesHolderSortable: function () {
            var filtersObj = this;
            $("#values-holder").sortable({
                cancel: "input,button,a"
                , cursor: "move"
                , update: function (event, ui) {
                    filtersObj.valueHolderChangePosition();
                }
            });
        }
        /**
         * Изменим позиции значений
         */
        , valueHolderChangePosition: function () {
            $(".js-value-holder").each(function (i) {
                $(this)
                    .data("valueIndex", i)
                    .attr("id", "value-holder-" + i)
                    .find(".js-value-id")
                        .attr("id", "Values_" + i + "__Id")
                        .attr("name", "Values[" + i + "].Id")
                .end()
                    .find(".js-value-title")
                        .attr("id", "Values_" + i + "__Title")
                        .attr("name", "Values[" + i + "].Title")
                .end()
                    .find(".js-value-title-en")
                        .attr("id", "Values_" + i + "__TitleIn")
                        .attr("name", "Values[" + i + "].TitleIn")
                .end()
                    .find(".js-value-as-double")
                        .attr("id", "Values_" + i + "__ValueAsDouble")
                        .attr("name", "Values[" + i + "].ValueAsDouble")
            });
        }
        /**
         * Автоматическая сортивка значений
         */
        , sortValueHolders: function () {
            var valuesObj = [];
            $(".js-value-title").each(function (i) {
                valuesObj.push({ value: this.value, index: i });
            });
            for (var i = valuesObj.length - 1; i >= 0; i--)
            {
                for (var j = 0; j < i; j++)
                {
                    if (valuesObj[j].value > valuesObj[j + 1].value)
                    {
                        var tmp = valuesObj[j];
                        valuesObj[j] = valuesObj[j + 1];
                        valuesObj[j + 1] = tmp;
                    }
                }
            }
            for (var i = 0; i < valuesObj.length; i++) {
                $("#value-holder-" + valuesObj[i].index).appendTo("#values-holder");
            }
            this.valueHolderChangePosition();
        }
        /* /Сортировака значений */
        /**
         * Распарсим значение double в независимости от культуры
         * @param {String} str
         * @returns {Number}
         */
        , parseDouble: function (str) {
            if (/[^0-9.,]/.test(str.trim()))
                return 0;
            var v = parseFloat(str.replace(",", ".").trim()).toString().replace(".", ",");
            return isNaN(v.replace(",", ".")) ? 0 : v;
        }
        /**
         * Обработаем и установим значение
         * @param {String} str
         * @param {String} valueIndex
         */
        , setValueDouble: function (str, valueIndex) {
            $("#Values_" + valueIndex + "__ValueAsDouble").val(this.parseDouble(str));
        }
        /**
         * Обработчик события input на элементе valueAsDouble
         * @param {JQueryEventObject} event
         */
        , valueAsDoubleOnBlur: function (event) {
            var el = $(event.currentTarget);
            var valueIndex = el.closest(".js-value-holder").data("valueIndex");
            
            this.setValueDouble(el.val(), valueIndex);
            
        }
        /**
         * Обработчик события change на элементе multipleCheckbox
         * @param {JQueryEventObject} event
         */
        , multipleCheckboxOnChange: function (event) {
            $("form").validate().element('#Multiple');
        }
        /**
        * Получим jquery элемент valueHolder посредством jqueryTmpl
        * @param {Number} valueIndex
        * @param {Object} value
        * @returns {jQueryObject}
        */
        , getValueHolder: function (valueIndex, value) {
            return $("#valueHolderTmpl").tmpl({
                valueIndex: valueIndex
                , value: value || { Id: 0, Title: "", TitleIn: "", ValueAsDouble: 0}
                , validationAttributes: this.validationAttributes
                , isAllowLocalization: common.isAllowLocalization
            });
        }
        /**
         * Отобразим элемент valueHolder в dom дереве
         * @param {Number} valueIndex                 
         * @param {Object} value
         */
        , showValueHolder: function (valueIndex, value) {
            this.getValueHolder(valueIndex, value).appendTo("#values-holder");
            this.setValueHolderEvents(".js-remove-value-holder-btn:eq(" + valueIndex + ")",
                ".js-value-as-double:eq(" + valueIndex + ")", ".js-btn-copy:eq(" + valueIndex + ")",
                ".js-btn-cut:eq(" + valueIndex + ")", ".js-btn-paste:eq(" + valueIndex + ")");
            $("#Values_" + valueIndex + "__Title").focus();
        }
        /**
         * Обработчик события click на элементе addValueHolder
         * @param {JQueryEventObject} event
         */
        , addValueHolderBtnOnClick: function (event) {
            this.showValueHolder($(".js-value-holder").length);
        }
        /**
         * Удалим привязанные значения на удаленном сервере
         * @param {Number} valueIndex
         */
        , deleteBindedValues: function (valueIndex) {
            if (!this.allowExtendedFunctions || this._choosedSectionIds.length == 0) {
                this.removeValueHolder(valueIndex);
                return;
            }

            if (this.denyAjax)
                return;
            this.denyAjax = true;

            var valueId = $("#value-holder-" + valueIndex).data("valueId");

            $.ajax({
                url: this.baseUrl + "Admin/Filters/DeleteBindedValues"
                , data: "valueId=" + valueId + "&allowExtendedFunctions=" + this.allowExtendedFunctions + "&sectionIds=" + this._choosedSectionIds.join(",")
                , type: "POST"
                , context: this
                , success: function () {
                    if (this._choosedSectionIds.length == this.sections.length)
                        this.removeValueHolder(valueIndex);
                    this.denyAjax = false;
                }
                , error: function () {
                    message.error("Не удалось удалить привязанные значения.");
                    this.denyAjax = false;
                }
            });
        }
        /**
         * Удалим valueHolder из dom дерева
         */
        , removeValueHolder: function (valueIndex) {
            $("#value-holder-" + valueIndex).remove();
            this.valueHolderChangePosition();
        }
        /**
         * Обработчик события click на элементе removeValueHolderBtn
         * @param {JQueryEventObject} event
         */
        , removeValueHolderBtnOnClick: function (event) {
            var filtersObj = this;
            var valueIndex = $(event.target).closest(".js-value-holder").data("valueIndex");
            if (this.allowExtendedFunctions)
                // откроем popup для выбора опций
                this.openPopup(this.getChooseSectionsForm(valueIndex, "deleteBindedValues",
                    "Внимаение: действие будет выполнено сразу! Удалятся только ссылки на значение." +
                    "Само значение удалится после сохранения фильтра, если не будет ссылок на текущее значение." +
                    "Если фильтр после не сохранить, то значение останется, а ссылки уже нельзя будет вернуть."),
                    $.proxy(this.setChooseSectionsFormEvents, this));
            else 
                if (parseInt($("#Id").val()) > 0 && parseInt($("#value-holder-" + valueIndex).data("valueId")) > 0) 
                    $.alerts.confirm("Все ссылки связанные с этим значением будут удалены. Вы действительно хотите удалить значение фильтра?", $.noop, function (r) {
                        if (r) {
                            filtersObj.removeValueHolder(valueIndex);
                        }
                    });
               else 
                    this.removeValueHolder(valueIndex);
                
            
        }
        /**
         * Установка событий на элемент valueHolder
         */
        , setValueHolderEvents: function (removeValueHolderBtnSelector, valueAsDoubleInputSelector, copyValueBtnSelector, cutValueBtnSelector, pasteValueBtnSelector) {
            $(removeValueHolderBtnSelector).on("click", $.proxy(this.removeValueHolderBtnOnClick, this));
            $(valueAsDoubleInputSelector).on("blur", $.proxy(this.valueAsDoubleOnBlur, this));
            $(copyValueBtnSelector).on("click", $.proxy(this.copyBtnOnClick, this));
            $(cutValueBtnSelector).on("click", $.proxy(this.cutBtnOnClick, this));
            $(pasteValueBtnSelector).on("click", $.proxy(this.pasteBtnOnClick, this));
        }
        /* Работа с объединением значений */
        /**
         * Обработчик события click на элементе cutBtn
         * @param {JQueryEventObject} event
         */
        , cutBtnOnClick: function (event) {
            if ($(event.currentTarget).closest(".merge-btns-active").length > 0) {
                // деактивируем кнопку 'cut'
                this.deactivateCutBtn();
            } else {
                var valueIndex = $(event.currentTarget).closest(".js-value-holder").data("valueIndex")
                if (this.allowExtendedFunctions)
                    // откроем popup для выбора опций
                    this.openPopup(this.getChooseSectionsForm(valueIndex, "activateCutBtn", ""),
                        $.proxy(this.setChooseSectionsFormEvents, this));
                else 
                    // активируем кнопку 'cut'
                    this.activateCutBtn(valueIndex);
            }
        }
        /**
         * Активируем кнопку cutBtn  
         * @param {JQueryEventObject} event
         * @param {Number} valueIndex
         */
        , activateCutBtn: function (valueIndex) {
            $("#value-holder-" + valueIndex).find(".merge-btns").addClass("merge-btns-active");
            $("#values-holder").addClass("merge-btns-wait");
        }
        /**
         * Деактивируем кнопку cutBtn  
         * @param {JQueryEventObject} event
         */
        , deactivateCutBtn: function () {
            $(".merge-btns-active").removeClass("merge-btns-active");
            $(".merge-btns-wait").removeClass("merge-btns-wait");
        }
        /**
         * Объединим значения
         * @param {Number} mainValueId
         */
        , pasteAction: function (mainValueId) {
            if (this.allowExtendedFunctions && this._choosedSectionIds.length == 0)
                return;

            if (this.denyAjax)
                return;
            this.denyAjax = true;

            var valueIndexForDelete = $(".merge-btns-active").closest(".js-value-holder").data("valueIndex");
            var valueId = $(".merge-btns-active").closest(".js-value-holder").data("valueId");

            $.ajax({
                url: this.baseUrl + "Admin/Filters/MergeBindedValues"
                , data: "valueId=" + valueId + "&mainValueId=" + mainValueId + "&allowExtendedFunctions=" + this.allowExtendedFunctions + "&sectionIds=" + this._choosedSectionIds.join(",")
                , type: "POST"
                , context: this
                , success: function () {
                    this.deactivateCutBtn();
                    if (!this.allowExtendedFunctions || (this.allowExtendedFunctions && this._choosedSectionIds.length == this.sections.length))
                        this.removeValueHolder(valueIndexForDelete);
                    this.denyAjax = false;
                }
                , error: function () {
                    message.error("Не удалось объединить значения.");
                    this.denyAjax = false;
                }
            });
        }
        /**
         * Обработчик события click на элементе pasteBtn
         * @param {JQueryEventObject} event
         */
        , pasteBtnOnClick: function (event) {
            var mainValueId = $(event.currentTarget).closest(".js-value-holder").data("valueId");
            var filtersObj = this;
            if (parseInt($("#Id").val()) > 0) {
                $.alerts.confirm("<p style='font-size:11px;text-align: justify;width: 500px;'><span class='text-danger'>Внимаение: действие будет выполнено сразу! Объединятся только ссылки на значение." +
                    "Объединяемое значение удалится после сохранения фильтра, если не будет ссылок на данное значение." + 
                    "Если фильтр после не сохранить, то значение останется, а ссылки уже нельзя будет вернуть." +
                    "</span> Вы действительно хотите объединить значение фильтра?</p>", $.noop, function (r) {
                    if (r) {
                        filtersObj.pasteAction(mainValueId);
                    }
                });
            } else {
                this.pasteAction(mainValueId);
            }
        }
        /* /Работа с объединением значений */
        //--
        /* Работа с копированием значений */
        /**
         * Обработчик события click на элементе copyBtn
         * @param {JQueryEventObject} event
         */
        , copyBtnOnClick: function (event) {

            if ($("#Multiple").val() == "False") {
                message.warning("Нельзя копировать значения, так как фильтр не позволяет множественное заполнение.");
                return;
            }
            var filtersObj = this;
            var el = $(event.currentTarget).closest(".js-value-holder");
            var valueIndex = el.data("valueIndex");
            var valueId = el.data("valueId");
            if (parseInt(valueId) === 0)
                // покажем сразу копируемое значение
                this.showValueHolder($(".js-value-holder").length, {
                    Id: 0
                    , Title: $("#Values_" + valueIndex + "__Title").val().split(" - copy ")[0] + " - copy " + new Date().getTime()
                    , TitleIn: $("#Values_" + valueIndex + "__TitleIn").val()
                    , ValueAsDouble: $("#Values_" + valueIndex + "__ValueAsDouble").val()
                });
            else if (this.allowExtendedFunctions)
                // откроем popup для выбора опций
                this.openPopup(this.getChooseSectionsForm(valueIndex, "copyBindedValues",
                    "Внимаение: действие будет выполнено сразу! Скопируются сразу   и ссылки, и само значение."),
                    $.proxy(this.setChooseSectionsFormEvents, this));
            else 
                $.alerts.confirm("<span class='text-danger'>Внимаение: действие будет выполнено сразу!</span> Все ссылки связанные с копируемым значением так же будут скопированы. Вы действительно хотите скопировать значение фильтра?", $.noop, function (r) {
                    if (r) {
                        filtersObj.copyBindedValues(valueIndex);
                    }
                });
        }
        /**
         * Копируем значения
         * @param {Number} valueIndex
         */
        , copyBindedValues: function (valueIndex) {

            if (this.allowExtendedFunctions && this._choosedSectionIds.length == 0) {
                // покажем сразу копируемое значение
                this.showValueHolder($(".js-value-holder").length, {
                    Id: 0
                    , Title: $("#Values_" + valueIndex + "__Title").val().split(" - copy ")[0] + " - copy " + new Date().getTime()
                    , TitleIn: $("#Values_" + valueIndex + "__TitleIn").val()
                    , ValueAsDouble: $("#Values_" + valueIndex + "__ValueAsDouble").val()
                });
                return;
            }

            if (this.denyAjax)
                return;
            this.denyAjax = true;

            var valueId = $("#value-holder-" + valueIndex).data("valueId");

            $.ajax({
                url: this.baseUrl + "Admin/Filters/CopyBindedValues"
                , data: "valueId=" + valueId + "&allowExtendedFunctions=" + this.allowExtendedFunctions + "&sectionIds=" + this._choosedSectionIds.join(",")
                , type: "POST"
                , dataType: "json"
                , context: this
                , success: function (value) {
                    this.showValueHolder($(".js-value-holder").length, value);
                    this.denyAjax = false;
                }
                , error: function (xhr, state) {
                    if (xhr.status == 400)
                        message.error(xhr.responseText);
                    else 
                        message.error("Не удалось скопировать значения.");
                    this.denyAjax = false;
                }
            });
        }
        /* /Работа с копированием значений */
        //
        // ----
        //
        /* Работа с расширенными функциями */
        /**
         * Обработчик события click на элементе onExtendedFunctionsBtn
         * @param {JQueryEventObject} event
         */
        , onExtendedFunctionsBtnOnClick: function (event) {
            this.allowExtendedFunctions = true;
            $(event.currentTarget).addClass("active").siblings().removeClass("active");
        }
        /**
         * Обработчик события click на элементе offExtendedFunctionsBtn
         * @param {JQueryEventObject} event
         */
        , offExtendedFunctionsBtnOnClick: function (event) {
            this.allowExtendedFunctions = false;
            $(event.currentTarget).addClass("active").siblings().removeClass("active");
            this._choosedSectionIds.length = 0;
        }
        /**
        * Получим jquery элемент chooseSectionsForm посредством jqueryTmpl
        * @param {Object} state
        * @param {String} action
        * @returns {jQueryObject}
        */
        , getChooseSectionsForm: function (state, action, warningMessage) {
            return $("#chooseSectionsFormTmpl").tmpl({
                state: state
                , action: action
                , warningMessage: warningMessage
                , sections: this.sections
            });
        }
        /**
         * Установка событий на форму chooseSectionsForm 
         */
        , setChooseSectionsFormEvents: function () {
            $("#applyChoosedSectionsBtn").on("click", $.proxy(this.applyChoosedSectionsBtnOnClick, this));
            $("#cancelChoosedSectionsBtn").on("click", $.proxy(this.cancelChoosedSectionsBtnOnClick, this));
        }
        /**
         * Обработчик события click на кнопке applyChoosedSectionsBtn
         * @param {JQueryEventObject} event
         */
        , applyChoosedSectionsBtnOnClick: function (event) {
            this._choosedSectionIds.length = 0;
            var obj = this;
            $(".js-choose-sections-item:checked").each(function () {
                obj._choosedSectionIds.push(this.value);
            });
            var action = $("#chooseSectionsForm").data("action");
            var state = $("#chooseSectionsForm").data("state");
            console.log(state)
            this[action](state);

            this.closePopup();
        }
        /**
         * Обработчик события click на кнопке cancelChoosedSectionsBtn
         * @param {JQueryEventObject} event
         */
        , cancelChoosedSectionsBtnOnClick: function (event) {
            this.closePopup();
        }
        /* / Работа с расширенными функциями */
        /**
         * Копируем фильтр
         */
        , copyFilter: function (filterId) {
            
        }
        /**
         * Обработчик события click на кнопке copyFilterBtn
         * @param {JQueryEventObject} event
         */
        , copyFilterBtnOnClick: function (event) {
            var filterId = $(event.currentTarget).data("filterId");
            this.openPopup(this.getChooseSectionsForm(filterId, "copyFilter"), $.proxy(this.setChooseSectionsFormEvents, this));
        }
        /**
         * Открытие popup-а
         * @param {jQueryObject} content
         * @param {Fuction} callback
         */
        , openPopup: function (content, callback) {
            this._choosedSectionIds.length = 0;
            this.deactivateCutBtn();
            //
            var obj = this;
            $.fancybox.open({
                content: content,
                fitToView: false,
                closeBtn: false,
                modal: true,
                afterShow: callback,
                beforeClose: $.noop,
                helpers: {
                    overlay: {
                        css: {
                            'background': 'rgba(20, 20, 20, 0.5)'
                        }
                    }
                }
            });
        }
        /**
         * Закрытие popup-а
         */
        , closePopup: function () {
            $.fancybox.close();
        }
    });
    return Filters;
});