﻿/**
 * Модуль содержит свойства и методы по работе со страницми    
 * @author Ruslan Rakhmankulov
 */
define("Admin/Pages", ["jquery", "common", "globalize", "tinymce", "jqueryUnobtrusive", "jqueryTmpl", "text!Admin/CommonTmpl.htm", "jqueryToastmessage", "jqueryAlerts"], function ($, common, Globalize, tinymce) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Pages() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;

    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Создаем протитип из родительского конструктора или берем прототип по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(Pages.prototype/* = new ParentClass()*/, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            baseUrl: common.baseUrl
            , culture: common.culture || "ru"
            , denySubmit: false // состояние, которое запрещает повторную отправку формы
        }
        /**
         * Метод вызывается при создании экземпляра класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(true, this/*, ParenClass.defaults*/, this.defaults, options);

            /* ParenClass.init.apply(this, arguments); */

            // устанавливаем текущую культуру
            Globalize.culture(this.culture);
        }
        /**
         * Навешиваем события для формы events
         */
        , setPageFormEvents: function () {
            $("#save-page-btn").on("click", $.proxy(this.savePageBtnOnClick, this));
            $("#delete-page-btn").on("click", $.proxy(this.deletePageBtnOnClick, this));

            common.tinymce({
                plugin_preview_width: 710,
                folder: "Pages"
            });

            
        }
        /**
         * Событие действия на кнопке save-page-btn
         * @param {JQueryEventObject} event
         */
        , savePageBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            var formIsValid = $("#page-form").valid();
            if (formIsValid) {
                this.denySubmit = true;
                $("#page-form").submit();
            }
            return;
        }
        /**
         * Событие действия на кнопке delete-page-btn
         * @param {JQueryEventObject} event
         */
        , deletePageBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            $.alerts.confirm("Вы действительно хотите удалить страницу?", $.noop, function (r) {
                if (r) {
                    this.denySubmit = true;
                    var action = $(event.currentTarget).data("href");
                    $(event.currentTarget).closest("form").attr("action", action).submit();
                }
            });
        }
    });
    return Pages;
});