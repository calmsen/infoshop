﻿/**
 * Модуль содержит свойства и методы по работе с настройками
 * @author Ruslan Rakhmankulov
 */
define("Admin/Options", ["jquery", "common", "globalize", "jqueryUnobtrusive", "jqueryToastmessage", "jqueryAlerts"], function ($, common, Globalize) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Options() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(Options.prototype, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            baseUrl: common.baseUrl
            , denyAjax: false // состояние, которое запрещает повторную отправку запроса аякс
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(this, this.defaults, options);
            $(".js-save-option-btn").on("click", $.proxy(this.saveOptionBtnOnClick, this));
        }

        /**
         * Обработчик события click на кнопке saveOptionBtnOnClick
         * @param {JQueryEventObject} event
         */
        , saveOptionBtnOnClick: function (event) {
            if (this.denyAjax)
                return;
            this.denyAjax = true;

            var item = $(event.currentTarget).closest("tr");
            var name = item.find("input[name=Name]").val();
            var value = item.find("input[name=Value]").val();
            $.ajax({
                url: this.baseUrl + "Admin/Options/Update"
                , data: {
                    name: name
                    , value: value
                }
                , type: "POST"
                , context: this
                , success: function (data) {
                    message.success("Опция успешна сохранена.");
                    this.denyAjax = false;
                }
                , error: function (xhr, state) {
                    if (xhr.status == 400)
                        message.error(xhr.responseText);
                    else
                        message.error("Не удалось сохранить опцию.");
                    this.denyAjax = false;
                }
            })
        }
    });
    return Options;
});