﻿/**
 * Модуль содержит свойства и методы по работе с настройками картинок
 * @author Ruslan Rakhmankulov
 */
define("Admin/ImageSettings", ["jquery", "common", "ajaxfileupload", "jqueryTmpl", "text!Admin/CommonTmpl.htm", "jqueryToastmessage", "fancybox"], function ($, common) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function ImageSettings() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(ImageSettings.prototype, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            baseUrl: common.baseUrl
            , st0SiteHttpHost: common.st0SiteHttpHost
            , culture: common.culture || "ru"
            , folder: ""
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(true, this, this.defaults, options);
        }
        /**
         * Установка событий для формы
         */
        , setImageSettingsEvents: function () {
            $("form").on("click", ".js-open-image-settings-btn", $.proxy(this.openImageSettingsBtnOnClick, this));
        }
        /**
         * Обработчик события click на кнопке openImageSettingsBtn
         * @param {JQueryEventObject} event
         */
        , openImageSettingsBtnOnClick: function (event) {
            common.lock("openImageSettings", function () {
                var imageId = $(event.currentTarget).data("imageId");
                this.openImageSettings(imageId).always(function() {
                    common.unlock("openImageSettings");
                });
            }, this);             
        }
        /**
         * Откроем окно с настройками
         * @param {Number} imageId
         */
        , openImageSettings: function (imageId) {
            return this.getImageSettingsForm(imageId).wait(function (el) {
                this.openPopup(el);
            }, function() {
                message.error("Произошла ошибка на сервере.");
            }, this);
            
        }
        /**
         * Открытие popup-а
         * @param {jQueryObject} content
         */
        , openPopup: function (content) {
            var obj = this;
            $.fancybox.open({
                content: content,
                fitToView: false,
                //scrolling: "no",
                afterShow: function () {
                    var imageId = content.data("imageId");
                    obj.setImageSettingsFormEvents(imageId);
                },
                beforeClose: $.noop
            });
        }
        /**
         * Закрытие popup-а
         */
        , closePopup: function () {
            $.fancybox.close();
        }
        /**
         * Получим данные о картинке из удаленного сервера
         * @param {Number} imageId   
         * @returns {jQueryXhr}
         */
        , getImageData: function (imageId) {    
            return $.ajax({
                url: this.baseUrl + "Admin/Images/Edit/" + imageId
                , data: {
                    type: this.folder
                }
                , type: "GET"
                , dataType: "json"
            });
        }
        /**
        * Получим jquery элемент imageSettingsForm посредством jqueryTmpl
        * @param {Number} imageId
        * @returns {jQueryObject}
        */
        , getImageSettingsForm: function (imageId) {
            return this.getImageData(imageId).wait(function (image) {
                var el = $("#imageSettingsFormTmpl").tmpl({
                    image: image
                    , folder: this.folder
                    , st0SiteHttpHost: this.st0SiteHttpHost
                    , curDate: new Date()
                });
                return el;
            }, this);
        }
        /**
         * Установка событий imageSettingsForm 
         * @param {Number} imageId
         */
        , setImageSettingsFormEvents: function (imageId) {
            $("#imageSettingsFormSubmitBtn-" + imageId).on("click", $.proxy(this.imageSettingsFormSubmitBtnOnClick, this));
            $(".js-reload-image-btn").on("change", $.proxy(this.reloadImageBtnOnChange, this));
        }
        /**
         * Обработчик события click на кнопке imageSettingsFormSubmitBtn
         * @param {JQueryEventObject} event
         */
        , imageSettingsFormSubmitBtnOnClick: function (event) {
            event.preventDefault();
            var form = $(event.currentTarget).closest("form");
            this.updateImage(form);
            this.closePopup();
        }
        /**
         * Обновим данные по картинке на удаленном сервере
         * @param {jQueryObject} form
         */
        , updateImage: function (form) {
            $.ajax({
                url: form.attr("action")
                , data: form.serialize()
                , dataType: "json"
                , type: form.attr("method")
            });
        }
        /**
         * Загрузим картинку на удаленный сервер
         * @param {Number} imageId
         * @param {Number} dimension
         */
        , reloadImage: function (imageId, dimension) {
            var uniqPrefixes = [];
            
            var obj = this;

            $.ajaxFileUpload({
                url: this.baseUrl + "Admin/Images/ReloadImage/" + imageId
                , data: "type=" + this.folder + "&dimension=" + dimension
                , fileElementId: "reload-image-btn-" + dimension
                , type: "POST"
                , dataType: "json"
                , success: function (json) {
                    var oldSrc = $("#image-" + dimension).attr("src").split("?")[0];
                    var newSrc = oldSrc + "?" + new Date();
                    $("img[src*='" + oldSrc + "']").attr("src", newSrc);
                }
                , error: function (xhr, status) {
                    message.error(xhr.responseText);
                }
            });
        }
        /**
         * Обработчик события change на элементе reloadImageBtn
         * @param {JQueryEventObject} event
         */
        , reloadImageBtnOnChange: function (event) {
            var el = $(event.currentTarget);
            var imageId = el.data("imageId");
            var dimension = el.data("dimension");
            this.reloadImage(imageId, dimension);
        }
        
    });
    return ImageSettings;
});