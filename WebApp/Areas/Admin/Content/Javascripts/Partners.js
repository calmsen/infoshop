﻿/**
 * Модуль содержит свойства и методы по работе с партнерами
 * @author Ruslan Rakhmankulov
 */
define("Admin/Partners", ["jquery", "common", "globalize", "Admin/MainImage", "jqueryUnobtrusive", "jqueryToastmessage", "jqueryAlerts"], function ($, common, Globalize) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Partners() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(Partners.prototype, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            baseUrl: common.baseUrl
            , culture: common.culture || "ru"
            , denyAjax: false // состояние, которое запрещает повторную отправку запроса аякс
            , denySubmit: false // состояние, которое запрещает повторную отправку формы
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(this, this.defaults, options);
            Globalize.culture(this.culture);
            $("#save-partner-btn").on("click", $.proxy(this.savePartnerBtnOnClick, this));
            $("#delete-partner-btn").on("click", $.proxy(this.deletePartnerBtnOnClick, this));
        }
        /**
         * Обработчик события click на кнопке savePartnerBtn
         * @param {JQueryEventObject} event
         */
        , savePartnerBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            var formEl = $("#partner-form");
            if (!formEl.valid())
                return;
            this.denySubmit = true;
            formEl.submit();
        }
        /**
         * Обработчик события click на кнопке deletePartnerBtn
         * @param {JQueryEventObject} event
         */
        , deletePartnerBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            $.alerts.confirm("Вы действительно хотите удалить партнера?", $.noop, function (r) {
                if (r) {
                    this.denySubmit = true;
                    var action = $(event.currentTarget).data("href");
                    $(event.currentTarget).closest("form").attr("action", action).submit();
                }
            });
        }
    });
    return Partners;
});