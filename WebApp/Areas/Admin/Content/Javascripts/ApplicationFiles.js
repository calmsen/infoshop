﻿/**
 * Модуль содержит свойства и методы по работе с файлами приложения
 * @author Ruslan Rakhmankulov
 */
define("Admin/ApplicationFiles", ["jquery", "common", "jqueryToastmessage", "ajaxfileupload"], function ($, common) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function ApplicationFiles() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(ApplicationFiles.prototype, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            baseUrl: common.baseUrl
            , denyAjax: false // состояние, которое запрещает повторную отправку запроса аякс
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(this, this.defaults, options);
            // установим события для полей file input
            $(".js-load-file-inp").on("change", $.proxy(this.loadFileInpOnChange, this));
            // -- -- --
        }
        /**
         * Проверим является ли файл экселевским
         * @param {jQuery} inpEl - jquery элемент file input
         * @returns {Boolean}
         */
        , checkExcelFile: function (inpEl) {
            var fileName = inpEl.val().split("\\").pop();
            if (fileName.indexOf(".xls") == -1) {
                common.error("Файл не является excel файлом.");
                return false;
            }
            return true;
        }
        /**
         * Отображаем элемент процесса загрузки
         * @param {jQuery} loadingEl - jquery элемент 
         * @param {String} inpElId
         * @param {String} callbackUrl - название функции, которая проинициировала вызов
         */
        , showLoadingEl: function (loadingEl, inpElId, callbackUrl) {
            $("#" + inpElId).hide();
            loadingEl.show();
            var obj = this;
            common.addEventHandler("ApplicationFiles::" + callbackUrl, function () {
                loadingEl.hide();
                $("#" + inpElId).show();  // обязательно находить элемент заново, так как ajaxfileupload удаляет оригинальный элемент
            });
        }
        /**
         * Обработчик события change на file input-е
         * @param {JQueryEventObject} event
         */
        , loadFileInpOnChange: function (event) {
            var inpEl = $(event.currentTarget);

            if (!this.checkExcelFile(inpEl))
                return;

            var loadingEl = inpEl.closest(".form-group").find(".loading");
            if (loadingEl.is(":hidden")) {
                var inpElId = inpEl.attr("id");
                var callbackUrl = inpEl.closest(".form-group").data("callbackUrl");
                this.showLoadingEl(loadingEl, inpElId, callbackUrl);
                this.loadFile(inpElId, callbackUrl);
            }
        }
        /**
         * Загружаем файл на удаленный сервер
         * @param {String} inpElId
         * @param {String} callbackUrl
         */
        , loadFile: function (inpElId, callbackUrl) {
            var obj = this;
            $.ajaxFileUpload({
                url: this.baseUrl + "Admin/Files/LoadFile?folder=App"
                , fileElementId: inpElId
                , type: "POST"
                , dataType: "json"
                , success: function (file) {
                    obj.callbackRequest(file.fileName, callbackUrl);
                }
                , error: function (xhr, status) {
                    message.error("Не удалось загрузить файл.");
                    common.executeEventHandlers("ApplicationFiles::" + callbackUrl);
                }
            });
        }
        /**
         * Парсим добавляем на удаленный сервер
         * @param {String} fileName
         * @param {String} callbackUrl
         */
        , callbackRequest: function (fileName, callbackUrl) {
            $.ajax({
                url: this.baseUrl + callbackUrl
                , data: {
                    fileName: fileName
                }
                , type: "post"
                , dataType: "json"
                , success: function () {
                    message.success("Файл успешно загружен и распарсен.");
                    common.executeEventHandlers("ApplicationFiles::" + callbackUrl);
                }
                , error: function (xhr, status) {
                    message.error("Не удалось распарсить файл.");
                    common.executeEventHandlers("ApplicationFiles::" + callbackUrl);
                }
            })
        }
    });
    return ApplicationFiles;
});;