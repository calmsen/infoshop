﻿/**
 * Модуль содержит свойства и методы по работе с новостями
 * @author Ruslan Rakhmankulov
 */
define("Admin/ProductTypes", ["jquery", "common", "jqueryUnobtrusive", "jqueryAlerts"], function ($, common) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function ProductTypes() {
        this.init.apply(this, arguments);
    }
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(ProductTypes.prototype, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            baseUrl: common.baseUrl
            , denySubmit: false // состояние, которое запрещает повторную отправку формы
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(this, this.defaults, options);
        }
        /**
         * Установка событий на форму 
         */
        , setProductTypeFormEvents: function () {
            $("#saveProductTypeBtn").on("click", $.proxy(this.saveProductTypeBtnOnClick, this));
            $("#deleteProductTypeBtn").on("click", $.proxy(this.deleteProductTypeBtnOnClick, this));
        }
        /**
         * Обработчик события click на кнопке submit формы
         * @param {JQueryEventObject} event
         */
        , saveProductTypesBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            var formIsValid = $("#productTypesForm").valid();
            if (formIsValid) {
                this.denySubmit = true;
                $("#productTypesForm").submit();
            }
            return;
        }
        /**
         * Обработчик события click на ссылке deleteArticleBtn
         * @param {JQueryEventObject} event
         */
        , deleteProductTypeBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            $.alerts.confirm("Вы действительно хотите удалить изделие?", $.noop, function (r) {
                if (r) {
                    this.denySubmit = true;
                    var action = $(event.currentTarget).data("href");
                    $(event.currentTarget).closest("form").attr("action", action).submit();
                }
            });
        }
    });
    return ProductTypes;
});