﻿/**
 * Модуль содержит свойства и методы по работе с обзорами    
 * @author Ruslan Rakhmankulov
 */
define("Admin/Posts", ["jquery", "common", "globalize", "tinymce", "Admin/MainImage", "jqueryUnobtrusive", "jqueryTmpl", "text!Admin/CommonTmpl.htm", "jqueryToastmessage", "jqueryAlerts"], function ($, common, Globalize, tinymce) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Posts() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;

    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Создаем протитип из родительского конструктора или берем прототип по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(Posts.prototype/* = new ParentClass()*/, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            baseUrl: common.baseUrl
            , culture: common.culture || "ru"
            , denySubmit: false // состояние, которое запрещает повторную отправку формы
        }
        /**
         * Метод вызывается при создании экземпляра класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(true, this/*, ParenClass.defaults*/, this.defaults, options);

            /* ParenClass.init.apply(this, arguments); */

            // устанавливаем текущую культуру
            Globalize.culture(this.culture);
        }
        /**
         * Навешиваем события для формы events
         */
        , setPostFormEvents: function () {
            $("#save-post-btn").on("click", $.proxy(this.savePostBtnOnClick, this));
            $("#delete-post-btn").on("click", $.proxy(this.deletePostBtnOnClick, this));

            common.tinymce({
                plugin_preview_width: 710,
                folder: "Posts"
            });

            $("#bind-product-input").on("input", $.proxy(this.searchProductInputOnInput, this));
            $("#bind-product-input").on("blur", $.proxy(this.searchProductInputOnBlur, this));

            var productsObj = this;
            $("#bind-product-input").autocomplete({
                source: this.baseUrl + "Admin/Products/AutocompleteSearchProducts",
                minLength: 2,
                select: function (event, ui) {
                    var el = $(this);
                    el.data("productId", ui.item.id);
                    productsObj.searchProductInputValidate(el);
                }
            });
            $("#bind-product-btn").on("click", $.proxy(this.bindProductBtnOnClick, this));
            this.setBindingProductEvents(".js-unbind-product-btn");
        }
        /**
         * Cобытие ввода на элементе search-product-input 
         * @param {JQueryEventObject} event
         */
        , searchProductInputOnInput: function (event) {
            $(event.currentTarget).data("productId", "0");
        }
        /**
         * Cобытие потери фокуса на элементе search-product-input 
         * @param {JQueryEventObject} event
         */
        , searchProductInputOnBlur: function (event) {
            var el = $(event.currentTarget);
            this.searchProductInputValidate(el);
        }
        /**
         * Cобытие поле ввода элемента search-product-input 
         * @param {JQueryObject} el
         * @returns {Boolean}
         */
        , searchProductInputValidate: function (el) {
            if (el.data("productId") == "0") {
                el.css("border-color", "#a94442");
                return false;
            } else {
                el.css("border-color", "");
                return true;
            }
        }
        /**
         * Cобытие действия на кнопке bind-product-btn
         * @param {JQueryEventObject} event
         */
        , bindProductBtnOnClick: function (event) {
            this.bindProduct();
        }
        /**
         * Привяжем товар к обзору
         */
        , bindProduct: function () {
            var el = $("#bind-product-input");

            if (!this.searchProductInputValidate(el))
                return;

            var productId = el.data("productId");
            var productTitle = el.val();

            this.showBindingProduct({
                id: productId
                    , title: productTitle
            });

            $("#bind-product-input").data("productId", "0");
            $("#bind-product-input").val("");
        }
        /**
         * Получим dom элемент привязанного товара к обзору
         * @param {Number} index порядковый номер привязанного товара к обзору
         * @param {Object} product данные товара
         * @returns {JQueryObject}
         */
        , getBindingProduct: function (index, product) {
            return $("#bindingProductTmpl").tmpl({
                index: index
              , product: product
            });
        }
        /**
         * Навешиваем события на привязанный товар к обзору
         * @param {String} unbindProductBtnSelector
         */
        , setBindingProductEvents: function (unbindProductBtnSelector) {
            $(unbindProductBtnSelector).on("click", $.proxy(this.unbindProductBtnOnClick, this));
        }
        /**
         * Показываем привязанный товар к обзору
         * @param {Object} product данные товара
         */
        , showBindingProduct: function (product) {
            if ($("#binding-product-" + product.id).length > 0)
                return;

            this.getBindingProduct($(".js-binding-product").length, product).appendTo("#binding-products");
            this.setBindingProductEvents("#binding-product-" + product.id + " .js-unbind-product-btn");
        }
        /**
         * Cобытие действия на кнопке unbind-product-btn
         * @param {JQueryEventObject} event
         */
        , unbindProductBtnOnClick: function (event) {
            var productId = $(event.currentTarget).closest(".js-binding-product").data("productId");
            this.unbindProduct(productId);
        }
        /**
         * Отвязываем товар от обзора
         * @param {Number} productId данные товара
         */
        , unbindProduct: function (productId) {
            $("#binding-product-" + productId).remove();
            this.updateBindingProductPositions();
        }
        /**
         * Обновляем позиции всех привязанных товаров к обзору
         */
        , updateBindingProductPositions: function () {
            $(".js-binding-product").each(function (i) {
                $(this).find(".js-binding-product-input")
                    .attr("id", "ProductIds_" + i + "_")
                    .attr("name", "ProductIds[" + i + "]")
            });
        }
        /**
         * Событие действия на кнопке save-post-btn
         * @param {JQueryEventObject} event
         */
        , savePostBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            var formIsValid = $("#post-form").valid();
            if (formIsValid) {
                this.denySubmit = true;
                $("#post-form").submit();
            }
            return;
        }
        /**
         * Событие действия на кнопке delete-post-btn
         * @param {JQueryEventObject} event
         */
        , deletePostBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            $.alerts.confirm("Вы действительно хотите удалить статью?", $.noop, function (r) {
                if (r) {
                    this.denySubmit = true;
                    var action = $(event.currentTarget).data("href");
                    $(event.currentTarget).closest("form").attr("action", action).submit();
                }
            });
        }
    });
    return Posts;
});