﻿/**
 * Модуль содержит свойства и методы по работе с ресурсами
 * @author Ruslan Rakhmankulov
 */
define("Admin/Resources", ["jquery", "common", "globalize", "jqueryUnobtrusive", "jqueryToastmessage", "jqueryAlerts"], function ($, common, Globalize) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Resources() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(Resources.prototype, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            baseUrl: common.baseUrl
            , denyAjax: false // состояние, которое запрещает повторную отправку запроса аякс
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(this, this.defaults, options);
            $(".js-save-resource-btn").on("click", $.proxy(this.saveResourceBtnOnClick, this));
        }

        /**
         * Обработчик события click на кнопке saveResourceBtnOnClick
         * @param {JQueryEventObject} event
         */
        , saveResourceBtnOnClick: function (event) {
            if (this.denyAjax)
                return;
            this.denyAjax = true;

            var item = $(event.currentTarget).closest("tr");
            var key = item.find("input[name=Key]").val();
            var value = item.find("input[name=Value]").val();
            var valueIn = item.find("input[name=ValueIn]").val();
            $.ajax({
                url: this.baseUrl + "Admin/Resources/CreateOrUpdate"
                , data: {
                    Key: key
                    , Value: value
                    , ValueIn: valueIn
                }
                , type: "POST"
                , context: this
                , success: function (value) {
                    message.success("Ресурс успешно сохранен.");
                    this.denyAjax = false;
                }
                , error: function (xhr, state) {
                    if (xhr.status == 400)
                        message.error(xhr.responseText);
                    else
                        message.error("Не удалось сохранить ресурс.");
                    this.denyAjax = false;
                }
            })
        }
    });
    return Resources;
});