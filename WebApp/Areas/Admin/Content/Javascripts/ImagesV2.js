﻿/**
 * Модуль содержит свойства и методы по работе с загрузкой картинок
 * @author Ruslan Rakhmankulov
 */
define("Admin/ImagesV2", ["jquery", "common", "globalize", "Admin/ImageSettings", "jqueryUnobtrusive", "ajaxfileupload", "jqueryTmpl", "text!Admin/CommonTmpl.htm", "jqueryToastmessage", "jqueryUi"], function ($, common, Globalize, ImageSettings) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function ImagesV2() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;
    var loadImageCounter = 0 // для генерирования uniqPrefix
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(ImagesV2.prototype, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            baseUrl: common.baseUrl
            , st0SiteHttpHost: common.st0SiteHttpHost
            , culture: common.culture || "ru"
            , mainImageIdPropName: "MainImageId"
            , imagesIdsPropName: "ImagesIds"
            , showImageSettingsBtn: true
            , denyAjax: false // состояние, которое запрещает повторную отправку запроса аякс
            , folder: ""
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(true, this, this.defaults, options);
            Globalize.culture(this.culture);

        }
        /**
         * Установка событий на форму
         */
        , setImagesFormEvents: function () {

            this.imageSettingsObj = new ImageSettings({
                baseUrl: this.baseUrl
                , st0SiteHttpHost: this.st0SiteHttpHost
                , folder: this.folder
            });
            //
            this.setAttachImagesBtnEvents();
            //
            this.setImageBtnsEvents();
            this.setFileEvents();
            $("#image-url-input").on("input", $.proxy(this.imageUrlInputOnInput, this));
            $("#upload-image-btn").on("click", $.proxy(this.uploadImageBtnOnClick, this));
            this.makeImagesHolderIsSortable();
        }
        /**
         * Установка событий для прикрепления картинок. 
         * В данный момент метод использует только для работы с обратной связью
         */
        , setAttachImagesBtnEvents: function () {
            var attachImagesBtn = $("#attach-images-btn");
            if (attachImagesBtn.length == 0)
                return;
            //
            var imagesFormGroupEl = $("#images-form-group");
            attachImagesBtn.on("click", function () {
                if (imagesFormGroupEl.is(":hidden"))
                    imagesFormGroupEl.show();
                else
                    imagesFormGroupEl.hide();
            });
            //
            var updateFeedbackImagesBtn = $("#feedback-images-form button").closest(".form-group");
            attachImagesBtn.on("click", function () {
                if (updateFeedbackImagesBtn.is(":hidden"))
                    updateFeedbackImagesBtn.show();
                else
                    updateFeedbackImagesBtn.hide();
            });
        }
        /**
         * Сделаем картинки сортируемыми
         */
        , makeImagesHolderIsSortable: function () {
            var obj = this;
            $(".js-images-holder").sortable({
                items: ".js-image"
                , cancel: ".caption"
                , update: function (event, ui) {
                    obj.changeImagePositions();
                }
            });
        }
        /**
         * Изменим позиции картинок
         */
        , changeImagePositions: function () {
            var obj = this;
            $(".js-image input").each(function (i, v) {
                this.name = obj.imagesIdsPropName + "[" + i + "].Value";
                this.id = obj.imagesIdsPropName + "_" + i + "__Value";
            });
        }
        /**
         * Удалим картинку из dom дерева
         * @param {Number} imageId
         */
        , removeImage: function (imageId) {
            $("#image-" + imageId).remove();
            //
            if (this.mainImageIdPropName != "null" && $("#" + this.mainImageIdPropName).val() == imageId) {
                var mainImageId = $(".js-image").data("imageId");
                this.setMainImage(mainImageId);
            }
            //
            this.changeImagePositions();
        }
        /**
         * Обработчик события click на кнопке removeImageBtn
         * @param {JQueryEventObject} event
         */
        , removeImageBtnOnClick: function (event) {
            var imageId = $(event.currentTarget).closest(".js-image").data("imageId");
            this.removeImage(imageId);
        }
        /**
         * Установим главную картинку
         * @param {Number} imageId
         */
        , setMainImage: function (imageId) {
            if (this.mainImageIdPropName == null || this.mainImageIdPropName === "null")
                return;
            imageId = imageId || 0;
            $("#" + this.mainImageIdPropName).val(imageId);
            $(".js-set-main-image-btn").removeClass("active");
            $("#set-main-image-btn-" + imageId).addClass("active");
            $("form").validate().element('#' + this.mainImageIdPropName);
        }
        /**
         * Обработчик события click на кнопке setMainImage
         * @param {JQueryEventObject} event
         */
        , setMainImageOnClick: function (event) {
            var imageId = $(event.currentTarget).closest(".js-image").data("imageId");
            this.setMainImage(imageId);
        }
        /**
         * Установка событий на картинке
         */
        , setImageBtnsEvents: function () {
            $("form").on("click", ".js-remove-image-btn", $.proxy(this.removeImageBtnOnClick, this));
            if (this.mainImageIdPropName != "null")
                $("form").on("click", ".js-set-main-image-btn", $.proxy(this.setMainImageOnClick, this));
            //$("form").on("click", ".js-open-image-settings-btn", $.proxy(this.openImageSettingsBtnOnClick, this));
            this.imageSettingsObj.setImageSettingsEvents();
        }
        /**
         * Отобразить на страницу новый элемент addImageHolder, клонируя старый элемент
         * @param {Number} uniqPrefix
         */
        , showClone: function (uniqPrefix) {
            var holder = $(".js-add-image-holder:last");
            var clone = holder.clone();

            holder.attr("id", "add-image-holder-" + uniqPrefix);
            holder.find(".js-file").attr("id", "file-" + uniqPrefix);
            holder.find(".js-loading").attr("id", "loading-" + uniqPrefix);
            holder.find(".js-add-image-btn").attr("id", "add-image-btn-" + uniqPrefix);

            $("#add-image-btn-" + uniqPrefix).hide();
            $("#file-" + uniqPrefix).hide();
            $("#loading-" + uniqPrefix).show();

            clone.insertAfter("#add-image-holder-" + uniqPrefix);

            this.setFileEvents();
        }
        /**
         * Создадим на удаленном сервере картинку для 1c базы
         * @param {Number} imageId
         */
        , create1cImage: function (imageId) {
            $.ajax({
                url: this.baseUrl + "Admin/Products1c/Create1cImage"
                , data: {
                    imageId: imageId
                    , code: $("#Code").val()
                }
                , type: "POST"
            });
        }
        /**
         * Загрузим картинку на удаленный сервер
         */
        , loadImage: function () {
            var uniqPrefixes = [];
            var files = $(".js-file:last").get(0).files;
            for (var i = 0; i < files.length; i++) {
                var uniqPrefix = loadImageCounter++;
                this.showClone(uniqPrefix);
                uniqPrefixes[i] = uniqPrefix;
            }

            var obj = this;

            $.ajaxFileUpload({
                url: this.baseUrl + "Admin/Images/LoadImages"
                , data: "type=" + this.folder
                , fileElementId: "file-" + uniqPrefixes[0]
                , type: "POST"
                , dataType: "json"
                , success: function (images) {
                    var needSetMainImage = $(".js-image").length == 0;
                    for (var i = 0; i < uniqPrefixes.length; i++) {
                        obj.showImageHolder(images[i].Id, uniqPrefixes[i]);
                    }
                    if (needSetMainImage) {
                        obj.setMainImage(images[0].Id);
                        if (obj.folder == "Products")
                            obj.create1cImage(images[0].Id);

                    }
                }
                , error: function (xhr, status) {
                    for (var i = 0; i < uniqPrefixes.length; i++) {
                        obj.removeImageHolder(uniqPrefixes[i]);
                    }
                    message.error(xhr.responseText);
                }
            });
        }
        /**
        * Получим jquery элемент imageHolder посредством jqueryTmpl
        * @param {Number} imageId
        * @returns {jQueryObject}
        */
        , getImageHolder: function (imageId) {
            return $("#imageTmpl").tmpl({
                imageId: imageId
                , order: $(".js-image").length
                , baseUrl: this.baseUrl
                , st0SiteHttpHost: this.st0SiteHttpHost
                , mainImageIdPropName: this.mainImageIdPropName
                , imagesIdsPropName: this.imagesIdsPropName
                , showImageSettingsBtn: this.showImageSettingsBtn
                , folder: this.folder
            });
        }
        /**
         * Отобразим элемент imageHolder в dom дереве
         * @param {Number} imageId
         * @param {Number} uniqPrefix
         */
        , showImageHolder: function (imageId, uniqPrefix) {
            this.getImageHolder(imageId).replaceAll("#add-image-holder-" + uniqPrefix);
        }
        /**
         * Удалим элемент imageHolder из dom дерева 
         */
        , removeImageHolder: function (uniqPrefix) {
            $("#add-image-holder-" + uniqPrefix).remove();
        }        /**
         * Обработчик события change на элементе file input
         * @param {JQueryEventObject} event
         */
        , fileOnChange: function () {
            this.loadImage();
        }
        /**
         * Установка событий для элемента file input
         */
        , setFileEvents: function () {
            $(".js-file:last").off("change").on("change", $.proxy(this.fileOnChange, this));
        }
        /**
         * Загрузим картинку на удаленный сервер по указанному url
         * @param {String} url
         */
        , loadImageFromUrlOrFtp: function (url) {
            if (url.trim().length == 0) {
                message.error("Укажите url или ftp картинки.");
                return;
            }
            if (url.indexOf("https://") >= 0) {
                message.error("url картинки не должен быть SSL.");
                return;
            }
            if (url.indexOf("http://") == -1 && url.indexOf("ftp://") == -1) {
                message.error("url или ftp картинки должен быть внешним.");
                return;
            }
            if (url.indexOf(this.baseUrl) != -1) {
                message.error("url или ftp картинки должен быть внешним.");
                return;
            }
            if ([".jpg", ".jpeg", ".png", ".bmp", ".gif"].indexOf(url.substring(url.lastIndexOf("."))) == -1) {
                message.error("url или ftp картинки не является изображением.");
                return;
            }
            var uniqPrefix = loadImageCounter++;

            this.showClone(uniqPrefix);

            $("#image-url-input").val("");
            $("#upload-image-btn").addClass("disabled");
            var obj = this;

            $.ajax({
                url: this.baseUrl + "Admin/Images/LoadImageFromUrlOrFtp"
                , data: "url=" + url + "&type=" + this.folder
                , type: "POST"
                , dataType: "json"
                , context: this
                , success: function (image) {
                    obj.showImageHolder(image.Id, uniqPrefix);
                    if ($(".js-image").length == 1) {
                        obj.setMainImage(image.Id);
                    }
                }
                , error: function (xhr, status) {
                    obj.removeImageHolder(uniqPrefix);
                    message.error(xhr.responseText);
                }
            });
        }
        /**
         * Обработчик события click на кнопке uploadImageBtn
         * @param {JQueryEventObject} event
         */
        , uploadImageBtnOnClick: function (event) {
            var url = $("#image-url-input").val();
            this.loadImageFromUrlOrFtp(url);
        }
        /**
         * Проверим url на валидность
         * @param {String} url
         * @returns {Boolean}
         */
        , validateImageUrl: function (url) {
            if (url.trim().length == 0)
                return false;
            if (url.indexOf("http://") == -1)
                return false;
            if (url.indexOf(this.baseUrl) != -1)
                return false;
            if ([".jpg", ".jpeg", ".png", ".bmp", ".gif"].indexOf(url.substring(url.lastIndexOf("."))) == -1)
                return false;
            return true;
        }
        /**
         * Активируем или деактивируем кнопку uploadImageBtn в зависимости от валиднои url
         * @param {String} url
         */
        , ActivateOrDeactivteUploadImageBtn: function (url) {
            if (this.validateImageUrl(url))
                $("#upload-image-btn").removeClass("disabled");
            else
                $("#upload-image-btn").addClass("disabled");
        }
        /**
         * Обработчик события input на элементе imageUrlInput
         * @param {JQueryEventObject} event
         */
        , imageUrlInputOnInput: function (event) {
            var url = $("#image-url-input").val();
            this.ActivateOrDeactivteUploadImageBtn(url);
        }
        /**
         * Удалим все картинки из dom дерева
         */
        , removeAllImages: function () {
            $(".js-image").remove();
            this.setMainImage(0);
        }
        /**
         * Добавим все картинки в dom дерево
         * @param {Array} images 
         */
        , addAllImages: function (images) {
            if (images.length > 0) {
                for (var i in images) {
                    this.getImageHolder(images[i].Id).insertBefore(".js-add-image-holder");
                }
                this.setMainImage(images[0].Id);
            }
        }
        /**
         * Заменим все картинки в dom дереве
         */
        , replaceAllImages: function (images) {
            this.removeAllImages();
            this.addAllImages(images);
        }
    });

    // проиницилизируем модули
    $("[data-module='images']").each(function () {
        var el = $(this);
        new ImagesV2({
            folder: el.data("folder")
            , mainImageIdPropName: el.data("mainImageIdPropName")
            , imagesIdsPropName: el.data("imagesIdsPropName")
            , showImageSettingsBtn: el.data("showImageSettingsBtn")
            , validationAttributes: el.data("validationAttributes")
        })
        .setImagesFormEvents();
    });
    return ImagesV2;
});