﻿/**
 * Модуль содержит свойства и методы по работе с файлами
 * @author Ruslan Rakhmankulov
 */
define("Admin/Files", ["jquery", "common", "globalize", "SelectControl", "jqueryUnobtrusive", "ajaxfileupload", "jqueryTmpl", "text!Admin/CommonTmpl.htm", "jqueryToastmessage", "jqueryAlerts"], function ($, common, Globalize, SelectControl) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Files() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;
    var allowedExts = [".zip", ".rar", ".pdf", ".doc", ".docx", ".rtf", ".txt", ".xls", ".xlsx", ".jpg", ".jpeg", "png"];
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(Files.prototype, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            baseUrl: common.baseUrl
            , st0SiteHttpHost: common.st0SiteHttpHost
            , culture: common.culture || "ru"
            , validationAttributes: {}
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(this, this.defaults, options);
            Globalize.culture(this.culture);
        }
        /**
         * Установим события на форму File
         */
        , setEditFileFormEvents: function () {
            $("#file").on("change", $.proxy(this.fileOnChange, this));
            $("#ExternalLink").on("input", $.proxy(this.fileExternalLinkOnInput, this));
            $(".js-select-file-filter").on("change", $.proxy(this.selectFileFilterOnChange, this));
            this.setSelectFileFilterHolderEvents(".js-remove-select-file-filter-btn");

            $("#saveFileBtn").on("click", $.proxy(this.saveFileBtnOnClick, this));
            $("#deleteFileBtn").on("click", $.proxy(this.deleteFileBtnOnClick, this));
        }

        /**
         * Событие действия на кнопке saveFileBtn
         * @param {JQueryEventObject} event
         */
        , saveFileBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            var formIsValid = $("#fileForm").valid();
            if (formIsValid) {
                this.denySubmit = true;
                $("#fileForm").submit();
            }
            return;
        }
        /**
         * Событие действия на кнопке deleteFileBtn
         * @param {JQueryEventObject} event
         */
        , deleteFileBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            $.alerts.confirm("Вы действительно хотите удалить файл?", $.noop, function (r) {
                if (r) {
                    this.denySubmit = true;
                    var action = $(event.currentTarget).data("href");
                    $(event.currentTarget).closest("form").attr("action", action).submit();
                }
            });
        }
        /**
         * Установка событий на список файлов в админке
         */
        , setFilesListForAdminEvents: function () {
            this.initSectionSelectControl("SectionId", "ProductId");
        }
        /**
         * Инициализация селекта раздела
         * @param {Number} sectionId - ид элемента раздела
         * @param {Number} productId - ид элемента товара
         */
        , initSectionSelectControl: function (sectionId, productId) {
            new SelectControl({
                element: $("#" + sectionId)
                , baseUrl: this.baseUrl
                , dependentSelectElem: $("#" + productId)
                , dependentSelectUri: "Admin/Products/ProductTitlesForSelectList"
                , dependentSelectParam: "sectionId"
            });
        }
        /* Работаем с Multiple Select */

        /**
         * Получим jquery элемент SelectFileFilterHolder посредством jqueryTmpl
         * @param {Object} item
         * @param {Number} index 
         * @returns {jQueryObject}
         */
        , getSelectFileFilterHolder: function (item, index) {
            return $("#selectFileFilterHolderTmpl").tmpl({
                item: item
                , index: index
                , validationAttributes: this.validationAttributes
            });
        }
        /**
         * Установим события на элемент SelectFileFilterHolder
         * @param {String} removeSelectFileFilterBtnSelector - css селектор кнопки удаления 
         */
        , setSelectFileFilterHolderEvents: function (removeSelectFileFilterBtnSelector) {
            $(removeSelectFileFilterBtnSelector).on("click", $.proxy(this.removeSelectFileFilterBtnOnClick, this));
        }
        /**
         * Отобразим элемент SelectFileFilterHolder в dom дереве
         * @param {Object} item
         */
        , showSelectFileFilterHolder: function (item) {
            var holder = $("#select-file-filters-holder");
            var index = holder.find(".js-select-file-filter-holder").length;
            this.getSelectFileFilterHolder(item, index).appendTo(holder);
            this.setSelectFileFilterHolderEvents("#select-file-filter-holder-" + item.Value + " .js-remove-select-file-filter-btn");
        }
        /**
         * Добавим элемент SelectFileFilterHolder в dom дерево
         * @param {String} fileFilter
         */
        , addSelectFileFilter: function (fileFilter) {
            var fileFiltersForSelectList = $("#select-file-filters-holder").data("list");
            var item = this.findFileFiltersForSelectListItem(fileFilter, fileFiltersForSelectList);
            if ($("#select-file-filter-holder-" + item.Value).length == 0) {
                this.showSelectFileFilterHolder(item);
            }
        }
        /**
         * Удалим элемент SelectFileFilterHolder из dom дерева
         * @param {String} fileFilter
         */
        , removeSelectFileFilter: function (fileFilter) {
            $("#select-file-filter-holder-" + fileFilter).remove();
            var fileFiltersHolder = $("#select-file-filters-holder");
            //
            fileFiltersHolder.find("input").each(function (i) {
                $(this)
                    .attr("id", "Filters_" + i + "_")
                    .attr("name", "Filters[" + i + "]");
            });
            //
            var select = $(".js-select-file-filter");
            if (select.val() == fileFilter) {
                var fileFilter = fileFiltersHolder.find(".js-select-file-filter-holder").data("fileFilter") || "";
                select.val(fileFilter);
            }
        }
        /**
         * Найдем данные о фильтре в переданном списке
         * @param {String} fileFilter
         * @param {Array} fileFiltersForSelectList - список с данными о фильтре
         */
        , findFileFiltersForSelectListItem: function (fileFilter, fileFiltersForSelectList) {
            for (var i in fileFiltersForSelectList) {
                if (fileFiltersForSelectList[i].Value == fileFilter) {
                    return fileFiltersForSelectList[i];
                }
            }
            return null;
        }
        /**
         * Обработчик события change на элементе selectFileFilter
         * @param {JQueryEventObject} event
         */
        , selectFileFilterOnChange: function (event) {
            var el = $(event.currentTarget);
            var fileFilter = el.val();
            this.addSelectFileFilter(fileFilter);
        }
        /**
         * Обработчик события click на элементе removeSelectFileFilterBtn
         * @param {JQueryEventObject} event
         */
        , removeSelectFileFilterBtnOnClick: function (event) {
            var el = $(event.currentTarget)
            var fileFilter = el.closest(".js-select-file-filter-holder").data("fileFilter");
            this.removeSelectFileFilter(fileFilter);
        }
        /* / Работает с Multiple Select */

        /**
         * Обработчик события input на элементе fileExternalLink
         * @param {JQueryEventObject} event
         */
        , fileExternalLinkOnInput: function (event) {
            var file = event.currentTarget.value.trim();
            if (file == "")
                return;
            var fileName = file.substring(file.lastIndexOf("/") + 1);
            fileName = fileName.substring(0, fileName.lastIndexOf("."));
            var ext = file.substring(file.lastIndexOf(".")).toLowerCase();
            if (allowedExts.indexOf(ext) == -1) {
                message.error("Файл должен быть с разрешением " + allowedExts.join(", ") + ".");
                return;
            }
            $("#Title").val(decodeURIComponent(fileName));
            $("#Version").val("1.0");

            if (file.indexOf("ftp://") != -1) {
                $.ajax({
                    url: this.baseUrl + "Admin/Files/GetFileSize"
                    , data: "ftpPath=" + encodeURIComponent(file)
                    , type: "GET"
                    , dataType: "json"
                    , success: function (fileSize) {
                        $("#FileSize").val(fileSize);
                    }
                    , error: function () {
                        $("#FileSize").val("");
                    }
                });
            }
            else {
                $("#FileSize").val("");
            }
            
        }
        /**
         * Обработчик события change на элементе file
         * @param {JQueryEventObject} event
         */
        , fileOnChange: function (event) {
            this.loadFile();
        }
        /**
         * Загружаем файл на удаленный сервер
         */
        , loadFile: function () {
            var file = $("#file").val();
            var ext = file.substring(file.lastIndexOf(".")).toLowerCase();
            if (allowedExts.indexOf(ext) == -1) {
                message.error("Файл должен быть с разрешением " + allowedExts.join(", ") + ".");
                return;
            }
            //
            $("#file").hide();
            $("#loading").show();
            var productsObj = this;

            $.ajaxFileUpload({
                url: this.baseUrl + "Admin/Files/LoadFile?folder=Products"
                , fileElementId: "file"
                , type: "POST"
                , dataType: "json"
                , success: function (json) {
                    var fileName = file.substring(file.lastIndexOf("\\") + 1);
                    fileName = fileName.substring(0, fileName.lastIndexOf("."));
                    $("#Title").val(fileName);
                    $("#Version").val("1.0");
                    $("#FileSize").val(json.filesize);
                    $("#loading").hide();
                    $("#file").hide();
                    $("#load-on-success").show();
                    $("#ExternalLink").val(productsObj.st0SiteHttpHost + "AppFiles/Products/" + json.fileName);
                    message.success("Файл загружен");
                }
                , error: function (xhr, status) {
                    message.error(xhr.responseText);
                    $("#loading").hide();
                    $("#file").show();
                }
            });
        }
    });
    return Files;
});