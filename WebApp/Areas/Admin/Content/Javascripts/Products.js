﻿/**
 * Модуль содержит свойства и методы по работе с Товарами
 * @author Ruslan Rakhmankulov
 */
define("Admin/Products", ["jquery", "common", "globalize", "Admin/Sections", "tinymce", "Admin/MainImage", "jqueryUnobtrusive", "ajaxfileupload", "jqueryTmpl", "text!Admin/CommonTmpl.htm", "jqueryToastmessage", "jqueryAlerts", "bootstrapMultiselect"], function ($, common, Globalize, Sections, tinymce) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Products() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;
    var loadImageCounter = 0 // для генерирования uniqPrefix
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(Products.prototype, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            sections: []
            , baseUrl: common.baseUrl
            , culture: common.culture || "ru"
            , denyAjax: false // состояние, которое запрещает повторную отправку запроса аякс
            , validationAttributes: {} // атрибуты для валидации по jquery unobtrusive
            , denySubmit: false // состояние, которое запрещает повторную отправку формы
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(this, this.defaults, options);
            Globalize.culture(this.culture);
        }
        /**
         * Обработчик события click на кнопке submit формы
         * @param {JQueryEventObject} event
         */
        , saveProductBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            var formIsValid = $("#product-form").valid();
            if (formIsValid) {
                this.denySubmit = true;
                $("#product-form").submit();
            }
            return;
        }
        /**
         * Обработчик события click на ссылке deleteProductBtn
         * @param {JQueryEventObject} event
         */
        , deleteProductBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            $.alerts.confirm("Вы действительно хотите удалить товар?", $.noop, function (r) {
                if (r) {
                    this.denySubmit = true;
                    var action = $(event.currentTarget).data("href");
                    $(event.currentTarget).closest("form").attr("action", action).submit();
                }
            });
        }
        /**
         * Копируем атрибуты товара
         */ 
        , copyAttributes: function () {
            var fromProductId = document.getElementById("CopyAttributes.FromProductId").value;
            if (!fromProductId)
                return;
            location.href = this.baseUrl + "Admin/Products/CopyAttributes/" + document.getElementById("Id").value + "?fromProductId=" + fromProductId +
                "&redirectToUrl=" + document.getElementById("redirectToUrl").value;
        }
        /**
         * Копируем картинки
         */
        , copyImages: function () {
            var fromProductId = document.getElementById("CopyImages.FromProductId").value;
            if (!fromProductId)
                return;
            location.href = this.baseUrl + "Admin/Products/CopyImages/" + document.getElementById("Id").value + "?fromProductId=" + fromProductId +
                "&redirectToUrl=" + document.getElementById("redirectToUrl").value;
        }
        // Работаем с сылками на партнеров
        /**
         * Обработчик события click на кнопке showPartnerAddBtn
         * @param {JQueryEventObject} event
         */
        , showPartnerAddBtnOnClick: function (event) {
            this.showPartnerEditInside("add", $(".js-partners-item").length, { Id: "0", Link: "" });
        }
        /**
         * Отобразим элемент partnerEditInside в dom дереве
         * @param {String} action
         * @param {Number} partnerIndex
         * @param {Object} partner
         */
        , showPartnerEditInside: function (action, partnerIndex, partner) {
            // удаляем текущею форму и показываем кнопку вызыющую эту форму
            var el = $("#partner-edit-inside");
            if (el.length > 0) {
                if (el.data("action") == "add")
                    $("#show-partner-add-btn").show();
                else {
                    $("#partners-item-" + el.data("partnerIndex")).show();
                }
                el.remove();
            }
            //
            if (action == "add")
                $("#show-partner-add-btn").hide();
            else
                $("#partners-item-" + partnerIndex).hide();
            this.getPartnerEditInside(action, partnerIndex, partner).appendTo("#partner-edit");
            this.setPartnerEditInsideEvents(action, partnerIndex);
        }
        /**
        * Получим jquery элемент partnerEditInside посредством jqueryTmpl
        * @param {String} action
        * @param {Number} partnerIndex
        * @param {Object} partner
        * @returns {jQueryObject}
        */
        , getPartnerEditInside: function (action, partnerIndex, partner) {
            return $("#partnerEditInsideTmpl").tmpl({
                action: action
                , partnerIndex: partnerIndex
                , partner: partner
                , partnersForSelectList: this.partnersForSelectList
            });
        }
        /**
         * Установим события на элемент partnerEditInside
         * @param {String} action
         * @param {Number} partnerIndex
         */
        , setPartnerEditInsideEvents: function (action, partnerIndex) {
            $("#" + action + "-partner-btn").on("click", $.proxy(this[action + "PartnerBtnOnClick"], this));
            $("#cancel-partner-btn").on("click", $.proxy(this.cancelPartnerBtnOnClick, this));
        }
        /**
         * Обработчик события click на кнопке addPartnerBtn
         * @param {JQueryEventObject} event
         */
        , addPartnerBtnOnClick: function (event) {
            var el = $("#partner-edit-inside");
            this.addPartner(el.data("partnerIndex"));
        }
        /**
         * Добавим партнера
         * @param {Number} partnerIndex
         */
        , addPartner: function (partnerIndex) {
            var partner = {};
            if (!this.validatePartner(partner))
                return;
            this.showPartnersItem("add", partnerIndex, partner);
        }
        /**
         * Обработчик события click на кнопке editPartnerBtn
         * @param {JQueryEventObject} event
         */
        , editPartnerBtnOnClick: function (event) {
            var el = $("#partner-edit-inside");
            this.editPartner(el.data("partnerIndex"));
        }
        /**
         * Сохраним изменения о партнере
         * @param {Number} partnerIndex
         */
        , editPartner: function (partnerIndex) {
            var partner = {};
            if (!this.validatePartner(partner))
                return;
            this.showPartnersItem("edit", partnerIndex, partner);
        }
        /**
         * Проверим на валидность данных о партнере
         * @param {Object} partner
         */
        , validatePartner: function (partner) {
            var id = document.getElementById("PartnerLinkOnProduct.Id").value.trim();
            var link = document.getElementById("PartnerLinkOnProduct.Link").value.trim();
            if (id == "" || link == "")
                return false;
            partner.Id = id;
            partner.Link = link;
            return true;
        }
        /**
         * Отобразим элемент partnersItem в dom дереве
         * @param {String} action
         * @param {Number} partnerIndex
         * @param {Object} partner
         */
        , showPartnersItem: function (action, partnerIndex, partner) {
            $("#partner-edit-inside").remove();
            var el = this.getPartnersItem(partnerIndex, partner);
            var oldEl = $("#partners-item-" + partnerIndex);

            if (oldEl.length > 0)
                el.replaceAll(oldEl);
            else
                el.insertBefore("#show-partner-add-btn");

            this.setPartnersItemEvents(partnerIndex);
            if (action == "add")
                $("#show-partner-add-btn").show();
        }
        /**
        * Получим jquery элемент partnersItem посредством jqueryTmpl
        * @param {Number} partnerIndex
        * @param {Object} partner
        * @returns {jQueryObject}
        */
        , getPartnersItem: function (partnerIndex, partner) {
            var itemTitle = "";
            for (var i in this.partnersForSelectList)
            {
                var item = this.partnersForSelectList[i];
                if (item.Value == partner.Id)
                {
                    itemTitle = item.Text;
                }
            }
            return $("#partnersItemTmpl").tmpl({
                partnerIndex: partnerIndex
                , partner: partner
                , itemTitle: itemTitle
            });
        }
        /**
         * Установим события на элемент partnersItem
         * @param {Number} partnerIndex
         */
        , setPartnersItemEvents: function (partnerIndex) {
            $("#show-partner-edit-btn-" + partnerIndex).on("click", $.proxy(this.showPartnerEditBtnOnClick, this));
            $("#delete-partner-btn-" + partnerIndex).on("click", $.proxy(this.deletePartnerBtnOnClick, this));
        }
        /**
         * Обработчик события click на кнопке deletePartnerBtn
         * @param {JQueryEventObject} event
         */
        , deletePartnerBtnOnClick: function (event) {
            var el = $(event.currentTarget).closest(".js-partners-item");
            this.deletePartner(el.data("partnerIndex"));
        }
        /**
         * Удалим партнера
         * @param {Number} partnerIndex
         */
        , deletePartner: function (partnerIndex) {
            $("#partners-item-" + partnerIndex).remove();
            $(".js-partners-item").each(function (i) {
                var el = $(this);
                el.attr("id", "partners-item-" + i).data("partnerIndex", i);
                el.find(".js-partners-item-id")
                    .attr("id", "PartnersLinksOnProduct_" + i + "__Id")
                    .attr("name", "PartnersLinksOnProduct[" + i + "].Id");
                el.find(".js-partners-item-link")
                    .attr("id", "PartnersLinksOnProduct_" + i + "__Link")
                    .attr("name", "PartnersLinksOnProduct[" + i + "].Link");
                el.find(".js-show-partner-edit-btn")
                    .attr("id", "show-partner-edit-btn-" + i);
                el.find(".js-delete-partner-btn")
                    .attr("id", "delete-partner-btn-" + i);
            });
        }
        /**
         * Обработчик события click на кнопке showPartnerEditBtn
         * @param {JQueryEventObject} event
         */
        , showPartnerEditBtnOnClick: function (event) {
            var partnerIndex = $(event.currentTarget).closest(".js-partners-item").data("partnerIndex");
            var id = document.getElementById("PartnersLinksOnProduct_" + partnerIndex + "__Id").value;
            var link = document.getElementById("PartnersLinksOnProduct_" + partnerIndex + "__Link").value;

            this.showPartnerEditInside("edit", partnerIndex, { Id: id, Link: link });
        }
        /**
         * Обработчик события click на кнопке cancelPartnerBtn
         * @param {JQueryEventObject} event
         */
        , cancelPartnerBtnOnClick: function (event) {
            var el = $("#partner-edit-inside");
            this.cancelPartner(el.data("action"), el.data("partnerIndex"));
        }
        /**
         * Отменим изменения о партнере
         * @param {String} action
         * @param {Number} partnerIndex
         */
        , cancelPartner: function (action, partnerIndex) {
            $("#partner-edit-inside").remove();
            if (action == "add") {
                $("#show-partner-add-btn").show();
            } else {
                $("#partners-item-" + partnerIndex).show();
            }
        }
        // -/ Работаем с сылками на партнеров
        /**
         * Установка событий на форму ProductForm
         */
        , setProductFormEvents: function () {
            $("#show-partner-add-btn").on("click", $.proxy(this.showPartnerAddBtnOnClick, this));
            var partnersObj = this;
            $(".js-partners-item").each(function (i) {
                partnersObj.setPartnersItemEvents(i);
            });
            $("#save-product-btn").on("click", $.proxy(this.saveProductBtnOnClick, this));
            $("#delete-product-btn").on("click", $.proxy(this.deleteProductBtnOnClick, this));
            $("#copy-attributes-btn").on("click", $.proxy(this.copyAttributes, this));
            $("#copy-images-btn").on("click", $.proxy(this.copyImages, this));
            this.setSelectGroupListEvents();
            /*this.setImageBtnsEvents();
            this.setFileEvents();*/
            $("#sertificate-url-input").on("input", $.proxy(this.sertificateUrlInputOnInput, this));
            $("#upload-sertificate-btn").on("click", $.proxy(this.uploadSertificateBtnOnClick, this));
            this.setFiltersForSectionEvents();
            this.setSelectAttributeValueHolderEvents(".js-remove-select-attribute-value-btn");
            common.tinymce({
                plugin_preview_width: 1078,
                folder: "Products"
            });
            // устанавливаем события на добавления новых значений в фильтры
            $(".js-add-select-attribute-value-btn").on("click", $.proxy(this.addSelectAttributeValueBtnOnClick, this));
        }
        /**
         * Установка событий на выбора раздела
         */
        , setSelectGroupListEvents: function () {
            var productsObj = this;
            new Sections.AsPathSiblings({
                sections: this.sections
                , onChange: function (p, currentSectionId) {
                    if (currentSectionId == 0) {
                        $("#SectionId").val("0");
                        $("#filters-groups-holder").empty();
                    } else {
                        $("#SectionId").val(currentSectionId);
                        productsObj.showFiltersGroupsHolder(currentSectionId);
                    }
                }
            });
        }
        // Работа с группой фильтров 
        /**
        * Получим jquery элемент filtersGroupsHolder посредством jqueryTmpl
        * @param {Object} filters
        * @param {Object} filtersGroups
        * @returns {jQueryObject}
        */
        , getFiltersGroupsHolde: function (filters, filtersGroups) {
            var attrs = this.getFormAttributes();
            this.addUnfilledFilters(attrs, filters);
            return $("#filtersGroupsHolder").tmpl({
                filters: filters
                , filtersGroups: filtersGroups
                , attributes: attrs
                , validationAttributes: this.validationAttributes
                , getValueItemFromFilterList: this.getValueItemFromFilterList
            });
        }
        /**
         * Отобразим filtersGroupsHolder в dom дереве
         * @param {Number} sectionId
         */
        , showFiltersGroupsHolder: function (sectionId) {
            var productsObj = this;
            this.getFiltersForSection(sectionId)
                .done(function (data) {
                    productsObj.getFiltersGroupsHolde(
                        data.filters
                        , data.filtersGroups
                    )
                    .replaceAll("#filters-groups-holder");
                    //
                    productsObj.setFiltersForSectionEvents();
                })
        }
        /**
         * Установка событий на фильтры
         */
        , setFiltersForSectionEvents: function () {
            $(".js-select-attribute-value").on("change", $.proxy(this.selectAttributeValueOnChange, this));
            // устанавливаем события на добавления новых значений в фильтры
            $(".js-add-select-attribute-value-btn").on("click", $.proxy(this.addSelectAttributeValueBtnOnClick, this));
        }
        /**
         * Получим фильтры для данного раздела
         * @param {Number} sectionId
         */
        , getFiltersForSection: function (sectionId) {
            var productsObj = this;
            return $.ajax({
                url: this.baseUrl + "Admin/Filters/ForSelectLists"
                , data: {
                    sectionId: sectionId
                    , formFilterIds: this.getFormFilterIds().join(",")
                }
                , Type: "GET"
                , dataType: "json"
            });
        }
        /**
         * Получим список идентификаторов всех заполненных характеристик
         * @returns {Array}
         */
        , getFormFilterIds: function () {
            var formFilterIds = [];
            $(".js-select-attribute-value-holder").each(function () {
                var filterId = $(this).data("filterId");
                if (formFilterIds.indexOf(filterId) == -1)
                    formFilterIds.push(filterId);
            });
            return formFilterIds;
        }
        /**
         * Получим список всех заполненных характеристик
         * @returns {Array}
         */
        , getFormAttributes: function () {
            var attrs = [];
            $(".js-select-attribute-value-holder").each(function () {
                var valueHolder = $(this);
                var filterId = valueHolder.data("filterId");
                var valueId = valueHolder.data("valueId");

                var finded = false;
                for (var i = 0; i < attrs.length; i++) {
                    if (attrs[i].Filter.Id == filterId) {
                        finded = true;
                        break;
                    }
                }

                if (!finded)
                    attrs.push({ Filter: { Id: filterId }, Values: [] });

                for (var i = 0; i < attrs.length; i++) {
                    if (attrs[i].Filter.Id == filterId) {
                        attrs[i].Values.push({ ValueId: valueId });
                        break;
                    }
                }
            });
            return attrs;
        }
        /**
         * Добавим незаполненные характеристики
         * @param {Array} filters
         */
        , addUnfilledFilters: function (attributes, filters) {
            $.each(filters, function (filterId, filter) {
                var finded = false;
                for (var i = 0; i < attributes.length; i++) {
                    if (attributes[i].Filter.Id == filterId) {
                        finded = true;
                        break;
                    }
                }
                if (!finded)
                    attributes.push({ Filter: { Id: filterId }, Value: {}});
            });
        }
        /**
         * Получим объект значения из списка
         * @returns {Object} содержит Value и Text
         */
        , getValueItemFromFilterList: function (filterList, valueId) {
            for (var i = 0; i < filterList.length; i++) {
                if (filterList[i].Value == valueId)
                    return filterList[i];
            }
        }
        // -/ Работа с группой фильтров
        // 
        //
        // Работа со значениями типа select
        /**
        * Получим jquery элемент selectAttributeValueHolder посредством jqueryTmpl
        * @param {Object} item
        * @param {Number} attributeIndex
        * @param {Number} valueIndex
        * @param {Number} filterId
        * @returns {jQueryObject}
        */
        , getSelectAttributeValueHolder: function (item, attributeIndex, valueIndex, filterId) {
            return $("#selectAttributeValueHolderTmpl").tmpl({
                item: item
                , attributeIndex: attributeIndex
                , valueIndex: valueIndex
                , filterId: filterId
                , validationAttributes: this.validationAttributes
            });
        }
        /**
         * Установим события на элемент selectAttributeValueHolder
         * @param {String} removeSelectAttributeValueBtnSelector - css селектор кнопки removeSelectAttributeValueBtnSelector
         */
        , setSelectAttributeValueHolderEvents: function (removeSelectAttributeValueBtnSelector) {
            $(removeSelectAttributeValueBtnSelector).on("click", $.proxy(this.removeSelectAttributeValueBtnOnClick, this));
        }
        /**
         * Отобразим элемент selectAttributeValueHolder в dom дереве 
         * @param {Object} item
         * @param {Number} filterId
         */
        , showSelectAttributeValueHolder: function (item, filterId) {
            if (!item.Value)
                return;
            var holder = $("#select-attribute-values-holder-" + filterId);
            var attributeIndex = holder.data("attributeIndex");
            var valueIndex = holder.find(".js-select-attribute-value-holder").length;
            this.getSelectAttributeValueHolder(item, attributeIndex, valueIndex, filterId).appendTo(holder);
            this.setSelectAttributeValueHolderEvents("#select-attribute-value-holder-" + item.Value + " .js-remove-select-attribute-value-btn");
        }
        /**
         * Добавим элемент selectAttributeValue
         * @param {Number} valueId
         * @param {Number} filterId
         */
        , addSelectAttributeValue: function (valueId, filterId) {
            var filter = $("#filter-holder-" + filterId);
            filter.find(".js-select-attribute-value").val("");
            var filterMultiple = filter.data("filterMultiple") === "True";
            var item = this.findFilterForSelectListItem(valueId, filterId);
            if ($("#select-attribute-value-holder-" + item.Value).length > 0)
                return;
            if (!filterMultiple && $("#select-attribute-values-holder-" + filterId).children().length > 0)
                return;
            this.showSelectAttributeValueHolder(item, filterId);
        }
        /**
         * Удалим элемент selectAttributeValue
         * @param {Number} valueId
         * @param {Number} filterId
         */
        , removeSelectAttributeValue: function (valueId, filterId) {
            $("#select-attribute-value-holder-" + valueId).remove();
            var valuesHolder = $("#select-attribute-values-holder-" + filterId);
            var attributeIndex = valuesHolder.data("attributeIndex");
            //
            valuesHolder.find("input").each(function (i) {
                $(this)
                    .attr("id", "Attributes_" + attributeIndex + "__Values_" + i + "__ValueId")
                    .attr("name", "Attributes[" + attributeIndex + "].Values[" + i + "].ValueId");
            });
            //
            var select = $("#filter-holder-" + filterId + " .js-select-attribute-value").val("");
        }
        /**
         * Найдем данные по фильтру из списка filterForSelectList
         * @param {Number} valueId
         * @param {Filter} filterId
         */
        , findFilterForSelectListItem: function (valueId, filterId) {
            var filterForSelectList = $("#filter-holder-" + filterId + " .js-select-attribute-value option").map(function () {
                var el = $(this);
                return {
                    Value: el.attr("value")
                    , Text: el.text()
                }
            });
            for (var i in filterForSelectList) {
                if (filterForSelectList[i].Value == valueId) {
                    return filterForSelectList[i];
                }
            }
            return null;
        }
        /**
         * Обработчик события change на кнопке selectAttributeValue
         * @param {JQueryEventObject} event
         */
        , selectAttributeValueOnChange: function (event) {
            var el = $(event.currentTarget);
            var valueId = el.val();
            var filterId = el.closest(".js-filter-holder").data("filterId");
            this.addSelectAttributeValue(valueId, filterId);
        }
        /**
         * Обработчик события change на кнопке removeSelectAttributeValueBtn
         * @param {JQueryEventObject} event
         */
        , removeSelectAttributeValueBtnOnClick: function (event) {
            var el = $(event.currentTarget)
            var valueId = el.closest(".js-select-attribute-value-holder").data("valueId");
            var filterId = el.closest(".js-filter-holder").data("filterId");
            this.removeSelectAttributeValue(valueId, filterId);
        }
        /**
         * Обработчик события change на кнопке addSelectAttributeValueBtn
         * @param {JQueryEventObject} event
         */
        , addSelectAttributeValueBtnOnClick: function (event) {
            var filterId = $(event.currentTarget).closest(".js-filter-holder").data("filterId");
            this.showSelectAttributeValueForm(filterId);
        }
        /**
         * Показываем форму для добавления нового значения
         * @param {Number} filterId идентификатор фильтра
         */
        , showSelectAttributeValueForm: function (filterId) {
            // закроем старую форму
            this.hideSelectAttributeValueForm();
            // покажем новую форму
            $("#filterInputGroup" + filterId).hide();
            this.getSelectAttributeValueForm().insertAfter("#filterInputGroup" + filterId);
            this.setSelectAttributeValueFormEvents();
        }

        /**
        * Получим jquery элемент selectAttributeValueForm посредством jqueryTmpl
        * @returns {jQueryObject}
        */
        , getSelectAttributeValueForm: function () {
            return $("#selectAttributeValueFormTmpl").tmpl({
                isAllowLocalization: common.isAllowLocalization
            });
        }
        /**
         * Устанавливаем события на форрму selectAttributeValueForm
         */
        , setSelectAttributeValueFormEvents: function () {
            $("#saveSelectAttributeValueBtn").on("click", $.proxy(this.saveSelectAttributeValueBtnOnClick, this))
            $("#cancelSelectAttributeValueBtn").on("click", $.proxy(this.cancelSelectAttributeValueOnClick, this))
        }
        /**
         * Проверяем правильно ли введено число
         * @param {String} value введенное значение
         */
        , validateSelectAttributeValueAsDouble: function (value) {
            if (/[^0-9.,]/.test(value.trim()))
                return false;
            var v = parseFloat(value.replace(",", ".").trim());
            return isNaN(v) ? false : true;
        }
        /**
         * Проверим форму selectAttributeValueForm
         * @param {Number} filterId
         * @param {String} value
         * @param {String} valueAsDouble
         */
        , validateSelectAttributeValueForm: function (filterId, value, valueAsDouble) {
            if (!value) {
                message.error("Введите значение.");
                return false;
            }

            if (!this.validateSelectAttributeValueAsDouble(valueAsDouble)) {
                message.error("Неправильно введено числовое значение.");
                return false;
            }

            if (this.isExistsSelectAttributeValue(filterId, value)) {
                message.warning("Такое значение уже есть.");
                return false;
            }
            return true;
        }
        /**
         * Проверяем есть уже такое значение
         * @param {Number} filterId
         * @param {String} value
         */
        , isExistsSelectAttributeValue: function (filterId, value) {
            return $("#filterInputGroup" + filterId + " option").filter(function () {
                return $(this).text() === value;
            }).length > 0;
        }
        /**
         * Обработчик события change на кнопке saveSelectAttributeValueBtn
         * @param {JQueryEventObject} event
         */
        , saveSelectAttributeValueBtnOnClick: function (event) {
            var filterId = $(event.currentTarget).closest(".js-filter-holder").data("filterId");
            this.saveSelectAttributeValue(filterId);
        }
        /**
         * Обработчик события change на кнопке cancelSelectAttributeValue
         * @param {JQueryEventObject} event
         */
        , cancelSelectAttributeValueOnClick: function (event) {
            this.hideSelectAttributeValueForm();
        }
        /**
         * Добавим опцию в select
         * @param {Object} value
         */
        , addSelectAttributeValueOption: function (value) {
            $("<option/>", {
                value: value.Id
                , text: value.Title
            })
            .appendTo("#filterInputGroup" + value.FilterId + " select");
        }
        /**
         * Сохраним значение на удаленный сервер
         * @param {Number} filterId
         */
        , saveSelectAttributeValue: function (filterId) {
            if (this.denyAjax)
                return;

            var value = $("#selectAttributeValue").val().trim();
            var valueIn = $("#selectAttributeValueIn").val().trim();
            var valueAsDouble = $("#selectAttributeValueAsDouble").val().trim();
            if (!valueAsDouble)
                valueAsDouble = "0";

            if (!this.validateSelectAttributeValueForm(filterId, value, valueAsDouble))
                return;

            this.denyAjax = true;

            $.ajax({
                url: this.baseUrl + "Admin/Filters/AddFilterValue"
                , data: {
                    filterId: filterId
                    , value: value
                    , valueIn: valueIn
                    , valueAsDouble: valueAsDouble
                }
                , type: "post"
                , dataType: "json"
                , context: this
                , success: function (value) {
                    this.addSelectAttributeValueOption(value);
                    this.hideSelectAttributeValueForm();
                    this.denyAjax = false;
                }
                , error: function (xhr, status) {
                    message.error("Произошла ошибка на удаленном сервере.");
                    this.denyAjax = false;
                }
            });

        }
        /**
         * Скроем форму selectAttributeValueForm
         */
        , hideSelectAttributeValueForm: function () {
            $("#selectAttributeValueForm").prev().show();
            $("#selectAttributeValueForm").remove();
        }
        // -/ Работа со значениями типа select
        // Работае с сертификатом
        /**
         * Загрузка сертификата на удаленный сервер
         * @param {String} url
         */
        , loadSertificate: function (url) {
            if (url.trim().length == 0) {
                message.error("Укажите url или ftp сертификата.");
                return;
            }
            if (url.indexOf("http://") == -1) {
                message.error("url или ftp сертификата должен быть внешним.");
                return;
            }
            if (url.indexOf(this.baseUrl) != -1) {
                message.error("url или ftp сертификата должен быть внешним.");
                return;
            }
            if (url.substring(url.lastIndexOf("/") + 1).replace(/\.\w+$/, "") == $("#Sertificate").val()) {
                message.error("Такой сертификат уже был загружен.");
                return;
            }
            if (this.denyAjax)
                return;
            this.denyAjax = true;

            $.ajax({
                url: this.baseUrl + "Admin/Products/LoadSertificate"
                , data: "url=" + url
                , type: "POST"
                , dataType: "json"
                , context: this
                , success: function (json) {
                    $("#Sertificate").val(json.Sertificate);
                    message.success("Cертификат загружен.");
                    this.denyAjax = false;
                }
                , error: function (xhr, status) {
                    message.error(xhr.responseText);
                    this.denyAjax = false;
                }
            });
        }
        /**
         * Обработчик события click на кнопке uploadSertificateBtn
         * @param {JQueryEventObject} event
         */
        , uploadSertificateBtnOnClick: function (event) {
            var url = $("#sertificate-url-input").val();
            this.loadSertificate(url);
        }
        /**
         * Проверим url на валидность
         * @param {String} url
         * @returns {Boolean}
         */
        , validateSertificateUrl: function (url) {
            if (url.trim().length == 0)
                return false;
            if (url.indexOf("http://") == -1)
                return false;
            if (url.indexOf(this.baseUrl) != -1)
                return false;
            if (url.substring(url.lastIndexOf("/") + 1).replace(/\.\w+$/, "") == $("#Sertificate").val())
                return false;
            return true;
        }
        /**
         * Активируем или деактивируем кнопку для загрузки сертификата.
         * Url проверяется на валидность
         * @param {String} url
         */
        , ActivateOrDeactivteUploadSertificateBtn: function (url) {
            if (this.validateSertificateUrl(url))
                $("#upload-sertificate-btn").removeClass("disabled");
            else 
                $("#upload-sertificate-btn").addClass("disabled");
        }
        /**
         * Обработчик события input на элементе sertificateUrlInput
         * @param {JQueryEventObject} event
         */
        , sertificateUrlInputOnInput: function (event) {
            var url = $("#sertificate-url-input").val();
            this.ActivateOrDeactivteUploadSertificateBtn(url);
        }
        // -/ Работае с сертификатом
    });
    return Products;
});