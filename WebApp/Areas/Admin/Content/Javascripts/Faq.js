﻿/**
 * Модуль содержит свойства и методы по работе с Вопросами и ответами   
 * @author Ruslan Rakhmankulov
 */
define("Admin/Faq", ["jquery", "common", "SelectControl", "jqueryUnobtrusive", "jqueryToastmessage", "jqueryAlerts", "jqueryTmpl", "text!Admin/CommonTmpl.htm"], function ($, common, SelectControl) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Faq() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(Faq.prototype, {
        defaults: {
            baseUrl: common.baseUrl
            , denySubmit: false // состояние, которое запрещает повторную отправку формы
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(true, this, this.defaults, options);
        }
        /**
         * Установим событий и инициализация плагинов для формы 
         */
        , setFaqFormEvents: function () {
            $("#faqSubmitBtn").on("click", $.proxy(this.faqSubmitBtnOnClick, this));
            $("#deleteFaqBtn").on("click", $.proxy(this.deleteFaqBtnOnClick, this));
            //
            this.initSectionSelectControl("SectionId", "ProductIds");
            //
            this.setFaqProductBindingEvents();
        }
        , setFaqListEvents: function () {
            this.initSectionSelectControl("SectionId", "ProductId");
        }
        /**
         * Инициализация селекта раздела
         * @param {Number} sectionId - ид элемента раздела
         * @param {Number} productId - ид элемента товара
         */
        , initSectionSelectControl: function (sectionId, productId) {
            new SelectControl({
                element: $("#" + sectionId)
                , baseUrl: this.baseUrl
                , dependentSelectElem: $("#" + productId)
                , dependentSelectUri: "Admin/Products/ProductTitlesForSelectList"
                , dependentSelectParam: "sectionId"
            });
        }
        /**
         * Обработчик события click на кнопке submit формы
         * @param {JQueryEventObject} event
         */
        , faqSubmitBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            var formIsValid = $("#faqForm").valid();
            if (formIsValid) {
                this.denySubmit = true;
                $("#faqForm").submit();
            }
            else {
                message.warning("Некорректно заполнены поля.");
            }
            
            return;
        }
        /**
         * Событие действия на кнопке deleteFaqBtn
         * @param {JQueryEventObject} event
         */
        , deleteFaqBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            $.alerts.confirm("Вы действительно хотите удалить faq?", $.noop, function (r) {
                if (r) {
                    this.denySubmit = true;
                    var action = $(event.currentTarget).data("href");
                    $(event.currentTarget).closest("form").attr("action", action).submit();
                }
            });
        }
        // --
        // Работа с привязками к товарам
        // --
        /**
         * Устанавливает события на элементы для привязки товаров
         */
        , setFaqProductBindingEvents: function () {
            $("#ProductIds").on("change", $.proxy(this.productIdsSelectOnChange, this));
            $("#productsHolder").on("click", ".js-remove-product-holder-btn", $.proxy(this.removeProductHolderBtnOnClick, this));
        }
        /**
         * Обработчик события change на селекте ProductIds
         * @param {JQueryEventObject} event
         */
        , productIdsSelectOnChange: function (event) {
            var selectEl = $(event.currentTarget);
            var productId = selectEl.val();
            if (productId === "")
                return;
            if ($("#productHolder" + productId).length > 0)
                return;
            var productTitle = selectEl.find("option[value=" + productId + "]").text();
            this.addProductHolderElement(productId, productTitle, $(".js-product-holder").length);
        }

        /**
         * Обработчик события click на кнопке removeProductHolderBtn
         * @param {JQueryEventObject} event
         */
        , removeProductHolderBtnOnClick: function (event) {
            var productId = $(event.currentTarget).closest(".js-product-holder").data("productId");
            this.removeProductHolderElement(productId);
        }
        /**
         * Добавляет элемент productHolder в dom дерево
         * @param {Number} productId идентификатор товара
         * @param {String} productTitle название товара
         * @param {Number} index порядковый номер элемента productHolder
         */
        , addProductHolderElement: function (productId, productTitle, index) {
            $("#faqProductHolderTmpl").tmpl({
                productId: productId
                , productTitle: productTitle
                , index: index
            })
            .appendTo("#productsHolder");
        }
        /**
         * Удаляет элемент productHolder из dom дерева
         */
        , removeProductHolderElement: function (productId) {
            $("#productHolder" + productId).remove();
            this.changeProductHolderPositions();
        }
        /**
         * Изменяет порядковые номера элементов productHolder  
         */
        , changeProductHolderPositions: function () {
            $(".js-product-holder").each(function (i) {
                $(this)
                    .find(".js-product-id")
                        .attr("id", "ProductIds_" + i + "__Value")
                        .attr("name", "ProductIds[" + i + "].Value");;
            });
        }
    });
    return Faq;
});