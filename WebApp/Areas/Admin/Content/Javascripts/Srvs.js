﻿/**
 * Модуль содержит свойства и методы по работе с СЦ
 * @author Ruslan Rakhmankulov
 */
define("Admin/Srvs", ["jquery", "common", "globalize", "jqueryUnobtrusive", "jqueryToastmessage", "jqueryAlerts"], function ($, common, Globalize) {
    /**
     * Конструктор является прокси для метода init
     * @constructor
     */
    function Srvs() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;
    /**
     * В прототипе содержатся все необходимые методы и объект defaults, содержащий все свойства по умолчанию. 
     * Расширяем прототип с помощью jQuery.extend
     */
    $.extend(Srvs.prototype, {
        /**
         * Все свойства defaults являются переопределяемые через объект options.
         */
        defaults: {
            baseUrl: common.baseUrl
            , culture: common.culture || "ru"
            , denyAjax: false // состояние, которое запрещает повторную отправку запроса аякс
            , denySubmit: false // состояние, которое запрещает повторную отправку формы
        }
        /**
         * Инициализиуем экземпляр класса
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(this, this.defaults, options);
            Globalize.culture(this.culture);
            // установка событий на форму
            $("#save-service-btn").on("click", $.proxy(this.saveServiceBtnOnClick, this));
            $("#delete-service-btn").on("click", $.proxy(this.deleteServiceBtnOnClick, this));
        }
        /**
         * Обработчик события click на кнопке saveServiceBtn
         * @param {JQueryEventObject} event
         */
        , saveServiceBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            var formIsValid = $("#service-form").valid();
            if (formIsValid) {
                this.denySubmit = true;
                $("#service-form").submit();
            }
            return;
        }

        /**
         * Обработчик события click на кнопке deleteServiceBtn
         * @param {JQueryEventObject} event
         */
        , deleteServiceBtnOnClick: function (event) {
            event.preventDefault();
            if (this.denySubmit)
                return;
            $.alerts.confirm("Вы действительно хотите удалить сервисный центр?", $.noop, function (r) {
                if (r) {
                    this.denySubmit = true;
                    var action = $(event.currentTarget).data("href");
                    $(event.currentTarget).closest("form").attr("action", action).submit();
                }
            });
        }
    });
    return Srvs;
});