﻿using System.Web.Http;
using System.Web.Mvc;

namespace WebApp.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.Routes.MapHttpRoute(
                name: "AdminApi",
                routeTemplate: "api/Admin/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            context.MapRoute(
                name: "CultureAdmin",
                url: "{culture}/Admin/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                constraints: new { culture = @"ru|en" },
                namespaces: new[] { "WebApp.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                name: "Admin_default",
                url: "Admin/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "WebApp.Areas.Admin.Controllers" }
            );
        }
    }
}