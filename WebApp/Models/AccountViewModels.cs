﻿using DomainLogic.Interfaces.Services;
using WebApp.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using WebApp.UiHelpers;

namespace WebApp.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.AccountRes), ErrorMessageResourceName = "ForgotFormUserEmailRequiredMessage")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.AccountRes), ErrorMessageResourceName = "ForgotFormUserEmailInvalidMessage")]
        [Display(Name = "ForgotFormUserEmailDisplayName", ResourceType = typeof(Resources.AccountRes))]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.AccountRes), ErrorMessageResourceName = "LoginFormUserEmailRequiredMessage")]
        [Display(Name = "LoginFormUserEmailDisplayName", ResourceType = typeof(Resources.AccountRes))]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.AccountRes), ErrorMessageResourceName = "LoginFormUserEmailInvalidMessage")]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.AccountRes), ErrorMessageResourceName = "LoginFormPasswordRequiredMessage")]
        [DataType(DataType.Password)]
        [Display(Name = "LoginFormPasswordDisplayName", ResourceType = typeof(Resources.AccountRes))]
        public string Password { get; set; }

        [Display(Name = "LoginFormRememberMeDisplayName", ResourceType = typeof(Resources.AccountRes))]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.AccountRes), ErrorMessageResourceName = "RegisterFormUserEmailRequiredMessage")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.AccountRes), ErrorMessageResourceName = "RegisterFormUserEmailInvalidMessage")]
        [Display(Name = "RegisterFormUserEmailDisplayName", ResourceType = typeof(Resources.AccountRes))]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.AccountRes), ErrorMessageResourceName = "RegisterFormPasswordRequiredMessage")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resources.AccountRes), ErrorMessageResourceName = "RegisterFormPasswordStringRestrictionMessage", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "RegisterFormPasswordDisplayName", ResourceType = typeof(Resources.AccountRes))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "RegisterFormConfirmPasswordDisplayName", ResourceType = typeof(Resources.AccountRes))]
        [Compare("Password", ErrorMessageResourceType = typeof(Resources.AccountRes), ErrorMessageResourceName = "RegisterFormConfirmPasswordMatchMessage")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.AccountRes), ErrorMessageResourceName = "ResetPasswordFormUserEmailRequiredMessage")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.AccountRes), ErrorMessageResourceName = "ResetPasswordFormUserEmailInvalidMessage")]
        [Display(Name = "ResetPasswordFormUserEmailDisplayName", ResourceType = typeof(Resources.AccountRes))]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.AccountRes), ErrorMessageResourceName = "ResetPasswordFormPasswordRequiredMessage")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resources.AccountRes), ErrorMessageResourceName = "ResetPasswordFormPasswordStringRestrictionMessage", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "ResetPasswordFormPasswordDisplayName", ResourceType = typeof(Resources.AccountRes))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "ResetPasswordFormConfirmPasswordDisplayName", ResourceType = typeof(Resources.AccountRes))]
        [Compare("Password", ErrorMessageResourceType = typeof(Resources.AccountRes), ErrorMessageResourceName = "ResetPasswordFormConfirmPasswordMatchMessage")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.AccountRes), ErrorMessageResourceName = "ForgotFormUserEmailRequiredMessage")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.AccountRes), ErrorMessageResourceName = "ForgotFormUserEmailInvalidMessage")]
        [Display(Name = "ForgotFormUserEmailDisplayName", ResourceType = typeof(Resources.AccountRes))]
        public string Email { get; set; }
    }
}
