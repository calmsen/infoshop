﻿using DomainLogic.Interfaces.Models;
using DomainLogic.Models.Enumerations;
using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Filters
{
    public class FeedbackListFilter : IFeedbackListFilter
    {
        [Display(Name = "Дата от")]
        //[DisplayFormat(DataFormatString = "{0:dd\\/MM\\/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime DateFrom { get; set; }

        [Display(Name = "Дата до")]
        [DataType(DataType.Date)]
        public DateTime DateTo { get; set; }

        [Display(Name = "Тема")]
        public int? Theme { get; set; }

        [Display(Name = "Катергория")]
        public long? SectionId { get; set; }

        [Display(Name = "Модель")]
        public long? ProductId { get; set; }

        [Display(Name = "Статус")]
        public StateEnum State { get; set; }

        [Display(Name = "Поиск по номеру, автору, email, тегам")]
        public string Search { get; set; }

        public FeedbackListFilter()
        {
            DateFrom = DateTime.Now;
            DateFrom = new DateTime(DateFrom.Year, DateFrom.Month, 1);
            DateTo = DateTime.Now.AddDays(1);
            State = StateEnum.New;
        }
    }
}