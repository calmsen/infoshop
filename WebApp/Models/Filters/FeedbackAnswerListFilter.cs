﻿using DomainLogic.Interfaces.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Filters
{
    public class FeedbackAnswerListFilter : IFeedbackAnswerListFilter
    {
        [Display(Name = "Дата от")]
        [DataType(DataType.Date)]
        public DateTime DateFrom { get; set; }

        [Display(Name = "Дата до")]
        [DataType(DataType.Date)]
        public DateTime DateTo { get; set; }

        [Display(Name = "Поиск по номеру, автору, email")]
        public string Search { get; set; }

        public FeedbackAnswerListFilter()
        {
            DateFrom = DateTime.Now;
            DateFrom = new DateTime(DateFrom.Year, DateFrom.Month, 1);
            DateTo = DateTime.Now.AddDays(1);
        }
    }
}