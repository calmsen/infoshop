﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Forms
{
    public class PostForm
    {
        public long Id { get; set; }

        [Display(Name = "Заголовок")]
        [StringLength(128, ErrorMessage = "Заголовок не может содержать больше 128 символов")]
        [Required(ErrorMessage = "Введите заголовок")]
        public string Title { get; set; }

        public string TitleIn { get; set; }

        [Display(Name = "Аннотация")]
        [StringLength(1024, ErrorMessage = "Аннотация не может содержать больше 1024 символов")]
        public string Annottion { get; set; }

        [StringLength(1024, ErrorMessage = "Аннотация не может содержать больше 1024 символов")]
        public string AnnottionIn { get; set; }

        public PostDescriptionForm Description { get; set; }

        [Display(Name = "Миниатюра")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Загрузите картинку.")]
        public long MainImageId { get; set; }

        [Display(Name = "Ссылка")]
        public string LinkToExpertPost { get; set; }

        [Display(Name = "Активный")]
        public bool Activate { get; set; }

        [Display(Name = "Привязать к товарам")]
        public List<long> ProductIds { get; set; }

        public DateTime CreatedDate { get; set; }

        public PostForm()
        {
            ProductIds = new List<long>();
        }
    }   

}