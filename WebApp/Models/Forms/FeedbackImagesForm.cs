﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Forms
{
    public class FeedbackImagesForm
    {
        public long Id { get; set; }

        [Display(Name = "Картинки")]
        public List<ImageId> ImagesIds { get; set; }

        public FeedbackImagesForm()
        {
            ImagesIds = new List<ImageId>();
        }
    }
}