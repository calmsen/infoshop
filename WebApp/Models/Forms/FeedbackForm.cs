﻿using DomainLogic.Infrastructure.Constants;
using DomainLogic.Models.Enumerations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebApp.ValidationAttributes;

namespace WebApp.Models.Forms
{
    public class FeedbackForm
    {
        [Display(Name = "FeedbackFormFullNameDisplayName", ResourceType = typeof(Resources.FeedbackRes))]
        [StringLength(128, ErrorMessageResourceType = typeof(Resources.FeedbackRes), ErrorMessageResourceName = "FeedbackFormFullNameStringRestrictionMessage")]
        public string FullName { get; set; }

        [Display(Name = "FeedbackFormUserEmailDisplayName", ResourceType = typeof(Resources.FeedbackRes))]
        [Required(ErrorMessageResourceType = typeof(Resources.FeedbackRes), ErrorMessageResourceName = "FeedbackFormUserEmailRequiredMessage")]
        [StringLength(128, ErrorMessageResourceType = typeof(Resources.FeedbackRes), ErrorMessageResourceName = "FeedbackFormUserEmailStringRestrictionMessage")]
        [RegularExpression(Patterns.Email, ErrorMessageResourceType = typeof(Resources.FeedbackRes), ErrorMessageResourceName = "FeedbackFormUserEmailInvalidMessage")]
        [CustomRemote("ValidateEmail", "Feedback", null, ErrorMessageResourceType = typeof(Resources.FeedbackRes), ErrorMessageResourceName = "FeedbackFormEmailRemoteValidateMessage")]
        public string Email { get; set; }

        [Display(Name = "FeedbackFormThemeDisplayName", ResourceType = typeof(Resources.FeedbackRes))]
        [Required(ErrorMessageResourceType = typeof(Resources.FeedbackRes), ErrorMessageResourceName = "FeedbackFormThemeRequiredMessage")]
        public FeedbackThemeEnum Theme { get; set; }

        [Display(Name = "FeedbackFormMessageDisplayName", ResourceType = typeof(Resources.FeedbackRes))]
        [Required(ErrorMessageResourceType = typeof(Resources.FeedbackRes), ErrorMessageResourceName = "FeedbackFormMessageRequiredMessage")]
        [StringLength(2048, ErrorMessageResourceType = typeof(Resources.FeedbackRes), ErrorMessageResourceName = "FeedbackFormMessageStringRestrictionMessage")]
        public string Message { get; set; }

        public string UrlReferer { get; set; }

        [Display(Name = "FeedbackFormSectionDisplayName", ResourceType = typeof(Resources.FeedbackRes))]
        public long? SectionId { get; set; }

        [Display(Name = "FeedbackFormProductDisplayName", ResourceType = typeof(Resources.FeedbackRes))]
        public long? ProductId { get; set; }

        [Display(Name = "CaptchaDisplayName", ResourceType = typeof(Resources.ViewsRes))]
        public string Captcha { get; set; }

        [Display(Name = "Прикрепить картинки")]
        public List<ImageId> ImagesIds { get; set; }

        public bool IsShowFeedbackMainFormGroups { get; set; }

        public bool IsReadOnlyFullNameInput { get; set; }

        public bool IsReadOnlyEmailInput { get; set; }

        public bool IsShowCaptcha { get; set; }

        public FeedbackForm()
        {
            ImagesIds = new List<ImageId>();
        }
    }
}