﻿using DomainLogic.Models;
using WebApp.ValidationAttributes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DomainLogic.Models.Enumerations;

namespace WebApp.Models.Forms
{
    
    public class FilterForm
    {
        public long Id { get; set; }

        [Display(Name = "Название фильтра")]
        [Required(ErrorMessage = "Введите название фильтра")]
        public string Title { get; set; }

        public string TitleIn { get; set; }

        [Display(Name = "Тип фильтра")]
        [Required(ErrorMessage = "Выберете тип фильтра")]
        public FilterTypeEnum Type { get; set; }

        [Display(Name = "Значения фильтра")]
        public List<FilterValueForm> Values { get; set; }

        [Display(Name = "Единица измерения")]
        public string Unit { get; set; }

        public string UnitIn { get; set; }

        public List<UnitConverter> UnitConverters { get; set; }

        [Display(Name = "В каталоге")]
        public long UnitConverterIdForCatalog { get; set; }

        [Display(Name = "В карточке")]
        public long UnitConverterIdForProduct { get; set; }

        [Display(Name = "Множественное заполнение")]
        [CustomRemote("ValidateMultiple", "Filters", "Admin", AdditionalFields = "Id", ErrorMessage = "Нельзя убрать галочку 'Множественное заполнение', так как есть товары которые имеют множество значений.")]
        public bool Multiple { get; set; }

        public FilterForm()
        {
            Values = new List<FilterValueForm>();
            UnitConverters = new List<UnitConverter>();
            UnitConverterIdForCatalog = -1;
            UnitConverterIdForProduct = -1;
        }
    }
    
}