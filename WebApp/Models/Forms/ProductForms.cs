﻿using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Forms
{
    public class ProductForm : IImagesForm
    {
        public long Id { get; set; }

        [Display(Name = "Заголовок")]
        [Required(ErrorMessage = "Поле Заголовок обязательно для заполнения")]
        [StringLength(128, ErrorMessage = "Заголовок не может быть больше 128 символов")]
        public string Title { get; set; }

        public string TitleIn { get; set; }

        [Display(Name = "Краткие характеристики")]
        [StringLength(128, ErrorMessage = "Поле Характеристики не может быть больше 64 символов")]
        public string ShortFeatures { get; set; }

        public DescriptionForm Description { get; set; }

        public long MainImageId { get; set; }

        [Display(Name = "Картинки")]
        public List<ImageId> ImagesIds { get; set; }

        [Display(Name = "Раздел")]
        [Required(ErrorMessage = "Раздел должен быть указан")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Укажите раздел.")]
        public long SectionId { get; set; }

        [Display(Name = "Характеристики")]
        public List<AttributeSimpleForm> Attributes { get; set; }

        [Display(Name = "Укажите сертификат")]
        public string Sertificate { get; set; }

        [Display(Name = "Загрузите банер если нужно")]
        public long BannerImageId { get; set; }

        [Display(Name = "Загрузите банер на английском")]
        public long BannerImageIdIn { get; set; }

        [Display(Name = "Порядковый номер баннера")]
        public int? BannerPosition { get; set; }

        [Display(Name = "Показать на банере")]
        public bool ToBanner { get; set; }

        [Display(Name = "Популярный")]
        public bool ToRating { get; set; }

        [Display(Name = "Разрешить загрузку файлов.")]
        public bool AllowFiles { get; set; }

        [Display(Name = "Опубликовать")]
        public bool Published { get; set; }

        [Display(Name = "Отметить как заполнено?")]
        public bool Filled { get; set; }

        public bool Active { get; set; }

        public bool Archive { get; set; }

        [Display(Name = "Новинка")]
        public bool New { get; set; }

        [Display(Name = "Ссылки на товары")]
        public List<PartnerLinkOnProduct> PartnersLinksOnProduct { get; set; }

        [Display(Name = "Служебный комментарий")]
        [StringLength(512, ErrorMessage = "Поле Служебный комментарий не может быть больше 512 символов")]
        public string Comment { get; set; }

        [Display(Name = "Укажите внешнюю ссылку для баннера")]
        [StringLength(512, ErrorMessage = "Ссылка не может содержать больше чем 512 символов.")]
        public string ExternalLink { get; set; }

        [Display(Name = "Цена")]
        [Range(0, Int32.MaxValue, ErrorMessage = "Поле Цена должно содержать число.")]
        public float Price { get; set; }

        [Display(Name = "Код производителя")]
        [Range(0, Int32.MaxValue, ErrorMessage = "Поле Код производителя должно содержать число.")]
        public long? Code { get; set; }

        [Display(Name = "Выберите изделие")]
        public long? TypeId { get; set; }

        public long? Barcode { get; set; }

        public long? StoreState { get; set; }

        public int ViewsAmount { get; set; }

        public DateTime CreatedDate { get; set; }

        public ProductForm()
        {
            Attributes = new List<AttributeSimpleForm>();
            ImagesIds = new List<ImageId>();
            Description = new DescriptionForm();
            Published = false;
            PartnersLinksOnProduct = new List<PartnerLinkOnProduct>();
            Active = true;
        }
    }           
}