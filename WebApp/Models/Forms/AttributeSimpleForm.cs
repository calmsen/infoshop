﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Forms
{
    public class AttributeSimpleForm
    {
        public long Id { get; set; }

        [Required]
        public FilterSimpleForm Filter { get; set; }

        public List<ValueSimpleForm> Values { get; set; }

        public AttributeSimpleForm()
        {
            Values = new List<ValueSimpleForm> {
                new ValueSimpleForm()
            };
        }
    }
}