﻿using DomainLogic.Models.Enumerations;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Forms
{
    public class VacancyForm
    {
        public int Id { get; set; }

        [Display(Name = "Название вакансии")]
        [StringLength(128, ErrorMessage = "Поле 'Название вакансии' не может содержать больше 128 символов")]
        [Required(ErrorMessage = "Введите название вакансии")]
        public string Title { get; set; }

        [Display(Name = "Адрес")]
        [Required(ErrorMessage = "Введите адрес")]
        [StringLength(256, ErrorMessage = "Поле 'Адрес' не может содержать больше чем 256 символов")]
        public string Address { get; set; }

        [Display(Name = "Занятость")]
        [Required(ErrorMessage = "Введите занятость")]
        [StringLength(256, ErrorMessage = "Поле 'Занятость' не может содержать больше чем 256 символов")]
        public string Employment { get; set; }

        [Display(Name = "Обязанности")]
        [Required(ErrorMessage = "Введите Обязанности")]
        [StringLength(2048, ErrorMessage = "Поле 'Обязанности' не может содержать больше чем 2048 символов")]
        public string Duties { get; set; }

        [Display(Name = "Образование")]
        [Required(ErrorMessage = "Введите Образование")]
        [StringLength(256, ErrorMessage = "Поле 'Образование' не может содержать больше чем 256 символов")]
        public string Education { get; set; }

        [Display(Name = "Опыт")]
        [Required(ErrorMessage = "Введите Опыт")]
        [StringLength(128, ErrorMessage = "Поле 'Опыт' не может содержать больше чем 128 символов")]
        public string Experience { get; set; }

        [Display(Name = "Доп. требования")]
        [Required(ErrorMessage = "Введите Дополнительные требования")]
        [StringLength(2048, ErrorMessage = "Поле 'Доп. требования' не может содержать больше чем 2048 символов")]
        public string AdditionalRequirements { get; set; }

        [Display(Name = "Зарплата")]
        [Required(ErrorMessage = "Введите Зарплату")]
        [StringLength(128, ErrorMessage = "Поле 'Зарплата' не может содержать больше чем 128 символов")]
        public string Salary { get; set; }

        [Display(Name = "E-mail")]
        [Required(ErrorMessage = "Введите e-mail")]
        [StringLength(128, ErrorMessage = "Поле 'E-mail' не может содержать больше чем 128 символов")]
        public string Email { get; set; }

        [Display(Name = "Условия работы")]
        public string WorkingConditions { get; set; }
        [Display(Name = "Статус вакансии")]
        [Required(ErrorMessage = "Выберите статус вакансии")]
        public VacancyStateEnum State { get; set; }
    }
}