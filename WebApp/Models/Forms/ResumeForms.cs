﻿using DomainLogic.Infrastructure.Constants;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Forms
{
    public class ResumeForm
    {
        [Display(Name = "Название вакансии")]
        [Required(ErrorMessage = "Поле 'Название вакансии' обязательно для заполнения")]
        [StringLength(128, ErrorMessage = "Поле 'Название вакансии' не может быть больше {1} символов")]
        public string Title { get; set; }

        [Display(Name = "ФИО")]
        [Required(ErrorMessage = "Поле 'ФИО' обязательно для заполнения")]
        [StringLength(128, ErrorMessage = "Поле 'ФИО' не может быть больше {1} символов")]
        public string FullName { get; set; }

        [Display(Name = "Контактный телефон")]
        [Required(ErrorMessage = "Поле 'Контактный телефон' обязательно для заполнения")]
        [StringLength(128, ErrorMessage = "Поле 'Контактный телефон' не может быть больше {1} символов")]
        public string Phone { get; set; }

        [Display(Name = "Электронная почта")]
        [Required(ErrorMessage = "Поле 'Электронная почта' обязательно для заполнения")]
        [StringLength(128, ErrorMessage = "Поле 'Электронная почта' не может быть больше {1} символов")]
        [RegularExpression(Patterns.Email, ErrorMessage = "Не правильно введен e-mail")]
        public string Email { get; set; }

        [Display(Name = "Образование")]
        [StringLength(1024, ErrorMessage = "Поле 'Образование' не может быть больше {1} символов")]
        public string Education { get; set; }

        [Display(Name = "Опыт работы")]
        [StringLength(4096, ErrorMessage = "Поле 'Опыт работы' не может быть больше {1} символов")]
        public string Experience { get; set; }

        [Display(Name = "Дополнительная информация")]
        [StringLength(2048, ErrorMessage = "Поле 'Дополнительная информация' не может быть больше {1} символов")]
        public string AdditionalInfo { get; set; }

        [Display(Name = "Вложить резюме")]
        public string ResumeDocLink { get; set; }

        [Display(Name = "Введите код с картинки")]
        [Required(ErrorMessage = "Введите код с картинки")]
        public string Captcha { get; set; }
    }
}