﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Forms
{
    public class ProductTypeForm 
    {
        public long Id { get; set; }

        [Display(Name = "Название изделия")]
        [StringLength(128, ErrorMessage = "Название не может содержать больше 128 символов")]
        [Required(ErrorMessage = "Введите название изделия")]
        public string Title { get; set; }

        [Display(Name = "Название изделия на английском")]
        [StringLength(128, ErrorMessage = "Название не может содержать больше 128 символов")]
        public string TitleIn { get; set; }

        public long? SectionId { get; set; }
    }
}