﻿using DomainLogic.Models.Enumerations;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Forms
{
    public class SimpleFeedbackForm
    {
        [Required]
        public long Id { get; set; }
        [Display(Name = "FeedbackAnswerFormStateDisplayName", ResourceType = typeof(Resources.FeedbackRes))]
        public StateEnum State { get; set; }
    }
}