﻿using DomainLogic.Infrastructure.Constants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Forms
{
    public class FeedbackInviteMessageForm
    {
        [Display(Name = "Сообщение")]
        [Required(ErrorMessage = "Поле Сообщение обязательное для заполнения.")]
        public string Message { get; set; }

        public List<Email> Emails { get; set; }

        public long FeedbackId { get; set; }

        public FeedbackInviteMessageForm()
        {
            Message = "Пожалуйста, ответьте на заявку и поменяйте статус на \"Решено\".";
        }

        //
        public class Email
        {
            [Display(Name = "E-mail")]
            [Required(ErrorMessage = "Поле E-mail обязательное для заполнения.")]
            [StringLength(128, ErrorMessage = "E-mail должен быть не больше 128 символов")]
            [RegularExpression(Patterns.Email, ErrorMessage = "E-mail введен неверно")]
            public string Value { get; set; }
        }
    }
}