﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Forms
{
    /// <summary>
    /// Данный класс описывает форму для добавления/редактирования партнера
    /// </summary>
    public class PartnerForm
    {
        public long Id { get; set; }

        [Display(Name = "Заголовок")]
        [StringLength(512, ErrorMessage = "Заголовок не может содержать больше 512 символов")]
        [Required(ErrorMessage = "Введите заголовок")]
        public string Title { get; set; }

        public string TitleIn { get; set; }

        [Display(Name = "Миниатюра")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Загрузите картинку.")]
        public long MainImageId { get; set; }

        [Display(Name = "Порядковый номер партнера")]
        public int? Position { get; set; }

        [Display(Name = "Ссылка")]
        [Required(ErrorMessage = "Введите ссылку")]
        public string LinkToPartner { get; set; }

        public DateTime CreatedDate { get; set; }

    }
}