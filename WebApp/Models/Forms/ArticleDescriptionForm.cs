﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebApp.Models.Forms
{
    public class ArticleDescriptionForm
    {
        public long Id { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = "Введите описание новости")]
        [Display(Name = "Описание новости")]
        public string Content { get; set; }

        [AllowHtml]
        public string ContentIn { get; set; }
    }
}