﻿using DomainLogic.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace WebApp.Models.Forms
{
    public class SectionForm : IValidatableObject
    {
        public long Id { get; set; }

        [Display(Name = "Название раздела")]
        [Required(ErrorMessage = "Поле Название раздела обязательно для заполнения")]
        [StringLength(128, ErrorMessage = "Название раздела не может быть больше 128 символов")]
        public string Title { get; set; }

        public string TitleIn { get; set; }

        [Display(Name = "Родительский раздел")]
        public long ParentId { get; set; }

        [Display(Name = "Фильтры")]
        public List<FilterGroup> FiltersGroups { get; set; }

        [Display(Name = "Миниатюра раздела")]
        public long? MainImageId { get; set; }

        [Display(Name = "Картинки для баннера")]
        public List<ImageId> BannerIds { get; set; }

        [Display(Name = "Скрыть раздел?")]
        public bool Hidden { get; set; }

        public int Position { get; set; }

        public long PathOfSections { get; set; }

        public SectionForm()
        {
            FiltersGroups = new List<FilterGroup>();
            BannerIds = new List<ImageId>();
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (FiltersGroups.SelectMany(x => x.Filters.ToList()).GroupBy(x => x.Id).Any(x => x.Count() > 1))
            {
                errors.Add(new ValidationResult("Нельзя указать одинаковые фильтры", new List<string> { "FiltersGroups" }));
            }
            return errors;
        }
    }
}