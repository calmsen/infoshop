﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Forms
{
    public class PageForm
    {
        public long Id { get; set; }

        [Display(Name = "Заголовок")]
        [StringLength(128, ErrorMessage = "Заголовок не может содержать больше 128 символов")]
        [Required(ErrorMessage = "Введите заголовок")]
        public string Title { get; set; }

        public string TitleIn { get; set; }

        [Display(Name = "Название страницы")]
        [Required(ErrorMessage = "Введите название страницы")]
        [StringLength(128, ErrorMessage = "Заголовок не может содержать больше 128 символов")]
        public string Name { get; set; }

        public PageDescriptionForm Description { get; set; }

        public DateTime CreatedDate { get; set; }

        [Display(Name = "Шаблон страницы")]
        [StringLength(512, ErrorMessage = "Заголовок не может содержать больше 512 символов")]
        public string Layout { get; set; }

        [Display(Name = "Имя ссылки в главном меню")]
        [StringLength(128, ErrorMessage = "Заголовок не может содержать больше 128 символов")]
        public string NavLinkName { get; set; }
    }
    
}