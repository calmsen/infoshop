﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Forms
{
    public class FilterValueForm
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Значение не может быть пустым")]
        public string Title { get; set; }

        public string TitleIn { get; set; }

        public double ValueAsDouble { get; set; }
    }
}