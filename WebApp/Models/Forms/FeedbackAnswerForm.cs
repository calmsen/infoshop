﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Forms
{
    public class FeedbackAnswerForm
    {
        public SimpleFeedbackForm Feedback { get; set; }

        [Display(Name = "FeedbackAnswerFormMessageDisplayName", ResourceType = typeof(Resources.FeedbackRes))]
        [StringLength(4096, ErrorMessageResourceType = typeof(Resources.FeedbackRes), ErrorMessageResourceName = "FeedbackAnswerFormMessageStringRestrictionMessage")]
        public string Message { get; set; }

        [Display(Name = "FeedbackAnswerFormNoticedAuthorLabel", ResourceType = typeof(Resources.FeedbackRes))]
        public bool NoticedAuthor { get; set; }

        public string Hash { get; set; }

        public string AuthorEmail { get; set; }

        public bool NeedPickUp { get; set; }
    }
}