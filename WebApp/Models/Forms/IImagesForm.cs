﻿using System.Collections.Generic;

namespace WebApp.Models.Forms
{
    public interface IImagesForm
    {
        long MainImageId { get; set; }

        List<ImageId> ImagesIds { get; set; }
    }
}