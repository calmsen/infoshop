﻿namespace WebApp.Models.Forms
{
    public class ProductId
    {
        public long Value { get; set; }

        public string Title { get; set; }
    }
}