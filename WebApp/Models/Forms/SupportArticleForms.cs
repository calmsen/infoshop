﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace WebApp.Models.Forms
{
    public class SupportArticleForm
    {
        public long Id { get; set; }
        [Display(Name = "Заголовок новости")]
        [StringLength(128, ErrorMessage = "Заголовок не может содержать больше 128 символов")]
        [Required(ErrorMessage = "Введите заголовок")]
        public string Title { get; set; }
        public string TitleIn { get; set; }
        [Display(Name = "Аннотация к новости")]
        [StringLength(1024, ErrorMessage = "Аннотация не может содержать больше 1024 символов")]
        [Required(ErrorMessage = "Введите аннотацию к новости")]
        public string Annottion { get; set; }
        [StringLength(1024, ErrorMessage = "Аннотация не может содержать больше 1024 символов")]
        public string AnnottionIn { get; set; }
        [Required(ErrorMessage = "Введите описание новости")]
        public SupportArticleDescriptionForm Description { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}