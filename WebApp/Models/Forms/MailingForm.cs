﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebApp.Models.Forms
{
    public class MailingForm
    {
        [Display(Name = "Тема письма")]
        [Required(ErrorMessage = "Требуется ввести тему письма")]
        public string Subject { get; set; }

        [Display(Name = "Содержимое письма")]
        [Required(ErrorMessage = "Требуется ввести содержимое письма")]
        [AllowHtml]
        public string Body { get; set; }

        [Display(Name = "Отправить тестовое письмо")]
        public bool DoTest { get; set; }
    }
}