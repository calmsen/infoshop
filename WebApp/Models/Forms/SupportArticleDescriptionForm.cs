﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebApp.Models.Forms
{
    public class SupportArticleDescriptionForm
    {
        public long Id { get; set; }

        [AllowHtml]
        [Display(Name = "Описание новости")]
        public string Content { get; set; }

        [AllowHtml]
        public string ContentIn { get; set; }
    }
}