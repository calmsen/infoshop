﻿namespace WebApp.Models.Forms
{
    public class FeedbackTagForm
    {
        public string Title { get; set; }

        public long FeedbackId { get; set; }
    }
}