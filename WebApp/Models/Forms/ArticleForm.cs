﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Forms
{
    public class ArticleForm : IImagesForm
    {
        public long Id { get; set; }

        [Display(Name = "Заголовок новости")]
        [StringLength(128, ErrorMessage = "Заголовок не может содержать больше 128 символов")]
        [Required(ErrorMessage = "Введите заголовок")]
        public string Title { get; set; }

        public string TitleIn { get; set; }

        [Display(Name = "Аннотация к новости")]
        [StringLength(1024, ErrorMessage = "Аннотация не может содержать больше 1024 символов")]
        [Required(ErrorMessage = "Введите аннотацию к новости")]
        public string Annottion { get; set; }

        [StringLength(1024, ErrorMessage = "Аннотация не может содержать больше 1024 символов")]
        public string AnnottionIn { get; set; }

        public ArticleDescriptionForm Description { get; set; }

        [Range(1, Int32.MaxValue, ErrorMessage = "Загрузите картинку.")]
        public long MainImageId { get; set; }

        [Display(Name = "Картинки")]
        //[ListRange(1, ErrorMessage = "Загрузите картинку.")]
        public List<ImageId> ImagesIds { get; set; }

        [Display(Name = "Выберите категорию для привязки")]
        public long? SectionId { get; set; }

        [Display(Name = "Привязки к товарам")]
        public List<ProductId> ProductIds { get; set; }

        [Display(Name = "Активная")]
        public bool Active { get; set; }

        [Display(Name = "Позиция")]
        public int? Position { get; set; }

        //[Display(Name = "Дата и время")]
        //[DataType(DataType.DateTime)]
        //[DisplayFormat(DataFormatString = "{0:MM\\/dd\\/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime CreatedDate { get; set; }

        public ArticleForm()
        {
            ImagesIds = new List<ImageId>();
            ProductIds = new List<ProductId>();
            Active = true;
        }
    }
}