﻿using DomainLogic.Models.Enumerations;

namespace WebApp.Models.Forms
{
    public class FeedbackChangeThemeForm
    {
        public string ReturnUrl { get; set; }

        public long FeedbackId { get; set; }

        public FeedbackThemeEnum Theme { get; set; }
    }
}