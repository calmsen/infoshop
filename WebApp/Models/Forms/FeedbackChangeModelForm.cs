﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Forms
{
    public class FeedbackChangeModelForm
    {
        [Display(Name = "Катергория")]
        public long? SectionId { get; set; }

        [Display(Name = "Модель")]
        public long? ProductId { get; set; }

        public long FeedbackId { get; set; }

        public string ReturnUrl { get; set; }
    }
}