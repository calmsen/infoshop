﻿using DomainLogic.Models.Enumerations;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Forms
{             
    public class ServiceForm
    {
        public long Id { get; set; }

        [Display(Name = "Страна")]
        [StringLength(128, ErrorMessage = "Поле 'Название страны' не может содержать больше 128 символов")]
        [Required(ErrorMessage = "Введите название страны")]
        public string Country { get; set; }

        [Display(Name = "Город")]
        [StringLength(128, ErrorMessage = "Поле 'Название города' не может содержать больше 128 символов")]
        [Required(ErrorMessage = "Введите название города")]
        public string City { get; set; }

        [Display(Name = "Адрес")]
        [StringLength(128, ErrorMessage = "Поле 'Название адреса' не может содержать больше 128 символов")]
        [Required(ErrorMessage = "Введите адрес")]
        public string Address { get; set; }

        [Display(Name = "Телефон(ы)")]
        [StringLength(128, ErrorMessage = "Поле 'Телефон(ы)' не может содержать больше 128 символов")]
        public string Phones { get; set; }

        [Display(Name = "Режим работы")]
        [StringLength(128, ErrorMessage = "Поле 'Режим работы' не может содержать больше 512 символов")]
        public string WorkingHours { get; set; }

        [Display(Name = "Сервис")]
        public ServiceTypeEnum Type { get; set; }

        public ServiceForm()
        {
            Country = "РФ";
        }
    }
}