﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebApp.Models.Forms
{
    public class DescriptionForm
    {
        public long Id { get; set; }

        [AllowHtml]
        [Display(Name = "Описание")]
        [StringLength(8000, ErrorMessage = "Описание не может быть больше {1} символов")]
        public string Content { get; set; }

        [AllowHtml]
        [StringLength(8000, ErrorMessage = "Описание не может быть больше {1} символов")]
        public string ContentIn { get; set; }
    }
}