﻿using DomainLogic.Models.Enumerations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Forms
{
    public class FaqForm
    {
        public long Id { get; set; }

        [Display(Name = "Заголовок")]
        [MaxLength(256)]
        public string Title { get; set; }

        [MaxLength(256)]
        public string TitleIn { get; set; }

        [Display(Name = "Вопрос")]
        [Required(ErrorMessage = "Введите вопрос")]
        public string Question { get; set; }

        public string QuestionIn { get; set; }

        [Display(Name = "Ответ")]
        [Required(ErrorMessage = "Введите ответ")]
        public string Answer { get; set; }

        public string AnswerIn { get; set; }

        [Display(Name = "Привязка к разделу")]
        public long? SectionId { get; set; }

        [Display(Name = "Привязки к товарам")]
        public List<ProductId> ProductIds { get; set; }

        [Display(Name = "Тема")]
        [Required(ErrorMessage = "Выберите тему")]
        public FeedbackThemeEnum Theme { get; set; }

        [Display(Name = "Только для менеджеров")]
        public bool OnlyManager { get; set; }

        public FaqForm()
        {
            ProductIds = new List<ProductId>();
        }
    }
}