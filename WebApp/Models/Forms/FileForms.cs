﻿using DomainLogic.Models.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Forms
{
    public class FileForm
    {
        public long Id { get; set; }

        [Display(Name = "Название файла")]
        [Required(ErrorMessage = "Введите название файла")]
        public string Title { get; set; }

        public string TitleIn { get; set; }

        [Display(Name = "Описание файла")]
        public string Description { get; set; }

        public string DescriptionIn { get; set; }

        [Display(Name = "Версия файла")]
        [Required(ErrorMessage = "Введите версию файла")]
        public string Version { get; set; }

        [Display(Name = "Выберите фильтр")]
        public List<FileFilterEnum> Filters { get; set; }

        [Display(Name = "Укажите размер файла (байт)")]
        [Required(ErrorMessage = "Введите размер файла в байтах")]
        public string FileSize { get; set; }

        [Display(Name = "Или укажите ссылку на файл")]
        [StringLength(512, ErrorMessage = "Ссылка не может содержать больше чем 512 символов.")]
        public string ExternalLink { get; set; }

        public DateTime CreatedDate { get; set; }

        [Display(Name = "Отображать в товарах с аналогичным разделом")]
        public bool DisplayWithSameSection { get; set; }

        [Display(Name = "Отображать в товарах с аналогичной платформой")]
        public bool DisplayWithSamePlatform { get; set; }

        public FileForm()
        {
            Filters = new List<FileFilterEnum>();
        }
    }
}