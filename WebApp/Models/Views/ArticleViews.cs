﻿using System;
using System.Collections.Generic;

namespace WebApp.Models.Views
{               
    public class ArticleView
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Title { get; set; }

        public string Annottion { get; set; }

        public long? DescriptionId { get; set; }

        public long? MainImageId { get; set; }

        public ArticleImageView MainImage { get; set; }

        public List<ArticleImageView> Images { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool Active { get; set; }

        public int Position { get; set; }

        public ArticleDescriptionView Description { get; set; }

        public List<ProductView> Products { get; set; }
    }
}