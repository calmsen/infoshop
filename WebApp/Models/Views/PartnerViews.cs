﻿using System;

namespace WebApp.Models.Views
{
    /// <summary>
    /// Данный класс представляет из себя данные о партнере для отдачи непосредственно в представление.
    /// Может отличаться от доменной модели Partner.
    /// </summary>
    public class PartnerView
    {
        public long Id { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// Название партнера
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Сайт партнера
        /// </summary>
        public string LinkToPartner { get; set; }

        /// <summary>
        /// Логотип партнера
        /// </summary>
        public long? MainImageId { get; set; }

        /// <summary>
        /// Номер позиции в списке партнеров. Для того чтобы выделить топ партнеров.
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Дата создания записи о партнере
        /// </summary>
        public DateTime CreatedDate { get; set; }
    }

    


    
}