﻿namespace WebApp.Models.Views
{
    public class PostDescriptionView
    {
        public long Id { get; set; }

        public string Content { get; set; }
    }
}