﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebApp.Models.Views
{
    public class DropDownMenu<TValue>
    {
        public string ActiveItemText { get; private set; }

        public List<DropDownMenuItem<TValue>> Items { get; set; }

        public Dictionary<string, object> Params { get; set; }

        public string ItemParamName { get; set; }

        public void SetActiveItemByValue(TValue value)  
        {
            foreach (DropDownMenuItem<TValue> item in Items) 
            {
                if (item.Value.Equals(value))
                {
                    ActiveItemText = item.Text;
                    item.Active = true;
                }
                else 
                {
                    item.Active = false;
                }
            }
        }

        public void ComputeParams() 
        {
            foreach (DropDownMenuItem<TValue> item in Items) 
            {
                item.Params = Params.ToDictionary(x => x.Key, y => y.Value);
                item.Params.Add(ItemParamName, item.Value);
            }
        }
    }    
}