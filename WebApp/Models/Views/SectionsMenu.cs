﻿using System.Collections.Generic;

namespace WebApp.Models.Views
{
    public class SectionsMenu
    {
        public SectionView Section { get; set; }

        public List<SectionView> SectionsWC { get; set; }

        public string Action { get; set; }

        public string Controller { get; set; }

        public Dictionary<string, object> Params { get; set; }
    }
}