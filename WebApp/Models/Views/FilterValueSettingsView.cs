﻿namespace WebApp.Models.Views
{
    public class FilterValueSettingsView
    {
        public long Id { get; set; }

        public bool Activated { get; set; }
    }
}