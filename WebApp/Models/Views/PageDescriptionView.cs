﻿namespace WebApp.Models.Views
{
    public class PageDescriptionView
    {
        public long Id { get; set; }

        public string Content { get; set; }
    }
}