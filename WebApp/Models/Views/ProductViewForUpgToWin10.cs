﻿namespace WebApp.Models.Views
{
    public class ProductViewForUpgToWin10
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public bool Upgrade { get; set; }
    }
}