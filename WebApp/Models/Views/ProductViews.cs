﻿using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace WebApp.Models.Views
{          
    public class ProductView
    {
        public ProductView()
        {
            Attributes = new List<AttributeView>();
            Images = new List<ImageView>();
            PartnersLinksOnProduct = new List<PartnerLinkOnProduct>();
            BlockViews = new List<ProductBlockView>();
        }

        public long Id { get; set; }

        public string Name { get; set; }

        public string ShortFeatures { get; set; }

        public List<AttributeView> Attributes { get; set; }

        public long SectionId { get; set; }

        public SectionView Section { get; set; }

        public long? DescriptionId { get; set; }

        public DescriptionView Description { get; set; }

        public long? MainImageId { get; set; }

        public ImageView MainImage { get; set; }

        public double MainImageK { get; set; }

        public List<ImageView> Images { get; set; }

        public string Sertificate { get; set; }

        public ImageView BannerImage { get; set; }

        public int BannerPosition { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool ToBanner { get; set; }

        public bool ToRating { get; set; }

        public bool AllowFiles { get; set; }

        public bool Published { get; set; }

        public bool Filled { get; set; }

        public bool Active { get; set; }

        public bool Archive { get; set; }

        public bool New { get; set; }

        public List<PartnerLinkOnProduct> PartnersLinksOnProduct { get; set; }

        public int? FilledFeatures { get; set; }

        public string Comment { get; set; }

        public float Price { get; set; }

        public string ExternalLink { get; set; }

        public List<PostView> Posts { get; set; }

        public long? Code { get; set; }

        public long? Barcode { get; set; }

        public long? StoreState { get; set; }

        public int ViewsAmount { get; set; }

        public List<FaqView> Faq { get; set; }

        public string Title { get; set; }

        public long? BannerImageId { get; set; }

        public string FilterValueByGroupType { get; set; }

        public string Preview { get; set; }

        public List<ProductBlockView> BlockViews { get; set; }

        public List<AttributeView> GetAttributes(List<FilterSettingsView> filters) 
        {
            List<AttributeView> attrs = new List<AttributeView>();
            foreach (var filter in filters)
            {
                foreach (var a in Attributes)
                {
                    if (a.FilterId != filter.Id)
                        continue;
                    attrs.Add(a);
                    break;
                    
                }
            }
            return attrs;
        }

        public AttributeView GetAttributeByFilterId(long filterId) 
        {
            return Attributes.FirstOrDefault(x => x.FilterId == filterId);
        }

        public AttributeView GetAttributeByFilterTitle(string filterTitle)
        {
            return Attributes.FirstOrDefault(x => x.Filter.Title.Equals(filterTitle));
        }

        private AttributeView _model;
        public AttributeView Model
        {
            get
            { 
                if (_model == null) 
                    _model = GetAttributeByFilterTitle("Модель");
                return _model;    
            } 
        }

        public string TitleAsModel 
        { 
            get
            {
                string model = Regex.Replace(Title, @"[а-яА-Я/]+|\(.+\)|InfoShop", "");
                model = Regex.Replace(model, @"^[0-9.""]+", "");
                return model;
            }                           
        }

        public bool Different(AttributeView attr, List<ProductView> products)
        {
            var different = false;
            var v = attr.GetValuesAsString();
            foreach (ProductView p in products)
            {
                if (!p.GetAttributeByFilterId(attr.FilterId).GetValuesAsString().Equals(v))
                {
                    different = true;
                    break;
                }
            }
            return different;
        }

        public CompareProductBtn CompareProductBtn { get; set; }
    }
}