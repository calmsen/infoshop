﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebApp.Models.Views
{
    public class AttributeView
    {
        public AttributeView()
        {
            Values = new List<ValueView>();
        }

        public long Id { get; set; }

        public long ProductId { get; set; }

        public long FilterId { get; set; }

        public FilterView Filter { get; set; }

        public List<ValueView> Values { get; set; }

        public FilterSettingsView FilterSettings { get; set; }

        public string GetValueAsString(ValueView v)
        {
            string valueAsString = v.Value == null ? string.Empty : v.Value.Title;
            var unit = Filter.Unit;
            var unitConverterIdForProduct = Filter.UnitConverterIdForProduct;
            if (FilterSettings.UnitConverterIdForProduct != null)
            {
                unitConverterIdForProduct = (long)FilterSettings.UnitConverterIdForProduct;
            }
            // покажем число в нужной единице измерения
            if (!string.IsNullOrEmpty(Filter.Unit) && unitConverterIdForProduct >= 0)
            {
                double value = v.Value == null ? 0 : v.Value.ValueAsDouble;
                if (unitConverterIdForProduct > 0)
                {
                    var converter = Filter.UnitConverters.Where(x => x.Id == unitConverterIdForProduct).FirstOrDefault();
                    if (converter != null)
                    {
                        var unitK = converter.UnitK.Split(':');
                        value = (double)value * Convert.ToInt32(unitK[1]) / Convert.ToInt32(unitK[0]);
                        unit = converter.Unit;
                    }
                }
                value = Math.Round(value, 2);
                valueAsString = value.ToString();
            }
            // покажем null если значение пустое.
            // Внимание: такая ситуция не должна возникнуть. Если возникла значит произошла непредвиденная ошибка
            if (string.IsNullOrEmpty(valueAsString))
            {
                valueAsString = "null";
            }
            // если значение имеет не числовые символы, значит к ней не надо добавлять единицу измерения
            if (!System.Text.RegularExpressions.Regex.IsMatch(valueAsString, @"^\s*[\d.,]+\s*$"))
            {
                unit = null;
            }
            return valueAsString + " " + unit;
        }

        private string _valuesAsString;
        public string GetValuesAsString()
        {
            if (_valuesAsString == null)
            {
                _valuesAsString = "";
                for (var i = 0; i < Values.Count; i++)
                {
                    if (i > 0)
                        _valuesAsString += ",";
                    _valuesAsString += GetValueAsString(Values[i]);
                }
            }
            return _valuesAsString;
        }
    }
}