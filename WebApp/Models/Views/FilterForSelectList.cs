﻿using DomainLogic.Models.Enumerations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.Web.Mvc;

namespace WebApp.Models.Views
{
    public class FilterForSelectList
    {
        public long Id { get; set; }

        public string Title { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public FilterTypeEnum Type { get; set; }

        public List<SelectListItem> List { get; set; }

        public bool Multiple { get; set; }

        public string Unit { get; internal set; }
    }
}