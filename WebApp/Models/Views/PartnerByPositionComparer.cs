﻿using System.Collections.Generic;

namespace WebApp.Models.Views
{
    public class PartnerByPositionComparer : IComparer<PartnerView>
    {
        public int Compare(PartnerView p1, PartnerView p2)
        {
            if (p1.Position == 0)
                return 1;
            else if (p1.Position > p2.Position)
                return 1;
            else if (p1.Position < p2.Position)
                return -1;
            else
                return 0;
        }
    }
}