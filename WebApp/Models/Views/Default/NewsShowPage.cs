﻿using DomainLogic.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Extensions;
using WebApp.Models.Views;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class NewsShowPage : MainLayout
    {
        [Inject]
        public INewsService NewsService { get; set; }

        public ArticleView Article { get; set; }

        public List<ArticleView> News { get; set; }
        
        public async Task InitAsync(long id)
        {
            Article = (await NewsService.GetArticleAsync(id))?.Map<ArticleView>();
            if (Article == null)
                return;
            //
            var paginationForNews = new Pagination
            {
                CurrentPage = 1,
                PageItemsAmount = 5
            };
            News = (await NewsService.GetNewsAsync(paginationForNews, true, new Sort { ColumnName = "Position" }))
                .MapList<ArticleView>();
            //
            await InitAsync(null, 0, false);
        }
    }
}