﻿using DomainLogic.Models;

namespace WebApp.Models.Views.Default
{
    public class ViewModelHolder<T> : MainLayout
    {
        public T ViewModel { get; set; }

    }
}