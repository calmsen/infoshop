﻿using System.Threading.Tasks;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using DomainLogic.Models.Enumerations;

namespace WebApp.Models.Views.Default
{
    public class VacanciesBodyLayout : MainLayout
    {
        [Inject]
        public IVacanciesService VacanciesService { get; set; }

        public VacanciesSidebar Sidebar { get; set; }

        protected async Task InitVacanciesBodyLayoutAsync(long activeVacancyId, string activeSideMenuLink, string activeNavLink)
        {
            Sidebar = await GetSidebarAsync(activeVacancyId, activeSideMenuLink);
            await InitAsync(activeNavLink, 0, false);
        }

        private async Task<VacanciesSidebar> GetSidebarAsync(long activeVacancyId, string activeSideMenuLink)
        {
            VacancyView vacancy = null;
            if (activeVacancyId > 0)
                vacancy = (await VacanciesService.GetVacancyAsync((int)activeVacancyId))
                    .Map<VacancyView>();
            var vacancies = (await VacanciesService.GetVacanciesAsync(VacancyStateEnum.Active))
                .MapList<VacancyView>();
            return new VacanciesSidebar
            {
                ActiveVacancy = vacancy,
                ActiveSideMenuLink = activeSideMenuLink,
                Vacancies = vacancies,
            };
        }
    }
}