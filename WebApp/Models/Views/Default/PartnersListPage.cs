﻿using DomainLogic.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class PartnersListPage : MainLayout
    {
        [Inject]
        public IPartnersService PartnersService { get; set; }

        public List<PartnerView> Partners { get; set; }

        public Pagination Pagination { get; set; }

        public async Task InitAsync(int page)
        {
            Pagination = new Pagination
            {
                CurrentPage = page,
                PageItemsAmount = 36
            };
            Pagination.RouteValues.Add("controller", "Partners");
            Pagination.RouteValues.Add("action", "Index");
            //
            Partners = (await PartnersService.GetPartnersAsync(Pagination))
                .MapList<PartnerView>();
            await InitAsync("Partners", 0, false);
        }
        
    }
}