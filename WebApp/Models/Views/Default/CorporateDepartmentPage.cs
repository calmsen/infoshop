﻿using DomainLogic.Interfaces.Services;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class CorporateDepartmentPage : AboutBodyLayout
    {
        [Inject]
        public IPagesService PagesService { get; set; }

        public PageView Page { get; set; }

        public async Task InitAsync()
        {
            Page page = await PagesService.GetPageByNameAsync("CorporateDepartment");
            Page = page?.Map<PageView>();
            await InitAboutBodyLayoutAsync("CorporateDepartment", "About");
        }
    }
}