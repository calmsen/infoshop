﻿using DomainLogic.Interfaces.Services;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class PostsShowPage : MainLayout
    {
        [Inject]
        public IPostsService PostsService { get; set; }

        public PostView Post { get; set; }

        public async Task InitAsync(long id)
        {
            Post = (await PostsService.GetPostByIdAsync(id))?
                .Map<PostView>();
            if (Post == null)
                return;
            await InitAsync(null, 0, false);
        }
    }
}