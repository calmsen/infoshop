﻿using DomainLogic.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class MyFeedbackListPage : MainLayout
    {
        [Inject]
        public IFeedbacksService FeedbacksService { get; set; }

        public List<FeedbackView> Feedbacks { get; set; }

        public async Task InitAsync(string userEmail)
        {
            Feedbacks = (await FeedbacksService.GetMyFeedbacksAsync(userEmail)).MapList<FeedbackView>();

            await base.InitAsync(null, 0, false);
        }
    }
}