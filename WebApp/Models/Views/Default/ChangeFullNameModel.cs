﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Models.Views.Default
{
    public class ChangeFullNameModel
    {
        [Display(Name = "ChangeFullNameFormDisplayName", ResourceType = typeof(Resources.AccountRes))]
        [Required(ErrorMessageResourceType = typeof(Resources.AccountRes), ErrorMessageResourceName = "ChangeFullNameFormRequiredMessage")]
        public string FullName { get; set; }

    }
}