﻿using DomainLogic.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApp.UiHelpers;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Providers;

namespace WebApp.Models.Views.Default
{
    public class MainLayout: ICloneable<MainLayout>
    {
        [Inject]
        public IUsersService UsersService { get; set; }
        [Inject]
        public ISectionsService SectionsService { get; set; }
        [Inject]
        public IOptionsService OptionsService { get; set; }
        [Inject]
        public SectionUiHelpers SectionUiHelpers { get; set; }
        [Inject]
        public UserUiHelpers UserUiHelpers { get; set; }

        public HeaderPartial Header { get; set; }

        public NeckPartial Neck { get; set; }

        public FooterPartial Footer { get; set; }

        protected User _currentUser;

        protected Product _product;

        protected List<Section> _allSections;


        public virtual async Task InitAsync(string activeNavLink, long activeSectionId, bool archive)
        {
            _currentUser = await UserUiHelpers.GetCurrentUserAsync();
            Header = await GetHeaderModelAsync(activeNavLink);
            Neck = await GetNeckAsync(activeNavLink, activeSectionId, archive);
            Footer = await GetFooterAsync();
        }

        private async Task<HeaderPartial> GetHeaderModelAsync(string activeNavLink)
        {
            List<Section> allSections = _allSections ?? (_allSections = await SectionsService.GetSectionsAsync());
            var sectionsWC = SectionsService.SectionsToTreeFromList(allSections, 0)
                .MapList<SectionView>();
            Dictionary<long, Positions> positionsOfSections = await OptionsService.GetPositionsOfSectonsAsync();

            return new HeaderPartial
            {
                ActiveNavLink = activeNavLink,
                Sections = sectionsWC,
                PositionsOfSections = positionsOfSections,
                CurrentUser = _currentUser?.Map<UserView>(),
                HeaderAccountInfo = GetHeaderAccountInfo()
            };
        }
        
        private async Task<NeckPartial> GetNeckAsync(string activeNavLink, long activeSectionId, bool archive = false)
        {
            if (!string.Equals(activeNavLink, "Products"))
                return new NeckPartial
                {
                    IsVisible = false
                };

            List<Section> allSections = _allSections ?? (_allSections = await SectionsService.GetSectionsAsync());
            List<Breadcrumb> breadcrumbs = SectionUiHelpers.GetBreadcrumbsFromSections(allSections, activeSectionId);

            return new NeckPartial
            {
                IsVisible = true,
                Breadcrumbs = breadcrumbs,
                Product = _product.Map<ProductView>(),
                ActiveSectionId = activeSectionId,
                Archive = archive
            };
        }

        private async Task<FooterPartial> GetFooterAsync()
        {
            bool managerLinksIsVisible = _currentUser != null ? 
                await UsersService.IsInRoleAsync(_currentUser.Email, "Manager")
                : false;
            
            return new FooterPartial
            {
                ManagerLinksIsVisible = managerLinksIsVisible
            };
        }

        private HeaderAccountInfoPartial GetHeaderAccountInfo()
        {
            return new HeaderAccountInfoPartial
            {
                IsShowAuthorizedUserLinks = _currentUser != null
            };
        }

        public MainLayout Clone()
        {
            return MemberwiseClone() as MainLayout;
        }
    }
}