﻿using DomainLogic.Infrastructure.Paging;
using System.Collections.Generic;

namespace WebApp.Models.Views.Default
{
    public class SidebarView
    {
        public List<SectionView> Sections { get; set; }

        public List<FilterParam> FParams { get; set; }

        public SectionView ActiveSection { get; set; }

        public bool Archive { get; set; }

        public string SortType { get; set; }

        public string GroupType { get; set; }

        public int SelectType { get; set; }

        public double[] Price { get; set; }

        public string ViewType { get; set; }

        public List<FilterView> Filters { get; set; }
    }
}