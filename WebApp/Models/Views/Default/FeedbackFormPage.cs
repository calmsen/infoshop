﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebApp.UiHelpers;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Extensions;
using WebApp.Models.Forms;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class FeedbackFormPage : MainLayout
    {
        [Inject]
        public FeedbackUiHelpers FeedbackUiHelpers { get; set; }

        public FeedbackForm Feedback { get; set; }

        public List<SelectListItem> FeedbackThemesSelectList { get; set; }

        public List<SelectListItem> ProductTitlesForSelectList { get; set; }

        public List<SelectListItem> SectionsForSelectList { get; set; }

        public List<FaqView> Faq { get; set; }

        public async Task InitAsync(FeedbackForm form)
        {
            var selectLists = await FeedbackUiHelpers.GetSelectListsAsync(form.SectionId);
            
            Feedback = form;
            FeedbackThemesSelectList = selectLists.Item1;
            SectionsForSelectList = selectLists.Item2;
            ProductTitlesForSelectList = selectLists.Item3;
            Faq = new List<Faq>().MapList<FaqView>();

            await base.InitAsync(null, 0, false);
        }
    }
}