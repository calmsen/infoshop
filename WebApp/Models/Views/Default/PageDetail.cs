﻿using DomainLogic.Interfaces.Services;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class PageDetail : MainLayout
    {
        [Inject]
        public IPagesService _pagesService { get; set; }

        public PageView Page { get; set; }

        public async Task InitAsync(string pageName)
        {
            Page page = await _pagesService.GetPageByNameAsync(pageName);
            if (page == null)
                return;

            Page = page?.Map<PageView>();
            await InitAsync(page?.NavLinkName, 0, false);
        }
        
    }
}