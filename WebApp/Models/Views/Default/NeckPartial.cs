﻿using DomainLogic.Models;
using System.Collections.Generic;

namespace WebApp.Models.Views.Default
{
    public class NeckPartial
    {
        public Dictionary<long, Positions> PositionsOfSections { get; set; }

        public List<Breadcrumb> Breadcrumbs { get; set; }

        public ProductView Product { get; set; }

        public long ActiveSectionId { get; set; }

        public bool Archive { get; set; }

        public bool IsVisible { get; internal set; }
    }
}