﻿using DomainLogic.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Models.Enumerations;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class ServicesPage : SupportBodyLayout
    {
        [Inject]
        public ISrvsService SrvsService { get; set; }
        [Inject]
        public IPartnersService PartnersService { get; set; }

        public List<ServiceView> Services { get; set; }

        public List<string> CitiesForSelectList { get; set; }

        public List<string> PartnersForSelectList { get; set; }

        public string ActiveCity { get; set; }

        public string ActiveType { get; set; }

        public new async Task InitAsync(string city, string partner)
        {
            var services = new List<ServiceView>();
            if (city != null && partner != null)
            {
                var type = ServiceTypeEnum.Service1;
                if (partner.Equals("Other"))
                    type = ServiceTypeEnum.Others;
                services = (await SrvsService.GetServicesAsync(city, type))
                    .MapList<ServiceView>();
            }
            List<string> citiesForSelectList = await SrvsService.GetCitiesForSelectListAsync();
            // TODO: add partners to services map
            List<string> partnersForSelectList = new List<string>
            {
                "Partner1", "Partner2", "Partner3", "Other"
            };
            Services = services;
            CitiesForSelectList = citiesForSelectList;
            PartnersForSelectList = partnersForSelectList;
            ActiveCity = city;
            ActiveType = partner;

            await base.InitAsync("Srvs", "Files");
        }
        
    }
}