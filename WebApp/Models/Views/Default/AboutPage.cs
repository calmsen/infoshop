﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System.Threading.Tasks;

namespace WebApp.Models.Views.Default
{
    public class AboutPage : AboutBodyLayout
    {
        [Inject]
        public IPagesService PagesService { get; set; }

        public PageView Page { get; set; }

        public async Task InitAsync()
        {
            Page page = await PagesService.GetPageByNameAsync("About");
            Page = page?.Map<PageView>();
            await InitAboutBodyLayoutAsync("About", "About");
        }
    }
}