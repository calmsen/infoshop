﻿using System.Collections.Generic;

namespace WebApp.Models.Views.Default
{
    public class FilesData
    {
        public List<FileView> Files { get; set; }
    }
}