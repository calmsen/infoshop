﻿using System.Collections.Generic;

namespace WebApp.Models.Views.Default
{
    public class UpgradeToWindows10Page
    {
        public List<ProductViewForUpgToWin10> ProductCodes { get; set; }
    }
}