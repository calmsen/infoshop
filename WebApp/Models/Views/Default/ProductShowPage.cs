﻿using DomainLogic.Interfaces;
using DomainLogic.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebApp.UiHelpers;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Models.Enumerations;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class ProductShowPage : MainLayout
    {
        [Inject]
        public IProductsService ProductsService { get; set; }
        [Inject]
        public IResourcesService ResourcesService { get; set; }
        [Inject]
        public IPartnersService PartnersService { get; set; }
        [Inject]
        public IFaqService FaqService { get; set; }
        [Inject]
        public IFilesService FilesService { get; set; }
        [Inject]
        public IResourceProvider ResourceProvider { get; set; }
        [Inject]
        public FileUiHelpers FileUiHelpers { get; set; }
        [Inject]
        public ProductUiHelpers ProductUiHelpers { get; set; }

        public ProductView Product { get; set; }

        public List<SelectListItem> FileFiltersSelectList { get; set; }

        public List<FileView> Files { get; set; }

        public List<PartnerView> Partners { get; set; }

        public ProductEditLink ProductEditLink { get; set; }

        public ProductImagesArchiveLink ProductImagesArchiveLink { get; set; }

        public CompareProductsBtn CompareProductsBtn { get; set; }        

        public async Task InitAsync(long productId, bool archive = false)
        {
            _product = await ProductsService.GetProductByIdAsync(productId);
            if (_product == null)
                return;
            var productView = _product.Map<ProductView>();
            if (!string.IsNullOrEmpty(productView.ShortFeatures))
            {
                productView.ShortFeatures = Regex.Replace(productView.ShortFeatures, @"^\[|\]$", "").Trim();
            }
            SetBlockViews(productView);

            ProductUiHelpers.FilterAttrsAndGroups(productView);
            // получим файлы по товару
            var files = (await FilesService.GetFilesAsync(FileFilterEnum.None, productId))
                .MapList<FileView>();
            // -- -- --                  
            List<SelectListItem> fileFiltersSelectList = FileUiHelpers.GetFileFiltersForSelectList(new SelectListItem
            {
                Value = "",
                Text = ResourceProvider.ReadResource("Resources.ProductsRes.FileFiltersFirstSelectItemText")
            });
            //
            var partners = (await PartnersService.GetPartnersByIdsAsync(productView.PartnersLinksOnProduct.Select(x => x.Id).ToList()))
                .MapList<PartnerView>();
            partners.Sort(new PartnerByPositionComparer());
            //
            productView.Faq = (await FaqService.GetFaqAsync(productView.SectionId, productView.Id))
                .MapList<FaqView>();
            //
            productView.CompareProductBtn = ProductUiHelpers.GetCompareProductBtn(productView.Id);
            //
            Product = productView;
            Partners = partners;
            FileFiltersSelectList = fileFiltersSelectList;
            Files = files;

            ProductEditLink = await GetProductEditLinkAsync(productView.Id);
            ProductImagesArchiveLink = await GetProductImagesArchiveLinkAsync(productView.Id);
            CompareProductsBtn = ProductUiHelpers.GetCompareProductsBtn(productView.SectionId);

            await InitAsync("Products", productView.SectionId, archive);
        }
        
        private void SetBlockViews(ProductView productView)
        {
            if (productView.Description == null || string.IsNullOrWhiteSpace(productView.Description.Content))
                return;
            // выберем все блоки из описания
            if (productView.Description.Content.IndexOf("[/block]") >= 0)
            {
                string description = productView.Description.Content;
                description = Regex.Replace(description, @"<br\s*/>", "");
                description = Regex.Replace(description, @"<p>\s*\[block", "[block");
                description = Regex.Replace(description, @"\[/block\]\s*</p>", "[/block]");
                string[] blocks = description.Split(new string[] { "[/block]" }, StringSplitOptions.None);
                description = string.Empty; // описание будет содержать все данные не заключенные в блоки
                foreach (string block in blocks)
                {
                    if (block.IndexOf("[block") == -1) // такой случай может быть если блок является последним
                    {
                        description += block;
                        continue;
                    }
                    string[] blockParts = block.Split(new string[] { "[block" }, StringSplitOptions.None);
                    if (blockParts.Length != 2)
                        continue;
                    description += blockParts[0].Trim();
                    string props = blockParts[1].Substring(0, blockParts[1].IndexOf("]")).Trim();
                    props = props.Replace("&nbsp;", " ");
                    props = Regex.Replace(props, "\\s+", " ");
                    props = props.Replace(" = ", "");
                    props = Regex.Replace(props, "\\s([a-zA-Z]+=)", "<!>$1");
                    if (string.IsNullOrEmpty(props))
                        continue;
                    string[] propsAsArray = props.Split(new string[] { "<!>" }, StringSplitOptions.None);
                    ProductBlockView blockView = new ProductBlockView();
                    for (int i = 0; i < propsAsArray.Length; i++)
                    {
                        blockView.Props.Add(new ProductBlockProperty
                        {
                            Name = propsAsArray[i].Substring(0, propsAsArray[i].IndexOf("=")),
                            Value = propsAsArray[i].Substring(propsAsArray[i].IndexOf("=") + 1).Trim()
                        });
                    }
                    ProductBlockProperty nameProp = blockView.Props.FirstOrDefault(x => x.Name.Equals("name"));
                    if (nameProp == null)
                        continue;
                    blockView.Name = nameProp.Value;
                    string content = blockParts[1].Substring(blockParts[1].IndexOf("]") + 1).Trim();
                    blockView.Content = content;
                    productView.BlockViews.Add(blockView);
                }
                productView.Description.Content = description;
            }
            // если найдется блок с названием "description", то перезапишем описание
            ProductBlockView descriptionBlockView = productView.BlockViews.FirstOrDefault(x => x.Name.Equals("description"));
            if (descriptionBlockView != null && !string.IsNullOrWhiteSpace(descriptionBlockView.Content))
                productView.Description.Content = descriptionBlockView.Content;
            // если найдется блок с "preview", то определим обзор
            ProductBlockView previewBlockView = productView.BlockViews.FirstOrDefault(x => x.Name.Equals("preview"));
            if (previewBlockView != null)
                productView.Preview = previewBlockView.Content;



            string pagebreak = "<!-- pagebreak -->";
            if (productView.Description.Content.IndexOf(pagebreak) > 0)
            {
                // если "preview" пустое то найдем его в описании
                if (string.IsNullOrEmpty(productView.Preview))
                    productView.Preview = productView.Description.Content.Substring(productView.Description.Content.IndexOf(pagebreak) + pagebreak.Length);
                // очистим описание от <!-- pagebreak -->
                productView.Description.Content = productView.Description.Content.Substring(0, productView.Description.Content.IndexOf(pagebreak));
            }

            // удалим блоки которые завязаны на табы, если блок "tabs" будет найден
            if (productView.BlockViews.FirstOrDefault(x => x.Name.Equals("tabs")) != null)
            {
                for (int i = 0; i < productView.BlockViews.Count; i++)
                {
                    if (productView.BlockViews[i].Name.Equals("description") || productView.BlockViews[i].Name.Equals("preview")
                        || productView.BlockViews[i].Name.Equals("gallery") || productView.BlockViews[i].Name.Equals("attributes")
                        || productView.BlockViews[i].Name.Equals("files") || productView.BlockViews[i].Name.Equals("faq"))
                    {
                        productView.BlockViews.RemoveAt(i);
                        i--;
                    }
                }
            }
        }
                
        private async Task<ProductEditLink> GetProductEditLinkAsync(long productId)
        {
            bool isProductEditLinkShowed = _currentUser != null 
                ? await UsersService.IsInRoleAsync(_currentUser.Email, "Manager")
                : false;
            
            return new ProductEditLink
            {
                IsProductEditLinkShowed = isProductEditLinkShowed,
                ProductId = productId
            };
        }

        private async Task<ProductImagesArchiveLink> GetProductImagesArchiveLinkAsync(long productId)
        {
            bool isProductImagesArchiveLinkShowed = _currentUser != null
                ? await UsersService.IsInRoleAsync(_currentUser.Email, "Manager")
                : false;

            return new ProductImagesArchiveLink
            {
                IsProductImagesArchiveLinkShowed = isProductImagesArchiveLinkShowed,
                ProductId = productId
            };
        }
    }    
}