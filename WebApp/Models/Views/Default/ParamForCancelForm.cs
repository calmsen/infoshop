﻿namespace WebApp.Models.Views.Default
{
    public class ParamForCancelForm
    {
        public string Name { get; set; }

        public string Value { get; set; }

        public string Text { get; set; }
    }
}