﻿namespace WebApp.Models.Views.Default
{
    public class MessageViewModel
    {
        public string PageTitle { get; set; }

        public string Message { get; set; }
    }
}