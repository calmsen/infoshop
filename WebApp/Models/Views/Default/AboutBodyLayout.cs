﻿using DomainLogic.Models;
using System.Threading.Tasks;

namespace WebApp.Models.Views.Default
{
    public class AboutBodyLayout : MainLayout
    {
        public string ActiveSideMenuLink { get; set; }
        
        protected async Task InitAboutBodyLayoutAsync(string activeSideMenuLink, string activeNavLink)
        {
            ActiveSideMenuLink = activeSideMenuLink;
            await InitAsync(activeNavLink, 0, false);
        }
    }
}