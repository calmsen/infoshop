﻿using DomainLogic.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.UiHelpers;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class CompareProductsPage : MainLayout
    {
        [Inject]
        public IProductsService ProductsService { get; set; }
        [Inject]
        public ProductUiHelpers ProductUiHelpers { get; set; }

        public List<ProductView> Products { get; set; }

        public SectionView Section { get; set; }

        public async Task InitAsync(long sectionId)
        {
            var sectionView = (await SectionsService.GetSectionByIdAsync(sectionId))?
                .Map<SectionView>();
            if (sectionView == null)
                return/* new HttpStatusCodeResult(400, "Не правильно указан раздел.")*/;
            List<ProductView> productViews = new List<ProductView>();
            ProductInComparison[] productsInComparison = ProductUiHelpers.GetProductsInComparison();
            foreach (ProductInComparison p in productsInComparison.Where(x => x.SectionId == sectionId))
            {
                var productView = (await ProductsService.GetProductByIdAsync(p.ProductId))?
                    .Map<ProductView>();
                if (productView == null)
                    continue;
                productViews.Add(productView);
            }
            //
            foreach (ProductView p in productViews)
                ProductUiHelpers.FilterAttrsAndGroups(p);
            AddEmptyAttributes(productViews);
            // установим данные для кнопки сравнения
            foreach (var p in productViews)
                p.CompareProductBtn = ProductUiHelpers.GetCompareProductBtn(p.Id);
            //

            Section = sectionView;
            Products = productViews;

            await InitAsync("Products", 0, false);
        }
        
        private void AddEmptyAttributes(List<ProductView> productViews)
        {
            foreach (ProductView p1 in productViews)
            {
                foreach (ProductView p2 in productViews)
                {
                    if (p1.Id == p2.Id)
                        continue;
                    AddEmptyAttributes(p1, p2);
                }
            }
        }

        private void AddEmptyAttributes(ProductView productView1, ProductView productView2)
        {
            foreach (AttributeView a2 in productView2.Attributes)
            {
                bool find = false;
                foreach (AttributeView a1 in productView1.Attributes)
                {
                    if (a1.FilterId == a2.FilterId)
                    {
                        find = true;
                        break;
                    }
                }
                if (!find)
                    productView1.Attributes.Add(new AttributeView { ProductId = productView1.Id, FilterId = a2.FilterId, Filter = a2.Filter });
            }
        }
    }
}