﻿using System.Threading.Tasks;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Models;

namespace WebApp.Models.Views.Default
{
    public class VacancyShowPage : VacanciesBodyLayout
    {
        public VacancyView Vacancy { get; set; }

        public async Task InitVacancyShowPageAsync(int id)
        {
            Vacancy = (await VacanciesService.GetVacancyAsync(id))?
                .Map<VacancyView>();
            Vacancy.Duties = MapNewlineToBrTag(Vacancy.Duties);
            Vacancy.AdditionalRequirements = MapNewlineToBrTag(Vacancy.AdditionalRequirements);
            Vacancy.WorkingConditions = MapNewlineToBrTag(Vacancy.WorkingConditions);

            await InitVacanciesBodyLayoutAsync(id, "Vacancies", "About");
        }

        private string MapNewlineToBrTag(string text)
        {
            return text?.Replace("\r\n", "<br/>").Replace("\n", "<br/>");
        }

    }
}