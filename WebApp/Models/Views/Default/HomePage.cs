﻿using DomainLogic.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{

    public class HomePage : MainLayout
    {
        [Inject]
        public INewsService NewsService { get; set; }
        [Inject]
        public IPostsService PostsService { get; set; }
        [Inject]
        public IProductsService ProductsService { get; set; }

        public List<ArticleView> News { get; set; }

        public List<PostView> Posts { get; set; }

        public List<ProductView> Products { get; set; }

        public List<ProductView> ProductsForBanner { get; set; }

        public List<ProductView> HitProducts { get; set; }

        public List<ProductView> NewProducts { get; set; }

        public async Task InitAsync()
        {
            var paginationForNews = new Pagination
            {
                CurrentPage = 1,
                PageItemsAmount = 5
            };

            var paginationForPosts = new Pagination
            {
                CurrentPage = 1,
                PageItemsAmount = 4
            };
            //
            List<Article> news = await NewsService.GetNewsAsync(paginationForNews, true, new Sort { ColumnName = "Position" });
            List<Post> posts = await PostsService.GetPostsAsync(paginationForPosts, true);
            List<Product> productsForBanner = await ProductsService.GetProductsForBannerAsync();
            List<Product> productsForRating = await ProductsService.GetHitProductsAsync();
            List<Product> newProducts = await ProductsService.GetNewProductsAsync();


            News = news.MapList<ArticleView>();
            Posts = posts.MapList<PostView>();
            ProductsForBanner = productsForBanner.MapList<ProductView>();
            HitProducts = productsForRating.MapList<ProductView>();
            NewProducts = newProducts.MapList<ProductView>();
            await InitAsync(null, 0, false);
        }
    }
}