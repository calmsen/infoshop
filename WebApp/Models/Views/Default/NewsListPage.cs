﻿using DomainLogic.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class NewsListPage : AboutBodyLayout
    {
        [Inject]
        public INewsService NewsService { get; set; }

        public List<ArticleView> News { get; set; }

        public Pagination Pagination { get; set; }

        public async Task InitAsync(int page)
        {
            Pagination = new Pagination
            {
                CurrentPage = page
            };
            Pagination.RouteValues.Add("controller", "News");
            Pagination.RouteValues.Add("action", "Search");
            News = (await NewsService.GetNewsAsync(Pagination, true))
                ?.MapList<ArticleView>();
            await InitAboutBodyLayoutAsync("News", "About");
        }
    }
}