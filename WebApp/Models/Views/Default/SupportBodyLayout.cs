﻿using System.Threading.Tasks;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;

namespace WebApp.Models.Views.Default
{
    public class SupportBodyLayout : MainLayout
    {
        public string ActiveSideMenuLink { get; set; }

        protected virtual async Task InitAsync(string activeSideMenuLink, string activeNavLink)
        {
            ActiveSideMenuLink = activeSideMenuLink;
            await InitAsync(activeNavLink, 0, false);
        }
    }
}