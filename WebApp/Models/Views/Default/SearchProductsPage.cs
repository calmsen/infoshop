﻿using DomainLogic.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApp.UiHelpers;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class SearchProductsPage : MainLayout
    {
        [Inject]
        public IProductsService ProductsService { get; set; }
        [Inject]
        public ProductUiHelpers ProductUiHelpers { get; set; }

        public List<ProductView> Products { get; set; }

        public Pagination Pagination { get; set; }

        public async Task InitAsync(string query, int page)
        {
            Pagination = new Pagination
            {
                CurrentPage = page,
                PageItemsAmount = 24,
                RouteValues = new Dictionary<string, object> { { "query", query } }
            };
            Pagination.RouteValues.Add("controller", "Products");
            Pagination.RouteValues.Add("action", "Search");
            //
            Products = (await ProductsService.SearchProductsAsync(query, Pagination))
                .MapList<ProductView>();

            // установим данные для кнопки сравнения
            foreach (var p in Products)
                p.CompareProductBtn = ProductUiHelpers.GetCompareProductBtn(p.Id);

            await InitAsync("Products", 0, false);
        }
    }
}