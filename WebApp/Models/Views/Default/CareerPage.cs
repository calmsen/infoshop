﻿using System.Threading.Tasks;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;

namespace WebApp.Models.Views.Default
{
    public class CareerPage : VacanciesBodyLayout
    {
        [Inject]
        public IPagesService PagesService { get; set; }

        public PageView Page { get; set; }

        public async Task InitCareerPageAsync()
        {
            Page page = await PagesService.GetPageByNameAsync("Career");
            Page = page?.Map<PageView>();
            await InitVacanciesBodyLayoutAsync(0, "Career", "About");
        }
    }
}