﻿using System.Collections.Generic;

namespace WebApp.Models.Views.Default
{
    public class BeforeBody
    {
        public List<SectionView> Sections { get; set; }

        public bool Archive { get; set; }
    }
}