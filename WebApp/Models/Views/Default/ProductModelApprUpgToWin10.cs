﻿namespace WebApp.Models.Views.Default
{
    public class ProductModelApprUpgToWin10
    {
        public string Type { get; set; }

        public long Code { get; set; }

        public string Model { get; set; }

        public string Filters { get; set; }

        public int Upgrade { get; set; }

        public int UpgradeNovember { get; set; }
    }
}