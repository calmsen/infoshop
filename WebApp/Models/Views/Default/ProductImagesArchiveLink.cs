﻿namespace WebApp.Models.Views.Default
{
    public class ProductImagesArchiveLink
    {
        public bool IsProductImagesArchiveLinkShowed { get; set; }

        public long ProductId { get; set; }
    }
}