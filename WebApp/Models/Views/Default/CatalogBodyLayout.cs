﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Models.Enumerations;
using WebApp.Models.Views;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class CatalogBodyLayout : MainLayout
    {
        [Inject]
        public IFiltersService FiltersService { get; set; }

        public List<FilterParam> FParams { get; set; }

        public SidebarView Sidebar { get; set; }

        public BeforeBody BeforeBody { get; set; }        

        private List<SectionView> _activeSectionChilds;

        public async Task<SidebarView> GetSidebarAsync(long activeSectionId, bool archive, string sortType, string groupType, int selectType, string viewType, double[] price)
        {
            Section section = await SectionsService.GetSectionByIdAsync(activeSectionId);
            // 
            List<FilterView> filterViews = new List<FilterView>();
            if (section != null)
            {
                List<DmFilter> filters = await FiltersService.GetFiltersForSectionAsync(section.Id);
                foreach (var g in section.FiltersGroups)
                {
                    foreach (var fs in g.Filters)
                    {
                        foreach (var f in filters)
                        {
                            if (fs.Id != f.Id)
                            {
                                continue;
                            }
                            if (fs.Type != FilterTypeEnum.None)
                            {
                                f.Type = fs.Type;
                            }
                            if (f.Type.Equals(FilterTypeEnum.Absent))
                            {
                                continue;
                            }
                            if ((f.Type.Equals(FilterTypeEnum.Select) || f.Type.Equals(FilterTypeEnum.Checkbox) || f.Type.Equals(FilterTypeEnum.Radio)) && f.Values.Count == 0)
                            {
                                continue;
                            }
                            if (f.Type.Equals(FilterTypeEnum.Range) && string.IsNullOrEmpty(f.Unit))
                            {
                                continue;
                            }
                            f.SetUpProps(fs);
                            var filterView = f.Map<FilterView>();
                            filterView.FParams = FParams?.Where(x => x.Id == f.Id).FirstOrDefault();
                            filterViews.Add(filterView);
                        }
                    }
                }
            }
            var sectionViews = _activeSectionChilds ??  (_activeSectionChilds = (await SectionsService.GetSectionChildsAsync(activeSectionId))
                .MapList<SectionView>());
            var model = new SidebarView
            {
                ActiveSection = section?.Map<SectionView>(),
                FParams = FParams,
                Sections = sectionViews,
                Filters = filterViews,
                Archive = archive,
                SortType = sortType,
                GroupType = groupType,
                SelectType = selectType,
                ViewType = viewType,
                Price = price
            };
            return model;
        }

        public async Task<BeforeBody> GetBeforeBodyAsync(long activeSectionId, bool archive)
        {
            var sectionViews = new List<SectionView>();
            if (activeSectionId != 0)
            {
                sectionViews = _activeSectionChilds ?? (_activeSectionChilds = (await SectionsService.GetSectionChildsAsync(activeSectionId))
                    .MapList<SectionView>());
            }

            var model = new BeforeBody
            {
                Sections = sectionViews,
                Archive = archive
            };
            return model;
        }
                
        protected async Task InitAsync(long activeSectionId, bool archive, string sortType, string groupType, int selectType, string viewType, double[] price)
        {
            Sidebar = await GetSidebarAsync(activeSectionId, archive, sortType, groupType, selectType, viewType, price);
            BeforeBody = await GetBeforeBodyAsync(activeSectionId, archive);
            await InitAsync("Products", activeSectionId, archive);
        }
    }
}