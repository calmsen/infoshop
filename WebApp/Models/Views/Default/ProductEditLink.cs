﻿namespace WebApp.Models.Views.Default
{
    public class ProductEditLink
    {
        public bool IsProductEditLinkShowed { get; set; }

        public long ProductId { get; set; }
    }
}