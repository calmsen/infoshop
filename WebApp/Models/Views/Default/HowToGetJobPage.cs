﻿using DomainLogic.Interfaces.Services;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Extensions;
using WebApp.Models.Forms;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class HowToGetJobPage : VacanciesBodyLayout
    {
        [Inject]
        public IPagesService PagesService { get; set; }

        public PageView Page { get; set; }

        public ResumeForm ResumeForm { get; set; }

        public bool IsCreatedResume { get; set; }

        public async Task InitHowToGetJobPageAsync()
        {
            Page page = await PagesService.GetPageByNameAsync("HowToGetJob");
            Page = page?.Map<PageView>();
            await InitVacanciesBodyLayoutAsync(0, "HowToGetJob", "About");
        }
    }
}