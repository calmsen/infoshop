﻿using DomainLogic.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class PostsListPage : MainLayout
    {
        [Inject]
        public IPostsService PostService { get; set; }

        public List<PostView> Posts { get; set; }

        public Pagination Pagination { get; set; }

        public async Task InitAsync(int page)
        {
            Pagination = new Pagination
            {
                CurrentPage = page
            };
            Pagination.RouteValues.Add("controller", "Posts");
            Pagination.RouteValues.Add("action", "Index");

            Posts = (await PostService.GetPostsAsync(Pagination, true))
                .MapList<PostView>();
            await InitAsync(null, 0, false);
        }
    }
}