﻿using DomainLogic.Models;
using System.Collections.Generic;

namespace WebApp.Models.Views.Default
{
    public class DropdownNavbarItem
    {
        public string ActiveNavLink { get; set; }

        public List<SectionView> Sections { get; set; }

        public Dictionary<long, Positions> PositionsOfSections { get; set; }
    }
}