﻿namespace WebApp.Models.Views.Default
{
    public class Breadcrumb
    {
        public string Name { get; set; }

        public string Title { get; set; }

        public string Path { get; set; }
    }
}