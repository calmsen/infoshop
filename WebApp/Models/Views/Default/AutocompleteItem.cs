﻿using DomainLogic.Models.Enumerations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;

namespace WebApp.Models.Views.Default
{
    public class AutocompleteItem
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public FilterTypeEnum type;

        public string unit;

        public List<UnitConverterView> unitConverters;

        public long id { get; set; }

        public string label { get; set; }

        public string value { get; set; }
    }
}