﻿using System.Collections.Generic;

namespace WebApp.Models.Views.Default
{
    public class VacanciesSidebar
    {
        public List<VacancyView> Vacancies { get; set; }

        public VacancyView ActiveVacancy { get; set; }

        public string ActiveSideMenuLink { get; set; }
    }
}