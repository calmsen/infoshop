﻿using System;

namespace WebApp.Models.Views.Default
{
    public class ErrorPage
    {
        public Exception Exception { get; set; }

        public string PageTitle { get; set; }
    }
}