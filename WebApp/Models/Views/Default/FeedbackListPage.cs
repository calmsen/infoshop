﻿using DomainLogic.Interfaces.Services;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebApp.UiHelpers;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Infrastructure.Extensions;
using WebApp.Models.Filters;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class FeedbackListPage: MainLayout
    {
        [Inject]
        public IFeedbacksService FeedbacksService { get; set; }
        [Inject]
        public FeedbackUiHelpers FeedbackUiHelpers { get; set; }

        public List<FeedbackView> Feedbacks { get; set; }

        public Pagination Pagination { get; set; }

        public FeedbackListFilter Filter { get;  set; }

        public List<SelectListItem> FeedbackThemesSelectList { get; set; }

        public List<SelectListItem> SectionsForSelectList { get; set; }

        public List<SelectListItem> ProductTitlesForSelectList { get; set; }

        public List<SelectListItem> FeedbackStatesSelectList { get;  set; }        

        public async Task InitAsync(NameValueCollection queryString, FeedbackListFilter filter, int page = 1)
        {
            var pagination = new Pagination
            {
                CurrentPage = page,
                RouteValues = filter.ToDictionaryOfFormatedValues(queryString)
            };
            pagination.RouteValues.Add("controller", "Feedback");
            pagination.RouteValues.Add("action", "List");
            // удалим строку поиска как null
            if (filter.Search != null && filter.Search.Equals("null"))
                filter.Search = null;
            // -- -- --
            var feedbacks = (await FeedbacksService.GetFeedbacksAsync(pagination, filter))
                .MapList<FeedbackView>();
            //
            var selectLists = await FeedbackUiHelpers.GetSelectListsAsync(filter.SectionId, true, true);
            //
            List<SelectListItem> feedbackStatesSelectList = FeedbackUiHelpers.GetFeedbackStatesForSelectList(new SelectListItem { Value = "", Text = "--" });
            //
            foreach (FeedbackView f in feedbacks)
            {
                f.StatesMenu = FeedbackUiHelpers.GetDropDownMenuForStates(filter.State, "stateForAction", new Dictionary<string, object> { { "action", "ChangeState" }, { "controller", "Feedback" }, { "id", @f.Id } }.Extend(pagination.RouteValues));
            }
            Feedbacks = feedbacks;
            Pagination = pagination;
            Filter = filter;
            FeedbackThemesSelectList = selectLists.Item1;
            SectionsForSelectList = selectLists.Item2;
            ProductTitlesForSelectList = selectLists.Item3;
            FeedbackStatesSelectList = feedbackStatesSelectList;

            await base.InitAsync(null, 0, false);
        }
    }
}