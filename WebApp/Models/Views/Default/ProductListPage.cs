﻿using DomainLogic.Interfaces;
using DomainLogic.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebApp.UiHelpers;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Models.Enumerations;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class ProductListPage : CatalogBodyLayout
    {
        [Inject]
        public IProductsService ProductsService { get; set; }
        [Inject]
        public IResourcesService ResourcesService { get; set; }
        [Inject]
        public IResourceProvider ResourceProvider { get; set; }
        [Inject]
        public ProductUiHelpers ProductUiHelpers { get; set; }

        public SectionView ActiveSection { get; set; }

        public Pagination Pagination { get; set; }

        public List<ProductGroup> GroupsOfProducts { get; set; }

        public Dictionary<string, object> ParamsForSortTypeForm { get; set; }

        public List<SelectListItem> SortTypesForSelectList { get; set; }

        public List<SelectListItem> GroupTypesForSelectList { get; set; }

        public List<SelectListItem> SelectTypesForSelectList { get; set; }

        public List<SelectListItem> ViewTypesForSelectList { get; set; }

        public PriceValue[] PriceField { get; set; }

        public bool Archive { get; set; }

        [Display(Name = "ProductListFilterSortLabel", ResourceType = typeof(Resources.ProductsRes))]
        public string SortType { get; set; }

        [Display(Name = "ProductListFilterGroupLabel", ResourceType = typeof(Resources.ProductsRes))]
        public string GroupType { get; set; }

        [Display(Name = "ProductListFilterSelectLabel", ResourceType = typeof(Resources.ProductsRes))]
        public int SelectType { get; set; }

        [Display(Name = "ProductListFilterPriceLabel", ResourceType = typeof(Resources.ProductsRes))]
        public double[] Price { get; set; }
        [Display(Name = "ProductListFilterViewLabel", ResourceType = typeof(Resources.ProductsRes))]
        public string ViewType { get; set; }

        public SectionImageView Banner { get; set; }

        public List<ParamForCancelForm> ParamsForCancelForm { get; set; }

        public List<ProductView> ProductsViewsForArchive { get; set; }

        public Pagination PaginationForArchive { get; set; }

        public CompareProductsBtn CompareProductsBtn { get; set; }

        public async Task InitAsync(NameValueCollection queryString, string sectionName = null, int page = 1, bool archive = false, string sortType = "Title", string groupType = null, int selectType = 0, string viewType = null, List<double> price = null)
        {
            if (groupType == null)
                selectType = 0;
            // получим информацию о текушем разделе
            Section section = await SectionsService.GetSectionByNameAsync(sectionName, needIncludeBanner: true);
            // получим входные параметры для фильтров из QueryString 
            List<DmFilter> filters = new List<DmFilter>();
            List<FilterParam> fParams = await FiltersService.GetFiltersParamsAsListAsync(queryString, section, filters);
            // получим информацию о пагинашке
            Pagination pagination = GetPaginationData(sectionName, fParams, page, archive, sortType, groupType, viewType, price != null ? price.ToArray() : null);
            // получим список товаров
            List<Sort> sortsList = GetSortsList(sortType, groupType);
            List<ProductView> productsViews = (await ProductsService.GetProductsAsync(pagination, section?.Id ?? 0, fParams, sortsList, selectType, archive, price != null ? price.ToArray() : null))
                .MapList<ProductView>();
            // установим значения, по которым нужно группировать
            await SetProductViewValueForGroupTypeAsync(productsViews, sortsList, section?.Id ?? 0);
            // установим данные для кнопки сравнения
            foreach (var p in productsViews)
                p.CompareProductBtn = ProductUiHelpers.GetCompareProductBtn(p.Id);
            // разобъем товары на группы
            List<Section> allSections = _allSections ?? (_allSections = await SectionsService.GetSectionsAsync());
            List<ProductGroup> groupsOfProducts = GetProductGroups(productsViews, groupType, allSections, section);
            // получим данные для сортировки
            Dictionary<string, object> paramsForSortTypeForm;
            List<SelectListItem> sortTypesForSelectList = GetSortTypesForSelectList(out paramsForSortTypeForm, pagination);
            // получим данные для группировки
            List<SelectListItem> groupTypesForSelectList = await GetGroupTypesForSelectListAsync(section);
            List<SelectListItem> selectTypeForSelectList = new List<SelectListItem>
            {
                new SelectListItem { Text = "--", Value = "0" },
                new SelectListItem { Text = "3", Value = "3" },
                new SelectListItem { Text = "6", Value = "6" }
            };
            // получим данные для выбора типа просмотра товаров
            List<SelectListItem> viewTypesForSelectList = GetViewTypesForSelectList();
            // получим PriceValue
            PriceValue[] priceField;
            if (price != null && price.Count == 2)
                priceField = price.Select(x => new PriceValue { Value = x }).ToArray();
            else
                priceField = new PriceValue[] {
                    new PriceValue(),
                    new PriceValue()
                };
            // получим рандомный баннер
            var bannerIndex = -1;
            if (section != null && section.SectionsToImages != null && section.SectionsToImages.Count > 0)
                bannerIndex = new Random().Next(0, section.SectionsToImages.Count);
            SectionImageView banner = null;
            if (bannerIndex >= 0)
                banner = section.SectionsToImages[bannerIndex].Image.Map<SectionImageView>();
            // получим параметры фильтров для отмены фильтров
            List<ParamForCancelForm> paramsForCancelForm = await GetParamsForCancelFormAsync(fParams, price != null ? price.ToArray() : null, filters);
            // получим товары для виджета - Архив
            List<ProductView> productsViewsForArchive = null;
            Pagination paginationForArchive = null;
            if (!archive)
            {
                Dictionary<string, object> paramsForArchive = pagination.RouteValues.ToDictionary(x => x.Key, y => y.Value);
                paramsForArchive["Archive"] = "True";
                paginationForArchive = new Pagination
                {
                    PageItemsAmount = 6,
                    CurrentPage = 1,
                    RouteValues = paramsForArchive
                };
                pagination.RouteValues.Add("controller", "Products");
                pagination.RouteValues.Add("action", "Index");
                pagination.RouteValues.Add("sectionName", "sectionName");
                productsViewsForArchive = (await ProductsService.GetProductsAsync(paginationForArchive, section != null ? section.Id : 0, fParams, sortsList, selectType, true, price != null ? price.ToArray() : null))
                .MapList<ProductView>();
            }

            // заполним модель
            ActiveSection = section?.Map<SectionView>();
            Banner = banner;
            Pagination = pagination;
            FParams = fParams;
            GroupsOfProducts = groupsOfProducts;
            SortTypesForSelectList = sortTypesForSelectList;
            ViewTypesForSelectList = viewTypesForSelectList;
            GroupTypesForSelectList = groupTypesForSelectList;
            SelectTypesForSelectList = selectTypeForSelectList;
            PriceField = priceField;
            SortType = sortType;
            GroupType = groupType;
            SelectType = selectType;
            ViewType = viewType ?? "Table";
            Price = price != null ? price.ToArray() : null;
            ParamsForSortTypeForm = paramsForSortTypeForm;
            ParamsForCancelForm = paramsForCancelForm;
            ProductsViewsForArchive = productsViewsForArchive;
            PaginationForArchive = paginationForArchive;
            if (ActiveSection != null)
                CompareProductsBtn = ProductUiHelpers.GetCompareProductsBtn(ActiveSection.Id);
            await base.InitAsync(0, archive, sortType, groupType, selectType, viewType, Price);

        }
        
        private Pagination GetPaginationData(string sectionName, List<FilterParam> fParams, int page, bool archive, string sortType, string groupType, string viewType, double[] price)
        {
            // сформируем парамтеры для постраничной навигации
            var parameters = new Dictionary<string, object>();
            foreach (var fp in fParams)
                parameters.Add(fp.Name, string.Join(",", fp.Values));
            // добавим параметр архива в pagination
            if (archive)
                parameters.Add("Archive", "True");
            if (!sortType.Equals("Title"))
                parameters.Add("SortType", sortType);
            if (groupType != null)
                parameters.Add("GroupType", groupType);
            if (viewType != null)
                parameters.Add("ViewType", viewType);
            if (price != null && price.Length == 2)
                parameters.Add("Price", string.Join(",", price));
            var pagination = new Pagination
            {
                PageItemsAmount = 21,
                CurrentPage = page,
                RouteValues = parameters
            };
            Pagination.RouteValues.Add("controller", "Products");
            Pagination.RouteValues.Add("action", "Index");
            Pagination.RouteValues.Add("sectionName", "sectionName");
            return pagination;
        }

        private List<Sort> GetSortsList(string sortType, string groupType)
        {
            List<Sort> sortsList = new List<Sort>();
            if (!string.IsNullOrEmpty(groupType))
            {
                sortsList.Add(new Sort
                {
                    ColumnName = groupType,
                    Type = groupType.Equals("PathOfSections") ? DomainLogic.Infrastructure.Paging.SortType.Field : DomainLogic.Infrastructure.Paging.SortType.Filter,
                    Order = OrderType.Asc
                });
            }
            Sort sortObj = new Sort();
            if (sortType.Equals("Title"))
            {
                sortObj.ColumnName = "Title";
                sortObj.Order = OrderType.Asc;
                sortObj.Type = DomainLogic.Infrastructure.Paging.SortType.Field;
            }
            else if (sortType.Equals("PriceUp"))
            {
                sortObj.ColumnName = "Price";
                sortObj.Order = OrderType.Asc;
                sortObj.Type = DomainLogic.Infrastructure.Paging.SortType.Field;
            }
            else if (sortType.Equals("PriceDown"))
            {
                sortObj.ColumnName = "Price";
                sortObj.Order = OrderType.Desc;
                sortObj.Type = DomainLogic.Infrastructure.Paging.SortType.Field;
            }
            sortsList.Add(sortObj);
            return sortsList;
        }

        private async Task SetProductViewValueForGroupTypeAsync(List<ProductView> productsViews, List<Sort> sortsList, long sectionId)
        {
            Sort groupObj = sortsList.FirstOrDefault(x => x.Type == DomainLogic.Infrastructure.Paging.SortType.Filter);
            if (groupObj != null && sectionId > 0)
            {
                long filterIdByGroupType = (await FiltersService.GetFilterByNameAsync(groupObj.ColumnName, sectionId))?.Id ?? 0;
                if (filterIdByGroupType > 0)
                    for (int i = 0; i < productsViews.Count; i++)
                    {
                        try
                        {
                            string value = productsViews[i].Attributes.FirstOrDefault(x => x.FilterId == filterIdByGroupType).Values[0].Value.Title;
                            productsViews[i].FilterValueByGroupType = value;
                        }
                        catch (Exception)
                        {
                        }
                    }
            }
        }

        private List<ProductGroup> GetProductGroups(List<ProductView> productsViews, string groupType, List<Section> sections, Section activeSection)
        {
            if (string.IsNullOrEmpty(groupType))
            {
                string groupTitle = ResourceProvider.ReadResource("Resources.ProductsRes.ProductsListRootGroupTitle");
                if (activeSection != null)
                    groupTitle = activeSection.Title;
                return new List<ProductGroup>
                {
                    new ProductGroup
                    {
                        Title = groupTitle,
                        Products = productsViews
                    }
                };

            }
            List<ProductGroup> groupsOfProducts = new List<ProductGroup>();

            if (groupType.Equals("PathOfSections"))
            {
                long activeSectionId = activeSection != null ? activeSection.Id : 0;
                List<ProductView> prs = productsViews.Where(x => x.SectionId == activeSectionId).ToList();
                if (prs.Count > 0)
                {
                    groupsOfProducts.Add(
                        new ProductGroup
                        {
                            Title = activeSection.Title,
                            Products = productsViews.Where(x => x.SectionId == activeSectionId).ToList()
                        }
                    );
                }
                // найдем дочерние разделы и разобъем товары по этим разделам
                List<Section> sectionsChilds = SectionsService.GetSectionChildsFromList(sections, activeSection != null ? activeSection.Id : 0);
                //
                foreach (Section s in sectionsChilds)
                {
                    if (s.Hidden)
                        continue;

                    List<long> lsIds = SectionsService.GetDescendenSectionsFromList(sections, s, false).Select(x => x.Id).ToList();
                    prs = productsViews.Where(x => lsIds.Contains(x.SectionId)).ToList();
                    if (prs.Count == 0)
                        continue;

                    groupsOfProducts.Add(new ProductGroup
                    {
                        Title = s.Title,
                        Products = prs
                    });
                }
            }
            else
            {
                IEnumerable<string> values = productsViews.Select(x => x.FilterValueByGroupType).ToList().Distinct();
                foreach (string v in values)
                {
                    List<ProductView> prs = productsViews.Where(x => string.Equals(x.FilterValueByGroupType, v)).ToList();
                    if (prs.Count == 0)
                        continue;

                    groupsOfProducts.Add(new ProductGroup
                    {
                        Title = prs[0].FilterValueByGroupType,
                        Products = prs
                    });
                }
            }
            return groupsOfProducts;
        }
        
        private List<SelectListItem> GetSortTypesForSelectList(out Dictionary<string, object> paramsForSortTypeForm, Pagination pagination)
        {
            string culture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
            List<SelectListItem> sortTypesForSelectList = new List<SelectListItem> {
                new SelectListItem { Value = "PriceUp", Text = ResourceProvider.ReadResource("Resources.ProductsRes.PriceUpSelectListItem") },
                new SelectListItem { Value = "PriceDown", Text = ResourceProvider.ReadResource("Resources.ProductsRes.PriceDownSelectListItem") },
                new SelectListItem { Value = "Title", Text = ResourceProvider.ReadResource("Resources.ProductsRes.TitleSelectListItem") }
            };
            paramsForSortTypeForm = pagination.RouteValues.ToDictionary(x => x.Key, y => y.Value);
            paramsForSortTypeForm.Remove("SortType");
            paramsForSortTypeForm.Remove("GroupType");
            paramsForSortTypeForm.Remove("Price");
            paramsForSortTypeForm.Remove("ViewType");
            return sortTypesForSelectList;
        }
        
        private async Task<List<SelectListItem>> GetGroupTypesForSelectListAsync(Section section)
        {
            List<SelectListItem> groupTypesForSelectList = new List<SelectListItem>
            {
                new SelectListItem { Value = "NaN", Text = "--" },
                 new SelectListItem { Value = "PathOfSections", Text = ResourceProvider.ReadResource("Resources.ProductsRes.BySections") }
            };
            if (section != null)
            {
                List<FilterSettings> filterSettings = new List<FilterSettings>();
                foreach (FilterGroup g in section.FiltersGroups)
                    foreach (FilterSettings fs in g.Filters)
                        if (fs.ShowInGroupSelect)
                            filterSettings.Add(fs);
                List<DmFilter> filters = await FiltersService.GetFiltersByIdsAsync(
                    filterSettings.Select(x => x.Id).ToList()
                );
                foreach (DmFilter f in filters)
                {
                    f.SetUpProps(filterSettings.FirstOrDefault(x => x.Id == f.Id));
                    groupTypesForSelectList.Add(new SelectListItem
                    {
                        Value = f.Name,
                        Text = f.Title
                    });
                }
            }
            return groupTypesForSelectList;
        }
        
        private List<SelectListItem> GetViewTypesForSelectList()
        {
            List<SelectListItem> viewTypesForSelectList = new List<SelectListItem> {
                new SelectListItem { Value = "Table", Text = "<i class=\"fa fa-th\"></i>"},
                new SelectListItem { Value = "List", Text = "<i class=\"fa fa-list\"></i>"}
            };
            return viewTypesForSelectList;
        }

        private async Task<List<ParamForCancelForm>> GetParamsForCancelFormAsync(List<FilterParam> fParams, double[] price, List<DmFilter> filters)
        {
            List<FilterValue> values = await FiltersService.GetFiltersValuesAsync(fParams);
            List<ParamForCancelForm> paramsForCancelForm = new List<ParamForCancelForm>();
            foreach (var fp in fParams)
            {
                string fpText = "";
                if (fp.Type == (int)FilterTypeEnum.Range)
                {
                    foreach (var f in filters)
                    {
                        if (fp.Id != f.Id)
                            continue;

                        string from = ResourceProvider.ReadResource("Resources.ProductsRes.ProductListCancelFilterPriceTo");
                        string to = ResourceProvider.ReadResource("Resources.ProductsRes.ProductListCancelFilterPriceFrom");
                        string filterTitle = f.Title;
                        string filterUnit = f.Unit;

                        fpText = filterTitle + ": " + from + " " + fp.Values[0] + " "
                            + filterUnit;
                        if (fp.Values[1] > 0)
                            fpText += " " + to + " " + fp.Values[1] + " " + filterUnit;
                        break;
                    }
                }
                else
                {
                    foreach (var f in filters)
                    {
                        if (fp.Id != f.Id)
                            continue;
                        foreach (var fpv in fp.Values)
                        {
                            foreach (var v in values)
                            {
                                if (v.Id != fpv)
                                    continue;
                                if (!string.IsNullOrEmpty(fpText))
                                    fpText += ",";

                                fpText += v.Title;
                                break;
                            }
                        }
                        fpText = f.Title 
                            + ": " + fpText;
                        break;
                    }
                }
                paramsForCancelForm.Add(new ParamForCancelForm
                {
                    Name = fp.Name,
                    Value = string.Join(",", fp.Values),
                    Text = fpText
                });
            }
            if (price != null && price.Length == 2)
            {
                string priceText;
                priceText = string.Format(ResourceProvider.ReadResource("Resources.ProductsRes.ProductListCancelFilterPriceFrom"), price[0]);
                if (price[1] > 0)
                    priceText += string.Format(ResourceProvider.ReadResource("Resources.ProductsRes.ProductListCancelFilterPriceTo"), price[1]);
                
                paramsForCancelForm.Add(new ParamForCancelForm
                {
                    Name = "Price",
                    Value = string.Join(",", price),
                    Text = priceText
                });
            }
            return paramsForCancelForm;
        }
    }
}