﻿using DomainLogic.Interfaces.Services;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Infrastructure.Extensions;
using WebApp.Models.Filters;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class FeedbackAnswerListPage : MainLayout
    {
        [Inject]
        public IFeedbacksService FeedbacksService { get; set; }

        public List<FeedbackAnswerView> FeedbackAnswers { get; set; }

        public Pagination Pagination { get; set; }

        public FeedbackAnswerListFilter Filter { get; set; }
        
        public async Task InitAsync(NameValueCollection queryString, FeedbackAnswerListFilter filter, int page = 1)
        {
            var pagination = new Pagination
            {
                CurrentPage = page,
                RouteValues = filter.ToDictionaryOfFormatedValues(queryString)
            };
            pagination.RouteValues.Add("controller", "Feedback");
            pagination.RouteValues.Add("action", "Answers");
            // удалим строку поиска как null
            if (filter.Search != null && filter.Search.Equals("null"))
                filter.Search = null;
            // -- -- --
            var feedbackAnswers = (await FeedbacksService.GetFeedbackAnswersAsync(pagination, filter))
                .MapList<FeedbackAnswerView>();

            FeedbackAnswers = feedbackAnswers;
            Pagination = pagination;
            Filter = filter;

            await base.InitAsync(null, 0, false);
        }
    }
}