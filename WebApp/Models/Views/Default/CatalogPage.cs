﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Models;
namespace WebApp.Models.Views.Default
{
    public class CatalogPage : MainLayout
    {
        public List<SectionView> Sections { get; set; }

        public async Task InitAsync()
        {
            List<Section> allSections = _allSections ?? (_allSections =await SectionsService.GetSectionsAsync());
            Sections = SectionsService.SectionsToTreeFromList(allSections, 0, false)
                .MapList<SectionView>();
            await InitAsync("Products", 0, false);
        }
    }
}