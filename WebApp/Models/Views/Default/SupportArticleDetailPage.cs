﻿using DomainLogic.Interfaces.Services;
using System.Threading.Tasks;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class SupportArticleDetailPage : SupportBodyLayout
    {
        [Inject]
        public ISupportArticlesService SupportArticlesService { get; set; }

        public SupportArticleView Article { get; set; }

        public async Task InitAsync(long id)
        {
            Article = (await SupportArticlesService.GetArticleAsync(id))?
                .Map<SupportArticleView>();
            if (Article == null)
                return;

            await InitAsync("SupportArticles", "Files");
        }
    }
}