﻿using DomainLogic.Interfaces.Services;
using System.Collections.Generic;
using System.Web.Mvc;
using WebApp.UiHelpers;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Infrastructure.Extensions;
using WebApp.Models.Forms;
using DomainLogic.Models.Enumerations;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Models.Settings;

namespace WebApp.Models.Views.Default
{
    public class FeedbackShowPage : MainLayout
    {
        [Inject]
        public FeedbackSettings FeedbackSettings { get; set; }
        [Inject]
        public IFeedbacksService FeedbacksService { get; set; }
        [Inject]
        public IFaqService FaqService { get; set; }
        [Inject]
        public FeedbackUiHelpers FeedbackUiHelpers { get; set; }
        [Inject]
        public CommonUtils CommonUtils { get; set; }

        public FeedbackView Feedback { get; set; }

        public List<SelectListItem> FeedbackThemesSelectList { get; set; }

        public List<SelectListItem> FeedbackStatesSelectList { get; set; }

        public FeedbackAnswerForm FeedbackAnswer { get; set; }

        public string ReturnUrl { get; set; }

        public FeedbackInviteMessageForm FeedbackInviteMessageForm { get; set; }

        public bool FeedbackReadPermission { get; set; }

        public FeedbackImagesForm ImagesForm { get; set; }

        public List<FaqView> Faq { get; set; }

        public FeedbackChangeModelForm FeedbackChangeModelForm { get; set; }

        public List<SelectListItem> SectionsForSelectList { get; set; }

        public List<SelectListItem> ProductTitlesForSelectList { get; set; }

        public FeedbackChangeThemeForm FeedbackChangeThemeForm { get; set; }

        public bool IsShowManagerControls { get; set; }

        public bool IsAuthor { get; set; }
               
        public async Task InitAsync(long id, string returnUrl = null, string hash = null, string email = null, bool needPickUp = false)
        {
            Feedback feedback = await FeedbacksService.GetFeedbackByIdAsync(id);
            var feedbackView = feedback.Map<FeedbackView>();
            var imagesForm = feedback.Map<FeedbackImagesForm>();
            var answerForm = new FeedbackAnswerForm
            {
                Feedback = new SimpleFeedbackForm { Id = feedback.Id },
                Hash = hash,
                AuthorEmail = email,
                NeedPickUp = needPickUp
            };
            

            var selectLists = await FeedbackUiHelpers.GetSelectListsAsync(feedback.SectionId, false, false);
            List<SelectListItem> feedbackStatesSelectList = FeedbackUiHelpers.GetFeedbackStatesForSelectList(new SelectListItem { Value = "", Text = "--" });
            //
            var feedbackInviteMessageForm = new FeedbackInviteMessageForm
            {
                FeedbackId = feedback.Id
            };
            // позволим читать обсуждение и писать комментарий
            bool feedbackReadPermission = false;
            if (hash != null && email != null)
                feedbackReadPermission = string.Equals(hash, CommonUtils.GetMd5Hash(id + email + FeedbackSettings.Md5HahSolt));
            // -- -- --
            // получим список вопросов и ответов для менеджеров
            var faqViews = (await FaqService.GetFaqAsync(feedback.SectionId, feedback.ProductId, FeedbackThemeEnum.None, true))
                .MapList<FaqView>();
            // -- -- --
            bool isShowManagerControls = await UsersService.IsInRoleAsync(_currentUser.Email, "Manager");
            bool isAuthor = _currentUser.Equals(feedback.User.Email);
            Feedback = feedbackView;
            FeedbackThemesSelectList = selectLists.Item1;
            SectionsForSelectList = selectLists.Item2;
            ProductTitlesForSelectList = selectLists.Item3;
            FeedbackStatesSelectList = feedbackStatesSelectList;
            FeedbackAnswer = answerForm;
            ImagesForm = imagesForm;
            ReturnUrl = returnUrl;
            FeedbackInviteMessageForm = feedbackInviteMessageForm;
            FeedbackReadPermission = feedbackReadPermission;
            Faq = faqViews;
            FeedbackChangeModelForm = new FeedbackChangeModelForm
            {
                ReturnUrl = returnUrl,
                FeedbackId = feedback.Id,
                SectionId = feedback.SectionId,
                ProductId = feedback.ProductId
            };
            FeedbackChangeThemeForm = new FeedbackChangeThemeForm
            {
                ReturnUrl = returnUrl,
                FeedbackId = feedback.Id,
                Theme = feedback.Theme
            };
            IsShowManagerControls = isShowManagerControls;
            IsAuthor = isAuthor;

            await base.InitAsync(null, 0, false);
        }
    }
}