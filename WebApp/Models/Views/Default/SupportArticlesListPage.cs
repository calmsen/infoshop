﻿using DomainLogic.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class SupportArticlesListPage : SupportBodyLayout
    {
        [Inject]
        public ISupportArticlesService SupportArticlesService { get; set; }

        public List<SupportArticleView> Articles { get; set; }

        public Pagination Pagination { get; set; }

        public async Task InitAsync(int page)
        {
            Pagination = new Pagination
            {
                CurrentPage = page
            };
            Pagination.RouteValues.Add("controller", "SupportArticles");
            Pagination.RouteValues.Add("action", "Index");
            //
            Articles = (await SupportArticlesService.GetArticlesAsync(Pagination))
                .MapList<SupportArticleView>();

            await InitAsync("SupportArticles", "Files");
        }
    }
}