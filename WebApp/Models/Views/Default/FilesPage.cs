﻿using DomainLogic.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebApp.UiHelpers;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Models.Enumerations;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Models.Views.Default
{
    public class FilesPage : SupportBodyLayout
    {
        [Inject]
        public IFilesService FilesService { get; set; }
        [Inject]
        public ISupportArticlesService SupportArticlesService { get; set; }
        [Inject]
        public ProductUiHelpers ProductUiHelpers { get; set; }
        [Inject]
        public FileUiHelpers FileUiHelpers { get; set; }

        public List<FileView> Files { get; set; }

        public List<SelectListItem> FileFiltersSelectList { get; set; }

        public List<SelectListItem> SectionsForSelectList { get; set; }

        public List<SelectListItem> ProductTitlesForSelectList { get; set; }

        [Display(Name = "FilesSearchFilterCategoryLabel", ResourceType = typeof(Resources.FilesRes))]
        public long? SectionId { get; set; }

        [Display(Name = "FilesSearchFilterModelLabel", ResourceType = typeof(Resources.FilesRes))]
        public long? ProductId { get; set; }

        [Display(Name = "FilesSearchFilterFiltersLabel", ResourceType = typeof(Resources.FilesRes))]
        public FileFilterEnum Filters { get; set; }
        
        public List<SupportArticleView> LastSupportArticles { get; set; }

        public async Task InitAsync(long? sectionId, long? productId, FileFilterEnum filters)
        {
            List<SelectListItem> sectionsForSelectList = await SectionUiHelpers.GetSectionsForSelectListAsync(new SelectListItem { Value = "", Text = "--" }, null);
            List<SelectListItem> productTitlesForSelectList;
            if (sectionId != null)
                productTitlesForSelectList = await ProductUiHelpers.GetProductTitlesForSelectListAsync((long)sectionId, new SelectListItem { Value = "", Text = "--" }, null);
            else
                productTitlesForSelectList = new List<SelectListItem> { new SelectListItem { Value = "", Text = "--" } };
            // получим список фильтров
            List<SelectListItem> fileFiltersSelectList = FileUiHelpers.GetFileFiltersForSelectList(new SelectListItem { Value = "", Text = "--" });

            var files = new List<FileView>();
            if (productId != null)
                files = (await FilesService.GetFilesAsync(filters, (long)productId))
                    .MapList<FileView>();

            Files = files;
            SectionsForSelectList = sectionsForSelectList;
            ProductTitlesForSelectList = productTitlesForSelectList;
            FileFiltersSelectList = fileFiltersSelectList;
            SectionId = sectionId;
            ProductId = productId;
            Filters = filters;

            LastSupportArticles = await GetLastSupportArticlesAsync();

            await InitAsync("Files", "Files");
        }
        
        private async Task<List<SupportArticleView>> GetLastSupportArticlesAsync()
        {
            var pagination = new Pagination();
            pagination.RouteValues.Add("controller", "SupportArticles");
            pagination.RouteValues.Add("action", "Index");
            var articles = (await SupportArticlesService.GetArticlesAsync(pagination))
                .MapList<SupportArticleView>();

            return articles;
        }
    }
}