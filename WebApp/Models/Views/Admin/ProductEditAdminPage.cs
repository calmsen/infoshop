﻿using System.Collections.Generic;

namespace WebApp.Models.Views.Admin
{
    public class ProductEditAdminPage : ProductCreateAdminPage
    {
        public List<ProductSnapshotView> ProductSnapshots { get; set; }

        public ProductEditAdminPage()
            : base()
        {
            ProductSnapshots = new List<ProductSnapshotView>();
        }
    }
}