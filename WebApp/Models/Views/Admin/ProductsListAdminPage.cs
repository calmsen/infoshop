﻿using DomainLogic.Infrastructure.Paging;
using DomainLogic.Models.Enumerations;
using System.Collections.Generic;

namespace WebApp.Models.Views.Admin
{
    public class ProductsListAdminPage
    {
        public List<ProductView> Products { get; set; }

        public Pagination Pagination { get; set; }

        public List<SectionView> SectionsWC { get; set; }

        public SectionView Section { get; set; }

        public long NoPhoto { get; set; }

        public List<AllowableFilterEnum> Filter { get; set; }

        public string Sort { get; set; }

        public SectionsMenu SectionsMenu { get; set; }

        public bool NeedShowUnactiveProducts { get; set; }

        public ProductsListAdminPage()
        {
            Products = new List<ProductView>();
            SectionsWC = new List<SectionView>();
        }
    }
}