﻿using System.Collections.Generic;
using System.Web.Mvc;
using WebApp.Models.Forms;

namespace WebApp.Models.Views.Admin
{
    public class ArticleCreateAdminPage
    {
        public ArticleForm ArticleForm { get; set; }

        public List<SelectListItem> SectionsForSelectList { get; set; }

        public List<SelectListItem> ProductTitlesForSelectList { get; set; }
    }
}