﻿using DomainLogic.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace WebApp.Models.Views.Admin
{
    public class UserInfoPage
    {
        public string Email { get; set; }

        public List<SelectListItem> RolesForSelectList { get; set; }

        public User User { get; set; }

        public List<string> UserRoles { get; set; }
    }
}