﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace WebApp.Models.Views.Admin
{
    public class FaqAdminPage
    {
        public List<FaqView> Faq { get; set; }

        public long? SectionId { get; set; }

        public List<SelectListItem> SectionsForSelectList { get; set; }

        public List<SelectListItem> ProductTitlesForSelectList { get; set; }

        public long? ProductId { get; set; }
    }
}