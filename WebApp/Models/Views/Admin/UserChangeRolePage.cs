﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace WebApp.Models.Views.Admin
{
    public class UserChangeRolePage
    {
        public string UserEmail { get; set; }

        public string UserRole { get; set; }

        public List<SelectListItem> RolesForSelectList { get; set; }
    }
}