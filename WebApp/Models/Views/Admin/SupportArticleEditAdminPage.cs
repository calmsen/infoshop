﻿using WebApp.Models.Forms;

namespace WebApp.Models.Views.Admin
{
    public class SupportArticleEditAdminPage
    {
        public SupportArticleForm ArticleForm { get; set; }
    }
}