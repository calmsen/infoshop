﻿using System.Collections.Generic;
using WebApp.Models.Forms;

namespace WebApp.Models.Views.Admin
{
    public class PostCreateAdminPage
    {
        public PostForm PostForm { get; set; }

        public List<ProductView> Products { get; set; }
    }
}