﻿using DomainLogic.Infrastructure.Paging;
using System.Collections.Generic;

namespace WebApp.Models.Views.Admin
{
    public class SectionsListAdminPage
    {
        /// <summary>
        /// Разделы для текущей страницы
        /// </summary>
        public List<SectionView> Sections { get; set; }

        public Pagination Pagination { get; set; }

        /// <summary>
        /// Текущий раздел  
        /// </summary>
        public SectionView Section { get; set; }

        public SectionsMenu SectionsMenu { get; set; }

        public SectionsListAdminPage()
        {
            Sections = new List<SectionView>();
        }
    }
}