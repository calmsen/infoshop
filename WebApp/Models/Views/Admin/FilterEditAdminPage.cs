﻿using DomainLogic.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using WebApp.Models.Forms;

namespace WebApp.Models.Views.Admin
{
    public class FilterEditAdminPage
    {
        public FilterForm FilterForm { get; set; }

        public List<SelectListItem> FiltersTypes { get; set; }

        public List<SectionTitle> Sections { get; set; }

        public string RedirectToUrl { get; set; }
    }
}