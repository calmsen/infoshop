﻿using DomainLogic.Infrastructure.Paging;
using System.Collections.Generic;

namespace WebApp.Models.Views.Admin
{
    public class RequestCounterAdminPage
    {
        public List<RequestCounterView> RequestCounterList { get; set; }

        public Pagination Pagination { get; set; }
    }
}