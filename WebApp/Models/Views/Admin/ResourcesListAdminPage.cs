﻿using DomainLogic.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace WebApp.Models.Views.Admin
{
    public class ResourcesListAdminPage
    {
        public string ClassKey { get; internal set; }

        public List<SelectListItem> ClassKeysForSelectList { get; internal set; }

        public List<Resource> Resources { get; internal set; }
    }
}