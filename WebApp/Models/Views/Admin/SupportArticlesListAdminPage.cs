﻿using DomainLogic.Infrastructure.Paging;
using System.Collections.Generic;

namespace WebApp.Models.Views.Admin
{
    public class SupportArticlesListAdminPage
    {
        public List<SupportArticleView> Articles { get; set; }

        public Pagination Pagination { get; set; }
    }
}