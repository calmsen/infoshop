﻿using WebApp.Models.Forms;

namespace WebApp.Models.Views.Admin
{
    public class SupportArticleCreateAdminPage
    {
        public SupportArticleForm ArticleForm { get; set; }
    }
}