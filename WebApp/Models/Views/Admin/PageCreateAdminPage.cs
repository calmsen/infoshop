﻿using WebApp.Models.Forms;

namespace WebApp.Models.Views.Admin
{
    public class PageCreateAdminPage
    {
        public PageForm PageForm { get; set; }
    }
}