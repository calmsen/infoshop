﻿using DomainLogic.Infrastructure.Paging;
using System.Collections.Generic;

namespace WebApp.Models.Views.Admin
{
    public class PagesListAdminPage
    {
        public List<PageView> Pages { get; set; }

        public Pagination Pagination { get; set; }
    }
}