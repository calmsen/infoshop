﻿using System.Collections.Generic;
using System.Web.Mvc;
using WebApp.Models.Forms;

namespace WebApp.Models.Views.Admin
{
    public class ServiceCreateAdminPage
    {
        public ServiceForm ServiceForm { get; set; }

        public List<SelectListItem> ServiceTypes { get; set; }

        public string RedirectToUrl { get; set; }
    }
}