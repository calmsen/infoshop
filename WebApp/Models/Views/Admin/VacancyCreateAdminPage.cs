﻿using System.Collections.Generic;
using System.Web.Mvc;
using WebApp.Models.Forms;

namespace WebApp.Models.Views.Admin
{
    public class VacancyCreateAdminPage
    {
        public VacancyForm VacancyForm { get; set; }

        public List<SelectListItem> VacancyStates { get; set; }
    }
}