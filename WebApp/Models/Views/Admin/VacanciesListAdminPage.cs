﻿using System.Collections.Generic;

namespace WebApp.Models.Views.Admin
{
    public class VacanciesListAdminPage
    {
        public List<VacancyView> Vacancies { get; set; }
    }
}