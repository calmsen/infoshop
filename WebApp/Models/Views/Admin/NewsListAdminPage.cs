﻿using DomainLogic.Infrastructure.Paging;
using System.Collections.Generic;

namespace WebApp.Models.Views.Admin
{
    public class NewsListAdminPage
    {
        public List<ArticleView> News { get; set; }

        public Pagination Pagination { get; set; }
    }
}