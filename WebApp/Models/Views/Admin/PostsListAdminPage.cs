﻿using DomainLogic.Infrastructure.Paging;
using System.Collections.Generic;

namespace WebApp.Models.Views.Admin
{
    public class PostsListAdminPage
    {
        public List<PostView> Posts { get; set; }

        public Pagination Pagination { get; set; }
    }
}