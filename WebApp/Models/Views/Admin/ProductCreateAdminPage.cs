﻿using System.Collections.Generic;
using System.Web.Mvc;
using WebApp.Models.Forms;

namespace WebApp.Models.Views.Admin
{
    public class ProductCreateAdminPage
    {
        public ProductForm ProductForm { get; set; }

        /// <summary>
        /// Все разделы, передаем это список в js
        /// </summary>
        public List<SectionView> Sections { get; set; }

        /// <summary>
        /// Список списков разделов, формируем селекты по полному пути
        /// </summary>
        public List<List<SectionView>> SectionsAsPathSiblings { get; set; }

        public Dictionary<long, FilterForSelectList> FiltersForSelectLists { get; set; }

        public List<FilterGroupView> FiltersGroups { get; set; }

        public List<SelectListItem> PartnersForSelectList { get; set; }

        public string RedirectToUrl { get; set; }

        public long NoPhoto { get; set; }

        public List<SelectListItem> TypesForSelectList { get; internal set; }

        public bool ToList { get; internal set; }

        public ProductCreateAdminPage()
        {
            Sections = new List<SectionView>();
            FiltersForSelectLists = new Dictionary<long, FilterForSelectList>();
            SectionsAsPathSiblings = new List<List<SectionView>>();
            FiltersGroups = new List<FilterGroupView>();
        }
    }
}