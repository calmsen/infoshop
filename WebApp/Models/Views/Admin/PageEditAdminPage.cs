﻿using WebApp.Models.Forms;

namespace WebApp.Models.Views.Admin
{
    public class PageEditAdminPage
    {
        public PageForm PageForm { get; set; }
    }
}