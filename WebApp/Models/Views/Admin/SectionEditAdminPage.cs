﻿using System.Collections.Generic;
using System.Web.Mvc;
using WebApp.Models.Forms;

namespace WebApp.Models.Views.Admin
{
    public class SectionEditAdminPage
    {
        public SectionForm SectionForm { get; set; }

        /// <summary>
        /// Все разделы, передаем это список в js
        /// </summary>
        public List<SectionView> Sections { get; set; }

        /// <summary>
        /// Cписок списков разделов, формируем селекты по полному пути
        /// </summary>
        public List<List<SectionView>> SectionsAsPathSiblings { get; set; }

        public List<FilterView> Filters { get; set; }

        public bool ToList { get; set; }

        public List<SelectListItem> FiltersTypes { get; set; }

        public string RedirectToUrl { get; set; }

        public SectionEditAdminPage()
        {
            Sections = new List<SectionView>();
            Filters = new List<FilterView>();
            SectionsAsPathSiblings = new List<List<SectionView>>();
        }
    }
}