﻿using WebApp.Models.Forms;

namespace WebApp.Models.Views.Admin
{
    public class PartnerEditAdminPage
    {
        public PartnerForm PartnerForm { get; set; }

        public string RedirectToUrl { get; set; }
    }
}