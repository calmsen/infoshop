﻿using System.Collections.Generic;
using System.Web.Mvc;
using WebApp.Models.Forms;

namespace WebApp.Models.Views.Admin
{
    public class EditFileAdminPage
    {
        public FileForm FileForm { get; set; }

        public List<SelectListItem> FileFiltersForSelectList { get; set; }

        public string RedirectToUrl { get; set; }

        public long ProductId { get; set; }
    }
}