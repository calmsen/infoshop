﻿using DomainLogic.Infrastructure.Paging;
using System.Collections.Generic;

namespace WebApp.Models.Views.Admin
{
    public class PartnersListAdminPage
    {
        public List<PartnerView> Partners { get; set; }

        public Pagination Pagination { get; set; }
    }
}