﻿using DomainLogic.Infrastructure.Paging;
using System.Collections.Generic;

namespace WebApp.Models.Views.Admin
{
    public class FiltersListAdminPage
    {

        public List<FilterView> Filters { get; set; }

        public Pagination Pagination { get; set; }

        public List<SectionView> SectionsWC { get; set; }

        public SectionView Section { get; set; }

        public long RedirectToSectionId { get; set; }

        public SectionsMenu SectionsMenu { get; set; }

        public FiltersListAdminPage()
        {
            Filters = new List<FilterView>();
            SectionsWC = new List<SectionView>();
        }
    }
}