﻿using DomainLogic.Models.Enumerations;
using System.Collections.Generic;

namespace WebApp.Models.Views.Admin
{
    public class ServicesListAdminPage
    {
        public List<ServiceView> Services { get; set; }

        public List<string> CitiesForSelectList { get; set; }

        public string ActiveCity { get; set; }

        public ServiceTypeEnum ActiveType { get; set; }
    }
}