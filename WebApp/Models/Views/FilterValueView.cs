﻿using Newtonsoft.Json;

namespace WebApp.Models.Views
{
    public class FilterValueView
    {
        public long Id { get; set; }

        public long ValueAsLong { get; set; }

        public double ValueAsDouble { get; set; }

        public long? ValueSet1 { get; set; }

        public long? ValueSet2 { get; set; }

        public long? ValueSet3 { get; set; }

        public long? ValueSet4 { get; set; }

        public long FilterId { get; set; }

        [JsonIgnore]
        public FilterView Filter { get; set; }

        public int Position { get; set; }

        public string Title { get; set; }
    }
}