﻿using System.Collections.Generic;

namespace WebApp.Models.Views
{
    public class FilterGroupView
    {
        public FilterGroupView()
        {
            Filters = new List<FilterSettingsView>();
        }

        public long Id { get; set; }

        public List<FilterSettingsView> Filters { get; set; }

        public string Title { get; set; }
    }
}