﻿using System;

namespace WebApp.Models.Views
{
    public class RequestCounterView
    {
        public long Id { get; set; }

        public DateTime RequestTime { get; set; }

        public int RequestExcevuteTime { get; set; }

        public string RequestUrl { get; set; }
    }
}
