﻿using System.Collections.Generic;

namespace WebApp.Models.Views
{
    public class SectionMenuItem
    {
        public List<SectionView> Sections { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        public Dictionary<string, object> Params { get; set; }
    }
}