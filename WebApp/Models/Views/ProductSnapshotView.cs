﻿using DomainLogic.Models.Enumerations;
using System;
using System.Collections.Generic;

namespace WebApp.Models.Views
{
    public class ProductSnapshotView
    {
        public long Id { get; set; }

        public string Snapshot { get; set; }

        public DateTime CreatedDate { get; set; }

        public long ProductId { get; set; }

        public int UserId { get; set; }

        public UserView User { get; set; }

        public List<ProductStateEnum> ProductStates { get; set; }
    }
}