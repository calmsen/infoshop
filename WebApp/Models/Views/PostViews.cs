﻿using System;

namespace WebApp.Models.Views
{
    public class PostView 
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public long? DescriptionId { get; set; }

        public PostDescriptionView Description { get; set; }

        public string LinkToExpertPost { get; set; }

        public long? MainImageId { get; set; }

        public PostImageView MainImage { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool Activate { get; set; }

        public string Title { get; set; }

        public string Annottion { get; set; }
    }
}