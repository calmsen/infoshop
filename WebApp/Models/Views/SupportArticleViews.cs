﻿using System;

namespace WebApp.Models.Views
{               
    public class SupportArticleView 
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public SupportArticleDescriptionView Description { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Title { get; set; }

        public string Annottion { get; set; }
    }    
}