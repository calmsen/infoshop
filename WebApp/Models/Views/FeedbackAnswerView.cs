﻿using System;

namespace WebApp.Models.Views
{
    public class FeedbackAnswerView
    {
        public long Id { get; set; }

        public int UserId { get; set; }

        public UserView User { get; set; }

        public string Message { get; set; }

        public long FeedbackId { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool NoticedAuthor { get; set; }
    }
}