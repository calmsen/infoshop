﻿namespace WebApp.Models.Views
{
    public class ProductBlockProperty
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}