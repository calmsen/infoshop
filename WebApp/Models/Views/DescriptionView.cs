﻿namespace WebApp.Models.Views
{
    public class DescriptionView
    {
        public long Id { get; set; }

        public string Content { get; set; }
    }
}