﻿using DomainLogic.Models.Enumerations;

namespace WebApp.Models.Views
{
    public class VacancyView
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Title { get; set; }

        public string Address { get; set; }

        public string Employment { get; set; }

        public string Duties { get; set; }

        public string Education { get; set; }

        public string Experience { get; set; }

        public string AdditionalRequirements { get; set; }

        public string Salary { get; set; }

        public string Email { get; set; }

        public string WorkingConditions { get; set; }

        public VacancyStateEnum State { get; set; }
    }
}
