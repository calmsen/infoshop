﻿using DomainLogic.Models.Enumerations;
using System;
using System.Collections.Generic;

namespace WebApp.Models.Views
{
    public class FeedbackView
    {
        public long Id { get; set; }

        public long ThemeId { get; set; }

        public FeedbackThemeEnum Theme { get; set; }

        public string Message { get; set; }

        public DateTime CreatedDate { get; set; }

        public StateEnum State { get; set; }

        public int AnswersAmount { get; set; }

        public List<FeedbackAnswerView> Answers { get; set; }

        public int UserId { get; set; }

        public UserView User { get; set; }

        public int UserIdWhoSetTheState { get; set; }

        public UserView UserWhoSetTheState { get; set; }

        public string UrlReferer { get; set; }

        public long? SectionId { get; set; }

        public SectionView Section { get; set; }

        public long? ProductId { get; set; }

        public ProductView Product { get; set; }

        public List<FeedbackTagView> FeedbackTags { get; set; }

        public List<FeedbackInvitingView> FeedbackInvitings { get; set; }

        public List<FeedbackImageView> Images { get; set; }

        public DropDownMenu<StateEnum> StatesMenu { get; set; }

        public FeedbackView()
        {
            FeedbackTags = new List<FeedbackTagView>();
            Images = new List<FeedbackImageView>();
        }
    }     
}