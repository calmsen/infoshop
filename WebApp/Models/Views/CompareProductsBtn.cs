﻿namespace WebApp.Models.Views
{
    public class CompareProductsBtn
    {
        public long SectionId { get; set; }

        public long ProductsAmountInComparison { get; set; }
    }
}