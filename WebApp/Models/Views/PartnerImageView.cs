﻿using DomainLogic.Interfaces.ImageInterfaces;

namespace WebApp.Models.Views
{
    /// <summary>
    /// Данный класс представляет из себя данные о картинке партнера для отдачи непосредственно в представление.
    /// Может отличаться от доменной модели PartnerImage.
    /// </summary>
    public class PartnerImageView : IImage
    {
        public long Id { get; set; }

        /// <summary>
        /// Ширина картинки
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Высота картинки
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Название картинки
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Внешняя ссылка картинки
        /// </summary>
        public string ExternalLink { get; set; }
    }
}