﻿namespace WebApp.Models.Views
{
    public class UnitConverterView
    {
        public long Id { get; set; }

        public string UnitK { get; set; }

        public string Unit { get; set; }
    }
}