﻿namespace WebApp.Models.Views
{
    public class FeedbackTagView
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Title { get; set; }
    }
}