﻿namespace WebApp.Models.Views
{
    public class ValueView
    {
        public long Id { get; set; }

        public long? ValueId { get; set; }

        public FilterValueView Value { get; set; }

        public long AttributeId { get; set; }
    }
}