﻿using DomainLogic.Models.Enumerations;
using System.Collections.Generic;

namespace WebApp.Models.Views
{
    public class FilterSettingsView
    {
        public FilterSettingsView()
        {
            Values = new List<FilterValueSettingsView>();
        }

        public long Id { get; set; }

        public FilterTypeEnum Type { get; set; }

        public long? UnitConverterIdForCatalog { get; set; }

        public long? UnitConverterIdForProduct { get; set; }

        public bool ShowInGroupSelect { get; set; }

        public List<FilterValueSettingsView> Values { get; set; }

        public string Title { get; set; }
    }
}