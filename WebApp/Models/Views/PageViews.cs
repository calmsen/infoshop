﻿using System;

namespace WebApp.Models.Views
{
    public class PageView 
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public long? DescriptionId { get; set; }

        public PageDescriptionView Description { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Layout { get; set; }

        public string NavLinkName { get; set; }

        public string Title { get; set; }
    }
}