﻿namespace WebApp.Models.Views
{
    public class FeedbackInvitingView
    {
        public int Id { get; set; }

        public long FeedbackId { get; set; }

        public string Emails { get; set; }
    }
}