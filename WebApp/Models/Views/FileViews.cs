﻿using DomainLogic.Models.Enumerations;
using System;
using System.Collections.Generic;

namespace WebApp.Models.Views
{          
    public class FileView 
    {
        public long Id { get; set; }

        public string Name { get; set; } 
        
        public string Version { get; set; }

        public string FileSize { get; set; }

        public List<FileFilterEnum> Filters { get; set; }

        public DateTime CreatedDate { get; set; }

        public string ExternalLink { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }

}