﻿using System.Collections.Generic;

namespace WebApp.Models.Views
{
    public class ProductGroup
    {
        public string Title { get; set; }

        public List<ProductView> Products { get; set; }
    }
}