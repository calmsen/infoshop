﻿namespace WebApp.Models.Views
{
    public class ArticleDescriptionView
    {
        public long Id { get; set; }

        public string Content { get; set; }
    }
}