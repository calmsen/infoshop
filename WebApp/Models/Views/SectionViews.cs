﻿using DomainLogic.Interfaces.ImageInterfaces;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace WebApp.Models.Views
{
    public class SectionView 
    {
        public SectionView()
        {
            FiltersGroups = new List<FilterGroupView>();
            Childs = new List<SectionView>();
        }

        public long Id { get; set; }

        public string Name { get; set; }

        public long ParentId { get; set; }

        public int ProductsAmount { get; set; }
        
        public bool Hidden { get; set; }

        public int Position { get; set; }

        public long PathOfSections { get; set; }

        public long? MainImageId { get; set; }

        public SectionImageView MainImage { get; set; }

        [JsonIgnore]
        public List<SectionImageView> Banners { get; set; }

        public string Title { get; set; }

        public List<FilterGroupView> FiltersGroups { get; set; }

        [JsonIgnore]
        public List<SectionView> Childs { get; set; }
    }
}