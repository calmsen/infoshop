﻿using System.Collections.Generic;

namespace WebApp.Models.Views
{
    public class ProductBlockView
    {
        public ProductBlockView()
        {
            Props = new List<ProductBlockProperty>();
        }

        public string Name { get; set; }

        public string Content { get; set; }

        public List<ProductBlockProperty> Props { get; set; }
    }
}