﻿using DomainLogic.Interfaces.ImageInterfaces;

namespace WebApp.Models.Views
{
    public class ArticleImageView : IImage
    {
        public long Id { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public string Title { get; set; }

        public string ExternalLink { get; set; }

        public int Position { get; set; }
    }
}