﻿namespace WebApp.Models.Views
{
    public class SupportArticleDescriptionView
    {
        public long Id { get; set; }
        public string Content { get; set; }
    }
}