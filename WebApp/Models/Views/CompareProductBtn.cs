﻿namespace WebApp.Models.Views
{
    public class CompareProductBtn
    {
        public long ProductId { get; set; }
        public bool ProductInComparison { get; set; }
    }
}