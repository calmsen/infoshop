﻿namespace WebApp.Models.Views
{
    public class ProductInComparison
    {
        public long ProductId { get; set; }

        public long SectionId { get; set; }
    }
}