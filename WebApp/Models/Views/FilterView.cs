﻿using DomainLogic.Infrastructure.Paging;
using DomainLogic.Models;
using DomainLogic.Models.Enumerations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;

namespace WebApp.Models.Views
{
    public class FilterView
    {
        public FilterView()
        {
            Values = new List<FilterValueView>();
            UnitConverters = new List<UnitConverterView>();
            Sections = new List<SectionTitle>();
        }

        public long Id { get; set; }

        public string Name { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]

        public FilterTypeEnum Type { get; set; }

        public List<FilterValueView> Values { get; set; }

        [JsonIgnore]
        public List<SectionTitle> Sections { get; set; }

        public List<UnitConverterView> UnitConverters { get; set; }

        public long UnitConverterIdForCatalog { get; set; }

        public long UnitConverterIdForProduct { get; set; }

        public bool Multiple { get; set; }

        public string Title { get; set; }

        public string Unit { get; set; }

        public FilterParam FParams { get; set; }

        public string GetValuesTitle()
        {
            var valuesTitle = "--";
            if (FParams != null && FParams.Values.Count > 0)
            {
                valuesTitle = "";
                foreach (var v in Values)
                {
                    foreach (var fpv in FParams.Values)
                    {
                        if (v.Id == fpv)
                        {
                            if (!string.IsNullOrEmpty(valuesTitle))
                            {
                                valuesTitle += ",";
                            }
                            valuesTitle += v.Title;
                        }
                    }
                }
            }
            return valuesTitle;
        }

        public string IsValueChecked(FilterValueView value)
        {
            var checkboxChecked = "";
            if (FParams != null)
            {
                foreach (var fpv in FParams.Values)
                {
                    if (value.Id == fpv)
                    {
                        checkboxChecked = "checked=\"checked\"";
                    }
                }
            }
            return checkboxChecked;
        }

        public string IsValueSelected(FilterValueView value)
        {
            var optionSelected = "";
            if (FParams != null)
            {
                foreach (var fpv in FParams.Values)
                {
                    if (value.Id == fpv)
                    {
                        optionSelected = "selected=\"selected\"";
                    }
                }
            }
            return optionSelected;
        }

        public string[] GetRangeValues()
        {
            var val1 = "";
            var val2 = "";
            if (FParams != null)
            {
                val1 = FParams.Values[0].ToString();
                val2 = FParams.Values[1].ToString();
            }
            return new string[] { val1, val2 };
        }

        public string GetSectionTitlesAsString()
        {
            string res = "";
            if (Sections.Count > 0)
            {
                res += "(" + Sections[0].Title;
                if (Sections.Count > 1)
                    res += ", " + Sections[1].Title;
                if (Sections.Count > 2)
                    res += ", ...";
                res += ")";
            }
            return res;
        }
    }
}