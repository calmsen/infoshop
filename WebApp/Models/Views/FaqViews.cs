﻿using DomainLogic.Models;
using DomainLogic.Models.Enumerations;

namespace WebApp.Models
{          
    public class FaqView
    {
        public long Id { get; set; }

        public FeedbackThemeEnum Theme { get; set; }

        public string Title { get; set; }

        public string Question { get; set; }

        public string Answer { get; set; }

        public long? SectionId { get; set; }
    }

}