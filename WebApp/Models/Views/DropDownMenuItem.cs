﻿using System.Collections.Generic;

namespace WebApp.Models.Views
{
    public class DropDownMenuItem<TValue>
    {
        public TValue Value { get; set; }

        public string Text { get; set; }

        public bool Active { get; set; }

        public Dictionary<string, object> Params { get; set; }
    }
}