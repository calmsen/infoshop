﻿using System;

namespace WebApp
{
    public partial class _500 : System.Web.UI.Page
    {
        public string version = "version=3.3.8.3";
        public string baseUrl;
        public string st0SiteHttpHost;
        public string culture;
        public string cultureUrl;
        public string ruCultureIsActive;
        public string enCultureIsActive;
        public string pageTitle;
        public int statusCode = 500;
        protected void Page_Load(object sender, EventArgs e)
        {
            baseUrl = MvcApplication.UrlUtils.GetBaseUrl();
            st0SiteHttpHost = MvcApplication.St0SiteSettings.HttpHost;
            culture = Request.RequestContext.RouteData.Values.ContainsKey("culture")
                ? Request.RequestContext.RouteData.Values["culture"].ToString()
                : "ru";
            cultureUrl = baseUrl;
            if (!culture.Equals("ru"))
                cultureUrl += culture;
            ruCultureIsActive = culture.Equals("ru") ? "active" : "";
            enCultureIsActive = culture.Equals("en") ? "active" : "";
            pageTitle = Resources.ViewsRes.ErrorPage500PageTitle;
        }
    }
}