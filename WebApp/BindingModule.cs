﻿using DomainLogic.Infrastructure.Mappers;
using DomainLogic.Infrastructure.Providers;
using DomainLogic.Infrastructure.ServiceContainers;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Interfaces;
using NLog;
using WebApp.Infrastructure.Mvc;
using WebApp.Infrastructure.Mvc.Localization;
using WebApp.Infrastructure.Mvc.Utils;
using WebApp.Models;
using WebApp.Models.Views.Default;
using WebApp.UiHelpers;
using WebApp.UiHelpers.Shared;

namespace WebApp
{
    public class BindingModule : IBindingModule
    {
        public void Load(IServiceContainer serviceContainer)
        {
            serviceContainer.Bind<NLog.Logger>(LogManager.GetCurrentClassLogger());
            serviceContainer.Bind<LocalizedModelMetadataProvider>();
            serviceContainer.Bind<LocalizedModelValidatorProvider>();
            serviceContainer.Bind<WebApp.Infrastructure.AutoMapperDefaultProfile>();
            serviceContainer.Bind<Provider<MainLayout>>();

            serviceContainer.Bind<IResourceProvider, ResourceProvider>();
            serviceContainer.Bind<IRenderViewsUtils, RenderViewsUtils>();
            serviceContainer.Bind<IUrlUtils, UrlUtils>();
            serviceContainer.Bind<ILogger, WebApp.Infrastructure.Mvc.Logger>();
            serviceContainer.Bind<ITLS, TLS>();
            serviceContainer.Bind<IRoleManagerWrap, RoleManagerWrap>();
            serviceContainer.Bind<ICaptcha, DomainLogic.Infrastructure.Captcha.DefaultCaptcha>();

            serviceContainer.Bind<MainLayout>((type) => !type.IsGenericType);
            serviceContainer.Bind<MainLayout, ViewModelHolder<LoginViewModel>>();
            serviceContainer.Bind<MainLayout, ViewModelHolder<VerifyCodeViewModel>>();
            serviceContainer.Bind<MainLayout, ViewModelHolder<RegisterViewModel>>();
            serviceContainer.Bind<MainLayout, ViewModelHolder<ForgotPasswordViewModel>>();
            serviceContainer.Bind<MainLayout, ViewModelHolder<ResetPasswordViewModel>>();
            serviceContainer.Bind<MainLayout, ViewModelHolder<SendCodeViewModel>>();
            serviceContainer.Bind<MainLayout, ViewModelHolder<MessageViewModel>>();
            serviceContainer.Bind<MainLayout, ViewModelHolder<IndexViewModel>>();
            serviceContainer.Bind<MainLayout, ViewModelHolder<AddPhoneNumberViewModel>>();
            serviceContainer.Bind<MainLayout, ViewModelHolder<VerifyPhoneNumberViewModel>>();
            serviceContainer.Bind<MainLayout, ViewModelHolder<ChangePasswordViewModel>>();
            serviceContainer.Bind<MainLayout, ViewModelHolder<SetPasswordViewModel>>();
            serviceContainer.Bind<MainLayout, ViewModelHolder<ManageLoginsViewModel>>();

            serviceContainer.BindInNamespaceOf<UserUiHelpers, SectionsSelectListCreater>();
        }
    }
}