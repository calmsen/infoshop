﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="500.aspx.cs" Inherits="WebApp._500" %>
<% Response.StatusCode = statusCode; %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="<%= baseUrl %>favicon.ico" type="image/x-icon">
    <title><%= pageTitle %></title>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<%= baseUrl %>Areas/Default/Content/Bundles/fontello/css/fontello.css" rel="stylesheet" />
    <link href="<%= baseUrl %>Areas/Default/Content/Styles/Site.css?<%= version %>" rel="stylesheet" />
</head>
<body>
    <div class="body-inner">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                     </button>
                    <a class="navbar-brand" href="<%= baseUrl %>"">
                        <img class="InfoShop-logo-img" src="<%= baseUrl %>Content/Styles/Images/InfoShop-logo.png">
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul id="navbar-nav-InfoShop" class="nav navbar-nav">
                        <li>
                            <a href="<%= cultureUrl %>Products/Catalog"><%= Resources.ViewsRes.ProductsNavLink %></a>
                        </li>
                        <li>
                            <a href="<%= cultureUrl %>Files"><%= Resources.ViewsRes.SupportNavLink %></a>
                        </li>
                        <li>
                            <a href="<%= cultureUrl %>Home/About"><%= Resources.ViewsRes.CompanyNavLink %></a>
                        </li>
                        <li>
                            <a href="<%= cultureUrl %>Partners"><%= Resources.ViewsRes.PartnersNavLink %></a>
                        </li>        
                    </ul>
                    <ul class="nav navbar-nav navbar-right navbar-inline">
                        <li class="navbar-form-holder">
                            <form class="navbar-form navbar-right" role="search" action="<%= cultureUrl %>Products/Search">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="search" class="form-control" id="search" name="query" placeholder="<%= Resources.ViewsRes.SearchInputPlaceholder %>">
                                        <span class="input-group-addon">
                                            <button type="submit" class="btn btn-search glyphicon glyphicon-search"></button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </li>
                        <li class="culture-btns">
                            <div class="btn-group">
                                <button data-culture="en" type="button" class="btn btn-default js-set-culture-btn <%= enCultureIsActive %>">en</button>
                                <button data-culture="ru" type="button" class="btn btn-default js-set-culture-btn <%= ruCultureIsActive %>">ru</button>
                            </div>
                        </li>
                    </ul>
            
                </div>        
            </div>                                                  
        </nav>        
        <div class="container-wrap">
            <div class="container-wrap-inner">
                <div class="container container-mh container-mt">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2><%= pageTitle %></h2>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container footer">
            <div class="row">
                <div class="col-sm-4 col-xs-6">
                    <h5 class="header-14-0">
                        <a href="<%= baseUrl %>Files"><%= Resources.ViewsRes.SupportNavLink %></a>
                    </h5>
                    <ul class="list-group">
                        <li class="list-group-item"><a class="link-12-0" href="<%= baseUrl %>Files"><%= Resources.ViewsRes.FilesNavLink %></a></li>
                        <li class="list-group-item"><a class="link-12-0" href="<%= baseUrl %>Srvs"><%= Resources.ViewsRes.ServicesNavLink %></a></li>
                        <li class="list-group-item"><a class="link-12-0" href="<%= baseUrl %>Feedback"><%= Resources.ViewsRes.FeedbackNavLink %></a></li>
                        <li class="list-group-item"><a class="link-12-0" href="<%= baseUrl %>Support articles"><%= Resources.ViewsRes.SupportArticlesNavLink %></a></li>
                    </ul>
                </div>
                <div class="col-sm-4 col-xs-6">
                    <h5 class="header-14-0">
                        <a href="<%= baseUrl %>Home/About"><%= Resources.ViewsRes.CompanyNavLink %></a>
                    </h5>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a class="link-12-0" href="<%= baseUrl %>News"><%= Resources.ViewsRes.NewsNavLink %></a>
                        </li>
                        <li class="list-group-item">
                            <a class="link-12-0" href="<%= baseUrl %>Home/About"><%= Resources.ViewsRes.AboutNavLink %></a>
                        </li>
                        <li class="list-group-item">
                            <a class="link-12-0" href="<%= baseUrl %>Home/CorporateDepartment"><%= Resources.ViewsRes.CorporateDepartmentNavLink %></a>
                        </li>
                        <li class="list-group-item">
                            <a class="link-12-0" href="<%= baseUrl %>Vacancies"><%= Resources.ViewsRes.VacanciesNavLink %></a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-4 hidden-xs footer-social-networks">
                    <h5 class="header-14-0">
                        <%= Resources.ViewsRes.SocialWeb %>
                    </h5>
                    <ul class="list-group fa-ul">
                        <li class="list-group-item vkontakte-item">
                            <a class="link-12-0" href="https://vk.com/we_are_InfoShop"><i class="fa-li fa icon-vkontakte"></i><%= Resources.ViewsRes.VK %></a>
                        </li>
                        <li class="list-group-item instagram-item">
                            <a class="link-12-0" href="https://instagram.com/InfoShop_official/"><i class="fa-li fa fa-instagram"></i>Instagram</a>
                        </li>
                        <li class="list-group-item twitter-item">
                            <a class="link-12-0" href="https://twitter.com/InfoShop_official"><i class="fa-li fa icon-twitter"></i>Twitter</a>
                        </li>
                        <li class="list-group-item youtube-play-item">
                            <a class="link-12-0" href="https://www.youtube.com/c/InfoShopDigitalExperience"><i class="fa-li fa fa-youtube-play"></i>YouTube</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <p class="text-12-0" style="margin: 30px 0;"><%= Resources.ViewsRes.InfoShopDisclaimer %></p>
                    <p class="copyrights text-12-0">© 2014<%= DateTime.Now.Year != 2014 ? "-" + DateTime.Now.Year : "" %> <%= Resources.ViewsRes.CopyrightsCompanyText %></p>
                </div>
            </div>
        </div>
    </div>
    <script>
        var baseUrl = "<%= baseUrl %>";
        var st0SiteHttpHost = "<%= st0SiteHttpHost %>";
        var culture = "<%= culture %>";
        var version = "<%= version %>";
        var jsRoute = "Default_Error_Index"
    </script>
    <script src="<%= baseUrl %>Content/Javascripts/require.js?<%= version %>"></script>
    <script src="<%= baseUrl %>Content/Javascripts/main.config.js?<%= version %>"></script>
</body>
</html>