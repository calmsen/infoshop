﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApp
{
    public class AreaConfig
    {
        public static void RegisterArea<T>(RouteCollection routes, object state) where T : AreaRegistration
        {
            AreaRegistration registration = (AreaRegistration)Activator.CreateInstance(typeof(T));
            AreaRegistrationContext context = new AreaRegistrationContext(registration.AreaName, routes, state);
            string tNamespace = registration.GetType().Namespace;
            if (tNamespace != null)
            {
                context.Namespaces.Add(tNamespace + ".*");
            }
            registration.RegisterArea(context);
        }
    }
}
