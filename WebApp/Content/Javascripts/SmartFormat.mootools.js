﻿/**
 * Extend SmartFormat with MooTools formatting for plurals, Date, Number, and anything with a `format` function:
 */
Smart.addExtensions('formatter', {
    pluralFormatter: function (value, format) {
        if (format && typeof(value) === 'number') {
            var params = format.split('|');
            if (params.length >= 2) {
                var c = Globalize.culture().name.split("-")[0];
                var index = Smart.getPluralRule(c)(value, params.length);/* *2* */
                return params[index];
            }
        }
    }
	,
    listFormatter: function (values, format) {
        if (typeof(values) === 'array') {
            var params = (format && format.split('|')) || ["", ", "];
            if (params.length >= 2) {
                var options = {
                    format: params[0]
					, join: params[1]
					, last: params[2]
                };

                if (options.last) {
                    if (values.length === 2) {
                        return values.join(options.last);
                    }
                    values = Array.clone(values).splice(values.length - 2, 0, options.last);
                }
                return values.join(options.join);
            }
        }
    }
	,
    /*conditionalFormatter: function (value, format) {
        if (format) {
            var params = format.split('|');
            if (params.length >= 2) {
                switch (typeof(value)) {
                    case 'date':
                        return (value <= new Date()) ? params[0] : params[1];
                    case 'number':
                        return (value < params.length) ? params[value] : null;
                    default:
                        return (value ? params[0] : params[1]);
                }
            }
        }
    }
	,*/ /* *3* */
    dateFormatter: function (value, format) {
        if (typeof(value) === 'date') {
            format = format || "long";

            return value.format(format);
        }
    }
	,
    numberFormatter: function (value, format) {
        if (typeof(value) === 'number' && format) {
            if (format.charAt(0) === 'C') {
                return value.formatCurrency(format.substr(1).toInt());
            } else if (format.charAt(0) === 'P') {
                return value.formatPercentage(format.substr(1).toInt());
            }
            /* *1* */
            if (format.charAt(0) === 'N') {
                var options = {};
                options.decimals = format.substr(1).toInt();
                return value.format(options);
            }            
        }
    }
	,
    formatFormatter: function (value, format) {
        if (value !== undefined && value !== null) {
            if (typeof(value.format) === 'function') {
                return value.format(format);
            }
        }
    }

});
