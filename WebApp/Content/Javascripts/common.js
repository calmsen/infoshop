﻿/**
 * файл common может содержать зависемости, которые неуказаны в параметре deps
 */
define("common", ["jquery"], function ($) {
    // Переопределим Deferred объект и Ajax метод. Добавим метод Deferred.wait
    $.OriginalDeferred = $.Deferred;
    $.Deferred = function () {
        var deferred = $.OriginalDeferred();
        deferred.wait = function (fu, fuOnFail, context) {
            context = context || fuOnFail || this;
            fuOnFail = fuOnFail instanceof Function ? fuOnFail : $.noop;

            var defObj = new $.Deferred();
            deferred.done(function () {
                var res = fu.apply(context, arguments);
                defObj.resolve(res);
            });
            deferred.fail(function () {
                var res = fuOnFail.apply(context, arguments);
                defObj.reject(res);
            });
            return defObj;
        };
        return deferred;
    };
    $.OriginalAjax = $.ajax;
    $.ajax = function (url, settings) {
        var deferred = $.OriginalAjax(url, settings);
        deferred.wait = function (fu, fuOnFail, context) {
            context = context || fuOnFail || this;
            fuOnFail = fuOnFail instanceof Function ? fuOnFail : $.noop;
            var defObj = new $.Deferred();
            deferred.done(function () {
                var res = fu.apply(context, arguments);
                defObj.resolve(res);
            });
            deferred.fail(function () {
                var res = fuOnFail.apply(context, arguments);
                defObj.reject(res);
            });
            return defObj;
        };
        return deferred;
    };
    $.OriginalWhen = $.when;
    $.when = function () {
        var deferred = $.OriginalWhen.apply($, arguments);
        deferred.wait = function (fu, fuOnFail, context) {
            context = context || fuOnFail || this;
            fuOnFail = fuOnFail instanceof Function ? fuOnFail : $.noop;
            var defObj = new $.Deferred();
            deferred.done(function () {
                var res = fu.apply(context, arguments);
                defObj.resolve(res);
            });
            deferred.fail(function () {
                var res = fuOnFail.apply(context, arguments);
                defObj.reject(res);
            });
            return defObj;
        };
        return deferred;
    };
    // -- -- --
    return {
        baseUrl: window.baseUrl || location.protocol + '//' + location.host + "/"
        , st0SiteHttpHost: window.st0SiteHttpHost || location.protocol + '//' + location.host + "/"
        , culture: window.culture
        , isAllowLocalization: window.isAllowLocalization
        , utils: {
            declension: function (num, expressions) {
                var result;
                var count = num % 100;
                if (count >= 5 && count <= 20) {
                    result = expressions['2'];
                } else {
                    count = count % 10;
                    if (count == 1) {
                        result = expressions['0'];
                    } else if (count >= 2 && count <= 4) {
                        result = expressions['1'];
                    } else {
                        result = expressions['2'];
                    }
                }
                return result;
            }
            , format: function () {

            }
        }
        , message: {
            // Сделаем поддержку старых сообщений
            error: function (error) {
                error = error || "";
                if (!error.trim() || error.indexOf("<html") != -1 || error.indexOf("<body") != -1 || error.indexOf("<div") != -1 || error.indexOf("<span") != -1) {
                    error = 'Произошла ошибка на сервере.';
                }
                //error = error.split('\\nStackTrace')[0];
                $().toastmessage('showToast', {
                    text: error,
                    type: 'error'
                });
            }
            , warning: function (warning) {
                warning = warning || "";
                if (!warning.trim() || warning.indexOf("<html") != -1 || warning.indexOf("<body") != -1 || warning.indexOf("<div") != -1 || warning.indexOf("<span") != -1) {
                    warning = 'Произошла ошибка на сервере.';
                }
                //warning = warning.split('\\nStackTrace')[0];
                $().toastmessage('showToast', {
                    text: warning,
                    type: 'warning'
                });
            }
            , success: function (success) {
                success = success || "";
                if (!success.trim() || success.indexOf("<html") != -1 || success.indexOf("<body") != -1 || success.indexOf("<div") != -1 || success.indexOf("<span") != -1) {
                    success = 'Произошла ошибка на сервере.';
                }
                //success = success.split('\\nStackTrace')[0];
                $().toastmessage('showToast', {
                    text: success,
                    type: 'success'
                });
            }
        }
        // Работа с подписчиками на события
        , addEventHandler: function (event, handler) {
            this.eventHadlers = this.eventHadlers || {};
            this.eventHadlers[event] = this.eventHadlers[event] || [];

            this.eventHadlers[event].push(handler);
        }
        , executeEventHandlers: function (event, args) {
            if (this.eventHadlers === undefined || this.eventHadlers[event] === undefined || this.eventHadlers[event].length == 0)
                return;

            for (var i in this.eventHadlers[event])
                this.eventHadlers[event][i].apply(this, args);
            this.eventHadlers[event].length = 0;
        }
        , lock: function (key, callback, context) {
            context = context || this;
            this.lockKeys = this.lockKeys || {};

            if (this.lockKeys[key])
                return;
            this.lockKeys[key] = true;
            var obj = this;
            this.addEventHandler(key, function () {
                obj.lockKeys[key] = false;
            });
            callback.call(context);
        }
        , unlock: function (key) {
            this.executeEventHandlers(key);
        }
        // /Работа с подписчиками на события
        //
        //  настройка tinymce
        , tinymce: function (options) {
            //
            options.plugin_preview_width = options.plugin_preview_width ? options.plugin_preview_width + 19 : undefined;
            //
            tinymce.init($.extend({
                setup : function(ed) {
                    ed.on('blur', function(e) {
                        $(ed.targetElm).val(ed.getContent())
                            .valid();
                    });
                    if (options.folder == "News") {
                        ed.on('BeforeSetContent', function (e) {
                            e.content = e.content.replace(new RegExp('<a[^>]*>\s*(<img[^>]*src="' + st0SiteHttpHost + '[^"]*"[^>]*>)\s*<\/a>', 'gi'), "$1");
                        });
                        ed.on('SaveContent', function (e) {
                            e.content = e.content.replace(new RegExp('<img[^>]*src="(' + st0SiteHttpHost + '[^"]*)"[^>]*>', 'gi'), function (m, m1) {
                                return '<a href="' + m1.replace("/middle/", "/max/").replace("/normal/", "/max/").replace("/min/", "/max/")
                                    + '" rel="shadowbox[News];player=img;"><img src="' + m1.replace("/max/", "/middle/").replace("/normal/", "/middle/").replace("/min/", "/middle/") + '" /></a>';
                            });
                        });
                    }
                },
                selector: "#Description_Content, #Description_ContentIn",
                file_browser_callback_types: 'image',
                file_browser_callback : function(field_name, url, type, win){
                    var filebrowser = baseUrl + "Admin/Images/ImageManagerSearch?type=" + options.folder;
                    tinymce.activeEditor.windowManager.open({
                        title : "Менеджер картинок",
                        width : 620,
                        height : 450,
                        url : filebrowser
                    }, {
                        window : win,
                        input : field_name
                    });
                    return false;
                },
                plugin_preview_width: 550,
                plugin_preview_height: 600, 
                plugins: [
                        "advlist textcolor nonbreaking link paste image imagetools media emoticons directionality visualchars visualblocks table template pagebreak fullscreen code preview"
                ],
                toolbar1: "undo redo bold italic underline strikethrough alignleft aligncenter alignright alignjustify bullist numlist outdent indent blockquote nonbreaking",
                toolbar2: "forecolor backcolor styleselect formatselect link image media emoticons ltr rtl visualchars visualblocks",
                toolbar3: "table template pagebreak fullscreen code preview",
                menubar: false,
                toolbar_items_size: 'small',
                convert_urls : false,
                content_css: [baseUrl + "Areas/Default/Content/Styles/Site.css"],
                templates: [
                    {
                        title: "Two columns for two objects", content:
                    "<table style=\"width: 100%;\">" +
                        "<tbody>" +
                            "<tr>" +
                                "<td style=\"width: 50%; padding-right: 15px; vertical-align: top;\">" +
                                    "<h3>Header 3</h3>" +
                                    "<img src=\"http://st0.InfoShop.club/Images/Products/33771.jpg\" width=\"100%\" />" +
                                    "<p>Your description</p>" +
                                "</td>" +
                                "<td style=\"width: 50%; padding-left: 15px; vertical-align: top;\">" +
                                    "<h3>Header 3</h3>" +
                                    "<img src=\"http://st0.InfoShop.club/Images/Products/33771.jpg\" width=\"100%\" />" +
                                    "<p>Your description</p>" +
                                "</td>" +
                            "</tr>" +
                        "</tbody>" +
                    "</table>"
                    },
                    {
                        title: "Two columns (image left)", content:
                    "<table style=\"width: 100%;\">" +
                        "<tbody>" +
                            "<tr>" +
                                "<td style=\"width: 50%; padding-right: 15px; vertical-align: top;\">" +
                                    "<img src=\"http://st0.InfoShop.club/Images/Products/33771.jpg\" width=\"100%\" />" +
                                "</td>" +
                                "<td style=\"width: 50%; padding-left: 15px; vertical-align: top;\">" +
                                    "<h3>Header 3</h3>" +
                                    "<p>Your description</p>" +
                                "</td>" +
                            "</tr>" +
                        "</tbody>" +
                    "</table>"
                    },
                    {
                        title: "Two columns (image right)", content:
                    "<table style=\"width: 100%;\">" +
                        "<tbody>" +
                            "<tr>" +
                                "<td style=\"width: 50%; padding-right: 15px; vertical-align: top;\">" +
                                    "<h3>Header 3</h3>" +
                                    "<p>Your description</p>" +
                                "</td>" +
                                "<td style=\"width: 50%; padding-left: 15px; vertical-align: top;\">" +
                                    "<img src=\"http://st0.InfoShop.club/Images/Products/33771.jpg\" width=\"100%\" />" +
                                "</td>" +
                            "</tr>" +
                        "</tbody>" +
                    "</table>"
                    },
                    {
                        title: "One column (align center and image bottom)", content:
                    "<table style=\"width: 100%;\">" +
                        "<tbody>" +
                            "<tr>" +
                                "<td style=\"width: 100%; padding-right: 15px; vertical-align: top;text-align: center;\">" +
                                    "<h3>Header 3</h3>" +
                                    "<h4>Header 4</h4>" +
                                    "<p>Your description</p>" +
                                    "<img src=\"http://st0.InfoShop.club/Images/Products/33771.jpg\" width=\"100%\" />" +
                                "</td>" +
                            "</tr>" +
                        "</tbody>" +
                    "</table>"
                    },
                    {
                        title: "One column (align left and image bottom)", content:
                    "<table style=\"width: 100%;\">" +
                        "<tbody>" +
                            "<tr>" +
                                "<td style=\"width: 100%; padding-right: 15px; vertical-align: top;text-align: left;\">" +
                                    "<h3>Header 3</h3>" +
                                    "<h4>Header 4</h4>" +
                                    "<p>Your description</p>" +
                                    "<img src=\"http://st0.InfoShop.club/Images/Products/33771.jpg\" width=\"100%\" />" +
                                "</td>" +
                            "</tr>" +
                        "</tbody>" +
                    "</table>"
                    },
                    {
                    title: "One column with slider", content:
                        "<table class=\"blur-effect-wrap\" style=\"width: 100%;\">" +
                            "<tbody>" +
                                "<tr>" +
                                    "<td>" +
                                        "<h4>[style]width:597px;left:44px;top:37px;[/style]</h4>" +
                                    "</td>" +
                                "</tr>" + 
                                "<tr>" +
                                    "<td style=\"width: 100%; position: relative;\">" +
                                        '<img src="http://st0.InfoShop.club/Images/Products/54538.jpg" style="width: 100%;" />' +
                                        '<div class="blur-effect-image-holder">' +
                                            '<img class="blur-effect-image" src="http://st0.InfoShop.club/Images/Products/54537.jpg" />' +
                                            '<div class="blur-effect-container"></div>' +
                                            '<div class="blur-effect-slider"></div>' +
                                        '</div>' +
                                    "</td>" +
                                "</tr>" +
                            "</tbody>" +
                        "</table>"
                    }
                ]
            }, options));
        }
    }
});