﻿define("addJqueryPlugin", [], function () {
    function addJqueryPlugin(classFunc, pluginName) {
        $.fn[pluginName] = function (method) {

            var params = arguments;
            var result = undefined;

            this.each(function () {
                var element = $(this);
                if (!element.data(pluginName)) {
                    var options = method || {};
                    if (typeof options === 'object') {
                        if (!options.element) {
                            options.element = element;
                        }
                    }
                    element.data(pluginName, new classFunc(options));
                    if (!element.data(pluginName).element) {
                        element.data(pluginName).element = element;
                    }
                }
                else if (element.data(pluginName)[method]) {
                    result = element.data(pluginName)[method].apply(element.data(pluginName), Array.prototype.slice.call(params, 1));

                    return result !== undefined ? false : true;
                }
                else {
                    if (typeof method == "string" && element.data(pluginName)) {
                        $.error('Метод ' + method + ' не существует в jQuery.' + pluginName);
                    }
                }
            });

            return result !== undefined ? result : this;
        };
    }

    return addJqueryPlugin;
});