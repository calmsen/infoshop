﻿/**
 * Модуль содержит свойства и методы по работе c селектами    
 * @author Ruslan Rakhmankulov
 */
define("SelectControl", ["jquery", "common", "addJqueryPlugin"], function ($, common, addJqueryPlugin) {
    /**
     * Конструктор SelectControl является прокси для метода init
     * @constructor
     */
    function SelectControl() {
        this.init.apply(this, arguments);
    }
    /**
     * Определим глобальные функции и объекты. Обычно это Utils функции и какие нибудь константы.
     */
    var message = common.message;

    /**
     * В прототипе содержатся все необходимые методы и объект defaults. Все свойства находятся в объекте defaults. 
     */
    $.extend(SelectControl.prototype/* = new ParentClass()*/, {
        /**
         * Все свойства являются переопределяемые через объект options.
         */
        defaults: {
            baseUrl: common.baseUrl
            , element: $()
            , dependentSelectElem: $()  // зависимый фильтр
            , dependentSelectUri: "" // url для получения новых данных для зависимого селекта
            , dependentSelectParam: "" // название параметра для отправки на сервер
            , dependentSelectData: {} // дополнительные параметры для отправки на сервер
            , dependentSelectCache: {} // кэш полученных данных от сервера
            , dependentSelectOnUpdate: $.noop
        }
        /**
         * Метод вызывается при создании экземпляра класса SelectControl
         * @param {Object} options
         */
        , init: function (options) {
            $.extend(this, this.defaults, options);
            
            this.addDependentSelectDataByInit();
            // 
            this.element.on("change", $.proxy(this.selectElemOnChange, this));
            //
            this._dependentSelectLoadingElem = this.dependentSelectElem.closest(".form-group").find(".form-control-loading");
        }
        /**
         * Добавим данные DependentSelectData в dependentSelectCache из данных взятых из селекта
         */
        , addDependentSelectDataByInit: function () {
            var key = this.element.val();
            var selectListItems = this.dependentSelectElem.find("option").get().map(function (el) { return { Value: el.value, Text: el.textContent }; })
            
            this.dependentSelectCache[key] = $.when(selectListItems);
        }
        /**
         * Добавим данные DependentSelectData в dependentSelectCache, получив данные из удаленного сервера
         * @param {String} key
         */
        , addDependentSelectDataByAjax: function (key) {
            this.dependentSelectData[this.dependentSelectParam] = key;
            this.dependentSelectCache[key] = $.ajax({
                url: this.baseUrl + this.dependentSelectUri
                , data: this.dependentSelectData
                , type: "POST"
                , dataType: "json"
                , context: this
                , beforeSend: function () {
                    this.activateDependentSelectElem();
                }
                , complete: function () {
                    this.deactivateDependentSelectElem();
                }
                , error: function () {
                    delete this.dependentSelectCache[key];
                }
            });
        }
        /**
         * Получим данные по ключу. В случае не найденных данных отправляется запрос на удаленный сервер
         * @param {String} key
         * @returns {Object} 
         */
        , getDependentSelectData: function(key){
            if (this.dependentSelectCache[key] === undefined)
                this.addDependentSelectDataByAjax(key);
            return this.dependentSelectCache[key];                 
        }
        /**
         * Обработчик события change на элементе select
         * @param {JQueryEventObject} event
         */
        , selectElemOnChange: function (event) {
            this.changeDependentSelectItems();
        }
        /**
         * Активируем кнопки зависимого селекта
         */
        , activateDependentSelectElem: function () {
            this.dependentSelectElem.attr("disabled", "disabled");
            this._dependentSelectLoadingElem.show();
        }
        /**
         * Деактивируем кнопки зависимого селекта
         */
        , deactivateDependentSelectElem: function () {
            this.dependentSelectElem.removeAttr("disabled");
            this._dependentSelectLoadingElem.hide();
        }
        /**
         * Изменим элементы option зависимого селекта
         */
        , changeDependentSelectItems: function () {
            var key = this.element.val();
            this.getDependentSelectData(key).wait(function (selectListItems) {
                if (key != this.element.val())
                    return;
                this.dependentSelectElem.empty();
                for (i in selectListItems)
                    $("<option/>", {
                        value: selectListItems[i].Value
                        , text: selectListItems[i].Text
                    })
                    .appendTo(this.dependentSelectElem);
                this.dependentSelectOnUpdate();
            }, function () {
                message.warning("Не получилось заполнить список значениями.");
                this.dependentSelectOnUpdate();
            }, this);
        }
    });
    // создаем jquery plugin
    addJqueryPlugin(SelectControl, "selectControl");

    return SelectControl;
});