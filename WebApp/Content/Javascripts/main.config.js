define.amd = false;
var config = {
    urlArgs: window.version || "1.0.0.0"
    , baseUrl: window.baseUrl || "/"
    , map: {
          '*': {
              'css': 'Content/Javascripts/css'
        }
    }
    , paths: {
        text: "Content/Javascripts/requirejs-text-2.0.10"
        , json: "Content/Javascripts/requirejs-json-0.4.0"
        , jquery: "Content/Javascripts/jquery-2.1.1.min"
        , bootstrap: "Content/Bundles/bootstrap/js/bootstrap" // http://getbootstrap.com/customize/?id=d69d6be2204a0601f194
        , bootstrapHoverDropdown: "Content/Javascripts/bootstrap-hover-dropdown.min"
        , jqueryTmpl: "Content/Javascripts/jquery.tmpl"
        , jqueryMask: "Content/Javascripts/jquery.mask.min"
        , jqueryValidate: "Content/Javascripts/jquery.validate"
        , jqueryUnobtrusive: "Content/Javascripts/jquery.validate.unobtrusive"
        , shadowbox: "Content/Bundles/shadowbox/shadowbox-3.0.3.min"
        , shadowboxCss: "Content/Bundles/shadowbox/shadowbox-3.0.3.min"
        , yandexMaps: "//api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU"
        , jqueryToastmessage: "Content/Bundles/toastmessage/js/jquery.toastmessage-min"
        , jqueryToastmessageCss: "Content/Bundles/toastmessage/css/jquery.toastmessage-min"
        , globalize: "Content/Javascripts/globalize/globalize"
        , globalizeCultureEnUs: "Content/Javascripts/globalize/cultures/globalize.culture.en-US"
        , globalizeCultureRuRu: "Content/Javascripts/globalize/cultures/globalize.culture.ru-RU"
        , jqueryValidateGlobalize: "Content/Javascripts/jquery.validate.globalize"
        , SmartFormatCore: "Content/Javascripts/SmartFormat.core"
        , SmartFormatPluralRules: "Content/Javascripts/SmartFormat.pluralRules"
        , SmartFormatMootools: "Content/Javascripts/SmartFormat.mootools"
        , ajaxfileupload: "Content/Javascripts/ajaxfileupload"
        , jqueryUi: "Content/Bundles/jquery-ui/js/jquery-ui-1.10.4.custom.min"
        , jqueryUiCss: "Content/Bundles/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.min"
        , jqueryEasing: "Content/Javascripts/jquery.easing.1.3.min"
        , jqueryAlerts: 'Content/Bundles/jquery-alerts/jquery.alerts'
        , jqueryAlertsCss: 'Content/Bundles/jquery-alerts/jquery.alerts'
        , jqueryBrowser: "Content/Javascripts/jquery.browser.min"
        , jsPlumb: "Content/Javascripts/dom.jsPlumb-1.7.0-min" // ��� �������� uml ����
        , jqueryConnect: "Content/Javascripts/jquery.connect" // ��� ���������� ���� �����
        , JsDiff: "Content/Javascripts/diff" // ��� ��������� ����������� � ������
        , jqPlot: "Content/Bundles/jqplot/jquery.jqplot"
        , jqPlotCss: "Content/Bundles/jqplot/jquery.jqplot.min"
        , jqPlotCanvasTextRenderer: "Content/Bundles/jqplot/plugins/jqplot.canvasTextRenderer.min"
        , jqPlotCategoryAxisRenderer: "Content/Bundles/jqplot/plugins/jqplot.categoryAxisRenderer.min"
        , jqPlotBarRendere: "Content/Bundles/jqplot/plugins/jqplot.barRenderer.min"
        , jqPlotPointLabels: "Content/Bundles/jqplot/plugins/jqplot.pointLabels.min"
        , jqPlotDateAxisRenderer: "Content/Bundles/jqplot/plugins/jqplot.dateAxisRenderer.min"
        , jqPlotHighlighter: "Content/Bundles/jqplot/plugins/jqplot.highlighter.min"
        , jqPlotCursor: "Content/Bundles/jqplot/plugins/jqplot.cursor.min"
        , jqPlotCanvasAxisTickRenderer: "Content/Bundles/jqplot/plugins/jqplot.canvasAxisTickRenderer.min"
        , jqPlotEnhancedLegendRenderer: "Content/Bundles/jqplot/plugins/jqplot.enhancedLegendRenderer.min"
        , tinymce: "Content/Bundles/tinymce/tinymce.min"
        , bootstrapMultiselect: "Content/Bundles/bootstrap/js/bootstrap-multiselect"
        , bootstrapMultiselectCss: "Content/Bundles/bootstrap/css/bootstrap-multiselect"
        , breakpoints: "Content/Javascripts/breakpoints"
        , device: "Content/Javascripts/device.min"
        , fancybox: "Content/Bundles/fancybox/jquery.fancybox.pack"
        , fancyboxCss: "Content/Bundles/fancybox/jquery.fancybox"
        , cookie: "Content/Javascripts/jquery.cookie"
        , jcarousel: "Content/Javascripts/jquery.jcarousel.min"
        , uiToTop: "Content/Bundles/UItoTop/js/jquery.ui.totop.min"
        , uiToTopCss: "Content/Bundles/UItoTop/css/ui.totop"

        , common: "Content/Javascripts/common"
        , CommonTmpl: "Content/Templates/Common"
        , SelectControl: "Content/Javascripts/SelectControl"
        , addJqueryPlugin: "Content/Javascripts/addJqueryPlugin"
        // Default 
        , "Default/CommonTmpl": "Areas/Default/Content/Templates/Common"
        , "Default/MainLayout": "Areas/Default/Content/Javascripts/MainLayout"
        , "Default/Files": "Areas/Default/Content/Javascripts/Files"
        , "Default/Home": "Areas/Default/Content/Javascripts/Home"
        , "Default/Products": "Areas/Default/Content/Javascripts/Products"
        , "Default/Feedback": "Areas/Default/Content/Javascripts/Feedback"
        , "Default/Account": "Areas/Default/Content/Javascripts/Account"
        , "Default/Resumes": "Areas/Default/Content/Javascripts/Resumes"
        , "Default/Srvs": "Areas/Default/Content/Javascripts/Srvs"
        , "Default/BlurEffectImage": "Areas/Default/Content/Javascripts/BlurEffectImage"
        // Admin
        , "Admin/CommonTmpl": "Areas/Admin/Content/Templates/Common"
        , "Admin/Products": "Areas/Admin/Content/Javascripts/Products"
        , "Admin/Files": "Areas/Admin/Content/Javascripts/Files"
        , "Admin/Sections": "Areas/Admin/Content/Javascripts/Sections"
        , "Admin/Filters": "Areas/Admin/Content/Javascripts/Filters"
        , "Admin/News": "Areas/Admin/Content/Javascripts/News"
        , "Admin/Partners": "Areas/Admin/Content/Javascripts/Partners"
        , "Admin/Posts": "Areas/Admin/Content/Javascripts/Posts"
        , "Admin/Pages": "Areas/Admin/Content/Javascripts/Pages"
        , "Admin/MainImage": "Areas/Admin/Content/Javascripts/MainImage"
        , "Admin/ImagesV2": "Areas/Admin/Content/Javascripts/ImagesV2"
        , "Admin/ImageSettings": "Areas/Admin/Content/Javascripts/ImageSettings"
        , "Admin/ProductTypes": "Areas/Admin/Content/Javascripts/ProductTypes"
        , "Admin/Srvs": "Areas/Admin/Content/Javascripts/Srvs"
        , "Admin/Vacancies": "Areas/Admin/Content/Javascripts/Vacancies"
        , "Admin/Resources": "Areas/Admin/Content/Javascripts/Resources"
        , "Admin/Options": "Areas/Admin/Content/Javascripts/Options"
        , "Admin/SupportArticles": "Areas/Admin/Content/Javascripts/SupportArticles"
        , "Admin/ApplicationFiles": "Areas/Admin/Content/Javascripts/ApplicationFiles"
        , "Admin/Faq": "Areas/Admin/Content/Javascripts/Faq"
        , "Admin/Mailing": "Areas/Admin/Content/Javascripts/Mailing"
    }
    , shim: {
        jquery: {
            exports: '$'
        }
        , bootstrap: {
            exports: '$'
            , deps: ["jquery"]
        }
        , bootstrapHoverDropdown: {
            exports: '$'
            , deps: ["bootstrap"]
        }
        , jqueryTmpl: {
            exports: '$'
            , deps: ["jquery"]
            , init: function ($) {
                $.extend($.tmpl.tag, {
                    "var": {
                        open: "var $1;"
                    }
                });
            }
        }
        , jqueryMask: {
            exports: '$'
            , deps: ["jquery"]
            , init: function ($) {
                // ���������������� ������
                $("[data-module='jqueryMask']").each(function () {
                    var el = $(this);
                    el.mask(el.data("format"));
                });
            }
        }
        , jqueryValidate: {
            exports: '$'
            , deps: ["jquery"]
            , init: function ($) {
                $.validator.setDefaults({
                    ignore: ""
                });
            }
        }
        , jqueryUnobtrusive: {
            exports: '$'
            , deps: ["jqueryValidateGlobalize"]
            , init: function () {
                /**
                 * http://stackoverflow.com/questions/7930808/boolean-value-of-true-for-required-attribute-on-mvc-net-property
                 */
                $.validator.addMethod('mustBeTrue', function (value) {
                    return value; // We don't need to check anything else, as we want the value to be true.
                }, '');

                // and an unobtrusive adapter
                $.validator.unobtrusive.adapters.add('mustbetrue', {}, function (options) {
                    options.rules['mustBeTrue'] = true;
                    options.messages['mustBeTrue'] = options.message;
                });
                $(function () {
                    $.validator.unobtrusive.parse(document);
                });
            }
        }
        , globalize: {
            exports: 'Globalize'
        }
        , globalizeCultureEnUs: {
            deps: ["globalize"]
        }
        , globalizeCultureRuRu: {
            deps: ["globalize"]
        }
        , jqueryValidateGlobalize: {
            deps: ["jqueryValidate", "globalizeCultureEnUs", "globalizeCultureRuRu"]
        }
        , shadowbox: {
            exports: 'Shadowbox'
            , deps: ["jquery", "device", "css!shadowboxCss"]
            , init: function ($) {
                Shadowbox.clearGallery = function (gallery) {
                    var self = this;
                    // ������ ��� �������� ������� � ������ ��
                    for (var i in self.cache) {
                        if (self.cache[i].gallery === gallery) {
                            self.removeCache(self.cache[i].link);
                        }
                    }
                };
                Shadowbox.initGalleries = function (objects) {
                    for (var i in objects) {
                        Shadowbox.addCache(objects[i].link, objects[i]);
                    }
                }

                Shadowbox.bodyInnerOnMouseover = function () {
                    if (Shadowbox.gallery.length <= 1) {
                        if (document.getElementById("sb-body-inner").style.cursor != "default")
                            document.getElementById("sb-body-inner").style.cursor = "default";
                    } else {
                        if (document.getElementById("sb-body-inner").style.cursor != "pointer")
                            document.getElementById("sb-body-inner").style.cursor = "pointer";
                    }
                        
                }
                if (!$("html").hasClass("mobile"))
                    Shadowbox.init({
                        continuous: true,
                    });
                else
                    $(function () {
                        $("a[rel*=shadowbox]").on("click", function (event) {
                            event.preventDefault();
                        });
                    });
                
                
            }
        }
        , yandexMaps: {
            exports: 'ymaps'
        }
        , jqueryToastmessage: {
            exports: '$'
            , deps: ["jquery", "css!jqueryToastmessageCss"]
            , init: function ($) {
                // �������� ��������� �� ���������
                $().toastmessage({
                    sticky: false,
                    inEffectDuration: 300,
                    stayTime: 5000,
                    position: 'top-right',
                    close: function () {
                        //
                    }
                });
            }
        }
        , SmartFormatPluralRules: {
            deps: ["SmartFormatCore"]
        }
        , SmartFormatMootools: {
            deps: ["SmartFormatPluralRules"]
        }
        , jqueryUi: {
            exports: '$'
            , deps: ["jquery", "css!jqueryUiCss"]
        }
        , jqueryEasing: {
            exports: '$'
            , deps: ["jquery"]
        }
        , yandexMaps: {
            exports: 'ymaps'
        }
        , ajaxfileupload: {
            exports: '$'
            , deps: ["jquery"]
        }
        , jqueryAlerts: {
            exports: '$'
            , deps: ["jquery", "jqueryBrowser", "css!jqueryAlertsCss"]
        }
        , jqueryBrowser: {
            exports: '$'
            , deps: ["jquery"]
        }
        , jsPlumb: {
            exports: 'jsPlumb'
            , deps: ["jquery", "jqueryUi"]
        }
        , jqueryConnect: {
            exports: '$'
            , deps: ["jquery"]
        }
        , jqPlot: {
            exports: '$'
            , deps: ["jquery", "css!jqPlot"]
        }
        , jqPlotCanvasTextRenderer: {
            exports: '$'
            , deps: ["jqPlot"]
        }
        , jqPlotCategoryAxisRenderer: {
            exports: '$'
            , deps: ["jqPlot"]
        }
        , jqPlotBarRendere: {
            exports: '$'
            , deps: ["jqPlot"]
        }
        , jqPlotPointLabels: {
            exports: '$'
            , deps: ["jqPlot"]
        }
        , jqPlotDateAxisRenderer: {
            exports: '$'
            , deps: ["jqPlot"]
        }
        , jqPlotHighlighter: {
            exports: '$'
            , deps: ["jqPlot"]
        }
        , jqPlotCursor: {
            exports: '$'
            , deps: ["jqPlot"]
        }
        , jqPlotCanvasAxisTickRenderer: {
            exports: '$'
            , deps: ["jqPlot"]
        }
        , jqPlotEnhancedLegendRenderer: {
            exports: '$'
            , deps: ["jqPlot"]
        }
        , tinymce: {
            exports: 'tinymce'
        }
        , bootstrapMultiselect: {
            exports: '$'
            , deps: ["bootstrap", "css!bootstrapMultiselectCss"]
        } 
        , breakpoints: {
            exports: '$'
            , deps: ["jquery"]
        }
        , fancybox: {
            exports: '$'
            , deps: ["jquery", "css!fancyboxCss"]
        }
        , cookie: {
            exports: '$'
            , deps: ["jquery"]
        }
        , jcarousel: {
            exports: '$'
            , deps: ["jquery"]
            , init: function ($) {
                $('[data-jcarousel]').each(function () {
                    var el = $(this);
                    el.jcarousel(el.data());
                });

                $('[data-jcarousel-control]').each(function () {
                    var el = $(this);
                    el.jcarouselControl(el.data());
                });
            }
        }
        , uiToTop: {
            exports: 'uiToTop'
            , deps: ["jquery", "jqueryEasing", "css!uiToTopCss"]
            , init: function ($) {
                $().UItoTop({ easingType: 'easeOutQuart', scrollSpeed: 800 });

            }
        }
    }
    , config: {
        text: {
            onXhrComplete: function (xhr, url) {
                if (url.indexOf(".htm") !== -1) {
                    var jqueryTmplsHolder = document.getElementById('jqueryTmplsHolder');
                    if (!jqueryTmplsHolder) {
                        jqueryTmplsHolder = document.createElement('div');
                        jqueryTmplsHolder.id = 'jqueryTmplsHolder';
                        var body = document.getElementsByTagName('body')[0];
                        body.insertBefore(jqueryTmplsHolder, body.firstChild);
                    }
                    var div = document.createElement('div');
                    div.innerHTML = xhr.responseText;
                    for (var i = 0; i < div.children.length; i++) {
                        if (document.getElementById(div.children[i].id) === null) {
                            jqueryTmplsHolder.appendChild(div.children[i]);
                            i--;
                        }
                    }
                } else if (url.indexOf(".css") !== -1 && document.getElementById(url) === null) {
                    var stylesHolder = document.getElementById('stylesHolder');
                    if (!stylesHolder) {
                        stylesHolder = document.createElement('div');
                        stylesHolder.id = 'stylesHolder';
                        var body = document.getElementsByTagName('body')[0];
                        body.insertBefore(stylesHolder, body.firstChild);
                    }
                    var style = document.createElement('style');
                    style.id = url;
                    style.innerHTML = xhr.responseText;
                    stylesHolder.appendChild(style);
                }
            }
        }
    }
};

require.config(config);
//
function fireEvent(element, event) {
    if (document.createEventObject) {
        // dispatch for IE
        var evt = document.createEventObject();
        return element.fireEvent('on' + event, evt)
    }
    else {
        // dispatch for firefox + others
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent(event, true, true); // event type,bubbling,cancelable
        return !element.dispatchEvent(evt);
    }
}

function addRoute(route, deps, action) {
    window[route] = {
        deps: deps,
        action: action
    };
}

addRoute("Default_Error_Index", ["Default/MainLayout"]);
addRoute("Default_Home_Index", ["Default/Home"], function (Home) {
    new Home();
});
addRoute("Default_Home_CorporateDepartment", ["yandexMaps"], function (ymaps) {
    ymaps.ready(function () {
        var map = new ymaps.Map("map", {
            center: [43.180293, 131.916869],
            zoom: 16
        });
        map.controls.add('mapTools');
        map.controls.add('zoomControl');
        var placemark = new ymaps.Placemark(
            [43.180293, 131.916869],
            {
                content: '�.�����������, �������� �������� ������������, ��� ...',
                balloonContent: '�������� InfoShop'
            }
        );
        map.geoObjects.add(placemark);
    });

});
addRoute("Default_Files_Index", ["Default/Files"], function (Files) {
    new Files({
        resources: {
            ViewsRes: $("[data-views-res]").data("viewsRes")
            , ProductsRes: $("[data-products-res]").data("productsRes")
        }
        , files: $("[data-files]").data("files")
    })
    .setFilesListEvents();
});
addRoute("Default_Srvs_Index", ["Default/Srvs"], function (Srvs) {
    new Srvs().setSrvsListEvents();
});
addRoute("Default_Feedback_Index", ["Default/Feedback"], function (Feedback) {
    new Feedback().setFeedbackFormEvents();
});
addRoute("Default_Feedback_Show", ["Default/Feedback"], function (Feedback) {
    var feedback = new Feedback({
        validationAttributes: $("#feedback-invite-message-form").data("validationAttributes")
    });
    feedback.setFeedbackDetailEvents();
});
addRoute("Default_Feedback_List", ["Default/Feedback"], function (Feedback) {
    new Feedback().setFeedbackListEvents();
});
addRoute("Default_News_Show", ["jcarousel", "shadowbox"]);
addRoute("Default_Resumes_Index", ["Default/Resumes"], function (Resumes) {
    new Resumes().setResumeFormEvents();
});
addRoute("Default_Products_Index", ["Default/Products"], function (Products) {
    new Products({
        resources: {
            ProductsRes: $("[data-products-res]").data("productsRes")
        }
    }).setProductsListEvent();
});
addRoute("Default_Products_Show", ["Default/Products"], function (Products) {
    new Products({
        productId: $("#productDetail").data("productId")
        , resources: {
            ViewsRes: $("#productDetail").data("viewsRes")
            , ProductsRes: $("#productDetail").data("productsRes")
        }
        , files: $("#productDetail").data("files")
    })
    .setDetailProductEvents();
});
addRoute("Default_Products_Compare", ["Default/Products"], function (Products) {
    new Products({
        resources: {
            ViewsRes: $("#productsComparison").data("viewsRes")
            , ProductsRes: $("#productsComparison").data("productsRes")
        }
    }).setCompareProductEvents();
});
addRoute("Default_Account_Login", ["Default/Account"], function (Account) {
    new Account().setFormEvents();
});
addRoute("Default_Account_Register", ["Default/Account"], function (Account) {
    new Account().setFormEvents();
});
addRoute("Default_Common_BlurEffectTest", ["jquery", "Default/BlurEffectImage"], function ($) {
    $(".blur-effect-slider").blurEffectImage();
});
addRoute("Admin_Common_ApplicationFiles", ["Admin/ApplicationFiles"], function (ApplicationFiles) {
    new ApplicationFiles();
});
addRoute("Admin_Files_EditFile", ["Admin/Files"], function (Files) {
    new Files().setEditFileFormEvents();
});
addRoute("Admin_Files_CreateFile", ["Admin/Files"], function (Files) {
    new Files().setEditFileFormEvents();
});
addRoute("Admin_Files_Index", ["Admin/Files"], function (Files) {
    new Files().setFilesListForAdminEvents();
});
addRoute("Admin_Faq_EditFaq", ["Admin/Faq"], function (Faq) {
    new Faq().setFaqFormEvents();
});
addRoute("Admin_Faq_CreateFaq", ["Admin/Faq"], function (Faq) {
    new Faq().setFaqFormEvents();
});
addRoute("Admin_Filters_Edit", ["Admin/Filters"], function (Filters) {
    new Filters({
        validationAttributes: $("#filter-form").data("validationAttributes")
        , sections: $("#filter-form").data("sections")
    }).setFilterFormEvents();
});
addRoute("Admin_Filters_Create", ["Admin/Filters"], function (Filters) {
    new Filters({
        validationAttributes: $("#filter-form").data("validationAttributes")
        , sections: $("#filter-form").data("sections")
    }).setFilterFormEvents();
});
addRoute("Admin_Filters_Index", ["Admin/Filters"], function (Filters) {
    new Filters().setFiltersListEvents();
});
addRoute("Admin_Mailing_Index", ["Admin/Mailing"], function (Mailing) {
    new Mailing();
});
addRoute("Admin_News_Edit", ["Admin/News"], function (News) {
    new News();
});
addRoute("Admin_News_Create", ["Admin/News"], function (News) {
    new News();
});
addRoute("Admin_Pages_Edit", ["Admin/Pages"], function (Pages) {
    new Pages().setPageFormEvents();
});
addRoute("Admin_Pages_Create", ["Admin/Pages"], function (Pages) {
    new Pages().setPageFormEvents();
});
addRoute("Admin_Partners_Edit", ["Admin/Partners"], function (Partners) {
    new Partners();
});
addRoute("Admin_Partners_Create", ["Admin/Partners"], function (Partners) {
    new Partners();
});
addRoute("Admin_Posts_Edit", ["Admin/Posts"], function (Posts) {
    new Posts().setPostFormEvents();
});
addRoute("Admin_Products_Index", ["Admin/Products"]);
addRoute("Admin_Posts_Create", ["Admin/Posts"], function (Posts) {
    new Posts().setPostFormEvents();
});
addRoute("Admin_Products_Edit", ["Admin/Products"], function (Products) {
    new Products({
        sections : $("#product-form").data("sections")
        , partnersForSelectList: $("#product-form").data("partnersForSelectList")
        , validationAttributes: $("#product-form").data("validationAttributes")
    })
    .setProductFormEvents();
});
addRoute("Admin_Products_Create", ["Admin/Products"], function (Products) {
    new Products({
        sections: $("#product-form").data("sections")
        , partnersForSelectList: $("#product-form").data("partnersForSelectList")
        , validationAttributes: $("#product-form").data("validationAttributes")
    })
    .setProductFormEvents();
});
addRoute("Admin_ProductTypes_Edit", ["Admin/ProductTypes"], function (ProductTypes) {
    new ProductTypes().setProductTypeFormEvents();
});
addRoute("Admin_ProductTypes_Create", ["Admin/ProductTypes"], function (ProductTypes) {
    new ProductTypes().setProductTypeFormEvents();
});
addRoute("Admin_Sections_Edit", ["Admin/Sections"], function (Sections) {
    new Sections({
        sectionId: $("#section-form").data("sectionId")
        , sections: $("#section-form").data("sections")
        , filtersTypes: $("#section-form").data("filtersTypes")
    })
    .setSectionFormEvents();
});
addRoute("Admin_Sections_Create", ["Admin/Sections"], function (Sections) {
    new Sections({
        sectionId: $("#section-form").data("sectionId")
        , sections: $("#section-form").data("sections")
        , filtersTypes: $("#section-form").data("filtersTypes")
    })
    .setSectionFormEvents();
});
addRoute("Admin_Sections_Index", ["Admin/Sections"], function (Sections) {
    new Sections().setSectionsListEvents();
});
addRoute("Admin_Srvs_Edit", ["Admin/Srvs"], function (Srvs) {
    new Srvs();
});
addRoute("Admin_Srvs_Create", ["Admin/Srvs"], function (Srvs) {
    new Srvs();
});
addRoute("Admin_SupportArticles_Edit", ["Admin/SupportArticles"], function (SupportArticles) {
    new SupportArticles();
});
addRoute("Admin_SupportArticles_Create", ["Admin/SupportArticles"], function (SupportArticles) {
    new SupportArticles();
});
addRoute("Admin_Vacancies_Edit", ["Admin/Vacancies"], function (Vacancies) {
    new Vacancies();
});
addRoute("Admin_Vacancies_Create", ["Admin/Vacancies"], function (Vacancies) {
    new Vacancies();
});
addRoute("Admin_Resources_Index", ["Admin/Resources"], function (Resources) {
    new Resources();
});
addRoute("Admin_Options_Index", ["Admin/Options"], function (Options) {
    new Options();
});

if (jsRoute && window[jsRoute]) {
    require(window[jsRoute].deps, window[jsRoute].action);
}