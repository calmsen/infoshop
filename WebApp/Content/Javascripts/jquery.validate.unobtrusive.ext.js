﻿define("jqueryUnobtrusiveExt", ["jquery", "jqueryUnobtrusive"], function ($) {
    $.validator.addMethod("mustbetrue", function (value, element, params) {
        return value.toLowerCase() == "true" ? true : false;
    });
    $.validator.unobtrusive.adapters.addBool('mustbetrue');
});