﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebApp.Infrastructure.Mvc.Localization
{
    /// <summary>
    /// creates adapters for client side validation
    /// </summary>
    public class ValidationAttributeAdapterFactory
    {
        /// <summary>
        /// Create client validation rules for Data Annotation attributes.
        /// </summary>
        /// <param name="attribute">Attribute</param>
        /// <param name="errorMessage">Not formatted error message (should contain {0} etc}</param>
        /// <returns>A collection of rules (or an empty collection)</returns>
        public IEnumerable<ModelClientValidationRule> Create(ValidationAttribute attribute, string errorMessage)
        {
            Type attrType = attribute.GetType();
            if (attrType == typeof(RangeAttribute))
            {
                var attr = (RangeAttribute)attribute;
                return new[]
                {
                        new ModelClientValidationRangeRule(errorMessage,attr.Minimum,attr.Maximum)
                    };
            }
            if (attrType == typeof(RegularExpressionAttribute))
            {
                var attr = (RegularExpressionAttribute)attribute;
                return new[]
                {
                        new ModelClientValidationRegexRule(errorMessage, attr.Pattern)
                    };
            }
            if (attrType == typeof(RequiredAttribute))
            {
                var attr = (RequiredAttribute)attribute;
                return new[]
                {
                        new ModelClientValidationRequiredRule(errorMessage)
                    };
            }
            if (attrType == typeof(StringLengthAttribute))
            {
                var attr = (StringLengthAttribute)attribute;
                return new[]
                {
                        new ModelClientValidationStringLengthRule(errorMessage, attr.MinimumLength,attr.MaximumLength)
                    };
            }
            if (attrType == typeof(System.ComponentModel.DataAnnotations.CompareAttribute))
            {
                var attr = (System.ComponentModel.DataAnnotations.CompareAttribute)attribute;
                return new[]
                {
                        new ModelClientValidationEqualToRule(errorMessage, attr.OtherProperty)
                    };
            }
            if (attrType == typeof(System.ComponentModel.DataAnnotations.MaxLengthAttribute))
            {
                var attr = (System.ComponentModel.DataAnnotations.MaxLengthAttribute)attribute;
                return new[]
                {
                        new ModelClientValidationMaxLengthRule(errorMessage, attr.Length)
                    };
            }
            if (attrType == typeof(System.ComponentModel.DataAnnotations.MinLengthAttribute))
            {
                var attr = (System.ComponentModel.DataAnnotations.MinLengthAttribute)attribute;
                return new[]
                {
                        new ModelClientValidationMaxLengthRule(errorMessage, attr.Length)
                    };
            }
            if (attrType == typeof(System.ComponentModel.DataAnnotations.CreditCardAttribute))
            {
                var attr = (System.ComponentModel.DataAnnotations.CreditCardAttribute)attribute;
                return new[]
                {
                        new ModelClientValidationRule
                        {
                            ValidationType = "creditcard",
                            ErrorMessage = errorMessage
                        }
                    };
            }
            if (attrType == typeof(System.ComponentModel.DataAnnotations.EmailAddressAttribute))
            {
                var attr = (System.ComponentModel.DataAnnotations.EmailAddressAttribute)attribute;
                return new[]
                {
                        new ModelClientValidationRule
                        {
                            ValidationType = "email",
                            ErrorMessage = errorMessage
                        }
                    };
            }
            if (attrType == typeof(System.ComponentModel.DataAnnotations.UrlAttribute))
            {
                var attr = (System.ComponentModel.DataAnnotations.UrlAttribute)attribute;
                return new[]
                {
                        new ModelClientValidationRule
                        {
                            ValidationType = "url",
                            ErrorMessage = errorMessage
                        }
                    };
            }
            if (attrType == typeof(System.ComponentModel.DataAnnotations.FileExtensionsAttribute))
            {
                var attr = (System.ComponentModel.DataAnnotations.FileExtensionsAttribute)attribute;
                var rule = new ModelClientValidationRule
                {
                    ValidationType = "accept",
                    ErrorMessage = errorMessage
                };
                string attrExtensionsNormalized = attr.Extensions.Replace(" ", String.Empty).Replace(".", String.Empty).ToLowerInvariant();
                rule.ValidationParameters["exts"] = attrExtensionsNormalized;

                return new[]
                {
                        rule
                    };
            }
            return new ModelClientValidationRule[0];
        }
    }
}