﻿using DomainLogic.Interfaces;
using DomainLogic.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using WebApp.Infrastructure.Mvc.Utils;

namespace WebApp.Infrastructure.Mvc.Localization
{
    public class ResourceProvider: IResourceProvider
    {
        [Inject]
        public ResourceUtils ResourceUtils { get; set; }

        /// <summary>
        /// Сервис для работы с ресурсами
        /// </summary>
        [Inject]
        public IResourcesService ResourcesService { get; set; }

        /// <summary>
        /// Читаем ресурс
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string ReadResource(string key)
        {
            Resource resource = ResourcesService.GetResourceByKey(key);
            return GetResourceValue(resource, key);
        }

        /// <summary>
        /// Получает ресурсы
        /// </summary>
        /// <param name="classKey"></param>
        /// <returns></returns>
        public Dictionary<string, string> ReadResources(string classKey)
        {
            string startResourceKey = $"Resources.{classKey}.";
            List<Resource> resources = ResourcesService.GetResources().Where(x => x.Key.StartsWith(startResourceKey)).ToList();
            Dictionary<string, string> resourceDict = new Dictionary<string, string>();
            foreach (KeyValuePair<string, string> kv in ResourceUtils.ReadResources(classKey))
            {
                string endResourceKey = "." + kv.Key;

                Resource resource = resources.FirstOrDefault(x => x.Key.EndsWith(endResourceKey));
                resourceDict.Add(kv.Key, GetResourceValue(resource, startResourceKey + kv.Key));
            }
            return resourceDict;
        }

        /// <summary>
        /// Получает ресурсы  для указанной локали
        /// </summary>
        /// <param name="classKey"></param>
        /// <returns></returns>
        public string ReadResourcesAsJson(string classKey)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(ReadResources(classKey));
        }

        /// <summary>
        /// Получает значение ресурса локализации
        /// </summary>
        /// <param name="resource"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetResourceValue(Resource resource, string key)
        {
            if (resource != null)
                return !System.Threading.Thread.CurrentThread.CurrentCulture.Name.Equals("ru") && !string.IsNullOrEmpty(resource.ValueIn)
                    ? resource.ValueIn
                    : resource.Value;
            else
                return ResourceUtils.ReadResource(key);
        }
    }
}