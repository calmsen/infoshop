﻿using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace WebApp.Infrastructure.Mvc.Localization
{
    /// <summary>
    /// Metadata provider used to localize models and their meta data.
    /// </summary>
    public class LocalizedModelMetadataProvider : DataAnnotationsModelMetadataProvider
    {
        private IResourcesService _resourcesService;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="resourcesService">The string provider.</param>
        public LocalizedModelMetadataProvider(IResourcesService resourcesService)
        {
            _resourcesService = resourcesService;
        }

        /// <summary>
        /// Gets the metadata for the specified property.
        /// </summary>
        /// <param name="attributes">The attributes.</param>
        /// <param name="containerType">The type of the container.</param>
        /// <param name="modelAccessor">The model accessor.</param>
        /// <param name="modelType">The type of the model.</param>
        /// <param name="propertyName">The name of the property.</param>
        /// <returns>
        /// The metadata for the property.
        /// </returns>
        protected override ModelMetadata CreateMetadata(IEnumerable<Attribute> attributes, Type containerType,
                                                        Func<object> modelAccessor, Type modelType, string propertyName)
        {
            var metadata = base.CreateMetadata(attributes, containerType, modelAccessor, modelType, propertyName);
            if (containerType == null || propertyName == null)
                return metadata;

            var displayAttr = attributes.OfType<DisplayAttribute>().FirstOrDefault();
            if (displayAttr != null && displayAttr.ResourceType != null && displayAttr.Name != null)
            {
                var resourceName = displayAttr.ResourceType.FullName + "." + displayAttr.Name;
                Resource resource = _resourcesService.GetResourceByKey(resourceName);
                if (resource != null)
                    metadata.DisplayName = !System.Threading.Thread.CurrentThread.CurrentCulture.Name.Equals("ru") && !string.IsNullOrEmpty(resource.ValueIn) 
                        ? resource.ValueIn 
                        : resource.Value;
            }

            return metadata;
        }
    }
}