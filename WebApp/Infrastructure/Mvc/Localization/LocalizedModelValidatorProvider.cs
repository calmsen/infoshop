﻿using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace WebApp.Infrastructure.Mvc.Localization
{
    /// <summary>
    /// Used to localize DataAnnotation attribute error messages and view models
    /// </summary>
    public class LocalizedModelValidatorProvider : DataAnnotationsModelValidatorProvider
    {
        private readonly IResourcesService _resourcesService;

        private readonly ValidationAttributeAdapterFactory _validationAttributeAdapterFactory;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="resourcesService">The string provider.</param>
        public LocalizedModelValidatorProvider(IResourcesService resourcesService, ValidationAttributeAdapterFactory validationAttributeAdapterFactory)
        {
            _resourcesService = resourcesService;
            _validationAttributeAdapterFactory = validationAttributeAdapterFactory;
        }

        /// <summary>
        /// Gets a list of validators.
        /// </summary>
        /// <param name="metadata">The metadata.</param>
        /// <param name="context">The context.</param>
        /// <param name="attributes">The list of validation attributes.</param>
        /// <returns>
        /// A list of validators.
        /// </returns>
        protected override IEnumerable<ModelValidator> GetValidators(ModelMetadata metadata, ControllerContext context,
                                                                     IEnumerable<Attribute> attributes)
        {
            var items = attributes.ToList();
            if (AddImplicitRequiredAttributeForValueTypes && metadata.IsRequired &&
                !items.Any(a => a is RequiredAttribute))
                items.Add(new RequiredAttribute());



            var validators = new List<ModelValidator>();
            foreach (var attr in items.OfType<ValidationAttribute>())
            {
                string formattedError = null;
                if (attr.ErrorMessageResourceName == null || attr.ErrorMessageResourceType == null)
                {
                    if (attr.ErrorMessage != null)
                        formattedError = FormatErrorMessage(metadata.GetDisplayName(), attr.ErrorMessage, attr);
                    else
                        formattedError = attr.FormatErrorMessage(metadata.GetDisplayName());
                }
                else
                {
                    var resourceName = attr.ErrorMessageResourceType.FullName + "." + attr.ErrorMessageResourceName;
                    Resource resource = _resourcesService.GetResourceByKey(resourceName);
                    if (resource != null)
                    {
                        string errorMessage = !System.Threading.Thread.CurrentThread.CurrentCulture.Name.Equals("ru") && !string.IsNullOrEmpty(resource.ValueIn)
                            ? resource.ValueIn
                            : resource.Value;
                        formattedError = FormatErrorMessage(metadata.GetDisplayName(), errorMessage, attr);
                    }
                    else
                        formattedError = attr.FormatErrorMessage(metadata.GetDisplayName());
                }


                var clientRules = GetClientRules(metadata, context, attr, formattedError);

                validators.Add(new MyValidator(attr, formattedError, metadata, context, clientRules));
            }


            return validators;
        }

        /// <summary>
        /// Get client rules
        /// </summary>
        /// <param name="metadata">Model meta data</param>
        /// <param name="context">Controller context</param>
        /// <param name="attr">Attribute being localized</param>
        /// <param name="formattedError">Localized error message</param>
        /// <returns>Collection (may be empty) with error messages for client side</returns>
        protected virtual IEnumerable<ModelClientValidationRule> GetClientRules(ModelMetadata metadata,
                                                                                ControllerContext context,
                                                                                ValidationAttribute attr,
                                                                                string formattedError)
        {
            var clientValidable = attr as IClientValidatable;
            var clientRules = clientValidable == null
                                  ? _validationAttributeAdapterFactory.Create(attr, formattedError)
                                  : clientValidable.GetClientValidationRules(
                                      metadata, context).ToList();

            foreach (var clientRule in clientRules)
            {
                clientRule.ErrorMessage = formattedError;
            }

            return clientRules;
        }
        protected virtual string FormatErrorMessage(string name, string errorMessage, ValidationAttribute attribute)
        {
            Type attrType = attribute.GetType();
            if (attrType == typeof(RangeAttribute))
            {
                var attr = (RangeAttribute)attribute;
                return string.Format(CultureInfo.CurrentCulture, errorMessage, name, attr.Minimum, attr.Maximum);
            }
            if (attrType == typeof(RegularExpressionAttribute))
            {
                var attr = (RegularExpressionAttribute)attribute;
                return string.Format(CultureInfo.CurrentCulture, errorMessage, name, attr.Pattern);
            }
            if (attrType == typeof(StringLengthAttribute))
            {
                var attr = (StringLengthAttribute)attribute;
                return string.Format(CultureInfo.CurrentCulture, errorMessage, name, attr.MaximumLength, attr.MinimumLength);
            }
            if (attrType == typeof(System.ComponentModel.DataAnnotations.CompareAttribute))
            {
                var attr = (System.ComponentModel.DataAnnotations.CompareAttribute)attribute;
                return string.Format(CultureInfo.CurrentCulture, errorMessage, name, attr.OtherProperty);
            }
            if (attrType == typeof(System.ComponentModel.DataAnnotations.MaxLengthAttribute))
            {
                var attr = (System.ComponentModel.DataAnnotations.MaxLengthAttribute)attribute;
                return string.Format(CultureInfo.CurrentCulture, errorMessage, name, attr.Length);
            }
            if (attrType == typeof(System.ComponentModel.DataAnnotations.MinLengthAttribute))
            {
                var attr = (System.ComponentModel.DataAnnotations.MinLengthAttribute)attribute;
                return string.Format(CultureInfo.CurrentCulture, errorMessage, name, attr.Length);
            }
            return string.Format(CultureInfo.CurrentCulture, errorMessage, name);
        }

        private class MyValidator : ModelValidator
        {
            private readonly ValidationAttribute _attribute;
            private readonly IEnumerable<ModelClientValidationRule> _clientRules;
            private readonly string _errorMsg;

            public MyValidator(ValidationAttribute attribute, string errorMsg, ModelMetadata metadata,
                               ControllerContext controllerContext, IEnumerable<ModelClientValidationRule> clientRules)
                : base(metadata, controllerContext)
            {
                _attribute = attribute;
                _errorMsg = errorMsg;
                _clientRules = clientRules;
            }

            public override bool IsRequired
            {
                get { return _attribute is RequiredAttribute; }
            }

            public override IEnumerable<ModelClientValidationRule> GetClientValidationRules()
            {

                return _clientRules;
            }

            public override IEnumerable<ModelValidationResult> Validate(object container)
            {
                var context = new ValidationContext(container, null, null);
                var result = _attribute.GetValidationResult(Metadata.Model, context);
                if (result == null)
                    yield break;

                yield return new ModelValidationResult { Message = _errorMsg };
            }
        }
    }
}
    