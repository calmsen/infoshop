﻿using DomainLogic.Interfaces;
using System;

namespace WebApp.Infrastructure.Mvc
{
    /// <summary>
    /// Класс содержит методы по работе с логированием. 
    /// </summary>
    public class Logger : ILogger
    {
        public NLog.Logger _logger { get; set; }
        /// <summary>
        /// Запись сообщения в лог
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public void Error(string message, Exception innerException)
        {
            Elmah.ErrorLog.GetDefault(System.Web.HttpContext.Current).Log(new Elmah.Error(new Exception(message, innerException)));
        }
        /// <summary>
        /// Запись сообщения в лог
        /// </summary>
        /// <param name="message"></param>
        public void Error(string message)
        {
            Elmah.ErrorLog.GetDefault(System.Web.HttpContext.Current).Log(new Elmah.Error(new Exception(message)));
        }                               
        /// <summary>
        /// Запись сообщения в лог
        /// </summary>
        /// <param name="exception"></param>
        public void Error(Exception exception)
        {
            Elmah.ErrorLog.GetDefault(System.Web.HttpContext.Current).Log(new Elmah.Error(exception));
        }
        /// <summary>
        /// Запись сообщения в лог
        /// </summary>
        /// <param name="message"></param>
        public void Info(string message, params object[] args)
        {
            _logger.Info(message, args);
        }
    }
}