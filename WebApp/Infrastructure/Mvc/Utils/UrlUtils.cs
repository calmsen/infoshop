﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApp.Infrastructure.Mvc.Utils
{
    /// <summary>
    /// Класс для работы с путями и url
    /// </summary>
    public class UrlUtils : IUrlUtils
    {
        [Inject]
        public CommonUtils CommonUtils { get; set; }
        
        /// <summary>
        /// Получает базовый url
        /// <see cref="http://stackoverflow.com/questions/7413466/how-can-i-get-the-baseurl-of-site"/>
        /// </summary>
        /// <returns></returns>
        public string GetBaseUrl()
        {
            string baseUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host;
            if (HttpContext.Current.Request.Url.Scheme.IndexOf("s") == -1 && HttpContext.Current.Request.Url.Port != 80)
                baseUrl += ":" + HttpContext.Current.Request.Url.Port;
            else if (HttpContext.Current.Request.Url.Scheme.IndexOf("s") > 0 && HttpContext.Current.Request.Url.Port != 443)
                baseUrl += ":" + HttpContext.Current.Request.Url.Port;
             baseUrl += HttpContext.Current.Request.ApplicationPath.TrimEnd('/') + "/";

             return baseUrl;
        }

        /// <summary>
        /// Генерирует ссылку
        /// <see cref="http://stackoverflow.com/questions/699782/creating-a-url-in-the-controller-net-mvc"/>
        /// </summary>
        /// <param name="action">Действие</param>
        /// <param name="controller">Контроллер</param>
        /// <param name="parameters">Дополнительные параметры</param>
        /// <returns></returns>
        public string UrlAction(string action, string controller, object parameters = null)
        {
            UrlHelper Url = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return Url.Action(action, controller, parameters);
        }
        
        /// <summary>
        /// Генерирует ссылку
        /// </summary>
        /// <param name="action">Действие</param>
        /// <param name="controller">Контроллер</param>
        /// <param name="parameters">Дополнительные параметры</param>
        /// <returns></returns>
        public string UrlAction(string action, string controller, RouteValueDictionary parameters = null)
        {
            UrlHelper Url = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return Url.Action(action, controller, parameters);
        }
        
        /// <summary>
        /// Получает полный путь к файлу
        /// </summary>
        /// <param name="filePath">Относительный путь</param>
        /// <returns></returns>
        public string MapPath(string filePath)
        {
            if (filePath.IndexOf(":\\") >= 0)
                return filePath;
            return AppDomain.CurrentDomain.BaseDirectory + filePath.Replace("~", string.Empty).TrimStart('/', '\\').Replace("/", Path.DirectorySeparatorChar.ToString());
        }
        
        /// <summary>
        /// Расширяет словарь values новыми значениями из словарей extValues
        /// </summary>
        /// <param name="values">Словарь, который нужно расширить</param>
        /// <param name="extValues">Словари, которые будут расширять словарь values</param>
        /// <returns>Словарь values</returns>
        public RouteValueDictionary Extend(RouteValueDictionary values, params Dictionary<string, object>[] extValues)
        {
            for (int i = 0; i < extValues.Length; i++)
            {
                if (extValues[i] != null && extValues[i].Count > 0)
                    extValues[i].ToList().ForEach(x => { values[x.Key] = x.Value; });
            }
            return values;
        }
        
        /// <summary>
        /// Преобразует словарь Dictionary в словарь RouteValueDictionary
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public RouteValueDictionary ToRouteValueDictionary(Dictionary<string, object> values)
        {
            RouteValueDictionary routeValues = new RouteValueDictionary();
            if (values != null && values.Count > 0)
                values.ToList().ForEach(x => { routeValues[x.Key] = x.Value; });
            return routeValues;
        }
        
        /// <summary>
        /// Преобразует объект source в словарь. 
        /// </summary>
        /// <param name="source">Объект, который будет преобразовываться</param>
        /// <param name="bindingAttr">Флаги, которые говарят какие свойства преобразовывать</param>
        /// <returns>Новый словарь</returns>
        public RouteValueDictionary AsRouteValueDictionary(object source, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
        {
            Dictionary<string, object> d = source.GetType().GetProperties(bindingAttr).ToDictionary
            (
                propInfo => propInfo.Name,
                propInfo => propInfo.GetValue(source, null)
            );
            RouteValueDictionary rvd = new RouteValueDictionary();
            d.ToList().ForEach(x => { rvd[x.Key] = x.Value; });
            return rvd;
        }
        
        /// <summary>
        /// Преобразует объект source в словарь. 
        /// </summary>
        /// <param name="source">Объект, который будет преобразовываться</param>
        /// <param name="queryString">Коллекция значений для сверки на пустые значения</param>
        /// <param name="bindingAttr">Флаги, которые говарят какие свойства преобразовывать</param>
        /// <returns>Новый словарь</returns>
        public RouteValueDictionary AsRouteValueDictionaryOfFormatedValues(object source, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
        {
            Dictionary<string, object> d = source.GetType().GetProperties(bindingAttr).ToDictionary
            (
                propInfo => propInfo.Name,
                propInfo => (object)CommonUtils.GetFormatedValue(source, propInfo)
            );
            RouteValueDictionary rvd = new RouteValueDictionary();
            d.ToList().ForEach(x => { rvd[x.Key] = x.Value; });
            return rvd;
        }        
    }
}