﻿using System;
using System.Linq;
using System.Web;

namespace WebApp.Infrastructure.Mvc.Utils
{
    /// <summary>
    /// Содержит методы по работе с куками
    /// </summary>
    public class CookieUtils
    {
        /// <summary>
        /// Проверяет есть ли кука с ключом key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool ContainsCookie(string key) 
        {
            return HttpContext.Current.Request.Cookies.AllKeys.Contains(key);
        }

        /// <summary>
        /// Получает куку по ключу key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetCookie(string key) 
        {
            if (!ContainsCookie(key))
                return null;

            return HttpContext.Current.Request.Cookies[key].Value;           
        }

        /// <summary>
        /// Устанавливает куку с ключом key и со значением value на срок days дней
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="days"></param>
        public void SetCookie(string key, string value = null, int? days = null) 
        {
            HttpCookie cookie = new HttpCookie(key);
            if (value != null)
                cookie.Value = value;
            if (days != null)
                cookie.Expires = DateTime.Now.AddDays((double)days);
            HttpContext.Current.Response.Cookies.Add(cookie);        
        }

        /// <summary>
        /// Удаляет куку с ключом key
        /// </summary>
        /// <param name="key"></param>
        public void DeleteCookie(string key) 
        {
            HttpCookie cookie = new HttpCookie(key);
            cookie.Expires = DateTime.Now.AddDays(-1);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }
    }
}