﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Utils;
using RazorEngine;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace WebApp.Infrastructure.Mvc.Utils
{
    /// <summary>
    /// Получает представления
    /// </summary>
    public class RenderViewsUtils : IRenderViewsUtils
    {
        /// <summary>
        /// Работа с путями и url
        /// </summary>
        [Inject]
        public UrlUtils UrlUtils { get; set; }

        /// <summary>
        /// Получает представления по указанному пути и переданной модели
        /// </summary>
        /// <param name="viewPath"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public string RenderPartialToString(string viewPath, object model)
        {
            string viewAbsolutePath = UrlUtils.MapPath(viewPath);

            var viewSource = File.ReadAllText(viewAbsolutePath);

            string renderedText = Razor.Parse(viewSource, model);
            return renderedText;
        }
        
        /// <summary>
        /// Получает представление
        /// </summary>
        /// <param name="context"></param>
        /// <param name="partialViewName"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public string RenderPartialToString(ControllerContext context, string partialViewName, object model)
        {
            ViewEngineResult result = ViewEngines.Engines.FindPartialView(context, partialViewName);

            var viewData = new ViewDataDictionary() { Model = model };

            if (result.View != null)
            {
                var sb = new StringBuilder();

                var sw = new StringWriter(sb);
                using (var output = new HtmlTextWriter(sw))
                {
                    var viewContext = new ViewContext(context, result.View, viewData, new TempDataDictionary(), output);
                    result.View.Render(viewContext, output);
                }

                return sb.ToString();
            }

            return string.Empty;
        }
        
        /// <summary>
        /// Получает представление
        /// </summary>
        /// <param name="controllerName"></param>
        /// <param name="partialViewName"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public string RenderPartialToString(string controllerName, string partialViewName, object model)
        {
            var controller = (ControllerBase)ControllerBuilder.Current.GetControllerFactory().CreateController(HttpContext.Current.Request.RequestContext, controllerName);

            var context = new ControllerContext(HttpContext.Current.Request.RequestContext, controller);

            return RenderPartialToString(context, partialViewName, model);
        }
    }
}