﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using Resources;
using System.Text.RegularExpressions;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Infrastructure.Attributes;

namespace WebApp.Infrastructure.Mvc.Utils
{
    /// <summary>
    /// Работа с ресурсами для локализации
    /// </summary>
    public class ResourceUtils
    {
        /// <summary>
        /// Работа со строками
        /// </summary>
        [Inject]
        public StringUtils StringUtils { get; set; }
        
        /// <summary>
        /// Словарь ресурсов локализации
        /// </summary>
        private Dictionary<string, Dictionary<string, string>> _resourcesDict = new Dictionary<string, Dictionary<string, string>>();

        /// <summary>
        /// Получает ресурсы для указанной локали
        /// </summary>
        /// <param name="classKey"></param>
        /// <param name="requestedCulture"></param>
        /// <returns></returns>
        public Dictionary<string, string> ReadResources(string classKey, CultureInfo requestedCulture)
        {
            string key = classKey + "_" + requestedCulture.Name.ToLower();
            if (!_resourcesDict.ContainsKey(key))
            {
                string resourceName = "WebApp.Resources.";
                if (classKey.Equals("ViewsRes"))
                    resourceName += classKey;
                else
                    resourceName += StringUtils.TrimEnd(classKey, "Res") + "." + classKey;

                var resourceManager = new ResourceManager(resourceName, typeof(ViewsRes).Assembly);

                using (var resourceSet = resourceManager.GetResourceSet(requestedCulture, true, true))
                {

                    _resourcesDict.Add(key, resourceSet
                        .Cast<DictionaryEntry>()
                        .ToDictionary(x => (string)x.Key, x => (string)x.Value));
                }
            }
            return _resourcesDict[key];
        }

        /// <summary>
        /// Получает ресурсы
        /// </summary>
        /// <param name="classKey"></param>
        /// <returns></returns>
        public Dictionary<string, string> ReadResources(string classKey)
        {
            return ReadResources(classKey, System.Threading.Thread.CurrentThread.CurrentCulture);
        }

        /// <summary>
        /// Получает ресурсы  для указанной локали
        /// </summary>
        /// <param name="classKey"></param>
        /// <returns></returns>
        public string ReadResourcesAsJson(string classKey, CultureInfo requestedCulture)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(ReadResources(classKey, requestedCulture));
        }

        /// <summary>
        /// Получает ресурсы
        /// </summary>
        /// <param name="classKey"></param>
        /// <returns></returns>
        public string ReadResourcesAsJson(string classKey)
        {
            return ReadResourcesAsJson(classKey, System.Threading.Thread.CurrentThread.CurrentCulture);
        }

        /// <summary>
        /// Получает ресурс по указанному ключу
        /// </summary>
        /// <param name="resourceName"></param>
        /// <returns></returns>
        public string ReadResource(string resourceName)
        {
            resourceName = Regex.Replace(resourceName, "^Resources.", "");
            if (resourceName.IndexOf(".") == -1)
                return null;
            string[] resourceParts = resourceName.Split('.');
            string classKey = resourceParts[0];
            string key = resourceParts[1];
            return ReadResources(classKey)[key];
        }
    }
}