﻿using DomainLogic.Infrastructure.Utils;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace System.Web.Mvc.Html
{
    /// <summary>
    /// Получает информацию по указанному полю
    /// </summary>
    public static class FieldInfoHtmlHelper
    {
        /// <summary>
        /// Работа с рефлексией
        /// </summary>
        public static ReflectorUtils ReflectorUtils { get; set; }

        /// <summary>
        /// Получает информацию по указанному полю
        /// </summary>
        /// <typeparam name="TContainer"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="Html"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static Dictionary<string, object> FieldInfo<TContainer, TValue>(this HtmlHelper<TContainer> Html, Expression<Func<TContainer, TValue>> expression)
        {
            var ViewData = Html.ViewData;
            // если поле является select
            var list = (List<SelectListItem>)(ViewData["list"] ?? new List<SelectListItem>());
            // -- -- --
            // если поле является TextArea
            var rows = ViewData["rows"];
            //-- -- --
            var classAttr = ViewData["class"];
            var placeholder = ViewData["placeholder"];
            var disabled = ViewData["disabled"];
            var readonlyAttr = ViewData["readonly"];
            var onChangeAttr = ViewData["onchange"];
            var id = (string)ViewData["id"];
            var name = (string)ViewData["name"];
            bool nameExists = true;
            if (ViewData["nameExists"] != null)
            {
                nameExists = (bool)ViewData["nameExists"];
            }
            if (name == null)
                name = Html.NameFor(m => m).ToHtmlString();
            if (id == null)
                id = Html.IdFor(m => m).ToHtmlString();
            //var value = Html.ValueFor(m => m);
            var value = (string)ViewData["value"];
            if (value == null)
                value = ViewData.Model != null ? ViewData.Model.ToString() : "";
            var propName = ViewData.ModelMetadata.PropertyName ?? null;
            var valid = true;
            var dataAttributes = "";
            if (ViewData["__Model"] != null)
            {
                var fullPath = Html.ViewData.TemplateInfo.HtmlFieldPrefix;
                string[] arr = fullPath.Split('.');
                var path = "";
                for (int i = 0; i < arr.Length; i++)
                {
                    if (i == arr.Length - 1)
                    {
                        propName = arr[i];
                        break;
                    }
                    if (!path.Equals(""))
                    {
                        path += ".";
                    }
                    path += arr[i];
                }
                Type type;
                if (path.Equals(""))
                {
                    type = ViewData["__Model"].GetType();
                }
                else
                {
                    type = ReflectorUtils.GetPropType(ViewData["__Model"], path);
                }

                value = ReflectorUtils.GetPropValue(ViewData["__Model"], (!path.Equals("") ? path + "." : "") + propName).ToString();
                value = value != null ? value.ToString() : "";

                var metaData = ModelMetadataProviders
                .Current
                .GetMetadataForProperty(null, type, propName);
                dataAttributes = String.Join(" ", Html.GetUnobtrusiveValidationAttributes(metaData.PropertyName, metaData).Select(d => d.Key + "=\"" + d.Value + "\"").ToList<string>().ToArray<string>());
                ModelState ms;
                if (Html.ViewData.ModelState.TryGetValue(fullPath, out ms))
                {
                    valid = ms.Errors.Count == 0;
                }
            }
            else
            {
                valid = Html.ViewData.ModelState.IsValidField(propName);
                dataAttributes = String.Join(" ", Html.GetUnobtrusiveValidationAttributes(propName, ViewData.ModelMetadata).Select(d => d.Key + "=\"" + d.Value + "\"").ToList<string>().ToArray<string>());
            }
            string nullIfValue = (string)ViewData["nullIfValue"];
            if (!string.IsNullOrEmpty(nullIfValue) && value.Equals(nullIfValue))
                value = "";

            // найдем все data атрибуты
            for (int i = 0; i < ViewData.Keys.Count; i++ )
            {
                if (ViewData.Keys.ElementAt(i).IndexOf("data_") == 0 || ViewData.Keys.ElementAt(i).IndexOf("data-") == 0)
                {
                    dataAttributes += " " + ViewData.Keys.ElementAt(i).Replace("_", "-") + "=\"" + ViewData.Values.ElementAt(i) + "\"";
                }
            }
            return new Dictionary<string, object> 
            { 
                {"class", classAttr},
                {"placeholder", placeholder},
                {"disabled", disabled},
                {"readonly", readonlyAttr},
                {"onchange", onChangeAttr},
                {"nameExists", nameExists},
                {"name", name},
                {"id", id},
                {"value", value},
                {"valid", valid},
                {"dataAttributes", dataAttributes},
                {"list", list},
                {"rows", rows}
            };
        }
    }
}