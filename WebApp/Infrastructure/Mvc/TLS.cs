﻿using DomainLogic.Interfaces;
using DomainLogic.Models.Enumerations;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Remoting.Messaging;
using System.Web;

namespace WebApp.Infrastructure.Mvc
{
    /// <summary>
    /// Класс содержит методы по работе с данными для потока (Thread Local Storage). Экземпляр класса следует использовать во внедрение зависимостей.
    /// Класс содержит три реализации поддержки хранилища для исполняемого потока. ThreadStatic и CallContext можно использовать 
    /// в независимости от библиотеки System.Web, то есть в не веб прилож
    /// </summary>
    public class TLS : ITLS
    {
        /// <summary>
        /// Режим использования контекста 
        /// </summary>
        public TlsMode Mode { get; set; }

        /// <summary>
        /// Словарь в котором хранятся все данные потока
        /// </summary>
        public Dictionary<string, object> Data
        {
            get
            {
                if (Mode == TlsMode.HttpContext)
                {
                    if (HttpContext.Current.Items["TLS"] == null)
                        HttpContext.Current.Items["TLS"] = new Dictionary<string, object>();
                    return (Dictionary<string, object>)HttpContext.Current.Items["TLS"];
                }
                else
                {
                    if (CallContext.LogicalGetData("TLS") == null)
                        CallContext.LogicalSetData("TLS", new Dictionary<string, object>());
                    return (Dictionary<string, object>)CallContext.LogicalGetData("TLS");
                }
            }
        }

        /// <summary>
        /// Устанавливает значение
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void Set(string name, object value)
        {
            Data[name] = value;
        }

        /// <summary>
        /// Получает значение
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public object Get(string name)
        {
            if (!Contains(name))
                return null;

            return Data[name];
        }

        /// <summary>
        /// Получает значение и преобразует в нужный тип
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public T Get<T>(string name)
        {
            return (T)Get(name);
        }

        /// <summary>
        /// Проверяет установлено ли значение
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool Contains(string name)
        {
            return Data.ContainsKey(name);
        }

        /// <summary>
        /// Очищает весь словарь, при необходимости уничтожает вызывает метод Dispose
        /// </summary>
        public void Clear()
        {
            Dictionary<string, object> data = Data;
            if (data.Count == 0)
                return;
            foreach (var k in data.Keys)
                if (data[k] is IDisposable)
                    ((IDisposable)data[k]).Dispose();

            Data.Clear();
        }
    }
}