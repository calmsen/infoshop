﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using DomainLogic.Infrastructure.ServiceContainers;

namespace WebApp.Infrastructure.Mvc
{
    /// <summary>
    /// Класс разрешает зависимости классов MVC
    /// </summary>
    public class WebAppDependencyResolver : IDependencyResolver
    {
        private IServiceContainer _serviceContainer;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="kernel"></param>
        public WebAppDependencyResolver(IServiceContainer kernel)
        {
            _serviceContainer = kernel;
        }

        /// <summary>
        /// Получает сервис
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public object GetService(Type serviceType)
        {
            return _serviceContainer.Get(serviceType);
        }

        /// <summary>
        /// Получает сервисы
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _serviceContainer.GetAll(serviceType);
        }
    }
}