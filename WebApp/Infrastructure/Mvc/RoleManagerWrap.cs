﻿using DomainLogic.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WebApp;
using WebApp.Models;

namespace WebApp.Infrastructure.Mvc
{
    /// <summary>
    /// Содержит Работа с ресурсами для локализации
    /// </summary>
    public class RoleManagerWrap : IRoleManagerWrap
    {
        /// <summary>
        /// Получает все роли
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllRoles()
        {
            RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
            return roleManager.Roles.Select(x => x.Name).ToList();
        }

        /// <summary>
        /// Получает роли для пользователя
        /// </summary>
        /// <param name="userEmail"></param>
        /// <returns></returns>
        public async Task<List<string>> GetRolesForUserAsync(string userEmail)
        {
            ApplicationUserManager userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            ApplicationUser user = await userManager.FindByEmailAsync(userEmail);
            if (user == null)
                return null;
            return (await userManager.GetRolesAsync(user.Id)).ToList();
        }

        /// <summary>
        /// Добавляет роль пользователю
        /// </summary>
        /// <param name="userEmail"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        public async Task AddRoleForUserAsync(string userEmail, string role)
        {
            ApplicationUserManager userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            ApplicationUser user = await userManager.FindByEmailAsync(userEmail);
            if (user == null)
                return;
            if (await userManager.IsInRoleAsync(user.Id, role))
                return;
            await userManager.AddToRoleAsync(user.Id, role);
        }

        /// <summary>
        /// Удаляет роль у пользователя
        /// </summary>
        /// <param name="userEmail"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        public async Task RemoveRoleFromUserAsync(string userEmail, string role)
        {
            ApplicationUserManager userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            ApplicationUser user = await userManager.FindByEmailAsync(userEmail);
            if (user == null)
                return;
            if (!(await userManager.IsInRoleAsync(user.Id, role)))
                return;
            await userManager.RemoveFromRoleAsync(user.Id, role);
        }

        /// <summary>
        /// Проверяет есть указанная роль у пользователя
        /// </summary>
        /// <param name="userEmail"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        public async Task<bool> IsInRoleAsync(string userEmail, string role)
        {
            ApplicationUserManager userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            ApplicationUser user = await userManager.FindByEmailAsync(userEmail);
            if (user == null)
                return false;
            return await userManager.IsInRoleAsync(user.Id, role);
        }

        /// <summary>
        /// Создает новую роль
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public async Task CreateRoleAsync(string roleName)
        {
            RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
            await roleManager.CreateAsync(new IdentityRole(roleName));
        }
    }
}