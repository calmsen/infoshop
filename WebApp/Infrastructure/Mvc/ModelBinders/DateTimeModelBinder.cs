﻿using System;
using System.Globalization;
using System.Web.Mvc;

namespace WebApp.Infrastructure.Mvc.ModelBinders
{
    /// <summary>
    /// Привязывает дату в зависимости от установленной культуры
    /// </summary>
    public class DateTimeModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {            
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (value == null)
                return base.BindModel(controllerContext, bindingContext);

            var displayFormat = bindingContext.ModelMetadata.DisplayFormatString; // for DateTypeName == "Date" default {0:d}, for DateTypeName == "Time" default {0:t}, for DateTypeName == "DateTime" default null

            if (!string.IsNullOrEmpty(displayFormat)) {
                displayFormat = displayFormat.Replace("{0:", string.Empty).Replace("}", string.Empty);
                // warning: if DateTimeFormat.DateSeparator was changed then will bad
                string dateSeparator = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.DateSeparator;
                DateTime date;
                if (DateTime.TryParseExact(value.AttemptedValue.Replace("/", dateSeparator).Replace("-", dateSeparator).Replace(".", dateSeparator), displayFormat, System.Threading.Thread.CurrentThread.CurrentCulture, DateTimeStyles.None, out date))
                    return date;
            }
            else // when DateTypeName == "DateTime" and displayFormat == null
            {
                DateTime date;
                if (DateTime.TryParse(value.AttemptedValue, System.Threading.Thread.CurrentThread.CurrentCulture, DateTimeStyles.None, out date))
                    return date;
            }
                

            bindingContext.ModelState.AddModelError(
                bindingContext.ModelName,
                string.Format("{0} is an invalid date format", value.AttemptedValue)
            );
            return base.BindModel(controllerContext, bindingContext);
        }
    }
}