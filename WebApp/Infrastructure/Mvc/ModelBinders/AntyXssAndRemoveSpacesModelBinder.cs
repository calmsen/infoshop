﻿using DomainLogic.Infrastructure;
using DomainLogic.Infrastructure.Attributes;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Infrastructure.Mvc.ModelBinders
{
    /// <summary>
    /// Удаляет опасные html теги и атрибуты из свойств, отмеченные  AllowHtmlAttribute. А так же удаляет пробельные символы слева и справа.
    /// http://stackoverflow.com/questions/13878967/how-to-check-for-a-property-attribute-inside-a-custom-model-binder
    /// http://www.bfcamara.com/post/48031562990/aspnet-mvc-default-model-binder-with-html
    /// </summary>
    public class AntyXssAndRemoveSpacesModelBinder : DefaultModelBinder
    {
        private HtmlSanitizer _htmlSanitizer;

        public AntyXssAndRemoveSpacesModelBinder(HtmlSanitizer htmlSanitizer)
        {
            _htmlSanitizer = htmlSanitizer;
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var holderType = bindingContext.ModelMetadata.ContainerType;
            if (holderType == null)
                return base.BindModel(controllerContext, bindingContext);
            
            var propertyType = holderType.GetProperty(bindingContext.ModelMetadata.PropertyName);
            var attributes = propertyType.GetCustomAttributes(true);
            bool hasAttribute = attributes
                .OfType<AllowHtmlAttribute>()
                .Any();

            var provider = bindingContext.ValueProvider;
            ValueProviderResult result;

            if (!hasAttribute)
            {
                result = provider.GetValue(bindingContext.ModelName);
            }
            else 
            {
                try
                {
                    result = provider.GetValue(bindingContext.ModelName);
                }
                catch (HttpRequestValidationException)
                {                                                    
                    result = ((IUnvalidatedValueProvider)provider).GetValue(bindingContext.ModelName, skipValidation: true);
                }
            }

            string value = result.AttemptedValue.Trim();
            if (string.IsNullOrEmpty(value))
                return null;
            value = _htmlSanitizer.RemoveInvalidHtmlTags(value).Trim();
            if (string.IsNullOrEmpty(value))
                return null;
            return value; 
        }
    }
}