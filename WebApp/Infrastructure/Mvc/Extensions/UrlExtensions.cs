﻿using System.Collections.Generic;
using System.Reflection;
using System.Web.Routing;
using WebApp.Infrastructure.Mvc.Utils;

namespace WebApp.Infrastructure.Mvc.Extensions
{
    public static class UrlExtensions
    {
        public static UrlUtils UrlUtils { get; set; }
        
        /// <summary>
        /// Расширяет словарь values новыми значениями из словарей extValues
        /// </summary>
        /// <param name="values">Словарь, который нужно расширить</param>
        /// <param name="extValues">Словари, которые будут расширять словарь values</param>
        /// <returns>Словарь values</returns>
        public static RouteValueDictionary Extend(this RouteValueDictionary values, params Dictionary<string, object>[] extValues)
        {
            return UrlUtils.Extend(values, extValues);
        }

        /// <summary>
        /// Преобразует словарь Dictionary в словарь RouteValueDictionary
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public static RouteValueDictionary ToRouteValueDictionary(this Dictionary<string, object> values)
        {
            return UrlUtils.ToRouteValueDictionary(values);
        }

        /// <summary>
        /// Преобразует объект source в словарь. 
        /// </summary>
        /// <param name="source">Объект, который будет преобразовываться</param>
        /// <param name="bindingAttr">Флаги, которые говарят какие свойства преобразовывать</param>
        /// <returns>Новый словарь</returns>
        public static RouteValueDictionary AsRouteValueDictionary(this object source, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
        {
            return UrlUtils.AsRouteValueDictionary(source, bindingAttr);
        }

        /// <summary>
        /// Преобразует объект source в словарь. 
        /// </summary>
        /// <param name="source">Объект, который будет преобразовываться</param>
        /// <param name="queryString">Коллекция значений для сверки на пустые значения</param>
        /// <param name="bindingAttr">Флаги, которые говарят какие свойства преобразовывать</param>
        /// <returns>Новый словарь</returns>
        public static RouteValueDictionary AsRouteValueDictionaryOfFormatedValues(this object source, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
        {
            return UrlUtils.AsRouteValueDictionaryOfFormatedValues(source, bindingAttr);
        }
    }
}