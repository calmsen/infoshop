﻿using AutoMapper;
using DomainLogic.Interfaces;
using WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using DomainLogic.Models;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Utils;
using WebApp.Models.Forms;
using DomainLogic.Infrastructure.Constants;
using WebApp.Models.Views;

namespace WebApp.Infrastructure
{
    /// <summary>
    /// Класс содержит конфигурацию преобразований для всех классов
    /// </summary>
    public class AutoMapperDefaultProfile : Profile
    {
        /// <summary>
        /// Контекст для хранения данных для рабочего потока
        /// </summary>
        [Inject]
        public ITLS TLS { get; set; }

        [Inject]
        public CommonUtils CommonUtils { get; set; }

        /// <summary>
        /// Конфигурирует преобразование объектов
        /// </summary>
        protected override void Configure()
        {
            ConfigureFormToDomain();
            ConfigureDomainToForm();
            ConfigureDomainToView();
        }

        /// <summary>
        /// Конфигурирует преобразование форм в домены
        /// </summary>
        private void ConfigureFormToDomain()
        {
            CreateMap<FeedbackForm, Feedback>()
                .ForMember(dest => dest.FeedbacksToImages, opt => opt.MapFrom(src => src.ImagesIds.Select(x => new FeedbackToImage { ImageId = x.Value }).ToList()));
            CreateMap<FeedbackAnswerForm, FeedbackAnswer>();
            CreateMap<SimpleFeedbackForm, Feedback>();
            CreateMap<FeedbackTagForm, FeedbackTag>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => CommonUtils.ToLat(src.Title)))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title.Trim()))
                .ForMember(dest => dest.FeedbacksToTags, opt => opt.MapFrom(src => new List<FeedbackToTag> { new FeedbackToTag { FeedbackId = src.FeedbackId } }));
            CreateMap<FeedbackImagesForm, Feedback>()
                .ForMember(dest => dest.FeedbacksToImages, opt => opt.MapFrom(src => src.ImagesIds.Select(x => new FeedbackToImage { ImageId = x.Value }).ToList()));

            CreateMap<AttributeSimpleForm, DmAttribute>();
            CreateMap<FilterSimpleForm, DmFilter>()
                .ForMember(dest => dest.Type, opt => opt.Ignore());
            CreateMap<ValueSimpleForm, ProductValue>();
            CreateMap<DescriptionForm, Description>();
            CreateMap<ProductForm, Product>()
                .ForMember(dest => dest.Type, opt => opt.Ignore())
                .ForMember(dest => dest.Section, opt => opt.MapFrom(src => new Section { Id = src.SectionId }))
                .ForMember(dest => dest.ProductsToImages, opt => opt.MapFrom(src => src.ImagesIds.Select(x => new ProductToImage { ImageId = x.Value }).ToList()))
                .ForMember(dest => dest.BannerPosition, opt => opt.MapFrom(src => src.BannerPosition != null ? src.BannerPosition : Int32.MaxValue));
            CreateMap<ProductTypeForm, ProductType>();
            CreateMap<SectionForm, Section>()
                .ForMember(dest => dest.SectionsToImages, opt => opt.MapFrom(src => src.BannerIds.Select(x => new SectionToImage { ImageId = x.Value }).ToList()));

            CreateMap<FilterValueForm, FilterValue>()
                .ForMember(dest => dest.ValueAsLong, opt => opt.MapFrom(src => Math.Floor(src.ValueAsDouble)));
            CreateMap<FilterForm, DmFilter>();

            CreateMap<FileForm, DmFile>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => CommonUtils.ToLat(src.Title)));

            CreateMap<FaqForm, Faq>()
                .ForMember(dest => dest.SectionId, opt => opt.MapFrom(src => src.SectionId != 0 ? src.SectionId : null))
                .ForMember(dest => dest.ProductsToFaqs, opt => opt.MapFrom(src => src.ProductIds.Select(x => new ProductToFaq { ProductId = x.Value }).ToList()));

            CreateMap<ArticleDescriptionForm, ArticleDescription>();
            CreateMap<ArticleForm, Article>()
                .ForMember(dest => dest.NewsToImages, opt => opt.MapFrom(src => src.ImagesIds.Select(x => new ArticleToImage { ImageId = x.Value }).ToList()))
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position != null ? src.Position : Int32.MaxValue))
                .ForMember(dest => dest.ProductsToNews, opt => opt.MapFrom(src => src.ProductIds.Select(x => new ProductToArticle { ProductId = x.Value }).ToList()));

            CreateMap<PostDescriptionForm, PostDescription>();
            CreateMap<PostForm, Post>()
                .ForMember(dest => dest.ProductsToPosts, opt => opt.Ignore())
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => CommonUtils.ToLat(src.Title)));

            CreateMap<PageDescriptionForm, PageDescription>();
            CreateMap<PageForm, Page>();

            CreateMap<PartnerForm, Partner>()
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position != null ? src.Position : Int32.MaxValue));

            CreateMap<ServiceForm, Service>();

            CreateMap<VacancyForm, Vacancy>();

            CreateMap<ResumeForm, Resume>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => CommonUtils.ToLat(src.Title)));

            CreateMap<SupportArticleDescriptionForm, SupportArticleDescription>();
            CreateMap<SupportArticleForm, SupportArticle>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => CommonUtils.ToLat(src.Title)));
        }

        /// <summary>
        /// Конфигурирует преобразование доменов в формы
        /// </summary>
        private void ConfigureDomainToForm()
        {
            CreateMap<Feedback, FeedbackImagesForm>()
                .ForMember(dest => dest.ImagesIds, opt => opt.MapFrom(src => src.FeedbacksToImages.Select(x => new ImageId { Value = x.ImageId }).ToList()));

            CreateMap<DmAttribute, AttributeSimpleForm>();
            CreateMap<DmFilter, FilterSimpleForm>();
            CreateMap<ProductValue, ValueSimpleForm>();
            CreateMap<Description, DescriptionForm>();
            CreateMap<Product, ProductForm>()
                .ForMember(dest => dest.ImagesIds, opt => opt.MapFrom(src => src.ProductsToImages.Select(x => new ImageId { Value = x.ImageId }).ToList()))
                .ForMember(dest => dest.BannerPosition, opt => opt.MapFrom(src => src.BannerPosition != 0 ? (int?)src.BannerPosition : null));
            CreateMap<ProductType, ProductTypeForm>();
            CreateMap<Section, SectionForm>()
                .ForMember(dest => dest.BannerIds, opt => opt.MapFrom(src => src.SectionsToImages.Select(x => new ImageId { Value = x.ImageId }).ToList()));

            CreateMap<FilterValueInline, FilterValueForm>();
            CreateMap<FilterValue, FilterValueForm>();
            CreateMap<DmFilter, FilterForm>();

            CreateMap<DmFile, FileForm>();

            CreateMap<Faq, FaqForm>()
                .ForMember(dest => dest.SectionId, opt => opt.MapFrom(src => src.SectionId == 0 ? null : src.SectionId))
                .ForMember(dest => dest.ProductIds, opt => opt.MapFrom(src => src.ProductsToFaqs));
            CreateMap<Product, ProductId>()
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id));
            CreateMap<FeedbackAnswer, FaqForm>()
                .ForMember(dest => dest.Answer, opt => opt.MapFrom(src => src.Message))
                .ForMember(dest => dest.Question, opt => opt.MapFrom(src => src.Feedback.Message))
                .ForMember(dest => dest.SectionId, opt => opt.MapFrom(src => src.Feedback.SectionId))
                .ForMember(dest => dest.ProductIds, opt => opt.MapFrom(src => MapProductIdsForFaqForm(src.Feedback.ProductId)))
                .ForMember(dest => dest.Theme, opt => opt.MapFrom(src => src.Feedback.Theme));

            CreateMap<ArticleDescription, ArticleDescriptionForm>();
            CreateMap<Article, ArticleForm>()
                .ForMember(dest => dest.ImagesIds, opt => opt.MapFrom(src => src.NewsToImages.Select(x => new ImageId { Value = x.ImageId }).ToList()))
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position != 0 ? (int?)src.Position : null))
                .ForMember(dest => dest.ProductIds, opt => opt.MapFrom(src => src.ProductsToNews.Select(x => x.ProductId).ToList()));

            CreateMap<PostDescription, PostDescriptionForm>();
            CreateMap<Post, PostForm>()
                .ForMember(dest => dest.ProductIds, opt => opt.MapFrom(src => src.ProductsToPosts.Select(x => x.ProductId).ToList()));

            CreateMap<PageDescription, PageDescriptionForm>();
            CreateMap<Page, PageForm>();

            CreateMap<Partner, PartnerForm>()
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position != 0 ? (int?)src.Position : null));

            CreateMap<Service, ServiceForm>();

            CreateMap<Vacancy, VacancyForm>();

            CreateMap<Resume, ResumeForm>();

            CreateMap<SupportArticleDescription, SupportArticleDescriptionForm>();
            CreateMap<SupportArticle, SupportArticleForm>();

            CreateMap<DescriptionForSnapshot, DescriptionForm>();
            CreateMap<AttributeForSnapshot, AttributeSimpleForm>()
                .AfterMap((src, dest) => dest.Filter = TLS.Get<List<FilterSimpleForm>>(TLSKeys.SnapshotFilterSimpleFormsForMapper).FirstOrDefault(x => x.Id == src.FilterId));
            CreateMap<ValueForSnapshot, ValueSimpleForm>();
            CreateMap<ProductInfoForSnapshot, ProductForm>()
                .ForMember(dest => dest.ImagesIds, opt => opt.MapFrom(src => src.ImageIds.Select(x => new ImageId { Value = x }).ToList()));
        }

        /// <summary>
        /// Конфигурирует преобразование домены во вьюмодели
        /// </summary>
        private void ConfigureDomainToView()
        {
            CreateMap<Description, DescriptionView>();
            CreateMap<DmAttribute, AttributeView>();
            CreateMap<ProductValue, ValueView>();
            CreateMap<DmFilter, FilterView>();
            CreateMap<FilterValue, FilterValueView>();
            CreateMap<FilterGroup, FilterGroupView>();
            CreateMap<FilterSettings, FilterSettingsView>();
            CreateMap<FilterValueSettings, FilterValueSettingsView>();
            CreateMap<UnitConverter, UnitConverterView>();
            CreateMap<Product, ProductView>()
                .ForMember(dest => dest.Images, opt => opt.MapFrom(src => src.ProductsToImages.Select(x => Mapper.Map<Image, ImageView>(x.Image)).ToList()));
            CreateMap<ProductSnapshot, ProductSnapshotView>();
            CreateMap<Image, ImageView>();
            CreateMap<DmFile, FileView>();
            CreateMap<Faq, FaqView>();
            CreateMap<Section, SectionView>();
            CreateMap<SectionImage, SectionImageView>();
            CreateMap<Vacancy, VacancyView>();
            CreateMap<Service, ServiceView>();
            CreateMap<Article, ArticleView>();
            CreateMap<ArticleImage, ArticleImageView>();
            CreateMap<ArticleDescription, ArticleDescriptionView>();
            CreateMap<Post, PostView>();
            CreateMap<PostImage, PostImageView>();
            CreateMap<PostDescription, PostDescriptionView>();
            CreateMap<Page, PageView>();
            CreateMap<PageDescription, PageDescriptionView>();
            CreateMap<Partner, PartnerView>();
            CreateMap<PartnerImage, PartnerImageView>();
            CreateMap<SupportArticleDescription, SupportArticleDescriptionView>();
            CreateMap<SupportArticle, SupportArticleView>();
            CreateMap<Feedback, FeedbackView>();
            CreateMap<FeedbackTag, FeedbackTagView>();
            CreateMap<FeedbackInviting, FeedbackInvitingView>();
            CreateMap<FeedbackImage, FeedbackImageView>();
            CreateMap<FeedbackAnswer, FeedbackAnswerView>();
            CreateMap<User, UserView>();
            CreateMap<RequestCounter, RequestCounterView>();
        }

        /// <summary>
        /// Преобразует идентификатор (если не равен null) в список идентифкаторов
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        private List<ProductId> MapProductIdsForFaqForm(long? productId) 
        { 
            List<ProductId> productIds = new List<ProductId>();
            if (productId != null)
                productIds.Add(new ProductId { Value = (long)productId });
            return productIds;
        }
    }
}