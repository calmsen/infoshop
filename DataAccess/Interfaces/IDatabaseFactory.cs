﻿using System.Data.Entity;

namespace DataAccess.Interfaces
{
    /// <summary>
    /// Фабрика для создания БД
    /// </summary>
    internal interface IDatabaseFactory
    {
        /// <summary>
        /// Создает БД
        /// </summary>
        /// <returns></returns>
        DbContext CreateDatabase();
    }
}
