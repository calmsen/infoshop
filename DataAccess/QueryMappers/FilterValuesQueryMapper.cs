﻿using DomainLogic.Interfaces.QueryBuilder;
using DomainLogic.Interfaces.QueryMappers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccess.QueryMappers
{
    /// <summary>
    /// Класс для работы с сущностью FilterValue. Методы содержат сырые запросы к БД
    /// </summary>
    internal class FilterValuesQueryMapper : IFilterValuesQueryMapper
    {
        /// <summary>
        /// Фабрика для создания QueryBuilder
        /// </summary>
        private IQueryBuilderFactory _queryBuilderFactory;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="queryBuilderFactory"></param>
        public FilterValuesQueryMapper(IQueryBuilderFactory queryBuilderFactory)
        {
            _queryBuilderFactory = queryBuilderFactory;
        }

        /// <summary>
        /// Удаляет значения фильтра
        /// </summary>
        /// <param name="filterId"></param>
        /// <param name="valuesIds"></param>
        /// <returns></returns>
        public async Task DeleteFilterValues(long filterId, string valuesIds)
        {
            await _queryBuilderFactory.Create().ExecuteAsync(
                    $"delete from FilterValues where FilterId = {filterId} and Id not in ({valuesIds});"
                );
        }

        /// <summary>
        /// Объединяет значения valueId c mainValueId в определенных разделах
        /// </summary>
        /// <param name="valueId"></param>
        /// <param name="mainValueId"></param>
        /// <param name="sectionIds"></param>
        public async Task MergeBindedValuesAsync(long valueId, long mainValueId, List<long> sectionIds)
        {
            string joinWithSections = string.Empty;
            string whereBySections = string.Empty;
            if (sectionIds != null && sectionIds.Count > 0)
            {
                joinWithSections = " join Attributes a on a.Id = v.AttributeId join Products p on p.Id = a.ProductId";
                whereBySections = string.Format(" and  p.SectionId in ({0})", string.Join(", ", sectionIds));
            }
            // все привязанные значения valueId обновляем в mainValueId
            await _queryBuilderFactory.Create().ExecuteAsync(string.Format(
                                                @"update v set v.ValueId = {0} 
                                                from ProductValues v 
                                                    {1}
                                                where v.ValueId = {2} {3};",
                                                mainValueId,
                                                joinWithSections,
                                                valueId,
                                                whereBySections
            ));
            // удалим дублирующие значения
            await _queryBuilderFactory.Create().ExecuteAsync(string.Format(@"with pvIds as (
                                                    select max(v.Id) pvId 
                                                    from ProductValues v 
                                                        {0}
                                                    where v.ValueId = {1} {2}
                                                    group by v.AttributeId
                                                    having count(*) > 1 
                                                )
                                                delete from ProductValues where Id in (select pvId from pvIds) ",
                                                joinWithSections,
                                                mainValueId,
                                                whereBySections
            ));
        }

        /// <summary>
        /// Удаляет значения valueId в определенных разделах
        /// </summary>
        /// <param name="valueId"></param>
        /// <param name="sectionIds"></param>
        public async Task DeleteBindedValuesAsync(long valueId, List<long> sectionIds)
        {
            string joinWithSections = string.Empty;
            string whereBySections = string.Empty;
            if (sectionIds != null && sectionIds.Count > 0)
            {
                joinWithSections = " join Attributes a on a.Id = v.AttributeId join Products p on p.Id = a.ProductId";
                whereBySections = string.Format(" and  p.SectionId in ({0})", string.Join(", ", sectionIds));
            }
            // удаляем все привязанные значения valueId
            await _queryBuilderFactory.Create().ExecuteAsync(string.Format(
                                                @"delete from v 
                                                from ProductValues v 
                                                    {0}
                                                where v.ValueId = {1} {2};",
                                                joinWithSections,
                                                valueId,
                                                whereBySections
            ));
        }

        /// <summary>
        /// Копируем все привязанные значения valueId
        /// </summary>
        /// <param name="valueId"></param>
        /// <param name="sectionIds"></param>
        /// <param name="filterValueCloneId"></param>
        /// <returns></returns>
        public async Task CopyBindedValuesAsync(long valueId, List<long> sectionIds, long filterValueCloneId)
        {
            string joinWithSections = " join Attributes a on a.Id = v.AttributeId join Products p on p.Id = a.ProductId";
            string whereBySections = string.Format(" and  p.SectionId in ({0})", string.Join(", ", sectionIds));
            
            await _queryBuilderFactory.Create().ExecuteAsync(string.Format(
                                                @"with attrIds as (
                                                    select v.AttributeId 
                                                    from ProductValues v  
                                                        {0}
                                                    where v.ValueId = {1} {2}
                                                )
                                                insert into ProductValues (AttributeId, ValueId)
                                                select AttributeId, {3} ValueId from attrIds ",
                                                joinWithSections,
                                                valueId,
                                                whereBySections,
                                                filterValueCloneId
            ));
        }
        
    }
}
