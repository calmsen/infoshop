﻿using DomainLogic.Interfaces.QueryBuilder;
using DomainLogic.Interfaces.QueryMappers;
using DomainLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccess.QueryMappers
{
    /// <summary>
    /// Класс для работы с сущностью ProductSnapshot. Методы содержат сырые запросы к БД
    /// </summary>
    internal class ProductSnapshotsQueryMapper : IProductSnapshotsQueryMapper
    {
        /// <summary>
        /// Фабрика для создания QueryBuilder
        /// </summary>
        private IQueryBuilderFactory _queryBuilderFactory;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="queryBuilderFactory"></param>
        public ProductSnapshotsQueryMapper(IQueryBuilderFactory queryBuilderFactory)
        {
            _queryBuilderFactory = queryBuilderFactory;
        }

        /// <summary>
        /// Получает последние снимки по списку идентификаторов товаров
        /// </summary>
        /// <param name="productIds"></param>
        /// <returns></returns>
        public async Task<List<ProductSnapshotInline>> GetLastProductSnapshotsAsync(List<long> productIds)
        {
            if (productIds == null || productIds.Count == 0)
                return new List<ProductSnapshotInline>();
            string sql = string.Format(
                    @"select * 
                    from (select ps.*, row_number() 
                        over (
                            partition by ps.ProductId
                            order by ps.CreatedDate 
                        ) AS RowNumber 
                        from ProductSnapshots ps 
	                    where ps.ProductId in ({0})) t
                    where RowNumber <= 1",
                    string.Join(", ", productIds)
            );

            return await _queryBuilderFactory.Create().ToTargetListAsync<ProductSnapshotInline>(sql);
        }
    }
}
