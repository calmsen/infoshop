﻿using DomainLogic.Interfaces;
using DomainLogic.Interfaces.QueryBuilder;
using DomainLogic.Interfaces.QueryMappers;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccess.QueryMappers
{
    /// <summary>
    /// Класс для работы с сущностью Section. Методы содержат сырые запросы к БД
    /// </summary>
    internal class SectionsQueryMapper : ISectionsQueryMapper
    {
        /// <summary>
        /// Фабрика для создания QueryBuilder
        /// </summary>
        private IQueryBuilderFactory _queryBuilderFactory;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="queryBuilderFactory"></param>
        public SectionsQueryMapper(IQueryBuilderFactory queryBuilderFactory)
        {
            _queryBuilderFactory = queryBuilderFactory;
        }

        /// <summary>
        /// Получает всех потомков и себя
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<List<long>> GetDescendantOrSelfSectionsAsync(long sectionId)
        {
            if (sectionId == 0)
                throw new ArgumentException("sectionId не может быть равен 0.");

            return await _queryBuilderFactory.Create().ToTargetListAsync<long>(
                @"with Tree as (
                    select Id, ParentId
                    from Sections
                    where Id = @p0
                    union all
                    select c.Id, c.ParentId 
                    from Sections c
                        join Tree p on p.Id = c.ParentId 
                )
                select Id	
                from Tree;",
                sectionId
            );
        }


        /// <summary>
        /// Получает полный путь из разделам относительно родителя
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public async Task<long[]> GetPathOfSectionsByParentIdAsync(long parentId)
        {
            // получим позиции ancestor-or-self разделов относительно своих соседей
            string subQuery = string.Format("select isnull((select top 1 RowNumber + 1 from S where ParentId = {0} order by Position desc, Id desc), 1) as RowNumber", parentId);

            string sql = string.Format(
                @"with S as (
	                select *, (row_number() 
                    over (
                        partition by ParentId
                        order by ParentId, Position, Id
                    )) as RowNumber 
                    from Sections
                ), Tree as (
                    select Id, ParentId, RowNumber
                    from S
                    where Id = {0} 
                    union all
                    select c.Id, c.ParentId, c.RowNumber
                    from S c
                        join Tree p on p.ParentId = c.Id 
                )
                {1}
                union all
                select RowNumber	
                from Tree;",
                parentId,
                subQuery
            );
            // -- -- --
            List<long> positions = await _queryBuilderFactory.Create().ToTargetListAsync<long>(sql);
            // 
            return new long[] { PositionsToPathOfSections(positions), positions[0] };
        }
        
        /// <summary>
        /// Получает полный путь по разделам относительно своих соседей
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<long[]> GetPathOfSectionsAsync(long sectionId)
        {
            if (sectionId == 0)
            {
                throw new ArgumentException("sectionId дожен быть больше 0.");
            }
            // получим позиции ancestor-or-self разделов относительно своих соседей
            List<long> positions = await _queryBuilderFactory.Create().ToTargetListAsync<long>(
                    @"with S as (
	                    select *, (row_number() 
                        over (
                            partition by ParentId
                            order by ParentId, Position, Id
                        )) as RowNumber 
                        from Sections
                    ), Tree as (
                        select Id, ParentId, RowNumber
                        from S
                        where Id = @p0 
                        union all
                        select c.Id, c.ParentId, c.RowNumber
                        from S c
                            join Tree p on p.ParentId = c.Id 
                    )
                    select RowNumber	
                    from Tree;",
                    sectionId
            );
            // 
            return new long[] { PositionsToPathOfSections(positions), positions[0] };
        }
        
        /// <summary>
        /// Получает заголовки разделов для указанных фильтров
        /// </summary>
        /// <param name="filterIds"></param>
        /// <returns></returns>
        public async Task<List<SectionTitle>> GetSectionTitlesAsync(List<long> filterIds)
        {
            if (filterIds == null || filterIds.Count == 0)
                return new List<SectionTitle>();
            return await _queryBuilderFactory.Create().ToTargetListAsync<SectionTitle>(string.Format(
                    @"select distinct s.Id, s.Title, a.FilterId 
                    from Sections s 
                        join Products p on s.Id = p.SectionId 
                        join Attributes a on p.Id = a.ProductId 
                    where a.FilterId in ({0});",
                    string.Join(",", filterIds)
                ));
        }

        /// <summary>
        /// Преобразует позиции в путь из разделов
        /// </summary>
        /// <param name="positions"></param>
        /// <returns></returns>
        private long PositionsToPathOfSections(List<long> positions)
        {
            byte[] positionsAsArrayOfByte = new byte[8];
            int n = 8 - positions.Count;
            for (int i = 0; i < 8; i++)
            {
                if (i < n)
                {
                    positionsAsArrayOfByte[i] = 0;
                }
                else
                {
                    if (positions[i - n] > 255)
                        throw new Exception("Позиция раздела не может быть больше значения, чем 255.");

                    positionsAsArrayOfByte[i] = (byte)positions[i - n];
                }
            }

            return BitConverter.ToInt64(positionsAsArrayOfByte, 0);
        }
    }
}
