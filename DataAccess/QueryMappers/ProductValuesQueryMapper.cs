﻿using DomainLogic.Interfaces.QueryBuilder;
using DomainLogic.Interfaces.QueryMappers;
using System.Threading.Tasks;

namespace DataAccess.QueryMappers
{
    /// <summary>
    /// Класс для работы с сущностью ProductValue. Методы содержат сырые запросы к БД
    /// </summary>
    internal class ProductValuesQueryMapper : IProductValuesQueryMapper
    {
        /// <summary>
        /// Фабрика для создания QueryBuilder
        /// </summary>
        private IQueryBuilderFactory _queryBuilderFactory;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="queryBuilderFactory"></param>
        public ProductValuesQueryMapper(IQueryBuilderFactory queryBuilderFactory)
        {
            _queryBuilderFactory = queryBuilderFactory;
        }

        /// <summary>
        /// Получает количество значений для указанного фильтра
        /// </summary>
        /// <param name="filterId"></param>
        /// <returns></returns>
        public async Task<int> GetProductValuesCount(long filterId)
        {
            return await _queryBuilderFactory.Create().ExecuteAsync(
                    @"select count(*) c 
                        from (select COUNT(a.Id) c 
                                from Attributes a 
                                    join ProductValues v on  v.AttributeId = a.Id
                                where a.FilterId = @p0 
                                group by a.Id
                                having count(a.Id) > 1) t",
                        filterId
                );
        }

        /// <summary>
        /// Удаляет значения у товаров для указанного фильтра за исключением указаных значений
        /// </summary>
        /// <param name="filterId"></param>
        /// <param name="excludeValuesIds"></param>
        /// <returns></returns>
        public async Task DeleteProductValues(long filterId, string excludeValuesIds)
        {
            await _queryBuilderFactory.Create().ExecuteAsync(
                    $@"delete v from ProductValues v 
                    inner join Attributes a on v.AttributeId = a.Id 
                    where a.FilterId = {filterId} and v.ValueId not in  ({excludeValuesIds});"
                );
        }
    }
}
