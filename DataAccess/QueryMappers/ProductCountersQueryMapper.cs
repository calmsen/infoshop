﻿using DomainLogic.Interfaces.QueryBuilder;
using DomainLogic.Interfaces.QueryMappers;
using System.Threading.Tasks;

namespace DataAccess.QueryMappers
{
    /// <summary>
    /// Класс для работы с сущностью ProductCounter. Методы содержат сырые запросы к БД
    /// </summary>
    internal class ProductCountersQueryMapper : IProductCountersQueryMapper
    {
        /// <summary>
        /// Фабрика для создания QueryBuilder
        /// </summary>
        private IQueryBuilderFactory _queryBuilderFactory;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="queryBuilderFactory"></param>
        public ProductCountersQueryMapper(IQueryBuilderFactory queryBuilderFactory)
        {
            _queryBuilderFactory = queryBuilderFactory;
        }

        /// <summary>
        /// Обновляет поле "Количество просмотров" у товаров
        /// </summary>
        public async Task UpdateProductCountersAsync()
        {
            // обновляем количество просмотров в товарах
            await _queryBuilderFactory.Create().ExecuteAsync(@"
                with UpdatedCounters as (
                    select * from ProductCounters where State in (0, 1) 
                )
                update p set p.ViewsAmount = c.ViewsAmount from Products p join UpdatedCounters c on p.Id = c.ProductId 
            ");
            // меняем статус у счетчиков для которых обновили товары выше
            await _queryBuilderFactory.Create().ExecuteAsync("update ProductCounters set State = 2 where State in (0, 1);");
        }
    }
}
