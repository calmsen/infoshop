﻿using DomainLogic.Interfaces.QueryBuilder;
using DomainLogic.Interfaces.QueryMappers;
using System.Threading.Tasks;

namespace DataAccess.QueryMappers
{
    /// <summary>
    /// Класс для работы с сущностью ProductsToFile. Методы содержат сырые запросы к БД
    /// </summary>
    internal class ProductsToFilesQueryMapper : IProductsToFilesQueryMapper
    {
        /// <summary>
        /// Фабрика для создания QueryBuilder
        /// </summary>
        private IQueryBuilderFactory _queryBuilderFactory;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="queryBuilderFactory"></param>
        public ProductsToFilesQueryMapper(IQueryBuilderFactory queryBuilderFactory)
        {
            _queryBuilderFactory = queryBuilderFactory;
        }

        /// <summary>
        /// Добавляет файлы к товарам в указанном разделе
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task AddFilesWithSameSection(long fileId, long sectionId)
        {
            await _queryBuilderFactory.Create().ExecuteAsync(
                    @"insert into ProductsToFiles (ProductId, FileId) 
                    (select p.Id ProductId, @p0 FileId from Products p
	                    left join ProductsToFiles pd on p.Id = pd.ProductId and @p1 = pd.FileId
                    where p.SectionId = @p2 and pd.ProductId is null)",
                    fileId,
                    fileId,
                    sectionId
                );
        }

        /// <summary>
        /// Добавляет файлы к товарам c указанным значением фильтра
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="filterValueId"></param>
        /// <returns></returns>
        public async Task AddFilesWithSameFilterValue(long fileId, long filterValueId)
        {
            await _queryBuilderFactory.Create().ExecuteAsync(
                @"insert into ProductsToFiles (ProductId, FileId) 
                        (select p.Id ProductId, @p0 FileId from Products p 
	                        join Attributes a on p.Id = a.ProductId 
	                        join ProductValues v on a.Id = v.AttributeId 
	                        left join ProductsToFiles pd on p.Id = pd.ProductId and @p1 = pd.FileId 
                        where v.ValueId = @p2 and pd.ProductId is null)",
                fileId,
                fileId,
                filterValueId
            );
        }
    }
}
