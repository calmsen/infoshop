﻿using DomainLogic.Interfaces.QueryBuilder;
using DomainLogic.Interfaces.QueryMappers;
using System.Threading.Tasks;

namespace DataAccess.QueryMappers
{
    /// <summary>
    /// Класс для работы с сущностью Attribute. Методы содержат сырые запросы к БД
    /// </summary>
    internal class AttributesQueryMapper : IAttributesQueryMapper
    {
        /// <summary>
        /// Фабрика для создания QueryBuilder
        /// </summary>
        private IQueryBuilderFactory _queryBuilderFactory;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="queryBuilderFactory"></param>
        public AttributesQueryMapper(IQueryBuilderFactory queryBuilderFactory)
        {
            _queryBuilderFactory = queryBuilderFactory;
        }

        /// <summary>
        /// Удаляет атрибуты по идентификатору фильтра
        /// </summary>
        /// <param name="filterId"></param>
        /// <returns></returns>
        public async Task DeleteAttributes(long filterId)
        {
            await _queryBuilderFactory.Create().ExecuteAsync(
                    "delete a from Attributes a left join ProductValues v on v.AttributeId = a.Id where a.FilterId = @p0 and v.ValueId is null;",
                    filterId
                );
        }
    }
}
