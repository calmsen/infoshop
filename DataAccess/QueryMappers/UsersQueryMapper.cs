﻿using DomainLogic.Interfaces.QueryBuilder;
using DomainLogic.Interfaces.QueryMappers;
using DomainLogic.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.QueryMappers
{
    /// <summary>
    /// Класс для работы с сущностью User. Методы содержат сырые запросы к БД
    /// </summary>
    internal class UsersQueryMapper : IUsersQueryMapper
    {
        private IQueryBuilderFactory _queryBuilderFactory;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="queryBuilderFactory"></param>
        public UsersQueryMapper(IQueryBuilderFactory queryBuilderFactory)
        {
            _queryBuilderFactory = queryBuilderFactory;
        }

        /// <summary>
        /// Получает список имен пользователей, имеющие роли на сайте
        /// </summary>
        /// <returns></returns>
        public async Task<List<string>> GetUserNamesWithRolesAsync()
        {
            return (await _queryBuilderFactory.Create().ToTargetListAsync<User>(
                @"select * 
                from Users u 
                    join AspNetUserRoles ur on u.Id = ur.UserId 
                where u.Email like '%@InfoShop.club'"
            ))
            .Select(x => x.Email.Substring(0, x.Email.IndexOf("@")))
            .ToList();
        }

        /// <summary>
        /// Удаляет ненужные роли у пользователя
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public async Task RemoveUnnecessaryUserRoleAsync(string userName)
        {
            await _queryBuilderFactory.Create().ExecuteAsync(
                @"delete ur from AspNetUserRoles ur join Users u on u.Id = ur.UserId 
                    where u.Email = '@p0'",
                userName + "@InfoShop.club"
            );
        }
    }
}
