﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Interfaces;
using DomainLogic.Interfaces.QueryBuilder;
using DomainLogic.Interfaces.QueryMappers;
using DomainLogic.Models;
using DomainLogic.Models.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DataAccess.QueryMappers
{
    /// <summary>
    /// Класс для работы с сущностью Product. Методы содержат сырые запросы к БД
    /// </summary>
    internal class ProductsQueryMapper : IProductsQueryMapper
    {
        /// <summary>
        /// Работа с разделами
        /// </summary>
        [Inject]
        public SectionsUtils SectionsUtils { get; set; }

        /// <summary>
        /// Работа со строками
        /// </summary>
        [Inject]
        public StringUtils StringUtils { get; set; }

        /// <summary>
        /// Фабрика для создания QueryBuilder
        /// </summary>
        private IQueryBuilderFactory _queryBuilderFactory;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="queryBuilderFactory"></param>
        public ProductsQueryMapper(IQueryBuilderFactory queryBuilderFactory)
        {
            _queryBuilderFactory = queryBuilderFactory;
        }
        
        /// <summary>
        /// Получает список товаров по указанным фильтрам
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="sectionId"></param>
        /// <param name="fParams"></param>
        /// <param name="sorts"></param>
        /// <param name="isArchive"></param>
        /// <param name="price"></param>
        /// <returns></returns>
        public async Task<List<Product>> GetProductsAsync(Pagination pagination, long sectionId, List<FilterParam> fParams, List<Sort> sorts, int selectType, bool isArchive, double[] price, long filterIdByGroupType, long pathOfSections, List<FilterValue> values)
        {
            string[] queryWhereResult = QueryWhere(fParams, values);
            

            string[] querySortResult = QuerySort(sorts, filterIdByGroupType);

            string sql = QueryCount(sectionId > 0 ? pathOfSections : 0, isArchive, price, queryWhereResult);

            if (selectType == 0)
            {
                pagination.ItemsAmount = (await _queryBuilderFactory.Create().ToTargetListAsync<int>(sql))
                    .DefaultIfEmpty(0).FirstOrDefault();
                //_logger.Info("sql pagination: " + sql);
                if (pagination.ItemsAmount == 0)
                    return new List<Product>();
                pagination.Refresh();
            }
            sql = QuerySelect(sectionId > 0 ? pathOfSections : 0, isArchive, price, querySortResult, queryWhereResult, pagination, selectType);

            return await _queryBuilderFactory.Create().ToTargetListAsync<Product>(sql);
        }
        
        /// <summary>
        /// Поиск товаров по строке запроса для определенных разделов
        /// </summary>
        /// <param name="q"></param>
        /// <param name="pagination"></param>
        /// <returns></returns>
        public async Task<List<Product>> SearchProductsAsync(string q, Pagination pagination)
        {
            if (string.IsNullOrWhiteSpace(q))
                return new List<Product>();

            string where = BuildWhereForTitle(q);

            pagination.ItemsAmount = (await _queryBuilderFactory.Create().ToTargetListAsync<int>(string.Format(
                @"select count(distinct p.Id) 
                from Products p 
                    join Sections s on s.Id = p.SectionId 
                where Published = 1 and Active = 1 and p.Archive = 0 and s.Hidden = 0 and {0};",
                where
            ))).DefaultIfEmpty(0).FirstOrDefault();
            if (pagination.ItemsAmount == 0)
                return new List<Product>();
            pagination.Refresh();
            return await _queryBuilderFactory.Create().ToTargetListAsync<Product>(string.Format(
                @"select * 
                from (
                    select p.*, row_number() over (order by p.Id) RowNumber 
                    from (
                        select distinct p.* from Products p 
                            join Sections s on s.Id = p.SectionId 
                        where Published = 1 and Active = 1 and p.Archive = 0 and s.Hidden = 0 and {0}
                    ) as p
                ) as p
                where RowNumber > {1} and RowNumber <= {2};",
                where,
                pagination.Offset,
                pagination.Offset + pagination.PageItemsAmount
                ));
        }

        /// <summary>
        /// Получает номер страницы в которой находится данный товар
        /// </summary>
        /// <returns></returns>
        public async Task<int> GetPageForProductAsync(long productId, long sectionId)
        {
            int rowNumber = await _queryBuilderFactory.Create().ToTargetItemAsync<int>(
                @"select cast(p.RowNumber as int) RowNumber 
                from ( 
                    select Id, row_number() over (order by Id) RowNumber 
                    from Products 
                    where SectionId = @p0
                ) p 
                where p.Id = @p1;",
                sectionId,
                productId
            );
            int page = (int)Math.Ceiling((double)rowNumber / 50);
            return page;
        }

        /// <summary>
        /// Строит расширенное условие для поиска по заголовку. Запросы разбивается на слова и учитываются все совпадения. 
        /// Так же делает расчет на ввод знака запятой или точки
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        private string BuildWhereForTitle(string q)
        {
            string where = string.Empty;
            string[] keywords = new Regex("\\s+").Replace(q, " ").Trim().Split(' ');
            foreach (string keyword in keywords)
            {
                if (!string.IsNullOrEmpty(where))
                    where += " and ";
                string subWhere = string.Format("p.Title like '%{0}%'", StringUtils.SqlEscape(keyword));
                if (keyword.IndexOf(",") != -1)
                {
                    subWhere += string.Format(" or p.Title like '%{0}%'", StringUtils.SqlEscape(keyword.Replace(',', '.')));
                }
                else if (keyword.IndexOf(".") != -1)
                {
                    subWhere += string.Format(" or p.Title like '%{0}%'", StringUtils.SqlEscape(keyword.Replace('.', ',')));
                }
                if (new Regex(@"^\d+$").IsMatch(keyword))
                {
                    subWhere += " or p.Id = " + keyword;
                }
                where += string.Format("({0})", subWhere);
            }
            return where;
        }

        /// <summary>
        /// Получает наборы значений по fParams. Наборы необходимы для поиска множества указанных значений у одного товара.
        /// </summary>
        /// <param name="fParams"></param>
        /// <returns></returns>
        private Dictionary<long, long[]> GetValueSetsForFilters(List<FilterParam> fParams, List<FilterValue> values)
        {
            // получим карту (ид фильтра - наборы сумм значений)
            Dictionary<long, long[]> d = new Dictionary<long, long[]>();
            foreach (FilterParam fp in fParams)
            {
                if (!d.ContainsKey(fp.Id))
                    d.Add(fp.Id, new long[4]);
                d[fp.Id][0] = values.Where(x => x.Position < 64 && fp.Values.Contains(x.Id)).Select(x => x.ValueSet1 ?? 0).Sum();
                d[fp.Id][1] = values.Where(x => x.Position < 128 && fp.Values.Contains(x.Id)).Select(x => x.ValueSet2 ?? 0).Sum();
                d[fp.Id][2] = values.Where(x => x.Position < 192 && fp.Values.Contains(x.Id)).Select(x => x.ValueSet3 ?? 0).Sum();
                d[fp.Id][3] = values.Where(x => x.Position < 256 && fp.Values.Contains(x.Id)).Select(x => x.ValueSet4 ?? 0).Sum();
            }
            return d;
        }

        /// <summary>
        /// Получает правый операнд для условия where
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        private string GetRightOperand(List<long> values)
        {
            if (values.Count == 0)
                throw new ArgumentException();

            if (values.Count == 1)
                return " = " + values[0];

            return string.Format(" in ({0})", String.Join(", ", values));
        }

        /// <summary>
        /// Строит условия для sql запроса
        /// </summary>
        /// <param name="fParams"></param>
        /// <param name="selectForWhere"></param>
        /// <param name="selectForWhere2"></param>
        /// <param name="where"></param>
        private string[] QueryWhere(List<FilterParam> fParams, List<FilterValue> values)
        {

            if (fParams == null || fParams.Count == 0)
                return new string[] { string.Empty, string.Empty, string.Empty };

            string selectForWhere = string.Empty;
            string selectForWhere2 = string.Empty;
            string where = string.Empty;

            Dictionary<long, long[]> fValueSets = GetValueSetsForFilters(fParams, values);

            foreach (FilterParam fp in fParams)
            {
                if (fp.Values.Count == 0)
                    continue;
                string v = "v.ValueId";
                if (fp.Type == (int)FilterTypeEnum.Range)
                    v = "fv.ValueAsLong";
                if (fp.Type != (int)FilterTypeEnum.Range && fp.Multiple)
                {
                    selectForWhere += string.Format(", (case when a.FilterId = {0} and fv.Position < 64 then fv.ValueSet1 else null end) as 'Value{1}_1'", fp.Id, fp.Id);
                    selectForWhere2 += string.Format(", sum(Value{0}_1) Value{1}_1", fp.Id, fp.Id);
                    selectForWhere += string.Format(", (case when a.FilterId = {0} and fv.Position < 128 then fv.ValueSet2 else null end) as 'Value{1}_2'", fp.Id, fp.Id);
                    selectForWhere2 += string.Format(", sum(Value{0}_2) Value{1}_2", fp.Id, fp.Id);
                    selectForWhere += string.Format(", (case when a.FilterId = {0} and fv.Position < 192 then fv.ValueSet3 else null end) as 'Value{1}_3'", fp.Id, fp.Id);
                    selectForWhere2 += string.Format(", sum(Value{0}_3) Value{1}_3", fp.Id, fp.Id);
                    selectForWhere += string.Format(", (case when a.FilterId = {0} and fv.Position < 256 then fv.ValueSet4 else null end) as 'Value{1}_4'", fp.Id, fp.Id);
                    selectForWhere2 += string.Format(", sum(Value{0}_4) Value{1}_4", fp.Id, fp.Id);
                }
                else if (fp.Type == (int)FilterTypeEnum.Range && fp.Multiple)
                {
                    selectForWhere += string.Format(", (case when a.FilterId = {0} then {1} else null end) as 'Value{2}'", fp.Id, v, fp.Id);
                    selectForWhere2 += string.Format(", avg(Value{0}) Value{1}", fp.Id, fp.Id);
                }
                else
                {
                    selectForWhere += string.Format(", (case when a.FilterId = {0} then {1} else null end) as 'Value{2}'", fp.Id, v, fp.Id);
                    selectForWhere2 += string.Format(", max(Value{0}) Value{1}", fp.Id, fp.Id);
                }

                if (!string.IsNullOrEmpty(where))
                    where += " and ";



                if (fp.Type == (int)FilterTypeEnum.Range)
                {
                    where += string.Format("v.Value{0} >= {1}", fp.Id, fp.ValuesK[0]);

                    if (fp.ValuesK[1] > 0) // TODO: передавать максимальное значение равное MaxValue
                        where += string.Format(" and v.Value{0} <= {1}", fp.Id, fp.ValuesK[1]);
                }
                else if (fp.Multiple)
                {
                    where += "(";
                    where += string.Format("v.Value{0}_1 & cast({1} as bigint) > 0", fp.Id, fValueSets[fp.Id][0]);
                    where += string.Format(" or v.Value{0}_2 & cast({1} as bigint) > 0", fp.Id, fValueSets[fp.Id][1]);
                    where += string.Format(" or v.Value{0}_3 & cast({1} as bigint) > 0", fp.Id, fValueSets[fp.Id][2]);
                    where += string.Format(" or v.Value{0}_4 & cast({1} as bigint)> 0", fp.Id, fValueSets[fp.Id][3]);
                    where += ")";
                }
                else
                {
                    where += "v.Value" + fp.Id + GetRightOperand(fp.Values);
                }
            }
            if (!string.IsNullOrEmpty(where))
                where = "where " + where;
            return new string[] { selectForWhere, selectForWhere2, where };
        }

        /// <summary>
        /// Строит сортировку для sql запроса
        /// </summary>
        /// <param name="sorts"></param>
        /// <param name="filterId"></param>
        /// <returns></returns>
        private string[] QuerySort(List<Sort> sorts, long filterId)
        {
            string selectForSort = string.Empty;
            string selectForSort2 = string.Empty;
            string sort = string.Empty;

            if (sorts == null || sorts.Count == 0)
            {
                sort = "p.Id asc";
                return new string[] { selectForSort, selectForSort2, sort };
            }
            foreach (Sort s in sorts)
            {
                if (s.Type.Equals(SortType.Filter))
                {
                    if (filterId <= 0)
                        continue;
                    selectForSort += string.Format(", (case when a.FilterId in ({0}) then fv.Title else null end) as 'Title{1}'", filterId, filterId);
                    //
                    if (!string.IsNullOrEmpty(sort))
                        sort += ",";
                    sort += string.Format("v.Title{0} {1}", filterId, s.Order.ToString().ToLower());
                    //
                    selectForSort2 += string.Format(", max(Title{0}) Title{1}", filterId, filterId);
                }
                else
                {
                    if (!string.IsNullOrEmpty(sort))
                        sort += ",";
                    sort += string.Format("p.{0} {1}", s.ColumnName, s.Order.ToString().ToLower());
                }
            }
            return new string[] { selectForSort, selectForSort2, sort };
        }

        /// <summary>
        /// Строит запрос для подсчета количества записей по данным фильтрам
        /// </summary>
        /// <param name="pathOfSections"></param>
        /// <param name="isArchive"></param>
        /// <param name="price"></param>
        /// <param name="queryWhereResult"></param>
        /// <returns></returns>
        private string QueryCount(long pathOfSections, bool isArchive, double[] price, string[] queryWhereResult)
        {
            return string.Format(
                        @"with prs as
                        (
	                        select p.*
	                        from Products p 
                                join Sections s on s.Id = p.SectionId 
	                        where Published = 1 and Active = 1 and p.Archive = {0}
                                 and s.Hidden = 0 and s.PathOfSections >= {1} and s.PathOfSections < {2}{3}
                        ),
                        vals as
                        (
	                        select a.ProductId{4}
	                        from prs p 
		                        left join Attributes a on p.Id = a.ProductId 
		                        left join ProductValues v on a.Id = v.AttributeId 
		                        left join FilterValues fv on fv.Id = v.ValueId
                        )
	                    select count(*) as amount
	                    from (select ProductId{5}
		                    from vals 
		                    group by ProductId) v
	                    {6};",
                Convert.ToInt32(isArchive), // 0
                pathOfSections, // 1
                SectionsUtils.GetRightPath(pathOfSections), // 2 
                BuildPriceWhere(price), // 3
                queryWhereResult[0], // 4
                queryWhereResult[1], // 5
                queryWhereResult[2] // 6
            );
        }

        /// <summary>
        /// Строит запрос для выборки записей по данным фильтрам
        /// </summary>
        /// <param name="pathOfSections"></param>
        /// <param name="isArchive"></param>
        /// <param name="price"></param>
        /// <param name="querySortResult"></param>
        /// <param name="queryWhereResult"></param>
        /// <param name="pagination"></param>
        /// <returns></returns>
        private string QuerySelect(long pathOfSections, bool isArchive, double[] price, string[] querySortResult
            , string[] queryWhereResult, Pagination pagination, int selectType)
        {
            string rowNumberOver;
            string rowNumberOffset;
            if (selectType == 0)
            {
                rowNumberOver = string.Format("order by {0}", querySortResult[2]);
                rowNumberOffset = string.Format("RowNumber > {0} and RowNumber <= {1}", pagination.Offset, pagination.Offset + pagination.PageItemsAmount);
            }
            else
            {
                string partitionBy = querySortResult[2].Split(new char[] { ',' })[0].Replace("asc", "").Replace("desc", "").Trim();
                if (partitionBy.Equals("p.PathOfSections"))
                {
                    long partitionByMask = SectionsUtils.GetPartitionByMask(pathOfSections);
                    partitionBy += string.Format(" & cast({0} as bigint)", partitionByMask);
                }
                rowNumberOver = string.Format("partition by {0} order by {1}", partitionBy, querySortResult[2]);
                rowNumberOffset = string.Format("RowNumber <= {0}", selectType);
            }
            return string.Format(@"with prs as
                        (
	                        select p.*, s.PathOfSections
	                        from Products p 
                                join Sections s on s.Id = p.SectionId 
	                        where Published = 1 and Active = 1 and p.Archive = {0}
                                 and s.Hidden = 0 and s.PathOfSections >= {1} and s.PathOfSections < {2}{3}
                        ),
                        vals as
                        (
	                        select a.ProductId{4}{5}
	                        from prs p 
		                        left join Attributes a on p.Id = a.ProductId 
		                        left join ProductValues v on a.Id = v.AttributeId 
		                        left join FilterValues fv on fv.Id = v.ValueId
                        )
                        select *
                        from (select p.*, row_number() over ({6}) RowNumber 
	                            from prs p 
		                            left join (select ProductId{7}{8}
		                                    from vals 
		                                    group by ProductId
                                        ) as v on p.Id = v.ProductId
	                            {9}
                                ) as p
                        where {10};",
                    Convert.ToInt32(isArchive), // 0
                    pathOfSections, // 1
                    SectionsUtils.GetRightPath(pathOfSections), // 2
                    BuildPriceWhere(price), // 3
                    querySortResult[0], // 4
                    queryWhereResult[0], // 5
                    rowNumberOver, // 6
                    querySortResult[1], // 7
                    queryWhereResult[1], // 8
                    queryWhereResult[2], // 9
                    rowNumberOffset // 10
                );
        }

        /// <summary>
        /// Строит условие по цене
        /// </summary>
        /// <param name="price"></param>
        /// <returns></returns>
        private string BuildPriceWhere(double[] price)
        {
            string priceWhere = "";
            if (price != null && price.Length == 2)
            {
                priceWhere += " and p.Price >= " + price[0].ToString("F0");
                if (price[1] > 0)
                    priceWhere += " and p.Price <= " + price[1].ToString("F0");
            }
            return priceWhere;
        }
    }
}
