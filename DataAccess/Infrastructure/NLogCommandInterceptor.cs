﻿using DomainLogic.Interfaces;
using System.Data.Common;
using System.Data.Entity.Infrastructure.Interception;

namespace DataAccess.Infrastructure
{
    /// <summary>
    /// Настройка логирования sql запросов. для этого реализуем интерфейс IDbCommandInterceptor.
    /// </summary>
    public class NLogCommandInterceptor : IDbCommandInterceptor
    {
        /// <summary>
        /// Логирование. Данный экземпляр определяется в момент инициализации приложения
        /// </summary>
        public static ILogger CurrentLogger { get; set; }

        /// <summary>
        /// См. описание в интерфейсе IDbCommandInterceptor
        /// </summary>
        /// <param name="command"></param>
        /// <param name="interceptionContext"></param>
        public void NonQueryExecuting(
            DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
        }

        /// <summary>
        /// См. описание в интерфейсе IDbCommandInterceptor
        /// </summary>
        /// <param name="command"></param>
        /// <param name="interceptionContext"></param>
        public void NonQueryExecuted(
            DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
            LogIfError(command, interceptionContext);
        }

        /// <summary>
        /// См. описание в интерфейсе IDbCommandInterceptor
        /// </summary>
        /// <param name="command"></param>
        /// <param name="interceptionContext"></param>
        public void ReaderExecuting(
            DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
        }

        /// <summary>
        /// См. описание в интерфейсе IDbCommandInterceptor
        /// </summary>
        /// <param name="command"></param>
        /// <param name="interceptionContext"></param>
        public void ReaderExecuted(
            DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
            LogIfError(command, interceptionContext);
        }

        /// <summary>
        /// См. описание в интерфейсе IDbCommandInterceptor
        /// </summary>
        /// <param name="command"></param>
        /// <param name="interceptionContext"></param>
        public void ScalarExecuting(
            DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
        }

        /// <summary>
        /// См. описание в интерфейсе IDbCommandInterceptor
        /// </summary>
        /// <param name="command"></param>
        /// <param name="interceptionContext"></param>
        public void ScalarExecuted(
            DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
            LogIfError(command, interceptionContext);
        }

        /// <summary>
        /// Логирует если произошла ошибка
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="command"></param>
        /// <param name="interceptionContext"></param>
        private void LogIfError<TResult>(
            DbCommand command, DbCommandInterceptionContext<TResult> interceptionContext)
        {
            if (interceptionContext.Exception != null)
            {
                if (command.CommandText.IndexOf("__MigrationHistory") != -1 && command.CommandText.IndexOf("CreatedOn") != -1)
                    return;
                
                //
                if (CurrentLogger != null) CurrentLogger.Error(string.Format("Command {0} failed with exception {1}", command.CommandText, ""), interceptionContext.Exception);
            }
        }
    }
}