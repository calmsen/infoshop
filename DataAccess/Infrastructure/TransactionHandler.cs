﻿using DomainLogic.Interfaces;
using System;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data.Entity;
using DataAccess.Interfaces;
using DomainLogic.Infrastructure.Constants;
using DomainLogic.Infrastructure.ServiceContainers;

namespace DataAccess.Infrastructure
{
    /// <summary>
    /// Обработчик транзакций
    /// </summary>
    internal class TransactionHandler : ITransactionHandler
    {
        /// <summary>
        /// Контекст для работы с БД
        /// </summary>
        private DbContext DbContext
        {
            get
            {
                if (!_tls.Contains(TLSKeys.DbContext))
                    _tls.Set(TLSKeys.DbContext, _databaseFactory.CreateDatabase());

                return _tls.Get<DbContext>(TLSKeys.DbContext);
            }
        }

        /// <summary>
        /// Логирование 
        /// </summary>
        private ILogger _logger;

        /// <summary>
        /// Фабрика базы данных
        /// </summary>
        private IDatabaseFactory _databaseFactory;

        /// <summary>
        /// Хранилище объектов
        /// </summary>
        private ITLS _tls;

        /// <summary>
        /// Инициазирует экземпляр класса
        /// </summary>
        /// <param name="logger"></param>
        public TransactionHandler(ILogger logger, IDatabaseFactory databaseFactory, ITLS tls)
        {
            _logger = logger;
            _databaseFactory = databaseFactory;
            _tls = tls;
        }

        /// <summary>
        /// Перехватчик вызываемого метода  
        /// </summary>
        /// <param name="action"></param>
        /// <param name="target"></param>
        public void Invoke(Action action, object target)
        {
            // если транзакция уже создавалась, то выполняем метод сразу
            if (DbContext.Database.CurrentTransaction != null)
            {
                action();
                return;
            }
            // создаем транзацию и пишем обработчики ошибок
            using (var tr = DbContext.Database.BeginTransaction())
            {
                string message;
                try
                {
                    action();
                    tr.Commit();
                }
                catch (DbUpdateException e)
                {
                    UpdateException updateException = (UpdateException)e.InnerException;
                    SqlException sqlException = (SqlException)updateException.InnerException;

                    if (updateException != null)
                    {
                        Debug.WriteLine(updateException.Message);
                        _logger.Error("DbUpdateException: " + updateException.Message);
                    }
                    if (sqlException != null)
                    {
                        foreach (SqlError error in sqlException.Errors)
                        {
                            Debug.WriteLine(error.Message);
                            _logger.Error("SqlException: " + error.Message);
                        }
                    }
                    //
                    tr.Rollback();
                    throw;
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        message = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        Debug.WriteLine(message);
                        _logger.Error(message);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            message = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                            _logger.Error(message);
                            Debug.WriteLine(message);
                        }

                    }
                    //
                    tr.Rollback();
                    throw;
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                    _logger.Error(e.Message + " on line " + new StackTrace(e, true).GetFrame(0).GetFileLineNumber());
                    _logger.Error(e.StackTrace);
                    tr.Rollback();
                    throw;
                }
            }
        }
    }
}