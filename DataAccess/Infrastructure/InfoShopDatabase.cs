﻿using DomainLogic.Models;
using DomainLogic.Models.Enumerations;
using System.Data.Entity;
using System.Diagnostics;

namespace DataAccess.Infrastructure
{
    /// <summary>
    /// Контекст базы данных
    /// </summary>
    public class InfoShopDatabase : DbContext
    {
        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        public InfoShopDatabase()
            : base("DefaultConnection")
        {
            Database.Log = Logger;
        }
        
        /// <summary>
        /// Конифигурирует сущности
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var dbAddress = modelBuilder.Entity<Address>();
            dbAddress.ToTable("Addresses");
            dbAddress.Property(x => x.Lat).HasMaxLength(16);
            dbAddress.Property(x => x.Lng).HasMaxLength(16);
            dbAddress.Property(x => x.Ip).HasMaxLength(16);

            var dbCountry = modelBuilder.Entity<Country>();
            dbCountry.ToTable("Countries");
            dbCountry.Property(x => x.Name).HasMaxLength(512);
            dbCountry.Property(x => x.Alias).HasMaxLength(512);
            dbCountry.Property(x => x.Lat).HasMaxLength(16);
            dbCountry.Property(x => x.Lng).HasMaxLength(16);

            var dbDistrict = modelBuilder.Entity<District>();
            dbDistrict.ToTable("Districts");
            dbDistrict.Property(x => x.Name).HasMaxLength(512);
            dbDistrict.Property(x => x.Alias).HasMaxLength(512);
            dbDistrict.Property(x => x.Lat).HasMaxLength(16);
            dbDistrict.Property(x => x.Lng).HasMaxLength(16);

            var dbRegion = modelBuilder.Entity<Region>();
            dbRegion.ToTable("Regions");
            dbRegion.Property(x => x.Name).HasMaxLength(512);
            dbRegion.Property(x => x.Alias).HasMaxLength(512);
            dbRegion.Property(x => x.Lat).HasMaxLength(16);
            dbRegion.Property(x => x.Lng).HasMaxLength(16);

            var dbCity = modelBuilder.Entity<City>();
            dbCity.ToTable("Cities");
            dbCity.Property(x => x.Name).HasMaxLength(512);
            dbCity.Property(x => x.Alias).HasMaxLength(512);
            dbCity.Property(x => x.Lat).HasMaxLength(16);
            dbCity.Property(x => x.Lng).HasMaxLength(16);
            
            var dbSection = modelBuilder.Entity<Section>();
            dbSection.ToTable("Sections");
            dbSection.Property(x => x.Name).IsRequired().HasMaxLength(128);
            dbSection.Property(x => x.Title).IsRequired().HasMaxLength(128);
            dbSection.Property(x => x.TitleIn).HasMaxLength(128);
            dbSection.Property(x => x.FiltersGroupsAsXml).HasColumnType("xml");
            dbSection.Ignore(x => x.FiltersGroups);
            dbSection.Ignore(x => x.Childs);

            var dbSectionImage = modelBuilder.Entity<SectionImage>();
            dbSectionImage.ToTable("SectionImages");
            dbSectionImage.Property(x => x.Title).HasMaxLength(256);
            dbSectionImage.Property(x => x.ExternalLink).HasMaxLength(512);

            var dbSectionToImage = modelBuilder.Entity<SectionToImage>();
            dbSectionToImage.ToTable("SectionsToImages");
            dbSectionToImage.HasKey(x => new { x.SectionId, x.ImageId });
            dbSectionToImage.Property(x => x.SectionId).HasColumnOrder(1);
            dbSectionToImage.Property(x => x.ImageId).HasColumnOrder(2);

            var dbProduct = modelBuilder.Entity<Product>();
            dbProduct.ToTable("Products");
            dbProduct.Property(x => x.Name).IsRequired().HasMaxLength(128);
            dbProduct.Property(x => x.Title).IsRequired().HasMaxLength(128);
            dbProduct.Property(x => x.TitleIn).HasMaxLength(128);
            dbProduct.Property(x => x.ShortFeatures).HasMaxLength(128);
            dbProduct.Property(x => x.Sertificate).HasMaxLength(256);
            dbProduct.Property(x => x.Comment).HasMaxLength(512);
            dbProduct.Property(x => x.ExternalLink).HasMaxLength(512);
            dbProduct.Ignore(x => x.PartnersLinksOnProduct);
            dbProduct.HasOptional(x => x.MainImage).WithMany(x => x.ProductsWithMainImage).HasForeignKey(x => x.MainImageId);
            dbProduct.HasOptional(x => x.BannerImage).WithMany(x => x.ProductsWithBanner).HasForeignKey(x => x.BannerImageId);
            dbProduct.HasOptional(x => x.BannerImageIn).WithMany(x => x.ProductsWithBannerIn).HasForeignKey(x => x.BannerImageIdIn);

            var dbProductToImage = modelBuilder.Entity<ProductToImage>();
            dbProductToImage.ToTable("ProductsToImages");
            dbProductToImage.HasKey(x => new { x.ProductId, x.ImageId });
            dbProductToImage.Property(x => x.ProductId).HasColumnOrder(1);
            dbProductToImage.Property(x => x.ImageId).HasColumnOrder(2);

            var dbAttribute = modelBuilder.Entity<DmAttribute>();
            dbAttribute.ToTable("Attributes");
            dbAttribute.HasIndex(x => new { x.ProductId, x.FilterId }).HasName("IX_ProductIdAndFilterId").IsUnique();
            dbAttribute.HasRequired(x => x.Filter).WithMany(x => x.Attributes).HasForeignKey(x => x.FilterId).WillCascadeOnDelete(false);

            var dbProductValue = modelBuilder.Entity<ProductValue>();
            dbProductValue.ToTable("ProductValues");

            var dbFilter = modelBuilder.Entity<DmFilter>();
            dbFilter.ToTable("Filters");
            dbFilter.Property(x => x.Name).IsRequired().HasMaxLength(128);
            dbFilter.Property(x => x.Title).IsRequired().HasMaxLength(128);
            dbFilter.Property(x => x.TitleIn).HasMaxLength(128);
            dbFilter.Property(x => x.Unit).HasMaxLength(32);
            dbFilter.Property(x => x.UnitIn).HasMaxLength(32);
            dbFilter.Property(x => x.UnitConvertersJson).HasMaxLength(2048);
            dbFilter.Ignore(x => x.UnitConverters);
            dbFilter.Ignore(x => x.CurrentUnitConverter);

            var dbFilterValue = modelBuilder.Entity<FilterValue>();
            dbFilterValue.ToTable("FilterValues");
            dbFilterValue.Property(x => x.Title).IsRequired().HasMaxLength(256);
            dbFilterValue.Property(x => x.TitleIn).HasMaxLength(256);

            var dbDescription = modelBuilder.Entity<Description>();
            dbDescription.ToTable("Descriptions");
            dbDescription.Property(x => x.Content).IsRequired().HasMaxLength(8000);
            dbDescription.Property(x => x.ContentIn).HasMaxLength(8000);

            var dbProductType = modelBuilder.Entity<ProductType>();
            dbProductType.ToTable("ProductTypes");
            dbProductType.Property(x => x.Title).IsRequired();

            var dbImage = modelBuilder.Entity<Image>();
            dbImage.ToTable("Images");
            dbImage.Property(x => x.Title).HasMaxLength(512);
            dbImage.Property(x => x.ExternalLink).HasMaxLength(512);

            var dbFile = modelBuilder.Entity<DmFile>();
            dbFile.ToTable("Files");
            dbFile.Property(x => x.Name).IsRequired().HasMaxLength(128);
            dbFile.Property(x => x.Title).IsRequired().HasMaxLength(128);
            dbFile.Property(x => x.TitleIn).HasMaxLength(128);
            dbFile.Property(x => x.Description).HasMaxLength(2048);
            dbFile.Property(x => x.DescriptionIn).HasMaxLength(2048);
            dbFile.Property(x => x.Version).HasMaxLength(32);
            dbFile.Property(x => x.FileSize).IsRequired().HasMaxLength(32);
            dbFile.Property(x => x.ExternalLink).HasMaxLength(512);
            modelBuilder.ComplexType<FileFilters>().Ignore(x => x.TestC);

            var dbProductToFile = modelBuilder.Entity<ProductToFile>();
            dbProductToFile.ToTable("ProductsToFiles");
            dbProductToFile.HasKey(x => new { x.ProductId, x.FileId });
            dbProductToFile.Property(x => x.ProductId).HasColumnOrder(1);
            dbProductToFile.Property(x => x.FileId).HasColumnOrder(2);

            var dbFeedback = modelBuilder.Entity<Feedback>();
            dbFeedback.ToTable("Feedbacks");
            dbFeedback.Property(x => x.Message).IsRequired().HasMaxLength(2048);
            dbFeedback.Property(x => x.UrlReferer).HasMaxLength(1024);
            dbFeedback.HasRequired(x => x.User).WithMany(x => x.Feedbacks).HasForeignKey(x => x.UserId).WillCascadeOnDelete(false);
            dbFeedback.HasRequired(x => x.UserWhoSetTheState).WithMany(x => x.FeedbacksWithUserWhoSetTheState).HasForeignKey(x => x.UserIdWhoSetTheState).WillCascadeOnDelete(false);

            var dbFeedbackTag = modelBuilder.Entity<FeedbackTag>();
            dbFeedbackTag.ToTable("FeedbackTags");
            dbFeedbackTag.Property(x => x.Name).IsRequired().HasMaxLength(128);
            dbFeedbackTag.Property(x => x.Title).IsRequired().HasMaxLength(128);

            var dbFeedbackToTag = modelBuilder.Entity<FeedbackToTag>();
            dbFeedbackToTag.ToTable("FeedbacksToFeedbackTags");
            dbFeedbackToTag.HasKey(x => new { x.FeedbackId, x.TagId });
            dbFeedbackToTag.Property(x => x.FeedbackId).HasColumnOrder(1);
            dbFeedbackToTag.Property(x => x.TagId).HasColumnOrder(2);

            var dbFeedbackAnswer = modelBuilder.Entity<FeedbackAnswer>();
            dbFeedbackAnswer.ToTable("FeedbackAnswers");
            dbFeedbackAnswer.Property(x => x.Message).IsRequired().HasMaxLength(4096);
            dbFeedbackAnswer.HasRequired(x => x.User).WithMany(x => x.FeedbackAnswers).HasForeignKey(x => x.UserId).WillCascadeOnDelete(false);

            var dbFeedbackInviting = modelBuilder.Entity<FeedbackInviting>();
            dbFeedbackInviting.ToTable("FeedbackInvitings");

            var dbFeedbackImage = modelBuilder.Entity<FeedbackImage>();
            dbFeedbackImage.ToTable("FeedbackImages");
            dbFeedbackImage.Property(x => x.Title).HasMaxLength(512);
            dbFeedbackImage.Property(x => x.ExternalLink).HasMaxLength(512);

            var dbFeedbackToImage = modelBuilder.Entity<FeedbackToImage>();
            dbFeedbackToImage.ToTable("FeedbacksToImages");
            dbFeedbackToImage.HasKey(x => new { x.FeedbackId, x.ImageId });
            dbFeedbackToImage.Property(x => x.FeedbackId).HasColumnOrder(1);
            dbFeedbackToImage.Property(x => x.ImageId).HasColumnOrder(2);

            var dbFaq = modelBuilder.Entity<Faq>();
            dbFaq.ToTable("Faq");
            dbFaq.Property(x => x.Title).HasMaxLength(256);
            dbFaq.Property(x => x.TitleIn).HasMaxLength(256);
            dbFaq.Property(x => x.Question).IsRequired().HasMaxLength(512);
            dbFaq.Property(x => x.QuestionIn).HasMaxLength(1024);
            dbFaq.Property(x => x.Answer).IsRequired();

            var dbProductToFaq = modelBuilder.Entity<ProductToFaq>();
            dbProductToFaq.ToTable("ProductsToFaqs");
            dbProductToFaq.HasKey(x => new { x.ProductId, x.FaqId });
            dbProductToFaq.Property(x => x.ProductId).HasColumnOrder(1);
            dbProductToFaq.Property(x => x.FaqId).HasColumnOrder(2);

            var dbSupportArticle = modelBuilder.Entity<SupportArticle>();
            dbSupportArticle.ToTable("SupportArticles");
            dbSupportArticle.Property(x => x.Name).IsRequired().HasMaxLength(128);
            dbSupportArticle.Property(x => x.Title).IsRequired().HasMaxLength(128);
            dbSupportArticle.Property(x => x.TitleIn).HasMaxLength(128);
            dbSupportArticle.Property(x => x.Annottion).IsRequired().HasMaxLength(1024);
            dbSupportArticle.Property(x => x.AnnottionIn).HasMaxLength(1024);

            var dbSupportArticleDescription = modelBuilder.Entity<SupportArticleDescription>();
            dbSupportArticleDescription.ToTable("SupportArticleDescriptions");
            dbSupportArticleDescription.Property(x => x.Content).IsRequired().HasMaxLength(4096);
            dbSupportArticleDescription.Property(x => x.ContentIn).HasMaxLength(4096);

            var dbArticle = modelBuilder.Entity<Article>();
            dbArticle.ToTable("News");
            dbArticle.Property(x => x.Name).IsRequired().HasMaxLength(128);
            dbArticle.Property(x => x.Title).IsRequired().HasMaxLength(128);
            dbArticle.Property(x => x.TitleIn).HasMaxLength(128);
            dbArticle.Property(x => x.Annottion).IsRequired().HasMaxLength(1024);
            dbArticle.Property(x => x.AnnottionIn).HasMaxLength(1024);
            dbArticle.HasOptional(x => x.MainImage).WithMany(x => x.NewsWithMainImage).HasForeignKey(x => x.MainImageId);

            var dbArticleDescription = modelBuilder.Entity<ArticleDescription>();
            dbArticleDescription.ToTable("ArticleDescriptions");
            dbArticleDescription.Property(x => x.Content).IsRequired().HasMaxLength(8000);
            dbArticleDescription.Property(x => x.ContentIn).HasMaxLength(8000);

            var dbArticleToImage = modelBuilder.Entity<ArticleToImage>();
            dbArticleToImage.ToTable("NewsToImages");
            dbArticleToImage.HasKey(x => new { x.ArticleId, x.ImageId });
            dbArticleToImage.Property(x => x.ArticleId).HasColumnOrder(1);
            dbArticleToImage.Property(x => x.ImageId).HasColumnOrder(2);

            var dbArticleImage = modelBuilder.Entity<ArticleImage>();
            dbArticleImage.ToTable("ArticleImages");
            dbArticleImage.Property(x => x.Title).HasMaxLength(512);
            dbArticleImage.Property(x => x.ExternalLink).HasMaxLength(512);

            var dbProductToArticle = modelBuilder.Entity<ProductToArticle>();
            dbProductToArticle.ToTable("ProductsToNews");
            dbProductToArticle.HasKey(x => new { x.ArticleId, x.ProductId });
            dbProductToArticle.Property(x => x.ProductId).HasColumnOrder(1);
            dbProductToArticle.Property(x => x.ArticleId).HasColumnOrder(2);

            var dbPost = modelBuilder.Entity<Post>();
            dbPost.ToTable("Posts");
            dbPost.Property(x => x.Name).IsRequired().HasMaxLength(128);
            dbPost.Property(x => x.Title).IsRequired().HasMaxLength(128);
            dbPost.Property(x => x.TitleIn).HasMaxLength(128);
            dbPost.Property(x => x.Annottion).HasMaxLength(1024);
            dbPost.Property(x => x.AnnottionIn).HasMaxLength(1024);
            dbPost.Property(x => x.LinkToExpertPost).HasMaxLength(128);

            var dbPostDescription = modelBuilder.Entity<PostDescription>();
            dbPostDescription.ToTable("PostDescriptions");
            dbPostDescription.Property(x => x.Content).IsRequired().HasMaxLength(8000);
            dbPostDescription.Property(x => x.ContentIn).HasMaxLength(8000);

            var dbPostImage = modelBuilder.Entity<PostImage>();
            dbPostImage.ToTable("PostImages");
            dbPostImage.Property(x => x.Title).HasMaxLength(512);
            dbPostImage.Property(x => x.ExternalLink).HasMaxLength(512);

            var dbProductToPost = modelBuilder.Entity<ProductToPost>();
            dbProductToPost.ToTable("ProductsToPosts");
            dbProductToPost.HasKey(x => new { x.ProductId, x.PostId });
            dbProductToPost.Property(x => x.ProductId).HasColumnOrder(1);
            dbProductToPost.Property(x => x.PostId).HasColumnOrder(2);

            var dbPartner = modelBuilder.Entity<Partner>();
            dbPartner.ToTable("Partners");
            dbPartner.Property(x => x.Name).IsRequired().HasMaxLength(128);
            dbPartner.Property(x => x.Title).IsRequired().HasMaxLength(128);
            dbPartner.Property(x => x.TitleIn).HasMaxLength(128);
            dbPartner.Property(x => x.LinkToPartner).IsRequired().HasMaxLength(512);

            var dbPartnerImage = modelBuilder.Entity<PartnerImage>();
            dbPartnerImage.ToTable("PartnerImages");
            dbPartnerImage.Property(x => x.Title).HasMaxLength(512);
            dbPartnerImage.Property(x => x.ExternalLink).HasMaxLength(512);

            var dbProductSnapshot = modelBuilder.Entity<ProductSnapshot>();
            dbProductSnapshot.ToTable("ProductSnapshots");
            dbProductSnapshot.Property(x => x.Snapshot).HasColumnType("text");
            dbProductSnapshot.HasRequired(x => x.User).WithMany(x => x.ProductSnapshots).HasForeignKey(x => x.UserId).WillCascadeOnDelete(false);
            modelBuilder.ComplexType<ProductStates>();

            var dbProductState = modelBuilder.Entity<ProductState>();
            dbProductState.ToTable("ProductStates");
            dbProductState.HasRequired(x => x.User).WithMany(x => x.ProductStates).HasForeignKey(x => x.UserId).WillCascadeOnDelete(false);

            

            var dbPage = modelBuilder.Entity<Page>();
            dbPage.ToTable("Pages");
            dbPage.Property(x => x.Name).IsRequired().HasMaxLength(128);
            dbPage.Property(x => x.Title).IsRequired().HasMaxLength(128);
            dbPage.Property(x => x.TitleIn).HasMaxLength(128);
            dbPage.Property(x => x.Layout).HasMaxLength(512);
            dbPage.Property(x => x.NavLinkName).HasMaxLength(128);

            var dbPageDescription = modelBuilder.Entity<PageDescription>();
            dbPageDescription.ToTable("PageDescriptions");
            dbPageDescription.Property(x => x.Content).IsRequired().HasMaxLength(8000);
            dbPageDescription.Property(x => x.ContentIn).HasMaxLength(8000);

            var dbVacancy = modelBuilder.Entity<Vacancy>();
            dbVacancy.ToTable("Vacancies");
            dbVacancy.Property(x => x.Name).IsRequired().HasMaxLength(128);
            dbVacancy.Property(x => x.Title).IsRequired().HasMaxLength(128);
            dbVacancy.Property(x => x.Address).HasMaxLength(256);
            dbVacancy.Property(x => x.Employment).HasMaxLength(256);
            dbVacancy.Property(x => x.Duties).HasMaxLength(2048);
            dbVacancy.Property(x => x.Education).HasMaxLength(256);
            dbVacancy.Property(x => x.Experience).HasMaxLength(128);
            dbVacancy.Property(x => x.AdditionalRequirements).HasMaxLength(2048);
            dbVacancy.Property(x => x.Salary).HasMaxLength(128);
            dbVacancy.Property(x => x.Email).HasMaxLength(128);

            var dbService = modelBuilder.Entity<Service>();
            dbService.ToTable("Services");
            dbService.Property(x => x.Country).HasMaxLength(128);
            dbService.Property(x => x.City).HasMaxLength(128);
            dbService.Property(x => x.Address).HasMaxLength(128);
            dbService.Property(x => x.Phones).HasMaxLength(128);
            dbService.Property(x => x.WorkingHours).HasMaxLength(512);
            dbService.Property(x => x.Guid).HasMaxLength(36);

            var dbResume = modelBuilder.Entity<Resume>();
            dbResume.ToTable("Resumes");
            dbResume.Property(x => x.Name).IsRequired().HasMaxLength(128);
            dbResume.Property(x => x.Title).IsRequired().HasMaxLength(128);
            dbResume.Property(x => x.FullName).IsRequired().HasMaxLength(128);
            dbResume.Property(x => x.Phone).IsRequired().HasMaxLength(32);
            dbResume.Property(x => x.Email).IsRequired().HasMaxLength(128);
            dbResume.Property(x => x.Education).HasMaxLength(1024);
            dbResume.Property(x => x.Experience).HasMaxLength(4096);
            dbResume.Property(x => x.AdditionalInfo).HasMaxLength(2048);
            dbResume.Property(x => x.ResumeDocLink).HasMaxLength(512);

            var dbRequestCounter = modelBuilder.Entity<RequestCounter>();
            dbRequestCounter.ToTable("RequestCounter");
            dbRequestCounter.Property(x => x.RequestUrl).HasMaxLength(1024);
            
            var dbProductCounter = modelBuilder.Entity<ProductCounter>();
            dbProductCounter.ToTable("ProductCounters");
            dbProductCounter.HasIndex(x => x.ProductId);

            var dbPageImage = modelBuilder.Entity<PageImage>();
            dbPageImage.ToTable("PageImages");
            dbPageImage.Property(x => x.Title).HasMaxLength(512);
            dbPageImage.Property(x => x.ExternalLink).HasMaxLength(512);

            var dbOption = modelBuilder.Entity<Option>();
            dbOption.ToTable("Options");
            dbOption.HasKey(x => x.Name);
            dbOption.Property(x => x.Name).HasMaxLength(128);

            var dbResource = modelBuilder.Entity<Resource>();
            dbResource.ToTable("Resources");
            dbResource.HasKey(x => x.Key);

            var dbUser = modelBuilder.Entity<User>();
            dbUser.ToTable("Users");
            dbUser.Property(x => x.Email).HasMaxLength(128);
            dbUser.Property(x => x.FullName).HasMaxLength(128);
        }
        
        /// <summary>
        /// Настройка логирования для дебага
        /// </summary>
        /// <param name="logString"></param>
        private void Logger(string logString)
        {
            Debug.WriteLine(logString);
        }
    }
}