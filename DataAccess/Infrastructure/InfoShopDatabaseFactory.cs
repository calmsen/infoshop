﻿using System.Data.Entity;
using DataAccess.Interfaces;

namespace DataAccess.Infrastructure
{
    /// <summary>
    /// Фабрика для создания БД
    /// </summary>
    internal class InfoShopDatabaseFactory : IDatabaseFactory
    {
        /// <summary>
        /// Создает БД
        /// </summary>
        /// <returns></returns>
        public DbContext CreateDatabase()
        {
            return new InfoShopDatabase();
        }
    }
}
