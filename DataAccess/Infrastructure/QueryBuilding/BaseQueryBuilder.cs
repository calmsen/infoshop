﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Interfaces.QueryBuilder;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Reflection;
using System.Threading.Tasks;

namespace DataAccess.Infrastructure.QueryBuilding
{
    /// <summary>
    /// Базовый класс для формирования запроса в БД.
    /// </summary>
    internal class BaseQueryBuilder : IBaseQueryBuilder
    {
        /// <summary>
        /// Методанные сущностей
        /// </summary>
        public EntityMetadata EntityMetadata
        {
            get
            {
                _entityMetadataProvider.SetMetadataWorkspace(_dbContext);
                return _entityMetadataProvider.Get(_entityType);
            }
        } 

        /// <summary>
        /// Основной ключ сущности
        /// </summary>
        public string PrimaryKey => EntityMetadata.PrimaryKeys.FirstOrDefault()?.FieldName;

        /// <summary>
        /// Работа со строками
        /// </summary>
        private StringUtils _stringUtils;

        /// <summary>
        /// Работа с перечислениями
        /// </summary>
        private EnumUtils _enumUtils;

        /// <summary>
        /// Работа с методанными сущностей
        /// </summary>
        private EntityMetadataProvider _entityMetadataProvider;

        /// <summary>
        /// Контекст для работы с БД
        /// </summary>
        private readonly DbContext _dbContext;

        /// <summary>
        /// Тип сущности, для которой формируется запрос к БД
        /// </summary>
        private Type _entityType;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="entityMetadataProvider"></param>
        /// <param name="stringUtils"></param>
        /// <param name="enumUtils"></param>
        public BaseQueryBuilder(DbContext dbContext, EntityMetadataProvider entityMetadataProvider, StringUtils stringUtils, EnumUtils enumUtils)
        {
            _dbContext = dbContext;
            _entityMetadataProvider = entityMetadataProvider;
            _stringUtils = stringUtils;
            _enumUtils = enumUtils;
        }

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="entityType"></param>
        /// <param name="entityMetadataProvider"></param>
        /// <param name="stringUtils"></param>
        /// <param name="enumUtils"></param>
        public BaseQueryBuilder(DbContext dbContext, Type entityType, EntityMetadataProvider entityMetadataProvider, StringUtils stringUtils, EnumUtils enumUtils)
        {
            _dbContext = dbContext;
            _entityType = entityType;
            _entityMetadataProvider = entityMetadataProvider;
            _stringUtils = stringUtils;
            _enumUtils = enumUtils;
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <returns></returns>
        public virtual async Task<TElement> FirstOrDefaultAsync<TElement>()
        {
            return (await FirstOrDefaultAsync<TElement>(null));
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="where"></param>
        /// <returns></returns>
        public async Task<TElement> FirstOrDefaultAsync<TElement>(object where)
        {
            var fields = typeof(TElement).GetProperties().Select(x => x.Name).ToArray();
            return (await FirstOrDefaultAsync<TElement>(fields, where));
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="fields"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public async Task<TElement> FirstOrDefaultAsync<TElement>(string[] fields, object where = null)
        {
            return (TElement)(await FirstOrDefaultAsync(typeof(TElement), fields, where));
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <param name="elementType"></param>
        /// <param name="fields"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public async Task<object> FirstOrDefaultAsync(Type elementType, string[] fields, object where = null)
        {
            List<object> list = await ToListAsync(elementType, fields, where);

            if (list.Count > 0)
                return list[0];

            if (elementType.IsValueType && Nullable.GetUnderlyingType(elementType) == null)
                return Activator.CreateInstance(elementType);
            else
                return null;
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <returns></returns>
        public virtual async Task<List<TElement>> ToListAsync<TElement>()
        {
            return await ToListAsync<TElement>(null, null);
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="where"></param>
        /// <returns></returns>
        public async Task<List<TElement>> ToListAsync<TElement>(object where)
        {
            return await ToListAsync<TElement>(null, where);
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="fields"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public async Task<List<TElement>> ToListAsync<TElement>(string[] fields, object where = null)
        {
            fields = fields ?? typeof(TElement).GetProperties().Select(x => x.Name).ToArray();
            return await _dbContext.Database.SqlQuery<TElement>(BuildSelectQuery(fields, where)).ToListAsync();
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <param name="elementType"></param>
        /// <param name="fields"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public async Task<List<object>> ToListAsync(Type elementType, string[] fields, object where = null)
        {
            fields = fields ?? elementType.GetProperties().Select(x => x.Name).ToArray();
            return await _dbContext.Database.SqlQuery(elementType, BuildSelectQuery(fields, where)).ToListAsync();
        }

        /// <summary>
        /// Добавляет запись в таблицу, соответсвующей сущности
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public async Task InsertAsync(object obj, object where = null)
        {
            if (EntityMetadata.PrimaryKeys.Count == 1)
            {
                string primaryKeyname = EntityMetadata.PrimaryKeys[0].FieldName;
                Type primaryKeyType = _entityType.GetProperty(primaryKeyname).PropertyType;

                if (EntityMetadata.PrimaryKeys[0].IsStoreGeneratedIdentity)
                    foreach (object id in _dbContext.Database.SqlQuery(primaryKeyType, BuildInsertQuery(obj)))
                        obj.GetType().GetProperty(primaryKeyname).SetValue(obj, id);
                else
                    await _dbContext.Database.ExecuteSqlCommandAsync(BuildInsertQuery(obj));
            }
            else
            {
                await _dbContext.Database.ExecuteSqlCommandAsync(BuildInsertQuery(obj));
            }
        }

        /// <summary>
        /// Обновляет запись
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public async Task UpdateAsync(object obj, object where = null)
        {
            await _dbContext.Database.ExecuteSqlCommandAsync(BuildUpdateQuery(obj, where));
        }

        /// <summary>
        /// Удаляет запись
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public async Task DeleteAsync(object where)
        {
            await _dbContext.Database.ExecuteSqlCommandAsync(BuildDeleteQuery(where));
        }

        /// <summary>
        /// Выполняет sql запрос
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public Task<int> ExecuteAsync(string sql, params object[] parameters)
        {
            return _dbContext.Database.ExecuteSqlCommandAsync(sql, parameters);
        }

        /// <summary>
        /// Выполняет sql запрос для получения одной записи 
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public async Task<TElement> ToTargetItemAsync<TElement>(string sql, params object[] parameters)
        {
            return await _dbContext.Database.SqlQuery<TElement>(sql, parameters).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Выполняет sql запрос для получения списка записей
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public async Task<List<TElement>> ToTargetListAsync<TElement>(string sql, params object[] parameters)
        {
            return await _dbContext.Database.SqlQuery<TElement>(sql, parameters).ToListAsync();
        }
        
        /// <summary>
        /// Указывает что строку не нужно обрабатывать в sql запросе
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string DbRaw(string str) => $"(RAW)" + str;

        /// <summary>
        /// Строит sql запрос для удаления записей
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        private string BuildDeleteQuery(object where)
        {
            return string.Format("delete from {0}{1};",
                EntityMetadata.TableName,
                BuildWhere(where));
        }

        /// <summary>
        /// Строит sql запрос для добавления записи
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private string BuildInsertQuery(object obj)
        {
            string tableName = EntityMetadata.TableName;

            string keys, values;
            GetKeysAndValuesForQuery(obj, out keys, out values);
            string sql = string.Format("insert into {0} ({1}) values ({2})", tableName, keys, values);

            if (EntityMetadata.PrimaryKeys.Count == 1)
            {
                string primaryKeyname = EntityMetadata.PrimaryKeys[0].FieldName;

                if (EntityMetadata.PrimaryKeys[0].IsStoreGeneratedIdentity)
                    sql += string.Format("select {0} from {1} where @@rowcount > 0 and {2} = scope_identity();", primaryKeyname, tableName, primaryKeyname);
            }

            return sql;
        }

        /// <summary>
        /// Строит sql запрос для обновления записи
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        private string BuildUpdateQuery(object obj, object where = null)
        {
            return string.Format("update {0} set {1}{2};",
                EntityMetadata.TableName,
                GetKeyValuePairsForQuery(obj),
                BuildWhere(obj, where));
        }

        /// <summary>
        /// Строит sql запрос для получения записей
        /// </summary>
        /// <param name="fields"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        private string BuildSelectQuery(string[] fields, object where = null)
        {
            return string.Format("select {0} from {1}{2}",
                    string.Join(", ", fields),
                    EntityMetadata.TableName,
                    BuildWhereFromObject(where)
                );
        }

        /// <summary>
        /// Строит выражение where, используя из объекта только основный ключи 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private string BuildWhereFromPrimaryKeys(object obj)
        {
            if (obj == null)
                return null;

            string where = string.Empty;
            foreach (PropertyInfo prop in obj.GetType().GetProperties())
            {
                var entityProp = EntityMetadata.PrimaryKeys.FirstOrDefault(x => x.FieldName == prop.Name);
                if (entityProp == null)
                    continue;

                if (!where.Equals(string.Empty))
                    where += ", ";

                where += string.Format("[{0}]={1}",
                    entityProp.FieldName,
                    GetPropValue(obj, prop)
                );
            }

            if (string.IsNullOrEmpty(where))
                return null;

            return string.Format(" where {0}", where);
        }

        /// <summary>
        /// Строит выражение where, используя из переданного объекта все свойства
        /// </summary>
        /// <param name="whereObject"></param>
        /// <returns></returns>
        private string BuildWhereFromObject(object whereObject)
        {
            if (whereObject == null)
                return null;
            string where = string.Empty;
            foreach (PropertyInfo prop in whereObject.GetType().GetProperties())
            {
                var entityProp = EntityMetadata.Properties.FirstOrDefault(x => x.FieldName == prop.Name);
                if (entityProp == null)
                    continue;

                if (!where.Equals(string.Empty))
                    where += " and ";
                var value = GetPropValue(whereObject, prop);
                where += $"[{prop.Name}]{(value != "null" ? " = " : " is ")}{value}";
            }
            if (where != null)
                where = $" where {where}";
            return where;
        }

        /// <summary>
        /// Строит выражение where. Если объект является сущностью, то выражения строится исходя из основных ключей, иначе из всех свойств
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private string BuildWhere(object obj)
        {
            bool onlyPrimaryKeys = obj.GetType() == _entityType;
            if (onlyPrimaryKeys)
                return BuildWhere(obj, null);
            else
                return BuildWhere(null, obj);
        }

        /// <summary>
        /// Строит выражение where. Если объект whereObject не равен null, то выражение строится исходя из него, иначе строится на анализе основных ключей из объекта obj
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="whereObject"></param>
        /// <returns></returns>
        private string BuildWhere(object obj, object whereObject)
        {
            if (whereObject == null)
                return BuildWhereFromPrimaryKeys(obj);

            if (whereObject.GetType() == typeof(string))
                return string.Format(" where {0}", (string)whereObject);

            return BuildWhereFromObject(whereObject);
        }

        /// <summary>
        /// Проверяет является ли тип свойства списком enum-ов
        /// </summary>
        /// <param name="propType"></param>
        /// <returns></returns>
        private bool IsGenericListOfEnums(Type propType)
        {
            return propType.GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>) && i.GetGenericArguments()[0].IsEnum);
        }

        /// <summary>
        /// Проверяет доступно ли свойство для обработки
        /// </summary>
        /// <param name="prop"></param>
        /// <returns></returns>
        private bool CheckAcceptableEntry(PropertyInfo prop)
        {
            if (prop.PropertyType == typeof(string)
                    || prop.PropertyType == typeof(long) || prop.PropertyType == typeof(long?)
                    || prop.PropertyType == typeof(int) || prop.PropertyType == typeof(int?)
                    || prop.PropertyType == typeof(bool) || prop.PropertyType == typeof(bool?)
                    || prop.PropertyType == typeof(double) || prop.PropertyType == typeof(double?)
                    || prop.PropertyType == typeof(float) || prop.PropertyType == typeof(float?)
                    || prop.PropertyType == typeof(DateTime) || prop.PropertyType == typeof(DateTime?)
                    || prop.PropertyType.IsEnum || IsNullableEnum(prop.PropertyType))
                return true;
            return false;
        }

        /// <summary>
        /// Проверяет является ли свойство Nullable enum-а
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        private bool IsNullableEnum(Type t)
        {
            return t.IsGenericType &&
                   t.GetGenericTypeDefinition() == typeof(Nullable<>) &&
                   t.GetGenericArguments()[0].IsEnum;
        }

        /// <summary>
        /// Получает значение свойства из объекта
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="prop"></param>
        /// <returns></returns>
        private string GetPropValue(object obj, PropertyInfo prop)
        {
            var propType = prop.PropertyType;
            var propValue = prop.GetValue(obj);

            if (propType == typeof(string) && propValue != null && ((string)propValue).IndexOf("(RAW)") == 0)
                return ((string)propValue).Replace("(RAW)", "");
            if (propType == typeof(string))
                return propValue == null ? "null" : "'" + _stringUtils.SqlEscape((string)propValue) + "'";
            if (propType == typeof(long))
                return Convert.ToInt64(propValue).ToString();
            if (propType == typeof(long?))
                return propValue == null ? "null" : Convert.ToInt64(propValue).ToString();
            if (propType == typeof(int) || propType == typeof(bool))
                return Convert.ToInt32(propValue).ToString();
            if (propType == typeof(int?) || propType == typeof(bool?))
                return propValue == null ? "null" : Convert.ToInt32(propValue).ToString();
            if (propType == typeof(float) || propType == typeof(double))
                return String.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0.##}", propValue);
            if (propType == typeof(float?) || propType == typeof(double?))
                return propValue == null ? "null" : string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0.##}", propValue);
            if (propType == typeof(DateTime))
                return "convert(datetime,'" + ((DateTime)propValue).ToString("yyyy-MM-dd HH:mm:ss") + "',120)";
            if (propType == typeof(DateTime?))
                return propValue == null ? "null" : "convert(datetime,'" + ((DateTime)propValue).ToString("yyyy-MM-dd HH:mm:ss") + "',120)";
            if (propType.IsEnum)
                return ((int)propValue).ToString();
            if (IsNullableEnum(propType))
                return propValue == null ? "null" : ((int)propValue).ToString();
            if (IsGenericListOfEnums(propType))
                return _enumUtils.ListOfEnumsToInt((IEnumerable)propValue).ToString();
            throw new ArgumentException();
        }

        /// <summary>
        /// Получает отдельно список ключей и значений для запроса (обычно insert)
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="keys"></param>
        /// <param name="values"></param>
        /// <param name="postfix"></param>
        /// <param name="propsMetadata"></param>
        private void GetKeysAndValuesForQuery(object obj, out string keys, out string values, string postfix = "", List<EntityProperty> propsMetadata = null)
        {
            keys = "";
            values = "";
            foreach (PropertyInfo prop in obj.GetType().GetProperties())
            {
                var entityProp = (propsMetadata ?? EntityMetadata.Properties).FirstOrDefault(x => x.FieldName == prop.Name);
                if (entityProp == null)
                    continue;

                if (entityProp.IsComplexType)
                {
                    if (!string.IsNullOrEmpty(keys))
                        keys += ",";
                    if (!string.IsNullOrEmpty(values))
                        values += ",";

                    string innerKeys;
                    string innerValues;
                    GetKeysAndValuesForQuery(prop.GetValue(obj), out innerKeys, out innerValues, prop.Name + "_", entityProp.ComplexTypeProperties);

                    keys += innerKeys;
                    values += innerValues;
                    continue;
                }

                if (!CheckAcceptableEntry(prop))
                    continue;

                if (entityProp.IsStoreGeneratedIdentity)
                    continue;

                if (!string.IsNullOrEmpty(keys))
                    keys += ",";

                keys += string.Format("[{0}]", postfix + prop.Name);

                if (!string.IsNullOrEmpty(values))
                    values += ",";

                values += GetPropValue(obj, prop);
            };
        }

        /// <summary>
        /// Получает строку включающую список ключей и значений (разделенных запятыми) для запроса (обычно update)
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="postfix"></param>
        /// <param name="propsMetadata"></param>
        /// <returns></returns>
        private string GetKeyValuePairsForQuery(object obj, string postfix = "", List<EntityProperty> propsMetadata = null)
        {
            string keyValuePairs = "";
            foreach (PropertyInfo prop in obj.GetType().GetProperties())
            {
                var entityProp = (propsMetadata ?? EntityMetadata.Properties).FirstOrDefault(x => x.FieldName == prop.Name);
                if (entityProp == null)
                    continue;

                if (entityProp.IsComplexType)
                {
                    if (!string.IsNullOrEmpty(keyValuePairs))
                        keyValuePairs += ",";
                    keyValuePairs += GetKeyValuePairsForQuery(prop.GetValue(obj), prop.Name + "_", entityProp.ComplexTypeProperties);
                    continue;
                }
                if (!CheckAcceptableEntry(prop))
                    continue;

                if (entityProp.IsStoreGeneratedIdentity)
                    continue;

                if (!string.IsNullOrEmpty(keyValuePairs))
                    keyValuePairs += ",";

                keyValuePairs += string.Format("[{0}]", postfix + prop.Name) + " = ";

                keyValuePairs += GetPropValue(obj, prop);
            };
            return keyValuePairs;
        }
    }
}
