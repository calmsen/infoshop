﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Mapping;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace DataAccess.Infrastructure.QueryBuilding
{
    /// <summary>
    /// Предоставляет метаданные по сущности
    /// </summary>
    internal class EntityMetadataProvider
    {
        /// <summary>
        /// Метаданные полученные из контекста БД
        /// </summary>
        private MetadataWorkspace _metadataWorkspace;

        /// <summary>
        /// Словарь метаданных по сущностям
        /// </summary>
        private Dictionary<Type, EntityMetadata> _metadataMap = new Dictionary<Type, EntityMetadata>();

        /// <summary>
        /// Устанавливает метаданные из контекста БД
        /// </summary>
        /// <param name="dbContext"></param>
        public void SetMetadataWorkspace(DbContext dbContext)
        {
            if (_metadataWorkspace != null)
                return;

            _metadataWorkspace = ((IObjectContextAdapter)dbContext).ObjectContext.MetadataWorkspace;
        }

        /// <summary>
        /// Получает метаданные по сущности
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        public EntityMetadata Get(Type entityType)
        {
            if (_metadataMap.ContainsKey(entityType))
                return _metadataMap[entityType];

            // Get the part of the model that contains info about the actual CLR types
            var objectItemCollection = ((ObjectItemCollection)_metadataWorkspace.GetItemCollection(DataSpace.OSpace));

            // Get the entity set that uses this entity type
            var entitySet = _metadataWorkspace
                .GetItems<EntityContainer>(DataSpace.CSpace)
                .Single()
                .EntitySets
                .Single(s => s.ElementType.Name == entityType.Name);

            // Find the mapping between conceptual and storage model for this entity set
            var mapping = _metadataWorkspace.GetItems<EntityContainerMapping>(DataSpace.CSSpace)
                    .Single()
                    .EntitySetMappings
                    .Single(s => s.EntitySet == entitySet);

            // Find the storage entity set (table) that the entity is mapped
            var table = mapping
                .EntityTypeMappings.Single()
                .Fragments.Single()
                .StoreEntitySet;


            var myEntityMetatdata = (from meta in _metadataWorkspace.GetItems<EntityType>(DataSpace.SSpace)
                                                .Where(m => m.BuiltInTypeKind == BuiltInTypeKind.EntityType)
                                                .Where(m => (m as EntityType).Name == entityType.Name)
                                     let m = (meta as EntityType)
                                     select new EntityMetadata
                                     {
                                         TableName = (string)table.MetadataProperties["Table"].Value ?? table.Name,
                                         Properties = m.Properties.Select(x => new EntityProperty
                                         {
                                             FieldName = x.Name,
                                             IsComplexType = x.Name.Contains("_"),
                                             IsStoreGeneratedIdentity = x.IsStoreGeneratedIdentity,
                                             IsPrimaryKey = m.KeyProperties.Any(y => y.Name == x.Name)
                                         }).ToList()
                                     }).First();

            var complexTypes = new Dictionary<string, List<EntityProperty>>();
            for (int i = 0; i < myEntityMetatdata.Properties.Count; i++)
            {
                var prop = myEntityMetatdata.Properties[i];
                if (!prop.IsComplexType)
                    continue;

                int propSeparatorPos = prop.FieldName.IndexOf("_");
                string complextTypeName = prop.FieldName.Substring(0, propSeparatorPos);

                if (!complexTypes.ContainsKey(complextTypeName))
                    complexTypes[complextTypeName] = new List<EntityProperty>();

                complexTypes[complextTypeName].Add(prop);
                myEntityMetatdata.Properties.RemoveAt(i);

                prop.FieldName = prop.FieldName.Substring(propSeparatorPos + 1);
                prop.IsComplexType = false;
                i--;
            }
            foreach (var item in complexTypes)
            {
                myEntityMetatdata.Properties.Add(new EntityProperty
                {
                    FieldName = item.Key,
                    ComplexTypeProperties = item.Value,
                    IsComplexType = true
                });
            }
            return _metadataMap[entityType] = myEntityMetatdata;
        }
        
        /// <summary>
        /// Проверяет если метаданные по сущности
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        public bool Contains(Type entityType)
        {
            if (_metadataMap.ContainsKey(entityType))
                return true;
            
            return _metadataWorkspace
                .GetItems<EntityContainer>(DataSpace.CSpace)
                .Single()
                .EntitySets
                .FirstOrDefault(s => s.ElementType.Name == entityType.Name) != null;
        }
    }
}
