﻿using DomainLogic.Infrastructure.Utils;
using DomainLogic.Interfaces.QueryBuilder;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DataAccess.Infrastructure.QueryBuilding
{
    /// <summary>
    /// Класс для формирования запроса в БД. Класс не имеет публичный конструктор. Для работы с данным классом следует использовать QueryBuilder<TEntity>
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    internal class InnerTypeQueryBuilder<TEntity> : BaseQueryBuilder, IQueryBuilder<TEntity>
    {
        /// <summary>
        /// Контекст для работы с БД
        /// </summary>
        private DbContext _dbContext;

        /// <summary>
        /// Тип сущности
        /// </summary>
        private Type _entityType;

        /// <summary>
        /// Формируемый запрос, который будет в конечном счете исполнен
        /// </summary>
        private IQueryable<TEntity> _dbQuery;

        /// <summary>
        ///  Работа со строками
        /// </summary>
        private StringUtils _stringUtils;

        /// <summary>
        /// Работа с перечислениями
        /// </summary>
        private EnumUtils _enumUtils;

        /// <summary>
        /// Работа с методаннами сущностей
        /// </summary>
        private EntityMetadataProvider _entityMetadataProvider;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="entityType"></param>
        /// <param name="dbQuery"></param>
        /// <param name="entityMetadataProvider"></param>
        /// <param name="stringUtils"></param>
        /// <param name="enumUtils"></param>
        private InnerTypeQueryBuilder(DbContext dbContext, Type entityType, IQueryable<TEntity> dbQuery, EntityMetadataProvider entityMetadataProvider, StringUtils stringUtils, EnumUtils enumUtils) 
            : base(dbContext, entityType, entityMetadataProvider, stringUtils, enumUtils)
        {
            _dbContext = dbContext;
            _entityType = entityType;
            _dbQuery = dbQuery;
            _entityMetadataProvider = entityMetadataProvider;
            _stringUtils = stringUtils;
            _enumUtils = enumUtils;
        }

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="entityMetadataProvider"></param>
        /// <param name="stringUtils"></param>
        /// <param name="enumUtils"></param>
        public InnerTypeQueryBuilder(DbContext dbContext, EntityMetadataProvider entityMetadataProvider, StringUtils stringUtils, EnumUtils enumUtils) 
            : base(dbContext, typeof(TEntity), entityMetadataProvider, stringUtils, enumUtils)
        {
            _dbContext = dbContext;
            _entityType = typeof(TEntity);
            _entityMetadataProvider = entityMetadataProvider;
            _stringUtils = stringUtils;
            _enumUtils = enumUtils;
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return GetQuery().FirstOrDefault();
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <returns></returns>
        public async Task<TEntity> FirstOrDefaultAsync()
        {
            return await GetQuery().FirstOrDefaultAsync();
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await GetQuery().FirstOrDefaultAsync(predicate);
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <returns></returns>
        public List<TEntity> ToList()
        {
            return GetQuery().ToList();
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <returns></returns>
        public async Task<List<TEntity>> ToListAsync()
        {
            return await GetQuery().ToListAsync();
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public async Task<List<TEntity>> ToListAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await Where(predicate).ToListAsync();
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="selector"></param>
        /// <returns></returns>
        public async Task<List<TElement>> ToListAsync<TElement>(Expression<Func<TEntity, TElement>> selector)
        {
            return await GetQuery().Select(selector).ToListAsync();
        }

        /// <summary>
        /// Строит выражение Select. См. описание у EF метода
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="selector"></param>
        /// <returns></returns>
        public IQueryBuilder<TElement> Select<TElement>(Expression<Func<TEntity, TElement>> selector)
        {
            return new InnerTypeQueryBuilder<TElement>(_dbContext, _entityType, GetQuery().Select(selector), _entityMetadataProvider, _stringUtils, _enumUtils);
        }

        /// <summary>
        /// Строит выражение Join. См. описание у EF метода
        /// </summary>
        /// <typeparam name="TInner"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="inner"></param>
        /// <param name="outerKeySelector"></param>
        /// <param name="innerKeySelector"></param>
        /// <param name="resultSelector"></param>
        /// <returns></returns>
        public IQueryBuilder<TElement> Join<TInner, TKey, TElement>(IEnumerable<TInner> inner, Expression<Func<TEntity, TKey>> outerKeySelector,
            Expression<Func<TInner, TKey>> innerKeySelector, Expression<Func<TEntity, TInner, TElement>> resultSelector)
             where TInner : class
        {
            var dbQuery = GetQuery().Join(_dbContext.Set<TInner>(), outerKeySelector, innerKeySelector, resultSelector);
            return new InnerTypeQueryBuilder<TElement>(_dbContext, _entityType, dbQuery, _entityMetadataProvider, _stringUtils, _enumUtils);
        }

        /// <summary>
        /// Строит выражение Include. См. описание у EF метода
        /// </summary>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public IQueryBuilder<TEntity> Include<TProperty>(Expression<Func<TEntity, TProperty>> path)
        {
            _dbQuery = GetQuery().Include(path);
            return this;
        }

        /// <summary>
        /// Строит выражение where. См. описание у EF метода
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public IQueryBuilder<TEntity> Where(Expression<Func<TEntity, bool>> predicate)
        {
            _dbQuery = GetQuery().Where(predicate);
            return this;
        }

        /// <summary>
        /// Строит выражение where. См. описание у EF метода
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public IQueryBuilder<TEntity> Where(string predicate, params object[] values)
        {
            _dbQuery = GetQuery().Where(predicate, values);
            return this;
        }

        /// <summary>
        /// Строит выражение OrderBy. См. описание у EF метода
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        public IQueryBuilder<TEntity> OrderBy<TKey>(Expression<Func<TEntity, TKey>> keySelector)
        {
            _dbQuery = GetQuery().OrderBy(keySelector);
            return this;
        }

        /// <summary>
        /// Строит выражение OrderBy. См. описание у EF метода
        /// </summary>
        /// <param name="ordering"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public IQueryBuilder<TEntity> OrderBy(string ordering, params object[] values)
        {
            _dbQuery = GetQuery().OrderBy(ordering, values);
            return this;
        }

        /// <summary>
        /// Строит выражение OrderByDescending. См. описание у EF метода
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        public IQueryBuilder<TEntity> OrderByDescending<TKey>(Expression<Func<TEntity, TKey>> keySelector)
        {
            _dbQuery = GetQuery().OrderByDescending(keySelector);
            return this;
        }

        /// <summary>
        /// Строит выражение Skip. Количество записей которые нужно пропустить
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public IQueryBuilder<TEntity> Skip(int count)
        {
            _dbQuery = GetQuery().Skip(count);
            return this;
        }

        /// <summary>
        /// Строит выражение Take. Количество записей которые нужно получить
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public IQueryBuilder<TEntity> Take(int count)
        {
            _dbQuery = GetQuery().Take(count);
            return this;
        }

        /// <summary>
        /// Строит выражение Distinct. См. описание у EF метода
        /// </summary>
        /// <returns></returns>
        public IQueryBuilder<TEntity> Distinct()
        {
            _dbQuery = GetQuery().Distinct();
            return this;
        }

        /// <summary>
        /// Получает количество записей. См. описание у EF метода 
        /// </summary>
        /// <returns></returns>
        public async Task<int> CountAsync()
        {
            return await GetQuery().CountAsync();
        }

        /// <summary>
        /// Получает количество записей. См. описание у EF метода
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public async Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await GetQuery().CountAsync(predicate);
        }

        /// <summary>
        /// Создает IQueryable<TEntity> объект по типу сущности
        /// </summary>
        /// <typeparam name="T"></typeparam>
        protected void SetQuery<T>()
            where T : class
        {
            _dbQuery = (IQueryable<TEntity>)_dbContext.Set<T>().AsNoTracking();
        }

        /// <summary>
        /// Получает IQueryable<TEntity> объект
        /// </summary>
        /// <returns></returns>
        private IQueryable<TEntity> GetQuery()
        {
            return _dbQuery;
        }
    }
}
