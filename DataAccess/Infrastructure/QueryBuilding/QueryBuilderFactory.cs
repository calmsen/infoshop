﻿using DataAccess.Interfaces;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Constants;
using DomainLogic.Infrastructure.Localization;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Interfaces;
using DomainLogic.Interfaces.QueryBuilder;
using System;
using System.Data.Entity;
namespace DataAccess.Infrastructure.QueryBuilding
{
    /// <summary>
    /// Фабрика для создания QueryBuilder
    /// </summary>
    internal class QueryBuilderFactory : IQueryBuilderFactory
    {
        /// <summary>
        /// Контекст для хранения данных для рабочего потока
        /// </summary>
        [Inject]
        public ITLS TLS { get; set; }

        /// <summary>
        /// Фабрика для создания контекста БД
        /// </summary>
        [Inject]
        public IDatabaseFactory DatabaseFactory { get; set; }

        /// <summary>
        /// Работа с локализацией
        /// </summary>
        [Inject]
        public AggregateLocalizer AggregateLocalizer { get; set; }

        /// <summary>
        /// Работа со строками
        /// </summary>
        [Inject]
        public StringUtils StringUtils { get; set; }

        /// <summary>
        /// Работа с перечислениями
        /// </summary>
        [Inject]
        public EnumUtils EnumUtils { get; set; }

        /// <summary>
        /// Работа с методанными сущностей
        /// </summary>
        [Inject]
        public EntityMetadataProvider EntityMetadataProvider { get; set; }

        /// <summary>
        /// Создает базовый билдер для формирования запроса
        /// </summary>
        /// <returns></returns>
        public IBaseQueryBuilder Create()
        {
            FixEfProviderServicesProblem();
            var queryBuilder = new BaseQueryBuilder(new InfoShopDatabase(), EntityMetadataProvider, StringUtils, EnumUtils);
            return new LocalizationBaseQueryBuilderDecorator(queryBuilder, AggregateLocalizer);
        }

        /// <summary>
        /// Создает билдер для формирования запроса
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public IQueryBuilder<TEntity> Create<TEntity>()
            where TEntity : class
        {
            FixEfProviderServicesProblem();
            var queryBuilder = new QueryBuilder<TEntity>(new InfoShopDatabase(), EntityMetadataProvider, StringUtils, EnumUtils);
            return new LocalizationQueryBuilderDecorator<TEntity>(queryBuilder, AggregateLocalizer, this);
        }

        /// <summary>
        /// Создает базовый билдер для формирования запроса
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="queryBuilder"></param>
        /// <returns></returns>
        public IQueryBuilder<TEntity> Create<TEntity>(IQueryBuilder<TEntity> queryBuilder)
        {
            FixEfProviderServicesProblem();
            return new LocalizationQueryBuilderDecorator<TEntity>(queryBuilder, AggregateLocalizer, this);
        }

        /// <summary>
        /// Создает базовый билдер для формирования запроса
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        public IQueryBuilder Create(Type entityType)
        {
            FixEfProviderServicesProblem();
            var queryBuilder = new QueryBuilder(new InfoShopDatabase(), entityType, EntityMetadataProvider, StringUtils, EnumUtils);
            return new LocalizationQueryBuilderDecorator(queryBuilder, AggregateLocalizer);
        }

        /// <summary>
        /// Контекст для работы с БД
        /// </summary>
        public DbContext DbContext
        {
            get
            {
                if (!TLS.Contains(TLSKeys.DbContext))
                    TLS.Set(TLSKeys.DbContext, DatabaseFactory.CreateDatabase());

                return TLS.Get<DbContext>(TLSKeys.DbContext);
            }
        }

        /// <summary>
        /// Фикс необходимый для работы с тестами
        /// </summary>
        public void FixEfProviderServicesProblem()
        {
            //The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer'
            //for the 'System.Data.SqlClient' ADO.NET provider could not be loaded. 
            //Make sure the provider assembly is available to the running application. 
            //See http://go.microsoft.com/fwlink/?LinkId=260882 for more information.

            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

    }
}
