﻿using DomainLogic.Infrastructure.Utils;
using DomainLogic.Interfaces.QueryBuilder;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DataAccess.Infrastructure.QueryBuilding
{
    /// <summary>
    /// Класс для формирования запроса в БД. Тип сущности передается в конструкторе. 
    /// Если другой обобщенный класс QueryBuilder<TEntity>, в конструктор которого не нужно передавать тип сущности
    /// </summary>
    internal class QueryBuilder : BaseQueryBuilder, IQueryBuilder
    {
        /// <summary>
        /// Контекст для работы с БД
        /// </summary>
        private readonly DbContext _dbContext;

        /// <summary>
        /// Тип сущности
        /// </summary>
        private Type _entityType;

        /// <summary>
        /// Работа с методаннами сущностей
        /// </summary>
        private EntityMetadataProvider _entityMetadataProvider;

        /// <summary>
        /// Формируемый запрос, который будет в конечном счете исполнен
        /// </summary>
        private IQueryable _dbQuery;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="entityType"></param>
        /// <param name="entityMetadataProvider"></param>
        /// <param name="stringUtils"></param>
        /// <param name="enumUtils"></param>
        public QueryBuilder(DbContext dbContext, Type entityType, EntityMetadataProvider entityMetadataProvider, StringUtils stringUtils, EnumUtils enumUtils) 
            : base(dbContext, entityType, entityMetadataProvider, stringUtils, enumUtils)
        {
            _dbContext = dbContext;
            _entityType = entityType;
            _entityMetadataProvider = entityMetadataProvider;
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <returns></returns>
        public async Task<object> FirstOrDefaultAsync()
        {
            return (await GetQuery().ToListAsync()).FirstOrDefault();
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <returns></returns>
        public override async Task<TElement> FirstOrDefaultAsync<TElement>()
        {
            var elemType = typeof(TElement);
            if (_entityType == null && _entityMetadataProvider.Contains(elemType))
                _entityType = elemType;
            if (_entityType == elemType)
                return (TElement)(await GetQuery().ToListAsync()).FirstOrDefault();

            return (await FirstOrDefaultAsync<TElement>(null));
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public async Task<object> FirstOrDefaultAsync(string predicate, params object[] values)
        {
            return (await Where(predicate, values).ToListAsync()).FirstOrDefault();
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <returns></returns>
        public async Task<List<object>> ToListAsync()
        {
            return await GetQuery().ToListAsync();
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <returns></returns>
        public override async Task<List<TElement>> ToListAsync<TElement>()
        {
            var elemType = typeof(TElement);
            if (_entityType == null && _entityMetadataProvider.Contains(elemType))
                _entityType = elemType;
            if (_entityType == elemType)
                return (await GetQuery().ToListAsync()).Select(x => (TElement)x).ToList();

            return await ToListAsync<TElement>(null);
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public async Task<List<object>> ToListAsync(string predicate, params object[] values)
        {
            return await Where(predicate, values).ToListAsync();
        }

        /// <summary>
        /// Строит выражение where. См. описание у EF метода
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public IQueryBuilder Where(string predicate, params object[] values)
        {
            _dbQuery = GetQuery().Where(predicate, values);
            return this;
        }

        /// <summary>
        /// Строит выражение OrderBy. См. описание у EF метода 
        /// </summary>
        /// <param name="ordering"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public IQueryBuilder OrderBy(string ordering, params object[] values)
        {
            _dbQuery = GetQuery().OrderBy(ordering, values);
            return this;
        }

        /// <summary>
        /// Строит выражение Skip. Количество записей которые нужно пропустить
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public IQueryBuilder Skip(int count)
        {
            _dbQuery = GetQuery().Skip(count);
            return this;
        }

        /// <summary>
        /// Строит выражение Take. Количество записей которые нужно получить 
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public IQueryBuilder Take(int count)
        {
            _dbQuery = GetQuery().Take(count);
            return this;
        }

        /// <summary>
        /// Получает количество записей. См. описание у EF метода 
        /// </summary>
        /// <returns></returns>
        public Task<int> CountAsync()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Получает количество записей. См. описание у EF метода
        /// </summary>
        /// <param name="ordering"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public Task<int> CountAsync(string ordering, params object[] values)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Получает IQueryable объект. Если объект еще не был создан, то объект создается по типу сущности
        /// </summary>
        /// <returns></returns>
        private IQueryable GetQuery()
        {
            return _dbQuery = _dbQuery ?? (_dbQuery = _dbContext.Set(_entityType).AsNoTracking());
        }
    }
}
