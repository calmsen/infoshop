﻿using System.Collections.Generic;

namespace DataAccess.Infrastructure.QueryBuilding
{
    /// <summary>
    /// Представляет данные по свойству сущности (информация о поле в таблице)
    /// </summary>
    internal class EntityProperty
    {
        /// <summary>
        /// Название поля
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// Является ли основным ключом
        /// </summary>
        public bool IsPrimaryKey { get; set; }

        /// <summary>
        /// Генерируется ли основной ключ
        /// </summary>
        public bool IsStoreGeneratedIdentity { get; set; }

        /// <summary>
        /// Является ли частью комплексного типа
        /// </summary>
        public bool IsComplexType { get; set; }

        /// <summary>
        /// Все свойства с комплексным типом
        /// </summary>
        public List<EntityProperty> ComplexTypeProperties { get; set; }
    }
}
