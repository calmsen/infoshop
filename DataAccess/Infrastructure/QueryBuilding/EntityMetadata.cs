﻿using System.Collections.Generic;
using System.Linq;

namespace DataAccess.Infrastructure.QueryBuilding
{
    /// <summary>
    /// Класс описывает метаданные для сущности
    /// </summary>
    internal class EntityMetadata
    {
        /// <summary>
        /// Название таблицы 
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// Свойства сущности (информация о полях в таблице)
        /// </summary>
        public List<EntityProperty> Properties { get; set; }

        /// <summary>
        /// Список основных ключей сущности
        /// </summary>
        public List<EntityProperty> PrimaryKeys => _primaryKeys ?? (_primaryKeys = Properties.Where(x => x.IsPrimaryKey).ToList());

        /// <summary>
        /// Список основных ключей сущности
        /// </summary>
        private List<EntityProperty> _primaryKeys;

    }
}
