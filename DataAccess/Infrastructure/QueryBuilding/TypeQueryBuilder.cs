﻿using DomainLogic.Infrastructure.Utils;
using System.Data.Entity;

namespace DataAccess.Infrastructure.QueryBuilding
{
    /// <summary>
    /// Класс для формирования запроса в БД. Вся реализация представлена в базовом классе InnerTypeQueryBuilder<TEntity>. 
    /// Данный класс необходим только для того чтобы ограничить тип, чтобы вызвать SetQuery<TEntity>(). 
    /// Внутри класса InnerTypeQueryBuilder<TEntity> тип должен быть неограниченным.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    internal class QueryBuilder<TEntity> : InnerTypeQueryBuilder<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="entityMetadataProvider"></param>
        /// <param name="stringUtils"></param>
        /// <param name="enumUtils"></param>
        public QueryBuilder(DbContext dbContext, EntityMetadataProvider entityMetadataProvider, StringUtils stringUtils, EnumUtils enumUtils) 
            : base(dbContext, entityMetadataProvider, stringUtils, enumUtils)
        {
            SetQuery<TEntity>();
        }
    }
}
