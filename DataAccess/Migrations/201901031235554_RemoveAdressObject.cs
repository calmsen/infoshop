namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveAdressObject : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.AddressObjects", new[] { "FORMALNAME" });
            DropTable("dbo.AddressObjects");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.AddressObjects",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        AOGUID = c.String(maxLength: 36),
                        FORMALNAME = c.String(maxLength: 120),
                        REGIONCODE = c.String(maxLength: 2),
                        AUTOCODE = c.String(maxLength: 1),
                        AREACODE = c.String(maxLength: 3),
                        CITYCODE = c.String(maxLength: 3),
                        CTARCODE = c.String(maxLength: 3),
                        PLACECODE = c.String(maxLength: 3),
                        STREETCODE = c.String(maxLength: 4),
                        EXTRCODE = c.String(maxLength: 4),
                        SEXTCODE = c.String(maxLength: 3),
                        OFFNAME = c.String(maxLength: 120),
                        POSTALCODE = c.String(maxLength: 6),
                        IFNSFL = c.String(maxLength: 4),
                        TERRIFNSFL = c.String(maxLength: 4),
                        IFNSUL = c.String(maxLength: 4),
                        TERRIFNSUL = c.String(maxLength: 4),
                        OKATO = c.String(maxLength: 11),
                        OKTMO = c.String(maxLength: 11),
                        UPDATEDATE = c.DateTime(nullable: false),
                        SHORTNAME = c.String(maxLength: 10),
                        AOLEVEL = c.Int(nullable: false),
                        PARENTGUID = c.String(maxLength: 36),
                        AOID = c.String(maxLength: 36),
                        PREVID = c.String(maxLength: 36),
                        NEXTID = c.String(maxLength: 36),
                        CODE = c.String(maxLength: 17),
                        PLAINCODE = c.String(maxLength: 15),
                        ACTSTATUS = c.Int(nullable: false),
                        CENTSTATUS = c.Int(nullable: false),
                        OPERSTATUS = c.Int(nullable: false),
                        CURRSTATUS = c.Int(nullable: false),
                        STARTDATE = c.DateTime(nullable: false),
                        ENDDATE = c.DateTime(nullable: false),
                        NORMDOC = c.String(maxLength: 36),
                        LIVESTATUS = c.Int(nullable: false),
                        SYSADDRESS = c.String(maxLength: 1024),
                        ADDRESS = c.String(maxLength: 1024),
                        BUFPARENTGUID = c.String(maxLength: 36),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.AddressObjects", "FORMALNAME");
        }
    }
}
