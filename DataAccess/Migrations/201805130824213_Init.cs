namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Lat = c.String(maxLength: 16),
                        Lng = c.String(maxLength: 16),
                        Ip = c.String(maxLength: 16),
                        CountryId = c.Long(),
                        DistrictId = c.Long(),
                        RegionId = c.Long(),
                        CityId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.CityId)
                .ForeignKey("dbo.Countries", t => t.CountryId)
                .ForeignKey("dbo.Districts", t => t.DistrictId)
                .ForeignKey("dbo.Regions", t => t.RegionId)
                .Index(t => t.CountryId)
                .Index(t => t.DistrictId)
                .Index(t => t.RegionId)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(maxLength: 512),
                        Alias = c.String(maxLength: 512),
                        Lat = c.String(maxLength: 16),
                        Lng = c.String(maxLength: 16),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(maxLength: 512),
                        Alias = c.String(maxLength: 512),
                        Lat = c.String(maxLength: 16),
                        Lng = c.String(maxLength: 16),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Districts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(maxLength: 512),
                        Alias = c.String(maxLength: 512),
                        Lat = c.String(maxLength: 16),
                        Lng = c.String(maxLength: 16),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Regions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(maxLength: 512),
                        Alias = c.String(maxLength: 512),
                        Lat = c.String(maxLength: 16),
                        Lng = c.String(maxLength: 16),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AddressObjects",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        AOGUID = c.String(maxLength: 36),
                        FORMALNAME = c.String(maxLength: 120),
                        REGIONCODE = c.String(maxLength: 2),
                        AUTOCODE = c.String(maxLength: 1),
                        AREACODE = c.String(maxLength: 3),
                        CITYCODE = c.String(maxLength: 3),
                        CTARCODE = c.String(maxLength: 3),
                        PLACECODE = c.String(maxLength: 3),
                        STREETCODE = c.String(maxLength: 4),
                        EXTRCODE = c.String(maxLength: 4),
                        SEXTCODE = c.String(maxLength: 3),
                        OFFNAME = c.String(maxLength: 120),
                        POSTALCODE = c.String(maxLength: 6),
                        IFNSFL = c.String(maxLength: 4),
                        TERRIFNSFL = c.String(maxLength: 4),
                        IFNSUL = c.String(maxLength: 4),
                        TERRIFNSUL = c.String(maxLength: 4),
                        OKATO = c.String(maxLength: 11),
                        OKTMO = c.String(maxLength: 11),
                        UPDATEDATE = c.DateTime(nullable: false),
                        SHORTNAME = c.String(maxLength: 10),
                        AOLEVEL = c.Int(nullable: false),
                        PARENTGUID = c.String(maxLength: 36),
                        AOID = c.String(maxLength: 36),
                        PREVID = c.String(maxLength: 36),
                        NEXTID = c.String(maxLength: 36),
                        CODE = c.String(maxLength: 17),
                        PLAINCODE = c.String(maxLength: 15),
                        ACTSTATUS = c.Int(nullable: false),
                        CENTSTATUS = c.Int(nullable: false),
                        OPERSTATUS = c.Int(nullable: false),
                        CURRSTATUS = c.Int(nullable: false),
                        STARTDATE = c.DateTime(nullable: false),
                        ENDDATE = c.DateTime(nullable: false),
                        NORMDOC = c.String(maxLength: 36),
                        LIVESTATUS = c.Int(nullable: false),
                        SYSADDRESS = c.String(maxLength: 1024),
                        ADDRESS = c.String(maxLength: 1024),
                        BUFPARENTGUID = c.String(maxLength: 36),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.FORMALNAME);
            
            CreateTable(
                "dbo.Sections",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false, maxLength: 128),
                        TitleIn = c.String(maxLength: 128),
                        ProductsAmount = c.Int(nullable: false),
                        Hidden = c.Boolean(nullable: false),
                        Position = c.Int(nullable: false),
                        PathOfSections = c.Long(nullable: false),
                        ParentId = c.Long(nullable: false),
                        MainImageId = c.Long(),
                        FiltersGroupsAsXml = c.String(storeType: "xml"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SectionImages", t => t.MainImageId)
                .Index(t => t.MainImageId);
            
            CreateTable(
                "dbo.Faq",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(maxLength: 256),
                        TitleIn = c.String(maxLength: 256),
                        Question = c.String(nullable: false, maxLength: 512),
                        QuestionIn = c.String(maxLength: 1024),
                        Answer = c.String(nullable: false),
                        AnswerIn = c.String(),
                        Theme = c.Int(nullable: false),
                        OnlyManager = c.Boolean(nullable: false),
                        SectionId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sections", t => t.SectionId)
                .Index(t => t.SectionId);
            
            CreateTable(
                "dbo.ProductsToFaqs",
                c => new
                    {
                        ProductId = c.Long(nullable: false),
                        FaqId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductId, t.FaqId })
                .ForeignKey("dbo.Faq", t => t.FaqId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.FaqId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false, maxLength: 128),
                        TitleIn = c.String(maxLength: 128),
                        ShortFeatures = c.String(maxLength: 128),
                        MainImageK = c.Double(nullable: false),
                        Sertificate = c.String(maxLength: 256),
                        BannerPosition = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ToBanner = c.Boolean(nullable: false),
                        ToRating = c.Boolean(nullable: false),
                        AllowFiles = c.Boolean(nullable: false),
                        Published = c.Boolean(nullable: false),
                        Filled = c.Boolean(nullable: false),
                        Active = c.Boolean(nullable: false),
                        Archive = c.Boolean(nullable: false),
                        New = c.Boolean(nullable: false),
                        FilledFeatures = c.Int(),
                        Comment = c.String(maxLength: 512),
                        Price = c.Single(nullable: false),
                        ExternalLink = c.String(maxLength: 512),
                        Code = c.Long(),
                        Barcode = c.Long(),
                        StoreState = c.Long(),
                        ViewsAmount = c.Int(nullable: false),
                        SectionId = c.Long(nullable: false),
                        TypeId = c.Long(),
                        DescriptionId = c.Long(),
                        MainImageId = c.Long(),
                        BannerImageId = c.Long(),
                        BannerImageIdIn = c.Long(),
                        PartnersLinksOnProductJson = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Images", t => t.BannerImageId)
                .ForeignKey("dbo.Images", t => t.BannerImageIdIn)
                .ForeignKey("dbo.Descriptions", t => t.DescriptionId)
                .ForeignKey("dbo.Images", t => t.MainImageId)
                .ForeignKey("dbo.Sections", t => t.SectionId, cascadeDelete: true)
                .ForeignKey("dbo.ProductTypes", t => t.TypeId)
                .Index(t => t.SectionId)
                .Index(t => t.TypeId)
                .Index(t => t.DescriptionId)
                .Index(t => t.MainImageId)
                .Index(t => t.BannerImageId)
                .Index(t => t.BannerImageIdIn);
            
            CreateTable(
                "dbo.Attributes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ProductId = c.Long(nullable: false),
                        FilterId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Filters", t => t.FilterId)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => new { t.ProductId, t.FilterId }, unique: true, name: "IX_ProductIdAndFilterId");
            
            CreateTable(
                "dbo.Filters",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false, maxLength: 128),
                        TitleIn = c.String(maxLength: 128),
                        Type = c.Int(nullable: false),
                        Unit = c.String(maxLength: 32),
                        UnitIn = c.String(maxLength: 32),
                        UnitConverterIdForCatalog = c.Long(nullable: false),
                        UnitConverterIdForProduct = c.Long(nullable: false),
                        Multiple = c.Boolean(nullable: false),
                        UnitConvertersJson = c.String(maxLength: 2048),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FilterValues",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 256),
                        TitleIn = c.String(maxLength: 256),
                        ValueAsLong = c.Long(nullable: false),
                        ValueAsDouble = c.Double(nullable: false),
                        ValueSet1 = c.Long(),
                        ValueSet2 = c.Long(),
                        ValueSet3 = c.Long(),
                        ValueSet4 = c.Long(),
                        Position = c.Int(nullable: false),
                        FilterId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Filters", t => t.FilterId, cascadeDelete: true)
                .Index(t => t.FilterId);
            
            CreateTable(
                "dbo.ProductValues",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ValueId = c.Long(),
                        AttributeId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attributes", t => t.AttributeId, cascadeDelete: true)
                .ForeignKey("dbo.FilterValues", t => t.ValueId)
                .Index(t => t.ValueId)
                .Index(t => t.AttributeId);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        Title = c.String(maxLength: 512),
                        ExternalLink = c.String(maxLength: 512),
                        Position = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductsToImages",
                c => new
                    {
                        ProductId = c.Long(nullable: false),
                        ImageId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductId, t.ImageId })
                .ForeignKey("dbo.Images", t => t.ImageId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.ImageId);
            
            CreateTable(
                "dbo.Descriptions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Content = c.String(nullable: false),
                        ContentIn = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Feedbacks",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Theme = c.Int(nullable: false),
                        Message = c.String(nullable: false, maxLength: 2048),
                        CreatedDate = c.DateTime(nullable: false),
                        State = c.Int(nullable: false),
                        AnswersAmount = c.Int(nullable: false),
                        UrlReferer = c.String(maxLength: 1024),
                        UserId = c.Int(nullable: false),
                        UserIdWhoSetTheState = c.Int(nullable: false),
                        SectionId = c.Long(),
                        ProductId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId)
                .ForeignKey("dbo.Sections", t => t.SectionId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .ForeignKey("dbo.Users", t => t.UserIdWhoSetTheState)
                .Index(t => t.UserId)
                .Index(t => t.UserIdWhoSetTheState)
                .Index(t => t.SectionId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.FeedbackAnswers",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Message = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        NoticedAuthor = c.Boolean(nullable: false),
                        UserId = c.Int(nullable: false),
                        FeedbackId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Feedbacks", t => t.FeedbackId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.FeedbackId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(maxLength: 128),
                        FullName = c.String(maxLength: 128),
                        Subscription = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductSnapshots",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Snapshot = c.String(unicode: false, storeType: "text"),
                        CreatedDate = c.DateTime(nullable: false),
                        ProductId = c.Long(nullable: false),
                        UserId = c.Int(nullable: false),
                        ProductStates_AsInt = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.ProductId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ProductStates",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        State = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        ProductId = c.Long(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.ProductId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.FeedbackInvitings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Emails = c.String(),
                        FeedbackId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Feedbacks", t => t.FeedbackId, cascadeDelete: true)
                .Index(t => t.FeedbackId);
            
            CreateTable(
                "dbo.FeedbacksToImages",
                c => new
                    {
                        FeedbackId = c.Long(nullable: false),
                        ImageId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.FeedbackId, t.ImageId })
                .ForeignKey("dbo.Feedbacks", t => t.FeedbackId, cascadeDelete: true)
                .ForeignKey("dbo.FeedbackImages", t => t.ImageId, cascadeDelete: true)
                .Index(t => t.FeedbackId)
                .Index(t => t.ImageId);
            
            CreateTable(
                "dbo.FeedbackImages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        Title = c.String(maxLength: 512),
                        ExternalLink = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FeedbacksToFeedbackTags",
                c => new
                    {
                        FeedbackId = c.Long(nullable: false),
                        TagId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.FeedbackId, t.TagId })
                .ForeignKey("dbo.Feedbacks", t => t.FeedbackId, cascadeDelete: true)
                .ForeignKey("dbo.FeedbackTags", t => t.TagId, cascadeDelete: true)
                .Index(t => t.FeedbackId)
                .Index(t => t.TagId);
            
            CreateTable(
                "dbo.FeedbackTags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductsToFiles",
                c => new
                    {
                        ProductId = c.Long(nullable: false),
                        FileId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductId, t.FileId })
                .ForeignKey("dbo.Files", t => t.FileId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.FileId);
            
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false, maxLength: 128),
                        TitleIn = c.String(maxLength: 128),
                        Description = c.String(maxLength: 2048),
                        DescriptionIn = c.String(maxLength: 2048),
                        Version = c.String(maxLength: 32),
                        FileSize = c.String(nullable: false, maxLength: 32),
                        CreatedDate = c.DateTime(nullable: false),
                        ExternalLink = c.String(maxLength: 512),
                        DisplayWithSameSection = c.Boolean(nullable: false),
                        DisplayWithSamePlatform = c.Boolean(nullable: false),
                        Filters_AsInt = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductsToNews",
                c => new
                    {
                        ProductId = c.Long(nullable: false),
                        ArticleId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductId, t.ArticleId })
                .ForeignKey("dbo.News", t => t.ArticleId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.ArticleId);
            
            CreateTable(
                "dbo.News",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false, maxLength: 128),
                        TitleIn = c.String(maxLength: 128),
                        Annottion = c.String(nullable: false, maxLength: 1024),
                        AnnottionIn = c.String(maxLength: 1024),
                        CreatedDate = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                        Position = c.Int(nullable: false),
                        DescriptionId = c.Long(),
                        MainImageId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ArticleDescriptions", t => t.DescriptionId)
                .ForeignKey("dbo.ArticleImages", t => t.MainImageId)
                .Index(t => t.DescriptionId)
                .Index(t => t.MainImageId);
            
            CreateTable(
                "dbo.ArticleDescriptions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Content = c.String(nullable: false),
                        ContentIn = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ArticleImages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        Title = c.String(maxLength: 512),
                        ExternalLink = c.String(maxLength: 512),
                        Position = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NewsToImages",
                c => new
                    {
                        ArticleId = c.Long(nullable: false),
                        ImageId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.ArticleId, t.ImageId })
                .ForeignKey("dbo.News", t => t.ArticleId, cascadeDelete: true)
                .ForeignKey("dbo.ArticleImages", t => t.ImageId, cascadeDelete: true)
                .Index(t => t.ArticleId)
                .Index(t => t.ImageId);
            
            CreateTable(
                "dbo.ProductsToPosts",
                c => new
                    {
                        ProductId = c.Long(nullable: false),
                        PostId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductId, t.PostId })
                .ForeignKey("dbo.Posts", t => t.PostId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.PostId);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false, maxLength: 128),
                        TitleIn = c.String(maxLength: 128),
                        Year = c.Int(nullable: false),
                        Annottion = c.String(maxLength: 1024),
                        AnnottionIn = c.String(maxLength: 1024),
                        LinkToExpertPost = c.String(maxLength: 128),
                        CreatedDate = c.DateTime(nullable: false),
                        Activate = c.Boolean(nullable: false),
                        DescriptionId = c.Long(),
                        MainImageId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PostDescriptions", t => t.DescriptionId)
                .ForeignKey("dbo.PostImages", t => t.MainImageId)
                .Index(t => t.DescriptionId)
                .Index(t => t.MainImageId);
            
            CreateTable(
                "dbo.PostDescriptions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Content = c.String(nullable: false),
                        ContentIn = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PostImages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        Title = c.String(maxLength: 512),
                        ExternalLink = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductTypes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        TitleIn = c.String(),
                        SectionId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sections", t => t.SectionId)
                .Index(t => t.SectionId);
            
            CreateTable(
                "dbo.SectionImages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        Title = c.String(maxLength: 256),
                        ExternalLink = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SectionsToImages",
                c => new
                    {
                        SectionId = c.Long(nullable: false),
                        ImageId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.SectionId, t.ImageId })
                .ForeignKey("dbo.SectionImages", t => t.ImageId, cascadeDelete: true)
                .ForeignKey("dbo.Sections", t => t.SectionId, cascadeDelete: true)
                .Index(t => t.SectionId)
                .Index(t => t.ImageId);
            
            CreateTable(
                "dbo.SupportArticles",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false, maxLength: 128),
                        TitleIn = c.String(maxLength: 128),
                        Annottion = c.String(nullable: false, maxLength: 1024),
                        AnnottionIn = c.String(maxLength: 1024),
                        CreatedDate = c.DateTime(nullable: false),
                        DescriptionId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SupportArticleDescriptions", t => t.DescriptionId)
                .Index(t => t.DescriptionId);
            
            CreateTable(
                "dbo.SupportArticleDescriptions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Content = c.String(nullable: false),
                        ContentIn = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Partners",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false, maxLength: 128),
                        TitleIn = c.String(maxLength: 128),
                        LinkToPartner = c.String(nullable: false, maxLength: 512),
                        Position = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        MainImageId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PartnerImages", t => t.MainImageId)
                .Index(t => t.MainImageId);
            
            CreateTable(
                "dbo.PartnerImages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        Title = c.String(maxLength: 512),
                        ExternalLink = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Products1c",
                c => new
                    {
                        Code = c.Long(nullable: false),
                        Id = c.String(maxLength: 36),
                        Name = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false),
                        ShortFeatures = c.String(),
                        State = c.Int(nullable: false),
                        Price = c.Single(nullable: false),
                        Active = c.Boolean(nullable: false),
                        Barcode = c.Long(),
                        DateOfChange = c.DateTime(),
                        TypeId = c.String(nullable: false, maxLength: 128),
                        ProductId = c.Long(),
                    })
                .PrimaryKey(t => t.Code)
                .ForeignKey("dbo.Products", t => t.ProductId)
                .ForeignKey("dbo.ProductTypes1c", t => t.TypeId)
                .Index(t => t.Id)
                .Index(t => t.TypeId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.ProductTypes1c",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false),
                        State = c.Int(nullable: false),
                        SectionId = c.String(maxLength: 128),
                        TypeId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sections1c", t => t.SectionId)
                .ForeignKey("dbo.ProductTypes", t => t.TypeId)
                .Index(t => t.SectionId)
                .Index(t => t.TypeId);
            
            CreateTable(
                "dbo.Sections1c",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false, maxLength: 128),
                        TitleIn = c.String(maxLength: 128),
                        State = c.Int(nullable: false),
                        SectionId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sections", t => t.SectionId)
                .Index(t => t.SectionId);
            
            CreateTable(
                "dbo.FilterGroups1c",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Title = c.String(),
                        TitleIn = c.String(),
                        State = c.Int(nullable: false),
                        SectionId = c.String(nullable: false, maxLength: 128),
                        GroupId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sections1c", t => t.SectionId, cascadeDelete: true)
                .Index(t => t.SectionId);
            
            CreateTable(
                "dbo.Filters1c",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false, maxLength: 128),
                        TitleIn = c.String(maxLength: 128),
                        Unit = c.String(maxLength: 32),
                        UnitIn = c.String(maxLength: 32),
                        Multiple = c.Boolean(nullable: false),
                        State = c.Int(nullable: false),
                        DateOfChange = c.DateTime(),
                        Type = c.Int(nullable: false),
                        GroupId = c.String(nullable: false, maxLength: 128),
                        FilterId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Filters", t => t.FilterId)
                .ForeignKey("dbo.FilterGroups1c", t => t.GroupId, cascadeDelete: true)
                .Index(t => t.GroupId)
                .Index(t => t.FilterId);
            
            CreateTable(
                "dbo.FilterValues1c",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false, maxLength: 256),
                        TitleIn = c.String(maxLength: 256),
                        ValueAsDouble = c.Double(),
                        ValueAsLong = c.Long(),
                        State = c.Int(nullable: false),
                        DateOfChange = c.DateTime(),
                        FilterId = c.String(nullable: false, maxLength: 128),
                        ValueId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Filters1c", t => t.FilterId, cascadeDelete: true)
                .ForeignKey("dbo.FilterValues", t => t.ValueId)
                .Index(t => t.FilterId)
                .Index(t => t.ValueId);
            
            CreateTable(
                "dbo.Pages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false, maxLength: 128),
                        TitleIn = c.String(maxLength: 128),
                        CreatedDate = c.DateTime(nullable: false),
                        Layout = c.String(maxLength: 512),
                        NavLinkName = c.String(maxLength: 128),
                        DescriptionId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PageDescriptions", t => t.DescriptionId)
                .Index(t => t.DescriptionId);
            
            CreateTable(
                "dbo.PageDescriptions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Content = c.String(nullable: false),
                        ContentIn = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Vacancies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false, maxLength: 128),
                        Address = c.String(maxLength: 256),
                        Employment = c.String(maxLength: 256),
                        Duties = c.String(maxLength: 2048),
                        Education = c.String(maxLength: 256),
                        Experience = c.String(maxLength: 128),
                        AdditionalRequirements = c.String(maxLength: 2048),
                        Salary = c.String(maxLength: 128),
                        Email = c.String(maxLength: 128),
                        WorkingConditions = c.String(),
                        State = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Services",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Country = c.String(maxLength: 128),
                        City = c.String(maxLength: 128),
                        Address = c.String(maxLength: 128),
                        Phones = c.String(maxLength: 128),
                        WorkingHours = c.String(maxLength: 512),
                        Type = c.Int(nullable: false),
                        Guid = c.String(maxLength: 36),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Resumes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false, maxLength: 128),
                        FullName = c.String(nullable: false, maxLength: 128),
                        Phone = c.String(nullable: false, maxLength: 32),
                        Email = c.String(nullable: false, maxLength: 128),
                        Education = c.String(maxLength: 1024),
                        Experience = c.String(),
                        AdditionalInfo = c.String(maxLength: 2048),
                        ResumeDocLink = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RequestCounter",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        RequestTime = c.DateTime(nullable: false),
                        RequestExcevuteTime = c.Int(nullable: false),
                        RequestUrl = c.String(maxLength: 1024),
                        CityId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.CityId)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.ProductValues1c",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        State = c.Int(nullable: false),
                        DateOfChange = c.DateTime(),
                        FilterId = c.String(maxLength: 36),
                        ProductId = c.String(maxLength: 36),
                        FilterValueId = c.String(maxLength: 36),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductCounters",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ViewsAmount = c.Int(nullable: false),
                        State = c.Int(nullable: false),
                        ProductId = c.Long(nullable: false),
                        CityId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.CityId)
                .Index(t => t.ProductId)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.PageImages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        Title = c.String(maxLength: 512),
                        ExternalLink = c.String(maxLength: 512),
                        Position = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Options",
                c => new
                    {
                        Name = c.String(nullable: false, maxLength: 128),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.Name);
            
            CreateTable(
                "dbo.Resources",
                c => new
                    {
                        Key = c.String(nullable: false, maxLength: 128),
                        Value = c.String(),
                        ValueIn = c.String(),
                    })
                .PrimaryKey(t => t.Key);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductCounters", "CityId", "dbo.Cities");
            DropForeignKey("dbo.RequestCounter", "CityId", "dbo.Cities");
            DropForeignKey("dbo.Pages", "DescriptionId", "dbo.PageDescriptions");
            DropForeignKey("dbo.Products1c", "TypeId", "dbo.ProductTypes1c");
            DropForeignKey("dbo.ProductTypes1c", "TypeId", "dbo.ProductTypes");
            DropForeignKey("dbo.ProductTypes1c", "SectionId", "dbo.Sections1c");
            DropForeignKey("dbo.Sections1c", "SectionId", "dbo.Sections");
            DropForeignKey("dbo.FilterGroups1c", "SectionId", "dbo.Sections1c");
            DropForeignKey("dbo.FilterValues1c", "ValueId", "dbo.FilterValues");
            DropForeignKey("dbo.FilterValues1c", "FilterId", "dbo.Filters1c");
            DropForeignKey("dbo.Filters1c", "GroupId", "dbo.FilterGroups1c");
            DropForeignKey("dbo.Filters1c", "FilterId", "dbo.Filters");
            DropForeignKey("dbo.Products1c", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Partners", "MainImageId", "dbo.PartnerImages");
            DropForeignKey("dbo.SupportArticles", "DescriptionId", "dbo.SupportArticleDescriptions");
            DropForeignKey("dbo.Sections", "MainImageId", "dbo.SectionImages");
            DropForeignKey("dbo.SectionsToImages", "SectionId", "dbo.Sections");
            DropForeignKey("dbo.SectionsToImages", "ImageId", "dbo.SectionImages");
            DropForeignKey("dbo.Faq", "SectionId", "dbo.Sections");
            DropForeignKey("dbo.ProductTypes", "SectionId", "dbo.Sections");
            DropForeignKey("dbo.Products", "TypeId", "dbo.ProductTypes");
            DropForeignKey("dbo.Products", "SectionId", "dbo.Sections");
            DropForeignKey("dbo.ProductsToPosts", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductsToPosts", "PostId", "dbo.Posts");
            DropForeignKey("dbo.Posts", "MainImageId", "dbo.PostImages");
            DropForeignKey("dbo.Posts", "DescriptionId", "dbo.PostDescriptions");
            DropForeignKey("dbo.ProductsToNews", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductsToNews", "ArticleId", "dbo.News");
            DropForeignKey("dbo.News", "MainImageId", "dbo.ArticleImages");
            DropForeignKey("dbo.NewsToImages", "ImageId", "dbo.ArticleImages");
            DropForeignKey("dbo.NewsToImages", "ArticleId", "dbo.News");
            DropForeignKey("dbo.News", "DescriptionId", "dbo.ArticleDescriptions");
            DropForeignKey("dbo.ProductsToFiles", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductsToFiles", "FileId", "dbo.Files");
            DropForeignKey("dbo.ProductsToFaqs", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Products", "MainImageId", "dbo.Images");
            DropForeignKey("dbo.Feedbacks", "UserIdWhoSetTheState", "dbo.Users");
            DropForeignKey("dbo.Feedbacks", "UserId", "dbo.Users");
            DropForeignKey("dbo.Feedbacks", "SectionId", "dbo.Sections");
            DropForeignKey("dbo.Feedbacks", "ProductId", "dbo.Products");
            DropForeignKey("dbo.FeedbacksToFeedbackTags", "TagId", "dbo.FeedbackTags");
            DropForeignKey("dbo.FeedbacksToFeedbackTags", "FeedbackId", "dbo.Feedbacks");
            DropForeignKey("dbo.FeedbacksToImages", "ImageId", "dbo.FeedbackImages");
            DropForeignKey("dbo.FeedbacksToImages", "FeedbackId", "dbo.Feedbacks");
            DropForeignKey("dbo.FeedbackInvitings", "FeedbackId", "dbo.Feedbacks");
            DropForeignKey("dbo.FeedbackAnswers", "UserId", "dbo.Users");
            DropForeignKey("dbo.ProductStates", "UserId", "dbo.Users");
            DropForeignKey("dbo.ProductStates", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductSnapshots", "UserId", "dbo.Users");
            DropForeignKey("dbo.ProductSnapshots", "ProductId", "dbo.Products");
            DropForeignKey("dbo.FeedbackAnswers", "FeedbackId", "dbo.Feedbacks");
            DropForeignKey("dbo.Products", "DescriptionId", "dbo.Descriptions");
            DropForeignKey("dbo.Products", "BannerImageIdIn", "dbo.Images");
            DropForeignKey("dbo.Products", "BannerImageId", "dbo.Images");
            DropForeignKey("dbo.ProductsToImages", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductsToImages", "ImageId", "dbo.Images");
            DropForeignKey("dbo.Attributes", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Attributes", "FilterId", "dbo.Filters");
            DropForeignKey("dbo.ProductValues", "ValueId", "dbo.FilterValues");
            DropForeignKey("dbo.ProductValues", "AttributeId", "dbo.Attributes");
            DropForeignKey("dbo.FilterValues", "FilterId", "dbo.Filters");
            DropForeignKey("dbo.ProductsToFaqs", "FaqId", "dbo.Faq");
            DropForeignKey("dbo.Addresses", "RegionId", "dbo.Regions");
            DropForeignKey("dbo.Addresses", "DistrictId", "dbo.Districts");
            DropForeignKey("dbo.Addresses", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.Addresses", "CityId", "dbo.Cities");
            DropIndex("dbo.ProductCounters", new[] { "CityId" });
            DropIndex("dbo.ProductCounters", new[] { "ProductId" });
            DropIndex("dbo.RequestCounter", new[] { "CityId" });
            DropIndex("dbo.Pages", new[] { "DescriptionId" });
            DropIndex("dbo.FilterValues1c", new[] { "ValueId" });
            DropIndex("dbo.FilterValues1c", new[] { "FilterId" });
            DropIndex("dbo.Filters1c", new[] { "FilterId" });
            DropIndex("dbo.Filters1c", new[] { "GroupId" });
            DropIndex("dbo.FilterGroups1c", new[] { "SectionId" });
            DropIndex("dbo.Sections1c", new[] { "SectionId" });
            DropIndex("dbo.ProductTypes1c", new[] { "TypeId" });
            DropIndex("dbo.ProductTypes1c", new[] { "SectionId" });
            DropIndex("dbo.Products1c", new[] { "ProductId" });
            DropIndex("dbo.Products1c", new[] { "TypeId" });
            DropIndex("dbo.Products1c", new[] { "Id" });
            DropIndex("dbo.Partners", new[] { "MainImageId" });
            DropIndex("dbo.SupportArticles", new[] { "DescriptionId" });
            DropIndex("dbo.SectionsToImages", new[] { "ImageId" });
            DropIndex("dbo.SectionsToImages", new[] { "SectionId" });
            DropIndex("dbo.ProductTypes", new[] { "SectionId" });
            DropIndex("dbo.Posts", new[] { "MainImageId" });
            DropIndex("dbo.Posts", new[] { "DescriptionId" });
            DropIndex("dbo.ProductsToPosts", new[] { "PostId" });
            DropIndex("dbo.ProductsToPosts", new[] { "ProductId" });
            DropIndex("dbo.NewsToImages", new[] { "ImageId" });
            DropIndex("dbo.NewsToImages", new[] { "ArticleId" });
            DropIndex("dbo.News", new[] { "MainImageId" });
            DropIndex("dbo.News", new[] { "DescriptionId" });
            DropIndex("dbo.ProductsToNews", new[] { "ArticleId" });
            DropIndex("dbo.ProductsToNews", new[] { "ProductId" });
            DropIndex("dbo.ProductsToFiles", new[] { "FileId" });
            DropIndex("dbo.ProductsToFiles", new[] { "ProductId" });
            DropIndex("dbo.FeedbacksToFeedbackTags", new[] { "TagId" });
            DropIndex("dbo.FeedbacksToFeedbackTags", new[] { "FeedbackId" });
            DropIndex("dbo.FeedbacksToImages", new[] { "ImageId" });
            DropIndex("dbo.FeedbacksToImages", new[] { "FeedbackId" });
            DropIndex("dbo.FeedbackInvitings", new[] { "FeedbackId" });
            DropIndex("dbo.ProductStates", new[] { "UserId" });
            DropIndex("dbo.ProductStates", new[] { "ProductId" });
            DropIndex("dbo.ProductSnapshots", new[] { "UserId" });
            DropIndex("dbo.ProductSnapshots", new[] { "ProductId" });
            DropIndex("dbo.FeedbackAnswers", new[] { "FeedbackId" });
            DropIndex("dbo.FeedbackAnswers", new[] { "UserId" });
            DropIndex("dbo.Feedbacks", new[] { "ProductId" });
            DropIndex("dbo.Feedbacks", new[] { "SectionId" });
            DropIndex("dbo.Feedbacks", new[] { "UserIdWhoSetTheState" });
            DropIndex("dbo.Feedbacks", new[] { "UserId" });
            DropIndex("dbo.ProductsToImages", new[] { "ImageId" });
            DropIndex("dbo.ProductsToImages", new[] { "ProductId" });
            DropIndex("dbo.ProductValues", new[] { "AttributeId" });
            DropIndex("dbo.ProductValues", new[] { "ValueId" });
            DropIndex("dbo.FilterValues", new[] { "FilterId" });
            DropIndex("dbo.Attributes", "IX_ProductIdAndFilterId");
            DropIndex("dbo.Products", new[] { "BannerImageIdIn" });
            DropIndex("dbo.Products", new[] { "BannerImageId" });
            DropIndex("dbo.Products", new[] { "MainImageId" });
            DropIndex("dbo.Products", new[] { "DescriptionId" });
            DropIndex("dbo.Products", new[] { "TypeId" });
            DropIndex("dbo.Products", new[] { "SectionId" });
            DropIndex("dbo.ProductsToFaqs", new[] { "FaqId" });
            DropIndex("dbo.ProductsToFaqs", new[] { "ProductId" });
            DropIndex("dbo.Faq", new[] { "SectionId" });
            DropIndex("dbo.Sections", new[] { "MainImageId" });
            DropIndex("dbo.AddressObjects", new[] { "FORMALNAME" });
            DropIndex("dbo.Addresses", new[] { "CityId" });
            DropIndex("dbo.Addresses", new[] { "RegionId" });
            DropIndex("dbo.Addresses", new[] { "DistrictId" });
            DropIndex("dbo.Addresses", new[] { "CountryId" });
            DropTable("dbo.Resources");
            DropTable("dbo.Options");
            DropTable("dbo.PageImages");
            DropTable("dbo.ProductCounters");
            DropTable("dbo.ProductValues1c");
            DropTable("dbo.RequestCounter");
            DropTable("dbo.Resumes");
            DropTable("dbo.Services");
            DropTable("dbo.Vacancies");
            DropTable("dbo.PageDescriptions");
            DropTable("dbo.Pages");
            DropTable("dbo.FilterValues1c");
            DropTable("dbo.Filters1c");
            DropTable("dbo.FilterGroups1c");
            DropTable("dbo.Sections1c");
            DropTable("dbo.ProductTypes1c");
            DropTable("dbo.Products1c");
            DropTable("dbo.PartnerImages");
            DropTable("dbo.Partners");
            DropTable("dbo.SupportArticleDescriptions");
            DropTable("dbo.SupportArticles");
            DropTable("dbo.SectionsToImages");
            DropTable("dbo.SectionImages");
            DropTable("dbo.ProductTypes");
            DropTable("dbo.PostImages");
            DropTable("dbo.PostDescriptions");
            DropTable("dbo.Posts");
            DropTable("dbo.ProductsToPosts");
            DropTable("dbo.NewsToImages");
            DropTable("dbo.ArticleImages");
            DropTable("dbo.ArticleDescriptions");
            DropTable("dbo.News");
            DropTable("dbo.ProductsToNews");
            DropTable("dbo.Files");
            DropTable("dbo.ProductsToFiles");
            DropTable("dbo.FeedbackTags");
            DropTable("dbo.FeedbacksToFeedbackTags");
            DropTable("dbo.FeedbackImages");
            DropTable("dbo.FeedbacksToImages");
            DropTable("dbo.FeedbackInvitings");
            DropTable("dbo.ProductStates");
            DropTable("dbo.ProductSnapshots");
            DropTable("dbo.Users");
            DropTable("dbo.FeedbackAnswers");
            DropTable("dbo.Feedbacks");
            DropTable("dbo.Descriptions");
            DropTable("dbo.ProductsToImages");
            DropTable("dbo.Images");
            DropTable("dbo.ProductValues");
            DropTable("dbo.FilterValues");
            DropTable("dbo.Filters");
            DropTable("dbo.Attributes");
            DropTable("dbo.Products");
            DropTable("dbo.ProductsToFaqs");
            DropTable("dbo.Faq");
            DropTable("dbo.Sections");
            DropTable("dbo.AddressObjects");
            DropTable("dbo.Regions");
            DropTable("dbo.Districts");
            DropTable("dbo.Countries");
            DropTable("dbo.Cities");
            DropTable("dbo.Addresses");
        }
    }
}
