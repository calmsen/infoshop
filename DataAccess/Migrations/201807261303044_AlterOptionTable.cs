namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterOptionTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Options", "Title", c => c.String());
            AddColumn("dbo.Options", "Type", c => c.Int(nullable: false));
            AddColumn("dbo.Options", "IsSystem", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Options", "IsSystem");
            DropColumn("dbo.Options", "Type");
            DropColumn("dbo.Options", "Title");
        }
    }
}
