namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOptions : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Products1c", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Filters1c", "FilterId", "dbo.Filters");
            DropForeignKey("dbo.Filters1c", "GroupId", "dbo.FilterGroups1c");
            DropForeignKey("dbo.FilterValues1c", "FilterId", "dbo.Filters1c");
            DropForeignKey("dbo.FilterValues1c", "ValueId", "dbo.FilterValues");
            DropForeignKey("dbo.FilterGroups1c", "SectionId", "dbo.Sections1c");
            DropForeignKey("dbo.Sections1c", "SectionId", "dbo.Sections");
            DropForeignKey("dbo.ProductTypes1c", "SectionId", "dbo.Sections1c");
            DropForeignKey("dbo.ProductTypes1c", "TypeId", "dbo.ProductTypes");
            DropForeignKey("dbo.Products1c", "TypeId", "dbo.ProductTypes1c");
            DropIndex("dbo.Products1c", new[] { "Id" });
            DropIndex("dbo.Products1c", new[] { "TypeId" });
            DropIndex("dbo.Products1c", new[] { "ProductId" });
            DropIndex("dbo.ProductTypes1c", new[] { "SectionId" });
            DropIndex("dbo.ProductTypes1c", new[] { "TypeId" });
            DropIndex("dbo.Sections1c", new[] { "SectionId" });
            DropIndex("dbo.FilterGroups1c", new[] { "SectionId" });
            DropIndex("dbo.Filters1c", new[] { "GroupId" });
            DropIndex("dbo.Filters1c", new[] { "FilterId" });
            DropIndex("dbo.FilterValues1c", new[] { "FilterId" });
            DropIndex("dbo.FilterValues1c", new[] { "ValueId" });
            DropTable("dbo.Products1c");
            DropTable("dbo.ProductTypes1c");
            DropTable("dbo.Sections1c");
            DropTable("dbo.FilterGroups1c");
            DropTable("dbo.Filters1c");
            DropTable("dbo.FilterValues1c");
            DropTable("dbo.ProductValues1c");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ProductValues1c",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        State = c.Int(nullable: false),
                        DateOfChange = c.DateTime(),
                        FilterId = c.String(maxLength: 36),
                        ProductId = c.String(maxLength: 36),
                        FilterValueId = c.String(maxLength: 36),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FilterValues1c",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false, maxLength: 256),
                        TitleIn = c.String(maxLength: 256),
                        ValueAsDouble = c.Double(),
                        ValueAsLong = c.Long(),
                        State = c.Int(nullable: false),
                        DateOfChange = c.DateTime(),
                        FilterId = c.String(nullable: false, maxLength: 128),
                        ValueId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Filters1c",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false, maxLength: 128),
                        TitleIn = c.String(maxLength: 128),
                        Unit = c.String(maxLength: 32),
                        UnitIn = c.String(maxLength: 32),
                        Multiple = c.Boolean(nullable: false),
                        State = c.Int(nullable: false),
                        DateOfChange = c.DateTime(),
                        Type = c.Int(nullable: false),
                        GroupId = c.String(nullable: false, maxLength: 128),
                        FilterId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FilterGroups1c",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Title = c.String(),
                        TitleIn = c.String(),
                        State = c.Int(nullable: false),
                        SectionId = c.String(nullable: false, maxLength: 128),
                        GroupId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sections1c",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false, maxLength: 128),
                        TitleIn = c.String(maxLength: 128),
                        State = c.Int(nullable: false),
                        SectionId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductTypes1c",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false),
                        State = c.Int(nullable: false),
                        SectionId = c.String(maxLength: 128),
                        TypeId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Products1c",
                c => new
                    {
                        Code = c.Long(nullable: false),
                        Id = c.String(maxLength: 36),
                        Name = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false),
                        ShortFeatures = c.String(),
                        State = c.Int(nullable: false),
                        Price = c.Single(nullable: false),
                        Active = c.Boolean(nullable: false),
                        Barcode = c.Long(),
                        DateOfChange = c.DateTime(),
                        TypeId = c.String(nullable: false, maxLength: 128),
                        ProductId = c.Long(),
                    })
                .PrimaryKey(t => t.Code);
            
            CreateIndex("dbo.FilterValues1c", "ValueId");
            CreateIndex("dbo.FilterValues1c", "FilterId");
            CreateIndex("dbo.Filters1c", "FilterId");
            CreateIndex("dbo.Filters1c", "GroupId");
            CreateIndex("dbo.FilterGroups1c", "SectionId");
            CreateIndex("dbo.Sections1c", "SectionId");
            CreateIndex("dbo.ProductTypes1c", "TypeId");
            CreateIndex("dbo.ProductTypes1c", "SectionId");
            CreateIndex("dbo.Products1c", "ProductId");
            CreateIndex("dbo.Products1c", "TypeId");
            CreateIndex("dbo.Products1c", "Id");
            AddForeignKey("dbo.Products1c", "TypeId", "dbo.ProductTypes1c", "Id");
            AddForeignKey("dbo.ProductTypes1c", "TypeId", "dbo.ProductTypes", "Id");
            AddForeignKey("dbo.ProductTypes1c", "SectionId", "dbo.Sections1c", "Id");
            AddForeignKey("dbo.Sections1c", "SectionId", "dbo.Sections", "Id");
            AddForeignKey("dbo.FilterGroups1c", "SectionId", "dbo.Sections1c", "Id", cascadeDelete: true);
            AddForeignKey("dbo.FilterValues1c", "ValueId", "dbo.FilterValues", "Id");
            AddForeignKey("dbo.FilterValues1c", "FilterId", "dbo.Filters1c", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Filters1c", "GroupId", "dbo.FilterGroups1c", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Filters1c", "FilterId", "dbo.Filters", "Id");
            AddForeignKey("dbo.Products1c", "ProductId", "dbo.Products", "Id");
        }
    }
}
