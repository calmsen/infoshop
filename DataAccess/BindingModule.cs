﻿using DataAccess.Infrastructure;
using DataAccess.Infrastructure.QueryBuilding;
using DataAccess.Interfaces;
using DataAccess.QueryMappers;
using DomainLogic.Infrastructure.ServiceContainers;
using DomainLogic.Interfaces.QueryBuilder;

namespace DataAccess
{
    public class BindingModule : IBindingModule
    {
        public void Load(IServiceContainer serviceContainer)
        {
            serviceContainer.Bind<TransactionHandler>();
            serviceContainer.Bind<EntityMetadataProvider>();

            serviceContainer.Bind<IDatabaseFactory, InfoShopDatabaseFactory>();
            serviceContainer.Bind<IQueryBuilderFactory, QueryBuilderFactory>();

            serviceContainer.BindInNamespaceOf<UsersQueryMapper>();
        }
    }
}
