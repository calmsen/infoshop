﻿using DomainLogic.Infrastructure;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Localization;
using DomainLogic.Infrastructure.Mappers;
using DomainLogic.Infrastructure.ServiceContainers;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Interfaces.Localization;
using DomainLogic.Interfaces.Services;
using DomainLogic.Localizers;
using DomainLogic.Models.Settings;
using DomainLogic.Services;
using DomainLogic.Services.Shared;
using DomainLogic.Services.Shared.Editors;
using DomainLogic.Services.Shared.Helpers;
using DomainLogic.Services.Shared.Providers;
using System;

namespace DomainLogic
{
    public class BindingModule : IBindingModule
    {
        public void Load(IServiceContainer serviceContainer)
        {
            serviceContainer.Bind<AggregateLocalizer>();
            serviceContainer.Bind<LocalizersProvider>();
            serviceContainer.Bind<ConfigValueParser>();
            serviceContainer.Bind<CollectionUtils>();
            serviceContainer.Bind<CommonUtils>();
            serviceContainer.Bind<DateUtils>();
            serviceContainer.Bind<EnumUtils>();
            serviceContainer.Bind<ExcelUtils>();
            serviceContainer.Bind<FileUtils>();
            serviceContainer.Bind<ReflectorUtils>();
            serviceContainer.Bind<SectionsUtils>();
            serviceContainer.Bind<StringUtils>();
            serviceContainer.Bind<HtmlSanitizer>();
            serviceContainer.Bind<AutoMapperDefaultProfile>();
            
            serviceContainer.Bind<IValueLocalizer, ValueLocalizer>();

            serviceContainer.BindInNamespaceOf<ArticleLocalizer>(BindableType.AllInterfaces);
            serviceContainer.BindInNamespaceOf<BaseService>(
                BindableType.DefaultInterface, 
                excluding: new Type[] {
                    typeof(ResourcesService),
                    typeof(SectionsService)
                }
            );
            serviceContainer.Bind<IResourcesService, ResourcesService>();
            //serviceContainer.Bind<IResourcesService, DomainLogic.Services.Cachable.ResourcesService>();
            serviceContainer.Bind<ISectionsService, SectionsService>();
            //serviceContainer.Bind<ISectionsService, DomainLogic.Services.Cachable.SectionsService>();

            serviceContainer.BindInNamespaceOf<EmailSender, UserRoleEditor, CityHelper, UsersProvider, AppSettings>(
                BindableType.DefaultInterface,
                excluding: new Type[] {
                    typeof(ResourcesService),
                    typeof(Credentials)
                }
            );

            serviceContainer.Bind<Credentials>();
        }
    }
}
