﻿
namespace DomainLogic.Infrastructure.Constants
{
    /// <summary>
    /// Содержит ключи для хранения данных в рабочем потоке
    /// </summary>
    public static class TLSKeys
    {
        /// <summary>
        /// Ключ для хранения контекста БД
        /// </summary>
        public const string DbContext = nameof(DbContext);

        /// <summary>
        /// Ключ для хранения данных для преобразования снимка товара
        /// </summary>
        public const string SnapshotFilterSimpleFormsForMapper = nameof(SnapshotFilterSimpleFormsForMapper);

        /// <summary>
        /// Ключ для хранения текущего пользователя
        /// </summary>
        public const string CurrentUser = nameof(CurrentUser);
    }
}
