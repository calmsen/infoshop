﻿
namespace DomainLogic.Infrastructure.Constants
{
    /// <summary>
    /// Класс содержит шаблоны регулярных выражений
    /// </summary>
    public static class Patterns
    {
        /// <summary>
        /// Регулярное выражение для проверки электронного адреса
        /// </summary>
        public const string Email = @"\w+[-.\w]+@\w+[-.\w]+\.\w+[-.\w]+";

        /// <summary>
        /// Регулярное выражение для проверки имени пользователя
        /// </summary>
        public const string UserName = @"[^.,!?#\/&*()':;+%^№$~`]+";
    }
}