﻿using DomainLogic.Interfaces.QueryBuilder;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DomainLogic.Infrastructure.QueryBuilder
{
    /// <summary>
    /// Абстрактный декоратор билдера запросов. 
    /// По сути проксирует все методы, для того чтобы в конкретных реализациях декоратора переопределять только необходимые методы.
    /// </summary>
    public abstract class AQueryBuilderDecorator<TEntity> : ABaseQueryBuilderDecorator, IQueryBuilder<TEntity>
    {
        /// <summary>
        /// Билдер запросов
        /// </summary>
        private readonly IQueryBuilder<TEntity> _queryBuilder;

        /// <summary>
        /// Фабрика для создания билдера запросов
        /// </summary>
        private readonly IQueryBuilderFactory _queryBuilderFactory;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="queryBuilder"></param>
        /// <param name="queryBuilderFactory"></param>
        public AQueryBuilderDecorator(IQueryBuilder<TEntity> queryBuilder, IQueryBuilderFactory queryBuilderFactory)
            : base(queryBuilder)
        {
            _queryBuilder = queryBuilder;
            _queryBuilderFactory = queryBuilderFactory;
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public virtual TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return _queryBuilder.FirstOrDefault(predicate);
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <returns></returns>
        public virtual  Task<TEntity> FirstOrDefaultAsync()
        {
            return _queryBuilder.FirstOrDefaultAsync();
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public virtual Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return _queryBuilder.FirstOrDefaultAsync(predicate);
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <returns></returns>
        public virtual List<TEntity> ToList()
        {
            return _queryBuilder.ToList();
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <returns></returns>
        public virtual Task<List<TEntity>> ToListAsync()
        {
            return _queryBuilder.ToListAsync();
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public virtual Task<List<TEntity>> ToListAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return _queryBuilder.ToListAsync(predicate);
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="selector"></param>
        /// <returns></returns>
        public virtual Task<List<TElement>> ToListAsync<TElement>(Expression<Func<TEntity, TElement>> selector)
        {
            return _queryBuilder.ToListAsync(selector);
        }

        /// <summary>
        /// Строит выражение Select. См. описание у EF метода
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="selector"></param>
        /// <returns></returns>
        public virtual IQueryBuilder<TElement> Select<TElement>(Expression<Func<TEntity, TElement>> selector)
        {
            var queryBuilder = _queryBuilder.Select(selector);
            return _queryBuilderFactory.Create(queryBuilder);
        }

        /// <summary>
        /// Строит выражение Join. См. описание у EF метода
        /// </summary>
        /// <typeparam name="TInner"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="inner"></param>
        /// <param name="outerKeySelector"></param>
        /// <param name="innerKeySelector"></param>
        /// <param name="resultSelector"></param>
        /// <returns></returns>
        public virtual IQueryBuilder<TElement> Join<TInner, TKey, TElement>(IEnumerable<TInner> inner, Expression<Func<TEntity, TKey>> outerKeySelector,
            Expression<Func<TInner, TKey>> innerKeySelector, Expression<Func<TEntity, TInner, TElement>> resultSelector)
             where TInner : class
        {
            var queryBuilder = _queryBuilder.Join(inner, outerKeySelector, innerKeySelector, resultSelector);
            return _queryBuilderFactory.Create(queryBuilder);
        }

        /// <summary>
        /// Строит выражение Include. См. описание у EF метода
        /// </summary>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public virtual IQueryBuilder<TEntity> Include<TProperty>(Expression<Func<TEntity, TProperty>> path)
        {
            _queryBuilder.Include(path);
            return this;
        }

        /// <summary>
        /// Строит выражение where. См. описание у EF метода
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public virtual IQueryBuilder<TEntity> Where(Expression<Func<TEntity, bool>> predicate)
        {
            _queryBuilder.Where(predicate);
            return this;
        }

        /// <summary>
        /// Строит выражение where. См. описание у EF метода
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public virtual IQueryBuilder<TEntity> Where(string predicate, params object[] values)
        {
            _queryBuilder.Where(predicate, values);
            return this;
        }

        /// <summary>
        /// Строит выражение OrderBy. См. описание у EF метода
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        public virtual IQueryBuilder<TEntity> OrderBy<TKey>(Expression<Func<TEntity, TKey>> keySelector)
        {
            _queryBuilder.OrderBy<TKey>(keySelector);
            return this;
        }

        /// <summary>
        /// Строит выражение OrderBy. См. описание у EF метода
        /// </summary>
        /// <param name="ordering"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public virtual IQueryBuilder<TEntity> OrderBy(string ordering, params object[] values)
        {
            _queryBuilder.OrderBy(ordering, values);
            return this;
        }

        /// <summary>
        /// Строит выражение OrderByDescending. См. описание у EF метода
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        public virtual IQueryBuilder<TEntity> OrderByDescending<TKey>(Expression<Func<TEntity, TKey>> keySelector)
        {
            _queryBuilder.OrderByDescending<TKey>(keySelector);
            return this;
        }

        /// <summary>
        /// Строит выражение Skip. Количество записей которые нужно пропустить
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public virtual IQueryBuilder<TEntity> Skip(int count)
        {
            _queryBuilder.Skip(count);
            return this;
        }

        /// <summary>
        /// Строит выражение Take. Количество записей которые нужно получить
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public virtual IQueryBuilder<TEntity> Take(int count)
        {
            _queryBuilder.Take(count);
            return this;
        }

        /// <summary>
        /// Строит выражение Distinct. См. описание у EF метода
        /// </summary>
        /// <returns></returns>
        public virtual IQueryBuilder<TEntity> Distinct()
        {
            _queryBuilder.Distinct();
            return this;
        }

        /// <summary>
        /// Получает количество записей. См. описание у EF метода 
        /// </summary>
        /// <returns></returns>
        public virtual Task<int> CountAsync()
        {
            return _queryBuilder.CountAsync();
        }

        /// <summary>
        /// Получает количество записей. См. описание у EF метода 
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public virtual Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return _queryBuilder.CountAsync(predicate);
        }
    }
}
