﻿using DomainLogic.Interfaces.QueryBuilder;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Infrastructure.QueryBuilder
{
    /// <summary>
    /// Абстрактный декоратор билдера запросов. 
    /// По сути проксирует все методы, для того чтобы в конкретных реализациях декоратора переопределять только необходимые методы.
    /// </summary>
    public abstract class AQueryBuilderDecorator : ABaseQueryBuilderDecorator, IQueryBuilder
    {
        /// <summary>
        /// Билдер запросов
        /// </summary>
        private readonly IQueryBuilder _queryBuilder;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="queryBuilder"></param>
        public AQueryBuilderDecorator(IQueryBuilder queryBuilder)
            : base(queryBuilder)
        {
            _queryBuilder = queryBuilder;
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <returns></returns>
        public virtual Task<object> FirstOrDefaultAsync()
        {
            return _queryBuilder.FirstOrDefaultAsync();
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public virtual Task<object> FirstOrDefaultAsync(string predicate, params object[] values)
        {
            return _queryBuilder.FirstOrDefaultAsync(predicate, values);
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <returns></returns>
        public virtual Task<List<object>> ToListAsync()
        {
            return _queryBuilder.ToListAsync();
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public virtual Task<List<object>> ToListAsync(string predicate, params object[] values)
        {
            return _queryBuilder.ToListAsync(predicate, values);
        }

        /// <summary>
        /// Строит выражение where. См. описание у EF метода
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public virtual IQueryBuilder Where(string predicate, params object[] values)
        {
            _queryBuilder.Where(predicate, values);
            return this;
        }

        /// <summary>
        /// Строит выражение OrderBy. См. описание у EF метода
        /// </summary>
        /// <param name="ordering"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public virtual IQueryBuilder OrderBy(string ordering, params object[] values)
        {
            _queryBuilder.OrderBy(ordering, values);
            return this;
        }

        /// <summary>
        /// Строит выражение Skip. Количество записей которые нужно пропустить
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public virtual IQueryBuilder Skip(int count)
        {
            _queryBuilder.Skip(count);
            return this;
        }

        /// <summary>
        /// Строит выражение Take. Количество записей которые нужно получить 
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public virtual IQueryBuilder Take(int count)
        {
            _queryBuilder.Take(count);
            return this;
        }

        /// <summary>
        /// Получает количество записей. См. описание у EF метода 
        /// </summary>
        /// <returns></returns>
        public virtual Task<int> CountAsync()
        {
            return _queryBuilder.CountAsync();
        }

        /// <summary>
        /// Получает количество записей. См. описание у EF метода 
        /// </summary>
        /// <param name="ordering"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public virtual Task<int> CountAsync(string ordering, params object[] values)
        {
            return _queryBuilder.CountAsync(ordering, values);
        }
    }
}
