﻿using DomainLogic.Interfaces.QueryBuilder;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Infrastructure.QueryBuilder
{
    /// <summary>
    /// Абстрактный декоратор базового билдера запросов. 
    /// По сути проксирует все методы, для того чтобы в конкретных реализациях декоратора переопределять только необходимые методы.
    /// </summary>
    public abstract class ABaseQueryBuilderDecorator : IBaseQueryBuilder
    {
        /// <summary>
        /// Базовый билдер запросов
        /// </summary>
        private readonly IBaseQueryBuilder _queryBuilder;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="queryBuilder"></param>
        public ABaseQueryBuilderDecorator(IBaseQueryBuilder queryBuilder)
        {
            _queryBuilder = queryBuilder;
        }

        /// <summary>
        /// Основной ключ сущности
        /// </summary>
        public virtual string PrimaryKey => _queryBuilder.PrimaryKey;

        /// <summary>
        /// Указывает что строку не нужно обрабатывать в sql запросе
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public virtual string DbRaw(string str)
        {
            return _queryBuilder.DbRaw(str);
        }

        /// <summary>
        /// Удаляет запись
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual Task DeleteAsync(object where)
        {
            return _queryBuilder.DeleteAsync(where);
        }

        /// <summary>
        /// Добавляет запись в таблицу, соответсвующей сущности
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual Task InsertAsync(object obj, object where = null)
        {
            return _queryBuilder.InsertAsync(obj, where);
        }

        /// <summary>
        /// Обновляет запись
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual Task UpdateAsync(object obj, object where = null)
        {
            return _queryBuilder.UpdateAsync(obj, where);
        }

        /// <summary>
        /// Выполняет sql запрос
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public virtual Task<int> ExecuteAsync(string sql, params object[] parameters)
        {
            return _queryBuilder.ExecuteAsync(sql, parameters);
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <param name="elementType"></param>
        /// <param name="fields"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual Task<object> FirstOrDefaultAsync(Type elementType, string[] fields = null, object where = null)
        {
            return _queryBuilder.FirstOrDefaultAsync(elementType, fields, where);
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="fields"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual Task<TElement> FirstOrDefaultAsync<TElement>(string[] fields, object where = null)
        {
            return _queryBuilder.FirstOrDefaultAsync<TElement>(fields, where);
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual Task<TElement> FirstOrDefaultAsync<TElement>(object where = null)
        {
            return _queryBuilder.FirstOrDefaultAsync<TElement>(where);
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <returns></returns>
        public virtual Task<TElement> FirstOrDefaultAsync<TElement>()
        {
            return _queryBuilder.FirstOrDefaultAsync<TElement>();
        }

        /// <summary>
        /// Выполняет sql запрос для получения одной записи
        /// </summary>
        /// <typeparam name="TTargetElement"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public virtual Task<TTargetElement> ToTargetItemAsync<TTargetElement>(string sql, params object[] parameters)
        {
            return _queryBuilder.ToTargetItemAsync<TTargetElement>(sql, parameters);
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <param name="elementType"></param>
        /// <param name="fields"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual Task<List<object>> ToListAsync(Type elementType, string[] fields = null, object where = null)
        {
            return _queryBuilder.ToListAsync(elementType, fields, where);
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="fields"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual Task<List<TElement>> ToListAsync<TElement>(string[] fields, object where = null)
        {
            return _queryBuilder.ToListAsync<TElement>(fields, where);
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual Task<List<TElement>> ToListAsync<TElement>(object where = null)
        {
            return _queryBuilder.ToListAsync<TElement>(where);
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <returns></returns>
        public virtual Task<List<TElement>> ToListAsync<TElement>()
        {
            return _queryBuilder.ToListAsync<TElement>();
        }

        /// <summary>
        /// Выполняет sql запрос для получения списка записей
        /// </summary>
        /// <typeparam name="TTargetElement"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public virtual Task<List<TTargetElement>> ToTargetListAsync<TTargetElement>(string sql, params object[] parameters)
        {
            return _queryBuilder.ToTargetListAsync<TTargetElement>(sql, parameters);
        }
    }
}
