﻿
namespace DomainLogic.Infrastructure.Paging
{
    /// <summary>
    /// Тип сортировки. Обычная или по EAV
    /// </summary>
    public enum SortType
    {
        /// <summary>
        /// обычная сортировка
        /// </summary>
        Field,

        /// <summary>
        /// сортировка по еав
        /// </summary>
        Filter
    }
}
