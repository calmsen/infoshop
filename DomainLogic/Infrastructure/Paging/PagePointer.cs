﻿using System.Collections.Generic;

namespace DomainLogic.Infrastructure.Paging
{

    /// <summary>
    /// Класс содержит данные об одном пункте постранички
    /// </summary>
    public class PagePointer
    {
        /// <summary>
        /// Числовое значение 
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// Отображаемое значение
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Отключена ли кнопка. Актуально для PrevPage и NextPage
        /// </summary>
        public bool Disabled { get; set; }

        /// <summary>
        /// Текущая страница
        /// </summary>
        public bool IsCurrenPage { get; set; }

        /// <summary>
        /// Совокупность всех параметров 
        /// </summary>
        public Dictionary<string, object> RouteValues { get; set; }
    }
}
