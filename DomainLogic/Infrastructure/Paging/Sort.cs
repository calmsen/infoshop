﻿
namespace DomainLogic.Infrastructure.Paging
{
    /// <summary>
    /// Класс содержит данные о сортировке
    /// </summary>
    public class Sort
    {
        /// <summary>
        /// Тип сортировки
        /// </summary>
        public SortType Type { get; set; }

        /// <summary>
        /// Направление сортировки
        /// </summary>
        public OrderType Order { get; set; }

        /// <summary>
        /// Название поля по которому необходимо сортировать
        /// </summary>
        public string ColumnName { get; set; }

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        public Sort ()
        {
            Type = SortType.Field;
            Order = OrderType.Asc;
            ColumnName = "Id";
        }
    }
    
    
}