﻿using System.Collections.Generic;

namespace DomainLogic.Infrastructure.Paging
{
    /// <summary>
    /// Данный класс описывает неопределенный входной get параметр
    /// </summary>
    public class FilterParam
    {
        /// <summary>
        /// Название параметра
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Введенные значения параметра
        /// </summary>
        public List<long> Values { get; set; }

        /// <summary>
        /// Уникальный идентификатор параметра
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Тип параметра
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// Возможность множественного заполнения
        /// </summary>
        public bool Multiple { get; set; }  
        
        /// <summary>
        /// Отконвертированные значения
        /// </summary>
        public List<long> ValuesK { get; set; }
    }
}
