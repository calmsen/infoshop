﻿
namespace DomainLogic.Infrastructure.Paging
{
    /// <summary>
    /// Сортировка по убыванию и возрастанию
    /// </summary>
    public enum OrderType
    {
        /// <summary>
        /// По убыванию
        /// </summary>
        Desc,

        /// <summary>
        /// По возрастанию
        /// </summary>
        Asc
    }
}
