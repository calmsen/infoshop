﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;

namespace DomainLogic.Infrastructure.Paging
{  
    /// <summary>
    /// Класс для работы с пагинацией. Данный класс следует использовать только в UI layer
    /// </summary>                                
    public class Pagination
    {
        /// <summary>
        /// Номер текущей страницы
        /// </summary>
        public int CurrentPage { get; set; }

        /// <summary>
        /// Номер последней страницы
        /// </summary>
        public int LastPage { get; set; }

        /// <summary>
        /// Количество записей на странице
        /// </summary>
        public int PageItemsAmount { get; set; }

        /// <summary>
        /// Всего записей в бд
        /// </summary>
        public int ItemsAmount { get; set; }

        /// <summary>
        /// Количествл записей на текущей странице
        /// </summary>
        public int CurrentPageItemsAmount { get; set; }

        /// <summary>
        /// Номер предыдущей страницы относительно текущей
        /// </summary>
        public int PrevPage { get; set; }

        /// <summary>
        /// Номер следующей страницы относительно текущей           
        /// </summary>
        public int NextPage { get; set; }

        /// <summary>
        /// Нужно ли показывать первые и последни страницы
        /// </summary>
        public bool ShowLastAndFirstPages { get; set; }

        /// <summary>
        /// Cмещение записей относительно первой текущей записи. (CurrentPage - 1) * PageItemsAmount   
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// данные о страницах которые будут показаны
        /// </summary>
        public List<PagePointer> Pages { get; set; } 

        /// <summary>
        /// Get параметры
        /// </summary>
        public Dictionary<string, object> RouteValues { get; set; } 

        /// <summary>
        /// Инициазирует экземпляр класса MvcPagination
        /// </summary>
        public Pagination() 
            : base()
        {
            CurrentPage = 1;
            LastPage = 1;
            PageItemsAmount = 10;
            ItemsAmount = 0;
            CurrentPageItemsAmount = 0;
            PrevPage = 1;
            NextPage = 1;
            ShowLastAndFirstPages = true;

            Pages = new List<PagePointer>();            
            RouteValues = new Dictionary<string, object>();
            //
            ComputePages();
        }

        /// <summary>
        /// Обновляет данные. 
        /// Есть смысл запускать если изменились значения CurrentPage, PageItemsAmount или ItemsAmount 
        /// </summary>
        public void Refresh() 
        {
            // устанавливаем значения по умолчанию, еслм это будет необходимо
            if (CurrentPage <= 0)
                CurrentPage = 1;
            if (LastPage <= 0)
                LastPage = 1;
            if (PageItemsAmount <= 0)
                PageItemsAmount = 10;
            if (ItemsAmount < 0)
                ItemsAmount = 0;
            // установим расчетные параметры
            Offset = (CurrentPage - 1) * PageItemsAmount;

            if (ItemsAmount > 0 && ItemsAmount > PageItemsAmount)
                LastPage = (int)Math.Ceiling((double)ItemsAmount / PageItemsAmount);

            if (CurrentPage < LastPage)
                CurrentPageItemsAmount = PageItemsAmount;
            else
                CurrentPageItemsAmount = ItemsAmount - (LastPage - 1) * PageItemsAmount;

            PrevPage = CurrentPage - 1;
            if (PrevPage <= 0)
                PrevPage = 1;

            NextPage = CurrentPage + 1;
            if (NextPage >= LastPage)
                NextPage = LastPage;

            ComputePages();
        }

        /// <summary>
        /// Вычисляет данные по постраничке
        /// </summary>
        private void ComputePages()
        {
            Pages.Clear();
            bool leftDotPage = false;
            bool rightDotPage = false;
            if (LastPage > 15)
                rightDotPage = true;
            // если страниц меньше чем 15(вкл) или текущая страница меньше 8(вкл), то покажем все начальные страницы
            if (LastPage <= 15)
            {
                for (int i = 1; i <= LastPage; i++)
                    Pages.Add(new PagePointer
                    {
                        Value = i,
                        Caption = i.ToString(),
                        IsCurrenPage = i == CurrentPage,
                        RouteValues = Merge(RouteValues, new Dictionary<string, object> { { "Page", i } })
                    });
            }
            else if (CurrentPage <= 8)
            {
                for (int i = 1; i <= 15; i++)
                    Pages.Add(new PagePointer
                    {
                        Value = i,
                        Caption = i.ToString(),
                        IsCurrenPage = i == CurrentPage,
                        RouteValues = Merge(RouteValues, new Dictionary<string, object> { { "Page", i } })
                    });
            }
            // иначе сдвигаем страницы на разницу текущей страницы от 8
            else
            {
                leftDotPage = true;
                int n = LastPage - 15; // количество позиций на которые можно сдвинуть влево 
                int c = CurrentPage - 8; // количество позиций относительно текущей страницы
                if (c > n)
                {
                    c = n;
                    rightDotPage = false;
                }

                for (int i = 1 + c, l = c + 15; i <= l; i++)
                    Pages.Add(new PagePointer
                    {
                        Value = i,
                        Caption = i.ToString(),
                        IsCurrenPage = i == CurrentPage,
                        RouteValues = Merge(RouteValues, new Dictionary<string, object> { { "Page", i } })
                    });
            }
            // если не нужно показывать первые и последние страницы, то покажем просто точки. Картина будет такая:                                  
            //  ___ ___ ___ ___ ___ ___
            //  .. | 6 | 7 | 8 | 9 | .. 
            //
            if (!ShowLastAndFirstPages)
            {
                for (var i = 0; i < Pages.Count; i++)
                {
                    if (i == 0 && leftDotPage || i == 14 && rightDotPage)
                    {
                        Pages[i].Caption = "..";
                    }
                }
            }
            // если нужно показывать первые и последние страницы, то покажем 1, 2, предпоследнюю и последнюю страницы. То есть картина будет такая: 
            //  ___ ___ ____ ___ ___ ___ ___ ____ ____ ____
            // | 1 | 2 | .. | 6 | 7 | 8 | 9 | .. | 14 | 15 |
            //
            else
            {
                for (var i = 0; i < Pages.Count; i++)
                {
                    if (i <= 2 && leftDotPage)
                    {
                        if (i == 0)
                        {
                            Pages[i].Caption = "1";
                            Pages[i].Value = 1;
                            Pages[i].RouteValues = Merge(RouteValues, new Dictionary<string, object> { { "Page", 1 } });
                        }
                        else if (i == 1)
                        {
                            Pages[i].Caption = "2";
                            Pages[i].Value = 2;
                            Pages[i].RouteValues = Merge(RouteValues, new Dictionary<string, object> { { "Page", 2 } });
                        }
                        else if (i == 2)
                        {
                            Pages[i].Caption = "..";
                        }
                    }
                    else if (i >= 12 && rightDotPage)
                    {
                        if (i == 14)
                        {
                            Pages[i].Caption = LastPage.ToString();
                            Pages[i].Value = LastPage;
                            Pages[i].RouteValues = Merge(RouteValues, new Dictionary<string, object> { { "Page", LastPage } });
                        }
                        else if (i == 13)
                        {
                            Pages[i].Caption = (LastPage - 1).ToString();
                            Pages[i].Value = LastPage - 1;
                            Pages[i].RouteValues = Merge(RouteValues, new Dictionary<string, object> { { "Page", LastPage - 1 } });
                        }
                        else if (i == 12)
                        {
                            Pages[i].Caption = "..";
                        }
                    }
                }
            }

            // добавим предыдущую и следующую странцы. Получится картина такая:
            //  ____ ___ ___ ____ ___ ___ ___ ___ ____ ____ ____ ____
            // | << | 1 | 2 | .. | 6 | 7 | 8 | 9 | .. | 14 | 15 | >> |
            //
            
            Pages.Insert(0, new PagePointer
            {
                Value = PrevPage,
                Caption = "&laquo;",
                Disabled = CurrentPage == 1,
                RouteValues = Merge(RouteValues, new Dictionary<string, object> { { "Page", PrevPage } })
            });
            
            Pages.Add(new PagePointer
            {
                Value = NextPage,
                Caption = "&raquo;",
                Disabled = CurrentPage == LastPage,
                RouteValues = Merge(RouteValues, new Dictionary<string, object> { { "Page", NextPage } })
            });
        }

        /// <summary>
        /// Объединяет словарь с другими словарями
        /// </summary>
        /// <param name="extValues"></param>
        /// <returns></returns>
        private Dictionary<string, object> Merge(params Dictionary<string, object>[] extValues)
        {
            Dictionary<string, object> values = new Dictionary<string, object>();
            for (int i = 0; i < extValues.Length; i++)
            {
                if (extValues[i] != null && extValues[i].Count > 0)
                    extValues[i].ToList().ForEach(x => { values[x.Key] = x.Value; });
            }
            return values;
        }
    }
}