﻿using AutoMapper;
using DomainLogic.Models;

namespace DomainLogic.Infrastructure.Mappers
{
    /// <summary>
    /// Класс содержит конфигурацию преобразований для классов, относящихся к модулю Product.
    /// Вынесено в отдельный класс для более гибкой настройки.
    /// </summary>
    public class AutoMapperProductProfile : Profile
    {
        /// <summary>
        /// Конфигурирует преобразование объектов
        /// </summary>
        protected override void Configure()
        {
            // конфигурируем преобразование доменов в такие же домены
            CreateMap<DmAttribute, DmAttribute>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ProductId, opt => opt.MapFrom(src => 0));
            CreateMap<DmFilter, DmFilter>();
            CreateMap<ProductValue, ProductValue>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.AttributeId, opt => opt.MapFrom(src => 0));
            CreateMap<Description, Description>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => 0));
            CreateMap<Product, Product>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ToRating, opt => opt.MapFrom(src => false))
                .ForMember(dest => dest.ToBanner, opt => opt.MapFrom(src => false))
                .ForMember(dest => dest.Published, opt => opt.MapFrom(src => false));
        }
    }
}
