﻿using AutoMapper;
using DomainLogic.Models;
using DomainLogic.Models.Enumerations;
using System.Linq;

namespace DomainLogic.Infrastructure.Mappers
{
    /// <summary>
    /// Класс содержит конфигурацию преобразований для всех объектов
    /// </summary>
    public class AutoMapperDefaultProfile : Profile
    {
        /// <summary>
        /// Конфигурирует преобразование объектов
        /// </summary>
        protected override void Configure()
        {
            // конфигурируем преобразование доменов в другие домены
            CreateMap<Section, SectionTitle>();
            CreateMap<Description, DescriptionForSnapshot>();
            CreateMap<DmAttribute, AttributeForSnapshot>();
            CreateMap<ProductValue, ValueForSnapshot>();
            CreateMap<Product, ProductInfoForSnapshot>()
                .ForMember(dest => dest.ImageIds, opt => opt.MapFrom(src => src.ProductsToImages.Select(x => x.ImageId).ToList()));

            // конфигурируем преобразование доменов в такие же домены
            CreateMap<ProductInfoForSnapshot, ProductInfoForSnapshot>();
            CreateMap<ProductSnapshot, ProductSnapshot>();
            CreateMap<ProductSnapshotInline, ProductSnapshot>()
                .ForMember(dest => dest.ProductStates, opt => opt.MapFrom(src => new ProductStates(src.ProductStates_AsInt).List));
            CreateMap<ProductType, ProductType>();
            CreateMap<Service, Service>();


            /*ForSourceType<Name>().AddFormatter<NameFormatter>();
            ForSourceType<decimal>().AddFormatExpression(context =>
              ((decimal)context.SourceValue).ToString("c"));
            CreateMap<Customer, CustomerInfo>()
              .ForMember(x => x.ShippingAddress, opt =>
              {
                  opt.AddFormatter<AddressFormatter>();
              });*/
        }
    }
}