﻿using AutoMapper;
using AutoMapper.Mappers;
using System;

namespace DomainLogic.Infrastructure.Mappers
{
    /// <summary>
    /// Реализация пользовательского маппера Product, который будет настроен для работы с профилем AutoMapperProductProfile
    /// Класс является оберткой двух классов, реализующих интерфейсы IConfiguration и IMappingEngine. 
    /// Экземпляры этих классов создаются и настраиваются в методе Initialize.
    /// Методы Map являются зеркальными от статического класса Automapper и описание этих метода можно найти тамже.
    /// </summary>
    public class ProductMapper
    {
        /// <summary>
        /// Cм. Automapper
        /// </summary>
        private IConfiguration _configuration;

        /// <summary>
        /// Cм. Automapper
        /// </summary>
        private IMappingEngine _engine;

        /// <summary>
        /// Cм. Automapper
        /// </summary>
        /// <param name="action"></param>
        public void Initialize(Action<IConfiguration> action)
        {
            _configuration = new ConfigurationStore(new TypeMapFactory(), MapperRegistry.Mappers);
            _engine = new MappingEngine((IConfigurationProvider)_configuration);
            action.Invoke(_configuration);
        }

        /// <summary>
        /// Cм. Automapper
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TDestination"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public TDestination Map<TSource, TDestination>(TSource source)
        {
            return _engine.Map<TSource, TDestination>(source);
        }

        /// <summary>
        /// Cм. Automapper
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TDestination"></typeparam>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <returns></returns>
        public TDestination Map<TSource, TDestination>(TSource source, TDestination destination)
        {
            return _engine.Map<TSource, TDestination>(source, destination);
        }

        /// <summary>
        /// Cм. Automapper
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TDestination"></typeparam>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <param name="opts"></param>
        /// <returns></returns>
        public TDestination Map<TSource, TDestination>(TSource source, TDestination destination, Action<IMappingOperationOptions> opts)
        {
            return _engine.Map<TSource, TDestination>(source, destination, opts);
        }
    }
}
