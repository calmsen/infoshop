﻿using AutoMapper;
using DomainLogic.Models;

namespace DomainLogic.Infrastructure.Mappers
{
    /// <summary>
    /// Класс содержит конфигурацию преобразований для объектов, относящихся к модулю Filter
    /// Вынесено в отдельный класс для более гибкой настройки.
    /// </summary>
    public class AutoMapperFilterProfile : Profile
    {
        /// <summary>
        ///  Конфигурирует преобразование объектов
        /// </summary>
        protected override void Configure()
        {
            // конфигурируем преобразование доменов в такие же домены
            CreateMap<FilterValue, FilterValue>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => 0));
            CreateMap<DmFilter, DmFilter>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => 0));
        }
    }
}
