﻿namespace DomainLogic.Infrastructure.Providers
{
    /// <summary>
    /// Представляет пустой контекст, который используется в Provider<TItem>, являющиеся производным от Provider<TContext, TItem>.
    /// </summary>
    public class EmptyContext : IHasKey
    {
        /// <summary>
        /// Ключ, для получения объекта из провайдера.
        /// </summary>
        public string Key { get; set; }
    }
}
