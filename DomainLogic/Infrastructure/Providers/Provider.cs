﻿using DomainLogic.Infrastructure.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace DomainLogic.Infrastructure.Providers
{
    /// <summary>
    /// Провайдер объектов. Объекты можно зарегистрировать с помощью метода Bind(), так и аннотируя атрибутом KeyAttribute.
    /// Объект может представлять из себя прототип (должен иметь реализацию ICloneable). В этом случае провайдер будет возвращать клон.
    /// Объект может иметь реализацию IInitable<TContext>, что очень актуально если объект представлен ввиде прототипа, 
    /// потому что обычно клоны создаются неглубоким копированием, а инициализации свойств и полей в методе Init() устраняет эти недостатки.
    /// Так же в провайдере можно регистировать фабрики объектов.
    /// </summary>
    /// <typeparam name="TItem"></typeparam>
    public class Provider<TItem> : Provider<EmptyContext, TItem>
    {
        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="items"></param>
        /// <param name="factories"></param>
        public Provider(IEnumerable<TItem> items, IEnumerable<IFactory<EmptyContext, TItem>> factories)
            : base(items, factories)
        {
        }
    }

    /// <summary>
    /// Провайдер объектов. Объекты можно зарегистрировать с помощью метода Bind(), так и аннотируя атрибутом KeyAttribute.
    /// Объект может представлять из себя прототип (должен иметь реализацию ICloneable). В этом случае провайдер будет возвращать клон.
    /// Объект может иметь реализацию IInitable<TContext>, что очень актуально если объект представлен ввиде прототипа, 
    /// потому что обычно клоны создаются неглубоким копированием, а инициализации свойств и полей в методе Init() устраняет эти недостатки.
    /// Так же в провайдере можно регистировать фабрики объектов.
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    /// <typeparam name="TItem"></typeparam>
    public class Provider<TContext, TItem>
        where TContext : class
    {
        /// <summary>
        /// Зарегистрированные объекты
        /// </summary>
        protected Dictionary<string, TItem> _items = new Dictionary<string, TItem>();

        /// <summary>
        /// Проинициализированные объекты
        /// </summary>
        protected HashSet<TItem> _initializedItems = new HashSet<TItem>();

        /// <summary>
        /// Зарегистрированные фабрики
        /// </summary>
        protected Dictionary<string, IFactory<TContext, TItem>> _factories = new Dictionary<string, IFactory<TContext, TItem>>();

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="items"></param>
        /// <param name="factories"></param>
        public Provider(IEnumerable<TItem> items, IEnumerable<IFactory<TContext, TItem>> factories)
        {
            BindByKeyAttributes(items);
            BindByKeyAttributes(factories);
        }

        /// <summary>
        /// Получает зарегистрированный объект
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public virtual T Get<T>()
            where T : TItem
        {
            return (T)Get(typeof(T));
        }

        /// <summary>
        /// Получает зарегистрированный объект
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public virtual TItem Get(Type type)
        {
            return Get(ResolveKey(type), default(TContext));
        }

        /// <summary>
        /// Получает зарегистрированный объект
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual TItem Get(string key)
        {
            return Get(key, default(TContext));
        }

        /// <summary>
        /// Получает зарегистрированный объект
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public virtual TItem Get(TContext context)
        {
            return Get(null, context);
        }

        /// <summary>
        /// Получает зарегистрированный объект
        /// </summary>
        /// <param name="key"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public virtual TItem Get(string key, TContext context)
        {
            if (context != default(TContext))
            {
                if (key == null && context is IHasKey)
                    key = ((IHasKey)context).Key;
                if (key == null && context.GetType().IsValueType)
                    key = context.ToString();
                if (key == null)
                    key = ResolveKey(context.GetType());
            }

            if (key == null)
                throw new ArgumentException();

            if (_factories.ContainsKey(key))
                return _factories[key].Create(context);

            if (!_items.ContainsKey(key))
                return default(TItem);

            var item = _items[key];
            if (item is ICloneable<TItem>)
            {
                item = ((ICloneable<TItem>)item).Clone();

                if (item is IInitable<TContext>)
                    ((IInitable<TContext>)item).Init(context);
            }
            else if (item is IInitable<TContext> && !_initializedItems.Contains(item))
            {
                _initializedItems.Add(item);
                ((IInitable<TContext>)item).Init(context);
            }

            return item;
        }

        /// <summary>
        /// Регистрирует объект
        /// </summary>
        /// <param name="key"></param>
        /// <param name="item"></param>
        public virtual void Bind<T>(T item) where T : TItem
        {
            _items.Add(item.GetType().Name, item);
        }

        /// <summary>
        /// Регистрирует объект
        /// </summary>
        /// <param name="key"></param>
        /// <param name="item"></param>
        public virtual void Bind(string key, TItem item)
        {
            _items.Add(key, item);
            string typeName = item.GetType().Name;
            if (!typeName.Equals(key))
            {
                _items.Add(typeName, item);
            }

        }

        /// <summary>
        /// Регистрирует фабрику
        /// </summary>
        /// <param name="key"></param>
        /// <param name="factory"></param>
        public virtual void Bind(string key, IFactory<TContext, TItem> factory)
        {
            _factories.Add(key, factory);
        }

        /// <summary>
        /// Регистрирует фабрику
        /// </summary>
        /// <param name="key"></param>
        /// <param name="factory"></param>
        public virtual void Bind(string key, Func<TContext, TItem> factory)
        {
            _factories.Add(key, new DelegateFactory<TContext, TItem>(factory));
        }

        /// <summary>
        /// Удаляет объект или фабрику
        /// </summary>
        /// <param name="key"></param>
        public virtual void Unbind(string key)
        {
            _items.Remove(key);
            _factories.Remove(key);
        }

        /// <summary>
        /// Очищает полностью все объекты и фабрики
        /// </summary>
        public void Clear()
        {
            _items.Clear();
            _factories.Clear();
        }
        /// <summary>
        /// Регистрирует все объекты по атрибутам
        /// </summary>
        /// <param name="items"></param>
        protected void BindByKeyAttributes(IEnumerable<TItem> items)
        {
            if (items == null)
                return;

            foreach (var item in items)
            {
                var attrs = item.GetType().GetCustomAttributes(true).OfType<KeyAttribute>();
                if (attrs.Any())
                {
                    foreach (var attr in attrs)
                    {
                        string key = attr.Key;
                        Bind(key, item);
                    }
                }
                else
                {
                    string key = ResolveKey(item.GetType());
                    Bind(key, item);
                }

            }
        }

        /// <summary>
        /// Регистрирует все фабрики по атрибутам
        /// </summary>
        /// <param name="factories"></param>
        protected void BindByKeyAttributes(IEnumerable<IFactory<TContext, TItem>> factories)
        {
            if (factories == null)
                return;

            foreach (var factory in factories)
            {
                if (factory is DelegateFactory<TContext, TItem>)
                    continue;
                var attrs = factory.GetType().GetCustomAttributes(true).OfType<KeyAttribute>();
                if (attrs.Any())
                {
                    foreach (var attr in attrs)
                    {
                        string key = attr.Key;
                        Bind(key, factory);
                    }
                }
                else
                {
                    string key = Regex.Replace(factory.GetType().Name, "Factory$", "");
                    Bind(key, factory);
                }
            }
        }

        /// <summary>
        /// Возвращает ключ по типу объекта
        /// </summary>
        /// <param name="itemType"></param>
        /// <returns></returns>
        protected virtual string ResolveKey(Type itemType)
        {
            if (itemType.IsGenericType)
            {
                return itemType.Name.Substring(0, itemType.Name.IndexOf("`")) +
                    ("<" + string.Join(">,<", itemType.GetGenericArguments().Select(x => x.Name).ToArray()) + ">");
            }
            return itemType.Name;
        }
    }
}
