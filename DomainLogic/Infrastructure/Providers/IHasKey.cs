﻿namespace DomainLogic.Infrastructure.Providers
{
    /// <summary>
    /// Имеет ли реализация ключ, по которому регистрируется объект в провайдере объектов Provider<TContext, TItem>.
    /// Интерфейс реализуется контекстом, если сам контекст не имеет конкретных реализаций, 
    /// по которому можно определить объект, соответсвующий контексту. 
    /// </summary>
    public interface IHasKey
    {
        /// <summary>
        /// Ключ, для получения объекта из провайдера.
        /// </summary>
        string Key { get; }
    }
}
