﻿namespace DomainLogic.Infrastructure.Providers
{
    /// <summary>
    /// Интерфейс фабрики, который можно зарегистрировать в Provider<TContext, TItem>
    /// </summary>
    /// <typeparam name="TContext">Данные которые передаются в фабричный метод</typeparam>
    /// <typeparam name="TItem">Абстрактный объект, который создается фабрикой</typeparam>
    public interface IFactory<TContext, TItem>
    {
        /// <summary>
        /// Создает объект
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        TItem Create(TContext context);
    }
}
