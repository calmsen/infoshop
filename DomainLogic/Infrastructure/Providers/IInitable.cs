﻿namespace DomainLogic.Infrastructure.Providers
{
    /// <summary>
    /// Интерфейс для инициализации объекта 
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    public interface IInitable<TContext>
    {
        /// <summary>
        /// Инициализирует объект
        /// </summary>
        /// <param name="context"></param>
        void Init(TContext context);
    }
}
