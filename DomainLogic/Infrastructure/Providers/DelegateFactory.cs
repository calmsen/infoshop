﻿using System;
namespace DomainLogic.Infrastructure.Providers
{
    /// <summary>
    /// Фабрика реализующая интерфейс, но по факту фабрикой является делегат, переданный в конструктор.
    /// Используется в Provider<TContext, TItem> для регистрации фабрик, не имеющих внешних зависимостей.
    /// Еси фабрика имеет зависимости, то следует создать полноцееный класс, реализующий интерфейс фабрики.
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    /// <typeparam name="TItem"></typeparam>
    public sealed class DelegateFactory<TContext, TItem> : IFactory<TContext, TItem>
    {
        /// <summary>
        /// Делегат фабрики
        /// </summary>
        private readonly Func<TContext, TItem> _factory;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="factory"></param>
        public DelegateFactory(Func<TContext, TItem> factory)
        {
            _factory = factory;
        }

        /// <summary>
        /// Создает объект
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public TItem Create(TContext context)
        {
            return _factory(context);
        }
    }
}
