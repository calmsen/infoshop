﻿using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace DomainLogic.Infrastructure
{
    /// <summary>
    /// Простой парсер html страниц с авторизацией
    /// </summary>
    public class HtmlParser
    {
        /// <summary>
        /// базовый url сайта, который нужно распарсить
        /// </summary>
        public string BaseUrl { get; set; } 

        /// <summary>
        /// uri формы для авторизации 
        /// </summary>
        public string LoginForm { get; set; } 

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string UserName { get; set; } 

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Cтрока данных для отправки на сервер по форме LoginForm. Строка содержит плейсхолдеры для UserName и Password
        /// </summary>
        public string AuthString { get; set; } 

        private CookieContainer _cookie;

        /// <summary>
        /// Домен для которого сохраняется cookie
        /// </summary>
        private Uri _cookieHostname;

        /// <summary>
        /// Домен для которого сохраняется cookie
        /// </summary>
        private Uri CookieHostname 
        { 
            get 
            {
                if (_cookieHostname == null)
                    _cookieHostname = new Uri(BaseUrl);
                return _cookieHostname;
            } 
        }

        /// <summary>
        /// Инициазирует экземпляр класса Pagination
        /// </summary>   
        public HtmlParser() 
        {
            _cookie = new CookieContainer();
        }

        /// <summary>
        /// Авторизует на сайте
        /// </summary>
        public void Authorize()
        {
            string authString = string.Format(AuthString, UserName, Password);
            UTF8Encoding encoding = new UTF8Encoding();
            byte[] buffer = encoding.GetBytes(authString);
            Uri CookieHostname = new Uri(BaseUrl);
            //
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(BaseUrl + LoginForm);
            request.Method = "POST";
            request.KeepAlive = true;
            request.ContentLength = buffer.Length;
            request.UserAgent = @"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.83 Safari/535.11";
            request.Accept = @"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            request.Headers.Add(HttpRequestHeader.AcceptLanguage, @"ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
            request.Headers.Add(HttpRequestHeader.AcceptEncoding, @"gzip,deflate,sdch");
            request.Headers.Add(HttpRequestHeader.AcceptCharset, @"Accept-Charset: utf-8;q=0.7,*;q=0.3");
            request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            request.CookieContainer = _cookie;
            request.ContentType = "application/x-www-form-urlencoded";
            Stream newStream = request.GetRequestStream();
            newStream.Write(buffer, 0, authString.Length);
            newStream.Close();
            //
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                CookieCollection cookies = new CookieCollection();
                cookies = response.Cookies;
                _cookie.Add(CookieHostname, cookies);
            }
        }

        /// <summary>
        /// Получает заголовок cookie
        /// </summary>
        /// <returns></returns>
        public string GetCookieHeader()
        {
            return _cookie.GetCookieHeader(CookieHostname);
        }

        /// <summary>
        /// Получает html по указанному url
        /// </summary>
        /// <param name="url"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public string GetHtmlString(string url, string encoding = null)
        {
            url = GetCorrectUrl(url);
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            //
            request.Headers.Add("Cache-Control", "max-age=0");
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36";
            request.Headers.Add("Accept-Encoding", "gzip, deflate, sdch");
            request.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
            //
            request.Headers.Add("Cookie", _cookie.GetCookieHeader(CookieHostname));
            using (HttpWebResponse hresponse = (HttpWebResponse)request.GetResponse())
            {
                Encoding responseEncoding = null;
                if (string.IsNullOrEmpty(encoding) || encoding.Equals("UTF8"))
                    responseEncoding = UTF8Encoding.UTF8;
                else
                    responseEncoding = Encoding.ASCII;

                if (!string.IsNullOrEmpty(hresponse.CharacterSet))
                    responseEncoding = Encoding.GetEncoding(hresponse.CharacterSet);
                
                using (StreamReader strReader = new StreamReader(hresponse.GetResponseStream(), responseEncoding))
                {
                    return strReader.ReadToEnd();
                }
            }
        }

        /// <summary>
        /// Получает html по указанному url и создает HtmlDocument из этого html
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public HtmlDocument GetHtmlDoc(string url)
        {
            string html = GetHtmlString(url);
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);
            return doc;
        }

        /// <summary>
        /// Получает текст узла html, полученного по указанному url.
        /// Узел указывается после двойного слэша. Например http://example.com/example-page//div/a
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string GetHtmlNodeText(string url)
        {
            new List<string>().Select(x => Convert.ToInt64(x)).ToList();
            url = GetCorrectUrl(url);
            string[] urlParts = url.Split(new string[] { "//" }, StringSplitOptions.RemoveEmptyEntries);
            url = urlParts[0] + "//" + urlParts[1];
            string xpath = "//" + urlParts[2];
            HtmlDocument doc = GetHtmlDoc(url);
            return doc.DocumentNode.SelectSingleNode(xpath).FirstChild.InnerText;
        }

        /// <summary>
        /// Получает json по указанному url и распарсивает в объект типа T
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public T GetJsonData<T>(string url)
        {
            string json = GetHtmlString(url);
            return JsonConvert.DeserializeObject<T>(json);
        }

        /// <summary>
        /// Делает url корректным
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private string GetCorrectUrl(string url)
        {
            if (!url.StartsWith("http://"))
                url = BaseUrl + url;
            return url;
        }
    }
}