﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace DomainLogic.Infrastructure
{
    /// <summary>
    /// Парсет значения из конфига
    /// </summary>
    public class ConfigValueParser
    {
        /// <summary>
        /// Возвращает строку если не пустая или значения по умолчанию
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public string ParseValue(string value, string defaultValue)
        {
            return !string.IsNullOrEmpty(value) ? value : defaultValue;
        }

        /// <summary>
        /// Парсет строку в long
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public long ParseValue(string value, long defaultValue)
        {
            return long.TryParse(value, out long result) ? result : defaultValue;
        }

        /// <summary>
        /// Парсет строку в int
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public int ParseValue(string value, int defaultValue)
        {
            return int.TryParse(value, out int result) ? result : defaultValue;
        }

        /// <summary>
        /// Парсет строку в bool
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public bool ParseValue(string value, bool defaultValue)
        {
            return bool.TryParse(value, out bool result) ? result : defaultValue;
        }

        /// <summary>
        /// Парсет строку в список строк
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public List<string> ParseValue(string value, List<string> defaultValue)
        {
            List<string> list = ParseValueToList(value);            
            return GetListOrDefault(list, defaultValue);
        }

        /// <summary>
        /// Парсет строку в список long
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public List<long> ParseValue(string value, List<long> defaultValue)
        {
            List<long> list = ParseValueToList(value)
                    .Select(x => Convert.ToInt64(x.Trim())).ToList();

            return GetListOrDefault(list, defaultValue);
        }

        /// <summary>
        /// Парсет строку в список int
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public List<int> ParseValue(string value, List<int> defaultValue)
        {
            List<int> list = ParseValueToList(value)
                    .Select(x => Convert.ToInt32(x.Trim())).ToList();

            return GetListOrDefault(list, defaultValue);
        }

        /// <summary>
        /// Парсет строку в список bool
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public List<bool> ParseValue(string value, List<bool> defaultValue)
        {
            List<bool> list = ParseValueToList(value)
                    .Select(x => Convert.ToBoolean(x.Trim())).ToList();

            return GetListOrDefault(list, defaultValue);
        }

        /// <summary>
        /// Парсет строку в словарь строк
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public Dictionary<string, string> ParseValue(string value, Dictionary<string, string> defaultValue)
        {
            Dictionary<string, string> map = ParseValueToDictionary(value);            
            return GetDictionaryOrDefault(map, defaultValue);
        }

        /// <summary>
        /// Парсет строку в словарь long
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public Dictionary<string, long> ParseValue(string value, Dictionary<string, long> defaultValue)
        {
            Dictionary<string, long> map = ParseValueToDictionary(value)
                    .ToDictionary(x => x.Key, x => long.Parse(x.Value));

            return GetDictionaryOrDefault(map, defaultValue);
        }

        /// <summary>
        /// Парсет строку в словарь int
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public Dictionary<string, int> ParseValue(string value, Dictionary<string, int> defaultValue)
        {
            Dictionary<string, int> map = ParseValueToDictionary(value)
                    .ToDictionary(x => x.Key, x => int.Parse(x.Value));

            return GetDictionaryOrDefault(map, defaultValue);
        }

        /// <summary>
        /// Парсет строку в словарь bool
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public Dictionary<string, bool> ParseValue(string value, Dictionary<string, bool> defaultValue)
        {
            Dictionary<string, bool> map = ParseValueToDictionary(value)
                    .ToDictionary(x => x.Key, x => bool.Parse(x.Value));

            return GetDictionaryOrDefault(map, defaultValue);
        }

        /// <summary>
        /// Парсет строку в список строк
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private List<string> ParseValueToList(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return new List<string>();

            List<string> list = value
                    .Split(',')
                    .Select(x => x.Trim()).ToList();
            return list;
        }

        /// <summary>
        /// Парсет строку в словарь строк
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        private List<T> GetListOrDefault<T>(List<T> list, List<T> defaultValue)
        {
            return list.Count > 0 ? list : defaultValue;
        }

        /// <summary>
        /// Парсет строку в словарь строк
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private Dictionary<string, string> ParseValueToDictionary(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return new Dictionary<string, string>();

            Dictionary<string, string> map = Regex.Replace(value, ";$", "")
                    .Split(';')
                    .Select(x => x.Trim())
                    .ToDictionary(x => x.Split('=')[0].Trim(), x => x.Split('=')[1].Trim());

            return map;
        }

        private Dictionary<string, T> GetDictionaryOrDefault<T>(Dictionary<string, T> dictionary, Dictionary<string, T> defaultValue)
        {
            return dictionary.Count > 0 ? dictionary : defaultValue;
        }
    }
}
