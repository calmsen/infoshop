﻿
namespace DomainLogic.Infrastructure.Transliteration
{
    /// <summary>
    /// Перечисление содержащее стандарты, с которыми работает класс Transliterater
    /// <see cref="http://usanov.net/748-transliteraciya-rus-2-lat-na-c"/>
    /// </summary>
    public enum TransliterationType
    {
        Gost,
        ISO
    }
}
