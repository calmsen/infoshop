﻿namespace DomainLogic.Infrastructure.Credentials
{
    /// <summary>
    /// Класс предоставляет учетные данные для проверки подлинности 
    /// на основе пароля и имени пользователя для закачки изображения по ftp 
    /// </summary>
    public class FtpCredential : NetCredential
    {
        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        public FtpCredential()
            : base()
        {
        }

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public FtpCredential(string userName, string password)
            : base(userName, password)
        {
        }
    }
}
