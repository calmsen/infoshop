﻿
namespace DomainLogic.Infrastructure.Credentials
{
    /// <summary>
    /// Класс предоставляет учетные данные для проверки подлинности 
    /// на основе пароля и имени пользователя для закачки изображения с удаленного диска
    /// </summary>
    public class FileCredential : NetCredential
    {
        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        public FileCredential()
            : base()
        {
        }

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public FileCredential(string userName, string password)
            : base(userName, password)
        {
        }
    }
}
