﻿
namespace DomainLogic.Infrastructure.Credentials
{
    /// <summary>
    /// Класс предоставляет учетные данные для проверки подлинности 
    /// на основе пароля и имени пользователя
    /// </summary>
    public class NetCredential
    {
        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Инициализирует новый экземпляр класса NetCredential.
        /// </summary>
        public NetCredential()
        {
        }
        /// <summary>
        /// Инициализирует новый экземпляр класса NetCredential с заданными
        /// значениями пароля и имени пользователя.
        /// </summary>
        /// <param name="userName">Имя пользователя</param>
        /// <param name="userPassword">Пароль пользователя</param>
        public NetCredential(string userName, string userPassword)
        {
            UserName = userName;
            Password = userPassword;
        }
    }
}
