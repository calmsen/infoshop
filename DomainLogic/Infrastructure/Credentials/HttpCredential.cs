﻿namespace DomainLogic.Infrastructure.Credentials
{
    /// <summary>
    /// Класс предоставляет учетные данные для проверки подлинности 
    /// на основе пароля и имени пользователя для закачки изображения по http 
    /// </summary>
    public class HttpCredential : NetCredential
    {
        /// <summary>
        /// Базовый url сайта
        /// </summary>
        public string BaseUrl { get; set; }

        /// <summary>
        /// URI для запроса формы авторизации
        /// </summary>
        public string LoginForm { get; set; }

        /// <summary>
        /// Шаблон для передачи параметров 
        /// </summary>
        public string AuthString { get; set; }

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        public HttpCredential()
            : base()
        {
        }

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public HttpCredential(string userName, string password)
            : base(userName, password)
        {
        }
    }
}
