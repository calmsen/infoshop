﻿using System;

namespace DomainLogic.Infrastructure.Utils
{
    /// <summary>
    /// Класс содержит методы для работы с датами
    /// </summary>
    public class DateUtils
    {        
        /// <summary>
        /// Получить дату с ведущим нулем
        /// </summary>
        /// <param name="date">Объект даты</param>
        /// <param name="format">Формат, только d, t, g</param>
        /// <returns></returns>
        public string DateToStringWithZero(DateTime date, string format = "d")
        {
            if (format.Equals("d"))
                format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
            else if (format.Equals("t"))
                format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortTimePattern;
            else if (format.Equals("g"))
                format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern + " " + System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortTimePattern;
            format = format
                .Replace("dd", "d").Replace("MM", "M").Replace("HH", "H").Replace("hh", "h")
                .Replace("d", "dd").Replace("M", "MM").Replace("H", "HH").Replace("h", "hh");

            return date.ToString(format);
        }
        
        /// <summary>
        /// Получить текущее юних время
        /// </summary>
        /// <returns></returns>
        public long UnixTimeNow()
        {
            var timeSpan = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0));
            return (long)timeSpan.TotalSeconds;
        }
    }
}