﻿using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;
using System.ComponentModel.DataAnnotations;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Transliteration;

namespace DomainLogic.Infrastructure.Utils
{
    /// <summary>
    /// Класс содержит разные методы, не привязанные к никакой группе
    /// </summary>
    public class CommonUtils
    {
        /// <summary>
        /// Преобразование кириллицы в латиницу
        /// </summary>
        [Inject]
        public Transliterater Transliterater { get; set; }

        /// <summary>
        /// Получить md5 хэш строки
        /// </summary>
        /// <param name="input">Строка из которые будет получен хэш</param>
        /// <returns></returns>
        public string GetMd5Hash(string input)
        {
            // создаем объект этого класса. Отмечу, что он создается не через new, а вызовом метода Create
            MD5 md5Hasher = MD5.Create();

            // Преобразуем входную строку в массив байт и вычисляем хэш
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Создаем новый Stringbuilder (Изменяемую строку) для набора байт
            StringBuilder sBuilder = new StringBuilder();

            // Преобразуем каждый байт хэша в шестнадцатеричную строку
            for (int i = 0; i < data.Length; i++)
            {
                //указывает, что нужно преобразовать элемент в шестнадцатиричную строку длиной в два символа
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        /// <summary>
        /// Преобразует кириллицу в латиницу
        /// </summary>
        /// <param name="str">Строка, которая будет преобразовываться</param>
        /// <returns>Новая строка</returns>
        public string ToLat(string str)
        {
            str = Regex.Replace(str, @"\s|\.|\(", " ");
            str = Regex.Replace(str, @"\s+", " ");
            str = Regex.Replace(str, @"[^\s\w\d-]", "");
            str = str.Trim();
            str = Transliterater.Front(str);
            if (str.Length > 64)
            {
                string[] words = str.Split('-');
                str = string.Empty;
                for (int i = 0; i < words.Length; i++)
                {
                    str += words[i];
                    if (str.Length > 32)
                        break;
                    str += "-";
                }
            }
            return str;
        }

        /// <summary>
        /// Преобразует double? в long?
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public long? ToLong(double? d)
        {
            if (d == null)
                return null;
            return Convert.ToInt64(Math.Floor(d ?? 0));
        }
        
        /// <summary>
        /// Получить форматированное значение. Например даты получаются в формате G
        /// </summary>
        /// <param name="source"></param>
        /// <param name="propInfo"></param>
        /// <returns>Форматированное значение</returns>
        public string GetFormatedValue(object source, PropertyInfo propInfo)
        {
            string valueAsString = null;
            var value = propInfo.GetValue(source, null);
            if (value == null)
                return valueAsString;
            if (value is DateTime)
            {
                string dateFormat = "G";
                DataTypeAttribute attr = propInfo.GetCustomAttribute<DataTypeAttribute>();
                if (attr != null)
                {
                    if (attr.DisplayFormat != null && attr.DisplayFormat.DataFormatString != null)
                    {
                        dateFormat = attr.DisplayFormat.DataFormatString.Replace("{0:", string.Empty).Replace("}", string.Empty);
                    }
                }
                valueAsString = ((DateTime)value).ToString(dateFormat);
            }
            else
            {
                valueAsString = value.ToString();
            }
            return valueAsString;
        }
        
        /// <summary>
        /// Получает версию исполняемой сборки
        /// </summary>
        /// <returns></returns>
        public string GetAssemblyVersion() {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        /// <summary>
        /// Получить url с учетом культуры
        /// </summary>
        /// <param name="culture"></param>
        /// <param name="currentUrl"></param>
        /// <returns></returns>
        public string GetUrlWithCulture(string culture, string currentUrl)
        {
            if (currentUrl == null)
                return null;
            string url = currentUrl.Replace("/" + culture, "");
            url = new Regex(@"\/$").Replace(url, "");
            if (!culture.Equals("ru"))
            {
                url = url.Replace("://", "ProtocolSeparator");
                string[] urlParts = url.Split('/');
                url = "";
                for (int i = 0; i < urlParts.Length; i++)
                {
                    if (i > 0)
                        url += "/";
                    url += urlParts[i];
                    if (i == 0)
                        url += "/" + culture;
                }
                url = url.Replace("ProtocolSeparator", "://");
            }
            url = new Regex(@"\/$").Replace(url, "");
            if (string.IsNullOrEmpty(url))
                url = "/";
            return url;
        }
    }
}