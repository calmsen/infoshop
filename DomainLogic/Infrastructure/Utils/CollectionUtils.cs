﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Collections.Specialized;
using DomainLogic.Infrastructure.Attributes;

namespace DomainLogic.Infrastructure.Utils
{
    /// <summary>
    /// Класс содержит методы для работы с коллекциями
    /// </summary>
    public class CollectionUtils
    {
        /// <summary>
        /// Общие методы, не привязанные к никакой группе
        /// </summary>
        [Inject]
        public CommonUtils CommonUtils { get; set; }
        
        /// <summary>
        /// Разбивает список source на n списков
        /// </summary>
        /// <typeparam name="T">Тип объектов, которые содержит список</typeparam>
        /// <param name="source">Список объектов, который нужно разбить</param>
        /// <param name="size">Количестов списков, на которые нужно разбить входной список</param>
        /// <returns>Список списков объектов</returns>
        public IEnumerable<IEnumerable<T>> Split<T>(IEnumerable<T> source, int size)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / size)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }
        
        /// <summary>
        /// Разбивает список source на n списков
        /// </summary>
        /// <typeparam name="T">Тип объектов, которые содержит список</typeparam>
        /// <param name="source"></param>
        /// <param name="chunksize">Количестов списков, на которые нужно разбить входной список</param>
        /// <returns>Список списков объектов</returns>
        public IEnumerable<IEnumerable<T>> Chunk<T>(IEnumerable<T> source, int chunksize)
        {
            while (source.Any())
            {
                yield return source.Take(chunksize);
                source = source.Skip(chunksize);
            }
        }
        
        /// <summary>
        /// Преобразует словарь в объект типа T
        /// </summary>
        /// <typeparam name="T">Тип объекта, в который нужно преобразовать</typeparam>
        /// <param name="source">Словарь</param>
        /// <returns>Объект тип T</returns>
        public T ToObject<T>(IDictionary<string, object> source)
        where T : class, new()
        {
            T someObject = new T();
            Type someObjectType = someObject.GetType();

            foreach (KeyValuePair<string, object> item in source)
            {
                someObjectType.GetProperty(item.Key).SetValue(someObject, item.Value, null);
            }

            return someObject;
        }
        
        /// <summary>
        /// Расширяет словарь values новыми значениями из словарей extValues
        /// </summary>
        /// <param name="values">Словарь, который нужно расширить</param>
        /// <param name="extValues">Словари, которые будут расширять словарь values</param>
        /// <returns>Словарь values</returns>
        public Dictionary<string, object> Extend(Dictionary<string, object> values, params Dictionary<string, object>[] extValues)
        {
            for (int i = 0; i < extValues.Length; i++)
            {
                if (extValues[i] != null && extValues[i].Count > 0)
                    extValues[i].ToList().ForEach(x => { values[x.Key] = x.Value; });
            }
            return values;
        }
        
        /// <summary>
        /// Преобразует объект source в словарь. 
        /// Внимание: метод следовала назвать ToDictionary, но такой метод уже есть!
        /// </summary>
        /// <param name="source">Объект, который будет преобразовываться</param>
        /// <param name="bindingAttr">Флаги, которые говарят какие свойства преобразовывать</param>
        /// <returns>Новый словарь</returns>
        public Dictionary<string, object> AsDictionary(object source, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
        {
            return source.GetType().GetProperties(bindingAttr).ToDictionary
            (
                propInfo => propInfo.Name,
                propInfo => propInfo.GetValue(source, null)
            );
        }
        
        /// <summary>
        /// Преобразует объект source в словарь. 
        /// </summary>
        /// <param name="source">Объект, который будет преобразовываться</param>
        /// <param name="queryString">Коллекция значений для сверки на пустые значения</param>
        /// <param name="bindingAttr">Флаги, которые говарят какие свойства преобразовывать</param>
        /// <returns>Новый словарь</returns>
        public Dictionary<string, object> ToDictionaryOfFormatedValues(object source, NameValueCollection queryString = null, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
        {
            return source.GetType().GetProperties(bindingAttr).ToDictionary
            (
                propInfo => propInfo.Name,
                propInfo => { 
                    object obj = CommonUtils.GetFormatedValue(source, propInfo);
                    if (obj == null && queryString != null && queryString[propInfo.Name] != null)
                        obj = "null";
                    return obj;
                }
            );
        }
    }
}