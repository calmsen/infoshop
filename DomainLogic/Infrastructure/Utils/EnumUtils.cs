﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace DomainLogic.Infrastructure.Utils
{
    /// <summary>
    /// Класс содержит методы для работы с перечислениями
    /// </summary>
    public class EnumUtils
    {         
        /// <summary>
        /// Преобразует список перечислений в число
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public int ListOfEnumsToInt<T>(IEnumerable<T> list)
        {
            int v = 0;
            foreach (T e in list)
                v += Convert.ToInt32(e as Enum);
            return v;
        }
        
        /// <summary>
        /// Преобразует список перечислений в число
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public int ListOfEnumsToInt(IEnumerable list)
        {
            int v = 0;
            foreach (var e in list)
                v += Convert.ToInt32(e as Enum);
            return v;
        }
    }
}