﻿
namespace DomainLogic.Infrastructure.Utils
{
    /// <summary>
    /// Класс для работы со строками
    /// </summary>
    public class StringUtils
    {        
                
        /// <summary>
        /// Форматируем текст, с учетом культуры и плюаризации
        /// </summary>
        /// <param name="text"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public string Format(string text, params object[] args)
        {
            return SmartFormat.Smart.Format(System.Threading.Thread.CurrentThread.CurrentCulture, text, args);
        }
        
        /// <summary>
        /// Экранирует одинарные ковычки
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string SqlEscape(string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return str.Replace("'", "''");
        }

        /// <summary>
        /// Удаляет подстроку с конца строки
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public string TrimEnd(string source, string value)
        {
            if (!source.EndsWith(value))
                return source;

            return source.Remove(source.LastIndexOf(value));
        }
    }
}