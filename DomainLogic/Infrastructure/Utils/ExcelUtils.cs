﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;

namespace DomainLogic.Infrastructure.Utils
{
    /// <summary>
    /// Класс содержит методы по работе с экселем
    /// </summary>
    public class ExcelUtils
    {
        /// <summary>
        /// Парсим эксель в DataTable
        /// </summary>
        /// <param name="path">Путь до экселевского файла</param>
        /// <returns></returns>
        public DataTable ExcelToDataTable(string path)
        {
            string strConn = null;
            if (Path.GetExtension(path) == ".xls")
                strConn = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"", path);
            else if (Path.GetExtension(path) == ".xlsx")
                strConn = String.Format("Provider=Microsoft.Ace.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=1\"", path);
            
            DataTable dt = null;
            using (OleDbConnection conn = new OleDbConnection(strConn))// создаем соединение            
            {
                conn.Open();
                // узнаем название листа
                DataTable dbSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string firstSheetName = dbSchema.Rows[0]["TABLE_NAME"].ToString();
                // выполняем команду
                string strExcel = "select * from [" + firstSheetName + "]";
                OleDbDataAdapter cmd = new OleDbDataAdapter(strExcel, strConn);
                DataSet ds = new DataSet();
                cmd.Fill(ds, "table");
                // преобразуем DataSet в DataTable
                dt = ds.Tables["table"];
            }
            return dt;
        }
        
        /// <summary>
        /// Записывает данные в массив байтов, который можно сохранить в экселевский файл
        /// <see cref="http://stackoverflow.com/questions/20743392/how-to-write-data-to-excel-file-and-download-it-in-asp-net"/> 
        /// <see cref="http://www.codeproject.com/Tips/681412/Insert-Access-Format-Filter-Setting-Formula-Header"/>
        /// <see cref="http://blog.newslacker.net/2012/05/epplus-format-as-table.html"/>
        /// </summary>
        /// <param name="dt">Данные</param>
        /// <param name="options">Опции для создания экселя</param>
        public byte[] DataTableToExcel(DataTable dt, Dictionary<string, string> options = null)
        {
            if (options == null)
                options = new Dictionary<string, string>();

            SetDefaultOptions(options);

            // Create the package and make sure you wrap it in a using statement
            using (var package = new ExcelPackage())
            {
                CreateWorksheet(package, dt, options);

                // save our new workbook and we are done!
                return package.GetAsByteArray();
            }
        }
        
        /// <summary>
        /// Записывает данные в экселевский файл
        /// </summary>
        /// <param name="dt">Данные</param>
        /// <param name="path">Путь до экселевского файла</param>
        /// <param name="options">Опции для создания экселя</param>
        public void DataTableToExcel(DataTable dt, string path, Dictionary<string, string> options = null)
        {
            if (options == null)
                options = new Dictionary<string, string>();

            SetDefaultOptions(options);

            // Create the file using the FileInfo object
            var file = new FileInfo(path);
            // Create the package and make sure you wrap it in a using statement
            using (var package = new ExcelPackage(file))
            {
                CreateWorksheet(package, dt, options);

                // save our new workbook and we are done!
                package.Save();
            }
        }

        /// <summary>
        /// Устанавливает опции по умолчаию, если они не были установлены в словарь options
        /// </summary>
        /// <param name="options"></param>
        private void SetDefaultOptions(Dictionary<string, string> options)
        {
            if (!options.ContainsKey("FormatAsTable")) options["FormatAsTable"] = "True";
            if (!options.ContainsKey("Worksheet.name")) options["Worksheet.Name"] = "Лист1";
            if (!options.ContainsKey("Worksheet.TabColor")) options["Worksheet.TabColor"] = "Black";
            if (!options.ContainsKey("Worksheet.DefaultColWidth")) options["Worksheet.DefaultColWidth"] = "24";
            if (!options.ContainsKey("Worksheet.DefaultRowHeight")) options["Worksheet.DefaultRowHeight"] = "12";
            if (!options.ContainsKey("Th.Style.Font.Bold")) options["Th.Style.Font.Bold"] = "true";
            if (!options.ContainsKey("Th.Style.Fill.BackgroundColor")) options["Th.Style.Fill.BackgroundColor"] = "Black";
            if (!options.ContainsKey("Th.Style.Fill.Color")) options["Th.Style.Fill.Color"] = "WhiteSmoke";
            if (!options.ContainsKey("Td.Style.Font.Bold")) options["Td.Style.Font.Bold"] = "false";
            if (!options.ContainsKey("Td.Style.Fill.BackgroundColor")) options["Td.Style.Fill.BackgroundColor"] = "LightSteelBlue";
            if (!options.ContainsKey("Td.Style.Fill.Color")) options["Td.Style.Fill.Color"] = "Black";
        }

        /// <summary>
        /// Устанавливает стили для рабочего листа (цвет таба, ширину, высоту ячеек)
        /// </summary>
        /// <param name="worksheet"></param>
        /// <param name="options"></param>
        private void SetWorkSheetStyles(ExcelWorksheet worksheet, Dictionary<string, string> options)
        {
            worksheet.TabColor = (Color)typeof(Color).GetProperty(options["Worksheet.TabColor"]).GetValue(null, null);
            worksheet.DefaultRowHeight = Int32.Parse(options["Worksheet.DefaultRowHeight"]);
            worksheet.DefaultColWidth = Int32.Parse(options["Worksheet.DefaultColWidth"]);
            /*worksheet.HeaderFooter.FirstFooter.LeftAlignedText = string.Format("Generated: {0}", DateTime.Now.ToShortDateString());
            worksheet.Row(1).Height = 20;*/
        }

        /// <summary>
        /// Устанавливает стили для заголовочных ячеек
        /// </summary>
        /// <param name="range">Диапозон ячеек</param>
        /// <param name="options">Опции форматирования</param>
        private void SetThStyle(ExcelRange range, Dictionary<string, string> options)
        {
            range.AutoFilter = true;
            range.Style.Font.Bold = Boolean.Parse(options["Th.Style.Font.Bold"]);
            range.Style.Fill.PatternType = ExcelFillStyle.Solid;
            range.Style.Fill.BackgroundColor.SetColor((Color)typeof(Color).GetProperty(options["Th.Style.Fill.BackgroundColor"]).GetValue(null, null));
            range.Style.Font.Color.SetColor((Color)typeof(Color).GetProperty(options["Th.Style.Fill.Color"]).GetValue(null, null));
            range.Style.ShrinkToFit = false;
        }

        /// <summary>
        /// Устанавливает стили для остальных ячеек
        /// </summary>
        /// <param name="range">Диапозон ячеек</param>
        /// <param name="options">Опции форматирования</param>
        private void SetTdStyle(ExcelRange range, Dictionary<string, string> options)
        {
            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
            range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            range.Style.Border.Top.Color.SetColor(Color.Black);
            range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            range.Style.Border.Right.Color.SetColor(Color.Black);
            range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            range.Style.Border.Bottom.Color.SetColor(Color.Black);
            range.Style.Font.Bold = Boolean.Parse(options["Td.Style.Font.Bold"]);
            range.Style.Fill.PatternType = ExcelFillStyle.Solid;
            range.Style.Fill.BackgroundColor.SetColor((Color)typeof(Color).GetProperty(options["Td.Style.Fill.BackgroundColor"]).GetValue(null, null));
            range.Style.Font.Color.SetColor((Color)typeof(Color).GetProperty(options["Td.Style.Fill.Color"]).GetValue(null, null));
            range.Style.ShrinkToFit = false;
        }

        /// <summary>
        /// Форматирует рабочий лист. (устанавливает высоту, ширину колонок, стили для заголочоных ячеек и для остальных ячеек)
        /// </summary>
        /// <param name="dt">Данные</param>
        /// <param name="worksheet">Рабочий лист</param>
        /// <param name="options">Опции форматирования</param>
        private void FormatWorksheet(DataTable dt, ExcelWorksheet worksheet, Dictionary<string, string> options)
        {
            // format the first row of the header
            using (var range = worksheet.Cells[1, 1, 1, dt.Columns.Count])
            {
                SetThStyle(range, options);
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    // format the ordinal row
                    if (j == dt.Columns.Count - 1)
                    {
                        using (var range = worksheet.Cells[i + 2, 1, i + 2, dt.Columns.Count])
                        {
                            SetTdStyle(range, options);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Форматирует рабочий лист по шаблону как таблица
        /// </summary>
        /// <param name="dt">Данные</param>
        /// <param name="worksheet">Рабочий лист</param>
        private void FormatWorksheetAsTable(DataTable dt, ExcelWorksheet worksheet)
        {
            using (var range = worksheet.Cells[1, 1, dt.Rows.Count + 3, dt.Columns.Count])
            {
                var table = worksheet.Tables.Add(range, "table1");
                table.ShowTotal = true;
                table.TableStyle = TableStyles.Medium20;
            }
        }

        /// <summary>
        /// Создает рабочий лист в пакете ExcelPackage. Рабочий лист форматируется как таблица если в options установлено FormatAsTable в True 
        /// </summary>
        /// <param name="package">Экселевский пакет</param>
        /// <param name="dt">Данные</param>
        /// <param name="options">Опции форматирования</param>
        private void CreateWorksheet(ExcelPackage package, DataTable dt, Dictionary<string, string> options = null)
        {
            // add a new worksheet to the empty workbook
            ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(options["Worksheet.Name"]);

            // Add some formatting to the worksheet
            SetWorkSheetStyles(worksheet, options);

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                worksheet.Cells[1, i + 1].Value = dt.Columns[i].ColumnName;
            }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    worksheet.Cells[i + 2, j + 1].Value = dt.Rows[i][j];
                }
            }
            if (options["FormatAsTable"] == "True")
                FormatWorksheetAsTable(dt, worksheet);
            else
                FormatWorksheet(dt, worksheet, options);

            // Fit the columns according to its content
            for (int i = 0; i < dt.Columns.Count; i++)
                worksheet.Column(i + 1).AutoFit();

            // Set some document properties
            /*package.Workbook.Properties.Title = "Sales list";
            package.Workbook.Properties.Author = "Gustaf Lindqvist @ Ted & Gustaf";
            package.Workbook.Properties.Company = "Ted & Gustaf";*/
        }
    }
}