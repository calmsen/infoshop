﻿using System;

namespace DomainLogic.Infrastructure.Utils
{
    /// <summary>
    /// Класс для работы со разделами
    /// </summary>
    public class SectionsUtils
    {
        /// <summary>
        /// Получает путь(правое ограничение), по которому будет построен фильтр по разделам 
        /// </summary>
        /// <param name="pathOfSections"></param>
        /// <returns></returns>
        public long GetRightPath(long pathOfSections)
        {
            if (pathOfSections == 0)
                return System.Int64.MaxValue;

            byte[] bytes = BitConverter.GetBytes(pathOfSections);
            for (int i = 0; i < bytes.Length; i++)
            {
                if (bytes[i] != 0)
                {
                    bytes[i] += 1;
                    break;
                }
            }
            return BitConverter.ToInt64(bytes, 0);
        }

        /// <summary>
        /// Получает маску по разделу. Нужно чтобы сгруппировать разделы относительно родительского в независимости от уровня вложенности
        /// </summary>
        /// <param name="pathOfSections"></param>
        /// <returns></returns>
        public long GetPartitionByMask(long pathOfSections)
        {
            if (pathOfSections == 0)
                return BitConverter.ToInt64(new byte[] { 0, 0, 0, 0, 0, 0, 0, 127 }, 0);

            byte[] bytes = BitConverter.GetBytes(pathOfSections);
            bool finded = false;
            for (int i = 0; i < bytes.Length; i++)
            {
                // после того как нашли байт не равный нулю, обнулям все байты следующие за ним
                if (finded)
                {
                    bytes[i] = 0;
                    continue;
                }
                // найдем байт не равный нулю
                if (bytes[i + 1] != 0)
                {
                    bytes[i] = 255;
                    finded = true;
                }
            }
            return BitConverter.ToInt64(bytes, 0);
        }
    }
}
