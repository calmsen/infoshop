﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DomainLogic.Infrastructure.Utils
{
    /// <summary>
    /// Класс содержит методы по работе с рефлексией
    /// </summary>
    public class ReflectorUtils
    {
        /// <summary>
        /// Получим тип объекты по пути свойств
        /// </summary>
        /// <param name="value">Объект, в котором будет искаться свойство</param>
        /// <param name="path">Путь до свойства, может быть неограниченной вложенности</param>
        /// <returns></returns>
        public Type GetPropType(object value, string path)
        {
            if (value == null) throw new ArgumentNullException("value");
            if (path == null) throw new ArgumentNullException("path");

            Type currentType = value.GetType();

            object obj = value;
            foreach (string propertyName in path.Split('.'))
            {
                if (currentType != null)
                {
                    PropertyInfo property = null;
                    int brackStart = propertyName.IndexOf("[");
                    int brackEnd = propertyName.IndexOf("]");

                    property = currentType.GetProperty(brackStart > 0 ? propertyName.Substring(0, brackStart) : propertyName);
                    obj = property.GetValue(obj, null);

                    if (brackStart > 0)
                    {
                        string index = propertyName.Substring(brackStart + 1, brackEnd - brackStart - 1);
                        foreach (Type iType in obj.GetType().GetInterfaces())
                        {
                            if (iType.IsGenericType && iType.GetGenericTypeDefinition() == typeof(IDictionary<,>))
                            {
                                obj = typeof(ReflectorUtils).GetMethod("GetDictionaryElement")
                                                     .MakeGenericMethod(iType.GetGenericArguments())
                                                     .Invoke(null, new object[] { obj, index });
                                break;
                            }
                            if (iType.IsGenericType && iType.GetGenericTypeDefinition() == typeof(IEnumerable<>))
                            {
                                obj = typeof(ReflectorUtils).GetMethod("GetListElement")
                                                     .MakeGenericMethod(iType.GetGenericArguments())
                                                     .Invoke(null, new object[] { obj, index });
                                break;
                            }
                        }
                    }

                    currentType = obj != null ? obj.GetType() : null; //property.PropertyType;
                }
                else return null;
            }
            return currentType;
        }
        
        /// <summary>
        /// Получим значение по пути свойств
        /// </summary>
        /// <typeparam name="T">Тип объекта который будет возвращен</typeparam>
        /// <param name="value">Объект, в котором будет искаться свойство</param>
        /// <param name="path">Путь до свойства, может быть неограниченной вложенности</param>
        /// <returns>Объект типа T, соотвествуйющий пути к свойству</returns>
        public T GetPropValue<T>(object value, string path)
        {
            object obj  = GetPropValue(value, path);
            return obj != null ? (T)obj : default(T);
        }
        
        /// <summary>
        /// Получим значение по пути свойств
        /// </summary>
        /// <param name="value">Объект, в котором будет искаться свойство</param>
        /// <param name="path">Путь до свойства, может быть неограниченной вложенности</param>
        /// <returns>Объект, соотвествуйющий пути к свойству</returns>
        public object GetPropValue(object value, string path)
        {
            if (value == null) throw new ArgumentNullException("value");
            if (path == null) throw new ArgumentNullException("path");

            Type currentType = value.GetType();

            object obj = value;
            foreach (string propertyName in path.Split('.'))
            {
                if (currentType != null)
                {
                    PropertyInfo property = null;
                    int brackStart = propertyName.IndexOf("[");
                    int brackEnd = propertyName.IndexOf("]");

                    property = currentType.GetProperty(brackStart > 0 ? propertyName.Substring(0, brackStart) : propertyName);
                    obj = property.GetValue(obj, null);

                    if (brackStart > 0)
                    {
                        string index = propertyName.Substring(brackStart + 1, brackEnd - brackStart - 1);
                        foreach (Type iType in obj.GetType().GetInterfaces())
                        {
                            if (iType.IsGenericType && iType.GetGenericTypeDefinition() == typeof(IDictionary<,>))
                            {
                                obj = typeof(ReflectorUtils).GetMethod("GetDictionaryElement")
                                                     .MakeGenericMethod(iType.GetGenericArguments())
                                                     .Invoke(null, new object[] { obj, index });
                                break;
                            }
                            if (iType.IsGenericType && iType.GetGenericTypeDefinition() == typeof(IEnumerable<>))
                            {
                                obj = typeof(ReflectorUtils).GetMethod("GetListElement")
                                                     .MakeGenericMethod(iType.GetGenericArguments())
                                                     .Invoke(null, new object[] { obj, index });
                                break;
                            }
                        }
                    }

                    currentType = obj != null ? obj.GetType() : null; //property.PropertyType;
                }
                else return null;
            }
            return obj;
        }
        
        /// <summary>
        /// Получим объект типа TValue из словаря по индексу
        /// </summary>
        /// <typeparam name="TKey">Тип ключей словаря</typeparam>
        /// <typeparam name="TValue">Тип значений словаря</typeparam>
        /// <param name="dict">Сам словарь</param>
        /// <param name="index">Индекс объекта в словаре</param>
        /// <returns>Объект типа TValue</returns>
        public TValue GetDictionaryElement<TKey, TValue>(IDictionary<TKey, TValue> dict, object index)
        {
            TKey key = (TKey)Convert.ChangeType(index, typeof(TKey), null);
            return dict[key];
        }
        
        /// <summary>
        /// Получим объект из списка по индексу
        /// </summary>
        /// <typeparam name="T">Тип объектов, которые содержит список</typeparam>
        /// <param name="list">Сам список</param>
        /// <param name="index">Индекс объекта в списке</param>
        /// <returns>Объект типа T</returns>
        public T GetListElement<T>(IEnumerable<T> list, object index)
        {
            return (T)list.ElementAt(Convert.ToInt32(index));
        }

    }
}