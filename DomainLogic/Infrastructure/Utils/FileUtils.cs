﻿using DomainLogic.Infrastructure.Credentials;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace DomainLogic.Infrastructure.Utils
{
    /// <summary>
    /// Класс содержит методы для работы с файлами и директорями
    /// </summary>
    public class FileUtils
    {        
        /// <summary>
        /// Получает названия файлов из указанной директории
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="ftpPath"></param>
        /// <returns></returns>
        public List<string> GetFilesNamesByFtp(NetCredential credential, string ftpPath)
        {
            List<string> files = new List<string>();
            FtpWebRequest request = FtpWebRequest.Create(ftpPath) as FtpWebRequest;
            request.Method = WebRequestMethods.Ftp.ListDirectory;
            request.Credentials = new NetworkCredential(credential.UserName, credential.Password);
            using (FtpWebResponse response = request.GetResponse() as FtpWebResponse)
            using (Stream responseStream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(responseStream))
            {
                while (!reader.EndOfStream)
                    files.Add(reader.ReadLine());
            }
            return files;
        }
                  
        /// <summary>
        /// Копирует папку с файлами
        /// </summary>
        /// <param name="FromDir"></param>
        /// <param name="ToDir"></param>
        public void CopyDir(string FromDir, string ToDir)
        {
            Directory.CreateDirectory(ToDir);
            
            foreach (string fullName in Directory.GetFiles(FromDir))
                File.Copy(fullName, ToDir + "\\" + Path.GetFileName(fullName));

            foreach (string fullName in Directory.GetDirectories(FromDir))
                CopyDir(fullName, ToDir + "\\" + Path.GetFileName(fullName));
        }
    }
}