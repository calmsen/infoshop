﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLogic.Infrastructure.ServiceContainers
{
    public interface IBindingModule
    {
        void Load(IServiceContainer serviceContainer);
    }
}
