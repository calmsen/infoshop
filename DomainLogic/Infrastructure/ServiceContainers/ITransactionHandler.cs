﻿using System;

namespace DomainLogic.Infrastructure.ServiceContainers
{
    public interface ITransactionHandler
    {
        void Invoke(Action action, object target);
    }
}
