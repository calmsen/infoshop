﻿using System;

namespace DomainLogic.Infrastructure.ServiceContainers
{
    public enum BindableType
    {
        Self = 1,
        DefaultInterface = 2,
        AllInterfaces = 4
    }
}
