﻿
using System;
using System.Collections.Generic;

namespace DomainLogic.Infrastructure.ServiceContainers
{
    public interface IServiceContainer
    {
        void Init();
        void Load(params IBindingModule[] modules);
        void Load(params string[] bindingModulePathsToXml);
        void Bind<TService, KService>() where KService: TService;        
        void Bind<TService, KService>(KService service) where KService: TService;
        void Bind<TService>(Predicate<Type> selector);
        void Bind<TService>();
        void Bind<TService>(TService service);
        void BindInNamespaceOf<TNamespace>(BindableType bindableType = BindableType.Self, Type[] excluding = null);
        void BindInNamespaceOf<TNamespace1, TNamespace2>(BindableType bindableTypes = BindableType.Self, Type[] excluding = null);
        void BindInNamespaceOf<TNamespace1, TNamespace2, TNamespace3>(BindableType bindableTypes = BindableType.Self, Type[] excluding = null);
        void BindInNamespaceOf<TNamespace1, TNamespace2, TNamespace3, TNamespace4>(BindableType bindableTypes = BindableType.Self, Type[] excluding = null);
        void BindInNamespaceOf<TNamespace1, TNamespace2, TNamespace3, TNamespace4, TNamespace5>(BindableType bindableTypes = BindableType.Self, Type[] excluding = null);
        TService Get<TService>();
        object Get(Type serviceType);
        IEnumerable<object> GetAll(Type serviceType);
    }
}
