﻿using Ninject.Extensions.Interception;

namespace DomainLogic.Infrastructure.ServiceContainers.Ninject
{
    /// <summary>
    /// Перехватчик методов аннотированных атрибутом Transaction
    /// </summary>
    public class TransactionInterceptor : IInterceptor
    {
        /// <summary>
        /// Обработчик транзакций
        /// </summary>
        private ITransactionHandler _transactionHandler;

        /// <summary>
        /// Инициазирует экземпляр класса
        /// </summary>
        /// <param name="transactionHandler"></param>
        public TransactionInterceptor(ITransactionHandler transactionHandler)
        {
            _transactionHandler = transactionHandler;
        }

        /// <summary>
        /// Перехватчик вызываемого метода
        /// </summary>
        /// <param name="invocation">Вызываемый метод</param>
        public void Intercept(IInvocation invocation)
        {
            _transactionHandler.Invoke(invocation.Proceed, invocation.Request.Target);
        }
    }
}
