﻿using DomainLogic.Infrastructure.Attributes;
using Ninject;
using Ninject.Extensions.Conventions;
using Ninject.Extensions.Conventions.Syntax;
using Ninject.Extensions.Xml;
using Ninject.Planning.Strategies;
using Ninject.Syntax;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace DomainLogic.Infrastructure.ServiceContainers.Ninject
{
    public class ServiceContainer : IServiceContainer
    {
        private IKernel _kernel;

        public void Init()
        {
            _kernel = new StandardKernel(
                new NinjectSettings
                {
                    InjectAttribute = typeof(Attributes.InjectAttribute),
                    LoadExtensions = true
                }
            );

            _kernel.Load();
            _kernel.Load(Assembly.GetExecutingAssembly());
            //_kernel.Load(new XmlExtensionModule());
            //_kernel.Components.Add<IPlanningStrategy, InterceptorRegistrationStrategy<TransactionAttribute, NoTransactionAttribute, TransactionInterceptor>>();
        }

        public void Load(params IBindingModule[] modules)
        {
            foreach (var module in modules)
            {
                module.Load(this);
            }
        }

        public void Load(params string[] bindingModulePathsToXml)
        {
            foreach(var bindingModulePathToXml in bindingModulePathsToXml)
            {
                _kernel.Load(bindingModulePathToXml);
            }
        }

        public void Bind<TService, KService>() where KService : TService
        {
            _kernel.Unbind<TService>();
            var bindable = _kernel.Bind<TService>().To<KService>();
            bindable.InSingletonScope();
        }

        public void Bind<TService, KService>(KService service) where KService : TService
        {
            _kernel.Unbind<TService>();
            _kernel.Bind<TService>().ToConstant(service).InSingletonScope();
        }

        public void Bind<TService>()
        {
            _kernel.Unbind<TService>();
            _kernel.Bind<TService>().ToSelf().InSingletonScope();
        }

        public void Bind<TService>(TService service)
        {
            _kernel.Unbind<TService>();
            _kernel.Bind<TService>().ToConstant(service).InSingletonScope();
        }

        public void Bind<TService>(Predicate<Type> selector)
        {
            _kernel.Bind(x => x
            .FromAssemblyContaining<TService>()
            .SelectAllClasses()
            .InheritedFrom<TService>()
            .BindSelection((type, baseTypes) => {
                if (!selector(type))
                    return new Type[0];
                return new Type[] { typeof(TService) };
            })
            .Configure(b =>
                b.InSingletonScope()
            ));
        }

        public void BindInNamespaceOf<TNamespace>(BindableType bindableType = BindableType.Self, Type[] excluding = null)
        {
            BindInNamespaceOf<TNamespace>(
                x => x.InNamespaceOf<TNamespace>(), 
                bindableType, 
                excluding
            );
        }

        public void BindInNamespaceOf<TNamespace1, TNamespace2>(BindableType bindableType = BindableType.Self, Type[] excluding = null)
        {
            BindInNamespaceOf<TNamespace1>(
                x => x
                    .InNamespaceOf<TNamespace1>()
                    .InNamespaceOf<TNamespace2>(), 
                bindableType, 
                excluding
            );
        }

        public void BindInNamespaceOf<TNamespace1, TNamespace2, TNamespace3>(BindableType bindableType = BindableType.Self, Type[] excluding = null)
        {
            BindInNamespaceOf<TNamespace1>(
                x => x
                    .InNamespaceOf<TNamespace1>()
                    .InNamespaceOf<TNamespace2>()
                    .InNamespaceOf<TNamespace3>(),
                bindableType,
                excluding
            );
        }

        public void BindInNamespaceOf<TNamespace1, TNamespace2, TNamespace3, TNamespace4>(BindableType bindableType = BindableType.Self, Type[] excluding = null)
        {
            BindInNamespaceOf<TNamespace1>(
                x => x
                    .InNamespaceOf<TNamespace1>()
                    .InNamespaceOf<TNamespace2>()
                    .InNamespaceOf<TNamespace3>()
                    .InNamespaceOf<TNamespace4>(),
                bindableType,
                excluding
            );
        }

        public void BindInNamespaceOf<TNamespace1, TNamespace2, TNamespace3, TNamespace4, TNamespace5>(BindableType bindableType = BindableType.Self, Type[] excluding = null)
        {
            BindInNamespaceOf<TNamespace1>(
                x => x
                    .InNamespaceOf<TNamespace1>()
                    .InNamespaceOf<TNamespace2>()
                    .InNamespaceOf<TNamespace3>()
                    .InNamespaceOf<TNamespace4>()
                    .InNamespaceOf<TNamespace5>(),
                bindableType,
                excluding
            );
        }

        public TService Get<TService>()
        {
            return _kernel.Get<TService>();
        }

        public object Get(Type serviceType)
        {
            return _kernel.Get(serviceType);
        }

        public IEnumerable<object> GetAll(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }

        private void BindInNamespaceOf<TNamespace>(Func<IFilterSyntax, IFilterSyntax> namesapceProcessor, BindableType bindableType = BindableType.Self, Type[] excluding = null)
        {
            _kernel.Bind(x => {
                IFluentSyntax fluentSyntax = x.FromAssemblyContaining<TNamespace>().SelectAllClasses();

                fluentSyntax = namesapceProcessor(fluentSyntax as IFilterSyntax);

                if (excluding != null && excluding.Length > 0)
                    fluentSyntax = (fluentSyntax as IExcludeSyntax).Excluding(excluding);

                switch (bindableType)
                {
                    case BindableType.DefaultInterface:
                        fluentSyntax = (fluentSyntax as IBindSyntax).BindDefaultInterface();
                        break;
                    case BindableType.AllInterfaces:
                        fluentSyntax = (fluentSyntax as IBindSyntax).BindAllInterfaces();
                        break;
                    default:
                        fluentSyntax = (fluentSyntax as IBindSyntax).BindToSelf();
                        break;
                }
                (fluentSyntax as IConfigureSyntax).Configure(b => b.InSingletonScope());
            });
        }
    }
}
