﻿using DomainLogic.Interfaces.Localization;

namespace DomainLogic.Infrastructure.Localization
{
    /// <summary>
    /// Локализует значение
    /// </summary>
    public class ValueLocalizer : IValueLocalizer
    {
        /// <summary>
        /// Локализует значение в соответсвии текущей локали или возвращает значение по умолчанию
        /// </summary>
        /// <param name="defaultCultureValue"></param>
        /// <param name="otherCultureValue"></param>
        /// <returns></returns>
        public string LocalizedValue(string defaultCultureValue, string otherCultureValue)
        {
            bool isRu = System.Threading.Thread.CurrentThread.CurrentCulture.Name.Equals("ru");
            return !isRu && !string.IsNullOrEmpty(otherCultureValue) ? otherCultureValue : defaultCultureValue;
        }

        /// <summary>
        /// Локализует значение в соответсвии текущей локали или возвращает значение по умолчанию
        /// </summary>
        /// <param name="defaultCultureValue"></param>
        /// <param name="otherCultureValue"></param>
        /// <returns></returns>
        public long? LocalizedValue(long? defaultCultureValue, long? otherCultureValue)
        {
            bool isRu = System.Threading.Thread.CurrentThread.CurrentCulture.Name.Equals("ru");
            return !isRu && otherCultureValue > 0 ? otherCultureValue : defaultCultureValue;
        }
    }   
}
