﻿using DomainLogic.Infrastructure.QueryBuilder;
using DomainLogic.Interfaces.QueryBuilder;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Infrastructure.Localization
{
    /// <summary>
    /// Декоратор билдера запросов для локализации. Переопределяет методы получения объектов, локализуя объекты перед возвращением.
    /// </summary>
    public class LocalizationQueryBuilderDecorator : AQueryBuilderDecorator
    {
        /// <summary>
        /// Работа с локализацией
        /// </summary>
        private readonly AggregateLocalizer _localizer;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="queryBuilder"></param>
        /// <param name="localizer"></param>
        public LocalizationQueryBuilderDecorator(IQueryBuilder queryBuilder, AggregateLocalizer localizer)
            : base(queryBuilder)
        {
            _localizer = localizer;
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <returns></returns>
        public override async Task<object> FirstOrDefaultAsync()
        {
            var item = await base.FirstOrDefaultAsync();
            _localizer.LocalizeItem(item);
            _localizer.Clear();
            return item;
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public override async Task<object> FirstOrDefaultAsync(string predicate, params object[] values)
        {
            var list = await base.ToListAsync(predicate, values);
            _localizer.LocalizeList(list);
            _localizer.Clear();
            return list;
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <returns></returns>
        public override async Task<List<object>> ToListAsync()
        {
            var list = await base.ToListAsync();
            _localizer.LocalizeList(list);
            _localizer.Clear();
            return list;
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public override async Task<List<object>> ToListAsync(string predicate, params object[] values)
        {
            var list = await base.ToListAsync(predicate, values);
            _localizer.LocalizeList(list);
            _localizer.Clear();
            return list;
        }
    }
}
