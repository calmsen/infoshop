﻿using DomainLogic.Interfaces.Localization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DomainLogic.Infrastructure.Localization
{
    /// <summary>
    /// Представляет агрегатор всех локализаторов
    /// </summary>
    public class AggregateLocalizer
    {
        private readonly LocalizersProvider _localizersProvider;

        /// <summary>
        /// Список локализованных объектов. 
        /// </summary>
        private readonly List<object> _localizedItems = new List<object>();

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="localizersProvider"></param>
        public AggregateLocalizer(LocalizersProvider localizersProvider)
        {
            _localizersProvider = localizersProvider;
            _localizersProvider.SetAggregateLocalizer(this);
        }

        /// <summary>
        /// Очищает список локализованных объектов
        /// </summary>
        public void Clear()
        {
            _localizedItems.Clear();
        }

        /// <summary>
        /// Локализует объект
        /// </summary>
        /// <param name="elementType"></param>
        /// <param name="item"></param>
        public void LocalizeItem(Type elementType, object item)
        {
            if (item == null)
                return;
            var localizer = _localizersProvider.Get(elementType.Name) as dynamic;
            if (localizer == null)
                return;
            if (_localizedItems.Any(x => Equals(x, item)))
                return;
            _localizedItems.Add(item);
            localizer.Localize(item);
        }

        /// <summary>
        /// Локализует объект
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="item"></param>
        public void LocalizeItem<TElement>(TElement item)
        {
            if (item == null)
                return;
            var localizer = _localizersProvider.Get(typeof(TElement).Name) as IDomainLocalizer<TElement>;
            if (localizer == null)
                return;
            if (_localizedItems.Any(x => Equals(x, item)))
                return;
            localizer.Localize(item);
            _localizedItems.Add(item);
        }

        /// <summary>
        /// Локализует список объектов
        /// </summary>
        /// <param name="elementType"></param>
        /// <param name="list"></param>
        public void LocalizeList(Type elementType, List<object> list)
        {
            var localizer = _localizersProvider.Get(elementType.Name) as dynamic;
            if (localizer == null)
                return;
            foreach(var item in list)
            {
                if (_localizedItems.Any(x => Equals(x, item)))
                    return;
                _localizedItems.Add(item);
                localizer.Localize(item);
            }
        }

        /// <summary>
        /// Локализует список объектов
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="list"></param>
        public void LocalizeList<TElement>(List<TElement> list)
        {
            var localizer = _localizersProvider.Get(typeof(TElement).Name) as IDomainLocalizer<TElement>;
            if (localizer == null)
                return;
            foreach (var item in list)
            {
                if (_localizedItems.Any(x => Equals(x, item)))
                    return;
                _localizedItems.Add(item);
                localizer.Localize(item);
            }
        }
    }
}
