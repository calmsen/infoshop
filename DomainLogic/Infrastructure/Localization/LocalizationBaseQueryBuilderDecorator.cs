﻿using DomainLogic.Infrastructure.QueryBuilder;
using DomainLogic.Interfaces.QueryBuilder;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Infrastructure.Localization
{
    /// <summary>
    /// Декоратор базового билдера запросов для локализации. Переопределяет методы получения объектов, локализуя объекты перед возвращением.
    /// </summary>
    public class LocalizationBaseQueryBuilderDecorator : ABaseQueryBuilderDecorator
    {

        /// <summary>
        /// Работа с локализацией
        /// </summary>
        private readonly AggregateLocalizer _localizer;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="queryBuilder"></param>
        /// <param name="localizer"></param>
        public LocalizationBaseQueryBuilderDecorator(IBaseQueryBuilder queryBuilder, AggregateLocalizer localizer)
            :base(queryBuilder)
        {
            _localizer = localizer;
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <param name="elementType"></param>
        /// <param name="fields"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public override async Task<object> FirstOrDefaultAsync(Type elementType, string[] fields = null, object where = null)
        {
            var item = await base.FirstOrDefaultAsync(elementType, fields, where);
            _localizer.LocalizeItem(item);
            _localizer.Clear();
            return item;
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="fields"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public override async Task<TElement> FirstOrDefaultAsync<TElement>(string[] fields, object where = null)
        {
            var item = await base.FirstOrDefaultAsync<TElement>(fields, where);
            _localizer.LocalizeItem(item);
            _localizer.Clear();
            return item;
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="where"></param>
        /// <returns></returns>
        public override async Task<TElement> FirstOrDefaultAsync<TElement>(object where = null)
        {
            var item = await base.FirstOrDefaultAsync<TElement>(where);
            _localizer.LocalizeItem(item);
            _localizer.Clear();
            return item;
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <returns></returns>
        public override async Task<TElement> FirstOrDefaultAsync<TElement>()
        {
            var item = await base.FirstOrDefaultAsync<TElement>();
            _localizer.LocalizeItem(item);
            _localizer.Clear();
            return item;
        }

        /// <summary>
        /// Выполняет sql запрос для получения одной записи 
        /// </summary>
        /// <typeparam name="TTargetElement"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public override async Task<TTargetElement> ToTargetItemAsync<TTargetElement>(string sql, params object[] parameters)
        {
            var item = await base.ToTargetItemAsync<TTargetElement>(sql, parameters);
            _localizer.LocalizeItem(item);
            _localizer.Clear();
            return item;
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <param name="elementType"></param>
        /// <param name="fields"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public override async Task<List<object>> ToListAsync(Type elementType, string[] fields = null, object where = null)
        {
            var list = await base.ToListAsync(elementType, fields, where);
            _localizer.LocalizeList(elementType, list);
            _localizer.Clear();
            return list;
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="fields"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public override async Task<List<TElement>> ToListAsync<TElement>(string[] fields, object where = null)
        {
            var list = await base.ToListAsync<TElement>(fields, where);
            _localizer.LocalizeList(list);
            _localizer.Clear();
            return list;
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="where"></param>
        /// <returns></returns>
        public override async Task<List<TElement>> ToListAsync<TElement>(object where = null)
        {
            var list = await base.ToListAsync<TElement>(where);
            _localizer.LocalizeList(list);
            _localizer.Clear();
            return list;
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <returns></returns>
        public override async Task<List<TElement>> ToListAsync<TElement>()
        {
            var list = await base.ToListAsync<TElement>();
            _localizer.LocalizeList(list);
            _localizer.Clear();
            return list;
        }

        /// <summary>
        /// Выполняет sql запрос для получения списка записей
        /// </summary>
        /// <typeparam name="TTargetElement"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public override async Task<List<TTargetElement>> ToTargetListAsync<TTargetElement>(string sql, params object[] parameters)
        {
            var list = await base.ToTargetListAsync<TTargetElement>(sql, parameters);
            _localizer.LocalizeList(list);
            _localizer.Clear();
            return list;
        }
    }
}
