﻿
using DomainLogic.Infrastructure.Providers;
using DomainLogic.Interfaces.Localization;
using System.Collections.Generic;

namespace DomainLogic.Infrastructure.Localization
{
    /// <summary>
    /// Провайдер для получения локализаторов объектов (н-р для локализации Product, Section и тп.)
    /// </summary>
    public class LocalizersProvider : Provider<IDomainLocalizer>
    {
        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="localizers"></param>
        /// <param name="factories"></param>
        public LocalizersProvider(IEnumerable<IDomainLocalizer> localizers, IEnumerable<IFactory<EmptyContext, IDomainLocalizer>> factories)
            : base(localizers, factories)
        {
        }

        /// <summary>
        /// Устанавливает каждому локализатору агрегатор локализаторов, для того чтобы локализаторы запускали локализацию для вложенных объектов
        /// </summary>
        /// <param name="aggregateLocalizer"></param>
        public virtual void SetAggregateLocalizer(AggregateLocalizer aggregateLocalizer)
        {
            foreach(var item in _items)
            {
                if(item.Value is IAggregateLocalizerHolder)
                    ((IAggregateLocalizerHolder)item.Value).AggregateLocalizer = aggregateLocalizer;
            }
        }
    }
}
