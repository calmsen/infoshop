﻿using DomainLogic.Infrastructure.QueryBuilder;
using DomainLogic.Interfaces.QueryBuilder;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DomainLogic.Infrastructure.Localization
{
    /// <summary>
    /// Декоратор билдера запросов для локализации. Переопределяет методы получения объектов, локализуя объекты перед возвращением.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class LocalizationQueryBuilderDecorator<TEntity> : AQueryBuilderDecorator<TEntity>
    {

        /// <summary>
        /// Работа с локализацией
        /// </summary>
        private readonly AggregateLocalizer _localizer;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="queryBuilder"></param>
        /// <param name="localizer"></param>
        /// <param name="queryBuilderFactory"></param>
        public LocalizationQueryBuilderDecorator(IQueryBuilder<TEntity> queryBuilder, AggregateLocalizer localizer, IQueryBuilderFactory queryBuilderFactory)
            : base(queryBuilder, queryBuilderFactory)
        {
            _localizer = localizer;
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public override TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            var item = base.FirstOrDefault(predicate);
            _localizer.LocalizeItem(item);
            _localizer.Clear();
            return item;
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <returns></returns>
        public override async Task<TEntity> FirstOrDefaultAsync()
        {
            var item = await base.FirstOrDefaultAsync();
            _localizer.LocalizeItem(item);
            _localizer.Clear();
            return item;
        }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public override async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var item = await base.FirstOrDefaultAsync(predicate);
            _localizer.LocalizeItem(item);
            _localizer.Clear();
            return item;
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <returns></returns>
        public override async Task<List<TEntity>> ToListAsync()
        {
            var list = await base.ToListAsync();
            _localizer.LocalizeList(list);
            _localizer.Clear();
            return list;
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public override async Task<List<TEntity>> ToListAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var list = await base.ToListAsync(predicate);
            _localizer.LocalizeList(list);
            _localizer.Clear();
            return list;
        }

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="selector"></param>
        /// <returns></returns>
        public override async Task<List<TElement>> ToListAsync<TElement>(Expression<Func<TEntity, TElement>> selector)
        {
            var list = await base.ToListAsync(selector);
            _localizer.LocalizeList(list);
            _localizer.Clear();
            return list;
        }
    }
}
