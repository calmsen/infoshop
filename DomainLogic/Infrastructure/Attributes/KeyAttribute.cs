﻿using System;

namespace DomainLogic.Infrastructure.Attributes
{
    /// <summary>
    /// Под данному ключу регистрируется объект в Provider<TContext, TItem>. 
    /// Так же подразумевается, что данный атрибут можно использовать и для других нужд.
    /// </summary>
    public class KeyAttribute : Attribute
    {
        public string Key { get; }

        public KeyAttribute(string key)
        {
            Key = key;
        }
    }
}
