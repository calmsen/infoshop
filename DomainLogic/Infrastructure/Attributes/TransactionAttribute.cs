﻿using System;

namespace DomainLogic.Infrastructure.Attributes
{
    /// <summary>
    /// Аннотирует метод/класс для использования транзакции
    /// </summary>
    public class TransactionAttribute : Attribute
    {
    }
}