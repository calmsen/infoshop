﻿using System;

namespace DomainLogic.Infrastructure.Attributes
{
    /// <summary>
    /// Аннотирует что класс ответчает за кэширование
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class CachableAttribute : Attribute
    {
    }
}