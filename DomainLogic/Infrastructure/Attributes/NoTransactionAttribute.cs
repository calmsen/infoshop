﻿using System;

namespace DomainLogic.Infrastructure.Attributes
{
    /// <summary>
    /// Аннотирует метод/класс для отмены транзакции
    /// </summary>
    public class NoTransactionAttribute : Attribute
    {
    }
}