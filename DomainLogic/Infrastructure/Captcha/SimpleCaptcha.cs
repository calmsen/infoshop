﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace DomainLogic.Infrastructure.Captcha
{
    /// <summary>
    /// Класс для генерации простой каптчи
    /// </summary>
    public class SimpleCaptcha : ACaptcha
    {
        /// <summary>
        /// Генерирует каптчу и записывает в stream
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public override string Generate(Stream stream) 
        {

            string captcha = GenerateRandomString();

            int height = 40;
            int width = 160;

            using (Bitmap bmp = new Bitmap(width, height))
            {                 
                RectangleF rectf = new RectangleF(30, 5, 0, 0);

                using (Graphics g = Graphics.FromImage(bmp))
                {
                    g.Clear(Color.White);
                    g.SmoothingMode = SmoothingMode.AntiAlias;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    g.DrawString(captcha, new Font("Georgia", 18, FontStyle.Italic), Brushes.Aquamarine, rectf);
                    g.DrawRectangle(new Pen(Color.BurlyWood), 1, 1, width - 2, height - 2);
                    g.Flush();
                    bmp.Save(stream, ImageFormat.Jpeg);
                }
            }
            return captcha;
        }        
    }
}