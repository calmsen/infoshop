﻿using DomainLogic.Infrastructure.ImageGeneration;
using System.Drawing.Imaging;
using System.IO;

namespace DomainLogic.Infrastructure.Captcha
{
    /// <summary>
    /// Класс для генерации каптчи по умолчанию
    /// </summary>
    public class DefaultCaptcha : ACaptcha
    {
        /// <summary>
        /// Генерирует каптчу и записывает в stream
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public override string Generate(Stream stream)
        {
            string captcha = GenerateRandomString();
            var bmp = new DefaultImageGenerator()
                .Generate(new DefaultDrawingModel(captcha));
            bmp.Save(stream, ImageFormat.Jpeg);
            bmp.Dispose();
            return captcha;
        }
    }
}