﻿using DomainLogic.Interfaces;
using System;
using System.IO;
using System.Text;

namespace DomainLogic.Infrastructure.Captcha
{
    /// <summary>
    ///Базовый класс для генератора каптч
    /// </summary>
    public abstract class ACaptcha : ICaptcha
    {
        /// <summary>
        /// Генерирует каптчу и записывает в stream
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public abstract string Generate(Stream stream);

        /// <summary>
        /// Генерирует случайную символьную строку
        /// </summary>
        /// <returns></returns>
        protected virtual string GenerateRandomString()
        {
            Random random = new Random();
            string combination = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 6; i++)
                sb.Append(combination[random.Next(combination.Length)]);

            return sb.ToString();
        }
    }
}