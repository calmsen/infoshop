﻿
namespace DomainLogic.Infrastructure.Pdf
{
    /// <summary>
    /// Класс содержит пути до пользовательских шрифтов
    /// </summary>
    public static class FontFamily
    {
        public const string Tahoma = @"c:\Windows\Fonts\tahoma.ttf";
        public const string TahomaBd = @"c:\Windows\Fonts\tahomabd.ttf";
        public const string Arial = @"c:\Windows\Fonts\arial.ttf";
        public const string ArialBd = @"c:\Windows\Fonts\arialbd.ttf";
    }
}
