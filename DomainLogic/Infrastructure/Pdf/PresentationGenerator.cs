﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DomainLogic.Infrastructure.Pdf
{
    /// <summary>
    /// Создает pdf презентации
    /// </summary>
    public class PresentationGenerator
    {
        /// <summary>
        /// Количество элементов в строке
        /// </summary>
        private int _numberItemsInRow;

        /// <summary>
        /// всего элементов
        /// </summary>
        private int _numberItems;  

        /// <summary>
        /// Элемент каталога
        /// </summary>
        private PresentationItem[] _items;

        /// <summary>
        /// Кэш шрифтов
        /// </summary>
        private Dictionary<string, Font> _fontsCache = new Dictionary<string, Font>(); 

        /// <summary>
        /// Инициализирует экземпляр класса PresentationGenerator
        /// </summary>
        /// <param name="items"></param>
        /// <param name="numberItemsInRow"></param>
        public PresentationGenerator(PresentationItem[] items, int numberItemsInRow)
        {
            _numberItemsInRow = numberItemsInRow;
            _numberItems = items.Length;
            _items = items;
        }

        /// <summary>
        /// Создает презентацию
        /// </summary>
        /// <param name="mainPageImagePath">Путь до картинки на титульном листе</param>
        /// <param name="mainPageHeader">Заголовок презентации</param>
        /// <param name="logoImagePath">Путь до картинки логотипа</param>
        /// <param name="catalogName">Название раздела</param>
        /// <returns></returns>
        public byte[] GeneratePresentation(string mainPageImagePath, string mainPageHeader, string logoImagePath, string catalogName)
        {
            using (MemoryStream ms = new MemoryStream())
            using (var doc = new Document())
            {
                // настройки для главной страницы
                SetPageSettings(doc, new BaseColor(37, 168, 81));
                PdfWriter.GetInstance(doc, ms);
                doc.Open();
                // фон для картинки
                SetBackgroundImage(doc, mainPageImagePath);
                // настройки для страницы каталога
                SetPageSettings(doc, BaseColor.WHITE);
                // добавляем главную страницу
                AddMainPage(doc, mainPageHeader, logoImagePath, catalogName);
                doc.NewPage();
                // добавляем страницу каталога
                List<PresentationItem[]> rows = SplitItems();
                for (int i = 0, page = 2; i < rows.Count(); i += 2, page++)
                {
                    AddCatalogPage(doc, rows[i], (i + 1 < rows.Count() ? rows[i + 1] : new PresentationItem[0]), page, logoImagePath, catalogName);
                    doc.NewPage();
                }
                doc.Close();
                //
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Настраивает страницу: цвет, размер и др
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="color"></param>
        private void SetPageSettings(Document doc, BaseColor color)
        {
            Rectangle rec = new Rectangle(PageSize.A4.Rotate());
            rec.BackgroundColor = color;
            doc.SetPageSize(rec);
            doc.SetMargins(0, 0, 0, 0);
        }

        /// <summary>
        /// Установливает фона на главной странице
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="imagePath"></param>
        private void SetBackgroundImage(Document doc, string imagePath)
        {
            if (!File.Exists(imagePath))
                return;
            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);
            image.ScaleToFit(842, 595);
            image.SetAbsolutePosition(0, 0);
            doc.Add(image);
        }

        /// <summary>
        /// Добавляет текст в таблицу
        /// </summary>
        /// <param name="text"></param>
        /// <param name="table"></param>
        /// <param name="paddingTop"></param>
        /// <param name="paddingRight"></param>
        /// <param name="paddingBottom"></param>
        /// <param name="paddingLeft"></param>
        /// <param name="fontFamily"></param>
        /// <param name="fontSize"></param>
        /// <param name="color"></param>
        /// <param name="rotation"></param>
        /// <param name="backgroundColor"></param>
        /// <param name="align"></param>
        /// <param name="valign"></param>
        /// <param name="height"></param>
        private void AddTextToTable(string text, PdfPTable table, float paddingTop = 0f, float paddingRight = 0f, float paddingBottom = 0f, float paddingLeft = 0f, string fontFamily = FontFamily.Arial, int fontSize = 14, BaseColor color = null, int rotation = 0, BaseColor backgroundColor = null, int align = 4, int valign = 0, float height = 0)
        {
            // переопределяем парамптры по умолчанию
            color = color ?? BaseColor.BLACK;
            // создает или получает шрифт
            string fontKey = fontFamily + ";" + fontSize + ";" + color.RGB;
            if (!_fontsCache.ContainsKey(fontKey))
                _fontsCache[fontKey] = new Font(BaseFont.CreateFont(fontFamily, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED), fontSize, Font.NORMAL, color);
            Font font = _fontsCache[fontKey];
            //
            PdfPCell cell = new PdfPCell(new Phrase(text, font));
            cell.PaddingTop = paddingTop;
            cell.PaddingRight = paddingRight;
            cell.PaddingBottom = paddingBottom;
            cell.PaddingLeft = paddingLeft;
            cell.Border = 0;
            cell.Rotation = rotation;
            if (valign != 0)
                cell.VerticalAlignment = valign;
            if (align != 4)
                cell.HorizontalAlignment = align;
            if (backgroundColor != null)
                cell.BackgroundColor = backgroundColor;
            if (height > 0)
                cell.MinimumHeight = height;
            table.AddCell(cell);
        }

        /// <summary>
        /// Добавляет картинку в таблицу
        /// </summary>
        /// <param name="imagePath"></param>
        /// <param name="table"></param>
        /// <param name="scale"></param>
        /// <param name="paddingTop"></param>
        /// <param name="paddingRight"></param>
        /// <param name="paddingBottom"></param>
        /// <param name="paddingLeft"></param>
        /// <param name="backgroundColor"></param>
        private void AddImageToTable(string imagePath, PdfPTable table, float scale = 100f, float paddingTop = 0f, float paddingRight = 0f, float paddingBottom = 0f, float paddingLeft = 0f, BaseColor backgroundColor = null)
        {
            if (!File.Exists(imagePath))
            {
                AddTextToTable("NoPhoto", table);
                return;
            }
            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);
            image.ScalePercent(scale);
            PdfPCell cell = new PdfPCell(image);
            cell.PaddingTop = paddingTop;
            cell.PaddingRight = paddingRight;
            cell.PaddingBottom = paddingBottom;
            cell.PaddingLeft = paddingLeft;
            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            cell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
            cell.Border = 0;
            if (backgroundColor != null)
                cell.BackgroundColor = backgroundColor;
            table.AddCell(cell);
        }

        /// <summary>
        /// Добавляет новую страницу в документ
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="page"></param>
        /// <param name="logoImagePath"></param>
        /// <param name="catalogName"></param>
        /// <param name="action"></param>
        private void AddPage(Document doc, int page, string logoImagePath, string catalogName, Action<PdfPTable> action)
        {
            //
            PdfPTable table = new PdfPTable(2);
            table.WidthPercentage = 100;
            table.SetWidths(new float[] { 812, 30 });

            action(table);

            AddRightInfoToTable(table, page, logoImagePath, catalogName);

            doc.Add(table);
        }

        /// <summary>
        /// Добавляет главную страницу в документ
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="mainPageHeader"></param>
        /// <param name="logoImagePath"></param>
        /// <param name="catalogName"></param>
        private void AddMainPage(Document doc, string mainPageHeader, string logoImagePath, string catalogName)
        {
            AddPage(doc, 1, logoImagePath, catalogName, x =>
            {
                AddMainPageContentToTable(x, mainPageHeader);
            });
        }

        /// <summary>
        /// Добавляет страницу каталога в документ
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="row1"></param>
        /// <param name="row2"></param>
        /// <param name="page"></param>
        /// <param name="logoImagePath"></param>
        /// <param name="catalogName"></param>
        private void AddCatalogPage(Document doc, PresentationItem[] row1, PresentationItem[] row2, int page, string logoImagePath, string catalogName)
        {
            AddPage(doc, page, logoImagePath, catalogName, x =>
            {
                AddCatalogPageContentToTable(x, row1, row2);
            });

        }

        /// <summary>
        /// Добавляет содержимое страницы каталога в таблицу
        /// </summary>
        /// <param name="table"></param>
        /// <param name="row1"></param>
        /// <param name="row2"></param>
        private void AddCatalogPageContentToTable(PdfPTable table, PresentationItem[] row1, PresentationItem[] row2)
        {
            PdfPTable innerTable = new PdfPTable(_numberItemsInRow);
            innerTable.SetTotalWidth(new float[] { 270.66f, 270.66f, 270.66f });
            //
            AddItemsOfCurrentRow(row1, innerTable);
            if (row2.Length > 0)
                AddItemsOfCurrentRow(row2, innerTable);
            else
                for (int i = 3; i > 0; i--)
                    AddTextToTable("", innerTable, height: innerTable.CalculateHeights());
            //
            PdfPCell cell = new PdfPCell(innerTable);
            cell.Padding = 15;
            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            cell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
            cell.Border = 0;
            table.AddCell(cell);
        }

        /// <summary>
        /// Добавим содержимое главной страницы в таблицу
        /// </summary>
        /// <param name="table"></param>
        /// <param name="mainPageHeader"></param>
        private void AddMainPageContentToTable(PdfPTable table, string mainPageHeader)
        {
            PdfPTable innerTable = new PdfPTable(1);
            innerTable.WidthPercentage = 100;
            //
            AddTextToTable(mainPageHeader, innerTable, paddingTop: 100, paddingRight: 125, paddingLeft: 125, fontFamily: FontFamily.TahomaBd, fontSize: 32, color: BaseColor.WHITE);
            //
            /*AddImageToTable(@"d:\Temp\Images\pleer.png", innerTable, scale: 65f);*/
            //
            PdfPCell cell = new PdfPCell(innerTable);
            cell.Border = 0;
            table.AddCell(cell);
        }

        /// <summary>
        /// Добавляет информацию справа в таблицу
        /// </summary>
        /// <param name="table"></param>
        /// <param name="page"></param>
        /// <param name="logoImagePath"></param>
        /// <param name="catalogName"></param>
        private void AddRightInfoToTable(PdfPTable table, int page = 1, string logoImagePath = null, string catalogName = null)
        {
            //
            BaseColor color = BaseColor.WHITE;
            if (page > 1)
                color = BaseColor.BLACK;
            //
            PdfPTable innerTable = new PdfPTable(1);
            innerTable.SetTotalWidth(new float[] { 30f });
            //
            AddImageToTable(logoImagePath, innerTable, scale: 50, paddingTop: 36, paddingBottom: 36, backgroundColor: new BaseColor(68, 114, 196));
            //
            AddTextToTable("Catalogue", innerTable, paddingTop: 10, paddingRight: 14, paddingBottom: 10, fontFamily: FontFamily.TahomaBd, fontSize: 14, color: color, rotation: 270);
            //
            AddTextToTable(catalogName, innerTable, paddingTop: 10, paddingRight: 18, paddingBottom: 10, fontFamily: FontFamily.Tahoma, fontSize: 10, color: color, rotation: 270);
            //
            AddTextToTable("2014-2015", innerTable, paddingTop: 10, paddingRight: 20, paddingBottom: 10, fontFamily: FontFamily.Tahoma, fontSize: 8, color: color, rotation: 270);
            //                                   
            AddTextToTable("", innerTable, height: 595 - innerTable.CalculateHeights() - 30);        // 
            //
            if (page > 1)
                AddTextToTable(page.ToString(), innerTable, fontFamily: FontFamily.Arial, fontSize: 10, color: BaseColor.WHITE, backgroundColor: new BaseColor(68, 114, 196), align: PdfPCell.ALIGN_CENTER, valign: PdfPCell.ALIGN_MIDDLE, height: 30);
            //
            PdfPCell cell = new PdfPCell(innerTable);
            cell.Border = 0;
            table.AddCell(cell);
        }

        /// <summary>
        /// Добавляет картинку элемента в таблицу
        /// </summary>
        /// <param name="imagePath"></param>
        /// <param name="table"></param>
        /// <param name="scale"></param>
        /// <param name="paddingTop"></param>
        /// <param name="paddingRight"></param>
        /// <param name="paddingBottom"></param>
        /// <param name="paddingLeft"></param>
        /// <param name="backgroundColor"></param>
        private void AddItemImageToTable(string imagePath, PdfPTable table, float scale = 90f, float paddingTop = 0f, float paddingRight = 0f, float paddingBottom = 0f, float paddingLeft = 0f, BaseColor backgroundColor = null)
        {
            AddImageToTable(imagePath, table, scale, paddingTop, paddingRight, paddingBottom, paddingLeft, backgroundColor);
        }

        /// <summary>
        /// Добавляет заголовок каталога в таблицу
        /// </summary>
        /// <param name="text"></param>
        /// <param name="table"></param>
        /// <param name="paddingTop"></param>
        /// <param name="paddingRight"></param>
        /// <param name="paddingBottom"></param>
        /// <param name="paddingLeft"></param>
        /// <param name="fontFamily"></param>
        /// <param name="fontSize"></param>
        /// <param name="color"></param>
        /// <param name="rotation"></param>
        /// <param name="backgroundColor"></param>
        /// <param name="align"></param>
        /// <param name="valign"></param>
        private void AddItemHeaderToTable(string text, PdfPTable table, float paddingTop = 0f, float paddingRight = 0f, float paddingBottom = 0f, float paddingLeft = 0f, string fontFamily = FontFamily.ArialBd, int fontSize = 10, BaseColor color = null, int rotation = 0, BaseColor backgroundColor = null, int align = 4, int valign = 0)
        {
            AddTextToTable(text, table, paddingTop, paddingRight, paddingBottom, paddingLeft, fontFamily, fontSize, color, rotation, backgroundColor, align, valign);
        }

        /// <summary>
        /// Добавляет описание элемента в таблицу
        /// </summary>
        /// <param name="text"></param>
        /// <param name="table"></param>
        /// <param name="paddingTop"></param>
        /// <param name="paddingRight"></param>
        /// <param name="paddingBottom"></param>
        /// <param name="paddingLeft"></param>
        /// <param name="fontFamily"></param>
        /// <param name="fontSize"></param>
        /// <param name="color"></param>
        /// <param name="rotation"></param>
        /// <param name="backgroundColor"></param>
        /// <param name="align"></param>
        /// <param name="valign"></param>
        private void AddItemDescriptionToTable(string text, PdfPTable table, float paddingTop = 0f, float paddingRight = 0f, float paddingBottom = 0f, float paddingLeft = 0f, string fontFamily = FontFamily.Arial, int fontSize = 9, BaseColor color = null, int rotation = 0, BaseColor backgroundColor = null, int align = 4, int valign = 0)
        {
            AddTextToTable(text, table, paddingTop, paddingRight, paddingBottom, paddingLeft, fontFamily, fontSize, color, rotation, backgroundColor, align, valign);
        }

        /// <summary>
        /// Добавляет элементы в текущий ряд
        /// </summary>
        /// <param name="items"></param>
        /// <param name="table"></param>
        private void AddItemsOfCurrentRow(PresentationItem[] items, PdfPTable table)
        {
            if (items.Length > _numberItemsInRow)
                throw new ArgumentException("items must be less then " + _numberItemsInRow + ".");
            //
            foreach (PresentationItem item in items)
            {
                AddItemImageToTable(item.ImagePath, table, paddingRight: 15, paddingBottom: 15, paddingLeft: 15);
            }
            int n = _numberItemsInRow - items.Length;
            while (n-- > 0)
                AddTextToTable("", table);

            foreach (PresentationItem item in items)
            {
                AddItemHeaderToTable(item.Header, table, paddingRight: 15, paddingBottom: 15, paddingLeft: 15);
            }
            n = _numberItemsInRow - items.Length;
            while (n-- > 0)
                AddTextToTable("", table);
            foreach (PresentationItem item in items)
            {
                AddItemDescriptionToTable(item.Description, table, paddingRight: 15, paddingBottom: 15, paddingLeft: 15);
            }
            n = _numberItemsInRow - items.Length;
            while (n-- > 0)
                AddTextToTable("", table);
        }

        /// <summary>
        /// Разбивает элекменты каталога для вмещения в одну строку
        /// </summary>
        /// <returns></returns>
        private List<PresentationItem[]> SplitItems()
        {
            return _items
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / _numberItemsInRow)
                .Select(x => x.Select(v => v.Value).ToArray())
                .ToList();
        }
    } 
    
}