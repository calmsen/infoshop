﻿namespace DomainLogic.Infrastructure.Pdf
{
    /// <summary>
    /// Содержит информацию об элементе каталога
    /// </summary>
    public class PresentationItem
    {
        /// <summary>
        /// Путь до картинки
        /// </summary>
        public string ImagePath { get; set; }

        /// <summary>
        /// Заголовок
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }
    }
}
