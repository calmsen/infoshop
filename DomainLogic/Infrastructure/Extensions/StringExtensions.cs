﻿
using DomainLogic.Infrastructure.Utils;

namespace DomainLogic.Infrastructure.Extensions
{
    /// <summary>
    /// Расширение для работы со строками
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Работа со строками. Данный экземпляр определяется в момент инициализации приложения
        /// </summary>
        public static StringUtils StringUtils { get; set; }

        /// <summary>
        /// Удаляет подстроку с конца строки
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string TrimEnd(this string source, string value)
        {
            return StringUtils.TrimEnd(source, value);
        }
    }
}
