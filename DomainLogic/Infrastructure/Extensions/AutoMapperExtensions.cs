﻿using AutoMapper;
using System.Collections.Generic;

namespace DomainLogic.Infrastructure.Extensions
{
    /// <summary>
    /// Расширение содержит методы для работы с AutoMapper
    /// </summary>
    public static class AutoMapperExtensions
    {
        /// <summary>
        /// Преобразует объект entity в объект типа TEntity  
        /// </summary>
        /// <typeparam name="TEntity">тип, в который нужно преобразовать объект</typeparam>
        /// <param name="entity">объект, который будет преобразовываться</param>
        /// <returns>новый объект типа TEntity</returns>
        public static TEntity Map<TEntity>(this object entity)
            where TEntity : class
        {
            return Mapper.Map<TEntity>(entity);
        }

        /// <summary>
        /// Преобразует список объектов entities в объекты типа TEntity
        /// </summary>
        /// <typeparam name="TEntity">тип, в который нужно преобразовать объекты</typeparam>
        /// <param name="entities">объекты, которые будут преобразовываться</param>
        /// <returns>новые объекты типа TEntity</returns>
        public static List<TEntity> MapList<TEntity>(this object entities)
            where TEntity : class
        {
            return Mapper.Map<List<TEntity>>(entities);
        }
    }
}
