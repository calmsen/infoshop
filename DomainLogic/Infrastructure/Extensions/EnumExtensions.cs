﻿using DomainLogic.Services.Shared.Providers;

namespace DomainLogic.Infrastructure.Extensions
{
    /// <summary>
    /// Расширение содержит методы для работы с перечислениями
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Предоставляет названия для перечислений. Данный экземпляр определяется в момент инициализации приложения
        /// </summary>
        public static EnumTitlesProvider EnumTitlesProvider { get; set; }

        /// <summary>
        /// Получает название для перечисления
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        public static string GetTitle<TEnum>(this TEnum enumValue) where TEnum : struct
        {
            return EnumTitlesProvider.GetTitle(enumValue);
        }
    }
}
