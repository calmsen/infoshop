﻿using System.Collections.Generic;
using System.Reflection;
using System.Collections.Specialized;
using DomainLogic.Infrastructure.Utils;

namespace DomainLogic.Infrastructure.Extensions
{
    /// <summary>
    /// Расширение содержит методы для работы с коллекциями
    /// </summary>
    public static class CollectionExtensions
    {
        /// <summary>
        ///  Работа с коллекциями. Данный экземпляр определяется в момент инициализации приложения
        /// </summary>
        public static CollectionUtils CollectionUtils { get; set; }
        
        /// <summary>
        /// Разбивает список source на n списков
        /// </summary>
        /// <typeparam name="T">Тип объектов, которые содержит список</typeparam>
        /// <param name="source">Список объектов, который нужно разбить</param>
        /// <param name="size">Количестов списков, на которые нужно разбить входной список</param>
        /// <returns>Список списков объектов</returns>
        public static IEnumerable<IEnumerable<T>> Split<T>(this IEnumerable<T> source, int size)
        {
            return CollectionUtils.Split(source, size);
        }
        
        /// <summary>
        /// Разбивает список source на n списков
        /// </summary>
        /// <typeparam name="T">Тип объектов, которые содержит список</typeparam>
        /// <param name="source"></param>
        /// <param name="chunksize">Количестов списков, на которые нужно разбить входной список</param>
        /// <returns>Список списков объектов</returns>
        public static IEnumerable<IEnumerable<T>> Chunk<T>(this IEnumerable<T> source, int chunksize)
        {
            return CollectionUtils.Chunk(source, chunksize);
        }
        
        /// <summary>
        /// Преобразует словарь в объект типа T
        /// </summary>
        /// <typeparam name="T">Тип объекта, в который нужно преобразовать</typeparam>
        /// <param name="source">Словарь</param>
        /// <returns>Объект тип T</returns>
        public static T ToObject<T>(this IDictionary<string, object> source)
            where T : class, new()
        {
            return CollectionUtils.ToObject<T>(source);
        }
        
        /// <summary>
        /// Расширяет словарь values новыми значениями из словарей extValues
        /// </summary>
        /// <param name="values">Словарь, который нужно расширить</param>
        /// <param name="extValues">Словари, которые будут расширять словарь values</param>
        /// <returns>Словарь values</returns>
        public static Dictionary<string, object> Extend(this Dictionary<string, object> values, params Dictionary<string, object>[] extValues)
        {
            return CollectionUtils.Extend(values, extValues);
        }
        
        /// <summary>
        /// Преобразует объект source в словарь. 
        /// Внимание: метод следовала назвать ToDictionary, но такой метод уже есть!
        /// </summary>
        /// <param name="source">Объект, который будет преобразовываться</param>
        /// <param name="bindingAttr">Флаги, которые говарят какие свойства преобразовывать</param>
        /// <returns>Новый словарь</returns>
        public static Dictionary<string, object> AsDictionary(this object source, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
        {
            return CollectionUtils.AsDictionary(source, bindingAttr);
        }
        
        /// <summary>
        /// Преобразует объект source в словарь. 
        /// </summary>
        /// <param name="source">Объект, который будет преобразовываться</param>
        /// <param name="queryString">Коллекция значений для сверки на пустые значения</param>
        /// <param name="bindingAttr">Флаги, которые говарят какие свойства преобразовывать</param>
        /// <returns>Новый словарь</returns>
        public static Dictionary<string, object> ToDictionaryOfFormatedValues(this object source, NameValueCollection queryString = null, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
        {
            return CollectionUtils.ToDictionaryOfFormatedValues(source, queryString, bindingAttr);
        }
    }
}