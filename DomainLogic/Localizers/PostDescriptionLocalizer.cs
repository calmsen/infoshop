﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Interfaces.Localization;
using DomainLogic.Models;

namespace DomainLogic.Localizers
{
    /// <summary>
    /// Локализует домен PostDescription
    /// </summary>
    [Key(nameof(PostDescription))]
    public class PostDescriptionLocalizer : IDomainLocalizer<PostDescription>
    {
        /// <summary>
        /// Локализует значение в соответсвии текущей локали
        /// </summary>
        private IValueLocalizer _valueLocalizer;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="valueLocalizer"></param>
        public PostDescriptionLocalizer(IValueLocalizer valueLocalizer)
        {
            _valueLocalizer = valueLocalizer;
        }

        /// <summary>
        /// Локализует домен PostDescription
        /// </summary>
        /// <param name="domain"></param>
        public void Localize(PostDescription domain)
        {
            domain.Content = _valueLocalizer.LocalizedValue(domain.Content, domain.ContentIn);
        }
    }
}
