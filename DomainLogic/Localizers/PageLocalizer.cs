﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Localization;
using DomainLogic.Interfaces.Localization;
using DomainLogic.Models;

namespace DomainLogic.Localizers
{
    /// <summary>
    /// Локализует домен Page
    /// </summary>
    [Key(nameof(Page))]
    public class PageLocalizer : IDomainLocalizer<Page>, IAggregateLocalizerHolder
    {
        /// <summary>
        /// Агрегатор всех локализаторв. Необходим для локализации вложенных доменов
        /// </summary>
        public AggregateLocalizer AggregateLocalizer { get; set; }

        /// <summary>
        /// Локализует значение в соответсвии текущей локали
        /// </summary>
        private IValueLocalizer _valueLocalizer;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="valueLocalizer"></param>
        public PageLocalizer(IValueLocalizer valueLocalizer)
        {
            _valueLocalizer = valueLocalizer;
        }

        /// <summary>
        /// Локализует домен Page
        /// </summary>
        /// <param name="domain"></param>
        public void Localize(Page domain)
        {
            domain.Title = _valueLocalizer.LocalizedValue(domain.Title, domain.TitleIn);

            if (AggregateLocalizer == null)
                return;

            if (domain.Description != null)
                AggregateLocalizer.LocalizeItem(domain.Description);            
        }
    }
}
