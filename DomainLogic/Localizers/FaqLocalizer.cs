﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Interfaces.Localization;
using DomainLogic.Models;

namespace DomainLogic.Localizers
{
    /// <summary>
    /// Локализует домен Faq
    /// </summary>
    [Key(nameof(Faq))]
    public class FaqLocalizer : IDomainLocalizer<Faq>
    {
        /// <summary>
        /// Локализует значение в соответсвии текущей локали
        /// </summary>
        private IValueLocalizer _valueLocalizer;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="valueLocalizer"></param>
        public FaqLocalizer(IValueLocalizer valueLocalizer)
        {
            _valueLocalizer = valueLocalizer;
        }

        /// <summary>
        /// Локализует домен Faq
        /// </summary>
        /// <param name="domain"></param>
        public void Localize(Faq domain)
        {
            domain.Answer = _valueLocalizer.LocalizedValue(domain.Answer, domain.AnswerIn);
            domain.Question = _valueLocalizer.LocalizedValue(domain.Question, domain.QuestionIn);
            domain.Title = _valueLocalizer.LocalizedValue(domain.Title, domain.TitleIn);
        }
    }
}
