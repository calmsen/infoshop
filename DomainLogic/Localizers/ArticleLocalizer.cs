﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Localization;
using DomainLogic.Interfaces.Localization;
using DomainLogic.Models;

namespace DomainLogic.Localizers
{
    /// <summary>
    /// Локализует домен Article
    /// </summary>
    [Key(nameof(Article))]
    public class ArticleLocalizer : IDomainLocalizer<Article>, IAggregateLocalizerHolder
    {
        /// <summary>
        /// Агрегатор всех локализаторв. Необходим для локализации вложенных доменов
        /// </summary>
        public AggregateLocalizer AggregateLocalizer { get; set; }

        /// <summary>
        /// Локализует значение в соответсвии текущей локали
        /// </summary>
        private IValueLocalizer _valueLocalizer;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="valueLocalizer"></param>
        public ArticleLocalizer(IValueLocalizer valueLocalizer)
        {
            _valueLocalizer = valueLocalizer;
        }

        /// <summary>
        /// Локализует домен Article
        /// </summary>
        /// <param name="domain"></param>
        public void Localize(Article domain)
        {
            domain.Annottion = _valueLocalizer.LocalizedValue(domain.Annottion, domain.AnnottionIn);
            domain.Title = _valueLocalizer.LocalizedValue(domain.Title, domain.TitleIn);

            if (AggregateLocalizer == null)
                return;

            if (domain.Description != null)
                AggregateLocalizer.LocalizeItem(domain.Description);
        }
    }
}
