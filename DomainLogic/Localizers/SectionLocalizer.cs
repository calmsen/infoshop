﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Localization;
using DomainLogic.Interfaces.Localization;
using DomainLogic.Models;
using System.Collections.Generic;

namespace DomainLogic.Localizers
{
    /// <summary>
    /// Локализует домен Section
    /// </summary>
    [Key(nameof(Section))]
    public class SectionLocalizer : IDomainLocalizer<Section>, IAggregateLocalizerHolder
    {
        /// <summary>
        /// Агрегатор всех локализаторв. Необходим для локализации вложенных доменов
        /// </summary>
        public AggregateLocalizer AggregateLocalizer { get; set; }

        /// <summary>
        /// Локализует значение в соответсвии текущей локали
        /// </summary>
        private IValueLocalizer _valueLocalizer;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="valueLocalizer"></param>
        public SectionLocalizer(IValueLocalizer valueLocalizer)
        {
            _valueLocalizer = valueLocalizer;
        }

        /// <summary>
        /// Локализует домен Section
        /// </summary>
        /// <param name="domain"></param>
        public void Localize(Section domain)
        {
            domain.Title = _valueLocalizer.LocalizedValue(domain.Title, domain.TitleIn);
            LocalizeFilterGroups(domain.FiltersGroups);

            if (AggregateLocalizer == null)
                return;

            if (domain.Products?.Count > 0)
                AggregateLocalizer.LocalizeList(domain.Products);
        }

        private void LocalizeFilterGroups(List<FilterGroup> filtersGroups)
        {
            if (filtersGroups?.Count > 0)
            {
                foreach (var filterGroup in filtersGroups)
                {
                    filterGroup.Title = _valueLocalizer.LocalizedValue(filterGroup.Title, filterGroup.TitleIn);
                    if (filterGroup.Filters == null || filterGroup.Filters.Count == 0)
                        continue;
                    foreach (var filter in filterGroup.Filters)
                    {
                        filter.Title = _valueLocalizer.LocalizedValue(filter.Title, filter.TitleIn);
                    }
                }
            }
        }
    }
}
