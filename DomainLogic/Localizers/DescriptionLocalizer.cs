﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Interfaces.Localization;
using DomainLogic.Models;

namespace DomainLogic.Localizers
{
    /// <summary>
    /// Локализует домен Description
    /// </summary>
    [Key(nameof(Description))]
    public class DescriptionLocalizer : IDomainLocalizer<Description>
    {
        /// <summary>
        /// Локализует значение в соответсвии текущей локали
        /// </summary>
        private IValueLocalizer _valueLocalizer;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="valueLocalizer"></param>
        public DescriptionLocalizer(IValueLocalizer valueLocalizer)
        {
            _valueLocalizer = valueLocalizer;
        }

        /// <summary>
        /// Локализует домен Description
        /// </summary>
        /// <param name="domain"></param>
        public void Localize(Description domain)
        {
            domain.Content = _valueLocalizer.LocalizedValue(domain.Content, domain.ContentIn);
        }
    }
}
