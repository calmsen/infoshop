﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Localization;
using DomainLogic.Interfaces.Localization;
using DomainLogic.Models;

namespace DomainLogic.Localizers
{
    /// <summary>
    /// Локализует домен FilterValueInline
    /// </summary>
    [Key(nameof(FilterValueInline))]
    public class FilterValueInlineLocalizer : IDomainLocalizer<FilterValueInline>
    {
        /// <summary>
        /// Локализует значение в соответсвии текущей локали
        /// </summary>
        private IValueLocalizer _valueLocalizer;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="valueLocalizer"></param>
        public FilterValueInlineLocalizer(IValueLocalizer valueLocalizer)
        {
            _valueLocalizer = valueLocalizer;
        }

        /// <summary>
        /// Локализует домен FilterValueInline
        /// </summary>
        /// <param name="domain"></param>
        public void Localize(FilterValueInline domain)
        {
            domain.Title = _valueLocalizer.LocalizedValue(domain.Title, domain.TitleIn);
        }
    }    
}
