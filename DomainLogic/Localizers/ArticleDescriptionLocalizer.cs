﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Interfaces.Localization;
using DomainLogic.Models;

namespace DomainLogic.Localizers
{
    /// <summary>
    /// Локализует домен ArticleDescription 
    /// </summary>
    [Key(nameof(ArticleDescription))]
    public class ArticleDescriptionLocalizer : IDomainLocalizer<ArticleDescription>
    {
        /// <summary>
        /// Локализует значение в соответсвии текущей локали
        /// </summary>
        private IValueLocalizer _valueLocalizer;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="valueLocalizer"></param>
        public ArticleDescriptionLocalizer(IValueLocalizer valueLocalizer)
        {
            _valueLocalizer = valueLocalizer;
        }

        /// <summary>
        /// Локализует домен ArticleDescription 
        /// </summary>
        /// <param name="domain"></param>
        public void Localize(ArticleDescription domain)
        {
            domain.Content = _valueLocalizer.LocalizedValue(domain.Content, domain.ContentIn);
        }
    }
}
