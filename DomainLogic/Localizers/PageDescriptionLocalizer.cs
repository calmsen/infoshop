﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Interfaces.Localization;
using DomainLogic.Models;

namespace DomainLogic.Localizers
{
    /// <summary>
    /// Локализует домен PageDescription
    /// </summary>
    [Key(nameof(PageDescription))]
    public class PageDescriptionLocalizer : IDomainLocalizer<PageDescription>
    {
        /// <summary>
        /// Локализует значение в соответсвии текущей локали
        /// </summary>
        private IValueLocalizer _valueLocalizer;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="valueLocalizer"></param>
        public PageDescriptionLocalizer(IValueLocalizer valueLocalizer)
        {
            _valueLocalizer = valueLocalizer;
        }

        /// <summary>
        /// Локализует домен PageDescription
        /// </summary>
        /// <param name="domain"></param>
        public void Localize(PageDescription domain)
        {
            domain.Content = _valueLocalizer.LocalizedValue(domain.Content, domain.ContentIn);
        }
    }
}
