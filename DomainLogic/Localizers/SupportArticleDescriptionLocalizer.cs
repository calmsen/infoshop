﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Interfaces.Localization;
using DomainLogic.Models;

namespace DomainLogic.Localizers
{
    /// <summary>
    /// Локализует домен SupportArticleDescription
    /// </summary>
    [Key(nameof(SupportArticleDescription))]
    public class SupportArticleDescriptionLocalizer : IDomainLocalizer<SupportArticleDescription>
    {
        /// <summary>
        /// Локализует значение в соответсвии текущей локали
        /// </summary>
        private IValueLocalizer _valueLocalizer;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="valueLocalizer"></param>
        public SupportArticleDescriptionLocalizer(IValueLocalizer valueLocalizer)
        {
            _valueLocalizer = valueLocalizer;
        }

        /// <summary>
        /// Локализует домен SupportArticleDescription
        /// </summary>
        /// <param name="domain"></param>
        public void Localize(SupportArticleDescription domain)
        {
            domain.Content = _valueLocalizer.LocalizedValue(domain.Content, domain.ContentIn);
        }
    }
}
