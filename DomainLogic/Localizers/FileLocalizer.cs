﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Interfaces.Localization;
using DomainLogic.Models;

namespace DomainLogic.Localizers
{
    /// <summary>
    /// Локализует домен DmFile
    /// </summary>
    [Key(nameof(DmFile))]
    public class FileLocalizer : IDomainLocalizer<DmFile>
    {
        /// <summary>
        /// Локализует значение в соответсвии текущей локали
        /// </summary>
        private IValueLocalizer _valueLocalizer;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="valueLocalizer"></param>
        public FileLocalizer(IValueLocalizer valueLocalizer)
        {
            _valueLocalizer = valueLocalizer;
        }

        /// <summary>
        /// Локализует домен DmFile
        /// </summary>
        /// <param name="domain"></param>
        public void Localize(DmFile domain)
        {
            domain.Description = _valueLocalizer.LocalizedValue(domain.Description, domain.DescriptionIn);
            domain.Title = _valueLocalizer.LocalizedValue(domain.Title, domain.TitleIn);
        }
    }
}
