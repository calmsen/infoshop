﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Localization;
using DomainLogic.Interfaces.Localization;
using DomainLogic.Models;

namespace DomainLogic.Localizers
{
    /// <summary>
    /// Локализует домен Post
    /// </summary>
    [Key(nameof(Post))]
    public class PostLocalizer : IDomainLocalizer<Post>, IAggregateLocalizerHolder
    {
        /// <summary>
        /// Агрегатор всех локализаторв. Необходим для локализации вложенных доменов
        /// </summary>
        public AggregateLocalizer AggregateLocalizer { get; set; }

        /// <summary>
        /// Локализует значение в соответсвии текущей локали
        /// </summary>
        private IValueLocalizer _valueLocalizer;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="valueLocalizer"></param>
        public PostLocalizer(IValueLocalizer valueLocalizer)
        {
            _valueLocalizer = valueLocalizer;
        }

        /// <summary>
        /// Локализует домен Post
        /// </summary>
        /// <param name="domain"></param>
        public void Localize(Post domain)
        {
            domain.Annottion = _valueLocalizer.LocalizedValue(domain.Annottion, domain.AnnottionIn);
            domain.Title = _valueLocalizer.LocalizedValue(domain.Title, domain.TitleIn);

            if (AggregateLocalizer == null)
                return;

            if (domain.Description != null)
                AggregateLocalizer.LocalizeItem(domain.Description);
            if (domain.MainImage != null)
                AggregateLocalizer.LocalizeItem(domain.MainImage);
        }
    }
}
