﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Localization;
using DomainLogic.Interfaces.Localization;
using DomainLogic.Models;

namespace DomainLogic.Localizers
{
    /// <summary>
    /// Локализует домен DmFilter
    /// </summary>
    [Key(nameof(DmFilter))]
    public class FilterLocalizer : IDomainLocalizer<DmFilter>, IAggregateLocalizerHolder
    {
        /// <summary>
        /// Агрегатор всех локализаторв. Необходим для локализации вложенных доменов
        /// </summary>
        public AggregateLocalizer AggregateLocalizer { get; set; }

        /// <summary>
        /// Локализует значение в соответсвии текущей локали
        /// </summary>
        private IValueLocalizer _valueLocalizer;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="valueLocalizer"></param>
        public FilterLocalizer(IValueLocalizer valueLocalizer)
        {
            _valueLocalizer = valueLocalizer;
        }

        /// <summary>
        /// Локализует домен DmFilter
        /// </summary>
        /// <param name="domain"></param>
        public void Localize(DmFilter domain)
        {
            domain.Title = _valueLocalizer.LocalizedValue(domain.Title, domain.TitleIn);
            foreach(var unitConverter in domain.UnitConverters)
                unitConverter.Unit = _valueLocalizer.LocalizedValue(unitConverter.Unit, unitConverter.UnitIn);

            if (AggregateLocalizer == null)
                return;

            if (domain.Values?.Count > 0)
                AggregateLocalizer.LocalizeList(domain.Values);

            if (domain.Attributes?.Count > 0)
                AggregateLocalizer.LocalizeList(domain.Attributes);
        }
    }
}
