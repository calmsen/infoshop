﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Interfaces.Localization;
using DomainLogic.Models;

namespace DomainLogic.Localizers
{
    /// <summary>
    /// Локализует домен FilterValue
    /// </summary>
    [Key(nameof(FilterValue))]
    public class FilterValueLocalizer : IDomainLocalizer<FilterValue>
    {
        /// <summary>
        /// Локализует значение в соответсвии текущей локали
        /// </summary>
        private IValueLocalizer _valueLocalizer;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="valueLocalizer"></param>
        public FilterValueLocalizer(IValueLocalizer valueLocalizer)
        {
            _valueLocalizer = valueLocalizer;
        }

        /// <summary>
        /// Локализует домен FilterValue
        /// </summary>
        /// <param name="domain"></param>
        public void Localize(FilterValue domain)
        {
            domain.Title = _valueLocalizer.LocalizedValue(domain.Title, domain.TitleIn);
        }
    }  
}
