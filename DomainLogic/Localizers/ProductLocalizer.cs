﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Localization;
using DomainLogic.Interfaces.Localization;
using DomainLogic.Models;

namespace DomainLogic.Localizers
{
    /// <summary>
    /// Локализует домен Product
    /// </summary>
    [Key(nameof(Product))]
    public class ProductLocalizer : IDomainLocalizer<Product>, IAggregateLocalizerHolder
    {
        /// <summary>
        /// Агрегатор всех локализаторв. Необходим для локализации вложенных доменов
        /// </summary>
        public AggregateLocalizer AggregateLocalizer { get; set; }

        /// <summary>
        /// Локализует значение в соответсвии текущей локали
        /// </summary>
        private IValueLocalizer _valueLocalizer;

        /// <summary>
        /// Конструктор для создания экземпляра класса
        /// </summary>
        /// <param name="valueLocalizer"></param>
        public ProductLocalizer(IValueLocalizer valueLocalizer)
        {
            _valueLocalizer = valueLocalizer;
        }

        /// <summary>
        /// Локализует домен Product
        /// </summary>
        /// <param name="domain"></param>
        public void Localize(Product domain)
        {
            domain.BannerImageId = _valueLocalizer.LocalizedValue(domain.BannerImageId, domain.BannerImageIdIn);
            domain.Title = _valueLocalizer.LocalizedValue(domain.Title, domain.TitleIn);
            
            if (AggregateLocalizer == null)
                return;

            if (domain.Description != null)
                AggregateLocalizer.LocalizeItem(domain.Description);
            if (domain.Section != null)
                AggregateLocalizer.LocalizeItem(domain.Section);
            if (domain.Attributes?.Count > 0)
                AggregateLocalizer.LocalizeList(domain.Attributes);
        }
    }
}
