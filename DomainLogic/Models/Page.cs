﻿using System;

namespace DomainLogic.Models
{
    /// <summary>
    /// Модель пользовательской страницы. Такиех как "О компании", "Отдел продаж" и др.
    /// </summary>
    public class Page
    {
        /// <summary>
        /// Инициазирует экземпляр класса DbPage
        /// </summary>
        public Page()
        {
            CreatedDate = DateTime.Now;
        }

        public long Id { get; set; }

        /// <summary>
        /// Транслитерируемое название страницы
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Название страницы
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Название на английском
        /// </summary>
        public string TitleIn { get; set; }

        /// <summary>
        /// Дата создания страницы
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Слой мастер страницы (если требуется)
        /// </summary>
        public string Layout { get; set; }

        /// <summary>
        /// Название ссылки в главном меню
        /// </summary>
        public string NavLinkName { get; set; }

        /// <summary>
        /// Идентификатор описания страницы
        /// </summary>
        public long? DescriptionId { get; set; }

        /// <summary>
        /// Привязка - описание страницы
        /// </summary>
        public PageDescription Description { get; set; }
    }
}
