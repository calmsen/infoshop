﻿namespace DomainLogic.Models
{
    public class ArticleToImage
    {
        public long ArticleId { get; set; }

        public long ImageId { get; set; }

        public Article Article { get; set; }

        public ArticleImage Image { get; set; }
    }
}
