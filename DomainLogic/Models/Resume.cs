﻿namespace DomainLogic.Models
{
    /// <summary>
    /// Данный класс описывает модель резюме
    /// </summary>
    public class Resume
    {
        public int Id { get; set; }

        /// <summary>
        /// Транслитерируемое название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Название резюме
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Полное имя владельца резюме
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Контактный телефон
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Контактный e-mail
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Образование
        /// </summary>
        public string Education { get; set; }

        /// <summary>
        /// Опыт работы
        /// </summary>
        public string Experience { get; set; }

        /// <summary>
        /// Дополнительная информация
        /// </summary>
        public string AdditionalInfo { get; set; }

        /// <summary>
        /// Прикрепленный файл
        /// </summary>
        public string ResumeDocLink { get; set; }
    }
}