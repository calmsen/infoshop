﻿using System.Collections.Generic;

namespace DomainLogic.Models
{
    /// <summary>
    /// Класс описывает пользователя
    /// </summary>
    public class User
    {
        public User()
        {
            Feedbacks = new List<Feedback>();
            FeedbacksWithUserWhoSetTheState = new List<Feedback>();
            FeedbackAnswers = new List<FeedbackAnswer>();
            ProductSnapshots = new List<ProductSnapshot>();
            ProductStates = new List<ProductState>();
        }

        public int Id { get; set; }

        /// <summary>
        /// E-mail пользователя
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Полное имя пользователя
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Активна ли подписка
        /// </summary>
        public bool Subscription { get; set; }

        /// <summary>
        /// Привязка к сообщениям обратной связи
        /// </summary>
        public List<Feedback> Feedbacks { get; set; }
        /// <summary>
        /// Привязка к сообщениям обратной связи, которые устанавливали статус
        /// </summary>
        public List<Feedback> FeedbacksWithUserWhoSetTheState { get; set; }
        /// <summary>
        /// Привязка к ответам сообщений обратной связи
        /// </summary>
        public List<FeedbackAnswer> FeedbackAnswers { get; set; }
        /// <summary>
        /// Привязка к снимкам товаров
        /// </summary>
        public List<ProductSnapshot> ProductSnapshots { get; set; }
        /// <summary>
        /// Привязка к состояниям товаров
        /// </summary>
        public List<ProductState> ProductStates { get; set; }
    }
}