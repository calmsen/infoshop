﻿using DomainLogic.Models.Enumerations;
using System;

namespace DomainLogic.Models
{
    /// <summary>
    ///  Снимок товара
    /// </summary>
    public class ProductSnapshot
    {
        /// <summary>
        /// Инициазирует экземпляр класса DbProductSnapshot
        /// </summary>
        public ProductSnapshot()
        {
            Snapshot = "{}";
            CreatedDate = DateTime.Now;
            ProductStates = new ProductStates();
        }

        public long Id { get; set; }

        /// <summary>
        /// Информация о товаре в определенный момент. Json формат.
        /// </summary>
        public string Snapshot { get; set; }

        /// <summary>
        /// Дата создания снимка
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Идентификатор товара
        /// </summary>
        public long ProductId { get; set; }

        /// <summary>
        /// Идентификатор пользователя, который создал редакцию товара
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Товар
        /// </summary>
        public Product Product { get; set; }

        /// <summary>
        /// Привязка к пользователю, который создал редакцию товара
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Состояния модификаций. Какие модификации были сделаны
        /// </summary>
        public ProductStates ProductStates { get; set; }
    }
}
