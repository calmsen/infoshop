﻿using DomainLogic.Models.Enumerations;

namespace DomainLogic.Models
{
    /// <summary>
    /// Данный класс описывает модель для хранений опций сайта
    /// </summary>
    public class Option
    {
        /// <summary>
        /// Название опции
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Заголок опции
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Значение опции
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Тип опции
        /// </summary>
        public OptionTypeEnum Type { get; set; }

        /// <summary>
        /// Системная опция
        /// </summary>
        public bool IsSystem { get; set; }
    }
}
