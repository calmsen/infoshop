﻿using System.Collections.Generic;

namespace DomainLogic.Models
{
    /// <summary>
    /// Модель страны
    /// </summary>
    public class Country
    {
        public long Id { get; set; }

        /// <summary>
        /// Название страны
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Псевдоним. Например РФ
        /// </summary>
        public string Alias { get; set; }

        /// <summary>
        /// Широта
        /// </summary>
        public string Lat { get; set; }

        /// <summary>
        /// Долгота
        /// </summary>
        public string Lng { get; set; }

        /// <summary>
        /// Связь с сущностью - Адрес
        /// </summary>
        public List<Address> Addresses { get; set; }
    }
}
