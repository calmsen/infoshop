﻿
namespace DomainLogic.Models
{
    public class FeedbackToImage
    {
        public long FeedbackId { get; set; }

        public long ImageId { get; set; }

        public Feedback Feedback { get; set; }

        public FeedbackImage Image { get; set; }
    }
}
