﻿using System;

namespace DomainLogic.Models
{
    public class ProductSnapshotInline
    {
        public long Id { get; set; }

        public string Snapshot { get; set; }

        public DateTime CreatedDate { get; set; }

        public long ProductId { get; set; }

        public int UserId { get; set; }

        public int ProductStates_AsInt { get; set; }
    }
}