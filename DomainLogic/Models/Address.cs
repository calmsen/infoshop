﻿namespace DomainLogic.Models
{
    /// <summary>
    /// Модель адреса
    /// </summary>
    public class Address
    {
        public long Id { get; set; }

        /// <summary>
        /// Широта
        /// </summary>
        public string Lat { get; set; }

        /// <summary>
        /// Долгота
        /// </summary>
        public string Lng { get; set; }

        /// <summary>
        /// IP адрес
        /// </summary>
        public string Ip { get; set; }

        /// <summary>
        /// Идентификатор страны
        /// </summary>
        public long? CountryId { get; set; }

        /// <summary>
        /// Идентификатор области
        /// </summary>
        public long? DistrictId { get; set; }

        /// <summary>
        /// Идентификатор региона
        /// </summary>
        public long? RegionId { get; set; }

        /// <summary>
        /// Идентификатор города
        /// </summary>
        public long? CityId { get; set; }

        /// <summary>
        /// Связь с сущностью - Страна 
        /// </summary>
        public Country Country { get; set; }

        /// <summary>
        /// Связь с сущностью - Область
        /// </summary>
        public District District { get; set; }

        /// <summary>
        /// Связь с сущностью - Регион
        /// </summary>
        public Region Region { get; set; }

        /// <summary>
        /// Связь с сущностью - Город
        /// </summary>
        public City City { get; set; }
    }
}
