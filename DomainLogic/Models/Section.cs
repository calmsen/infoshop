﻿using System.Collections.Generic;

namespace DomainLogic.Models
{
    /// <summary>
    /// Данный класс описывает модель раздела
    /// </summary>
    public class Section
    {
        /// <summary>
        /// Инициазирует экземпляр класса Section
        /// </summary>
        public Section()
        {
            SectionsToImages = new List<SectionToImage>();
            Faq = new List<Faq>();
            Feedback = new List<Feedback>();
            Products = new List<Product>();
            Childs = new List<Section>();
        }

        public long Id { get; set; }

        /// <summary>
        /// Транслитерируемое название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Название раздела
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Название раздела на английском
        /// </summary>
        public string TitleIn { get; set; }

        /// <summary>
        /// Количество товаров
        /// </summary>
        public int ProductsAmount { get; set; }

        /// <summary>
        /// Скрыть ли раздел
        /// </summary>
        public bool Hidden { get; set; }

        /// <summary>
        /// Номер позиции раздела
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Полный путь раздела
        /// </summary>
        public long PathOfSections { get; set; }

        /// <summary>
        /// Родительский раздел
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// Идентификатор главной картинки раздела
        /// </summary>
        public long? MainImageId { get; set; }

        /// <summary>
        /// Главная картинка раздела
        /// </summary>
        public SectionImage MainImage { get; set; }

        /// <summary>
        /// Привязка к вопросам и ответам
        /// </summary>
        public List<Faq> Faq { get; set; }
        /// <summary>
        /// Привязка к сообщениям обратной связи
        /// </summary>
        public List<Feedback> Feedback { get; set; }
        /// <summary>
        /// Привязка к товарам 
        /// </summary>
        public List<Product> Products { get; set; }

        /// <summary>
        /// Привязка к дочерним разделам
        /// </summary>
        public List<Section> Childs { get; set; }
        /// <summary>
        /// Привязка к шаблонам характеристик 
        /// </summary>
        private List<FilterGroup> _filtersGroups;
        public string FiltersGroupsAsXml { get; set; }
        public List<FilterGroup> FiltersGroups
        {
            get
            {
                if (_filtersGroups == null)
                {
                    try
                    {
                        var formatter = new System.Xml.Serialization.XmlSerializer(typeof(List<FilterGroup>));
                        using (var ms = new System.IO.MemoryStream(System.Text.Encoding.UTF8.GetBytes(FiltersGroupsAsXml)))
                        {
                            _filtersGroups = (List<FilterGroup>)formatter.Deserialize(ms);
                        }
                    }
                    catch
                    {
                        _filtersGroups = new List<FilterGroup>();
                    }
                }
                return _filtersGroups;
            }
            set
            {
                try
                {
                    var formatter = new System.Xml.Serialization.XmlSerializer(typeof(List<FilterGroup>));
                    using (var ms = new System.IO.MemoryStream())
                    {
                        formatter.Serialize(ms, _filtersGroups);
                        FiltersGroupsAsXml = System.Text.Encoding.UTF8.GetString(ms.ToArray());
                    }
                    _filtersGroups = value;
                }
                catch
                {
                }
            }
        }

        /// <summary>
        /// Привяка к картинкам для раздела
        /// </summary>
        public List<SectionToImage> SectionsToImages { get; set; }
    }
}
