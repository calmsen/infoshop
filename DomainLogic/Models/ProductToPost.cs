﻿namespace DomainLogic.Models
{
    public class ProductToPost
    {
        public long ProductId { get; set; }

        public long PostId { get; set; }

        public Product Product { get; set; }

        public Post Post { get; set; }
    }
}