﻿namespace DomainLogic.Models
{
    public class UnitConverter
    {
        public long Id { get; set; }

        public string Unit { get; set; }

        public string UnitIn { get; set; }

        public string UnitK { get; set; }
    }
}
