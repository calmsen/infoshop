﻿using System.Collections.Generic;

namespace DomainLogic.Models
{
    public class FeedbackTagContainer
    {
        public List<FeedbackTag> Tags { get; set; }

        public long FeedbackId { get; set; }
    }
}
