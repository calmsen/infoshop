﻿using DomainLogic.Interfaces.ImageInterfaces;
using System.Collections.Generic;

namespace DomainLogic.Models
{
    /// <summary>
    /// Модель картинки для сообщения
    /// </summary>
    public class FeedbackImage : IImage
    {
        public FeedbackImage()
        {
            FeedbacksToImages = new List<FeedbackToImage>();
        }

        public long Id { get; set; }

        /// <summary>
        /// Ширина картинки
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Высота картинки
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Название картинки
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Внешняя ссылка на картинку
        /// </summary>
        public string ExternalLink { get; set; }

        /// <summary>
        /// Привязка с сообщениям
        /// </summary>
        public List<FeedbackToImage> FeedbacksToImages { get; set; }


    }
}
