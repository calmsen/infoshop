﻿using System;

namespace DomainLogic.Models
{
    /// <summary>
    /// Лента обновлений файлов
    /// </summary>
    public class SupportArticle
    {
        /// <summary>
        /// Инициазирует экземпляр класса SupportArticle
        /// </summary>
        public SupportArticle()
        {
            CreatedDate = DateTime.Now;
        }

        public long Id { get; set; }

        /// <summary>
        /// Транслитерируемое название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Название 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Название на английском
        /// </summary>
        public string TitleIn { get; set; }

        /// <summary>
        /// Краткое описание
        /// </summary>
        public string Annottion { get; set; }

        /// <summary>
        /// Краткое описание на английском
        /// </summary>
        public string AnnottionIn { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Идентификатор описания
        /// </summary>
        public long? DescriptionId { get; set; }

        /// <summary>
        /// Привязка к описанию
        /// </summary>
        public SupportArticleDescription Description { get; set; }
    }
}
