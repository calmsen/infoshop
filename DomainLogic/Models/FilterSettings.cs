﻿using DomainLogic.Models.Enumerations;
using System.Collections.Generic;

namespace DomainLogic.Models
{
    public class FilterSettings
    {
        public FilterSettings()
        {
            Values = new List<FilterValueSettings>();
        }

        public long Id { get; set; }

        public string Title { get; set; }

        public string TitleIn { get; set; }

        public FilterTypeEnum Type { get; set; }

        public long? UnitConverterIdForCatalog { get; set; }

        public long? UnitConverterIdForProduct { get; set; }
        
        public bool ShowInGroupSelect { get; set; }

        public List<FilterValueSettings> Values { get; set; }
    }
}
