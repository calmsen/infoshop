﻿using System.Collections.Generic;

namespace DomainLogic.Models
{
    public class ProductInfoForSnapshot
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Title { get; set; }

        public string TitleIn { get; set; }

        public string ShortFeatures { get; set; }

        public double MainImageK { get; set; }

        public string Sertificate { get; set; }

        public int BannerPosition { get; set; }

        public bool ToBanner { get; set; }

        public bool ToRating { get; set; }

        public bool AllowFiles { get; set; }

        public bool Published { get; set; }

        public bool Filled { get; set; }

        public bool New { get; set; }

        public string Comment { get; set; }

        public float Price { get; set; }

        public string ExternalLink { get; set; }

        public long? Code { get; set; }

        public long SectionId { get; set; }

        public long? TypeId { get; set; }

        public long? MainImageId { get; set; }

        public long? BannerImageId { get; set; }

        public long? BannerImageIdIn { get; set; }

        public DescriptionForSnapshot Description { get; set; }

        public List<AttributeForSnapshot> Attributes { get; set; }

        public List<PartnerLinkOnProduct> PartnersLinksOnProduct { get; set; }

        public List<long> ImageIds { get; set; }
    }
}
