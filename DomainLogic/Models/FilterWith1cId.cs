﻿
namespace DomainLogic.Models
{
    public class FilterWith1cId
    {
        public string Id { get; set; }

        public long FilterId { get; set; }

        public long SectionId { get; set; }
    }
}
