﻿using System.Collections.Generic;

namespace DomainLogic.Models
{

    /// <summary>
    /// Данный класс описывает характеристики товара
    /// </summary>
    public class DmAttribute
    {
        /// <summary>
        /// Инициазирует экземпляр класса DmAttribute
        /// </summary>
        public DmAttribute()
        {
            Values = new List<ProductValue>();
        }

        public long Id { get; set; }

        /// <summary>
        /// Идентификатора товара
        /// </summary>
        public long ProductId { get; set; }

        /// <summary>
        /// Идентификатор фильтра
        /// </summary>
        public long FilterId { get; set; }

        /// <summary>
        /// Привязка к товару, к которому привязана данная характеристика
        /// </summary>
        public Product Product { get; set; }

        /// <summary>
        /// Привязка к фильтр
        /// </summary>
        public DmFilter Filter { get; set; }

        /// <summary>
        /// Привязка к значениям 
        /// </summary>
        public List<ProductValue> Values { get; set; }
    }
}
