﻿using System.Collections.Generic;

namespace DomainLogic.Models
{
    /// <summary>
    /// Модель описывающая описание страницы
    /// </summary>
    public class PageDescription
    {
        public PageDescription()
        {
            Pages = new List<Page>();
        }

        public long Id { get; set; }

        /// <summary>
        /// Описание страницы
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Описание страницы на английском
        /// </summary>
        public string ContentIn { get; set; }

        /// <summary>
        /// Привязка к странице
        /// </summary>
        public List<Page> Pages { get; set; }
    }
}
