﻿namespace DomainLogic.Models
{
    public class ProductTitle 
    {
        public long Id { get; set; }

        public string Title { get; set; }
    }
}