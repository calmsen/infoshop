﻿namespace DomainLogic.Models
{
    public class ProductToArticle
    {
        public long ProductId { get; set; }

        public long ArticleId { get; set; }

        public Product Product { get; set; }

        public Article Article { get; set; }
    }
}
