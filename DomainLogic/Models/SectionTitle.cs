﻿namespace DomainLogic.Models
{
    public class SectionTitle
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public long FilterId { get; set; }
    }
}
