﻿namespace DomainLogic.Models
{
    public class PartnerLinkOnProduct
    {
        public long Id { get; set; }

        public string Link { get; set; }
    }
}
