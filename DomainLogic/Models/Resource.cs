﻿namespace DomainLogic.Models
{
    public class Resource
    {
        public string Key { get; set; }

        public string Value { get; set; }

        public string ValueIn { get; set; }
    }
}
