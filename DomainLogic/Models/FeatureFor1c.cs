﻿
namespace DomainLogic.Models
{
    public class FeatureFor1c
    {
        public long id_1c { get; set; }

        public string vid_h_name { get; set; }

        public long vid_h_cod { get; set; }

        /// <summary>
        /// полный путь раздел 1/раздел 2/группа/фильтр
        /// </summary>
        public string vid_h_path { get; set; } 

        public string zn_h_name { get; set; }

        public string zn_h_name_en { get; set; }

        public double zn_h_number { get; set; }

        public long? zn_h_cod { get; set; }

        public long SectionId { get; set; }
    }
}
