﻿namespace DomainLogic.Models
{
    public class ProductToImage
    {
        public long ProductId { get; set; }

        public long ImageId { get; set; }

        public Product Product { get; set; }

        public Image Image { get; set; }
    }
}
