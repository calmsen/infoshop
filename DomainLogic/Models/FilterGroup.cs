﻿using System.Collections.Generic;

namespace DomainLogic.Models
{
    public class FilterGroup
    {
        public FilterGroup()
        {
            Filters = new List<FilterSettings>();
        }

        public long Id { get; set; }

        public string Title { get; set; }

        public string TitleIn { get; set; }

        public List<FilterSettings> Filters { get; set; }
    }
}
