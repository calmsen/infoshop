﻿using DomainLogic.Models.Enumerations;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DomainLogic.Models
{
    /// <summary>
    /// Модель фильтра сайта
    /// </summary>
    public class DmFilter
    {
        /// <summary>
        /// Инициазирует экземпляр класса DbFilter
        /// </summary>
        public DmFilter()
        {
            Values = new List<FilterValue>();
            UnitConverterIdForCatalog = -1;
            UnitConverterIdForProduct = -1;
        }

        public long Id { get; set; }

        /// <summary>
        /// Транслитерируемое название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Название фильтра
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Название на английском
        /// </summary>
        public string TitleIn { get; set; }

        /// <summary>
        /// Тип отображения фильтра (список, диапозон, список чекбоксов)
        /// </summary>
        public FilterTypeEnum Type { get; set; }

        /// <summary>
        /// Единица измерения
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// Единица измерения на английском
        /// </summary>
        public string UnitIn { get; set; }

        /// <summary>
        /// Ссылка на конвертер, который будет применяться в каталоге
        /// </summary>
        public long UnitConverterIdForCatalog { get; set; }

        /// <summary>
        /// Ссылка на конвертер, который будет применяться в карточке товара
        /// </summary>
        public long UnitConverterIdForProduct { get; set; }

        /// <summary>
        /// Указывает на возможность множественного заполнения значений
        /// </summary>
        public bool Multiple { get; set; }

        /// <summary>
        /// Список конверторов единиц измерения. Например конвертер из г в кг.
        /// </summary>
        private List<UnitConverter> _unitConverters;
        public string UnitConvertersJson { get; set; }
        public List<UnitConverter> UnitConverters
        {
            get
            {
                if (_unitConverters == null)
                {
                    try
                    {
                        _unitConverters = JsonConvert.DeserializeObject<List<UnitConverter>>(UnitConvertersJson);
                    }
                    catch (Exception)
                    {
                        _unitConverters = new List<UnitConverter>();
                    }
                }
                return _unitConverters;
            }
            set
            {
                _unitConverters = value;
                UnitConvertersJson = JsonConvert.SerializeObject(value);
            }
        }

        /// <summary>
        /// Список значений фильтра
        /// </summary>
        public List<FilterValue> Values { get; set; }

        /// <summary>
        /// Привязка к характеристикам
        /// </summary>
        public List<DmAttribute> Attributes { get; set; }

        //---
        public UnitConverter CurrentUnitConverter { get; set; }

        public void SetUpProps(FilterSettings settings)
        {
            Type = settings.Type;

            string unit = "";
            string unitIn = "";
            if (Type == FilterTypeEnum.Range)
            {
                // покажем единицу измерения, в которой нужно указывать значения
                unit = Unit;
                unitIn = UnitIn;
                if (settings.UnitConverterIdForCatalog != null)
                    UnitConverterIdForCatalog = (long)settings.UnitConverterIdForCatalog;
                if (UnitConverterIdForCatalog > 0)
                {
                    CurrentUnitConverter = UnitConverters.Where(x => x.Id == UnitConverterIdForCatalog).FirstOrDefault();
                    if (CurrentUnitConverter != null)
                    {
                        unit = CurrentUnitConverter.Unit;
                        unitIn = CurrentUnitConverter.UnitIn;
                    }
                }
            }
            Unit = unit;
            UnitIn = unitIn;

            if (!string.IsNullOrEmpty(settings.Title))
                Title = settings.Title;
            if (!string.IsNullOrEmpty(settings.TitleIn))
                TitleIn = settings.TitleIn;
        }
    }

}
