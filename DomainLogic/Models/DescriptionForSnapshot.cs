﻿namespace DomainLogic.Models
{
    public class DescriptionForSnapshot
    {
        public string Content { get; set; }

        public string ContentIn { get; set; }
    }
}
