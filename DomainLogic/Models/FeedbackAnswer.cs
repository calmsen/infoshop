﻿using System;

namespace DomainLogic.Models
{
    /// <summary>
    /// Модель ответов на сообщение
    /// </summary>
    public class FeedbackAnswer
    {
        /// <summary>
        /// Инициазирует экземпляр класса DbFeedbackAnswer
        /// </summary>
        public FeedbackAnswer()
        {
            CreatedDate = DateTime.Now;
        }

        public long Id { get; set; }

        /// <summary>
        /// Текст ответа
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Дата создания ответа
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Указывает надо ли показывать ответ автору сообщения. Данная функция доступна только менеджерам.
        /// </summary>
        public bool NoticedAuthor { get; set; }

        /// <summary>
        /// Идентификатор пользователя - автор ответа
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Идентификатор сообщения, к которому относится ответ
        /// </summary>
        public long FeedbackId { get; set; }

        /// <summary>
        /// Привязка к пользователю - автору ответа
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Привязка к сообщению, к которому относится ответ
        /// </summary>
        public Feedback Feedback { get; set; }
    }
}
