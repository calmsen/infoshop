﻿using System.Collections.Generic;

namespace DomainLogic.Models
{
    /// <summary>
    /// Данный класс описывает тип изделия
    /// </summary>
    public class ProductType
    {
        public ProductType()
        {
            Products = new List<Product>();
        }

        public long Id { get; set; }

        /// <summary>
        /// Название изделия
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Название изделия на английском
        /// </summary>
        public string TitleIn { get; set; }

        /// <summary>
        /// Идентификатор раздела, к которому привязано изделие
        /// </summary>
        public long? SectionId { get; set; }

        /// <summary>
        /// Раздел, к которому привязано изделие
        /// </summary>
        public Section Section { get; set; }

        /// <summary>
        /// Привязка к товарам с этим изделием
        /// </summary>
        public List<Product> Products { get; set; }
    }
}