﻿using System.Collections.Generic;

namespace DomainLogic.Models.Settings
{
    public class ManagerSettings
    {
        public List<string> Emails { get; set; } = new List<string>();
    }
}
