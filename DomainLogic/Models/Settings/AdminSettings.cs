﻿
namespace DomainLogic.Models.Settings
{
    public class AdminSettings
    {
        public long AdminId { get; set; }

        public string AdminEmail { get; set; }
    }
}
