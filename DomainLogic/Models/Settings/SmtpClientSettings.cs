﻿using DomainLogic.Infrastructure.Attributes;

namespace DomainLogic.Models.Settings
{
    public class SmtpClientSettings
    {
        public string Host { get; set; }

        public int Port { get; set; }

        public bool EnableSsl { get; set; }

        [Inject]
        public Credentials Credentials { get; set; }
    }
}
