﻿using DomainLogic.Infrastructure.Attributes;

namespace DomainLogic.Models.Settings
{
    public class ImagesSettings
    {
        [Inject]
        public FtpSettings Ftp { get; set; }
    }
}
