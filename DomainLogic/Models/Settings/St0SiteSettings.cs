﻿namespace DomainLogic.Models.Settings
{
    public class St0SiteSettings
    {
        public string Root { get; set; }

        public string HttpHost { get; set; }
    }
}
