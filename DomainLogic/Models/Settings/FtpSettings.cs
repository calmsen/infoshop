﻿
using DomainLogic.Infrastructure.Attributes;

namespace DomainLogic.Models.Settings
{
    public class FtpSettings
    {
        public string Path { get; set; }

        [Inject]
        public Credentials Credentials { get; set; }
    }
}
