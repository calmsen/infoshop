﻿using System.Collections.Generic;

namespace DomainLogic.Models.Settings
{
    public class ImagesLoaderSettings
    {
        public List<string> AllowedExts { get; set; }

        public long NoPhoto { get; set; }

        public int ImageMinDim { get; set; }

        public Dictionary<string, int> ImageMinDimsByFolders { get; set; } = new Dictionary<string, int>();
    }
}
