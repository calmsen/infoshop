﻿using DomainLogic.Infrastructure.Attributes;

namespace DomainLogic.Models.Settings
{
    public class SectionsSettings
    {
        [Inject]
        public SectionSettings OtherSection { get; set; }
    }
}
