﻿using DomainLogic.Infrastructure.Attributes;

namespace DomainLogic.Models.Settings
{
    public class AppSettings
    {
        [Inject]
        public AdminSettings Admin { get; set; }

        [Inject]
        public LocalizationSettings Localization { get; set; }

        [Inject]
        public St0SiteSettings St0Site { get; set; }

        [Inject]
        public SmtpClientSettings SmtpClient { get; set; }

        [Inject]
        public ImagesLoaderSettings ImagesLoader { get; set; }

        [Inject]
        public ResumeMessagesSettings ResumeMessages { get; set; }

        [Inject]
        public ManagerSettings Manager { get; set; }

        [Inject]
        public FilesSettings Files { get; set; }

        [Inject]
        public SectionsSettings Sections { get; set; }

        [Inject]
        public ProductsSettings Products { get; set; }

        [Inject]
        public UploadSettings Upload { get; set; }

        [Inject]
        public FeedbackSettings Feedback { get; set; }

        [Inject]
        public MailingSettings Mailing { get; set; }
    }
}
