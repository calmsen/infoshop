﻿using DomainLogic.Infrastructure.Attributes;
using System.Collections.Generic;

namespace DomainLogic.Models.Settings
{
    public class ProductsSettings
    {
        public List<long> PlatformFilterIds { get; set; } = new List<long>();

        public long ArchiveState { get; set; }

        [Inject]
        public ImagesSettings Images { get; set; }
    }
}
