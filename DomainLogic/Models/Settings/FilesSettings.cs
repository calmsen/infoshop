﻿using DomainLogic.Infrastructure;
using System.Collections.Generic;

namespace DomainLogic.Models.Settings
{
    public class FilesSettings
    {
        public List<string> AllowedExts { get; set; } = new List<string>();
    }
}
