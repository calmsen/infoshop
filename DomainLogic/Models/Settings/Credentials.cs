﻿
namespace DomainLogic.Models.Settings
{
    public class Credentials
    {
        public string User { get; set; }

        public string Password { get; set; }
    }
}
