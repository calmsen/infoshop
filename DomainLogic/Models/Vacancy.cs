﻿using DomainLogic.Models.Enumerations;

namespace DomainLogic.Models
{
    public class Vacancy
    {
        public int Id { get; set; }

        /// <summary>
        /// Транслитерируемое название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Название вакансии
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Название адреса
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Занятость
        /// </summary>
        public string Employment { get; set; }

        /// <summary>
        /// Обязанности
        /// </summary>
        public string Duties { get; set; }

        /// <summary>
        /// Образование
        /// </summary>
        public string Education { get; set; }

        /// <summary>
        /// Опыт
        /// </summary>
        public string Experience { get; set; }

        /// <summary>
        /// Доп. требования
        /// </summary>
        public string AdditionalRequirements { get; set; }

        /// <summary>
        /// Зарплата
        /// </summary>
        public string Salary { get; set; }

        /// <summary>
        /// E-mail
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Условия работы
        /// </summary>
        public string WorkingConditions { get; set; }

        /// <summary>
        /// Состояние вакансии
        /// </summary>
        public VacancyStateEnum State { get; set; }
    }

}