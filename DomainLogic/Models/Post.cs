﻿using System;
using System.Collections.Generic;

namespace DomainLogic.Models
{
    /// <summary>
    /// Данный класс описывает модель данных обзора
    /// </summary>
    public class Post
    {
        public Post()
        {
            CreatedDate = DateTime.Now;
            ProductsToPosts = new List<ProductToPost>();
        }

        public long Id { get; set; }

        /// <summary>
        /// Транслитерируемое название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Название обзора
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Название обзора на английском
        /// </summary>
        public string TitleIn { get; set; }

        /// <summary>
        /// Год обзора
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Краткое описание обзора
        /// </summary>
        public string Annottion { get; set; }

        /// <summary>
        /// Краткое описание обзора на английском
        /// </summary>
        public string AnnottionIn { get; set; }

        /// <summary>
        /// Внешняя ссылка на обзор. Если она указана, то переход идет по этой ссылке.
        /// </summary>
        public string LinkToExpertPost { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Активен ли обзор
        /// </summary>
        public bool Activate { get; set; }

        /// <summary>
        /// Идентификатор описания обзора
        /// </summary>
        public long? DescriptionId { get; set; }

        /// <summary>
        /// Идентификатор главной картинки обзора
        /// </summary>
        public long? MainImageId { get; set; }

        /// <summary>
        /// Привязка к описанию обзора
        /// </summary>
        public PostDescription Description { get; set; }

        /// <summary>
        /// Привязка к картинке
        /// </summary>
        public PostImage MainImage { get; set; }

        /// <summary>
        /// Привязка к товарам
        /// </summary>
        public List<ProductToPost> ProductsToPosts { get; set; }
    }
}
