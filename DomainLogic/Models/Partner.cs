﻿using System;

namespace DomainLogic.Models
{
    /// <summary>
    /// Данный класс описывает доменный объект партнера. 
    /// </summary>
    public class Partner
    {
        public Partner()
        {
            CreatedDate = DateTime.Now;
            Position = Int32.MaxValue;
        }

        public long Id { get; set; }

        /// <summary>
        /// Транслитерируемое название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Название партнера
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Название на английском
        /// </summary>
        public string TitleIn { get; set; }

        /// <summary>
        /// Сайт партнера
        /// </summary>
        public string LinkToPartner { get; set; }

        /// <summary>
        /// Номер позиции в списке партнеров. Для того чтобы выделить топ партнеров.
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Дата создания записи о партнере
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Логотип партнера
        /// </summary>
        public long? MainImageId { get; set; }

        /// <summary>
        /// Привязка к картинке
        /// </summary>
        public PartnerImage MainImage { get; set; }
    }
}
