﻿using System.Collections.Generic;

namespace DomainLogic.Models
{
    /// <summary>
    /// Модель тегов сообщений
    /// </summary>
    public class FeedbackTag
    {
        public FeedbackTag()
        {
            FeedbacksToTags = new List<FeedbackToTag>();
        }

        public int Id { get; set; }

        /// <summary>
        /// Транслитерируемое название тега
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Название тега
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Сообщения, к которым относится данный тег
        /// </summary>
        public List<FeedbackToTag> FeedbacksToTags { get; set; }
    }
}
