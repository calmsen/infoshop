﻿using DomainLogic.Interfaces.ImageInterfaces;
using System.Collections.Generic;

namespace DomainLogic.Models
{
    /// <summary>
    /// Модель картинки новости
    /// </summary>
    public class ArticleImage : IImage
    {
        public ArticleImage()
        {
            NewsToImages = new List<ArticleToImage>();
            NewsWithMainImage = new List<Article>();
        }

        public long Id { get; set; }

        /// <summary>
        /// Ширина картинки
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Высота картинки
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Название картинки
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Внешняя ссылка на картинку
        /// </summary>
        public string ExternalLink { get; set; }

        /// <summary>
        /// Номер позиции картинки. Нужно для отображения в галереи.
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Привязка к новостям 
        /// </summary>
        public List<ArticleToImage> NewsToImages { get; set; }

        /// <summary>
        /// Привязка к новостям, имеющие главную картинку
        /// </summary>
        public List<Article> NewsWithMainImage { get; set; }
    }
}
