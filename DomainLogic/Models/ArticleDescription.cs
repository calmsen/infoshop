﻿using System.Collections.Generic;

namespace DomainLogic.Models
{
    /// <summary>
    /// Модель описания новости
    /// </summary>
    public class ArticleDescription
    {
        public ArticleDescription()
        {
            News = new List<Article>();
        }

        public long Id { get; set; }

        /// <summary>
        /// Описание новости
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Описание новости на английском
        /// </summary>
        public string ContentIn { get; set; }

        /// <summary>
        /// Привязка к новости
        /// </summary>
        public List<Article> News { get; set; }
    }
}
