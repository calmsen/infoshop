﻿
namespace DomainLogic.Models
{
    public class FilterValueInline
    {
        public string Title { get; set; }

        public string TitleIn { get; set; }

        public double ValueAsDouble { get; set; }
    }
}
