﻿using System.Collections.Generic;

namespace DomainLogic.Models
{
    /// <summary>
    /// Описание товара
    /// </summary>
    public class Description
    {
        public Description()
        {
            Products = new List<Product>();
        }

        public long Id { get; set; }

        /// <summary>
        /// Описание товара
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Описание товара на английском
        /// </summary>
        public string ContentIn { get; set; }

        /// <summary>
        /// Привязка к товару
        /// </summary>
        public List<Product> Products { get; set; }
    }
}
