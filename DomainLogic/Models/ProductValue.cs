﻿namespace DomainLogic.Models
{
    /// <summary>
    /// Данный класс представляет значение товара для определенной характеристики
    /// </summary>
    public class ProductValue
    {
        public long Id { get; set; }

        /// <summary>
        /// Идентификатор значения фильтра
        /// </summary>
        public long? ValueId { get; set; }

        /// <summary>
        /// Идентификатор характеристики
        /// </summary>
        public long AttributeId { get; set; }

        /// <summary>
        /// Привязка к значению фильтра
        /// </summary>
        public FilterValue Value { get; set; }

        /// <summary>
        /// Привязка к характеристикам товара
        /// </summary>
        public DmAttribute Attribute { get; set; }
    }
}
