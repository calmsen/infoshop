﻿using DomainLogic.Interfaces.ImageInterfaces;
using System;
using System.Collections.Generic;

namespace DomainLogic.Models
{ 
    /// <summary>
    /// Данный класс описывает доменный объект картинки партнера. 
    /// </summary>
    public class PartnerImage : IImage
    {
        public PartnerImage()
        {
            PartnersWithMainImage = new List<Partner>();
        }

        public long Id { get; set; }

        /// <summary>
        /// Ширина картинки
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Высота картинки
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Название картинки
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Внешняя ссылка картинки
        /// </summary>
        public string ExternalLink { get; set; }

        /// <summary>
        /// Связь с моделью DbPartner
        /// </summary>
        public List<Partner> PartnersWithMainImage { get; set; }
    }
}