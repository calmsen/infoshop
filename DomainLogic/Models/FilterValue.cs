﻿using System.Collections.Generic;

namespace DomainLogic.Models
{
    /// <summary>
    /// Модель значения фильтра
    /// </summary>
    public class FilterValue
    {
        public long Id { get; set; }

        /// <summary>
        /// Название значения фильтра
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Названия значения фильтра на английском
        /// </summary>
        public string TitleIn { get; set; }

        /// <summary>
        /// Значение long
        /// </summary>
        public long ValueAsLong { get; set; }

        /// <summary>
        /// Значение double
        /// </summary>
        public double ValueAsDouble { get; set; }

        /// <summary>
        /// Наборы для хранения значения. Каждое значение занимает один бит из всех наборов.
        /// </summary>
        public long? ValueSet1 { get; set; }
        public long? ValueSet2 { get; set; }
        public long? ValueSet3 { get; set; }
        public long? ValueSet4 { get; set; }

        /// <summary>
        /// Номер позиции значения
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Идентификатор фильтра
        /// </summary>
        public long FilterId { get; set; }

        /// <summary>
        /// Привязка к фильтру
        /// </summary>
        public DmFilter Filter { get; set; }

        /// <summary>
        /// Привязка к значениям товаров
        /// </summary>
        public List<ProductValue> ProductValues { get; set; }
    }
}
