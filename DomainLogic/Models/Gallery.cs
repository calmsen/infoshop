﻿using DomainLogic.Interfaces.ImageInterfaces;
using System.Collections.Generic;

namespace DomainLogic.Models
{
    public class Gallery
    {
        public IEnumerable<IImage> Images { get; set; }

        public string Folder { get; set; }

        public long OmitImageId { get; set; }
    }
}