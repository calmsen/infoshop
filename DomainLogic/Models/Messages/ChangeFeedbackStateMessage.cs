﻿
namespace DomainLogic.Models.Messages
{
    public class ChangeFeedbackStateMessage
    {
        public string FeedbackUrl { get; set; }

        public User User { get; set; }

        public string Message { get; set; }
    }
}
