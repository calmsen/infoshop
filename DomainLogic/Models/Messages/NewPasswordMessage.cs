﻿namespace DomainLogic.Models.Messages
{
    public class NewPasswordMessage
    {
        public string UserPassword { get; set; }

        public string BaseUrl { get; set; }
    }
}
