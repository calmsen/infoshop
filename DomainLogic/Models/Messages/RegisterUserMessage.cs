﻿
namespace DomainLogic.Models.Messages
{
    public class RegisterUserMessage
    {
        public string UserPassword { get; set; }

        public string BaseUrl { get; set; }

        public string UserEmail { get; set; }
    }
}
