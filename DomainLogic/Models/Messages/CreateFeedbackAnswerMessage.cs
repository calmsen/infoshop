﻿namespace DomainLogic.Models.Messages
{
    public class CreateFeedbackAnswerMessage
    {
        public FeedbackAnswer FeedbackAnswer { get; set; }

        public string FeedbackUrl { get; set; }
    }
}
