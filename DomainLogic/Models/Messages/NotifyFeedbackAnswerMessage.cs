﻿
namespace DomainLogic.Models.Messages
{
    public class NotifyFeedbackAnswerMessage
    {
        public FeedbackAnswer FeedbackAnswer { get; set; }

        public string FeedbackUrl { get; set; }
    }
}
