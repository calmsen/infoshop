﻿
namespace DomainLogic.Models.Messages
{
    public class NotifyFeedbackMessage
    {
        public Feedback Feedback { get; set; }

        public string FeedbackUrl { get; set; }
    }
}
