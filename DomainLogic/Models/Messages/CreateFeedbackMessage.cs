﻿
namespace DomainLogic.Models.Messages
{
    public class CreateFeedbackMessage
    {
        public Feedback Feedback { get; set; }

        public string FeedbackUrl { get; set; }
    }
}
