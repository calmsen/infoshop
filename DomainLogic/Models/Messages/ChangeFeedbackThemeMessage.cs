﻿namespace DomainLogic.Models.Messages
{
    public class ChangeFeedbackThemeMessage
    {
        public Feedback Feedback { get; set; }

        public string FeedbackUrl { get; set; }
    }
}
