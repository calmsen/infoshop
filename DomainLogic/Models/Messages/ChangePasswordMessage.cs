﻿
namespace DomainLogic.Models.Messages
{
    public class ChangePasswordMessage
    {
        public string UserPassword { get; set; }

        public string BaseUrl { get; set; }
    }
}
