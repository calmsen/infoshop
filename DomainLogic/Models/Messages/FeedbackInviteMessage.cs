﻿
namespace DomainLogic.Models.Messages
{
    public class FeedbackInviteMessage
    {
        public long FeedbackId { get; set; }

        public string FeedbackUrl { get; set; }

        public string Message { get; set; }
    }
}
