﻿
namespace DomainLogic.Models.Messages
{
    public class ResetPasswordMessage
    {
        public string BaseUrl { get; set; }

        public string ResetPasswordUrl { get; set; }
    }
}
