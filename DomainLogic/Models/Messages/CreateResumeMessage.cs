﻿
namespace DomainLogic.Models.Messages
{
    public class CreateResumeMessage
    {
        public Resume Resume { get; set; }
        
        public string BaseUrl { get; set; }

        public string St0SiteHttpHost { get; set; }
    }
}