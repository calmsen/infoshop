﻿namespace DomainLogic.Models
{
    public class SectionToImage
    {
        public long SectionId { get; set; }

        public long ImageId { get; set; }

        public Section Section { get; set; }

        public SectionImage Image { get; set; }
    }
}