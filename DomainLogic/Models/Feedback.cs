﻿using DomainLogic.Models.Enumerations;
using System;
using System.Collections.Generic;

namespace DomainLogic.Models
{
    /// <summary>
    /// Модель сообщения
    /// </summary>
    public class Feedback
    {
        /// <summary>
        /// Инициазирует экземпляр класса DbFeedback
        /// </summary>
        public Feedback()
        {
            CreatedDate = DateTime.Now;
            State = StateEnum.New;
            Answers = new List<FeedbackAnswer>();
            FeedbackInvitings = new List<FeedbackInviting>();
            FeedbacksToTags = new List<FeedbackToTag>();
            FeedbacksToImages = new List<FeedbackToImage>();
        }
        public long Id { get; set; }

        /// <summary>
        /// Тема сообщения
        /// </summary>
        public FeedbackThemeEnum Theme { get; set; }

        /// <summary>
        /// Текст сообщения
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Дата создания сообщения
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Статус сообщения
        /// </summary>
        public StateEnum State { get; set; }

        /// <summary>
        /// Количество ответов на данное сообщение
        /// </summary>
        public int AnswersAmount { get; set; }

        /// <summary>
        /// Адрес с которого зашел посетитель на форму обратной связи
        /// </summary>
        public string UrlReferer { get; set; }

        /// <summary>
        /// Автор сообщения
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Пользователь, который установил последнее состояние сообщения
        /// </summary>
        public int UserIdWhoSetTheState { get; set; }

        /// <summary>
        /// Раздел к которому относится данное сообщение
        /// </summary>
        public long? SectionId { get; set; }

        /// <summary>
        /// Товар к которому относится данное сообщение
        /// </summary>
        public long? ProductId { get; set; }

        /// <summary>
        /// Привязка к автору сообщения
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Привязка к автору, который установил последнее состояние сообщения
        /// </summary>
        public User UserWhoSetTheState { get; set; }

        /// <summary>
        /// Привязка к разделу
        /// </summary>
        public Section Section { get; set; }

        /// <summary>
        /// Привязка к товару
        /// </summary>
        public Product Product { get; set; }

        /// <summary>
        /// Ответы на данное сообщение
        /// </summary>
        public List<FeedbackAnswer> Answers { get; set; }

        /// <summary>
        /// Приглашения на обсуждение данного сообщения
        /// </summary>
        public List<FeedbackInviting> FeedbackInvitings { get; set; }

        /// <summary>
        /// Привзяка к тегам
        /// </summary>
        public List<FeedbackToTag> FeedbacksToTags { get; set; }

        /// <summary>
        /// Привзяка к картинкам
        /// </summary>
        public List<FeedbackToImage> FeedbacksToImages { get; set; }
    }
}
