﻿using DomainLogic.Models.Enumerations;

namespace DomainLogic.Models
{
    /// <summary>
    /// Данный класс описывает модель сервисного центра
    /// </summary>
    public class Service
    {
        public long Id { get; set; }

        /// <summary>
        /// Название страны
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Название города
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Название адреса
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Номера телефонов
        /// </summary>
        public string Phones { get; set; }

        /// <summary>
        /// Часы работы
        /// </summary>
        public string WorkingHours { get; set; }

        /// <summary>
        /// Тип сервисного центра (ДНС сервис, ProService и др)
        /// </summary>
        public ServiceTypeEnum Type { get; set; }

        /// <summary>
        /// Идентификатор 
        /// </summary>
        public string Guid { get; set; }
    }
    
}