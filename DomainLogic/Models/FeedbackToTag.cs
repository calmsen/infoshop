﻿namespace DomainLogic.Models
{
    public class FeedbackToTag
    {
        public long FeedbackId { get; set; }

        public int TagId { get; set; }

        public Feedback Feedback { get; set; }

        public FeedbackTag Tag { get; set; }
    }
}