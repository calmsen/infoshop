﻿using System.Collections.Generic;

namespace DomainLogic.Models
{
    /// <summary>
    /// Модель региона
    /// </summary>
    public class Region
    {
        public long Id { get; set; }

        /// <summary>
        /// Название региона
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Псевдоним
        /// </summary>
        public string Alias { get; set; }

        /// <summary>
        /// Широтва
        /// </summary>
        public string Lat { get; set; }

        /// <summary>
        /// Долгота
        /// </summary>
        public string Lng { get; set; }

        /// <summary>
        /// Связь с сущностью - Адрес
        /// </summary>
        public List<Address> Addresses { get; set; }
    }
}
