﻿using DomainLogic.Interfaces.ImageInterfaces;

namespace DomainLogic.Models
{
    /// <summary>
    /// Модель картинки страницы
    /// </summary>
    public class PageImage : IImage
    {
        public long Id { get; set; }

        /// <summary>
        /// Ширина картинки
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Высота картинки
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Название картинки
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Внешняя ссылка на картинку
        /// </summary>
        public string ExternalLink { get; set; }

        /// <summary>
        /// Номер позиции картинки. Нужно для отображения в галереи.
        /// </summary>
        public int Position { get; set; }
    }
}