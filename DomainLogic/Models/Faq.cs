﻿using DomainLogic.Models.Enumerations;
using System.Collections.Generic;

namespace DomainLogic.Models
{
    /// <summary>
    /// Модель данных Вопрос - ответ
    /// </summary>
    public class Faq
    {
        public Faq()
        {
            ProductsToFaqs = new List<ProductToFaq>();
        }
        public long Id { get; set; }

        /// <summary>
        /// Название вопроса
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Название вопроса на английском
        /// </summary>
        public string TitleIn { get; set; }

        /// <summary>
        /// Вопрос 
        /// </summary>
        public string Question { get; set; }

        /// <summary>
        /// Вопрос на английском
        /// </summary>
        public string QuestionIn { get; set; }

        /// <summary>
        /// Ответ
        /// </summary>
        public string Answer { get; set; }

        /// <summary>
        /// Ответ на английском
        /// </summary>
        public string AnswerIn { get; set; }

        /// <summary>
        /// Тема, к которому относится данный вопрос
        /// </summary>
        public FeedbackThemeEnum Theme { get; set; }

        /// <summary>
        /// Отображать только менеджерам. Нужны для отображения шаблонов для автоответов в обратной связи
        /// </summary>
        public bool OnlyManager { get; set; }

        /// <summary>
        /// Идентификатор раздела, к которому привязан вопрос
        /// </summary>
        public long? SectionId { get; set; }

        /// <summary>
        /// Раздел, к которому привязан вопрос
        /// </summary>
        public Section Section { get; set; }

        /// <summary>
        /// Товары, к которым привязан вопрос
        /// </summary>
        public List<ProductToFaq> ProductsToFaqs { get; set; }
    }
}
