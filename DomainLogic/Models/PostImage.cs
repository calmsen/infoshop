﻿using DomainLogic.Interfaces.ImageInterfaces;
using System.Collections.Generic;

namespace DomainLogic.Models
{/// <summary>
 /// Модель картинки обзора
 /// </summary>
    public class PostImage : IImage
    {
        public PostImage()
        {
            PostsWithMainImage = new List<Post>();
        }

        public long Id { get; set; }

        /// <summary>
        /// Ширина картинки
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Высота картинки
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Название картинки
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Внешняя ссылка на картинку
        /// </summary>
        public string ExternalLink { get; set; }

        /// <summary>
        /// Привязка к обзорам, имеющие главную картинку
        /// </summary>
        public List<Post> PostsWithMainImage { get; set; }
    }
}
