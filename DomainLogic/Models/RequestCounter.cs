﻿using System;

namespace DomainLogic.Models
{
    /// <summary>
    /// Модель счетчика для запроса. Для измерения посещения и времени выполнения запроса.
    /// </summary>
    public class RequestCounter
    {
        /// <summary>
        /// Инициазирует экземпляр класса DbRequestCounter
        /// </summary>
        public RequestCounter()
        {
            RequestTime = DateTime.Now;
        }

        public long Id { get; set; }

        /// <summary>
        /// Время начала запроса
        /// </summary>
        public DateTime RequestTime { get; set; }

        /// <summary>
        /// Количество милисекунд выполнения запроса
        /// </summary>
        public int RequestExcevuteTime { get; set; }

        /// <summary>
        /// Адрес запроса
        /// </summary>
        public string RequestUrl { get; set; }

        /// <summary>
        /// Идентификатор города, с которого зашел посетитель
        /// </summary>
        public long? CityId { get; set; }

        /// <summary>
        /// Город, с которого зашел посетитель
        /// </summary>
        public City City { get; set; }
    }
}