﻿namespace DomainLogic.Models
{
    public class ProductToFile
    {
        public long ProductId { get; set; }

        public long FileId { get; set; }

        public Product Product { get; set; }

        public DmFile File { get; set; }
    }
}