﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DomainLogic.Models
{/// <summary>
 /// Данный класс описывает данные о товаре
 /// </summary>
    public class Product
    {
        /// <summary>
        /// Инициазирует экземпляр класса Product
        /// </summary>
        public Product()
        {
            CreatedDate = DateTime.Now;
            BannerPosition = Int32.MaxValue;
            Attributes = new List<DmAttribute>();
            ProductsToImages = new List<ProductToImage>();
            ProductsToPosts = new List<ProductToPost>();
            ProductsToFaqs = new List<ProductToFaq>();
            ProductsToNews = new List<ProductToArticle>();
            ProductsToFiles = new List<ProductToFile>();
        }

        public long Id { get; set; }

        /// <summary>
        /// Транслитерируемое название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Название товара
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Название на английском
        /// </summary>
        public string TitleIn { get; set; }

        /// <summary>
        /// Краткие характеристики товара
        /// </summary>
        public string ShortFeatures { get; set; }

        /// <summary>
        /// Коэфициент показывает какого формата картинка.
        /// </summary>
        public double MainImageK { get; set; }

        /// <summary>
        /// Сертификат картинки. Id картинки сайта или url внешней ссылки
        /// </summary>
        public string Sertificate { get; set; }

        /// <summary>
        /// Номер позиции баннера
        /// </summary>
        public int BannerPosition { get; set; }

        /// <summary>
        /// Дата создания товара
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Отображать ли товар в банере на главной странице
        /// </summary>
        public bool ToBanner { get; set; }

        /// <summary>
        /// Отображать ли товар в виджете "Популярные"
        /// </summary>
        public bool ToRating { get; set; }

        /// <summary>
        /// Разрешить отображение файлов в карточке товара
        /// </summary>
        public bool AllowFiles { get; set; }

        /// <summary>
        /// Опубликовать товар
        /// </summary>
        public bool Published { get; set; }

        /// <summary>
        /// Отметить товар как заполненный
        /// </summary>
        public bool Filled { get; set; }

        /// <summary>
        /// Активный ли товар.
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Находится ли товар в архиве
        /// </summary>
        public bool Archive { get; set; }

        /// <summary>
        /// Отображать ли товар в виджете "Новинки"
        /// </summary>
        public bool New { get; set; }

        /// <summary>
        /// Процент заполненности характеристик
        /// </summary>
        public int? FilledFeatures { get; set; }

        /// <summary>
        /// Служебный комментарий
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Цена
        /// </summary>
        public float Price { get; set; }

        /// <summary>
        /// Внешняя ссылка. Если указана то переход идет на этот адрес а не на карточку товара. Например на промо сайт товара.
        /// </summary>
        public string ExternalLink { get; set; }

        /// <summary>
        /// Код производителя
        /// </summary>
        public long? Code { get; set; }

        /// <summary>
        /// Штрихкод товара
        /// </summary>
        public long? Barcode { get; set; }

        /// <summary>
        /// Статус жизненного цикла (Готовится к производству, Снят с производства, Производится, Не покупаем)
        /// </summary>
        public long? StoreState { get; set; }

        /// <summary>
        /// Количестов просмотров
        /// </summary>
        public int ViewsAmount { get; set; }

        /// <summary>
        /// Раздел
        /// </summary>
        public long SectionId { get; set; }

        /// <summary>
        /// Тип изделия
        /// </summary>
        public long? TypeId { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public long? DescriptionId { get; set; }

        /// <summary>
        /// Главная картинка
        /// </summary>
        public long? MainImageId { get; set; }

        /// <summary>
        /// Картинка для баннера
        /// </summary>
        public long? BannerImageId { get; set; }

        /// <summary>
        /// Картинка для баннера на английском
        /// </summary>
        public long? BannerImageIdIn { get; set; }

        /// <summary>
        /// Привязка к разделу
        /// </summary>
        public Section Section { get; set; }

        /// <summary>
        /// Привязка к типу изделия
        /// </summary>
        public ProductType Type { get; set; }

        /// <summary>
        /// Привязка к описанию
        /// </summary>
        public Description Description { get; set; }

        /// <summary>
        /// Привязка к картинке
        /// </summary>
        public Image MainImage { get; set; }

        /// <summary>
        /// Привязка к баннеру
        /// </summary>
        public Image BannerImage { get; set; }

        /// <summary>
        /// Привязка к баннеру на английском
        /// </summary>
        public Image BannerImageIn { get; set; }

        /// <summary>
        /// Привязка к характеристики товара
        /// </summary>
        public List<DmAttribute> Attributes { get; set; }

        /// <summary>
        /// Привязка к снимкам товара. Нужно для просмотра и возврата к той или иной редакции товара
        /// </summary>
        public List<ProductSnapshot> Snapshots { get; set; }

        /// <summary>
        /// Привязка к сообщениям обратной связи
        /// </summary>
        public List<Feedback> Feedbacks { get; set; }

        /// <summary>
        /// Привязка к состояниям товара
        /// </summary>
        public List<ProductState> States { get; set; }

        /// <summary>
        /// Привязка к партнерам
        /// </summary>
        private List<PartnerLinkOnProduct> _partnersLinksOnProduct;
        public string PartnersLinksOnProductJson { get; set; }
        public List<PartnerLinkOnProduct> PartnersLinksOnProduct
        {
            get
            {
                if (_partnersLinksOnProduct == null)
                {
                    try
                    {
                        _partnersLinksOnProduct = JsonConvert.DeserializeObject<List<PartnerLinkOnProduct>>(PartnersLinksOnProductJson);
                    }
                    catch (Exception)
                    {
                        _partnersLinksOnProduct = new List<PartnerLinkOnProduct>();
                    }
                }
                return _partnersLinksOnProduct;
            }
            set
            {
                _partnersLinksOnProduct = value;
                PartnersLinksOnProductJson = JsonConvert.SerializeObject(value);
            }
        }

        /// <summary>
        /// Привязка к спискам картинок товара для галереи
        /// </summary>
        public List<ProductToImage> ProductsToImages { get; set; }

        /// <summary>
        /// Привзяка к обзорам
        /// </summary>
        public List<ProductToPost> ProductsToPosts { get; set; }

        /// <summary>
        /// Привязка к вопросам и ответам
        /// </summary>
        public List<ProductToFaq> ProductsToFaqs { get; set; }

        /// <summary>
        /// Товары, к которым привязан вопрос
        /// </summary>
        public List<ProductToArticle> ProductsToNews { get; set; }

        /// <summary>
        /// Привязка к файлам
        /// </summary>
        public List<ProductToFile> ProductsToFiles { get; set; }
    }
}
