﻿
namespace DomainLogic.Models
{
    public class FilterValueSettings
    {
        public long Id { get; set; }

        public bool Activated { get; set; }
    }
}
