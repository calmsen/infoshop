﻿using DomainLogic.Interfaces.ImageInterfaces;
using System.Collections.Generic;

namespace DomainLogic.Models
{
    /// <summary>
    /// Картинка товара
    /// </summary>
    public class Image : IImage
    {
        public Image()
        {
            ProductsToImages = new List<ProductToImage>();
            ProductsWithMainImage = new List<Product>();
            ProductsWithBanner = new List<Product>();
            ProductsWithBannerIn = new List<Product>();
        }

        public long Id { get; set; }

        /// <summary>
        /// Ширина картинки
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Высота картинки
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Название картинки
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Внешняяя ссылка
        /// </summary>
        public string ExternalLink { get; set; }

        /// <summary>
        /// Номер позиции в галереи
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Список картинок товара для галереи
        /// </summary>
        public List<ProductToImage> ProductsToImages { get; set; }

        /// <summary>
        /// Привязка к товарам, имеющие главную картинку 
        /// </summary>
        public List<Product> ProductsWithMainImage { get; set; }

        /// <summary>
        /// Привязка к товарам, имеющие баннер
        /// </summary>
        public List<Product> ProductsWithBanner { get; set; }

        /// <summary>
        /// Привязка к товарам, имеющие баннер на английском
        /// </summary>
        public List<Product> ProductsWithBannerIn { get; set; }
    }
}
