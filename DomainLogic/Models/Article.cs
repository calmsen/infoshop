﻿using System;
using System.Collections.Generic;

namespace DomainLogic.Models
{
    /// <summary>
    /// Модель новости
    /// </summary>
    public class Article
    {
        /// <summary>
        /// Инициазирует экземпляр класса DbArticle
        /// </summary>
        public Article()
        {
            NewsToImages = new List<ArticleToImage>();
            ProductsToNews = new List<ProductToArticle>();
            CreatedDate = DateTime.Now;
        }

        public long Id { get; set; }

        /// <summary>
        /// Транслитерируемое название новости
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Название новости
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Название новости на аглийском
        /// </summary>
        public string TitleIn { get; set; }

        /// <summary>
        /// Краткое описание новости
        /// </summary>
        public string Annottion { get; set; }

        /// <summary>
        /// Краткое описание новости на анлийском
        /// </summary>
        public string AnnottionIn { get; set; }

        /// <summary>
        /// Дата создания новости
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Указывает активна ли новость
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Номер позиции новости. Нужно для отображения топ новостей
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Идентификатор описания новости
        /// </summary>
        public long? DescriptionId { get; set; }

        /// <summary>
        /// Идентификатор картинки, являющиеся главной
        /// </summary>
        public long? MainImageId { get; set; }

        /// <summary>
        /// Описание новости
        /// </summary>
        public ArticleDescription Description { get; set; }

        /// <summary>
        /// Главная картинка новости
        /// </summary>
        public ArticleImage MainImage { get; set; }

        /// <summary>
        /// Картинки новости
        /// </summary>
        public List<ArticleToImage> NewsToImages { get; set; }

        /// <summary>
        /// Товары, к которым привязан вопрос
        /// </summary>
        public List<ProductToArticle> ProductsToNews { get; set; }
    }
}
