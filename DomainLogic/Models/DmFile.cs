﻿using DomainLogic.Models.Enumerations;
using System;
using System.Collections.Generic;

namespace DomainLogic.Models
{
    /// <summary>
    /// Модель данных о файлах
    /// </summary>
    public class DmFile
    {
        /// <summary>
        /// Инициазирует экземпляр класса DmFile
        /// </summary>
        public DmFile()
        {
            Filters = new FileFilters();
            ProductsToFiles = new List<ProductToFile>();
        }
        public long Id { get; set; }

        /// <summary>
        /// Транслитерируемое название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Название файла
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Название на английском
        /// </summary>
        public string TitleIn { get; set; }

        /// <summary>
        /// Описание файла
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Описание на английском
        /// </summary>
        public string DescriptionIn { get; set; }

        /// <summary>
        /// Версия файла
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Размер файла
        /// </summary>
        public string FileSize { get; set; }

        /// <summary>
        /// Дата создания файла
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Внешняя ссылка на файл
        /// </summary>
        public string ExternalLink { get; set; }

        /// <summary>
        /// Отображать в товарах с аналогичным разделом
        /// </summary>
        public bool DisplayWithSameSection { get; set; }

        /// <summary>
        /// Отображать в товарах с аналогичной платформой
        /// </summary>
        public bool DisplayWithSamePlatform { get; set; }

        /// <summary>
        /// Фильтры
        /// </summary>

        public FileFilters Filters { get; set; }

        /// <summary>
        /// Привязка к файлам
        /// </summary>
        public List<ProductToFile> ProductsToFiles { get; set; }
    }
}
