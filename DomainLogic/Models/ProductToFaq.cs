﻿
namespace DomainLogic.Models
{
    public class ProductToFaq
    {
        public long ProductId { get; set; }

        public long FaqId { get; set; }

        public Product Product { get; set; }

        public Faq Faq { get; set; }
    }
}