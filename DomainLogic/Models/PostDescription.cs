﻿using System.Collections.Generic;

namespace DomainLogic.Models
{
    /// <summary>
    /// Модель, описывающая описание обзора
    /// </summary>
    public class PostDescription
    {
        public PostDescription()
        {
            Posts = new List<Post>();
        }

        public long Id { get; set; }

        /// <summary>
        /// Описание обзора
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Описание обзора на английском
        /// </summary>
        public string ContentIn { get; set; }

        /// <summary>
        /// Привязка к обзору
        /// </summary>
        public List<Post> Posts { get; set; }
    }
}
