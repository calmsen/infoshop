﻿using DomainLogic.Models.Enumerations;
using System;

namespace DomainLogic.Models
{
    /// <summary>
    /// Состояния модификации товара.
    /// </summary>
    public class ProductState
    {
        /// <summary>
        /// Инициазирует экземпляр класса ProductState
        /// </summary>
        public ProductState()
        {
            Date = DateTime.Now;
        }

        public long Id { get; set; }

        /// <summary>
        /// Состояние товара
        /// </summary>
        public ProductStateEnum State { get; set; }

        /// <summary>
        /// Дата заведения состояния товара
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Идентификатор товара
        /// </summary>
        public long ProductId { get; set; }

        /// <summary>
        /// Идентификатор пользователя, который  установил состояние
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Привязка к товару
        /// </summary>
        public Product Product { get; set; }

        /// <summary>
        /// Привязка к пользователю, который установил состояние
        /// </summary>
        public User User { get; set; }
    }
}
