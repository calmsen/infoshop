﻿

namespace DomainLogic.Models
{
    /// <summary>
    /// Данный класс описывает модель счетчика для страниц товара. 
    /// Сначала все посещения записываются в данную табличку. 
    /// Затем из данный таблички обновляются(через определенное время) просмотры у товаров.
    /// </summary>
    public class ProductCounter
    {
        public long Id { get; set; }

        /// <summary>
        /// Количество просмотров
        /// </summary>
        public int ViewsAmount { get; set; }

        /// <summary>
        /// Состояние указывающее нужно ли обновить счетчик у товара
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// Идентификатор товара, который посетили
        /// </summary>
        public long ProductId { get; set; }

        /// <summary>
        /// Идентификатор города посетителя
        /// </summary>
        public long? CityId { get; set; }

        /// <summary>
        /// Привязка к городу посетителя
        /// </summary>
        public City City { get; set; }
    }
}