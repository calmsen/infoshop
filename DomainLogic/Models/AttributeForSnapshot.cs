﻿using System.Collections.Generic;

namespace DomainLogic.Models
{
    public class AttributeForSnapshot
    {
        public long FilterId { get; set; }

        public List<ValueForSnapshot> Values { get; set; }
    }
}
