﻿using System.Collections.Generic;

namespace DomainLogic.Models
{
    /// <summary>
    /// Данный класс описывает описание ленты обновлений
    /// </summary>
    public class SupportArticleDescription
    {
        public SupportArticleDescription()
        {
            Articles = new List<SupportArticle>();
        }

        public long Id { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Описание на английском
        /// </summary>
        public string ContentIn { get; set; }

        /// <summary>
        /// Привязка ленте обновления
        /// </summary>
        public List<SupportArticle> Articles { get; set; }
    }
}