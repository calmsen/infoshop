﻿
namespace DomainLogic.Models
{
    /// <summary>
    /// Модель приглашения для обсуждения сообщения
    /// </summary>
    public class FeedbackInviting
    {
        public int Id { get; set; }

        /// <summary>
        /// Электронный адреса на которые отправить приглашение
        /// </summary>
        public string Emails { get; set; }

        /// <summary>
        /// Идентификатор сообщения, которое нужно обсудить
        /// </summary>
        public long FeedbackId { get; set; }

        // <summary>
        /// Привязка к сообщению, которое нужно обсудить
        /// </summary>
        public Feedback Feedback { get; set; }
    }
}
