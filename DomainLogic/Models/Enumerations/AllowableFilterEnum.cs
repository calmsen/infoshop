﻿using System.ComponentModel;

namespace DomainLogic.Models.Enumerations
{
    public enum AllowableFilterEnum
    {
        [Description("None")]
        None,

        [Description("С баннерами")]
        ToBanner,

        [Description("Популярные")]
        ToRating,

        [Description("Новинки")]
        New,

        [Description("Не опубликованные")]
        Unpublished,

        [Description("Опубликованные")]
        Published,

        [Description("С картинками")]
        WithPhoto,

        [Description("Без картинок")]
        WithoutPhoto,

        [Description("C описанием")]
        WithDescription,

        [Description("Без описания")]
        WithoutDescription,

        [Description("C характеристиками")]
        FilledFeatures,

        [Description("Без характеристик")]
        UnfilledFeatures,

        [Description("Отмечены как заполненные")]
        Filled,

        [Description("Не отмечены как заполненные")]
        Unfilled,

        [Description("C кодами 1c")]
        WithCode,

        [Description("Без кодов 1c")]
        WithoutCode,

        [Description("Не указана модель")]
        WithoutModel,

        [Description("Без заголовка на английском")]
        WithoutTitleIn,

        [Description("Без описания на английском")]
        WithoutDescriptionIn,

        [Description("Без руководства пользователя")]
        WithoutManual
    }
}
