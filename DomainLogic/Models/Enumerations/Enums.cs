﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DomainLogic.Models.Enumerations
{
    /// <summary>
    /// Абстрактный класс представляет из себя контейнер для хранения списка значений перечислений.
    /// Физически данные будут хранится в поле AsInt.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class Enums<T> : ICollection<T>
    {
        public List<T> List { get; set; }

        public Enums()
        {
            List = new List<T>();
        }

        public int AsInt
        {
            get
            {
                int v = 0;
                foreach (T e in List)
                    v += Convert.ToInt32(e as Enum);
                return v;
            }
            set
            {
                List.Clear();

                if (value == 0)
                    return;

                foreach (T e in Enum.GetValues(typeof(T)))
                    if ((value & Convert.ToInt32(e as Enum)) != 0)
                        List.Add(e);
            }
        }

        public void Add(T item)
        {
            List.Add(item);
        }

        public void Clear()
        {
            List.Clear();
        }

        public bool Contains(T item)
        {
            return List.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            List.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return List.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(T item)
        {
            return List.Remove(item);
        }
                
        public IEnumerator<T> GetEnumerator()
        {
            return List.GetEnumerator();
        }
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            return List.GetEnumerator();
        }
    }
}
