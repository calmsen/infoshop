﻿using System.ComponentModel;

namespace DomainLogic.Models.Enumerations
{
    public enum FilterTypeEnum
    {
        [Description("None")]
        None,

        [Description("Список")]
        Select,

        [Description("Радиобоксы")]
        Radio,

        [Description("Чекбоксы")]
        Checkbox,

        [Description("Диапозон")]
        Range,

        [Description("Отсутсвует")]
        Absent
    }
}
