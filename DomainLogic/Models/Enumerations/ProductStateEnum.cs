﻿using System.ComponentModel;

namespace DomainLogic.Models.Enumerations
{
    public enum ProductStateEnum
    {
        [Description("None")]
        None = 0,

        [Description("создал")]
        Created = 1,

        [Description("модифицировал")]
        Modified = 2,

        [Description("загрузил картинки")]
        UploadedImages = 4,

        [Description("удалил картинки")]
        UnuploadedImages = 8,

        [Description("заполнил характеристики")]
        FilledFeatures = 16,

        [Description("удалил характеристики")]
        UnfilledFeatures = 32,

        [Description("отметил как заполнено")]
        Filled = 64,

        [Description("удалил отметкку как заполнено")]
        Unfilled = 128,

        [Description("опубликовал")]
        Published = 256,

        [Description("удалил публикацию")]
        Unpublished = 512,

        [Description("создал описание")]
        FilledDescription = 1024,

        [Description("удалил описание")]
        UnfilledDescription = 2048,

        [Description("модифицировал описани" +
            "е")]
        ModifiedDescription = 4096,
        [Description("изменил картинки")]
        ModifiedImages = 8192,

        [Description("модифицировал характеристики")]
        ModifiedFeatures = 16384,

        [Description("изменил цену")]
        ModifiedPrice = 32768,

        [Description("изменил код производителя")]
        ModifiedCode = 65536
    }
}
