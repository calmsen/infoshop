﻿using System.ComponentModel;

namespace DomainLogic.Models.Enumerations
{
    public enum FileFilterEnum
    {
        [Description("None")]
        None = 0,

        [Description("Resources.FilesRes.FileFilter1")]
        Filter_1 = 1,

        [Description("Resources.FilesRes.FileFilter2")]
        Filter_2 = 2,

        [Description("Resources.FilesRes.FileFilter3")]
        Filter_3 = 4,

        [Description("Resources.FilesRes.FileFilter4")]
        Filter_4 = 8
    }
}
