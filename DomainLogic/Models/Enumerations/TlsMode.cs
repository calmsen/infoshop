﻿namespace DomainLogic.Models.Enumerations
{
    public enum TlsMode
    {
        HttpContext,
        CallContext
    }
}
