﻿
namespace DomainLogic.Models.Enumerations
{
    /// <summary>
    /// Класс представляет контейнер для хранения списка значений перечисления FileFilterEnum
    /// </summary>
    public class FileFilters : Enums<FileFilterEnum>
    {
        public string TestC { get; set; }

        public FileFilters() : base()
        {
        }

        public FileFilters(int i)
            : base()
        {
            AsInt = i;
        }
    }
}
