﻿using System.ComponentModel;

namespace DomainLogic.Models.Enumerations
{
    public enum VacancyStateEnum
    {
        [Description("None")]
        None,

        [Description("Активная")]
        Active,

        [Description("Неактивная")]
        Inactive
    }
}
