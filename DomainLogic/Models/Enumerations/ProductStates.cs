﻿
namespace DomainLogic.Models.Enumerations
{
    /// <summary>
    /// Класс представляет контейнер для хранения списка значений перечисления ProductStateEnum
    /// </summary>
    public class ProductStates : Enums<ProductStateEnum>
    {
        public ProductStates() : base()
        {
        }

        public ProductStates(int i)
            : base()
        {
            AsInt = i;
        }
    }
}
