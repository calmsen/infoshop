﻿
using System.ComponentModel;

namespace DomainLogic.Models.Enumerations
{
    public enum FeedbackThemeEnum
    {
        [Description("None")]
        None,

        [Description("Resources.FeedbackRes.FeedbackThemeEnumServiceQuestions")]
        ServiceQuestions,

        [Description("Resources.FeedbackRes.FeedbackThemeEnumSellingQuestions")]
        SellingQuestions,

        [Description("Resources.FeedbackRes.FeedbackThemeEnumFeaturesAndCompleteSets")]
        FeaturesAndCompleteSets,

        [Description("Resources.FeedbackRes.FeedbackThemeEnumEquipmentQuestions")]
        EquipmentQuestions,

        [Description("Resources.FeedbackRes.FeedbackThemeEnumSiteError")]
        SiteError,

        [Description("Resources.FeedbackRes.FeedbackThemeEnumOther")]
        Other
    }
}
