﻿using System.ComponentModel;

namespace DomainLogic.Models.Enumerations
{
    public enum ServiceTypeEnum
    {
        [Description("None")]
        None,

        [Description("Resources.SrvsRes.Service1")]
        Service1,

        [Description("Resources.SrvsRes.Service2")]
        Service2,

        [Description("Resources.SrvsRes.Service3")]
        Service3,

        [Description("Resources.SrvsRes.Others")]
        Others
    }
}
