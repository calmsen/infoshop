﻿using System.ComponentModel;

namespace DomainLogic.Models.Enumerations
{
    public enum StateEnum
    {
        [Description("None")]
        None,

        [Description("Новый")]
        New,

        [Description("В работе")]
        Active,

        [Description("Отклонен")]
        Unapproved,

        [Description("Решен")]
        Decided
    }
}
