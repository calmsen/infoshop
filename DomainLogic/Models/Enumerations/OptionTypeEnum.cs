﻿namespace DomainLogic.Models.Enumerations
{
    public enum OptionTypeEnum
    {
        String,
        Long,
        Int,
        Bool,

        ListOfStrings,
        ListOfLongs,
        ListOfInts,
        ListOfBools,

        DictionaryOfStrings,
        DictionaryOfLongs,
        DictionaryOfInts,
        DictionaryOfBools,
    }
}
