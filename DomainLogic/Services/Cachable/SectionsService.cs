﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainLogic.Services.Cachable
{
    [Cachable]
    public class SectionsService : ISectionsService
    {
        private ISectionsService _sectionsService;

        private List<Section> _cachedSections;

        private Dictionary<long, List<SectionToImage>> _cachedSectionBanners = new Dictionary<long, List<SectionToImage>>();

        private void ClearCache()
        {
            _cachedSections = null;
            _cachedSectionBanners.Clear();
        }
        
        public SectionsService(ISectionsService sectionsService)
        {
            _sectionsService = sectionsService;
        }
        
        /// <summary>
        /// Получает все разделы
        /// </summary>
        /// <returns></returns>
        public async Task<List<Section>> GetSectionsAsync()
        {
            return await GetCachedSectionsAsync();
        }
        
        /// <summary>
        /// Получает разделы по списку идентификаторов
        /// </summary>
        /// <param name="sectionIds"></param>
        /// <returns></returns>
        public async Task<List<Section>> GetSectionsAsync(List<long> sectionIds)
        {
            return (await GetCachedSectionsAsync())
                .Where(x => sectionIds.Contains(x.Id))
                .MapList<Section>();
        }
        
        /// <summary>
        /// Получает дочерние разделы
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<List<Section>> GetSectionChildsAsync(long sectionId)
        {
            var sections = new List<Section>();
            foreach (var s in (await GetCachedSectionsAsync()))
                if (s.ParentId == sectionId)
                    sections.Add(s);
            return sections
                .MapList<Section>();
        }
        
        /// <summary>
        /// Получает коллекцию из потомков и себя по пути разделов (не кэшируется)
        /// </summary>
        /// <param name="pathOfSections"></param>
        /// <returns></returns>
        public async Task<List<Section>> GetDescendenOrSelfSectionsAsync(long pathOfSections)
        {
            return await _sectionsService.GetDescendenOrSelfSectionsAsync(pathOfSections);
        }
        
        /// <summary>
        /// Получает соседние разделы
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<List<Section>> GetSectionSiblingsAsync(long sectionId)
        {
            long parentId = (await GetCachedSectionsAsync()).FirstOrDefault(x => x.Id == sectionId).ParentId;
            var sections = new List<Section>();
            foreach (var s in (await GetCachedSectionsAsync()))
                if (s.ParentId == parentId)
                    sections.Add(s);
            return sections;
        }
        
        /// <summary>
        /// Получает разделы в которых есть товары (не кэшируется)
        /// </summary>
        /// <returns></returns>
        public async Task<List<Section>> GetSectionsWithProductsAsync()
        { 
            return await _sectionsService.GetSectionsWithProductsAsync();
        }
        
        /// <summary>
        /// Получает список разделов по списку идентификаторов
        /// </summary>
        /// <param name="sectionIds"></param>
        /// <returns></returns>
        public async Task<List<Section>> GetSectionsByIdsAsync(List<long> sectionIds)
        {
            if (sectionIds == null || sectionIds.Count == 0)
                return new List<Section>();
            return (await GetCachedSectionsAsync())
                .Where(x => sectionIds.Contains(x.Id))
                .ToList();
        }
        
        /// <summary>
        /// Получает раздел по наименованию
        /// </summary>
        /// <param name="sectionName"></param>
        /// <param name="needIncludeBanner"></param>
        /// <returns></returns>
        public async Task<Section> GetSectionByNameAsync(string sectionName, bool needIncludeBanner = false)
        {
            Section section = (await GetCachedSectionsAsync()).FirstOrDefault(x => x.Name == sectionName)?.Map<Section>();
            await SetSectionBannesIfNeedAsync(section, needIncludeBanner);
            return section;
        }
        
        /// <summary>
        /// Получает раздел по идентификатору
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="needIncludeBanner"></param>
        /// <returns></returns>
        public async Task<Section> GetSectionByIdAsync(long sectionId, bool needIncludeBanner = false)
        {
            Section section = (await GetCachedSectionsAsync()).FirstOrDefault(x => x.Id == sectionId)?.Map<Section>();
            await SetSectionBannesIfNeedAsync(section, needIncludeBanner);
            return section;
        }
        
        /// <summary>
        /// Проверяет имеет ли раздел дочерние разделы
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<bool> HasChildsAsync(long sectionId)
        {
            return (await GetCachedSectionsAsync()).Where(x => x.ParentId == sectionId).Count() > 0;
        }
        
        /// <summary>
        /// Получает заголовки разделов для указанных фильтров (не кэшируется)
        /// </summary>
        /// <param name="filterIds"></param>
        /// <returns></returns>
        public async Task<List<SectionTitle>> GetSectionTitlesAsync(List<long> filterIds) 
        {
            return await _sectionsService.GetSectionTitlesAsync(filterIds);
        }

        /// <summary>
        /// Обновляет раздел
        /// </summary>
        /// <param name="section"></param>
        public async Task EditSectionAsync(Section section)
        {
            await _sectionsService.EditSectionAsync(section);
            ClearCache();
        }
        
        /// <summary>
        /// Создает раздел
        /// </summary>
        /// <param name="section"></param>
        public async Task CreateSectionAsync(Section section)
        {
            await _sectionsService.CreateSectionAsync(section);
            ClearCache();
        }
        
        /// <summary>
        /// Удаляет раздел
        /// </summary>
        /// <param name="sectionId"></param>
        public async Task DeleteSectionAsync(long sectionId)
        {
            await _sectionsService.DeleteSectionAsync(sectionId);
            ClearCache();

        }
        
        /// <summary>
        /// Сохраняет позиции разделов
        /// </summary>
        /// <param name="sections"></param>
        public async Task SaveSectionsPositionsAsync(List<Section> sections)
        {
            await _sectionsService.SaveSectionsPositionsAsync(sections);
            ClearCache();
        }        
        
        /// <summary>
        /// Отвязывает все товары от раздела
        /// </summary>
        /// <param name="sectionId"></param>
        [Transaction]
        public async Task UnbindProductsAsync(long sectionId) 
        {
            await _sectionsService.UnbindProductsAsync(sectionId);
            ClearCache();
        }

        public void MapSectionChildsFromList(List<Section> sections, Section section)
        {
            _sectionsService.MapSectionChildsFromList(sections, section);
        }

        public List<Section> SectionsToTreeFromList(List<Section> sections, long parentId, bool isGetHidden = true)
        {
            return _sectionsService.SectionsToTreeFromList(sections, parentId, isGetHidden);
        }

        public List<List<Section>> GetSectionsAsPathSiblingsFromList(List<Section> sections, long sectionId = 0, bool isGetChilds = false)
        {
            return  _sectionsService.GetSectionsAsPathSiblingsFromList(sections, sectionId, isGetChilds);
        }

        public List<Section> GetDescendenSectionsFromList(List<Section> sections, Section section, bool isGetHidden = true)
        {
            return _sectionsService.GetDescendenSectionsFromList(sections, section, isGetHidden);
        }

        public List<long> GetSectionChildsIdsFromList(List<Section> sections, Section section, bool isGetHidden = true)
        {
            return _sectionsService.GetSectionChildsIdsFromList(sections, section, isGetHidden);
        }

        public Section GetSectionByIdFromList(List<Section> sections, long sectionId)
        {
            return _sectionsService.GetSectionByIdFromList(sections, sectionId);
        }

        public List<Section> GetSectionChildsFromList(List<Section> sections, long sectionId)
        {
            return _sectionsService.GetSectionChildsFromList(sections, sectionId);
        }

        public List<Section> GetSectionByQueryFromList(List<Section> sections, string query = "")
        {
            return _sectionsService.GetSectionByQueryFromList(sections, query);
        }

        public List<Section> GetPathOfSectionsFromList(List<Section> sections, Section section)
        {
            return _sectionsService.GetPathOfSectionsFromList(sections, section);
        }

        public string GetPathOfSectionNamesFromList(List<Section> sections, Section section)
        {
            return _sectionsService.GetPathOfSectionNamesFromList(sections, section);
        }

        public string GetPathOfSectionIdsFromList(List<Section> sections, Section section)
        {
            return _sectionsService.GetPathOfSectionIdsFromList(sections, section);
        }

        private async Task<List<Section>> GetCachedSectionsAsync()
        {
            if (_cachedSections == null)
                _cachedSections = await _sectionsService.GetSectionsAsync();
            return _cachedSections;
        }

        /// <summary>
        /// Устанавливает баннеры для раздела
        /// </summary>
        /// <param name="section"></param>
        /// <param name="needIncludeBanner"></param>
        /// <returns></returns>
        private async Task SetSectionBannesIfNeedAsync(Section section, bool needIncludeBanner = false)
        {
            if (section == null)
                return;

            if (!needIncludeBanner)
                return;

            if (!_cachedSectionBanners.ContainsKey(section.Id))
            {
                Section sectionWithBanners = await _sectionsService.GetSectionByIdAsync(section.Id, true);
                _cachedSectionBanners[section.Id] = sectionWithBanners.SectionsToImages;
            }

            section.SectionsToImages = _cachedSectionBanners[section.Id];
        }
    }
}