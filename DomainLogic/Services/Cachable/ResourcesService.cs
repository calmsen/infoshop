﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainLogic.Services.Cachable
{
    [Cachable]
    public class ResourcesService: IResourcesService
    {
        private IResourcesService _resourcesService;
        
        private List<Resource> _cachedResources;

        public ResourcesService(IResourcesService resourcesService)
        {
            _resourcesService = resourcesService;
        }

        /// <summary>
        /// Получает все ресурсы
        /// </summary>
        /// <returns></returns>
        public List<Resource> GetResources()
        {
            return GetCachedRersources();
        }

        /// <summary>
        /// Получает ресурс по ключу
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public Resource GetResourceByKey(string key)
        {
            return GetCachedRersources().FirstOrDefault(x => x.Key == key);
        }

        /// <summary>
        ///  Создает ресурс
        /// </summary>
        /// <param name="resource"></param>
        public async Task CreateResourceAsync(Resource resource)
        {
            if (GetResourceByKey(resource.Key) != null)
                throw new ArgumentException($"Ресурс с ключом {resource.Key} уже был создан.");
            await _resourcesService.CreateResourceAsync(resource);
            GetCachedRersources().Add(resource);
        }

        /// <summary>
        /// Обновляет ресурс
        /// </summary>
        /// <param name="resource"></param>
        public async Task UpdateResourceAsync(Resource resource)
        {
            Resource oldResource = GetResourceByKey(resource.Key);
            if (oldResource == null)
                throw new ArgumentException($"Ресурс с ключом {resource.Key} не существует.");
            await _resourcesService.UpdateResourceAsync(resource);
            GetCachedRersources().Remove(oldResource);
            GetCachedRersources().Add(resource);

        }

        /// <summary>
        /// Создает или обновляет ресурс
        /// </summary>
        /// <param name="resource"></param>
        public async Task CreateOrUpdateResourceAsync(Resource resource)
        {
            Resource oldResource = GetResourceByKey(resource.Key);
            if (oldResource == null)
            {
                await CreateResourceAsync(resource);
            }
            else
            {
                await UpdateResourceAsync(resource);
            }            
        }

        public string ReadResource(string key)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, string> ReadResources(string classKey)
        {
            throw new NotImplementedException();
        }

        private List<Resource> GetCachedRersources()
        {
            if (_cachedResources == null)
                _cachedResources = _resourcesService.GetResources();
            return _cachedResources;
        }
    }
}