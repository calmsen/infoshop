﻿using DomainLogic.Infrastructure;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLogic.Services
{
    public class RequestCounterService : BaseService, IRequestCounterService
    {
        /// <summary>
        /// Создает посещение 
        /// </summary>
        /// <param name="requestCounter"></param>
        public async Task CreateRequestCounterAsync(RequestCounter requestCounter)
        {
            await QueryBuilderFactory.Create<RequestCounter>().InsertAsync(requestCounter);
        }

        /// <summary>
        /// Получает список посещений с указанным количестовм элементов
        /// </summary>
        /// <param name="pagination"></param>
        /// <returns></returns>
        public async Task<List<RequestCounter>> GetRequestCounterListAsync(Pagination pagination)
        {
            pagination.ItemsAmount = await QueryBuilderFactory.Create<RequestCounter>().CountAsync();
            if (pagination.ItemsAmount == 0)
                return new List<RequestCounter>();
            pagination.Refresh();
            return await QueryBuilderFactory.Create<RequestCounter>()
                .OrderByDescending(p => p.Id)
                .Skip(pagination.Offset)
                .Take(pagination.PageItemsAmount)
                .ToListAsync();
        }
    }
}
