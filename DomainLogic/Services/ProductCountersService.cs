﻿using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Attributes;

namespace DomainLogic.Services
{
    public class ProductCountersService :BaseService, IProductCountersService
    {
        /// <summary>
        /// Получает счетчик по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ProductCounter> GetAsync(long id)
        {
            return await QueryBuilderFactory.Create<ProductCounter>()
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        /// <summary>
        /// Получает счетчик по товару
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        private async Task<ProductCounter> GetByProductIdAsync(long productId)
        {
            return await QueryBuilderFactory.Create<ProductCounter>()
                .FirstOrDefaultAsync(x => x.ProductId == productId);
        }

        /// <summary>
        /// Создает счетчик
        /// </summary>
        /// <param name="counter"></param>
        [Transaction]
        public async Task AddAsync(ProductCounter counter)
        {
            await QueryBuilderFactory.Create<ProductCounter>().InsertAsync(counter);
        }

        /// <summary>
        /// Обновляет счетчик
        /// </summary>
        /// <param name="counter"></param>
        [Transaction]
        public async Task UpdateAsync(ProductCounter counter)
        {
            await QueryBuilderFactory.Create<ProductCounter>().UpdateAsync(counter);
        }

        /// <summary>
        /// Создает или обновляет счетчик
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="cityId"></param>
        [Transaction]
        public async Task AddOrUpdateByProductAsync(long productId, long? cityId)
        {
            ProductCounter counter = await GetByProductIdAsync(productId);
            if (counter == null)
                await AddAsync(new ProductCounter
                {
                    ProductId = productId,
                    ViewsAmount = 1,
                    CityId = cityId
                });
            else
            {
                counter.ViewsAmount++;
                counter.State = 1;
                counter.CityId = cityId;
                await UpdateAsync(counter);
            }
        }
    }
}
