﻿using DomainLogic.Interfaces.QueryMappers;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using DomainLogic.Infrastructure.Pdf;
using DomainLogic.Models.Settings;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Mappers;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Services.Shared.Providers;
using DomainLogic.Services.Shared.Editors;
using DomainLogic.Models.Enumerations;

namespace DomainLogic.Services
{
    public partial class ProductsService : BaseService, IProductsService
    {
        [Inject]
        public ProductsProvider ProductProvider { get; set; }
        [Inject]
        public FiltersProvider FiltersProvider { get; set; }
        [Inject]
        public FiltersValuesProvider FiltersValuesProvider { get; set; }
        [Inject]
        public SectionsProvider SectionsProvider { get; set; }
        [Inject]
        public ProductSnapshotEditor ProductSnapshotEditor { get; set; }
        [Inject]
        public ProductMapper ProductMapper { get; set; }
        [Inject]
        public SectionsUtils SectionsUtils { get; set; }
        [Inject]
        public ExcelUtils ExcelUtils { get; set; }
        [Inject]
        public CommonUtils CommonUtils { get; set; }
        [Inject]
        public IUrlUtils UrlUtils { get; set; }

        [Inject]
        public IProductsQueryMapper ProductsQueryMapper { get; set; }
        [Inject]
        public IProductCountersQueryMapper ProductCountersQueryMapper { get; set; }

        [Inject]
        public ImagesLoaderSettings ImagesLoaderSettings { get; set; }
        [Inject]
        public St0SiteSettings St0SiteSettings { get; set; }

        public async Task<byte[]> GenerateProductsPresentationAsync(long sectionId) 
        {
            List<Product> products = await GetProductsAsync(sectionId: sectionId, hasPhoto: true, isPublished: true, isActive: true);
            products.Sort((x, y) => x.Title.CompareTo(y.Title));

            Section section = await SectionsProvider.GetSectionByIdAsync(sectionId);
            //
            PresentationItem[] items = products.Select(x => new PresentationItem {
                ImagePath = St0SiteSettings.Root + @"Images\Products\normal\" + x.MainImageId + ".jpg",
                Header = x.Title,
                Description = x.ShortFeatures != null ? Regex.Replace(x.ShortFeatures, @"^\[|\]$", "").Replace("/", " / ") : ""
            })
            .ToArray();                                                                  
            //
            //File.WriteAllText(@"d:\Temp\PresentationItemsAsJson.txt", JsonConvert.SerializeObject(items)); 
            //
            PresentationGenerator pg = new PresentationGenerator(items, 3);
            return pg.GeneratePresentation(AppDomain.CurrentDomain.BaseDirectory + @"Content\Images\presentation-cover-" + sectionId + ".jpg", section.Title, AppDomain.CurrentDomain.BaseDirectory + @"Content\Styles\Images\InfoShop-text.png", section.Title);
        }
        
        /// <summary>
        /// Получает список товаров по указанным фильтрам
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="sectionId"></param>
        /// <param name="fParams"></param>
        /// <param name="sorts"></param>
        /// <param name="isArchive"></param>
        /// <param name="price"></param>
        /// <returns></returns>
        public async Task<List<Product>> GetProductsAsync(Pagination pagination, long sectionId, List<FilterParam> fParams, List<Sort> sorts, int selectType, bool isArchive, double[] price)
        {
            Section section = null;
            if (sectionId > 0)
            {
                section = await SectionsProvider.GetSectionByIdAsync(sectionId);
            }
            if (section != null && section.Hidden)
                return new List<Product>();
            //
            if (sorts == null)
                sorts = new List<Sort>();
            if (sorts.Count == 0)
                sorts.Add(new Sort
                {
                    Type = SortType.Field,
                    Order = OrderType.Asc,
                    ColumnName = "Title"
                });
            //
            long filterIdByGroupType = 0L;
            //
            List<Product> products = null;
            if ((fParams == null || fParams.Count == 0) && !sorts.Any(x => x.Type.Equals(SortType.Filter)) && selectType == 0)
            {
                long leftPath = section != null ? section.PathOfSections : 0;
                long rightPath = SectionsUtils.GetRightPath(leftPath);
                var query = QueryBuilderFactory.Create<Product>()
                    .Where(x => x.Published && x.Active && x.Archive == isArchive && !x.Section.Hidden && x.Section.PathOfSections >= leftPath && x.Section.PathOfSections < rightPath);
                if (price != null && price.Length == 2)
                {
                    double priceFrom = double.IsNaN(price[0]) ? 0 : price[0];
                    query = query.Where(x => x.Price >= priceFrom);
                    if (price[1] > 0)
                    {
                        double priceTo = double.IsNaN(price[1]) ? 0 : price[1];
                        query = query.Where(x => x.Price <= priceTo);
                    }
                }

                pagination.ItemsAmount = await query.CountAsync();
                if (pagination.ItemsAmount == 0)
                    return new List<Product>();
                pagination.Refresh();
                string orderBy = string.Empty;
                for (int i = 0; i < sorts.Count; i++)
                {
                    if (!orderBy.Equals(string.Empty))
                        orderBy += ",";

                    if (sorts[i].ColumnName.Equals("PathOfSections"))
                    {
                        orderBy += "Section.";
                    }
                    orderBy += sorts[i].ColumnName + " " + sorts[i].Order;
                }
                products = await query
                    .OrderBy(orderBy)
                    .Skip(pagination.Offset)
                    .Take(pagination.PageItemsAmount)
                    .ToListAsync();
            }
            else
            {
                // узнаем ид фильтра
                Sort groupObj = sorts.FirstOrDefault(x => x.Type == SortType.Filter);
                if (groupObj != null)
                {
                    DmFilter filter = await FiltersProvider.GetFilterByNameAsync(groupObj.ColumnName, sectionId);
                    if (filter != null)
                    {
                        filterIdByGroupType = filter.Id;
                    }
                }
                List<FilterValue> values = await FiltersValuesProvider.GetFiltersValuesAsync(fParams);
                products = await ProductsQueryMapper.GetProductsAsync(pagination, sectionId, fParams, sorts, selectType, isArchive, price, filterIdByGroupType, section.PathOfSections, values);
                //_logger.Info("sql list: " + sql);
            }
            List<long> filterIds = new List<long>();
            if (filterIdByGroupType > 0)
                filterIds.Add(filterIdByGroupType);

            await LoadProductAttributesAsync(products, filterIds);

            return products;
        }

        /// <summary>
        /// Получает список товаров по указанным фильтрам
        /// </summary>
        /// <param name="isPublished"></param>
        /// <param name="hasPhoto"></param>
        /// <param name="sectionId"></param>
        /// <param name="isActive"></param>
        /// <param name="isArchive"></param>
        /// <returns></returns>
        public async Task<List<Product>> GetProductsAsync(bool? isPublished = false, bool? hasPhoto = false, long sectionId = 0, bool? isActive = false, bool? isArchive = false)
        {
            var query = QueryBuilderFactory.Create<Product>()
                .Include(x => x.ProductsToImages.Select(y => y.Image));
            if (sectionId > 0)
                query = query.Where(x => x.SectionId == sectionId);
            if (isPublished != null)
                query = query.Where(x => x.Published == isPublished);
            if (isArchive != null)
                query = query.Where(x => x.Archive == isArchive);
            if (hasPhoto != null)
                query = query.Where(x => (hasPhoto == true && x.MainImageId != ImagesLoaderSettings.NoPhoto) || (hasPhoto == false && x.MainImageId == ImagesLoaderSettings.NoPhoto));
            if (isActive != null)
                query = query.Where(x => x.Active == isActive);

            return await query.ToListAsync();
        }

        /// <summary>
        /// Получает список товаров по переданным идентификаторам
        /// </summary>
        /// <param name="productIds"></param>
        /// <returns></returns>
        public async Task<List<Product>> GetProductsByIdsAsync(List<long> productIds)
        {
            return await QueryBuilderFactory.Create<Product>()
                .ToListAsync(x => productIds.Contains(x.Id));
        }

        /// <summary>
        /// Получает товары для обзора
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        public async Task<List<Product>> GetProductsForPostAsync(long postId)
        {
            return await QueryBuilderFactory.Create<Product>()
                .ToListAsync(x => x.ProductsToPosts.Any(y => y.PostId == postId));
        }

        /// <summary>
        /// Поиск товаров по строке запроса для определенных разделов
        /// </summary>
        /// <param name="q"></param>
        /// <param name="pagination"></param>
        /// <returns></returns>
        public async Task<List<Product>> SearchProductsAsync(string q, Pagination pagination)
        {
            return await ProductsQueryMapper.SearchProductsAsync(q, pagination);
        }

        /// <summary>
        /// Поиск товаров по точному совпадению строки запроса
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<List<Product>> SearchProductsAsync(string query)
        {
            return await QueryBuilderFactory.Create<Product>()
                .Where(x => x.Title.Contains(query))
                .Take(30)
                .ToListAsync();
        }

        /// <summary>
        /// Получает товары для баннера
        /// </summary>
        /// <returns></returns>
        public async Task<List<Product>> GetProductsForBannerAsync()
        {
            return await QueryBuilderFactory.Create<Product>()
                .Where(x => x.ToBanner == true)
                .OrderBy(x => Guid.NewGuid())
                .Take(4)
                .ToListAsync();
        }

        /// <summary>
        /// Получает популярные товары
        /// </summary>
        /// <returns></returns>
        public async Task<List<Product>> GetHitProductsAsync()
        {
            return await QueryBuilderFactory.Create<Product>()
                .Where(x => x.ToRating == true)
                .OrderBy(x => Guid.NewGuid())
                .Take(6)
                .ToListAsync();
        }

        /// <summary>
        /// Получает новинки товаров
        /// </summary>
        /// <returns></returns>
        public async Task<List<Product>> GetNewProductsAsync()
        {
            return await QueryBuilderFactory.Create<Product>()
                .Where(x => x.New == true)
                .OrderBy(x => Guid.NewGuid())
                .Take(6)
                .ToListAsync();
        }

        /// <summary>
        /// Получает товары для админки по указанным фильтрам
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="pathOfSections"></param>
        /// <param name="filter"></param>
        /// <param name="sort"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<List<Product>> GetProductsForAdminAsync(Pagination pagination, long pathOfSections, List<AllowableFilterEnum> filter = null, bool needShowUnactiveProducts = false, string query = "", string sort = null)
        {
            if (!string.IsNullOrEmpty(query))
            {
                if (new Regex(@"^\d+$").IsMatch(query))
                {
                    long productId = Convert.ToInt64(query);
                    return await QueryBuilderFactory.Create<Product>()
                            .Include(x => x.Section)
                            .Where(x => x.Id == productId || x.Code == productId)
                            .OrderBy(x => x.Id)
                            .Skip(pagination.Offset)
                            .Take(pagination.PageItemsAmount)
                            .ToListAsync();
                }
            }
            if (filter == null)
                filter = new List<AllowableFilterEnum>();

            long rightPath = SectionsUtils.GetRightPath(pathOfSections);
            var queryObj = QueryBuilderFactory.Create<Product>()
                .Where(x => x.Section.PathOfSections >= pathOfSections && x.Section.PathOfSections < rightPath);
            if (!needShowUnactiveProducts)
                queryObj = queryObj.Where(x => x.Active);
            foreach (AllowableFilterEnum f in filter)
            {
                if (f == AllowableFilterEnum.ToBanner)
                    queryObj = queryObj.Where(x => x.ToBanner);
                else if (f == AllowableFilterEnum.ToRating)
                    queryObj = queryObj.Where(x => x.ToRating);
                else if (f == AllowableFilterEnum.New)
                    queryObj = queryObj.Where(x => x.New);
                else if (f == AllowableFilterEnum.Unpublished)
                    queryObj = queryObj.Where(x => !x.Published);
                else if (f == AllowableFilterEnum.Published)
                    queryObj = queryObj.Where(x => x.Published);
                else if (f == AllowableFilterEnum.WithPhoto)
                    queryObj = queryObj.Where(x => x.MainImageId != ImagesLoaderSettings.NoPhoto);
                else if (f == AllowableFilterEnum.WithoutPhoto)
                    queryObj = queryObj.Where(x => x.MainImageId == ImagesLoaderSettings.NoPhoto);
                else if (f == AllowableFilterEnum.FilledFeatures)
                    queryObj = queryObj.Where(x => x.FilledFeatures > 0);
                else if (f == AllowableFilterEnum.UnfilledFeatures)
                    queryObj = queryObj.Where(x => x.FilledFeatures == null || x.FilledFeatures == 0);
                else if (f == AllowableFilterEnum.Filled)
                    queryObj = queryObj.Where(x => x.Filled);
                else if (f == AllowableFilterEnum.Unfilled)
                    queryObj = queryObj.Where(x => !x.Filled);
                else if (f == AllowableFilterEnum.WithDescription)
                    queryObj = queryObj.Where(x => x.DescriptionId > 0 && !x.Description.Content.Trim().Equals(""));
                else if (f == AllowableFilterEnum.WithoutDescription)
                    queryObj = queryObj.Where(x => x.DescriptionId == null || x.Description.Content.Trim().Equals(""));
                else if (f == AllowableFilterEnum.WithCode)
                    queryObj = queryObj.Where(x => x.Code > 0);
                else if (f == AllowableFilterEnum.WithoutCode)
                    queryObj = queryObj.Where(x => x.Code == null);
                else if (f == AllowableFilterEnum.WithoutModel)
                    queryObj = queryObj.Where(x => !x.Attributes.SelectMany(y => y.Values).Any(z => z.Value.Filter.Title.Equals("Модель")));
                else if (f == AllowableFilterEnum.WithoutTitleIn)
                    queryObj = queryObj.Where(x => x.TitleIn.Trim().Equals(""));
                else if (f == AllowableFilterEnum.WithoutDescriptionIn)
                    queryObj = queryObj.Where(x => x.DescriptionId == null || x.Description.ContentIn.Trim().Equals(""));
                else if (f == AllowableFilterEnum.WithoutManual)
                {
                    int filtersAsInt = (int)FileFilterEnum.Filter_1;
                    queryObj = queryObj.Where(x => !x.ProductsToFiles.Any(y => (y.File.Filters.AsInt & filtersAsInt) > 0));
                }
            }
            if (!string.IsNullOrEmpty(query))
            {
                string queryWcd = query;
                if (query.IndexOf(",") != -1)
                    queryWcd = query.Replace(",", ".");
                else if (query.IndexOf(".") != -1)
                    queryWcd = query.Replace(".", ",");

                queryObj = queryObj
                    .Where(x => x.Title.Contains(query) || x.Title.Contains(queryWcd));
            }

            pagination.ItemsAmount = await queryObj.CountAsync();
            if (pagination.ItemsAmount == 0)
                return new List<Product>();
            pagination.Refresh();
            string order = null;
            if (sort != null && sort.Equals("Title"))
                order = "Title asc";
            else if (sort != null && sort.Equals("ViewsAmount"))
                order = "ViewsAmount desc";
            if (order != null)
                order += ",";
            else
                order += "";
            order += "Id";
            return await queryObj
                .OrderBy(order)
                .Skip(pagination.Offset)
                .Take(pagination.PageItemsAmount)
                .ToListAsync();
        }

        /// <summary>
        /// Получает товары по кодам 1c
        /// </summary>
        /// <param name="codes"></param>
        /// <returns></returns>
        public async Task<List<Product>> GetProductsByCodesAsync(List<long> codes)
        {
            return await QueryBuilderFactory.Create<Product>()
                .ToListAsync(x => codes.Contains((long)x.Code));
        }

        /// <summary>
        /// Получает заголовки товаров в определенном разделе
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<List<ProductTitle>> GetProductTitlesAsync(long sectionId)
        {
            return await QueryBuilderFactory.Create<Product>()
                .Where(x => x.SectionId == sectionId)
                .Select(x => new ProductTitle { Id = x.Id, Title = x.Title })
                .ToListAsync();
        }

        /// <summary>
        /// Получает товары ввиде xml
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<string> GetProductsAsXmlAsync(long sectionId)
        {
            // выберем все позиции
            List<Product> products1c = await GetProductsAsync(true, null, sectionId, true, false);
            // создадим xml документ
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<Товары></Товары>");
            foreach (Product p in products1c)
            {
                XmlNode productNode = doc.CreateElement("Товар");
                doc.DocumentElement.AppendChild(productNode);
                XmlNode codeNode = doc.CreateElement("Код");
                codeNode.InnerText = p.Code.ToString();
                productNode.AppendChild(codeNode);
                XmlNode titleNode = doc.CreateElement("Заголовок");
                titleNode.InnerText = p.Title;
                productNode.AppendChild(titleNode);
                XmlNode linkNode = doc.CreateElement("Ссылка");
                linkNode.InnerText = $"{UrlUtils.GetBaseUrl()}Products/Show/{p.Id}";
                productNode.AppendChild(linkNode);
                XmlNode publishedNode = doc.CreateElement("Публикация");
                publishedNode.InnerText = p.Published ? "Опубликовано" : "Не опубликовано";
                productNode.AppendChild(publishedNode);
                XmlNode imagesArchiveNode = doc.CreateElement("Архив_с_картинками");
                imagesArchiveNode.InnerText = $"{UrlUtils.GetBaseUrl()}Products/ProductImagesAsZipArchive/{p.Id}";
                productNode.AppendChild(imagesArchiveNode);
            }
            return doc.OuterXml;
        }

        /// <summary>
        /// Получает товары ввиде excel
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<byte[]> GetProductsInExcelAsync(long sectionId)
        {
            // выберем все позиции
            List<Product> products1c = await GetProductsAsync(true, null, sectionId, true, false);
            // создадим xml документ
            DataTable table = new DataTable();
            table.Columns.Add("Код", typeof(string));
            table.Columns.Add("Заголовок", typeof(string));
            table.Columns.Add("Price", typeof(string));
            table.Columns.Add("Краткие характеристики", typeof(string));

            products1c.Sort((x, y) => x.Title.CompareTo(y.Title));
            foreach (Product p in products1c)
            {
                table.Rows.Add(p.Code.ToString(), p.Title, p.Price.ToString("F"), (p.ShortFeatures ?? string.Empty).TrimStart('[').TrimEnd(']'));
            }
            return ExcelUtils.DataTableToExcel(table);
        }

        public async Task<byte[]> GetProductImagesAsZipAsync(long productId)
        {
            Product product = await GetProductByIdAsync(productId);
            using (var memoryStream = new MemoryStream())
            {
                using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {
                    foreach (var im in product.ProductsToImages)
                        archive.CreateEntryFromFile(St0SiteSettings.Root + "Images\\Products\\" + im.ImageId + ".jpg", im.ImageId + ".jpg");
                }

                byte[] ba = memoryStream.ToArray();
                
                return ba;
            }
        }

        /// <summary>
        /// Получает товар по иденитификатору
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public async Task<Product> GetProductByIdAsync(long productId)
        {
            return await ProductProvider.GetProductByIdAsync(productId);
        }

        /// <summary>
        /// Получает количество товаров по данному разделу
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<int> GetProductsAmountAsync(long sectionId)
        {
            return await QueryBuilderFactory.Create<Product>()
                .CountAsync(x => x.Section.Id == sectionId);
        }

        /// <summary>
        /// Получает номер страницы в которой находится данный товар
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public async Task<int> GetPageForProductAsync(Product product)
        {
            return await ProductsQueryMapper.GetPageForProductAsync(product.Id, product.SectionId);
        }

        /// <summary>
        /// Получает описания товаров по указанным идентификаторам
        /// </summary>
        /// <param name="descriptionIds"></param>
        /// <returns></returns>
        public async Task<List<Description>> GetProductDescriptionsAsync(List<long> descriptionIds)
        {
            return await QueryBuilderFactory.Create<Description>()
                .ToListAsync(x => descriptionIds.Contains(x.Id));
        }

        /// <summary>
        /// Получает и устанавливает атрибуты со значениями для указанных товаров
        /// </summary>
        /// <param name="productIds"></param>
        /// <param name="filterIds"></param>
        /// <returns></returns>
        public async Task LoadProductAttributesAsync(List<Product> products, List<long> filterIds = null)
        {
            List<long> productIds = products.Select(x => x.Id).ToList();
            var query = QueryBuilderFactory.Create<DmAttribute>()
                .Include(x => x.Filter)
                .Include(x => x.Values)
                .Include(x => x.Values.Select(y => y.Value))
                .Where(x => productIds.Contains(x.ProductId));
            if (filterIds != null && filterIds.Count > 0)
            {
                query = query.Where(x => filterIds.Contains(x.FilterId));
            }
            List<DmAttribute> attrs = await query
                .ToListAsync();

            foreach (Product p in products)
                foreach (DmAttribute a in attrs)
                    if (a.ProductId == p.Id)
                        p.Attributes.Add(a);
        }

        /// <summary>
        /// Получает картинки для указанных товаров
        /// </summary>
        /// <param name="productIds"></param>
        /// <returns></returns>
        public async Task<List<Image>> GetProductImagesAsync(List<long> productIds)
        {
            return await QueryBuilderFactory.Create<Image>()
                .ToListAsync(x => x.ProductsToImages.Any(y => productIds.Contains(y.ProductId)));
        }

        /// <summary>
        /// Удаляет картинки для указанного товара
        /// </summary>
        /// <param name="productId"></param>
        [Transaction]
        public async Task DeleteImagesAsync(long productId)
        {
            await QueryBuilderFactory.Create<ProductToImage>().DeleteAsync(new { ProductId = productId });
        }

        /// <summary>
        /// Добавляет картинки для указанного товара
        /// </summary>
        /// <param name="product"></param>
        [Transaction]
        public async Task AddImagesAsync(Product product)
        {
            for (int i = 0; i < product.ProductsToImages.Count; i++)
            {
                await QueryBuilderFactory.Create<Image>().UpdateAsync(new { Position = i, Id = product.ProductsToImages[i].ImageId });
                await QueryBuilderFactory.Create<ProductToImage>().InsertAsync(new ProductToImage { ProductId = product.Id, ImageId = product.ProductsToImages[i].ImageId });
            }
        }

        /// <summary>
        /// Обновляет товар
        /// </summary>
        /// <param name="product"></param>
        /// <param name="userId"></param>
        [Transaction]
        public async Task EditProductAsync(Product product, int userId = 0)
        {

            var oldProduct = await GetProductByIdAsync(product.Id);
            // удаляем старые характеристики
            await DeleteAttributesAsync(product.Id);
            // удаляем старые картинки
            await DeleteImagesAsync(product.Id);
            // добавляем новые характеристики
            await AddAttributesAsync(product);
            // добавляем новые картинки
            await AddImagesAsync(product);
            // обновляем описание товара
            await CreateOrUpdateDescriptionAsync(product);
            // обновляем товар
            product.Name = CommonUtils.ToLat(product.Title);
            if (product.MainImageId == 0)
            {
                product.MainImageId = ImagesLoaderSettings.NoPhoto;
            }
            product.BannerImageId = product.BannerImageId > 0 ? product.BannerImageId : product.MainImageId;
            if (product.BannerImageIdIn == 0)
                product.BannerImageIdIn = null;
            var image = await QueryBuilderFactory.Create<Image>().FirstOrDefaultAsync(x => x.Id == product.MainImageId);
            if (image.Width == 0 || image.Height == 0)
            {
                product.MainImageK = 0;
            }
            else
            {
                product.MainImageK = image.Width / image.Height;
            }
            //
            await SetFilledFeaturesAsync(product);
            //
            await QueryBuilderFactory.Create<Product>().UpdateAsync(product);
            // создадим снимок
            await CreateSnapshotForEditedProductAsync(product, oldProduct, userId);
        }

        /// <summary>
        /// Создает товар
        /// </summary>
        /// <param name="product"></param>
        /// <param name="userId"></param>
        [Transaction]
        public async Task CreateProductAsync(Product product, int userId = 0)
        {
            // создадим описание товара
            await CreateOrUpdateDescriptionAsync(product);
            // создадим товар
            product.Name = CommonUtils.ToLat(product.Title);
            if (product.MainImageId == 0)
            {
                product.MainImageId = ImagesLoaderSettings.NoPhoto;
            }
            product.BannerImageId = product.BannerImageId > 0 ? product.BannerImageId : product.MainImageId;
            if (product.BannerImageIdIn == 0)
                product.BannerImageIdIn = null;
            if (product.MainImageId != ImagesLoaderSettings.NoPhoto)
            {
                var image = await QueryBuilderFactory.Create<Image>().FirstOrDefaultAsync(x => x.Id == product.MainImageId);
                if (image == null || image.Width == 0 || image.Height == 0)
                    product.MainImageK = 1;
                else
                    product.MainImageK = image.Width / image.Height;
            }
            //
            await SetFilledFeaturesAsync(product);

            product.CreatedDate = DateTime.Now;
            await QueryBuilderFactory.Create<Product>().InsertAsync(product);
            // создадим характеристики товара
            await AddAttributesAsync(product);
            // создадим картинки
            await AddImagesAsync(product);
            // создадим снимок
            List<ProductStateEnum> productStates = new List<ProductStateEnum>();
            productStates.Add(ProductStateEnum.Created);
            if (product.MainImageId != ImagesLoaderSettings.NoPhoto)
                productStates.Add(ProductStateEnum.UploadedImages);
            if (product.FilledFeatures > 0)
                productStates.Add(ProductStateEnum.FilledFeatures);
            if (product.Description != null && !string.IsNullOrEmpty(product.Description.Content))
                productStates.Add(ProductStateEnum.FilledDescription);
            if (product.Filled)
                productStates.Add(ProductStateEnum.Filled);
            if (product.Published)
                productStates.Add(ProductStateEnum.Published);
            await ProductSnapshotEditor.CreateProductSnapshotAsync(product, productStates, userId);
        }

        /// <summary>
        /// Удаляет товар
        /// </summary>
        /// <param name="productId"></param>
        [Transaction]
        public async Task DeleteProductAsync(long productId)
        {
            // удаляем картинки
            await QueryBuilderFactory.Create<ProductToImage>().DeleteAsync(new { ProductId = productId });
            // получим DescriptionId для последующего удаления
            long? descriptionId = await QueryBuilderFactory.Create<Product>()
                .Where(x => x.Id == productId)
                .Select(x => x.DescriptionId)
                .FirstOrDefaultAsync();
            // удалим товар
            await QueryBuilderFactory.Create<Product>().DeleteAsync(new { Id = productId });
            // удалим описание
            if (descriptionId != null)
                await QueryBuilderFactory.Create<Description>().DeleteAsync(new { Id = descriptionId });
        }

        /// <summary>
        /// Обновляет товар. Только саму модель, не учитывая всякие вложенные модели
        /// </summary>
        /// <param name="product"></param>
        [Transaction]
        public async Task UpdateProductAsync(Product product)
        {
            await QueryBuilderFactory.Create<Product>().UpdateAsync(product);
        }

        /// <summary>
        /// Копирует товар
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Transaction]
        public async Task<Product> CopyProductAsync(long productId, int userId)
        {
            Product original = await GetProductByIdAsync(productId); ;
            Product clon = ProductMapper.Map<Product, Product>(original);
            await CreateProductAsync(clon, userId);
            return clon;
        }

        /// <summary>
        /// Копирует характеристики с указанного товара
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="fromProductId"></param>
        [Transaction]
        public async Task CopyAttributesAsync(long productId, long fromProductId)
        {
            Product fromProduct = await GetProductByIdAsync(fromProductId);
            if (fromProduct == null)
                return;
            var attributes = ProductMapper.Map<List<DmAttribute>, List<DmAttribute>>(fromProduct.Attributes);
            Product product = new Product
            {
                Id = productId,
                Attributes = attributes,
                CreatedDate = DateTime.Now
            };
            await DeleteAttributesAsync(productId);
            await AddAttributesAsync(product);
        }

        /// <summary>
        /// Копирует картинки с указанного товара
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="fromProductId"></param>
        /// <param name="product"></param>
        [Transaction]
        public async Task<Product> CopyImagesAsync(long productId, long fromProductId)
        {
            Product fromProduct = await GetProductByIdAsync(fromProductId);
            if (fromProduct == null)
                return null;

            var images = ProductMapper.Map<List<ProductToImage>, List<ProductToImage>>(fromProduct.ProductsToImages);
            Product product = new Product
            {
                Id = productId,
                ProductsToImages = images,
                MainImageId = fromProduct.MainImageId
            };
            await DeleteImagesAsync(productId);
            await AddImagesAsync(product);
            if (product.MainImageId != 0)
                await QueryBuilderFactory.Create<Product>().UpdateAsync(new { MainImageId = product.MainImageId, Id = product.Id });
            return product;
        }

        /// <summary>
        /// Удаляет все значения всех товаров
        /// </summary>
        [Transaction]
        public async Task DeleteAllProductValuesAsync()
        {
            await QueryBuilderFactory.Create<ProductValue>().DeleteAsync("1");
        }

        public async Task UpdateProductCountersAsync()
        {
            await ProductCountersQueryMapper.UpdateProductCountersAsync();
        }

        /// <summary>
        /// Удяляет все характеристики для указанного товара
        /// </summary>
        /// <param name="productId"></param>
        private async Task DeleteAttributesAsync(long productId)
        {
            await QueryBuilderFactory.Create<DmAttribute>().DeleteAsync(new { ProductId = productId });
        }

        /// <summary>
        /// Удяляет неправильно переданные характеристики
        /// </summary>
        /// <param name="product"></param>
        private void RemoveBadAttributes(Product product)
        {
            for (int i = 0; i < product.Attributes.Count(); i++)
            {
                DmAttribute a = product.Attributes[i];
                if (a.Values.Count == 0 || (a.Values.Count == 1 && a.Values[0].ValueId == null))
                {
                    product.Attributes.RemoveAt(i);
                    i--;
                }
            }
        }

        /// <summary>
        /// Добавляет характеристики 
        /// </summary>
        /// <param name="product"></param>
        private async Task AddAttributesAsync(Product product)
        {
            RemoveBadAttributes(product);
            for (int i = 0; i < product.Attributes.Count(); i++)
            {
                DmAttribute a = product.Attributes[i];
                a.ProductId = product.Id;
                await QueryBuilderFactory.Create<DmAttribute>().InsertAsync(a);
                foreach (ProductValue v in a.Values)
                {
                    v.AttributeId = a.Id;
                    await QueryBuilderFactory.Create<ProductValue>().InsertAsync(v);
                }
            }
        }

        /// <summary>
        /// Создает или обновляет описание для товара
        /// </summary>
        /// <param name="product"></param>
        private async Task CreateOrUpdateDescriptionAsync(Product product)
        {
            if (product.Description == null)
                return;
            if (product.Description.Id == 0)
            {
                if (product.Description.Content == null)
                {
                    product.Description = null;
                    product.DescriptionId = null;
                    return;
                }
                await QueryBuilderFactory.Create<Description>().InsertAsync(product.Description);
                product.DescriptionId = product.Description.Id;
            }
            else
            {
                if (product.Description.Content == null)
                    product.Description.Content = "";
                await QueryBuilderFactory.Create<Description>().UpdateAsync(product.Description);
            }
        }

        /// <summary>
        /// Устанавливает процент заполненных характеристик
        /// </summary>
        /// <param name="product"></param>
        private async Task SetFilledFeaturesAsync(Product product)
        {
            Section section = await SectionsProvider.GetSectionByIdAsync(product.SectionId);
            int filtersAmount = section.FiltersGroups.SelectMany(x => x.Filters).Select(x => x.Id).Count();
            if (filtersAmount == 0)
                return;
            int attributesAmount = 0;
            foreach (DmAttribute a in product.Attributes)
            {
                if (a.Values.Count == 0 || (a.Values.Count == 1 && a.Values[0].ValueId == null))
                    continue;
                attributesAmount++;
            }
            if (attributesAmount == 0)
                return;
            product.FilledFeatures = (int)Math.Ceiling((float)attributesAmount * 100 / filtersAmount);
        }

        /// <summary>
        /// Получает снимок для редактируемого товара
        /// </summary>
        /// <param name="product"></param>
        /// <param name="oldProduct"></param>
        /// <param name="userId"></param>
        private async Task CreateSnapshotForEditedProductAsync(Product product, Product oldProduct, int userId)
        {
            List<ProductStateEnum> productStates = new List<ProductStateEnum>();
            List<ProductValue> oldValues = oldProduct.Attributes.SelectMany(x => x.Values).ToList();
            oldValues.Sort((x, y) => x.Attribute.FilterId.CompareTo(y.Attribute.FilterId));
            List<ProductToImage> oldImages = oldProduct.ProductsToImages.ToList(); // делаем копию листа чтобы отсортироваться
            oldImages.Sort((x, y) => x.ImageId.CompareTo(y.ImageId));
            string oldImagesMd5Hash = CommonUtils.GetMd5Hash(string.Join(",", oldImages.Select(x => x.ImageId)));
            string oldValuesMd5Hash = CommonUtils.GetMd5Hash(string.Join(";", oldValues.Select(x => x.Attribute.FilterId + ":" + (x.ValueId ?? 0))));

            foreach (DmAttribute a in product.Attributes)
                foreach (ProductValue v in a.Values)
                    v.Attribute = a;

            List<ProductValue> values = product.Attributes.SelectMany(x => x.Values).ToList();
            values.Sort((x, y) => x.Attribute.FilterId.CompareTo(y.Attribute.FilterId));
            List<ProductToImage> images = product.ProductsToImages.ToList();// делаем копию листа чтобы отсортироваться
            images.Sort((x, y) => x.ImageId.CompareTo(y.ImageId));
            string imagesMd5Hash = CommonUtils.GetMd5Hash(string.Join(",", images.Select(x => x.ImageId)));
            string valuesMd5Hash = CommonUtils.GetMd5Hash(string.Join(";", values.Select(x => x.Attribute.FilterId + ":" + "," + (x.ValueId ?? 0))));

            productStates.Add(ProductStateEnum.Modified);

            if (oldProduct.MainImageId == ImagesLoaderSettings.NoPhoto && product.MainImageId != ImagesLoaderSettings.NoPhoto)
                productStates.Add(ProductStateEnum.UploadedImages);
            else if (oldProduct.MainImageId != ImagesLoaderSettings.NoPhoto && product.MainImageId == ImagesLoaderSettings.NoPhoto)
                productStates.Add(ProductStateEnum.UnuploadedImages);
            else if (!oldImagesMd5Hash.Equals(imagesMd5Hash))
                productStates.Add(ProductStateEnum.ModifiedImages);

            if (oldProduct.FilledFeatures == 0 && product.FilledFeatures > 0)
                productStates.Add(ProductStateEnum.FilledFeatures);
            else if (oldProduct.FilledFeatures > 0 && product.FilledFeatures == 0)
                productStates.Add(ProductStateEnum.UnfilledFeatures);
            else if (!oldValuesMd5Hash.Equals(valuesMd5Hash))
                productStates.Add(ProductStateEnum.ModifiedFeatures);

            if ((oldProduct.Description == null || string.IsNullOrWhiteSpace(oldProduct.Description.Content))
                && (product.Description != null && !string.IsNullOrWhiteSpace(product.Description.Content)))
                productStates.Add(ProductStateEnum.FilledDescription);
            else if ((oldProduct.Description != null && !string.IsNullOrWhiteSpace(oldProduct.Description.Content))
                && (product.Description == null || string.IsNullOrWhiteSpace(product.Description.Content)))
                productStates.Add(ProductStateEnum.UnfilledDescription);
            else if (oldProduct.Description != null && !string.IsNullOrWhiteSpace(oldProduct.Description.Content)
                && product.Description != null && !string.IsNullOrWhiteSpace(product.Description.Content)
                && !CommonUtils.GetMd5Hash(oldProduct.Description.Content).Equals(CommonUtils.GetMd5Hash(product.Description.Content)))
                productStates.Add(ProductStateEnum.ModifiedDescription);

            if (!oldProduct.Filled && product.Filled)
                productStates.Add(ProductStateEnum.Filled);
            else if (oldProduct.Filled && !product.Filled)
                productStates.Add(ProductStateEnum.Unfilled);

            if (!oldProduct.Published && product.Published)
                productStates.Add(ProductStateEnum.Published);
            else if (oldProduct.Published && !product.Published)
                productStates.Add(ProductStateEnum.Unpublished); ;

            if (oldProduct.Price != product.Price)
                productStates.Add(ProductStateEnum.ModifiedPrice);

            if (oldProduct.Code != product.Code)
                productStates.Add(ProductStateEnum.ModifiedCode);

            await ProductSnapshotEditor.CreateProductSnapshotAsync(product, productStates, userId);
        }
    }
}