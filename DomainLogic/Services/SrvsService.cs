﻿using AutoMapper;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using DomainLogic.Models.Settings;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Models.Enumerations;

namespace DomainLogic.Services
{
    public class SrvsService: BaseService, ISrvsService
    {
        [Inject]
        public St0SiteSettings St0SiteSettings { get; set; }

        [Inject]
        public ExcelUtils ExcelUtils { get; set; }

        /// <summary>
        /// Получает список всех сервисов
        /// </summary>
        /// <returns></returns>
        public async Task<List<Service>> GetServicesAsync()
        {
            return await QueryBuilderFactory.Create<Service>()
                .ToListAsync();
        }

        /// <summary>
        /// Получает список сервисов с указанным городом и типом
        /// </summary>
        /// <param name="city"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public async Task<List<Service>> GetServicesAsync(string city, ServiceTypeEnum type)
        {
            List<ServiceTypeEnum> types = new List<ServiceTypeEnum>();
            types.Add(type);
            if (type.Equals(ServiceTypeEnum.Others))
            {
                types.Add(ServiceTypeEnum.Service2);
                types.Add(ServiceTypeEnum.Service3);
            }

            return await QueryBuilderFactory.Create<Service>()
                .ToListAsync(x => x.City == city && types.Contains(x.Type));
        }

        /// <summary>
        /// Получает список городов
        /// </summary>
        /// <param name="country"></param>
        /// <returns></returns>
        public async Task<List<string>> GetCitiesForSelectListAsync(string country = null)
        {
            return await QueryBuilderFactory.Create<Service>()
                .Where(x => country == null || x.Country.Equals(country))
                .OrderBy(x => x.City)
                .Select(x => x.City + (!x.Country.Equals("РФ") ? "<!>(" + x.Country + ")" : ""))
                .Distinct().ToListAsync();
        }

        /// <summary>
        /// Получает информацию о сервисе по идентификатору
        /// </summary>
        /// <param name="serviceId"></param>
        /// <returns></returns>
        public async Task<Service> GetServiceAsync(long serviceId)
        {
            return await QueryBuilderFactory.Create<Service>()
                .FirstOrDefaultAsync(x => x.Id == serviceId);
        }

        public async Task<string> GetSrvsListAsXmlAsync()
        {
            // выберем все позиции
            List<Service> services = await GetServicesAsync();
            // создадим xml документ
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<СервисныеЦентры></СервисныеЦентры>");
            foreach (Service s in services)
            {
                if (string.IsNullOrWhiteSpace(s.City) || string.IsNullOrWhiteSpace(s.Address))
                    continue;

                XmlNode scNode = doc.CreateElement("СЦ");
                doc.DocumentElement.AppendChild(scNode);
                XmlNode ascNode = doc.CreateElement("АСЦ");
                ascNode.InnerText = s.Type.GetTitle();
                scNode.AppendChild(ascNode);
                XmlNode cityNode = doc.CreateElement("Город");
                cityNode.InnerText = s.City;
                scNode.AppendChild(cityNode);
                XmlNode addressNode = doc.CreateElement("Адрес");
                addressNode.InnerText = s.Address;
                scNode.AppendChild(addressNode);
            }
            return doc.OuterXml;
        }

        public async Task ServiceFillTableAsync(ServiceTypeEnum type)
        {
            List<Service> services = new List<Service>();
            //
            XmlDocument doc = new XmlDocument();
            doc.Load(St0SiteSettings.Root + "Files/" + type.ToString() + ".xml");
            XmlNamespaceManager ss = new XmlNamespaceManager(doc.NameTable);
            ss.AddNamespace("ss", "urn:schemas-microsoft-com:office:spreadsheet");
            XmlNodeList rows = doc.SelectNodes("//ss:Row", ss);
            for (int i = 1; i < rows.Count; i++)
            {
                XmlNode row = rows[i];
                XmlNode cell = row.FirstChild;
                string city = cell.FirstChild.InnerText;
                cell = cell.NextSibling;
                cell = cell.NextSibling;
                string address = cell.FirstChild != null ? cell.FirstChild.InnerText.Replace(",,", ",") : "";
                address = Regex.Replace(address, @"^,|,$", "");
                cell = cell.NextSibling;
                string phones = cell.FirstChild != null ? cell.FirstChild.InnerText : "";
                services.Add(new Service
                {
                    Country = "РФ",
                    City = city.Trim(),
                    Address = address.Trim(),
                    Phones = phones.Trim(),
                    Type = type
                });
            }
            await DeleteServicesAsync(ServiceTypeEnum.Service1);
            //
            foreach (Service s in services)
                await CreateServiceAsync(s);
        }

        /// <summary>
        /// Обновляет сервис
        /// </summary>
        /// <param name="service"></param>
        [Transaction]
        public async Task EditServiceAsync(Service service)
        {
            if (service.Country.ToLower().Equals("россия") || service.Country.ToLower().Equals("российская федерация"))
                service.Country = "РФ";

            await QueryBuilderFactory.Create<Service>().UpdateAsync(service);
        }

        /// <summary>
        /// Создает сервис
        /// </summary>
        /// <param name="service"></param>
        [Transaction]
        public async Task CreateServiceAsync(Service service)
        {
            if (service.Country.ToLower().Equals("россия") || service.Country.ToLower().Equals("российская федерация"))
                service.Country = "РФ";

            await QueryBuilderFactory.Create<Service>().InsertAsync(service);
        }

        /// <summary>
        /// Создает сервисы
        /// </summary>
        /// <param name="services"></param>
        [Transaction]
        public async Task CreateServicesAsync(List<Service> services)
        {
            if (services == null || services.Count == 0)
                return;
            int[] types = services.Select(x => (int)x.Type).Distinct().ToArray();
            // удалим старые СЦ
            await QueryBuilderFactory.Create<Service>().DeleteAsync($"Type in ({string.Join(", ", types)})");
            // добавим новые сервисы
            foreach (Service service in services)
                await CreateServiceAsync(service);
        }

        public async Task CreateServicesFromFileAsync(string fileName, ServiceTypeEnum type)
        {
            List<Service> services = ParseServices(fileName, type);
            await CreateServicesAsync(services);
        }

        /// <summary>
        /// Создает или обновляет сервисы.
        /// </summary>
        /// <param name="services"></param>
        public async Task CreateOrUpdateServicesAsync(List<Service> services)
        {
            if (services == null || services.Count == 0)
                return;
            // находим существующие сервисы
            List<string> serviceGuids = services.Select(x => x.Guid).ToList();
            List<Service> existingServices = await QueryBuilderFactory.Create<Service>()
                .ToListAsync(x => serviceGuids.Contains(x.Guid));
            // создаем или обновляем сервисы
            // взависимости от того будет найден старый сервис или нет
            foreach (Service s in services)
            {
                // найдем старый сервис
                Service exSrv = null;
                foreach (Service es in existingServices)
                    if (es.Guid.Equals(s.Guid))
                    {
                        exSrv = es;
                        break;
                    }

                // если сервис не удалось найти, то создадим сервис
                if (exSrv == null)
                {
                    await QueryBuilderFactory.Create<Service>().InsertAsync(s);
                    continue;
                }
                // иначе обновим существующий сервис

                // переносим текущие значения сервиса в существующий сервис
                long serviceId = exSrv.Id;
                Mapper.Map(s, exSrv);
                exSrv.Id = serviceId;
                // обновим сервис
                await QueryBuilderFactory.Create<Service>().UpdateAsync(exSrv);
            }
        }

        /// <summary>
        /// Удаляет сервис
        /// </summary>
        /// <param name="service"></param>
        [Transaction]
        public async Task DeleteServiceAsync(Service service)
        {
            await QueryBuilderFactory.Create<Service>().DeleteAsync(service);
        }

        /// <summary>
        /// Удаляет сервисы
        /// </summary>
        /// <param name="type"></param>
        [Transaction]
        public async Task DeleteServicesAsync(ServiceTypeEnum type)
        {
            await QueryBuilderFactory.Create<Service>().DeleteAsync(new { Type = (int)type });
        }

        /// <summary>
        /// Удаляет сервисы
        /// </summary>
        /// <param name="serviceGuids"></param>
        public async Task DeleteServicesAsync(List<string> serviceGuids)
        {
            if (serviceGuids == null || serviceGuids.Count == 0)
                return;
            await QueryBuilderFactory.Create<Service>().DeleteAsync($"Guid in ('{string.Join("','", serviceGuids)}')");
        }

        private List<Service> ParseServices(string excelFileName, ServiceTypeEnum type)
        {
            List<Service> services = new List<Service>();
            DataTable dt = ExcelUtils.ExcelToDataTable(St0SiteSettings.Root + "Files/" + excelFileName);
            for (var i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];

                services.Add(new Service
                {
                    Country = dr[0].ToString().Trim(),
                    City = dr[1].ToString().Trim(),
                    Address = dr[2].ToString().Trim(),
                    Phones = dr[3].ToString().Trim(),
                    WorkingHours = dr[4].ToString().Trim(),
                    Type = type
                });
            }
            return services;
        }
    }
}
