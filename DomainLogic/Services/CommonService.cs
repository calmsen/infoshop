﻿using DomainLogic.Infrastructure;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using DomainLogic.Models.Settings;
using DomainLogic.Services.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DomainLogic.Services
{
    public class CommonService: BaseService, ICommonService
    {
        [Inject]
        public EmailSender EmailSender { get; set; }

        [Inject]
        public CommonUtils CommonUtils { get; set; }

        /// <summary>
        /// Сервис для работы с пользователями
        /// </summary>
        [Inject]
        public IUsersService UsersService { get; set; }

        [Inject]
        public IOptionsService OptionsService { get; set; }

        [Inject]
        public MailingSettings MailingSettings { get; set; }

        public async Task<string> GetCityByIpAsync(string ip)
        {
            byte[] responseBytes;
            WebRequest request = WebRequest.Create("http://ipgeobase.ru:7020/geo?ip=" + "212.19.16.134");
            using (WebResponse response = await request.GetResponseAsync())
            using (Stream rs = response.GetResponseStream())
            using (MemoryStream ms = new MemoryStream())
            {
                byte[] buffer = new byte[2048];
                int count;
                while ((count = rs.Read(buffer, 0, buffer.Length)) > 0)
                    ms.Write(buffer, 0, count);
                responseBytes = ms.ToArray();

            }
            responseBytes = Encoding.Convert(Encoding.GetEncoding("windows-1251"), UTF8Encoding.UTF8, responseBytes);
            string responseXml = Encoding.UTF8.GetString(responseBytes);
            var doc = new XmlDocument();
            doc.Load(new StringReader(responseXml));
            string city = doc.SelectSingleNode("//city").InnerText;
            return city;
        }

        public async Task SendMailAsync(string subject, string body, bool doTest)
        {
            if (doTest)
            {
                string hash = CommonUtils.GetMd5Hash(MailingSettings.From + MailingSettings.Md5HahSolt);
                body = ProcessUnsubscribeLink(body, MailingSettings.From, hash);
                EmailSender.SendMessage(null, MailingSettings.From, subject, body);
                return;
            }
            int skip = 0;
            int take = 100;
            int count = await UsersService.GetNumberOfUsersAsync();
            int delay = 100;

            while (skip < count)
            {
                System.Threading.Thread.Sleep(delay);

                List<User> users = await UsersService.GetUsersAsync(skip, take);
                foreach (User user in users)
                {
                    try
                    {
                        string hash = CommonUtils.GetMd5Hash(user.Email + MailingSettings.Md5HahSolt);
                        body = ProcessUnsubscribeLink(body, user.Email, hash);
                        EmailSender.SendMessage(null, user.Email, subject, body);
                    }
                    catch (Exception)
                    {

                    }
                }
                skip += take;
            }
        }

        private string ProcessUnsubscribeLink(string body, string email, string hash)
        {
            return body.Replace("#unsubscribe", string.Format("User/UnsubscribeByHash?email={0}&hash={1}", email, hash));
        }
    }
}
