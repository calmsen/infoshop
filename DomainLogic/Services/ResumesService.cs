﻿using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System.Threading.Tasks;
using DomainLogic.Services.Shared;
using DomainLogic.Models.Settings;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Models.Messages;

namespace DomainLogic.Services
{
    public class ResumesService: BaseService, IResumesService
    {
        [Inject]
        public EmailSender EmailSender { get; set; }
        [Inject]
        public IUrlUtils UrlUtils { get; set; }
        [Inject]
        public IRenderViewsUtils RenderViewsUtils { get; set; }

        [Inject]
        public ResumeMessagesSettings ResumeMessages { get; set; }
        [Inject]
        public St0SiteSettings St0Site { get; set; }

        [Transaction]
        public async Task CreateResumeAsync(Resume resume)
        {
            await QueryBuilderFactory.Create<Resume>().InsertAsync(resume);
            SendMessageAfterCreateResume(resume);
        }

        private void SendMessageAfterCreateResume(Resume resume)
        {
            string subject = "Резюме - " + resume.Title;
            string body = RenderViewsUtils.RenderPartialToString("~/Views/_Messages/_CreateResumeMessage.cshtml", new CreateResumeMessage
            {
                Resume = resume,
                BaseUrl = UrlUtils.GetBaseUrl(),
                St0SiteHttpHost = St0Site.HttpHost
            });
            EmailSender.SendMessage(null, ResumeMessages.To, subject, body);
        }
    }
}