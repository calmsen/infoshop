﻿using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Paging;

namespace DomainLogic.Services
{
    public class PostsService : BaseService, IPostsService
    {
        /// <summary>
        /// Получает список обзоров с указанием количества элементов на странице и фильтра по активности обзора
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="isActivated"></param>
        /// <returns></returns>
        public async Task<List<Post>> GetPostsAsync(Pagination pagination, bool? isActivated = null)
        {
            // построим запрос и узнаем количество обзоров
            pagination.ItemsAmount = await QueryBuilderFactory.Create<Post>()
                .CountAsync(x => isActivated == null || x.Activate == isActivated);
            if (pagination.ItemsAmount == 0)
                return new List<Post>();
            pagination.Refresh();
            // получим список обзоров
            return await QueryBuilderFactory.Create<Post>()
                .Where(x => isActivated == null || x.Activate == isActivated)
                .OrderByDescending(p => p.Id)
                .Skip(pagination.Offset)
                .Take(pagination.PageItemsAmount)
                .ToListAsync();
        }

        /// <summary>
        /// Получает обзор по идентификатору
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        public async Task<Post> GetPostByIdAsync(long postId)
        {
            return await QueryBuilderFactory.Create<Post>()
                .Include(x => x.Description)
                .Include(x => x.ProductsToPosts.Select(y => y.Product))
                .FirstOrDefaultAsync(x => x.Id == postId);
        }

        /// <summary>
        /// Привязывает товары к обзору
        /// </summary>
        /// <param name="post"></param>
        private async Task BindProductsAsync(Post post)
        {
            for (int i = 0; i < post.ProductsToPosts.Count; i++)
            {
                await QueryBuilderFactory.Create<ProductToPost>().InsertAsync(new ProductToPost { ProductId = post.ProductsToPosts[i].ProductId, PostId = post.Id });
            }
        }

        /// <summary>
        /// Отвязывает товары от обзора
        /// </summary>
        /// <param name="post"></param>
        private async Task UnbindProductsAsync(Post post)
        {
            await QueryBuilderFactory.Create<ProductToPost>().DeleteAsync(new { PostId = post.Id });
        }

        /// <summary>
        /// Обновляет обзор
        /// </summary>
        /// <param name="post"></param>
        [Transaction]
        public async Task EditPostAsync(Post post)
        {
            // отвязываем старые товары от обзора
            await UnbindProductsAsync(post);
            // привязываем новые товары к обзору
            await BindProductsAsync(post);
            // обновляем или создаем описание обзора
            if (post.Description.Id > 0)
                await QueryBuilderFactory.Create<PostDescription>().UpdateAsync(post.Description);
            else if (!string.IsNullOrWhiteSpace(post.Description.Content))
            {
                await QueryBuilderFactory.Create<PostDescription>().InsertAsync(post.Description);
                post.DescriptionId = post.Description.Id;
            }
            else
                post.DescriptionId = null;
            // обновляем обзор 
            await QueryBuilderFactory.Create<Post>().UpdateAsync(post);
        }

        /// <summary>
        /// Создает обзор
        /// </summary>
        /// <param name="post"></param>
        [Transaction]
        public async Task CreatePostAsync(Post post)
        {
            // создаем описание обзора
            if (!string.IsNullOrWhiteSpace(post.Description.Content))
            {
                await QueryBuilderFactory.Create<PostDescription>().InsertAsync(post.Description);
                post.DescriptionId = post.Description.Id;
            }
            else
                post.DescriptionId = null;
            // создаем обзор
            post.CreatedDate = DateTime.Now;
            await QueryBuilderFactory.Create<Post>().InsertAsync(post);
            // привязываем товары к обзору
            await BindProductsAsync(post);
        }

        /// <summary>
        /// Удаляет обзор
        /// </summary>
        /// <param name="post"></param>
        [Transaction]
        public async Task DeletePostAsync(Post post)
        {
            await QueryBuilderFactory.Create<Post>().DeleteAsync(post);
        }
    }
}
