﻿using DomainLogic.Interfaces.QueryMappers;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainLogic.Models.Settings;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Services.Shared.Providers;

namespace DomainLogic.Services
{
    public class SectionsService : BaseService, ISectionsService
    {
        [Inject]
        public SectionsProvider SectionsProvider { get; set; }

        [Inject]
        public ISectionsQueryMapper SectionsQueryMapper { get; set; }

        [Inject]
        public CommonUtils CommonUtils { get; set; }

        [Inject]
        public SectionsSettings SectionsSettings { get; set; }

        public void MapSectionChildsFromList(List<Section> sections, Section section)
        {
            section.Childs = SectionsToTreeFromList(sections, section.Id);
        }

        public Section GetSectionByIdFromList(List<Section> sections, long sectionId)
        {
            return sections.Where(s => s.Id == sectionId).FirstOrDefault<Section>();
        }

        public List<Section> SectionsToTreeFromList(List<Section> sections, long parentId, bool isGetHidden = true)
        {
            List<Section> sectionsWC = new List<Section>();
            foreach (var s in sections)
            {
                if (s.ParentId == parentId)
                {
                    if (!isGetHidden && s.Hidden)
                        continue;
                    MapSectionChildsFromList(sections, s);
                    sectionsWC.Add(s);
                }
            }
            sectionsWC.Sort((x, y) => x.Position.CompareTo(y.Position));
            return sectionsWC;
        }
        
        public List<List<Section>> GetSectionsAsPathSiblingsFromList(List<Section> sections, long sectionId, bool isGetChilds)
        {
            List<List<Section>> sectionsList = new List<List<Section>>();
            sectionsList.Add(GetSectionChildsFromList(sections, sectionId));
            if (sectionId == 0)
            {                
                return sectionsList;
            }


            long currentSectionId = sectionId;
            for (int i = 0; i < 10; i++)
            {
                List<Section> sectionSiblings = GetSectionSiblingsFromList(sections, currentSectionId);
                sectionsList.Add(sectionSiblings);
                if (sectionSiblings[0].ParentId == 0)
                    break;
                currentSectionId = GetSectionByIdFromList(sections, sectionSiblings[0].ParentId).Id;
            }
            sectionsList.Reverse();
            List<Section> sectionChilds = GetSectionChildsFromList(sections, sectionId);
            if (isGetChilds && sectionChilds.Count > 0)
            {
                sectionsList.Add(sectionChilds);
            }
            return sectionsList;
        }

        public List<Section> GetSectionChildsFromList(List<Section> sections, long sectionId)
        {
            return sections.Where(x => x.ParentId == sectionId).ToList();
        }

        public List<Section> GetSectionByQueryFromList(List<Section> sections, string query = "")
        {
            return sections
                .Where(x => query.Equals("") || x.Title.Contains(query))
                .ToList();
        }

        public List<Section> GetDescendenSectionsFromList(List<Section> sections, Section section, bool isGetHidden = true)
        {
            List<Section> descendenSections = new List<Section>();
            if (section != null && (!section.Hidden  || (section.Hidden && isGetHidden)))
            {
                descendenSections.Add(section);
            }            

            List<Section> sectionChilds = null;
            if (section == null)
                sectionChilds = GetSectionChildsFromList(sections, 0);
            else
                sectionChilds = GetSectionChildsFromList(sections, section.Id);

            if (sectionChilds.Count == 0)
                return descendenSections;
                        
            foreach (Section s in sectionChilds)
            {
                if (!isGetHidden && s.Hidden)
                    continue;
                descendenSections.AddRange(GetDescendenSectionsFromList(sections, s, isGetHidden));
            }

            return descendenSections;
        }

        public List<long> GetSectionChildsIdsFromList(List<Section> sections, Section section, bool isGetHidden)
        {

            if (section != null && !isGetHidden && section.Hidden)
                return new List<long>();

            List<long> sectionsIds = new List<long>();

            List<Section> sectionChilds = null;
            if (section == null)
                sectionChilds = GetSectionChildsFromList(sections, 0);
            else
                sectionChilds = GetSectionChildsFromList(sections, section.Id);

            foreach (Section s in sectionChilds)
            {
                if (!isGetHidden && s.Hidden)
                    continue;
                sectionsIds.Add(s.Id);
            }
            return sectionsIds;
        }

        public List<Section> GetPathOfSectionsFromList(List<Section> sections, Section section)
        {
            List<Section> pathOfSections = new List<Section>();
            pathOfSections.Add(section);
            while (true)
            {
                if (section == null || section.ParentId == 0)
                    break;
                section = GetSectionByIdFromList(sections, section.ParentId);                
                pathOfSections.Add(section);
            }
            pathOfSections.Reverse();
            return pathOfSections;
        }

        public string GetPathOfSectionNamesFromList(List<Section> sections, Section section)
        {
            List<Section> pathOfSections = GetPathOfSectionsFromList(sections, section);
            List<string> pathOfSectionNames = pathOfSections.Select(x => x.Title).ToList();
            return string.Join("\\", pathOfSectionNames);
        }

        public string GetPathOfSectionIdsFromList(List<Section> sections, Section section)
        {
            List<Section> pathOfSections = GetPathOfSectionsFromList(sections, section);
            List<string> pathOfSectionIds = pathOfSections.Select(x => x.Id.ToString()).ToList();
            return string.Join("\\", pathOfSectionIds);
        }
        
        /// <summary>
        /// Получает все разделы
        /// </summary>
        /// <returns></returns>
        public async Task<List<Section>> GetSectionsAsync()
        {
            return await QueryBuilderFactory.Create<Section>()
                .ToListAsync();
        }

        /// <summary>
        /// Получает разделы по списку идентификаторов
        /// </summary>
        /// <param name="sectionIds"></param>
        /// <returns></returns>
        public async Task<List<Section>> GetSectionsAsync(List<long> sectionIds)
        {
            return await QueryBuilderFactory.Create<Section>()
                .ToListAsync(x => sectionIds.Contains(x.Id));
        }

        /// <summary>
        /// Получает дочерние разделы
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<List<Section>> GetSectionChildsAsync(long sectionId)
        {
            var sections = new List<Section>();
            foreach (var s in (await GetSectionsAsync()))
                if (s.ParentId == sectionId)
                    sections.Add(s);
            return sections;
        }

        /// <summary>
        /// Получает коллекцию из потомков и себя по пути разделов
        /// </summary>
        /// <param name="pathOfSections"></param>
        /// <returns></returns>
        public async Task<List<Section>> GetDescendenOrSelfSectionsAsync(long pathOfSections)
        {
            return await SectionsProvider.GetDescendenOrSelfSectionsAsync(pathOfSections);
        }

        /// <summary>
        /// Получает соседние разделы
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<List<Section>> GetSectionSiblingsAsync(long sectionId)
        {
            long parentId = (await GetSectionByIdAsync(sectionId)).ParentId;
            var sections = new List<Section>();
            foreach (var s in (await GetSectionsAsync()))
                if (s.ParentId == parentId)
                    sections.Add(s);
            return sections;
        }

        /// <summary>
        /// Получает разделы в которых есть товары
        /// </summary>
        /// <returns></returns>
        public async Task<List<Section>> GetSectionsWithProductsAsync()
        {
            return await QueryBuilderFactory.Create<Section>()
                .ToListAsync(x => !x.Hidden && x.Products.Count > 0);
        }

        /// <summary>
        /// Получает список разделов по списку идентификаторов
        /// </summary>
        /// <param name="sectionIds"></param>
        /// <returns></returns>
        public async Task<List<Section>> GetSectionsByIdsAsync(List<long> sectionIds)
        {
            if (sectionIds == null || sectionIds.Count == 0)
                return new List<Section>();
            return await QueryBuilderFactory.Create<Section>()
                .ToListAsync(x => sectionIds.Contains(x.Id));
        }

        /// <summary>
        /// Получает раздел по наименованию
        /// </summary>
        /// <param name="sectionName"></param>
        /// <param name="needIncludeBanner"></param>
        /// <returns></returns>
        public async Task<Section> GetSectionByNameAsync(string sectionName, bool needIncludeBanner = false)
        {
            if (sectionName == null)
                return null;

            var query = QueryBuilderFactory.Create<Section>();

            if (needIncludeBanner)
                query = query.Include(x => x.SectionsToImages.Select(y => y.Image));

            return await query
                .FirstOrDefaultAsync(s => s.Name == sectionName);
        }

        /// <summary>
        /// Получает раздел по идентификатору
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="needIncludeBanner"></param>
        /// <returns></returns>
        public async Task<Section> GetSectionByIdAsync(long sectionId, bool needIncludeBanner = false)
        {
            return await SectionsProvider.GetSectionByIdAsync(sectionId, needIncludeBanner);
        }

        /// <summary>
        /// Проверяет имеет ли раздел дочерние разделы
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<bool> HasChildsAsync(long sectionId)
        {
            return await QueryBuilderFactory.Create<Section>()
                .Where(x => x.ParentId == sectionId).CountAsync() > 0;
        }

        /// <summary>
        /// Получает заголовки разделов для указанных фильтров
        /// </summary>
        /// <param name="filterIds"></param>
        /// <returns></returns>
        public async Task<List<SectionTitle>> GetSectionTitlesAsync(List<long> filterIds)
        {
            return await SectionsQueryMapper.GetSectionTitlesAsync(filterIds);
        }

        /// <summary>
        /// Обновляет раздел
        /// </summary>
        /// <param name="section"></param>
        [Transaction]
        public async Task EditSectionAsync(Section section)
        {
            // выберем старый раздел 
            Section oldDbSection = await QueryBuilderFactory.Create<Section>().FirstOrDefaultAsync(x => x.Id == section.Id);
            // отвязываем старые баннеры
            await QueryBuilderFactory.Create<SectionToImage>().DeleteAsync(new { SectionId = section.Id });
            // привязываем новые баннеры
            await AddBannersAsync(section);
            // обновляем раздел
            section.Name = CommonUtils.ToLat(section.Title);
            await QueryBuilderFactory.Create<Section>().UpdateAsync(section);
            // обновим пути из разделов
            if (oldDbSection.ParentId != section.ParentId)
            {
                await SaveSectionsPositionsAsync(oldDbSection.ParentId);
                await SaveSectionsPositionsAsync(section.ParentId);
            }
        }

        /// <summary>
        /// Создает раздел
        /// </summary>
        /// <param name="section"></param>
        [Transaction]
        public async Task CreateSectionAsync(Section section)
        {
            long[] pathResult = await SectionsQueryMapper.GetPathOfSectionsByParentIdAsync(section.ParentId);
            section.PathOfSections = pathResult[0];
            section.Position = (int)pathResult[1];
            section.Name = CommonUtils.ToLat(section.Title);
            // создаем раздел            
            await QueryBuilderFactory.Create<Section>().InsertAsync(section);
            // привязываем баннеры
            await AddBannersAsync(section);
        }

        /// <summary>
        /// Удаляет раздел
        /// </summary>
        /// <param name="sectionId"></param>
        [Transaction]
        public async Task DeleteSectionAsync(long sectionId)
        {
            Section section = await GetSectionByIdAsync(sectionId);
            // отвязываем все товары
            await UnbindProductsAsync(sectionId);
            // обновляем родителя у своих детей
            await QueryBuilderFactory.Create<Section>().UpdateAsync(new { ParentId = section.ParentId }, new { ParentId = sectionId });
            // удаляем раздел
            await QueryBuilderFactory.Create<Section>().DeleteAsync(section);
            // обновим пути из разделов
            await SaveSectionsPositionsAsync(section.ParentId);

        }

        /// <summary>
        /// Сохраняет позиции разделов
        /// </summary>
        /// <param name="sections"></param>
        [Transaction]
        public async Task SaveSectionsPositionsAsync(List<Section> sections)
        {
            foreach (Section s in sections)
                await QueryBuilderFactory.Create<Section>().UpdateAsync(new { Position = s.Position, Id = s.Id });
            List<long> sectionsIds = new List<long>();
            foreach (long id in sections.Select(x => x.Id).ToList())
                sectionsIds.AddRange(await GetDescendantOrSelfSectionsAsync(id));

            await SavePathOfSectionsAsync(sectionsIds);
        }

        /// <summary>
        /// Отвязывает все товары от раздела
        /// </summary>
        /// <param name="sectionId"></param>
        [Transaction]
        public async Task UnbindProductsAsync(long sectionId)
        {
            if (SectionsSettings.OtherSection.Id == 0)
                throw new ArgumentException("Определите раздел \"Другие\"");

            await QueryBuilderFactory.Create<Product>().UpdateAsync(new { SectionId = SectionsSettings.OtherSection.Id }, new { SectionId = sectionId });
        }

        private List<Section> GetSectionSiblingsFromList(List<Section> sections, long sectionId)
        {
            Section section = GetSectionByIdFromList(sections, sectionId);
            return sections.Where(x => x.ParentId == section.ParentId).ToList();
        }

        /// <summary>
        /// Сохраняет позиции разделов относительно родительского раздела
        /// </summary>
        /// <param name="parentId"></param>
        [Transaction]
        private async Task SaveSectionsPositionsAsync(long parentId)
        {
            List<long> sectionsIds = await GetDescendantOrSelfSectionsAsync(parentId);
            await SavePathOfSectionsAsync(sectionsIds);
        }

        /// <summary>
        /// Получает всех потомков и себя
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        private async Task<List<long>> GetDescendantOrSelfSectionsAsync(long sectionId)
        {
            if (sectionId == 0)
                return await QueryBuilderFactory.Create<Section>().Select(x => x.Id).ToListAsync();

            return await SectionsQueryMapper.GetDescendantOrSelfSectionsAsync(sectionId);
        }

        /// <summary>
        /// Привязывает баннеры к разделу
        /// </summary>
        /// <param name="section"></param>
        private async Task AddBannersAsync(Section section)
        {
            foreach (var b in section.SectionsToImages)
                await QueryBuilderFactory.Create<SectionToImage>().InsertAsync(new SectionToImage { SectionId = section.Id, ImageId = b.ImageId });
        }

        /// <summary>
        /// Сохраняет пути из разделов
        /// </summary>
        /// <param name="sectionsIds"></param>
        private async Task SavePathOfSectionsAsync(List<long> sectionsIds)
        {
            Dictionary<long, long[]> data = new Dictionary<long, long[]>();
            foreach (long id in sectionsIds)
            {
                long[] pathResult = await SectionsQueryMapper.GetPathOfSectionsAsync(id);
                data.Add(id, pathResult);
            }
            foreach (KeyValuePair<long, long[]> d in data)
                await QueryBuilderFactory.Create<Section>().UpdateAsync(new { PathOfSections = d.Value[0], Position = d.Value[1] }, new { Id = d.Key });
        }
    }
}