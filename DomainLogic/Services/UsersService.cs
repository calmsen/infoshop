﻿using DomainLogic.Interfaces;
using DomainLogic.Interfaces.QueryMappers;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DomainLogic.Models.Settings;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Services.Shared.Editors;

namespace DomainLogic.Services
{
    public class UsersService : BaseService, IUsersService
    {
        [Inject]
        public UserRoleEditor UserRoleEditor { get; set; }
        [Inject]
        public IRoleManagerWrap RoleManagerWrap { get; set; }
        [Inject]
        public CommonUtils CommonUtils { get; set; }
        [Inject]
        public IUsersQueryMapper UsersQueryMapper { get; set; }
        [Inject]
        public MailingSettings MailingSettings { get; set; }

        /// <summary>
        /// Получает список пользователей
        /// </summary>
        /// <param name="offset">Количество записей, которые нужно пропустить</param>
        /// <param name="numberOfItems">Число записей, которые надо выбрать</param>
        /// <returns></returns>
        public async Task<List<User>> GetUsersAsync(int offset, int numberOfItems)
        {
            return await QueryBuilderFactory.Create<User>()
                .OrderBy(x => x.Id)
                .Skip(offset)
                .Take(numberOfItems)
                .ToListAsync();
        }

        /// <summary>
        /// Получает количество пользователей в бд
        /// </summary>
        /// <returns></returns>
        public async Task<int> GetNumberOfUsersAsync()
        {
            return await QueryBuilderFactory.Create<User>().CountAsync();
        }

        /// <summary>
        /// Получает пользователя по идентификатору
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<User> GetUserByIdAsync(int userId)
        {
            return await QueryBuilderFactory.Create<User>()
                .FirstOrDefaultAsync(x => x.Id == userId);
        }

        /// <summary>
        /// Получает пользователя по email-у
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<User> GetUserByEmailAsync(string email)
        {
            return await QueryBuilderFactory.Create<User>()
                .FirstOrDefaultAsync(x => x.Email == email);
        }

        /// <summary>
        /// Получает пользователей по списку идентификаторов
        /// </summary>
        /// <param name="usersIds"></param>
        /// <returns></returns>
        public async Task<List<User>> GetUsersByIdsAsync(List<int> usersIds)
        {
            return await QueryBuilderFactory.Create<User>()
                .ToListAsync(x => usersIds.Contains(x.Id));
        }

        /// <summary>
        /// Создает пользователя
        /// </summary>
        /// <param name="user"></param>
        [Transaction]
        public async Task CreateUserAsync(User user)
        {
            await QueryBuilderFactory.Create<User>().InsertAsync(user);
        }

        /// <summary>
        /// Обновляет пользователя
        /// </summary>
        /// <param name="user"></param>
        [Transaction]
        public async Task UpdateUserAsync(User user)
        {
            await QueryBuilderFactory.Create<User>().UpdateAsync(user);
        }

        /// <summary>
        /// Отписывает пользователя от подписки InfoShop
        /// </summary>
        [Transaction]
        public async Task UnsubscribeAsync(int userId)
        {
            await QueryBuilderFactory.Create<User>().UpdateAsync(new { Subscription = 0, Id = userId });
        }

        /// <summary>
        /// Отписывает пользователя от подписки InfoShop
        /// </summary>
        [Transaction]
        public async Task UnsubscribeAsync(string email, string hash)
        {
            string checkHash = CommonUtils.GetMd5Hash(email + MailingSettings.Md5HahSolt);
            if (hash != checkHash)
                throw new ArgumentException("Неправильный хэш.");
            User user = await GetUserByEmailAsync(email);
            if (user == null)
                throw new ArgumentException("Email неверный или неправильный.");
            await UnsubscribeAsync(user.Id);
        }

        /// <summary>
        /// Подписывает пользователя на подписок InfoShop
        /// </summary>
        /// <param name="userId"></param>
        [Transaction]
        public async Task SubscribeAsync(int userId)
        {
            await QueryBuilderFactory.Create<User>().UpdateAsync(new { Subscription = 1, Id = userId });
        }

        /// <summary>
        /// Получает список имен пользователей, имеющие роли на сайте
        /// </summary>
        /// <returns></returns>
        public async Task<List<string>> GetUserNamesWithRolesAsync()
        {
            return await UsersQueryMapper.GetUserNamesWithRolesAsync();
        }

        public async Task RemoveUnnecessaryUserRoleAsync(string userName)
        {
            await UsersQueryMapper.RemoveUnnecessaryUserRoleAsync(userName);
        }
        
        public List<string> GetAllRoles()
        {
            return RoleManagerWrap.GetAllRoles();
        }

        public async Task<List<string>> GetRolesForUserAsync(string userName)
        {
            return await RoleManagerWrap.GetRolesForUserAsync(userName);
        }

        public async Task AddRoleForUserAsync(string userName, string userRole)
        {
            await UserRoleEditor.AddRoleForUserAsync(userName, userRole);
        }

        public async Task RemoveRoleFromUserAsync(string userName, string userRole)
        {
            await UserRoleEditor.RemoveRoleFromUserAsync(userName, userRole);
        }

        public async Task CreateRoleAsync(string roleName)
        {
            await UserRoleEditor.CreateRoleAsync(roleName);
        }

        public async Task<bool> IsInRoleAsync(string userEmail, string role)
        {
            return await RoleManagerWrap.IsInRoleAsync(userEmail, role);
        }

    }
}