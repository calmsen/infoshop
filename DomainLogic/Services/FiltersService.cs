﻿using DomainLogic.Interfaces.QueryMappers;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Infrastructure.Mappers;
using DomainLogic.Services.Shared.Providers;
using DomainLogic.Models.Enumerations;

namespace DomainLogic.Services
{
    public class FiltersService : BaseService, IFiltersService
    {
        [Inject]
        public SectionsProvider SectionsProvider { get; set; }
        [Inject]
        public FiltersProvider FiltersProvider { get; set; }
        [Inject]
        public FiltersValuesProvider FiltersValuesProvider { get; set; }
        [Inject]
        public FilterMapper FilterMapper { get; set; }
        [Inject]
        public CommonUtils CommonUtils { get; set; }


        [Inject]
        public IProductValuesQueryMapper ProductValuesQueryMapper { get; set; }
        [Inject]
        public IAttributesQueryMapper AttributesQueryMapper { get; set; }
        [Inject]
        public IFilterValuesQueryMapper FilterValuesQueryMapper { get; set; }

        public async Task<Dictionary<long, List<long>>> GetFiltersParamsAsync(NameValueCollection qParams)
        {
            Dictionary<long, List<long>> fParams = new Dictionary<long, List<long>>();
            List<DmFilter> filters = await GetFiltersByNamesAsync(qParams.AllKeys);
            foreach (DmFilter f in filters)
            {
                foreach (string key in qParams.AllKeys)
                {
                    if (f.Name.Equals(key)) {
                        List<long> values = new List<long>();
                        if (qParams[key].IndexOf(",") != -1) {
                            string[] valuesAsArr = qParams[key].Split(',');
                            foreach (string s in valuesAsArr) {
                                long v;
                                long.TryParse(s, out v);
                                if (v <= 0)
                                    continue;
                                values.Add(v);
                            }
                        }
                        else
                        {
                            long v;
                            long.TryParse(qParams[key], out v);
                            if (v <= 0)
                                continue;
                            values.Add(v);
                        }
                        if (values.Count > 0)
                        {
                            fParams[f.Id] = values;
                        }
                        break;
                    }                    
                }
            }
            return fParams;
        }

        public async Task<List<FilterParam>> GetFiltersParamsAsListAsync(NameValueCollection qParams, Section section, List<DmFilter> outFilters = null)
        {
            List<FilterParam> fParams = new List<FilterParam>();
            List<DmFilter> filters = await GetFiltersByNamesAsync(qParams.AllKeys);
            List<FilterGroup> filtersGroups = new List<FilterGroup>();
            if (section != null)
                filtersGroups = section.FiltersGroups;
            foreach (DmFilter f in filters)
            {
                // найдем фильтр с настройками, если настроек нет, то пропустим фильтр
                FilterSettings fs = null;
                foreach (FilterGroup g in filtersGroups)
                {
                    foreach (FilterSettings gf in g.Filters)
                    {
                        if (f.Id == gf.Id)
                            fs = gf;
                    }
                }
                if (fs == null)
                    continue;
                outFilters.Add(f);
                // -- -- --
                foreach (string key in qParams.AllKeys)
                {
                    if (f.Name.Equals(key))
                    {
                        // определим массив значений
                        List<long> values = new List<long>();
                        List<long> valuesK = new List<long>(); // отконвертируемые значения
                        string[] valuesAsArr = qParams[key].Split(',');
                        foreach (string s in valuesAsArr)
                        {
                            if (string.IsNullOrEmpty(s))
                            {
                                values.Add(0);
                                valuesK.Add(0);
                                continue;
                            }

                            long v;
                            if (!long.TryParse(s, out v))
                                continue;
                            values.Add(v);
                            valuesK.Add(v);

                        }
                        // создадим FilterParam если необходимо
                        if (values.Count > 0)
                        {
                            f.SetUpProps(fs);
                            // пропустим фильтры типа диапозон, если значений меньше чем два
                            if (f.Type == FilterTypeEnum.Range && (values.Count < 2 || values[0] == 0 && values[1] == 0))
                                break;
                            // -- -- --

                            // конвертируем значения в зависемости от единицы измерения
                            string[] unitK = null;
                            if (f.CurrentUnitConverter != null)
                            {
                                unitK = f.CurrentUnitConverter.UnitK.Split(':');
                            }
                            if (unitK != null) {
                                for(int i  = 0; i < values.Count; i++) {
                                    valuesK[i] = valuesK[i] * Convert.ToInt32(unitK[0]) / Convert.ToInt32(unitK[1]);
                                }
                            }
                            // -- -- --
                            fParams.Add(new FilterParam
                            {
                                Name = f.Name,
                                Values = values,
                                Id = f.Id,
                                Type = (int)f.Type,
                                Multiple = f.Multiple,
                                ValuesK = valuesK,
                            });
                        }
                        break;
                    }
                }
            }
            return fParams;
        }

        public async Task ReplaceProductsValuesRefsAsync(long filterId1, long filterId2, long sectionId)
        {
            if (!(await CompatibleFilterOneWithFilterTwoAsync(filterId1, filterId2)))
                throw new Exception("Первый фильтр не совместим со вторым фильтром.");
            // обновляем все атрибуты товаров

        }
        
        /// <summary>
        /// Получает список всех фильтров
        /// </summary>
        /// <returns></returns>
        public async Task<List<DmFilter>> GetFiltersAsync()
        {
            return await QueryBuilderFactory.Create<DmFilter>()
                .ToListAsync();
        }

        /// <summary>
        /// Получает фильтры по наименованиям
        /// </summary>
        /// <param name="filterNames"></param>
        /// <returns></returns>
        public async Task<List<DmFilter>> GetFiltersByNamesAsync(string[] filterNames)
        {
            if (filterNames.Count() == 0)
                return new List<DmFilter>();
            return await QueryBuilderFactory.Create<DmFilter>()
                .ToListAsync(f => filterNames.Contains(f.Name));
        }        

        /// <summary>
        /// Получает фильтры для определенного раздела
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<List<DmFilter>> GetFiltersForSectionAsync(long sectionId)
        {
            if (sectionId == 0)
                return new List<DmFilter>();
            // получим список идентификаторов фильтров из данного раздела
            Section section = await SectionsProvider.GetSectionByIdAsync(sectionId);
            List<long> filterIds = section
                .FiltersGroups
                .SelectMany(x => x.Filters)
                .Select(x => x.Id)
                .ToList();
            // получим фильтры по идентификаторам и после отсортируем фильтры по позиции
            List<DmFilter> filters = await GetFiltersByIdsAsync(filterIds);
            filters.ForEach(z => z.Values.Sort((x, y) => x.Position.CompareTo(y.Position)));
            return filters;
        }

        /// <summary>
        /// Получает фильтры по списку идентификаторов
        /// </summary>
        /// <param name="filtersIds"></param>
        /// <param name="needIncludeValues">Нужно ли выбирать значения фильтров</param>
        /// <returns></returns>
        public async Task<List<DmFilter>> GetFiltersByIdsAsync(List<long> filtersIds, bool needIncludeValues = true)
        {
            return await FiltersProvider.GetFiltersByIdsAsync(filtersIds, needIncludeValues);
        }

        /// <summary>
        /// Находит фильтры по строке запроса. Выбираются все фильтры, которые в заголовке содержить данную строку
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<List<DmFilter>> SearchFiltersAsync(string query)
        {
            return await QueryBuilderFactory.Create<DmFilter>()
                .Where(x => x.Title.Contains(query))
                .OrderByDescending(x => x.Id)
                .Take(100)
                .ToListAsync();
        }

        /// <summary>
        /// Получает фильтры для определенных разделов и принимает фильтр к наименованиям, если передана строка запроса
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="pathOfSections"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<List<DmFilter>> GetFiltersAsync(Pagination pagination, long pathOfSections, string query = "")
        {
            query = query.Trim();

            var queryObj = QueryBuilderFactory.Create<DmFilter>();
            // если переданы разделы, то возмем фильтры только из этих разделов
            if (pathOfSections > 0)
            {
                // получим раздел и узнаем идентификаторы фильтров
                List<Section> sections = await SectionsProvider.GetDescendenOrSelfSectionsAsync(pathOfSections);
                List<long> filterIds = sections
                    .SelectMany(x => x.FiltersGroups)
                    .SelectMany(x => x.Filters)
                    .Select(x => x.Id).ToList();
                // добавим условаия в объект queryableObj
                if (filterIds.Count > 0)
                    queryObj = queryObj.Where(x => filterIds.Contains(x.Id));
            }
            // если строка запроса не пустая, то выберем фильтры только содержащие данную строку
            if (!string.IsNullOrEmpty(query))
                queryObj = queryObj.Where(x => x.Title.Contains(query));

            // определим количество пропускаемых записей в выборке и кол-во выбираемых записей
            int skip = 0;
            int take = 200;
            // переопределим skip и take если передан объект pagination
            if (pagination != null)
            {
                pagination.ItemsAmount = await queryObj.CountAsync();
                if (pagination.ItemsAmount == 0)
                    return new List<DmFilter>();
                pagination.Refresh();

                skip = pagination.Offset;
                take = pagination.PageItemsAmount;
            }

            return await queryObj
                .OrderBy(p => p.Id)
                .Skip(skip)
                .Take(take)
                .ToListAsync();
        }

        /// <summary>
        /// Получает фильтр по идентификатору. Если values равен true, то также выбираются значения фильтров
        /// </summary>
        /// <param name="filterId">Идентификатор фильтра</param>
        /// <param name="needsIncludeValues">Получать ли значения</param>
        /// <returns></returns>
        public async Task<DmFilter> GetFilterByIdAsync(long filterId, bool needsIncludeValues = true)
        {
            var query = QueryBuilderFactory.Create<DmFilter>();
            if (needsIncludeValues)
                query = query.Include(x => x.Values);
            var dbFilter = await query.Where(x => x.Id == filterId).FirstOrDefaultAsync();
            if (dbFilter == null)
                return null;
            if (needsIncludeValues)
                dbFilter.Values.Sort((x, y) => x.Position.CompareTo(y.Position));
            return dbFilter;
        }

        /// <summary>
        /// Получает фильтр по наименованию в разделе с идентификатором sectionId
        /// </summary>
        /// <param name="filterTitle"></param>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<DmFilter> GetFilterByTitleAsync(string filterTitle, long sectionId)
        {
            Section section = await SectionsProvider.GetSectionByIdAsync(sectionId);
            List<long> filterIds = section.FiltersGroups.SelectMany(x => x.Filters).Select(x => x.Id).ToList();
            List<DmFilter> filters = await GetFiltersByIdsAsync(filterIds, false);
            DmFilter filter = filters.FirstOrDefault(x => x.Title.Equals(filterTitle));
            return filter;
        }

        /// <summary>
        /// Получает фильтр по наименованию(транслитерируемому) в разделе с идентификатором sectionId
        /// </summary>
        /// <param name="filterName"></param>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<DmFilter> GetFilterByNameAsync(string filterName, long sectionId)
        {
            return await FiltersProvider.GetFilterByNameAsync(filterName, sectionId);
        }
        
        /// <summary>
        /// Обновляет фильтр. Перед обноавлением фильтра удаляет старые ненужные значения
        /// </summary>
        /// <param name="filter"></param>
        [Transaction]
        public async Task EditFilterAsync(DmFilter filter)
        {
            // удалим старые ненужные значения
            await DeleteOldUnnecessaryValuesAsync(filter);
            await UpdateFilterWithoutClearingAsync(filter);
        }

        /// <summary>
        /// Создает фильтр
        /// </summary>
        /// <param name="filter"></param>
        [Transaction]
        public async Task CreateFilterAsync(DmFilter filter)
        {
            // создадим сам фильтр
            filter.Name = CommonUtils.ToLat(filter.Title);
            await QueryBuilderFactory.Create<DmFilter>().InsertAsync(filter);
            // создадим значения фильтра
            for (int i = 0; i < filter.Values.Count; i++)
            {
                filter.Values[i].FilterId = filter.Id;
                filter.Values[i].Position = i;
                long valueSet1, valueSet2, valueSet3, valueSet4;
                DefineValueSet(i, out valueSet1, out valueSet2, out valueSet3, out valueSet4);
                filter.Values[i].ValueSet1 = valueSet1;
                filter.Values[i].ValueSet2 = valueSet2;
                filter.Values[i].ValueSet3 = valueSet3;
                filter.Values[i].ValueSet4 = valueSet4;
                await QueryBuilderFactory.Create<FilterValue>().InsertAsync(filter.Values[i]);
            }
        }

        /// <summary>
        /// Удаляет фильтр.
        /// </summary>
        /// <param name="filterId"></param>
        [Transaction]
        public async Task DeleteFilterAsync(long filterId)
        {
            // удалим все привязанные к товарам характеристики
            await QueryBuilderFactory.Create<DmAttribute>().DeleteAsync(new { FilterId = filterId });
            // удалим сам фильтр
            await QueryBuilderFactory.Create<DmFilter>().DeleteAsync(new { Id = filterId });
        }

        /// <summary>
        /// Создает копию фильтра
        /// </summary>
        /// <param name="filterId"></param>
        /// <returns></returns>
        [Transaction]
        public async Task<DmFilter> CopyFilterAsync(long filterId)
        {
            DmFilter original = await GetFilterByIdAsync(filterId, true);
            DmFilter clon = FilterMapper.Map<DmFilter, DmFilter>(original);
            await CreateFilterAsync(clon);
            return clon;
        }
        
        /// <summary>
        /// Проверяет есть ли множество заполненных значений фильтра для какого-либо товара
        /// </summary>
        /// <param name="filterId"></param>
        /// <returns></returns>
        public async Task<bool> CheckProductsValuesMultipleAsync(long filterId)
        {
            int count = await ProductValuesQueryMapper.GetProductValuesCount(filterId);
            return count > 0;
        }

        /// <summary>
        /// Получает значения для определенного фильтра
        /// </summary>
        /// <param name="filterId"></param>
        /// <returns></returns>
        public async Task<List<FilterValue>> GetFilterValuesAsync(long filterId)
        {
            return await QueryBuilderFactory.Create<FilterValue>()
                .ToListAsync(x => x.FilterId == filterId);
        }

        /// <summary>
        /// Получает значения для определенного фильтра с указанным наименованием в указанном разделе
        /// </summary>
        /// <param name="filterTitle"></param>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<List<FilterValue>> GetFilterValuesAsync(string filterTitle, long sectionId)
        {
            DmFilter filter = await GetFilterByTitleAsync(filterTitle, sectionId);
            if (filter == null)
                return new List<FilterValue>();
            // -- -- --
            return await QueryBuilderFactory.Create<FilterValue>()
                .ToListAsync(x => x.FilterId == filter.Id);
        }

        /// <summary>
        /// Получает значения по списку идентификаторов
        /// </summary>
        /// <param name="valuesIds"></param>
        /// <returns></returns>
        public async Task<List<FilterValue>> GetFiltersValuesAsync(List<long> valuesIds)
        {
            return await FiltersValuesProvider.GetFiltersValuesAsync(valuesIds);
        }

        /// <summary>
        /// Получает значения по списку параметров FilterParam
        /// </summary>
        /// <param name="fParams"></param>
        /// <returns></returns>
        public async Task<List<FilterValue>> GetFiltersValuesAsync(List<FilterParam> fParams)
        {
            return await FiltersValuesProvider.GetFiltersValuesAsync(fParams);
        }

        /// <summary>
        /// Объединяет значения valueId c mainValueId в определенных разделах
        /// </summary>
        /// <param name="valueId"></param>
        /// <param name="mainValueId"></param>
        /// <param name="sectionIds"></param>
        [Transaction]
        public async Task MergeBindedValuesAsync(long valueId, long mainValueId, List<long> sectionIds)
        {
            await FilterValuesQueryMapper.MergeBindedValuesAsync(valueId, mainValueId, sectionIds);
        }

        /// <summary>
        /// Удаляет значения valueId в определенных разделах
        /// </summary>
        /// <param name="valueId"></param>
        /// <param name="sectionIds"></param>
        [Transaction]
        public async Task DeleteBindedValuesAsync(long valueId, List<long> sectionIds)
        {
            await FilterValuesQueryMapper.DeleteBindedValuesAsync(valueId, sectionIds);
        }

        /// <summary>
        /// Копирует значения valueId в определенных разделах
        /// </summary>
        /// <param name="valueId"></param>
        /// <param name="sectionIds"></param>
        /// <returns></returns>
        [Transaction]
        public async Task<FilterValue> CopyBindedValuesAsync(long valueId, List<long> sectionIds)
        {
            FilterValue filterValueOriginal = await GetFilterValueByIdAsync(valueId);

            // получим Position и ValueSet
            int position = await GetLastFilterValuePositionAsync(filterValueOriginal.FilterId) + 1;
            long valueSet1, valueSet2, valueSet3, valueSet4;
            DefineValueSet(position, out valueSet1, out valueSet2, out valueSet3, out valueSet4);

            FilterValue filterValueClone = new FilterValue
            {
                Title = filterValueOriginal.Title.Split(new string[] { " - copy " }, StringSplitOptions.None)[0] + " - copy " + DateTime.Now,
                TitleIn = filterValueOriginal.TitleIn,
                ValueAsDouble = filterValueOriginal.ValueAsDouble,
                FilterId = filterValueOriginal.FilterId,
                Position = position,
                ValueSet1 = valueSet1,
                ValueSet2 = valueSet2,
                ValueSet3 = valueSet3,
                ValueSet4 = valueSet4
            };
            await CreateFilterValueAsync(filterValueClone);
            if (sectionIds == null || sectionIds.Count == 0)
                return filterValueClone;

            // копируем все привязанные значения valueId
            await FilterValuesQueryMapper.CopyBindedValuesAsync(valueId, sectionIds, filterValueClone.Id);

            return filterValueClone;
        }

        /// <summary>
        /// Получает или создает значение, если значения нет
        /// </summary>
        /// <param name="filterId"></param>
        /// <param name="valueTitle"></param>
        /// <param name="valueTitleIn"></param>
        /// <param name="valueAsDouble"></param>
        /// <returns></returns>
        [Transaction]
        public async Task<FilterValue> GetOrCreateFilterValueAsync(long filterId, string valueTitle, string valueTitleIn, double valueAsDouble)
        {
            FilterValue filterValue = await GetFilterValueByTitleAsync(valueTitle, filterId);
            if (filterValue != null)
                return filterValue;
            // получим Position и ValueSet
            int position = await GetLastFilterValuePositionAsync(filterId) + 1;
            long valueSet1, valueSet2, valueSet3, valueSet4;
            DefineValueSet(position, out valueSet1, out valueSet2, out valueSet3, out valueSet4);

            filterValue = new FilterValue
            {
                Title = valueTitle,
                TitleIn = valueTitleIn,
                ValueAsDouble = Math.Floor(valueAsDouble),
                FilterId = filterId,
                Position = position,
                ValueSet1 = valueSet1,
                ValueSet2 = valueSet2,
                ValueSet3 = valueSet3,
                ValueSet4 = valueSet4
            };
            await CreateFilterValueAsync(filterValue);
            return filterValue;
        }

        private async Task<bool> CompatibleFilterOneWithFilterTwoAsync(long filterId1, long filterId2)
        {
            DmFilter filter1 = await GetFilterByIdAsync(filterId1);
            DmFilter filter2 = await GetFilterByIdAsync(filterId2);
            if (!filter1.Type.Equals(filter2.Type))
                return false;
            if (filter1.Multiple != filter2.Multiple)
                return false;
            foreach (FilterValue v in filter1.Values)
            {
                if (!filter2.Values.Any(x => x.Title.Equals(v.Title)) || !filter2.Values.Any(x => x.TitleIn.Equals(v.TitleIn)) || !filter2.Values.Any(x => x.ValueAsDouble.Equals(v.ValueAsDouble)))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Удаляет старые не нужные значения фильтра
        /// </summary>
        /// <param name="filter"></param>
        private async Task DeleteOldUnnecessaryValuesAsync(DmFilter filter)
        {
            if (filter.Values.Any(x => x.Id > 0))
            {
                // получим список идентификаторов значений фильтра
                string valuesIds = string.Join(", ", filter.Values.Where(x => x.Id != 0).Select(x => x.Id));
                // удалим привязанные к товарам ссылки на значения
                await ProductValuesQueryMapper.DeleteProductValues(filter.Id, valuesIds);
                // удалим привязанные к товарам характеристики, не имеющих значений
                await AttributesQueryMapper.DeleteAttributes(filter.Id);
                // удалим сами значения
                await FilterValuesQueryMapper.DeleteFilterValues(filter.Id, valuesIds);
            }
        }

        /// <summary>
        /// Обновляет фильтр без очистки от старых значений. Так же обновляются и создаются новые значения фильтра
        /// </summary>
        /// <param name="filter"></param>
        private async Task UpdateFilterWithoutClearingAsync(DmFilter filter)
        {
            // обновим сам фильтр
            filter.Name = CommonUtils.ToLat(filter.Title);
            await QueryBuilderFactory.Create<DmFilter>().UpdateAsync(filter);
            // обновим и создадим новые значения
            for (int i = 0; i < filter.Values.Count; i++)
            {
                FilterValue v = filter.Values[i];
                v.FilterId = filter.Id;
                v.Position = i;
                long valueSet1, valueSet2, valueSet3, valueSet4;
                DefineValueSet(i, out valueSet1, out valueSet2, out valueSet3, out valueSet4);
                v.ValueSet1 = valueSet1;
                v.ValueSet2 = valueSet2;
                v.ValueSet3 = valueSet3;
                v.ValueSet4 = valueSet4;
                if (v.Id > 0)
                    await QueryBuilderFactory.Create<FilterValue>().UpdateAsync(v);
                else
                    await QueryBuilderFactory.Create<FilterValue>().InsertAsync(v);
            }
        }

        /// <summary>
        /// Получает значение фильтра по идентификатору
        /// </summary>
        /// <param name="valueId"></param>
        /// <returns></returns>
        private async Task<FilterValue> GetFilterValueByIdAsync(long valueId)
        {
            return await QueryBuilderFactory.Create<FilterValue>()
                .FirstOrDefaultAsync(x => x.Id == valueId);
        }

        /// <summary>
        /// Получает значение по наименованию для указанного фильтра
        /// </summary>
        /// <param name="valueTitle"></param>
        /// <param name="filterId"></param>
        /// <returns></returns>
        private async Task<FilterValue> GetFilterValueByTitleAsync(string valueTitle, long filterId)
        {
            return await QueryBuilderFactory.Create<FilterValue>()
                .FirstOrDefaultAsync(x => x.FilterId == filterId && x.Title.Equals(valueTitle));

        }

        /// <summary>
        /// Получает позицию последнего значения для указанного фильтра
        /// </summary>
        /// <param name="filterId"></param>
        /// <returns></returns>
        private async Task<int> GetLastFilterValuePositionAsync(long filterId)
        {
            var dbLastFilterValue = await QueryBuilderFactory.Create<FilterValue>()
                .OrderByDescending(x => x.Position)
                .FirstOrDefaultAsync(x => x.FilterId == filterId);
            if (dbLastFilterValue == null)
                return 0;

            return dbLastFilterValue.Position;
        }

        /// <summary>
        /// Создать значение фильтра
        /// </summary>
        /// <param name="filterValue"></param>
        private async Task CreateFilterValueAsync(FilterValue filterValue)
        {
            await QueryBuilderFactory.Create<FilterValue>().InsertAsync(filterValue);
        }

        /// <summary>
        /// Определеяет наборы для значения, такие как в mysql datatype set.
        /// Такие наборы нужны для поиска товаров по этим наборам, если товар имеет множество заполненных значений по одному фильтру.
        /// Каждое значение может занимать только один бит из всех наборов.
        /// </summary>
        /// <param name="i"></param>
        /// <param name="valueSet1"></param>
        /// <param name="valueSet2"></param>
        /// <param name="valueSet3"></param>
        /// <param name="valueSet4"></param>
        private void DefineValueSet(int i, out long valueSet1, out long valueSet2, out long valueSet3, out long valueSet4)
        {
            valueSet1 = 0;
            valueSet2 = 0;
            valueSet3 = 0;
            valueSet4 = 0;
            if (i < 64)
                valueSet1 = 1L << i;
            else if (i < 128)
                valueSet2 = 1L << i - 64;
            else if (i < 192)
                valueSet3 = 1L << i - 128;
            else if (i < 256)
                valueSet4 = 1L << i - 192;
        }

    }

}