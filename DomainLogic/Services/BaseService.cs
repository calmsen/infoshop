﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Interfaces;
using DomainLogic.Interfaces.QueryBuilder;

namespace DomainLogic.Services
{
    public abstract class BaseService
    {
        [Inject]
        public ILogger Logger {get;set; }

        [Inject]
        public IQueryBuilderFactory QueryBuilderFactory { get; set; }
    }
}