﻿using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Utils;

namespace DomainLogic.Services
{
    public class NewsService : BaseService, INewsService
    {
        [Inject]
        public CommonUtils CommonUtils { get; set; }

        /// <summary>
        /// Получает список новостей
        /// </summary>
        /// <param name="pagination">Информация о количестве новостей на странице</param>
        /// <param name="isActived">Фильтр по активным новостям</param>
        /// <param name="sort">Информация о сортировке новостей</param>
        /// <returns></returns>
        public async Task<List<Article>> GetNewsAsync(Pagination pagination, bool? isActived, Sort sort = null)
        {
            var query = QueryBuilderFactory.Create<Article>();
            // если передан параметр isActived, то добавляем условие
            if (isActived != null)
                query = query.Where(x => x.Active == isActived);
            // узнаем количество записей 
            pagination.ItemsAmount = await query.CountAsync();
            if (pagination.ItemsAmount == 0)
                return new List<Article>();
            pagination.Refresh();
            // сформируем строку сортировки списка новостей
            string order = null;
            if (sort != null)
                order = sort.ColumnName + " " + sort.Order;
            if (order != null)
                order += ",";
            else
                order = "";
            order += "Id desc";
            // получим список новостей
            return await query
                .OrderBy(order)
                .Skip(pagination.Offset)
                .Take(pagination.PageItemsAmount)
                .ToListAsync();
        }

        /// <summary>
        /// Получает новость по идентификатору
        /// </summary>
        /// <param name="articleId"></param>
        /// <returns></returns>
        public async Task<Article> GetArticleAsync(long articleId)
        {
            // получим информацию о новости
            var dbArticle = await QueryBuilderFactory.Create<Article>()
                .Include(x => x.Description)
                .Include(x => x.NewsToImages.Select(y => y.Image))
                .Include(x => x.ProductsToNews.Select(y => y.Product))
                .Where(x => x.Id == articleId)
                .FirstOrDefaultAsync();
            if (dbArticle == null)
                return null;
            // отсортируем картинки по позиции
            dbArticle.NewsToImages.Sort((x, y) => x.Image.Position.CompareTo(y.Image.Position));
            return dbArticle;
        }

        /// <summary>
        /// Обновляет новость
        /// </summary>
        /// <param name="article"></param>
        [Transaction]
        public async Task EditArticleAsync(Article article)
        {
            // отвязываем старые товары
            await UnbindProductsAsync(article);
            // привязываем новые товары
            await BindProductsAsync(article);
            // отвязываем старые картинки
            await DeleteArticleImagesAsync(article);
            // привязываем новые какртинки
            await AddArticleImagesAsync(article);
            // обновляем описание новости
            article.Name = CommonUtils.ToLat(article.Title);
            await QueryBuilderFactory.Create<ArticleDescription>().UpdateAsync(article.Description);
            // обновляем новость
            await QueryBuilderFactory.Create<Article>().UpdateAsync(article);
        }

        /// <summary>
        /// Создает новость
        /// </summary>
        /// <param name="article"></param>
        [Transaction]
        public async Task CreateArticleAsync(Article article)
        {
            // создадим описание новости
            if (!string.IsNullOrWhiteSpace(article.Description.Content))
            {
                await QueryBuilderFactory.Create<ArticleDescription>().InsertAsync(article.Description);
                article.DescriptionId = article.Description.Id;
            }
            else
            {
                article.DescriptionId = null;
            }
            // создадим новость
            article.Name = CommonUtils.ToLat(article.Title);
            article.CreatedDate = DateTime.Now;
            await QueryBuilderFactory.Create<Article>().InsertAsync(article);
            // привязываем картинки к новости
            await AddArticleImagesAsync(article);
            // привязываем товары
            await BindProductsAsync(article);
        }

        /// <summary>
        /// Удаляет новость
        /// </summary>
        /// <param name="article"></param>
        [Transaction]
        public async Task DeleteArticleAsync(Article article)
        {
            // отвязываем картинки
            await DeleteArticleImagesAsync(article);
            // удаляем новость
            await QueryBuilderFactory.Create<Article>().DeleteAsync(article);
            // удаляем описание новости
            if (article.DescriptionId > 0 && article.Description != null)
                await QueryBuilderFactory.Create<ArticleDescription>().DeleteAsync(article.Description);
        }

        /// <summary>
        /// Привязывает картинки к новости
        /// </summary>
        /// <param name="article"></param>
        private async Task AddArticleImagesAsync(Article article)
        {
            for (int i = 0; i < article.NewsToImages.Count; i++)
            {
                // обновим позицию у картинки
                await QueryBuilderFactory.Create<ArticleImage>().UpdateAsync(new { Position = i, Id = article.NewsToImages[i].ImageId });
                // привяжем картинку к новости
                await QueryBuilderFactory.Create<ArticleToImage>().InsertAsync(new ArticleToImage { ArticleId = article.Id, ImageId = article.NewsToImages[i].ImageId });
            }
        }

        /// <summary>
        /// Отвязывает картинки от новости
        /// </summary>
        /// <param name="article"></param>
        private async Task DeleteArticleImagesAsync(Article article)
        {
            await QueryBuilderFactory.Create<ArticleToImage>().DeleteAsync(new { ArticleId = article.Id });
        }

        /// <summary>
        /// Привязывает товары
        /// </summary>
        /// <param name="article"></param>
        private async Task BindProductsAsync(Article article)
        {
            for (int i = 0; i < article.ProductsToNews.Count; i++)
            {
                await QueryBuilderFactory.Create<ProductToArticle>().InsertAsync(
                    new ProductToArticle { ProductId = article.ProductsToNews[i].ProductId, ArticleId = article.Id }
                );
            }
        }

        /// <summary>
        /// Отвязывает товары
        /// </summary>
        /// <param name="article"></param>
        private async Task UnbindProductsAsync(Article article)
        {
            await QueryBuilderFactory.Create<ProductToArticle>().DeleteAsync(new { ArticleId = article.Id });
        }
    }
}
