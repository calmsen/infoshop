﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using DomainLogic.Models.Enumerations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Services
{
    public class VacanciesService : BaseService, IVacanciesService
    {
        [Inject]
        public CommonUtils CommonUtils { get; set; }

        /// <summary>
        /// Получает все вакансии
        /// </summary>
        /// <returns></returns>
        public async Task<List<Vacancy>> GetVacanciesAsync()
        {
            return await QueryBuilderFactory.Create<Vacancy>()
                .OrderByDescending(p => p.Id)
                .ToListAsync();
        }

        /// <summary>
        /// Получает вакансии с указанным состоянием
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public async Task<List<Vacancy>> GetVacanciesAsync(VacancyStateEnum state)
        {
            return await QueryBuilderFactory.Create<Vacancy>()
                .Where(x => x.State == state)
                .OrderByDescending(p => p.Id)
                .ToListAsync();
        }

        /// <summary>
        /// Получает вакансию по идентификатору
        /// </summary>
        /// <param name="vacancyId"></param>
        /// <returns></returns>
        public async Task<Vacancy> GetVacancyAsync(int vacancyId)
        {
            return await QueryBuilderFactory.Create<Vacancy>()
                .FirstOrDefaultAsync(x => x.Id == vacancyId);
        }

        /// <summary>
        /// Обновляет вакансию
        /// </summary>
        /// <param name="vacancy"></param>
        [Transaction]
        public async Task EditVacancyAsync(Vacancy vacancy)
        {
            vacancy.Name = CommonUtils.ToLat(vacancy.Title);

            await QueryBuilderFactory.Create<Vacancy>().UpdateAsync(vacancy);

        }

        /// <summary>
        /// Создает вакансию
        /// </summary>
        /// <param name="vacancy"></param>
        [Transaction]
        public async Task CreateVacancyAsync(Vacancy vacancy)
        {
            vacancy.Name = CommonUtils.ToLat(vacancy.Title);

            await QueryBuilderFactory.Create<Vacancy>().InsertAsync(vacancy);
        }

        /// <summary>
        /// Удаляет вакансию
        /// </summary>
        /// <param name="vacancy"></param>
        [Transaction]
        public async Task DeleteVacancyAsync(Vacancy vacancy)
        {
            await QueryBuilderFactory.Create<Vacancy>().DeleteAsync(vacancy);
        }
    }
}