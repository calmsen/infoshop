﻿using DomainLogic.Interfaces.QueryMappers;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DomainLogic.Models.Settings;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Services.Shared.Providers;
using DomainLogic.Models.Enumerations;
using DomainLogic.Interfaces.QueryBuilder;

namespace DomainLogic.Services
{
    public class FilesService : BaseService, IFilesService
    {
        [Inject]
        public ProductsProvider ProductsProvider { get; set; }

        [Inject]
        public IProductsToFilesQueryMapper ProductsToFilesQueryMapper { get; set; }

        [Inject]
        public St0SiteSettings St0SiteSettings { get; set; }
        [Inject]
        public ImagesLoaderSettings ImagesLoaderSettings { get; set; }
        [Inject]
        public ProductsSettings ProductsSettings { get; set; }
        
        private IQueryBuilder<Faq> QueryBuilder => QueryBuilderFactory.Create<Faq>();
        
        /// <summary>
        /// Получает список файлов с указанным filters для определенного товара 
        /// </summary>
        /// <param name="filters">Фильтры</param>
        /// <param name="productId">Идентификатор товара</param>
        /// <returns></returns>
        public async Task<List<DmFile>> GetFilesAsync(FileFilterEnum filters, long productId)
        {
            // получим товар для которого нужно найти фильтры
            Product product = await ProductsProvider.GetProductByIdAsync(productId);
            if (product == null)
                return new List<DmFile>();
            // получим файлы
            int filtersAsInt = (int)filters;
            return await QueryBuilderFactory.Create<DmFile>()
                .Where(x => x.ProductsToFiles.Any(y => y.ProductId == productId)
                    && (filtersAsInt == 0 || (x.Filters.AsInt & filtersAsInt) > 0))
                .OrderBy(x => x.Title)
                .ToListAsync();
        }

        /// <summary>
        /// Получает список файлов для определенного товара
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public async Task<List<DmFile>> GetFilesAsync(long productId)
        {
            return await GetFilesAsync(FileFilterEnum.None, productId);
        }

        /// <summary>
        /// Находит файлы с указанным filters по строке запроса. 
        /// Если запрос парсится в число то производится поиск файлов для определенного товара. 
        /// Иначе производится поиск товаров, а список файлов будет пустым.
        /// </summary>
        /// <param name="query">Строка запроса</param>
        /// <param name="filters">Фильтры</param>
        /// <param name="pagination">Пагинация для поиска товаров</param>
        /// <param name="sectionsIds">Разделы, в которых искать товары</param>
        /// <returns>Список файлов</returns>
        public async Task<List<DmFile>> SerchFilesAsync(string query, FileFilterEnum filters, Pagination pagination, List<long> sectionsIds)
        {
            // если строка парсится в число то получим список файлов
            if (!string.IsNullOrWhiteSpace(query) && new Regex(@"^\d+$").IsMatch(query))
                return await GetFilesAsync(filters, long.Parse(query));

            return new List<DmFile>();

        }

        /// <summary>
        /// Получает  файл по идишнику
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<DmFile> GetFileByIdAsync(long id)
        {
            return await QueryBuilderFactory.Create<DmFile>()
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        /// <summary>
        /// Редактирует файл
        /// </summary>
        /// <param name="file"></param>
        [Transaction]
        public async Task EditFileAsync(DmFile file, long productId)
        {
            await QueryBuilderFactory.Create<DmFile>().UpdateAsync(file);
            await AddFilesWithSameSectionOrPlatformAsync(file.Id, productId, file.DisplayWithSameSection, file.DisplayWithSamePlatform);
        }

        /// <summary>
        /// Создает файл
        /// </summary>
        /// <param name="file"></param>
        [Transaction]
        public async Task CreateFileAsync(DmFile file, long productId)
        {
            if (file.Id > 0)
                throw new ArgumentException("file.Id не должен быть больше нуля.");
            file.CreatedDate = DateTime.Now;
            await QueryBuilderFactory.Create<DmFile>().InsertAsync(file);
            await QueryBuilderFactory.Create<ProductToFile>().InsertAsync(new ProductToFile { ProductId = productId, FileId = file.Id });
            await AddFilesWithSameSectionOrPlatformAsync(file.Id, productId, file.DisplayWithSameSection, file.DisplayWithSamePlatform);
        }

        /// <summary>
        /// Удаляет файл
        /// </summary>
        /// <param name="file"></param>
        [Transaction]
        public async Task DeleteFileAsync(DmFile file)
        {
            await QueryBuilderFactory.Create<DmFile>().DeleteAsync(file);
        }

        public async Task LoadFileAsync(Stream file, string fileName, string folder)
        {
            string fileExt = System.IO.Path.GetExtension(fileName);
            CheckFile(fileExt);
            string dir = AppDomain.CurrentDomain.BaseDirectory + System.IO.Path.Combine(St0SiteSettings.Root, "AppFiles", folder);
            System.IO.Directory.CreateDirectory(dir);
            string path = System.IO.Path.Combine(dir, fileName);
            if (System.IO.File.Exists(path))
                System.IO.File.Delete(path);
            using (FileStream fileStream = File.Create(path, (int)file.Length))
            {
                await file.CopyToAsync(fileStream);
            }
        }

        private async Task AddFilesWithSameSectionOrPlatformAsync(long fileId, long productId, bool displayWithSameSection, bool displayWithSamePlatform)
        {
            if (!displayWithSameSection && !displayWithSamePlatform)
                return;

            Product product = await ProductsProvider.GetProductByIdAsync(productId);
            if (displayWithSameSection)
            {
                await ProductsToFilesQueryMapper.AddFilesWithSameSection(fileId, product.SectionId);
            }
            if (displayWithSamePlatform)
            {
                try
                {
                    long? filterValueId = product.Attributes.FirstOrDefault(x => ProductsSettings.PlatformFilterIds.Contains(x.FilterId)).Values[0].ValueId;
                    if (filterValueId != null)
                    {
                        await ProductsToFilesQueryMapper.AddFilesWithSameFilterValue(fileId, (long)filterValueId);
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void CheckFile(string fileExt)
        {
            if (!ImagesLoaderSettings.AllowedExts.Any(x => x == fileExt))
                throw new Exception("Файл не является архивом или документом.");
        }
    }
}
