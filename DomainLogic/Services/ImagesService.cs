﻿using DomainLogic.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DomainLogic.Infrastructure;
using DomainLogic.Services.Shared.Helpers;
using DomainLogic.Models.Settings;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Infrastructure.Credentials;
using DomainLogic.Interfaces.ImageInterfaces;

namespace DomainLogic.Services
{
    public class ImagesService: BaseService, IImagesService
    {
        [Inject]
        public ImagesHelper ImagesHelper { get; set; }
        [Inject]
        public FileUtils FileUtils { get; set; }
        [Inject]
        public IUrlUtils UrlUtils { get; set; }

        [Inject]
        public ImagesLoaderSettings ImagesLoaderSettings { get; set; }
        [Inject]
        public St0SiteSettings St0SiteSettings { get; set; }

        public async Task<T> UploadImageAsync<T>(Stream file)
        {
            return (T)(await UploadImageAsync(typeof(T), file));
        }

        public async Task<object> UploadImageAsync(Type type, Stream file)
        {
            object image = (object)Activator.CreateInstance(type);
            if (!(image is IImage))
                throw new Exception("Класс должен реализовывать интерфейс IImage.");
            if (!(image is IImageDimension))
                throw new Exception("Класс должен реализовывать интерфейс IImageDimension.");

            await CreateImageAsync(type, image);
            UploadImage(file, (image as IImage).Id + ".jpg", ImagesHelper.GetFolder(type), true, (IImageDimension)image, GetMinDim(type));
            await UpdateImageAsync(type, image);
            return image;
        }

        public async Task<object> UploadImageAsync(string type, Stream file)
        {
            return await UploadImageAsync(GetType(type), file);
        }

        public async Task<IList<T>> UploadImagesAsync<T>(Stream[] files)
        {
            return (IList<T>)(await UploadImagesAsync(typeof(T), files));
        }

        public async Task<IList<object>> UploadImagesAsync(Type type, Stream[] files)
        {
            IList<object> images = new List<object>();
            foreach (Stream f in files)
                images.Add(await UploadImageAsync(type, f));
            return images;
        }

        public async Task<IList<object>> UploadImagesAsync(string type, Stream[] files)
        {
            return await UploadImagesAsync(GetType(type), files);
        }
        
        public async Task<T> UploadImageAsync<T>(string url)
        {
            return (T)(await UploadImageAsync(typeof(T), url));
        }

        public async Task<object> UploadImageAsync(Type type, string url)
        {
            object image = Activator.CreateInstance(type);
            await CreateImageAsync(type, image);
            await UploadImageAsync(url, (image as IImage).Id + ".jpg", ImagesHelper.GetFolder(type), true, (IImageDimension)image, 600);
            await UpdateImageAsync(type, image);
            return image;
        }

        public async Task<object> UploadImageAsync(string type, string url)
        {
            return await UploadImageAsync(GetType(type), url);
        }
        
        public async Task<IList<T>> UploadImagesAsync<T>(string url)
        {
            return (IList<T>)(await UploadImagesAsync(typeof(T), url));
        }

        public Task<IList<object>> UploadImagesAsync(Type type, string url)
        {
            throw new NotImplementedException();
        }

        public async Task<IList<object>> UploadImagesAsync(string type, string url)
        {
            return await UploadImagesAsync(GetType(type), url);
        }
        
        public async Task<T> UploadImageAsync<T>(HttpCredential settings, string url)
        {
            return (T)(await UploadImageAsync(typeof(T), settings, url));
        }

        public async Task<object> UploadImageAsync(Type type, HttpCredential settings, string url)
        {
            object image = Activator.CreateInstance(type);
            await CreateImageAsync(type, image);
            await UploadImageAsync(settings, url, (image as IImage).Id + ".jpg", ImagesHelper.GetFolder(type), true, (IImageDimension)image, 600);
            await UpdateImageAsync(type, image);
            return image;
        }

        public async Task<object> UploadImageAsync(string type, HttpCredential settings, string url)
        {
            return await UploadImageAsync(GetType(type), settings, url);
        }
        
        public async Task<IList<T>> UploadImagesAsync<T>(HttpCredential settings, string url)
        {
            return (IList<T>)(await UploadImagesAsync(typeof(T), settings, url));
        }

        public Task<IList<object>> UploadImagesAsync(Type type, HttpCredential settings, string url)
        {
            throw new NotImplementedException();
        }

        public async Task<IList<object>> UploadImagesAsync(string type, HttpCredential settings, string url)
        {
            return await UploadImagesAsync(GetType(type), settings, url);
        }

        public async Task<T> UploadImageAsync<T>(FtpCredential credential, string ftpPath)
        {
            return (T)(await UploadImageAsync(typeof(T), credential, ftpPath));
        }

        public async Task<object> UploadImageAsync(Type type, FtpCredential credential, string ftpPath)
        {
            object image = Activator.CreateInstance(type);
            await CreateImageAsync(type, image);
            await UploadImageAsync(credential, ftpPath, (image as IImage).Id + ".jpg", ImagesHelper.GetFolder(type), true, (IImageDimension)image, 600);
            await UpdateImageAsync(type, image);
            return image;
        }

        public async Task<object> UploadImageAsync(string type, FtpCredential credential, string ftpPath)
        {
            return await UploadImageAsync(GetType(type), credential, ftpPath);
        }
        
        public async Task<IList<T>> UploadImagesAsync<T>(FtpCredential credential, string ftpPath)
        {
            return (IList<T>)(await UploadImagesAsync(typeof(T), credential, ftpPath));
        }

        public async Task<IList<object>> UploadImagesAsync(Type type, FtpCredential credential, string ftpPath)
        {
            List<string> filesNames = FileUtils.GetFilesNamesByFtp(credential, ftpPath);
            // загрузим картинки
            IList<object> images = new List<object>();
            foreach (string fileName in filesNames)
            {
                images.Add(await UploadImageAsync(type, credential, ftpPath + "/" + fileName));
            }
            return images;
        }

        public async Task<IList<object>> UploadImagesAsync(string type, FtpCredential credential, string ftpPath)
        {
            return await UploadImagesAsync(GetType(type), credential, ftpPath);
        }

        private string GetMiniatureFolder(int dimension)
        {
            switch(dimension) {
                case 800: return "max";
                case 400: return "middle";
                case 200: return "normal";
                case 100: return "min";
                default: return null;
            }
        }

        public void ReloadImage(string type, Stream file, long id, int dimension) 
        {
            string watermarkPath = null;
            if (dimension == 800 && type.Equals("Products"))                       
                watermarkPath = AppDomain.CurrentDomain.BaseDirectory + @"Content\Styles\Images\watermark.png";

            string path = UrlUtils.MapPath(St0SiteSettings.Root) + @"Images\" + type + @"\" + GetMiniatureFolder(dimension) + @"\" + id + ".jpg";
            if (System.IO.File.Exists(path))
                System.IO.File.Delete(path);
            ResizeImage(dimension, file, path, watermarkPath);
        }

        public void DrawWatermark(string watermarkImagePath, System.Drawing.Image image)
        {
            using (System.Drawing.Image watermarkImage = System.Drawing.Image.FromFile(watermarkImagePath))
            using (Graphics imageGraphics = Graphics.FromImage(image))
            using (TextureBrush watermarkBrush = new TextureBrush(watermarkImage))
            {
                
                if (image.Width < watermarkImage.Width || image.Height < watermarkImage.Height)
                    return;
                int x = 0, y = 0;
                while (y < image.Height)
                {
                    while (x < image.Width)
                    {
                        watermarkBrush.TranslateTransform(x, y);
                        imageGraphics.FillRectangle(watermarkBrush, new Rectangle(new Point(x, y), new Size(watermarkImage.Width + 1, watermarkImage.Height)));
                        x += watermarkImage.Width;
                    }
                    x = 0;
                    y += watermarkImage.Height;
                }
            }
        }

        private void ResizeImage(int size, Bitmap curImage, string saveFilePath, string watermarkPath = null)
        {
            //variables for image dimension/scale
            double newHeight = 0;
            double newWidth = 0;
            double scale = 0;

            //Determine image scaling
            if (curImage.Height > curImage.Width)
                scale = Convert.ToSingle(size) / curImage.Height;
            else
                scale = Convert.ToSingle(size) / curImage.Width;

            //New image dimension
            newHeight = Math.Floor(Convert.ToSingle(curImage.Height) * scale);
            newWidth = Math.Floor(Convert.ToSingle(curImage.Width) * scale);

            //Create new object image
            Bitmap newImage = new Bitmap(curImage, Convert.ToInt32(newWidth), Convert.ToInt32(newHeight));
            Graphics imgDest = Graphics.FromImage(newImage);
            imgDest.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            imgDest.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            imgDest.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            imgDest.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            ImageCodecInfo[] info = ImageCodecInfo.GetImageEncoders();
            EncoderParameters param = new EncoderParameters(1);
            param.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);

            //Draw the object image
            imgDest.DrawImage(curImage, 0, 0, newImage.Width, newImage.Height);

            // Draw watermark if need
            if (watermarkPath != null)
                DrawWatermark(watermarkPath, newImage);
            //Save image file
            newImage.Save(saveFilePath, info[1], param);

            //Dispose the image objects
            curImage.Dispose();
            newImage.Dispose();
            imgDest.Dispose();
        }

        private void ResizeImage(int size, Stream stream, string saveFilePath, string watermarkPath = null)
        {
            Bitmap curImage = new Bitmap(stream);

            ResizeImage(size, curImage, saveFilePath, watermarkPath);
        }

        public void ResizeImage(int size, string filePath, string saveFilePath, string watermarkPath = null)
        {   
            Bitmap curImage = new Bitmap(filePath);

            ResizeImage(size, curImage, saveFilePath, watermarkPath);
        }

        public async Task UploadImageAsync(HttpCredential settings, string url, string fileNameDst, string folderDst, bool createMini = true, IImageDimension imDim = null, int minDim = 800)
        {
            if (!url.StartsWith("http://"))
            {
                throw new Exception("Не правильно указан url.");
            }
            if (settings == null || settings.LoginForm == null || settings.LoginForm == null)
            {
                WebRequest request = WebRequest.Create(url);
                using (WebResponse response = await request.GetResponseAsync())
                using (Stream stream = response.GetResponseStream())
                {
                    UploadImage(stream, fileNameDst, folderDst, createMini, imDim, minDim);
                }
                return;
            }
            HtmlParser hp = new HtmlParser
            {
                BaseUrl = settings.BaseUrl,
                LoginForm = settings.LoginForm,
                UserName = settings.UserName,
                Password = settings.Password,
                AuthString = settings.AuthString
            };
            hp.Authorize();
            //
            HttpWebRequest hrequest = (HttpWebRequest)HttpWebRequest.Create(url);
            hrequest.Headers.Add("Cookie", hp.GetCookieHeader());
            using (WebResponse res = await hrequest.GetResponseAsync())
            using (Stream stream = res.GetResponseStream())
            {
                UploadImage(stream, fileNameDst, folderDst, createMini, imDim, minDim);
            }
        }

        public async Task UploadImageAsync(FtpCredential credential, string ftpPath, string fileNameDst, string folderDst, bool createMini = true, IImageDimension imDim = null, int minDim = 800)
        {
            if (!ftpPath.StartsWith("ftp://"))
            {
                throw new Exception("Не правильно указан ftp путь.");
            }
            string userName = "anonymous";
            string userPassword = "";
            if (credential != null)
            {
                userName = credential.UserName;
                userPassword = credential.Password;
            }
            FtpWebRequest frequest = (FtpWebRequest)WebRequest.Create(ftpPath);
            frequest.Method = WebRequestMethods.Ftp.DownloadFile;
            frequest.Credentials = new NetworkCredential(userName, userPassword);
            using (FtpWebResponse fresponse = (FtpWebResponse)(await frequest.GetResponseAsync()))
            using (Stream fstream = fresponse.GetResponseStream())
            {
                UploadImage(fstream, fileNameDst, folderDst, createMini, imDim, minDim);
            }
        }

        public void UploadImage(FileCredential credential, string filePath, string fileNameDst, string folderDst, bool createMini = true, IImageDimension imDim = null, int minDim = 800)
        {
            using (FileStream stream = new FileStream(filePath, FileMode.Open)) 
            {
                string fileNameSrc = Path.GetFileName(filePath);
                string fileExtSrc = Path.GetExtension(fileNameSrc);
                if (!new string[] { ".jpg", ".jpeg", ".png", ".gif", ".bmp" }.Any(x => x == fileExtSrc))
                    throw new Exception("Файл не является изображением.");
                UploadImage(stream, fileNameDst, folderDst, createMini, imDim, minDim);
            }
        }
        
        public async Task UploadImageAsync(string urlOrFtpPathOrFilePath, string fileNameDst, string folderDst, bool createMini = true, IImageDimension imDim = null, int minDim = 800)
        {
            if (urlOrFtpPathOrFilePath.StartsWith("http://"))
            {
                await UploadImageAsync((HttpCredential)null, urlOrFtpPathOrFilePath, fileNameDst, folderDst, createMini, imDim, minDim);
                return;
            }
            if (urlOrFtpPathOrFilePath.StartsWith("ftp://"))
            {
                await UploadImageAsync((FtpCredential)null, urlOrFtpPathOrFilePath, fileNameDst, folderDst, createMini, imDim, minDim);
                return;
            }
            UploadImage((FileCredential)null, urlOrFtpPathOrFilePath, fileNameDst, folderDst, createMini, imDim, minDim);
        }

        public void UploadImage(Stream stream, string fileNameDst, string folderDst, bool createMini = true, IImageDimension imDim = null, int minDim = 800)
        {
            using (System.Drawing.Image image = System.Drawing.Image.FromStream(stream))
            {
                if (image.Width < minDim && image.Height < minDim)
                    throw new Exception("Ширина и высота не может быть одновременно меньше чем " + minDim + "px.");
                if (imDim != null)
                {
                    imDim.Width = image.Width;
                    imDim.Height = image.Height;
                }
                string path = UrlUtils.MapPath(St0SiteSettings.Root) + "Images\\" + folderDst;
                if (!System.IO.Directory.Exists(path))
                    System.IO.Directory.CreateDirectory(path);

                string fileExtDst = Path.GetExtension(fileNameDst);
                string originalFilePath = Path.Combine(path, fileNameDst);
                if (fileExtDst.Equals(".jpg") || fileExtDst.Equals(".jpeg"))
                    image.Save(originalFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                else
                    throw new Exception("Нет обработчика для данного разрешения файла.");

                string watermarkPath = null;
                if (folderDst.Equals("Products"))
                    watermarkPath = AppDomain.CurrentDomain.BaseDirectory + @"Content\Styles\Images\watermark.png";
                if (createMini)
                    CreateMini(originalFilePath, path, fileNameDst, watermarkPath);
            }
        }
        
        public void CreateMini(string originalFilePath, string path, string fileName, string watermarkPath = null)
        {
            if (!System.IO.Directory.Exists(Path.Combine(path, "max")))
                System.IO.Directory.CreateDirectory(Path.Combine(path, "max"));
            if (!System.IO.Directory.Exists(Path.Combine(path, "middle")))
                System.IO.Directory.CreateDirectory(Path.Combine(path, "middle"));
            if (!System.IO.Directory.Exists(Path.Combine(path, "normal")))
                System.IO.Directory.CreateDirectory(Path.Combine(path, "normal"));
            if (!System.IO.Directory.Exists(Path.Combine(path, "min")))
                System.IO.Directory.CreateDirectory(Path.Combine(path, "min"));


            ResizeImage(800, originalFilePath, Path.Combine(path, "max", fileName), watermarkPath);
            ResizeImage(400, originalFilePath, Path.Combine(path, "middle", fileName));
            ResizeImage(200, originalFilePath, Path.Combine(path, "normal", fileName));
            ResizeImage(100, originalFilePath, Path.Combine(path, "min", fileName));
        }
        
        private int GetMinDim(Type type)
        {
            int minDim = ImagesLoaderSettings.ImageMinDim;
            // redefine
            string folder = ImagesHelper.GetFolder(type);
            if (ImagesLoaderSettings.ImageMinDimsByFolders.ContainsKey(folder))
                minDim = Convert.ToInt32(ImagesLoaderSettings.ImageMinDimsByFolders[folder]);
            // -- -- --
            return minDim;
        }

        public async Task<IEnumerable<object>> SearchAsync(string type, string query, int page)
        {
            return await SearchAsync(GetType(type), query, page);
        }

        /// <summary>
        /// Находит список картинок по заголовку. Если строка запроса пустая, то список не фильтруется по заголовку
        /// </summary>
        /// <param name="type">Тип объекта картинки</param>
        /// <param name="query">Строка запроса - часть заголовка картинки</param>
        /// <param name="page">Номер запрашиваемой страницы</param>
        /// <returns></returns>
        public async Task<IEnumerable<object>> SearchAsync(Type type, string query, int page)
        {
            var queryObj = QueryBuilderFactory.Create(type);
            // если строка не пустая то добавим условие в запрос
            if (!string.IsNullOrWhiteSpace(query))
                queryObj = queryObj.Where("Title.Contains(@0)", query);
            // выбираем картинки только для определенной странички
            var dbImages = await queryObj.OrderBy("Id desc").Skip((page - 1) * 12).Take(12).ToListAsync();
            // получаем и конвертируем картинки в доменные объекты
            List<object> images = new List<object>();
            foreach (var dbIm in dbImages)
                images.Add(dbIm);

            return images;
        }

        /// <summary>
        /// Получает информацию о картинке по идентификатору
        /// </summary>
        /// <typeparam name="T">Тип объекта картинки</typeparam>
        /// <param name="id">Идентификатор картинки</param>
        /// <returns></returns>
        public async Task<T> GetImageByIdAsync<T>(long id)
        {
            return (T)(await GetImageByIdAsync(typeof(T), id));
        }

        /// <summary>
        /// Получает информацию о картинке по идентификатору
        /// </summary>
        /// <param name="type">Тип объекта картинки</param>
        /// <param name="id">Идентификатор картинки</param>
        /// <returns></returns>
        public async Task<object> GetImageByIdAsync(Type type, long id)
        {
            var dbImage = await QueryBuilderFactory.Create(type).FirstOrDefaultAsync("Id = @0", id);
            if (dbImage == null)
                return null;
            return dbImage;
        }

        /// <summary>
        /// Получает информацию о картинке по идентификатору
        /// </summary>
        /// <param name="type">Тип объекта картинки</param>
        /// <param name="id">Идентификатор картинки</param>
        /// <returns></returns>
        public async Task<object> GetImageByIdAsync(string type, long id)
        {
            return await GetImageByIdAsync(GetType(type), id);
        }

        /// <summary>
        /// Создает картинку 
        /// </summary>
        /// <typeparam name="T">Тип объекта картинки</typeparam>
        /// <param name="image"></param>
        [Transaction]
        public async Task CreateImageAsync<T>(T image)
        {
            await CreateImageAsync(typeof(T), image);
        }

        /// <summary>
        /// Создает картинку
        /// </summary>
        /// <param name="type">Тип объекта картинки</param>
        /// <param name="image"></param>
        [Transaction]
        public async Task CreateImageAsync(Type type, object image)
        {
            await QueryBuilderFactory.Create(type).InsertAsync(image);
        }

        /// <summary>
        /// Обновляет информацию о картинке
        /// </summary>
        /// <typeparam name="T">Тип объекта картинки</typeparam>
        /// <param name="image"></param>
        [Transaction]
        public async Task UpdateImageAsync<T>(T image)
        {
            await UpdateImageAsync(typeof(T), image);
        }

        /// <summary>
        /// Обновляет информацию о картинке
        /// </summary>
        /// <param name="type">Тип объекта картинки</param>
        /// <param name="image"></param>
        [Transaction]
        public async Task UpdateImageAsync(Type type, object image)
        {
            await QueryBuilderFactory.Create(type).UpdateAsync(image);
        }

        /// <summary>
        /// Обновляет информацию о картинке
        /// </summary>
        /// <param name="type">Тип объекта картинки</param>
        /// <param name="image"></param>
        [Transaction]
        public async Task UpdateImageAsync(string type, object image)
        {
            await UpdateImageAsync(GetType(type), image);
        }
        
        /// <summary>
        /// Получает тип для соотвествующей строки
        /// </summary>
        /// <param name="type">Тип объекта картинки</param>
        /// <returns></returns>
        private Type GetType(string type)
        {
            return Type.GetType(ImagesHelper.ImageTypesByFolders[type]);
        }
    }
}