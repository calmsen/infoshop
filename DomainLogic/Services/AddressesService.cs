﻿using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Services.Shared.Helpers;
using DomainLogic.Interfaces.QueryBuilder;

namespace DomainLogic.Services
{
    public class AddressesService : BaseService, IAddressesService
    {
        [Inject]
        public CountryHelper CountryHelper { get; set; }

        [Inject]
        public DistrictHelper DistrictHelper { get; set; }

        [Inject]
        public RegionHelper RegionHelper { get; set; }

        [Inject]
        public CityHelper CityHelper { get; set; }

        [Inject]
        public CommonUtils CommonUtils { get; set; }

        private IQueryBuilder<Address> QueryBuilder => QueryBuilderFactory.Create<Address>();

        [Transaction]
        public async Task<Address> GetAddressByIpAsync(string ip)
        {
            string xml = await GetRemoteIpDataAsync(ip);
            Address address = ParseXmlIpData(xml);
            
            Address exAddress = await QueryBuilder
                .Include(x => x.Country)
                .Include(x => x.District)
                .Include(x => x.Region)
                .Include(x => x.City)
                .FirstOrDefaultAsync(x => x.Ip.Equals(ip));

            if (exAddress != null && AddressEquals(address, exAddress))
                return exAddress;
            if (exAddress != null)
                await DeleteAddressAsync(exAddress);
            await CreateAddressAsync(address);
            return address;

        }
        
        /// <summary>
        /// Создает адрес
        /// </summary>
        /// <param name="address"></param>
        private async Task CreateAddressAsync(Address address)
        {
            await CountryHelper.CreateOrUpdateCountryAsync(address.Country);
            if (address.Country.Id > 0)
                address.CountryId = address.Country.Id;
            await DistrictHelper.CreateOrUpdateDistrictAsync(address.District);
            if (address.District.Id > 0)
                address.DistrictId = address.District.Id;
            await RegionHelper.CreateOrUpdateRegionAsync(address.Region);
            if (address.Region.Id > 0)
                address.RegionId = address.Region.Id;
            await CityHelper.CreateOrUpdateCityAsync(address.City);
            if (address.City.Id > 0)
                address.CityId = address.City.Id;

            await QueryBuilder.InsertAsync(address);
        }

        /// <summary>
        /// Удаляет адрес
        /// </summary>
        /// <param name="address"></param>
        private async Task DeleteAddressAsync(Address address)
        {
            await QueryBuilder.DeleteAsync(address);
        }

        private bool AddressEquals(Address address1, Address address2)
        {
            return string.Equals(CommonUtils.GetMd5Hash(address1.Ip + address1.Lat + address1.Lng), CommonUtils.GetMd5Hash(address2.Ip + address2.Lat + address2.Lng));
        }

        private async Task<string> GetRemoteIpDataAsync(string ip)
        {
            byte[] responseBytes;
            WebRequest request = WebRequest.Create("http://ipgeobase.ru:7020/geo?ip=" + ip);
            using (WebResponse response = request.GetResponse())
            using (Stream rs = response.GetResponseStream())
            using (MemoryStream ms = new MemoryStream())
            {
                byte[] buffer = new byte[2048];
                int count;
                while ((count = await rs.ReadAsync(buffer, 0, buffer.Length)) > 0)
                    ms.Write(buffer, 0, count);
                responseBytes = ms.ToArray();

            }
            responseBytes = Encoding.Convert(Encoding.GetEncoding("windows-1251"), UTF8Encoding.UTF8, responseBytes);
            return Encoding.UTF8.GetString(responseBytes);
        }

        private Address ParseXmlIpData(string xml)
        {
            var doc = new XmlDocument();
            doc.Load(new StringReader(xml));
            XmlNode ipXmlNode = doc.SelectSingleNode("//ip");
            return new Address
            {
                Ip = ipXmlNode.Attributes["value"].Value,
                Country = new Country { Alias = ipXmlNode.SelectSingleNode("country").InnerText },
                District = new District { Name = ipXmlNode.SelectSingleNode("district").InnerText },
                Region = new Region { Name = ipXmlNode.SelectSingleNode("region").InnerText },
                City = new City { Name = ipXmlNode.SelectSingleNode("city").InnerText },
                Lat = ipXmlNode.SelectSingleNode("lat").InnerText,
                Lng = ipXmlNode.SelectSingleNode("lng").InnerText
            };
        }
    }
}