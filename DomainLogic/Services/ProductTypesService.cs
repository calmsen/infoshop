﻿using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Attributes;

namespace DomainLogic.Services
{
    public class ProductTypesService: BaseService, IProductTypesService
    {

        /// <summary>
        /// Получает все изделия
        /// </summary>
        /// <returns></returns>
        public async Task<List<ProductType>> GetTypesAsync()
        {
            return await QueryBuilderFactory.Create<ProductType>()
                .ToListAsync();
        }

        /// <summary>
        /// Получает изделие по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ProductType> GetTypeByIdAsync(long id)
        {
            return await QueryBuilderFactory.Create<ProductType>()
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        /// <summary>
        /// Обновляет изделие
        /// </summary>
        /// <param name="type"></param>
        [Transaction]
        public async Task EditTypeAsync(ProductType type)
        {
            await QueryBuilderFactory.Create<ProductType>().UpdateAsync(type);
        }

        /// <summary>
        /// Создает изделие
        /// </summary>
        /// <param name="type"></param>
        [Transaction]
        public async Task CreateTypeAsync(ProductType type)
        {
            await QueryBuilderFactory.Create<ProductType>().InsertAsync(type);
        }

        /// <summary>
        /// Удаляет изделие
        /// </summary>
        /// <param name="type"></param>
        [Transaction]
        public async Task DeleteTypeAsync(ProductType type)
        {
            await QueryBuilderFactory.Create<ProductType>().DeleteAsync(type);
        }
    }
}
