﻿using DomainLogic.Infrastructure;
using DomainLogic.Models.Settings;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Services.Shared.Editors;
using DomainLogic.Services.Shared.Providers;
using DomainLogic.Models.Enumerations;
using DomainLogic.Interfaces.QueryBuilder;
using DomainLogic.Interfaces;

namespace DomainLogic.Services
{
    public class SettingsService: BaseService, ISettingsService
    {
        [Inject]
        public IRoleManagerWrap RoleManagerWrap { get; set; }

        [Inject]
        public UserRoleEditor UserRoleEditor { get; set; }

        [Inject]
        public OptionsProvider OptionsProvider { get; set; }

        [Inject]
        public ConfigValueParser ConfigValueParser { get; set; }

        private IQueryBuilder<Option> QueryBuilder => QueryBuilderFactory.Create<Option>();

        [Transaction]
        public async Task CreateDataOnInitApp()
        {
            List<string> roles = RoleManagerWrap.GetAllRoles();
            if (roles.Count > 0)
                return;

            await UserRoleEditor.CreateRoleAsync("Administrator");
            await UserRoleEditor.CreateRoleAsync("Manager");
            
            await QueryBuilder.InsertAsync(new Option { Name = "Localization.IsAllowLocalization", Title = "Включить локализацию", Value = "True", Type = OptionTypeEnum.Bool });

            await QueryBuilder.InsertAsync(new Option { Name = "ImagesLoader.NoPhoto", Title = "Нет фото", Value = "1", Type = OptionTypeEnum.Long });
            await QueryBuilder.InsertAsync(new Option { Name = "ImagesLoader.ImageMinDim", Title = "Минимальный размер картинки", Value = "600", Type = OptionTypeEnum.Int });
            await QueryBuilder.InsertAsync(new Option { Name = "ImagesLoader.ImageMinDimsByFolders", Title = "Переопределенный размер картинок", Value = "Partners=200;Posts=400;", Type = OptionTypeEnum.DictionaryOfInts });

            await QueryBuilder.InsertAsync(new Option { Name = "Sections.OtherSection.Id", Title = "Раздел 'Прочее'", Value = "0", Type = OptionTypeEnum.String });

            await QueryBuilder.InsertAsync(new Option { Name = "Products.PlatformFilterIds", Title = "Фильтры отчечающие за платформу", Value = "0", Type = OptionTypeEnum.ListOfLongs });
            await QueryBuilder.InsertAsync(new Option { Name = "Products.ArchiveState", Title = "Фильтры отчечающий за архив", Value = "0", Type = OptionTypeEnum.Long });

        }

        public async Task UpdateDataOnCreateAdmin(User admin)
        {
            await UserRoleEditor.AddRoleForUserAsync(admin.Email, "Administrator");
            await UserRoleEditor.AddRoleForUserAsync(admin.Email, "Manager");
            await QueryBuilder.InsertAsync(new Option { Name = "Admin.AdminId", Title = "Идентификатор админа", Value = admin.Id.ToString(), Type = OptionTypeEnum.Long, IsSystem = true });
            await QueryBuilder.InsertAsync(new Option { Name = "Admin.AdminEmail", Title = "E-mail админа", Value = admin.Email.ToString(), Type = OptionTypeEnum.String, IsSystem = true });

            await QueryBuilder.InsertAsync(new Option { Name = "ResumeMessages.To", Title = "Электронный адрес для отправки резюме", Value = admin.Email.ToString(), Type = OptionTypeEnum.String });

            await QueryBuilder.InsertAsync(new Option { Name = "Mailing.From", Title = "E-mail поддержки", Value = admin.Email.ToString(), Type = OptionTypeEnum.String });
            await QueryBuilder.InsertAsync(new Option { Name = "Manager.Emails", Title = "E-mail-ы менеджера", Value = admin.Email.ToString(), Type = OptionTypeEnum.ListOfStrings });


        }

        public void FillSettings(AppSettings settings)
        {
            List<Option> options = OptionsProvider.GetOptions();
            
            settings.Localization.IsAllowLocalization = GetOptionValueAsBool(options, "Localization.IsAllowLocalization", settings.Localization.IsAllowLocalization);
            settings.Admin.AdminId = GetOptionValueAsLong(options, "Admin.AdminId", settings.Admin.AdminId);
            settings.Admin.AdminEmail = GetOptionValueAsString(options, "Admin.AdminEmail", settings.Admin.AdminEmail);

            settings.ImagesLoader.NoPhoto = GetOptionValueAsLong(options, "ImagesLoader.NoPhoto", settings.ImagesLoader.NoPhoto);
            settings.ImagesLoader.ImageMinDim = GetOptionValueAsInt(options, "ImagesLoader.ImageMinDim", settings.ImagesLoader.ImageMinDim);
            settings.ImagesLoader.ImageMinDimsByFolders = GetOptionValueAsDictionaryOfInts(options, "ImagesLoader.ImageMinDimsByFolders", settings.ImagesLoader.ImageMinDimsByFolders);

            settings.Sections.OtherSection.Id = GetOptionValueAsLong(options, "Sections.OtherSection.Id", settings.Sections.OtherSection.Id);

            settings.Products.PlatformFilterIds = GetOptionValueAsListOfLongs(options, "Products.PlatformFilterIds", settings.Products.PlatformFilterIds);
            settings.Products.ArchiveState = GetOptionValueAsLong(options, "Products.ArchiveState", settings.Products.ArchiveState);

            settings.ResumeMessages.To = GetOptionValueAsString(options, "ResumeMessages.To", settings.ResumeMessages.To);

            settings.Mailing.From = GetOptionValueAsString(options, "Mailing.From", settings.Mailing.From);
            settings.Manager.Emails = GetOptionValueAsListOfStrings(options, "Manager.Emails", settings.Manager.Emails);
        }

        private string GetOptionValue(List<Option> options, string name)
        {
             return options.FirstOrDefault(x => x.Name == name)?.Value;
        }

        private string GetOptionValueAsString(List<Option> options, string name, string defaultValue)
        {
            string value = GetOptionValue(options, name);
            return ConfigValueParser.ParseValue(value, defaultValue);
        }

        private long GetOptionValueAsLong(List<Option> options, string name, long defaultValue)
        {
            string value = GetOptionValue(options, name);
            return ConfigValueParser.ParseValue(value, defaultValue);
        }

        private int GetOptionValueAsInt(List<Option> options, string name, int defaultValue)
        {
            string value = GetOptionValue(options, name);
            return ConfigValueParser.ParseValue(value, defaultValue);
        }

        private bool GetOptionValueAsBool(List<Option> options, string name, bool defaultValue)
        {
            string value = GetOptionValue(options, name);
            return ConfigValueParser.ParseValue(value, defaultValue);
        }

        private List<string> GetOptionValueAsListOfStrings(List<Option> options, string name, List<string> defaultValue)
        {
            string value = GetOptionValue(options, name);
            return ConfigValueParser.ParseValue(value, defaultValue);
        }

        private List<long> GetOptionValueAsListOfLongs(List<Option> options, string name, List<long> defaultValue)
        {
            string value = GetOptionValue(options, name);
            return ConfigValueParser.ParseValue(value, defaultValue);
        }

        private Dictionary<string, int> GetOptionValueAsDictionaryOfInts(List<Option> options, string name, Dictionary<string, int> defaultValue)
        {
            string value = GetOptionValue(options, name);
            return ConfigValueParser.ParseValue(value, defaultValue);
        }
    }
}
