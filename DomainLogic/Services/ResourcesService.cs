﻿using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Services
{
    public class ResourcesService : BaseService, IResourcesService
    {
        /// <summary>
        /// Получает все ресурсы
        /// </summary>
        /// <returns></returns>
        public List<Resource> GetResources()
        {
            return QueryBuilderFactory.Create<Resource>().ToList();
        }

        /// <summary>
        /// Получает ресурс по ключу
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public Resource GetResourceByKey(string key)
        {
            return QueryBuilderFactory.Create<Resource>()
                .FirstOrDefault(x => x.Key == key);
        }

        /// <summary>
        ///  Создает ресурс
        /// </summary>
        /// <param name="resource"></param>
        public async Task CreateResourceAsync(Resource resource)
        {
            await QueryBuilderFactory.Create<Resource>().InsertAsync(resource);
        }

        /// <summary>
        /// Обновляет ресурс
        /// </summary>
        /// <param name="resource"></param>
        public async Task UpdateResourceAsync(Resource resource)
        {
            await QueryBuilderFactory.Create<Resource>().UpdateAsync(resource);
        }

        /// <summary>
        /// Создает или обновляет ресурс
        /// </summary>
        /// <param name="resource"></param>
        public async Task CreateOrUpdateResourceAsync(Resource resource)
        {
            if (GetResourceByKey(resource.Key) == null)
                await CreateResourceAsync(resource);
            else
                await UpdateResourceAsync(resource);
        }
    }
}