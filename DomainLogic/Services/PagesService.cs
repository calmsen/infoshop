﻿using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Paging;

namespace DomainLogic.Services
{
    public class PagesService : BaseService, IPagesService
    {
        /// <summary>
        /// Получает список страниц
        /// </summary>
        /// <param name="pagination"></param>
        /// <returns></returns>
        public async Task<List<Page>> GetPagesAsync(Pagination pagination)
        {
            pagination.ItemsAmount = await QueryBuilderFactory.Create<Page>().CountAsync();
            if (pagination.ItemsAmount == 0)
                return new List<Page>();
            pagination.Refresh();
            return await QueryBuilderFactory.Create<Page>()
                .OrderByDescending(p => p.Id)
                .Skip(pagination.Offset)
                .Take(pagination.PageItemsAmount)
                .ToListAsync();
        }

        /// <summary>
        /// Получает страницу по идентификатору
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns></returns>
        public async Task<Page> GetPageByIdAsync(long pageId)
        {
            return await QueryBuilderFactory.Create<Page>()
                .Include(x => x.Description)
                .FirstOrDefaultAsync(x => x.Id == pageId);
        }

        /// <summary>
        /// Получает страницу по имени
        /// </summary>
        /// <param name="pageName"></param>
        /// <returns></returns>
        public async Task<Page> GetPageByNameAsync(string pageName)
        {
            return await QueryBuilderFactory.Create<Page>()
                .Include(x => x.Description)
                .FirstOrDefaultAsync(x => x.Name.Equals(pageName));
        }

        /// <summary>
        /// Обновляет страницу
        /// </summary>
        /// <param name="page"></param>
        [Transaction]
        public async Task EditPageAsync(Page page)
        {
            // обновляем или создаем описание страницы
            if (page.Description.Id > 0)
                await QueryBuilderFactory.Create<PageDescription>().UpdateAsync(page.Description);
            else if (!string.IsNullOrWhiteSpace(page.Description.Content))
            {
                await QueryBuilderFactory.Create<PageDescription>().InsertAsync(page.Description);
                page.DescriptionId = page.Description.Id;
            }
            else
                page.DescriptionId = null;
            // обновляем страницу
            await QueryBuilderFactory.Create<Page>().UpdateAsync(page);
        }

        /// <summary>
        /// Создает страницу
        /// </summary>
        /// <param name="page"></param>
        [Transaction]
        public async Task CreatePageAsync(Page page)
        {
            // создаем описание страницы
            if (!string.IsNullOrWhiteSpace(page.Description.Content))
            {
                await QueryBuilderFactory.Create<PageDescription>().InsertAsync(page.Description);
                page.DescriptionId = page.Description.Id;
            }
            else
                page.DescriptionId = null;
            // создаем страницы
            page.CreatedDate = DateTime.Now;
            await QueryBuilderFactory.Create<Page>().InsertAsync(page);
        }

        /// <summary>
        /// Удаляет страницу
        /// </summary>
        /// <param name="page"></param>
        [Transaction]
        public async Task DeletePageAsync(Page page)
        {
            await QueryBuilderFactory.Create<Page>().DeleteAsync(page);
        }
    }
}
