﻿using DomainLogic.Interfaces.QueryMappers;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Mappers;
using DomainLogic.Services.Shared.Editors;
using DomainLogic.Models.Enumerations;

namespace DomainLogic.Services
{

    public class ProductSnapshotsService: BaseService, IProductSnapshotsService
    {
        [Inject]
        public ProductSnapshotEditor ProductSnapshotEditor { get; set; }

        [Inject]
        public ProductMapper ProductMapper { get; set; }

        [Inject]
        public IProductSnapshotsQueryMapper ProductSnapshotsQueryMapper { get; set; }

        /// <summary>
        /// Получает снимки для определенного товара
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public async Task<List<ProductSnapshot>> GetProductSnapshotsAsync(long productId)
        {
            return await QueryBuilderFactory.Create<ProductSnapshot>()
                .Include(x => x.User)
                .ToListAsync(x => x.ProductId == productId);
        }

        /// <summary>
        /// Получает последние снимки по списку идентификаторов товаров
        /// </summary>
        /// <param name="productIds"></param>
        /// <returns></returns>
        public async Task<List<ProductSnapshot>> GetLastProductSnapshotsAsync(List<long> productIds)
        {
            List<ProductSnapshotInline> productSnapshots = await ProductSnapshotsQueryMapper.GetLastProductSnapshotsAsync(productIds);
            return ProductMapper.Map<List<ProductSnapshotInline>, List<ProductSnapshot>>(productSnapshots);
        }

        /// <summary>
        /// Получает снимок по идентификатору
        /// </summary>
        /// <param name="snapshotId"></param>
        /// <returns></returns>
        public async Task<ProductSnapshot> GetProductSnapshotAsync(long snapshotId)
        {
            return await QueryBuilderFactory.Create<ProductSnapshot>()
                .Include(x => x.User)
                .FirstOrDefaultAsync(x => x.Id == snapshotId);
        }

        /// <summary>
        /// Получает снимок для определенного товара
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public async Task<ProductSnapshot> GetLastProductSnapshotAsync(long productId)
        {
            return await QueryBuilderFactory.Create<ProductSnapshot>()
                .Join(new ProductSnapshot[0], x => x.Id, y => y.Id, (x, y) => new { ps1 = x, ps2 = y })
                .Where(z => z.ps1.ProductId == productId && z.ps1.CreatedDate > z.ps2.CreatedDate)
                .Select(z => z.ps1)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Создает снимок. Сначала создается клон снимка originalProductSnapshot.
        /// А затем переопределяются поля из fields, меняется productStates и UserId
        /// </summary>
        /// <param name="originalProductSnapshot"></param>
        /// <param name="fields"></param>
        /// <param name="productStates"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Transaction]
        public async Task<ProductSnapshot> CreateProductSnapshotAsync(ProductSnapshot originalProductSnapshot, Dictionary<string, object> fields, List<ProductStateEnum> productStates, int userId)
        {
            return await ProductSnapshotEditor.CreateProductSnapshotAsync(originalProductSnapshot, fields, productStates, userId);
        }

        /// <summary>
        /// Создает снимок
        /// </summary>
        /// <param name="product"></param>
        /// <param name="productStates"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Transaction]
        public async Task<ProductSnapshot> CreateProductSnapshotAsync(Product product, List<ProductStateEnum> productStates, int userId)
        {
            return await ProductSnapshotEditor.CreateProductSnapshotAsync(product, productStates, userId);
        }
    }
}
