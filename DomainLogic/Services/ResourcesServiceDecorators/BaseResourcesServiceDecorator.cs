﻿using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLogic.Services.ResourcesServiceDecorators
{
    public abstract class BaseResourcesServiceDecorator : IResourcesService
    {
        private readonly IResourcesService _resourcesService;

        public BaseResourcesServiceDecorator(IResourcesService resourcesService)
        {
            _resourcesService = resourcesService;
        }

        public virtual Task CreateOrUpdateResourceAsync(Resource resource)
        {
            return _resourcesService.CreateOrUpdateResourceAsync(resource);
        }

        public virtual Task CreateResourceAsync(Resource resource)
        {
            return _resourcesService.CreateResourceAsync(resource);
        }

        public virtual Resource GetResourceByKey(string key)
        {
            return _resourcesService.GetResourceByKey(key);
        }

        public virtual List<Resource> GetResources()
        {
            return _resourcesService.GetResources();
        }

        public virtual Task UpdateResourceAsync(Resource resource)
        {
            return _resourcesService.UpdateResourceAsync(resource);
        }
    }
}
