﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;

namespace DomainLogic.Services.ResourcesServiceDecorators
{
    public class CacheResourcesServiceDecorator : BaseResourcesServiceDecorator
    {
        private List<Resource> _cachedResources;
        public CacheResourcesServiceDecorator(IResourcesService resourcesService) : base(resourcesService)
        {
        }
        
        /// <summary>
        /// Получает все ресурсы
        /// </summary>
        /// <returns></returns>
        public override List<Resource> GetResources()
        {
            return GetCachedRersources();
        }

        /// <summary>
        /// Получает ресурс по ключу
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public override Resource GetResourceByKey(string key)
        {
            return GetCachedRersources().FirstOrDefault(x => x.Key == key);
        }

        /// <summary>
        ///  Создает ресурс
        /// </summary>
        /// <param name="resource"></param>
        public override async Task CreateResourceAsync(Resource resource)
        {
            if (GetResourceByKey(resource.Key) != null)
                throw new ArgumentException($"Ресурс с ключом {resource.Key} уже был создан.");
            await base.CreateResourceAsync(resource);
            GetCachedRersources().Add(resource);
        }

        /// <summary>
        /// Обновляет ресурс
        /// </summary>
        /// <param name="resource"></param>
        public override async Task UpdateResourceAsync(Resource resource)
        {
            Resource oldResource = GetResourceByKey(resource.Key);
            if (oldResource == null)
                throw new ArgumentException($"Ресурс с ключом {resource.Key} не существует.");
            await base.UpdateResourceAsync(resource);
            GetCachedRersources().Remove(oldResource);
            GetCachedRersources().Add(resource);

        }

        /// <summary>
        /// Создает или обновляет ресурс
        /// </summary>
        /// <param name="resource"></param>
        public override async Task CreateOrUpdateResourceAsync(Resource resource)
        {
            Resource oldResource = GetResourceByKey(resource.Key);
            if (oldResource == null)
                await CreateResourceAsync(resource);
            else
                await UpdateResourceAsync(resource);
        }

        private List<Resource> GetCachedRersources()
        {
            if (_cachedResources == null)
                _cachedResources = base.GetResources();
            return _cachedResources;
        }
    }
}
