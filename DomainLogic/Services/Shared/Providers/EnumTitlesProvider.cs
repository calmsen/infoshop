﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Interfaces;
using System.ComponentModel;
using System.Reflection;

namespace DomainLogic.Services.Shared.Providers
{
    /// <summary>
    /// Предоставляет названия для перечислений
    /// </summary>
    public class EnumTitlesProvider
    {
        /// <summary>
        /// Предоставляет ресурсы для текущей локали
        /// </summary>
        [Inject]
        public IResourceProvider ResourceProvider { get; set; }

        /// <summary>
        /// Получает название для перечисления
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        public string GetTitle<TEnum>(TEnum enumValue) where TEnum : struct
        {
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());
            if (fi == null)
                return null;
            object[] attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
            if (attrs == null || attrs.Length == 0)
                return null;
            string resourceName = ((DescriptionAttribute)attrs[0]).Description;
            return ResourceProvider.ReadResource(resourceName) ?? resourceName;
        }
    }
}
