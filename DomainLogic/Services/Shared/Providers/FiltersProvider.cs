﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainLogic.Services.Shared.Providers
{
    public class FiltersProvider: BaseService
    {
        [Inject]
        public SectionsProvider SectionsProvider { get; set; }

        /// <summary>
        /// Получает фильтр по наименованию(транслитерируемому) в разделе с идентификатором sectionId
        /// </summary>
        /// <param name="filterName"></param>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<DmFilter> GetFilterByNameAsync(string filterName, long sectionId)
        {
            Section section = await SectionsProvider.GetSectionByIdAsync(sectionId);
            List<long> filterIds = section
                .FiltersGroups
                .SelectMany(x => x.Filters)
                .Select(x => x.Id)
                .ToList();
            List<DmFilter> filters = await GetFiltersByIdsAsync(filterIds, false);
            DmFilter filter = filters
                .FirstOrDefault(x => x.Name.Equals(filterName));
            return filter;
        }


        /// <summary>
        /// Получает фильтры по списку идентификаторов
        /// </summary>
        /// <param name="filtersIds"></param>
        /// <param name="needIncludeValues">Нужно ли выбирать значения фильтров</param>
        /// <returns></returns>
        public async Task<List<DmFilter>> GetFiltersByIdsAsync(List<long> filtersIds, bool needIncludeValues = true)
        {
            if (filtersIds == null || filtersIds.Count == 0)
                return new List<DmFilter>();

            var query = QueryBuilderFactory.Create<DmFilter>();
            if (needIncludeValues)
                query = query.Include(x => x.Values);

            var filters = await query
                .ToListAsync(x => filtersIds.Contains(x.Id));
            filters.ForEach(z => z.Values.Sort((x, y) => x.Position.CompareTo(y.Position)));
            return filters;
        }
    }
}
