﻿using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLogic.Services.Shared.Providers
{
    public class ProductsProvider : BaseService
    {
        /// <summary>
        /// Получает товар по иденитификатору
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public async Task<Product> GetProductByIdAsync(long productId)
        {
            var dbProduct = await QueryBuilderFactory.Create<Product>()
                .Include(x => x.Section)
                .Include(x => x.Description)
                .Include(x => x.ProductsToImages.Select(y => y.Image))
                .Where(x => x.Id == productId)
                .FirstOrDefaultAsync();
            if (dbProduct == null)
                return null;

            dbProduct.Attributes = await QueryBuilderFactory.Create<DmAttribute>()
                .Include(x => x.Filter)
                .Include(x => x.Values)
                .Include(x => x.Values.Select(y => y.Value))
                .Where(x => x.ProductId == productId)
                .ToListAsync();
            //
            dbProduct.ProductsToPosts = await QueryBuilderFactory.Create<ProductToPost>()
                .Include(x => x.Post)
                .Where(x => x.ProductId == productId)
                .ToListAsync();
            //
            dbProduct.ProductsToImages.Sort((x, y) => x.Image.Position.CompareTo(y.Image.Position));
            return dbProduct;
        }
    }
}
