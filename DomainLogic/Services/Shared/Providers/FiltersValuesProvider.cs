﻿using DomainLogic.Infrastructure.Paging;
using DomainLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Services.Shared.Providers
{
    public class FiltersValuesProvider: BaseService
    {

        /// <summary>
        /// Получает значения по списку идентификаторов
        /// </summary>
        /// <param name="valuesIds"></param>
        /// <returns></returns>
        public async Task<List<FilterValue>> GetFiltersValuesAsync(List<long> valuesIds)
        {
            return await QueryBuilderFactory.Create<FilterValue>()
                .Where(x => valuesIds.Contains(x.Id))
                .ToListAsync();
        }

        /// <summary>
        /// Получает значения по списку параметров FilterParam
        /// </summary>
        /// <param name="fParams"></param>
        /// <returns></returns>
        public async Task<List<FilterValue>> GetFiltersValuesAsync(List<FilterParam> fParams)
        {
            // получим идишники значений
            List<long> valuesIds = new List<long>();
            foreach (FilterParam fp in fParams)
                valuesIds.AddRange(fp.Values);
            // -- -- --
            return await GetFiltersValuesAsync(valuesIds);
        }
    }
}
