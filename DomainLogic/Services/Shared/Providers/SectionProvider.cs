﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainLogic.Services.Shared.Providers
{
    public class SectionsProvider: BaseService
    {
        [Inject]
        public SectionsUtils SectionsUtils { get; set; }

        /// <summary>
        /// Получает раздел по идентификатору
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="needIncludeBanner"></param>
        /// <returns></returns>
        public async Task<Section> GetSectionByIdAsync(long sectionId, bool needIncludeBanner = false)
        {
            if (sectionId == 0)
                return null;

            var query = QueryBuilderFactory.Create<Section>();

            if (needIncludeBanner)
                query = query.Include(x => x.SectionsToImages.Select(y => y.Image));

            return await query
                .FirstOrDefaultAsync(s => s.Id == sectionId);
        }
        

        /// <summary>
        /// Получает коллекцию из потомков и себя по пути разделов
        /// </summary>
        /// <param name="pathOfSections"></param>
        /// <returns></returns>
        public async Task<List<Section>> GetDescendenOrSelfSectionsAsync(long pathOfSections)
        {
            long rightPath = SectionsUtils.GetRightPath(pathOfSections);
            return await QueryBuilderFactory.Create<Section>()
                .ToListAsync(x => x.PathOfSections >= pathOfSections && x.PathOfSections < rightPath);
        }
    }
}
