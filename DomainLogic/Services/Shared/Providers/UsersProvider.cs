﻿using DomainLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Services.Shared.Providers
{
    public class UsersProvider: BaseService
    {
        /// <summary>
        /// Получает пользователя по email-у
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<User> GetUserByEmailAsync(string email)
        {
            return await QueryBuilderFactory.Create<User>()
                .FirstOrDefaultAsync(x => x.Email == email);
        }

        /// <summary>
        /// Получает пользователей по списку идентификаторов
        /// </summary>
        /// <param name="usersIds"></param>
        /// <returns></returns>
        public async Task<List<User>> GetUsersByIdsAsync(List<int> usersIds)
        {
            return await QueryBuilderFactory.Create<User>()
                .ToListAsync(x => usersIds.Contains(x.Id));
        }

    }
}
