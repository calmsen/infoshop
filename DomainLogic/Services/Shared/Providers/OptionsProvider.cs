﻿using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLogic.Services.Shared.Providers
{
    public class OptionsProvider: BaseService
    {
        /// <summary>
        /// Получает все опции
        /// </summary>
        /// <returns></returns>
        public async Task<List<Option>> GetOptionsAsync()
        {
            return await QueryBuilderFactory.Create<Option>()
                .OrderBy(x => x.Title)
                .ToListAsync();
        }


        /// <summary>
        /// Получает все опции
        /// </summary>
        /// <returns></returns>
        public List<Option> GetOptions()
        {
            return QueryBuilderFactory.Create<Option>()
                .OrderBy(x => x.Title)
                .ToList();
        }
    }
}
