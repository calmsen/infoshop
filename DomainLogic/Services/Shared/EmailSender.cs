﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using DomainLogic.Models.Settings;
using DomainLogic.Infrastructure.Attributes;

namespace DomainLogic.Services.Shared
{
    public class EmailSender : BaseService
    {
        [Inject]
        public MailingSettings MailingSettings { get; set; }

        [Inject]
        public SmtpClientSettings SmtpClientSettings { get; set; }

        public void SendMessage(string from, List<string> to, string subject, string body, string[] files = null)
        {
            to = to.Where(x => !string.IsNullOrEmpty(x)).Distinct().ToList();

            using (SmtpClient smtpServer = new SmtpClient())
            using (MailMessage mail = new MailMessage())
            {
                try
                {
                    if (string.IsNullOrEmpty(from))
                        from = MailingSettings.From;
                    if (to == null)
                        to = new List<string>();

                    if (to.Count() == 0)
                        return;

                    mail.IsBodyHtml = true;
                    mail.From = new MailAddress(from);
                    foreach (string t in to)
                        mail.To.Add(t);
                    mail.Subject = subject;
                    mail.Body = body;

                    if (files != null) 
                        AttachFiles(mail, files);

                    smtpServer.Host = SmtpClientSettings.Host;
                    smtpServer.Port = SmtpClientSettings.Port;
                    smtpServer.EnableSsl = SmtpClientSettings.EnableSsl;

                    smtpServer.Credentials = new System.Net.NetworkCredential(SmtpClientSettings.Credentials.User, SmtpClientSettings.Credentials.Password);

                    smtpServer.Send(mail);

                }
                catch (Exception ex)
                {
                    ex = new Exception("Произошла ошибка при отправке сообщения на почту", ex);
                    Logger.Error(ex);
                    throw;
                }
            }
        }

        public void SendMessage(string from, string to, string subject, string body, string[] files = null)
        {
            List<string> toAsList = new List<string>();
            if (!string.IsNullOrEmpty(to))
                toAsList.Add(to);
            SendMessage(from, toAsList, subject, body);
        }

        public void SendMessage(string to, string subject, string body)
        {
            SendMessage(null, to, subject, body);
        }

        private void AttachFiles(MailMessage mail, string[] files) 
        {
            foreach (string file in files)
                mail.Attachments.Add(new Attachment(file, GetMediaType(file)));
        }

        private string GetMediaType(string file) 
        {
            string fileExt = Path.GetExtension(file).ToLower();
            switch (fileExt) 
            {
                case "pdf": return MediaTypeNames.Application.Pdf;
                case "rtf": return MediaTypeNames.Application.Rtf;
                case "zip": return MediaTypeNames.Application.Zip;
                case "gif": return MediaTypeNames.Image.Gif;
                case "jpeg": return MediaTypeNames.Image.Jpeg;
                case "jpg": return MediaTypeNames.Image.Jpeg;
                case "tiff": return MediaTypeNames.Image.Tiff;
                case "png": return "image/png";
                default: return MediaTypeNames.Application.Octet;

            }
        }
        
    }
}