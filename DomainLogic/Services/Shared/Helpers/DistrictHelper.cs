﻿using DomainLogic.Models;
using System;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Interfaces.QueryBuilder;

namespace DomainLogic.Services.Shared.Helpers
{
    public class DistrictHelper
    {
        [Inject]
        public IQueryBuilderFactory QueryBuilderFactory { get; set; }

        private IQueryBuilder<District> QueryBuilder => QueryBuilderFactory.Create<District>();

        /// <summary>
        /// Получает область по идишнику
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<District> GetDistrictByIdAsync(long id)
        {
            return await QueryBuilder
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        /// <summary>
        /// Получает область по наименованию
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<District> GetDistrictByNameAsync(string name)
        {
            return await QueryBuilder
                .FirstOrDefaultAsync(x => x.Name.Equals(name));
        }

        /// <summary>
        /// Создает область
        /// </summary>
        /// <param name="district"></param>
        public async Task CreateDistrictAsync(District district)
        {
            if (district.Id > 0)
                throw new ArgumentException("district.Id не должно быть больше нуля.");
            await QueryBuilder.InsertAsync(district);
        }

        /// <summary>
        /// Обновляет область
        /// </summary>
        /// <param name="district"></param>
        public async Task UpdateDistrictAsync(District district)
        {
            if (district.Id == 0)
                throw new ArgumentException("district.Id не должно быть равно нулю.");
            await QueryBuilder.UpdateAsync(district);
        }

        /// <summary>
        /// Обновляет данные по области, найдя соответсвие по наименованию
        /// </summary>
        /// <param name="district"></param>
        /// <returns></returns>
        public async Task<bool> UpdateDistrictByNameAsync(District district)
        {
            District exDistrict = await GetDistrictByNameAsync(district.Name);
            if (exDistrict == null)
                return false;

            if (district.Id == 0)
                district.Id = exDistrict.Id;
            else if (district.Id != exDistrict.Id)
                throw new ArgumentException("district.Id должно равняться exDistrict.Id");

            await UpdateDistrictAsync(district);
            return true;
        }

        /// <summary>
        /// Создает или обновляет данные по области
        /// </summary>
        /// <param name="district"></param>
        public async Task CreateOrUpdateDistrictAsync(District district)
        {
            if (district.Id == 0)
            {
                if (!(await UpdateDistrictByNameAsync(district)))
                    await CreateDistrictAsync(district);
            }
            else
            {
                await UpdateDistrictAsync(district);
            }
        }
    }
}