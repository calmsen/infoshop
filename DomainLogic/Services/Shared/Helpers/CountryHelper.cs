﻿using DomainLogic.Models;
using System;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Interfaces.QueryBuilder;

namespace DomainLogic.Services.Shared.Helpers
{
    public class CountryHelper
    {
        [Inject]
        public IQueryBuilderFactory QueryBuilderFactory { get; set; }

        private IQueryBuilder<Country> QueryBuilder => QueryBuilderFactory.Create<Country>();

        /// <summary>
        /// Получает страну по идишнику
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Country> GetCountryByIdAsync(long id) 
        {
            return await QueryBuilder
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        /// <summary>
        /// Получает страну по алиасу. Например: РФ
        /// </summary>
        /// <param name="alias"></param>
        /// <returns></returns>
        public async Task<Country> GetCountryByAliasAsync(string alias) 
        {
            return await QueryBuilder
                .FirstOrDefaultAsync(x => x.Alias.Equals(alias));
        }

        /// <summary>
        /// Создает страну
        /// </summary>
        /// <param name="country"></param>
        public async Task CreateCountryAsync(Country country) 
        {
            if (country.Id > 0)
                throw new ArgumentException("country.Id не должно быть больше нуля.");
            await QueryBuilder.InsertAsync(country);
        }

        /// <summary>
        /// Обновляет страну
        /// </summary>
        /// <param name="country"></param>
        public async Task UpdateCountryAsync(Country country)
        {
            if (country.Id == 0)
                throw new ArgumentException("country.Id не должно быть равно нулю.");
            await QueryBuilder.UpdateAsync(country);
        }

        /// <summary>
        /// Обновляет данные по стране, найдя соответсвие по алиасу
        /// </summary>
        /// <param name="country"></param>
        /// <returns></returns>
        public async Task<bool> UpdateCountryByAliasAsync(Country country) 
        {
            Country exCountry = await GetCountryByAliasAsync(country.Alias);
            if (exCountry == null)
                return false;
            
            if (country.Id == 0)
                country.Id = exCountry.Id;
            else if (country.Id != exCountry.Id)
                throw new ArgumentException("country.Id должно равняться exCountry.Id");

            await UpdateCountryAsync(country);
            return true;
        }

        /// <summary>
        /// Создает или обновляет данные по стране
        /// </summary>
        /// <param name="country"></param>
        public async Task CreateOrUpdateCountryAsync(Country country) 
        {
            if (country.Id == 0)
            {
                if (!(await UpdateCountryByAliasAsync(country)))
                    await CreateCountryAsync(country);
            }
            else 
            {
                await UpdateCountryAsync(country);
            }
        }
    }
}