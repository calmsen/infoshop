﻿using DomainLogic.Models;
using System;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Interfaces.QueryBuilder;

namespace DomainLogic.Services.Shared.Helpers
{
    public class RegionHelper
    {
        [Inject]
        public IQueryBuilderFactory QueryBuilderFactory { get; set; }

        private IQueryBuilder<Region> QueryBuilder => QueryBuilderFactory.Create<Region>();

        /// <summary>
        /// Получает регион по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Region> GetRegionByIdAsync(long id)
        {
            return await QueryBuilder.FirstOrDefaultAsync(x => x.Id == id);
        }

        /// <summary>
        /// Получает регион по наименованию
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<Region> GetRegionByNameAsync(string name)
        {
            return await QueryBuilder.FirstOrDefaultAsync(x => x.Name.Equals(name));
        }
        
        /// <summary>
        /// Создает регион
        /// </summary>
        /// <param name="region"></param>
        public async Task CreateRegionAsync(Region region)
        {
            if (region.Id > 0)
                throw new ArgumentException("region.Id не должно быть больше нуля.");
            await QueryBuilder.InsertAsync(region);
        }
        
        /// <summary>
        /// Обновляет регион
        /// </summary>
        /// <param name="region"></param>
        public async Task UpdateRegionAsync(Region region)
        {
            if (region.Id == 0)
                throw new ArgumentException("region.Id не должно быть равно нулю.");
            await QueryBuilder.UpdateAsync(region);
        }
        
        /// <summary>
        /// Обновляет данные по региону, найдя соответсвие по наименованию
        /// </summary>
        /// <param name="region"></param>
        /// <returns></returns>
        public async Task<bool> UpdateRegionByNameAsync(Region region)
        {
            Region exRegion = await GetRegionByNameAsync(region.Name);
            if (exRegion == null)
                return false;

            if (region.Id == 0)
                region.Id = exRegion.Id;
            else if (region.Id != exRegion.Id)
                throw new ArgumentException("region.Id должно равняться exRegion.Id");

            await UpdateRegionAsync(region);
            return true;
        }
        
        /// <summary>
        /// Создает или обновляет регион
        /// </summary>
        /// <param name="region"></param>
        public async Task CreateOrUpdateRegionAsync(Region region)
        {
            if (region.Id == 0)
            {
                if (!(await UpdateRegionByNameAsync(region)))
                    await CreateRegionAsync(region);
            }
            else
            {
                await UpdateRegionAsync(region);
            }
        }
    }
}