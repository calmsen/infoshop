﻿using DomainLogic.Models;
using System;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Interfaces.QueryBuilder;

namespace DomainLogic.Services.Shared.Helpers
{
    public class CityHelper
    {
        [Inject]
        public IQueryBuilderFactory QueryBuilderFactory { get; set; }

        private IQueryBuilder<City> QueryBuilder => QueryBuilderFactory.Create<City>();

        /// <summary>
        /// Получает город по идишнику
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<City> GetCityByIdAsync(long id)
        {
            return await QueryBuilder
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        /// <summary>
        /// Получает город по наименованию
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<City> GetCityByNameAsync(string name)
        {
            return await QueryBuilder
                .FirstOrDefaultAsync(x => x.Name.Equals(name));
        }

        /// <summary>
        /// Создает город
        /// </summary>
        /// <param name="city"></param>
        public async Task CreateCityAsync(City city)
        {
            if (city.Id > 0)
                throw new ArgumentException("city.Id не должно быть больше нуля.");
            await QueryBuilder.InsertAsync(city);
        }

        /// <summary>
        /// Обновляет город
        /// </summary>
        /// <param name="city"></param>
        /// 
        public async Task UpdateCityAsync(City city)
        {
            if (city.Id == 0)
                throw new ArgumentException("city.Id не должно быть равно нулю.");
            await QueryBuilder.UpdateAsync(city);
        }

        /// <summary>
        /// Обновляет данные по городу, найдя соответсвие по наименованию
        /// </summary>
        /// <param name="city"></param>
        /// <returns></returns>
        public async Task<bool> UpdateCityByNameAsync(City city)
        {
            City exCity = await GetCityByNameAsync(city.Name);
            if (exCity == null)
                return false;

            if (city.Id == 0)
                city.Id = exCity.Id;
            else if (city.Id != exCity.Id)
                throw new ArgumentException("city.Id должно равняться exCity.Id");

            await UpdateCityAsync(city);
            return true;
        }

        /// <summary>
        /// Создает или обновляет город
        /// </summary>
        /// <param name="city"></param>
        public async Task CreateOrUpdateCityAsync(City city)
        {
            if (city.Id == 0)
            {
                if (!(await UpdateCityByNameAsync(city)))
                    await CreateCityAsync(city);
            }
            else
            {
                await UpdateCityAsync(city);
            }
        }
    }
}