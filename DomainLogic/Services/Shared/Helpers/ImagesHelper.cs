﻿using System;
using System.Collections.Generic;

namespace DomainLogic.Services.Shared.Helpers
{
    public class ImagesHelper
    {
        public Dictionary<string, string> ImageTypesByFolders { get; set; }

        public ImagesHelper()
        {
            ImageTypesByFolders = new Dictionary<string, string>
            {
                { "Products", "DomainLogic.Models.Image, DomainLogic" },
                { "News", "DomainLogic.Models.ArticleImage, DomainLogic" },
                { "Posts", "DomainLogic.Models.PostImage, DomainLogic" },
                { "Sections", "DomainLogic.Models.SectionImage, DomainLogic" },
                { "Partners", "DomainLogic.Models.PartnerImage, DomainLogic" },
                { "Feedbacks", "DomainLogic.Models.FeedbackImage, DomainLogic" },
                { "Pages", "DomainLogic.Models.PageImage, DomainLogic" },
            };
        }

        public string GetFolder(Type type)
        {
            string folder = null;
            foreach (KeyValuePair<string, string> kv in ImageTypesByFolders)
                if (kv.Value.Contains(type.FullName))
                    folder = kv.Key;
            return folder;
        }

        public Type GetType(string folder)
        {
            return Type.GetType(ImageTypesByFolders[folder]);
        }
    }
}
