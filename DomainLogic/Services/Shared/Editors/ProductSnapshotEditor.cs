﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Models;
using DomainLogic.Models.Enumerations;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainLogic.Services.Shared.Editors
{
    public class ProductSnapshotEditor: BaseService
    {
        [Inject]
        public EnumUtils EnumUtils { get; set; }

        /// <summary>
        /// Создает снимок. Сначала создается клон снимка originalProductSnapshot.
        /// А затем переопределяются поля из fields, меняется productStates и UserId
        /// </summary>
        /// <param name="originalProductSnapshot"></param>
        /// <param name="fields"></param>
        /// <param name="productStates"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Transaction]
        public async Task<ProductSnapshot> CreateProductSnapshotAsync(ProductSnapshot originalProductSnapshot, Dictionary<string, object> fields, List<ProductStateEnum> productStates, int userId)
        {
            // создадим клон снимка
            ProductSnapshot cloneProductSnapshot = originalProductSnapshot.Map<ProductSnapshot>();
            cloneProductSnapshot.Id = 0;
            // переопределим некоторые поля снимка            
            ProductInfoForSnapshot snapshot = JsonConvert.DeserializeObject<ProductInfoForSnapshot>(cloneProductSnapshot.Snapshot);
            if (fields.ContainsKey("Title"))
                snapshot.Title = (string)fields["Title"];
            if (fields.ContainsKey("ShortFeatures"))
                snapshot.ShortFeatures = (string)fields["ShortFeatures"];
            if (fields.ContainsKey("Price"))
                snapshot.Price = (float)fields["Price"];
            if (fields.ContainsKey("SectionId"))
                snapshot.SectionId = (long)fields["SectionId"];
            if (fields.ContainsKey("Published"))
                snapshot.Published = (bool)fields["Published"];
            if (fields.ContainsKey("MainImageId"))
                snapshot.MainImageId = (long?)fields["MainImageId"];
            if (fields.ContainsKey("MainImageK"))
                snapshot.MainImageK = (double)fields["MainImageK"];
            if (fields.ContainsKey("Images"))
                snapshot.ImageIds = (fields["Images"] as List<Image>).Select(x => x.Id).ToList();
            cloneProductSnapshot.Snapshot = JsonConvert.SerializeObject(snapshot);
            // сохраним снимок
            cloneProductSnapshot.ProductStates = new ProductStates(EnumUtils.ListOfEnumsToInt(productStates));
            cloneProductSnapshot.UserId = userId;
            await QueryBuilderFactory.Create<ProductSnapshot>().InsertAsync(cloneProductSnapshot);
            return cloneProductSnapshot;
        }

        /// <summary>
        /// Создает снимок
        /// </summary>
        /// <param name="product"></param>
        /// <param name="productStates"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Transaction]
        public async Task<ProductSnapshot> CreateProductSnapshotAsync(Product product, List<ProductStateEnum> productStates, int userId)
        {
            if (product.Id == 0)
                throw new Exception("Снимок не может быть создан для товара с Id = 0.");

            ProductInfoForSnapshot snapshot = product.Map<ProductInfoForSnapshot>();
            ProductSnapshot productSnapshot = new ProductSnapshot
            {
                Snapshot = JsonConvert.SerializeObject(snapshot),
                ProductId = product.Id,
                UserId = userId,
                ProductStates = new ProductStates(EnumUtils.ListOfEnumsToInt(productStates)),
                CreatedDate = DateTime.Now
            };
            await QueryBuilderFactory.Create<ProductSnapshot>().InsertAsync(productSnapshot);
            return productSnapshot;
        }
    }
}
