﻿using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Interfaces;
using System.Threading.Tasks;

namespace DomainLogic.Services.Shared.Editors
{
    public class UserRoleEditor: BaseService
    {
        [Inject]
        public IRoleManagerWrap RoleManagerWrap { get; set; }

        public async Task AddRoleForUserAsync(string userName, string userRole)
        {
            await RoleManagerWrap.AddRoleForUserAsync(userName, userRole);
        }

        public async Task RemoveRoleFromUserAsync(string userName, string userRole)
        {
            await RoleManagerWrap.RemoveRoleFromUserAsync(userName, userRole);
        }

        public async Task CreateRoleAsync(string roleName)
        {
            await RoleManagerWrap.CreateRoleAsync(roleName);
        }
    }
}
