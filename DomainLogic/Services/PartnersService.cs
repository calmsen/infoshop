﻿using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Utils;

namespace DomainLogic.Services
{
    /// <summary>
    /// Сервис для работы с партнерами. Данный класс в отличие от класса репозитория может включать бизнес логику приложения. 
    /// Но в простейшем случае делегирует методы репозитрия.
    /// </summary>
    public class PartnersService : BaseService, IPartnersService
    {
        [Inject]
        public CommonUtils CommonUtils { get; set; }

        /// <summary>
        /// Получает список всех партнеров
        /// </summary>
        /// <returns></returns>
        public async Task<List<Partner>> GetPartnersAsync()
        {
            return await QueryBuilderFactory.Create<Partner>()
                .OrderBy(p => p.Position)
                .ToListAsync();
        }

        /// <summary>
        /// Получает партнеров с указанным количеством элементов на странице
        /// </summary>
        /// <param name="pagination"></param>
        /// <returns></returns>
        public async Task<List<Partner>> GetPartnersAsync(Pagination pagination)
        {
            pagination.ItemsAmount = await QueryBuilderFactory.Create<Partner>().CountAsync();
            if (pagination.ItemsAmount == 0)
                return new List<Partner>();
            pagination.Refresh();
            // получим список партнеров из бд
            return await QueryBuilderFactory.Create<Partner>()
                .OrderBy(p => p.Position)
                .Skip(pagination.Offset)
                .Take(pagination.PageItemsAmount)
                .ToListAsync();
        }

        /// <summary>
        /// Получает список партнеров по списку идентификаторов
        /// </summary>
        /// <param name="partnerIds"></param>
        /// <returns></returns>
        public async Task<List<Partner>> GetPartnersByIdsAsync(List<long> partnerIds)
        {
            return await QueryBuilderFactory.Create<Partner>()
                .ToListAsync(x => partnerIds.Contains(x.Id));
        }

        /// <summary>
        /// Получает информацию о партнере по идентификатору
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public async Task<Partner> GetPartnerByIdAsync(long partnerId)
        {
            return await QueryBuilderFactory.Create<Partner>()
                .FirstOrDefaultAsync(x => x.Id == partnerId);
        }

        /// <summary>
        /// Обновляет партнера
        /// </summary>
        /// <param name="partner"></param>
        [Transaction]
        public async Task EditPartnerAsync(Partner partner)
        {
            partner.Name = CommonUtils.ToLat(partner.Title);
            await QueryBuilderFactory.Create<Partner>().UpdateAsync(partner);
        }

        /// <summary>
        /// Создает партнера
        /// </summary>
        /// <param name="partner"></param>
        [Transaction]
        public async Task CreatePartnerAsync(Partner partner)
        {
            partner.Name = CommonUtils.ToLat(partner.Title);
            partner.CreatedDate = DateTime.Now;
            await QueryBuilderFactory.Create<Partner>().InsertAsync(partner);
        }

        /// <summary>
        /// Удаляет партнера
        /// </summary>
        /// <param name="partner"></param>
        [Transaction]
        public async Task DeletePartnerAsync(Partner partner)
        {
            await QueryBuilderFactory.Create<Partner>().DeleteAsync(partner);
        }
    }
}
