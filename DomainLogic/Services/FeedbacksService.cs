﻿using DomainLogic.Interfaces.Models;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DomainLogic.Infrastructure;
using DomainLogic.Services.Shared;
using DomainLogic.Models.Settings;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Infrastructure.Utils;
using DomainLogic.Services.Shared.Providers;
using DomainLogic.Infrastructure.Constants;
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Models.Messages;
using DomainLogic.Models.Enumerations;

namespace DomainLogic.Services
{
    public class FeedbacksService : BaseService, IFeedbacksService
    {
        [Inject]
        public UsersProvider UsersProvider { get; set; }
        [Inject]
        public EmailSender EmailSender { get; set; }
        [Inject]
        public StringUtils StringUtils { get; set; }
        [Inject]
        public CommonUtils CommonUtils { get; set; }
        [Inject]
        public IUrlUtils UrlUtils { get; set; }
        [Inject]
        public IRenderViewsUtils RenderViewsUtils { get; set; }

        [Inject]
        public FeedbackSettings FeedbackSettings { get; set; }
        [Inject]
        public MailingSettings MailingSettings { get; set; }

        /// <summary>
        /// Выбирает пользователей и устанавливает для указанных сообщений
        /// </summary>
        /// <param name="feedbacks"></param>
        private async Task LoadFeedbackUsersAsync(List<Feedback> feedbacks)
        {
            List<int> userIds = feedbacks.Select(x => x.UserId).ToList();
            List<User> users = await UsersProvider.GetUsersByIdsAsync(userIds);
            foreach (User u in users)
            {
                foreach (Feedback f in feedbacks)
                {
                    if (f.UserId == u.Id)
                    {
                        f.User = u;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Выбирает теги и устанавливает для указанных сообщений
        /// </summary>
        /// <param name="feedbacks"></param>
        private async Task LoadFeedbackTagsAsync(List<Feedback> feedbacks)
        {
            List<long> feedbackIds = feedbacks.Select(x => x.Id).ToList();
            List<FeedbackTagContainer> tags = await GetFeedbackTagsAsync(feedbackIds);
            foreach (FeedbackTagContainer t in tags)
                foreach (Feedback f in feedbacks)
                    if (f.Id == t.FeedbackId)
                    {
                        f.FeedbacksToTags = t.Tags.Select(x => new FeedbackToTag { Tag = x }).ToList();
                        break;
                    }
        }

        #region Feedbacks
        /// <summary>
        /// Получает список сообщений в соотвествии с фильтром filter
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<List<Feedback>> GetFeedbacksAsync(Pagination pagination, IFeedbackListFilter filter)
        {
            var query = QueryBuilderFactory.Create<Feedback>();
            // добавим условия в запрос если поле Search не пустое
            if (!string.IsNullOrWhiteSpace(filter.Search) && !filter.Search.Equals("null"))
            {
                string search = StringUtils.SqlEscape(filter.Search.Trim());
                // делаем поиск по идишнику если распарсится в число
                if (new Regex(@"^\d+$").IsMatch(search))
                {
                    long feedbackId = Convert.ToInt64(search);
                    query = query.Where(x => x.Id == feedbackId);
                }
                // делаем поиск по email если распарсится в e-mail
                else if (new Regex(Patterns.Email).IsMatch(search))
                    query = query.Where(x => x.User.Email == search || x.Answers.Any(y => y.User.Email.Equals(search)));
                // делаем поиск по тегам если строка содержит запятую
                else if (Regex.Matches(search, ",").Count >= 1)
                {
                    string[] tags = search.Split(',').Select(x => x.Trim()).ToArray();
                    query = query.Where(x => tags.All(y => x.FeedbacksToTags.Any(z => z.Tag.Title.Equals(y))));
                }
                // иначе делаем поиск по именам пользователей и по одному тегу
                else
                    query = query.Where(x => x.User.FullName.Contains(search) || x.FeedbacksToTags.Any(y => y.Tag.Title == search));
            }
            // добавим условия по дате
            if (filter.DateFrom.Year > 1900 && filter.DateFrom.Year < 2100)
                query = query.Where(x => x.CreatedDate >= filter.DateFrom);
            if (filter.DateTo.Year > 1900 && filter.DateTo.Year < 2100)
                query = query.Where(x => x.CreatedDate <= filter.DateTo);
            // добавим условия по таким полям как SectionId, ProductId, Theme и State
            if (filter.SectionId != null)
                query = query.Where(x => x.SectionId == filter.SectionId);
            if (filter.ProductId != null)
                query = query.Where(x => x.ProductId == filter.ProductId);
            if (filter.Theme != null)
                query = query.Where(x => (int)x.Theme == (int)filter.Theme);
            if (filter.State != StateEnum.None)
                query = query.Where(x => x.State == filter.State);
            // посчитаем количество возможных записей
            pagination.ItemsAmount = await query.CountAsync();
            if (pagination.ItemsAmount == 0)
                return new List<Feedback>();
            pagination.Refresh();
            // получим список сообщений по данным условиям
            List<Feedback> feedbacks = (await query.OrderByDescending(x => x.Id)
                .Skip(pagination.Offset)
                .Take(pagination.PageItemsAmount)
                .ToListAsync());
            // выберем пользователей
            await LoadFeedbackUsersAsync(feedbacks);
            // выберем теги
            await LoadFeedbackTagsAsync(feedbacks);
            return feedbacks;
        }

        /// <summary>
        /// Получает список сообщений автора
        /// </summary>
        /// <param name="userEmail"></param>
        /// <returns></returns>
        public async Task<List<Feedback>> GetMyFeedbacksAsync(string userEmail)
        {
            return await QueryBuilderFactory.Create<Feedback>()
                .ToListAsync(x => x.User.Email.Equals(userEmail));
        }

        /// <summary>
        /// Получает сообщение по идишнику
        /// </summary>
        /// <param name="feedbackId"></param>
        /// <returns></returns>
        public async Task<Feedback> GetFeedbackByIdAsync(long feedbackId)
        {
            // получим сообщение
            var dbFeedback = await QueryBuilderFactory.Create<Feedback>()
                .Include(x => x.User)
                .Include(x => x.UserWhoSetTheState)
                .Include(x => x.Section)
                .Include(x => x.Product)
                .Include(x => x.Answers)
                .Include(x => x.Answers.Select(y => y.User))
                .FirstOrDefaultAsync(x => x.Id == feedbackId);
            if (dbFeedback == null)
                return null;
            // сортируем ответы в сообщении
            dbFeedback.Answers.Sort((x, y) => x.Id.CompareTo(y.Id));
            // получим теги, приглашения и картинки для данного сообщения
            dbFeedback.FeedbacksToTags = await QueryBuilderFactory.Create<FeedbackToTag>()
                .Include(x => x.Tag)
                .ToListAsync(x => x.FeedbackId == feedbackId);
            dbFeedback.FeedbackInvitings = await QueryBuilderFactory.Create<FeedbackInviting>()
                .ToListAsync(x => x.FeedbackId == feedbackId);
            dbFeedback.FeedbacksToImages = await QueryBuilderFactory.Create<FeedbackToImage>()
                .Include(x => x.Image)
                .ToListAsync(x => x.FeedbackId == feedbackId);
            return dbFeedback;
        }

        /// <summary>
        /// Создает сообщение
        /// </summary>
        /// <param name="feedback"></param>
        [Transaction]
        public async Task CreateFeedbackAsync(Feedback feedback)
        {
            string rememberUserFullName = feedback.User.FullName;
            if (feedback.User.Id == 0)
            {
                // проверим есnть ли такой пользователь, если нет то создадим
                User findedUser = await UsersProvider.GetUserByEmailAsync(feedback.User.Email);
                if (findedUser != null)
                    feedback.User = findedUser;
                else
                    await QueryBuilderFactory.Create<User>().InsertAsync(feedback.User);
            }
            // обновим имя пользователя если его раньше не было
            if (string.IsNullOrEmpty(feedback.User.FullName) && !string.IsNullOrEmpty(rememberUserFullName))
            {
                feedback.User.FullName = rememberUserFullName;
                await QueryBuilderFactory.Create<User>().UpdateAsync(feedback.User);
            }

            feedback.UserWhoSetTheState = feedback.User;

            // если идентификатор пользователя(автора) равен нулю, 
            // то пробуем взять его из объекта
            if (feedback.User != null && feedback.UserId == 0)
                feedback.UserId = feedback.User.Id;
            // если идентификатор пользователя(установившего статус) равен нулю, 
            // то пробуем взять его из объекта
            if (feedback.UserWhoSetTheState != null && feedback.UserIdWhoSetTheState == 0)
                feedback.UserIdWhoSetTheState = feedback.UserWhoSetTheState.Id;
            // устанавливаем дату и статус по умолчанию
            feedback.CreatedDate = DateTime.Now;
            feedback.State = StateEnum.New;
            // создаем сообщение
            await QueryBuilderFactory.Create<Feedback>().InsertAsync(feedback);
            // привязывем картинки к сообщению
            List<long> imageIds = feedback.FeedbacksToImages.Select(x => x.ImageId).ToList();
            await AddFeedbackImagesAsync(feedback.Id, imageIds);

            // send notification to the author of the message
            string feedbackUrl = UrlUtils.GetBaseUrl() + UrlUtils.UrlAction("Show", "Feedback", new { id = feedback.Id, hash = CommonUtils.GetMd5Hash(feedback.Id + feedback.User.Email + FeedbackSettings.Md5HahSolt), email = feedback.User.Email }).Substring(1);

            string subject = feedback.Theme.GetTitle();
            string body = RenderViewsUtils.RenderPartialToString("~/Views/_Messages/_CreateFeedbackMessage.cshtml", new CreateFeedbackMessage { Feedback = feedback, FeedbackUrl = feedbackUrl });
            EmailSender.SendMessage(null, feedback.User.Email, subject, body);
            // send notification to support if support is not the author of the feedback
            if (!feedback.User.Email.Equals(MailingSettings.From))
            {
                feedbackUrl = UrlUtils.GetBaseUrl() + UrlUtils.UrlAction("Show", "Feedback", new { id = feedback.Id }).Substring(1);
                body = RenderViewsUtils.RenderPartialToString("~/Views/_Messages/_NotifyFeedbackMessage.cshtml", new NotifyFeedbackMessage { Feedback = feedback, FeedbackUrl = feedbackUrl });
                EmailSender.SendMessage(null, MailingSettings.From, subject, body);
            }
        }
        
        /// <summary>
        /// Обновляет привязки картинок к сообщению
        /// </summary>
        /// <param name="feedback"></param>
        [Transaction]
        public async Task UpdateFeedbackImagesAsync(Feedback feedback)
        {
            // отвязываем старые картинки от сообщения
            await DeleteFeedbackImagesAsync(feedback.Id);
            // привязываем новые картинки к сообщению
            List<long> imageIds = feedback.FeedbacksToImages.Select(x => x.ImageId).ToList();
            await AddFeedbackImagesAsync(feedback.Id, imageIds);
        }

        /// <summary>
        /// Изменяет состояние сообщения
        /// </summary>
        /// <param name="feedbackId">Идентификатор сообщения</param>
        /// <param name="state">Состояние, которое нужно установить</param>
        /// <param name="userId">Идентификатор пользователя, который устанавливает состояние</param>
        [Transaction]
        public async Task ChangeStateAsync(long feedbackId, StateEnum state, long userId)
        {
            await QueryBuilderFactory.Create<Feedback>().UpdateAsync(new { State = (int)state, UserIdWhoSetTheState = userId }, new { Id = feedbackId });
        }

        /// <summary>
        /// Изменить указанную модель
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="productId"></param>
        /// <param name="feedbackId"></param>
        [Transaction]
        public async Task ChangeModelAsync(long? sectionId, long? productId, long feedbackId)
        {
            await QueryBuilderFactory.Create<Feedback>().UpdateAsync(new { SectionId = sectionId, ProductId = productId }, new { Id = feedbackId });
        }

        /// <summary>
        /// Привязывает картинки к сообщению
        /// </summary>
        /// <param name="feedbackId"></param>
        /// <param name="imageIds"></param>
        private async Task AddFeedbackImagesAsync(long feedbackId, List<long> imageIds)
        {
            for (int i = 0; i < imageIds.Count; i++)
            {
                await QueryBuilderFactory.Create<FeedbackToImage>().InsertAsync(new FeedbackToImage { FeedbackId = feedbackId, ImageId = imageIds[i] });
            }
        }

        /// <summary>
        /// Отвязывает картинки от сообщения
        /// </summary>
        /// <param name="feedbackId"></param>
        private async Task DeleteFeedbackImagesAsync(long feedbackId)
        {
            await QueryBuilderFactory.Create<FeedbackToImage>().DeleteAsync(new { FeedbackId = feedbackId });
        }
        #endregion

        #region FeedbackAnswers
        /// <summary>
        /// Получает список сообщений в соотвествии с фильтром filter
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<List<FeedbackAnswer>> GetFeedbackAnswersAsync(Pagination pagination, IFeedbackAnswerListFilter filter)
        {
            var query = QueryBuilderFactory.Create<FeedbackAnswer>();
            // добавим условия в запрос если поле Search не пустое
            if (!string.IsNullOrWhiteSpace(filter.Search) && !filter.Search.Equals("null"))
            {
                string search = StringUtils.SqlEscape(filter.Search.Trim());
                // делаем поиск по идишнику если распарсится в число
                if (new Regex(@"^\d+$").IsMatch(search))
                {
                    long feedbackId = Convert.ToInt64(search);
                    query = query.Where(x => x.Id == feedbackId);
                }
                // делаем поиск по email если распарсится в e-mail
                else if (new Regex(Patterns.Email).IsMatch(search))
                    query = query.Where(x => x.User.Email == search);
                // иначе делаем поиск по именам пользователей и по одному тегу
                else
                    query = query.Where(x => x.User.FullName.Contains(search));
            }
            // добавим условия по дате
            if (filter.DateFrom.Year > 1900 && filter.DateFrom.Year < 2100)
                query = query.Where(x => x.CreatedDate >= filter.DateFrom);
            if (filter.DateTo.Year > 1900 && filter.DateTo.Year < 2100)
                query = query.Where(x => x.CreatedDate <= filter.DateTo);
            // посчитаем количество возможных записей
            pagination.ItemsAmount = await query.CountAsync();
            if (pagination.ItemsAmount == 0)
                return new List<FeedbackAnswer>();
            pagination.Refresh();
            // получим список сообщений по данным условиям
            return await query
                .Include(x => x.User)
                .OrderByDescending(x => x.Id)
                .Skip(pagination.Offset)
                .Take(pagination.PageItemsAmount)
                .ToListAsync();
        }
        
        /// <summary>
        /// Получает ответ по идентификатору
        /// </summary>
        /// <param name="answerId"></param>
        /// <returns></returns>
        public async Task<FeedbackAnswer> GetFeedbackAnswerAsync(long answerId)
        {
            return await QueryBuilderFactory.Create<FeedbackAnswer>()
                .Include(x => x.Feedback)
                .FirstOrDefaultAsync(x => x.Id == answerId);
        }

        /// <summary>
        /// Создает ответ 
        /// </summary>
        /// <param name="feedbackAnswer"></param>
        /// <param name="needChangeState"></param>
        /// <param name="needPickUp"></param>
        /// <returns></returns>
        [Transaction]
        public async Task CreateFeedbackAnswerAsync(FeedbackAnswer feedbackAnswer, bool needChangeState, bool needPickUp)
        {
            bool isChangeStateMessage = false;
            if (feedbackAnswer.Message == null && needChangeState)
            {
                feedbackAnswer.Message = feedbackAnswer.User.Email + " сменил статус на " + feedbackAnswer.Feedback.State.GetTitle();
                isChangeStateMessage = true;
            }
            if (feedbackAnswer.Message == null)
                return;
            // Создаем ответ: 

            // если идентификатор пользователя равен нулю, 
            // то пробуем взять его из объекта
            if (feedbackAnswer.User != null && feedbackAnswer.UserId == 0)
                feedbackAnswer.UserId = feedbackAnswer.User.Id;
            // задаем дату создания по умолчанию и создаем ответ
            feedbackAnswer.CreatedDate = DateTime.Now;
            await QueryBuilderFactory.Create<FeedbackAnswer>().InsertAsync(feedbackAnswer);
            // увеличим кол-ва ответов в сообщении
            await IncreaseFeedbackAnswersAmountAsync(feedbackAnswer.Feedback.Id);

            // 
            if (needChangeState)
            {
                await ChangeStateAsync(feedbackAnswer.Feedback.Id, feedbackAnswer.Feedback.State, feedbackAnswer.User.Id);
            }
            else if (needPickUp)
            {
                feedbackAnswer.Feedback.State = StateEnum.New;
                await ChangeStateAsync(feedbackAnswer.Feedback.Id, feedbackAnswer.Feedback.State, feedbackAnswer.User.Id);
            }

            if (isChangeStateMessage)
                return;

            // send notifying to email    
            Feedback feedback = await GetFeedbackByIdAsync(feedbackAnswer.Feedback.Id);
            string subject = feedback.Theme.GetTitle(); 
            string feedbackUrl, body;
            string hash = CommonUtils.GetMd5Hash(feedback.Id + feedback.User.Email + FeedbackSettings.Md5HahSolt);
            // send notification to the author of the message
            if (!feedbackAnswer.User.Email.Equals(MailingSettings.From) || (feedbackAnswer.User.Email.Equals(MailingSettings.From)))
            {
                if (feedbackAnswer.User.Email.Equals(feedback.User.Email))
                    feedbackUrl = UrlUtils.GetBaseUrl() + UrlUtils.UrlAction("Show", "Feedback", new { id = feedback.Id, hash = hash, email = feedback.User.Email }).Substring(1);
                else
                    feedbackUrl = UrlUtils.GetBaseUrl() + UrlUtils.UrlAction("Show", "Feedback", new { id = feedback.Id }).Substring(1);

                body = RenderViewsUtils.RenderPartialToString("~/Views/_Messages/_CreateFeedbackAnswerMessage.cshtml", new CreateFeedbackAnswerMessage { FeedbackAnswer = feedbackAnswer, FeedbackUrl = feedbackUrl });
                EmailSender.SendMessage(null, feedbackAnswer.User.Email, subject, body);
            }
            // send notification to the author of the feedback
            if (!feedbackAnswer.User.Email.Equals(feedback.User.Email) && feedbackAnswer.NoticedAuthor) 
            {
                feedbackUrl = UrlUtils.GetBaseUrl() + UrlUtils.UrlAction("Show", "Feedback", new { id = feedback.Id, hash = hash, email = feedback.User.Email }).Substring(1);
                body = RenderViewsUtils.RenderPartialToString("~/Views/_Messages/_NotifyFeedbackAnswerMessage.cshtml", new NotifyFeedbackAnswerMessage { FeedbackAnswer = feedbackAnswer, FeedbackUrl = feedbackUrl });
                EmailSender.SendMessage(null, feedback.User.Email, subject, body);
            }
            // send notification to other members except the author of the feedback
            feedbackUrl = UrlUtils.GetBaseUrl() + UrlUtils.UrlAction("Show", "Feedback", new { id = feedback.Id }).Substring(1);
            body = RenderViewsUtils.RenderPartialToString("~/Views/_Messages/_NotifyFeedbackAnswerMessage.cshtml", new NotifyFeedbackAnswerMessage { FeedbackAnswer = feedbackAnswer, FeedbackUrl = feedbackUrl });

            List<string> emails = feedback.Answers
                .Where(x => !x.User.Email.Equals(feedback.User.Email))
                .Where(x => !x.User.Email.Equals(MailingSettings.From))
                .Select(x => x.User.Email)
                .Distinct().ToList();
            // send notification to support if support is not the author of the feedback and if support is not the author of the message
            if (!feedback.User.Email.Equals(MailingSettings.From) && !feedbackAnswer.User.Email.Equals(MailingSettings.From))
                emails.Add(MailingSettings.From);
                
            EmailSender.SendMessage(null, emails, subject, body);
        }

        /// <summary>
        /// Получает ответы по идентификатору сообщения
        /// </summary>
        /// <param name="feedbackId"></param>
        /// <returns></returns>
        private async Task<List<FeedbackAnswer>> GetFeedbackAnswersAsync(long feedbackId)
        {
            return await QueryBuilderFactory.Create<FeedbackAnswer>()
                .ToListAsync(x => x.Feedback.Id == feedbackId);
        }

        /// <summary>
        /// Увеличивает счетчик у сообщения
        /// </summary>
        /// <param name="feedbackId"></param>
        private async Task IncreaseFeedbackAnswersAmountAsync(long feedbackId)
        {
            await QueryBuilderFactory.Create<Feedback>().UpdateAsync(new { AnswersAmount = "(RAW)AnswersAmount + 1" }, new { Id = feedbackId });
        }
        #endregion

        #region FeedbackThemes
        /// <summary>
        /// Изменяет тему сообщения
        /// </summary>
        /// <param name="theme"></param>
        /// <param name="feedbackId"></param>
        /// <param name="userId"></param>
        [Transaction]
        public async Task ChangeThemeAsync(FeedbackThemeEnum theme, long feedbackId, int userId)
        {
            Feedback feedback = await GetFeedbackByIdAsync(feedbackId);

            // создаем ответ
            string message = string.Format("Заявка перенесена из \"{0}\" в \"{1}\".",
                feedback.Theme.GetTitle(),
                theme.GetTitle()
            );
            FeedbackAnswer feedbackAnswer = new FeedbackAnswer
            {
                Message = message,
                CreatedDate = DateTime.Now,
                FeedbackId = feedbackId,
                UserId = userId,
                NoticedAuthor = false
            };
            await QueryBuilderFactory.Create<FeedbackAnswer>().InsertAsync(feedbackAnswer);
            // обновляем сообщение
            await QueryBuilderFactory.Create<Feedback>().UpdateAsync(new { ThemeId = (int)theme, AnswersAmount = "(RAW)AnswersAmount + 1" }, new { Id = feedbackId });

            // send notifying to email
            string feedbackUrl = UrlUtils.GetBaseUrl() + UrlUtils.UrlAction("Show", "Feedback", new { id = feedback.Id }).Substring(1);
            string subject = feedback.Theme.GetTitle();
            string body = RenderViewsUtils.RenderPartialToString("~/Views/_Messages/_ChangeFeedbackThemeMessage.cshtml", new ChangeFeedbackThemeMessage { Feedback = feedback, FeedbackUrl = feedbackUrl });
            List<string> emails = feedback.Answers
                .Where(x => !x.User.Email.Equals(feedback.User.Email))
                .Where(x => !x.User.Email.Equals(MailingSettings.From))
                .Select(x => x.User.Email)
                .Distinct().ToList();
            emails.Add(MailingSettings.From);

            EmailSender.SendMessage(null, emails, subject, body);
        }
        #endregion

        #region FeedbackTags
        /// <summary>
        /// Поиск тега по строке запроса
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<List<FeedbackTag>> SearchTagsAsync(string query)
        {
            return await QueryBuilderFactory.Create<FeedbackTag>()
                .Where(x => x.Title.Contains(query))
                .Take(30)
                .ToListAsync();
        }

        /// <summary>
        /// Создает тег и привязывает его к сообщению
        /// </summary>
        /// <param name="tag"></param>
        [Transaction]
        public async Task CreateFeedbackTagAsync(FeedbackTag tag)
        {
            // получим существующий тег по заголовку
            FeedbackTag exTag = await GetFeedbackTagAsync(tag.Title);
            // получим существующее сообщение
            long feedbackId = tag.FeedbacksToTags[0].FeedbackId;
            Feedback exFeedback = await QueryBuilderFactory.Create<Feedback>()
                .FirstOrDefaultAsync(x => x.Id == feedbackId);
            if (exFeedback == null)
                throw new Exception();

            exFeedback.FeedbacksToTags = await QueryBuilderFactory.Create<FeedbackToTag>()
                .Include(x => x.Tag)
                .ToListAsync(x => x.FeedbackId == feedbackId);

            // находим в существующем сообщение привязанный тег.
            // если таковой есть то его не создаем тег
            FeedbackTag bndTag = exFeedback.FeedbacksToTags
                .Where(x => x.Tag.Title.Equals(tag.Title))
                .Select(x => x.Tag)
                .FirstOrDefault();
            if (bndTag != null)
                return;
            // если нет еше такого тега, то создаем тег.
            // иначе просто присваиваемый существующий тег
            if (exTag == null)
                await QueryBuilderFactory.Create<FeedbackTag>().InsertAsync(tag);
            else
                tag.Id = exTag.Id;
            // делаем привязку тега к сообщению
            await QueryBuilderFactory.Create<FeedbackToTag>().InsertAsync(new FeedbackToTag { FeedbackId = feedbackId, TagId = tag.Id });
        }

        /// <summary>
        /// Отвязывает тег от сообщения. Сам тег не удаляется
        /// </summary>
        /// <param name="feedbackId"></param>
        /// <param name="tagTitle"></param>
        [Transaction]
        public async Task DeleteFeedbackTagAsync(long feedbackId, string tagTitle)
        {
            FeedbackTag exTag = await GetFeedbackTagAsync(tagTitle);
            if (exTag == null)
                return;
            await QueryBuilderFactory.Create<FeedbackToTag>().DeleteAsync(new FeedbackToTag { FeedbackId = feedbackId, TagId = exTag.Id });
        }

        /// <summary>
        /// Получает теги для сообщений обратной связи
        /// </summary>
        /// <param name="feedbackIds"></param>
        /// <returns></returns>
        private async Task<List<FeedbackTagContainer>> GetFeedbackTagsAsync(List<long> feedbackIds)
        {

            var items = await QueryBuilderFactory.Create<FeedbackToTag>()
                .Include(x => x.Tag)
                .ToListAsync(x => feedbackIds.Contains(x.FeedbackId));

            List<FeedbackTagContainer> tagContainers = new List<FeedbackTagContainer>();
            foreach (var item in items)
            {
                FeedbackTagContainer tagContainer = tagContainers.FirstOrDefault(x => x.FeedbackId == item.FeedbackId);
                if (tagContainer == null)
                {
                    tagContainer = new FeedbackTagContainer
                    {
                        FeedbackId = item.FeedbackId,
                        Tags = new List<FeedbackTag>()
                    };
                    tagContainers.Add(tagContainer);
                }

                tagContainer.Tags.Add(item.Tag);
            }
            return tagContainers;
        }

        /// <summary>
        /// Получает тег по наименованию
        /// </summary>
        /// <param name="tagTitle"></param>
        /// <returns></returns>
        private async Task<FeedbackTag> GetFeedbackTagAsync(string tagTitle)
        {
            return await QueryBuilderFactory.Create<FeedbackTag>()
                .FirstOrDefaultAsync(x => x.Title.Equals(tagTitle));
        }
        #endregion

        #region FeedbackInvitings
        /// <summary>
        /// Создает приглашения к обсуждению сообщения
        /// </summary>
        /// <param name="feedbackId">Идентификатор сообщения</param>
        /// <param name="emails">e-mail-ы пользователей</param>
        /// <param name="message"></param>
        /// <returns></returns>
        [Transaction]
        public async Task InviteMessageAsync(long feedbackId, List<string> emails, string message)
        {
            FeedbackInviting inviting = new FeedbackInviting
            {
                FeedbackId = feedbackId,
                Emails = string.Join(",", emails)
            };
            await QueryBuilderFactory.Create<FeedbackInviting>().InsertAsync(inviting);
            Feedback feedback = await GetFeedbackByIdAsync(feedbackId);
            string feedbackUrl = UrlUtils.GetBaseUrl() + UrlUtils.UrlAction("Show", "Feedback", new { id = feedbackId, hash = CommonUtils.GetMd5Hash(feedback.Id + FeedbackSettings.Md5HahSolt) }).Substring(1);
            string subject = feedback.Theme.GetTitle();
            string body = RenderViewsUtils.RenderPartialToString("~/Views/_Messages/_FeedbackInviteMessage.cshtml", new FeedbackInviteMessage { FeedbackUrl = feedbackUrl, Message = message });

            EmailSender.SendMessage(null, emails, subject, body);
        }
        #endregion

    }
}