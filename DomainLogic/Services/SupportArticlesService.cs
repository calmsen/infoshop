﻿using DomainLogic.Infrastructure;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Infrastructure.Paging;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Services
{
    public class SupportArticlesService : BaseService, ISupportArticlesService
    {
        /// <summary>
        /// Получаем список статей
        /// </summary>
        /// <param name="pagination"></param>
        /// <returns></returns>
        public async Task<List<SupportArticle>> GetArticlesAsync(Pagination pagination)
        {
            pagination.ItemsAmount = await QueryBuilderFactory.Create<SupportArticle>().CountAsync();
            if (pagination.ItemsAmount == 0)
                return new List<SupportArticle>();
            pagination.Refresh();
            return await QueryBuilderFactory.Create<SupportArticle>()
                .OrderByDescending(p => p.Id)
                .Skip(pagination.Offset)
                .Take(pagination.PageItemsAmount)
                .ToListAsync();
        }

        /// <summary>
        /// Получает статью по идентификатору
        /// </summary>
        /// <param name="articleId"></param>
        /// <returns></returns>
        public async Task<SupportArticle> GetArticleAsync(long articleId)
        {
            return await QueryBuilderFactory.Create<SupportArticle>()
                .Include(x => x.Description)
                .FirstOrDefaultAsync(x => x.Id == articleId);
        }

        /// <summary>
        /// Обновляет статью
        /// </summary>
        /// <param name="article"></param>
        [Transaction]
        public async Task EditArticleAsync(SupportArticle article)
        {
            await QueryBuilderFactory.Create<SupportArticleDescription>().UpdateAsync(article.Description);
            await QueryBuilderFactory.Create<SupportArticle>().UpdateAsync(article);
        }

        /// <summary>
        /// Создает статью
        /// </summary>
        /// <param name="article"></param>
        [Transaction]
        public async Task CreateArticleAsync(SupportArticle article)
        {
            if (!string.IsNullOrWhiteSpace(article.Description.Content))
            {
                await QueryBuilderFactory.Create<SupportArticleDescription>().InsertAsync(article.Description);
                article.DescriptionId = article.Description.Id;
            }
            else
                article.DescriptionId = null;
            article.CreatedDate = DateTime.Now;
            await QueryBuilderFactory.Create<SupportArticle>().InsertAsync(article);
        }

        /// <summary>
        /// Удаляет статью
        /// </summary>
        /// <param name="article"></param>
        [Transaction]
        public async Task DeleteArticleAsync(SupportArticle article)
        {
            await QueryBuilderFactory.Create<SupportArticle>().DeleteAsync(article);
        }
    }
}
