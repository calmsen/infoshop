﻿using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Models.Enumerations;
using DomainLogic.Interfaces.QueryBuilder;

namespace DomainLogic.Services
{
    public class FaqService : BaseService, IFaqService
    {
        private IQueryBuilder<Faq> QueryBuilder => QueryBuilderFactory.Create<Faq>();
        
        /// <summary>
        /// Получает список вопросов и ответов для определенного раздела
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<List<Faq>> GetFaqAsync(long? sectionId)
        {
            return await GetFaqAsync(sectionId, null, FeedbackThemeEnum.None, false);
        }

        /// <summary>
        /// Получает список вопросов и ответов для определенного раздела или товара, если указан товар
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        public async Task<List<Faq>> GetFaqAsync(long? sectionId, long? productId)
        {
            return await GetFaqAsync(sectionId, productId, FeedbackThemeEnum.None, false);
        }

        /// <summary>
        /// Получает список вопросов и ответов для определенного раздела или товара, если указан товар
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="productId"></param>
        /// <param name="theme"></param>
        /// <returns></returns>
        public async Task<List<Faq>> GetFaqAsync(long? sectionId, long? productId, FeedbackThemeEnum theme)
        {
            return await GetFaqAsync(sectionId, productId, FeedbackThemeEnum.None, false);
        }

        /// <summary>
        /// Получает список вопросов и ответов для определенного раздела или товара, если указан товар
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="productId"></param>
        /// <param name="theme"></param>
        /// <param name="onlyManager"></param>
        /// <returns></returns>
        public async Task<List<Faq>> GetFaqAsync(long? sectionId, long? productId, FeedbackThemeEnum theme, bool? onlyManager)
        {
            if (productId != null && sectionId == null)
                throw new ArgumentException("sectionId не может быть равен null");

            var query = QueryBuilder;

            if (onlyManager != null)
                query = query.Where(x => x.OnlyManager == onlyManager);

            if (theme != FeedbackThemeEnum.None)
                query = query.Where(x => x.Theme == theme);

            if (sectionId == null || sectionId == 0)
            {
                return await query.ToListAsync();
            }
            else if (sectionId == -1)
            {
                return await query.Where(x => x.SectionId == null)
                    .ToListAsync();
            }
            else
            {
                query = query.Where(x => x.SectionId == null || x.SectionId == sectionId);
                var items = await query.ToListAsync(x => new { Faq = x, ProductIds = x.ProductsToFaqs.Select(y => y.ProductId).ToList() });
                List<Faq> faq = new List<Faq>();
                foreach (var item in items)
                {
                    if (item.Faq.SectionId == null)
                    {
                        faq.Add(item.Faq);
                        continue;
                    }
                    if (item.Faq.SectionId != null && item.ProductIds.Count == 0)
                    {
                        faq.Add(item.Faq);
                        continue;
                    }
                    if (item.ProductIds.Any(x => x == productId))
                    {
                        faq.Add(item.Faq);
                    }
                }
                return faq;
            }
        }

        /// <summary>
        /// Получает информацию о вопросе и ответе по идишнику
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Faq> GetFaqByIdAsync(long id)
        {
            return await QueryBuilder
                .Include(x => x.Section)
                .Include(x => x.ProductsToFaqs.Select(y => y.Product))
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        /// <summary>
        /// Обновляет информацию о вопросе и ответе
        /// </summary>
        /// <param name="faq"></param>
        [Transaction]
        public async Task EditFaqAsync(Faq faq)
        {
            // отвязываем старые товары
            await UnbindProductsAsync(faq);
            // привязываем новые товары
            await BindProductsAsync(faq);
            await QueryBuilder.UpdateAsync(faq);
        }

        /// <summary>
        /// Создает вопрос и ответ
        /// </summary>
        /// <param name="faq"></param>
        [Transaction]
        public async Task CreateFaqAsync(Faq faq)
        {
            await QueryBuilder.InsertAsync(faq);
            // привязываем товары
            await BindProductsAsync(faq);
        }

        /// <summary>
        /// Удаляем вопрос и ответ
        /// </summary>
        /// <param name="faq"></param>
        [Transaction]
        public async Task DeleteFaqAsync(Faq faq)
        {
            // отвязываем старые товары
            await UnbindProductsAsync(faq);
            await QueryBuilder.DeleteAsync(faq);
        }

        /// <summary>
        /// Привязывает товары
        /// </summary>
        /// <param name="faq"></param>
        private async Task BindProductsAsync(Faq faq)
        {
            for (int i = 0; i < faq.ProductsToFaqs.Count; i++)
            {
                await QueryBuilderFactory.Create<ProductToFaq>().InsertAsync(new ProductToFaq { ProductId = faq.ProductsToFaqs[i].ProductId, FaqId = faq.Id });
            }
        }

        /// <summary>
        /// Отвязывает товары
        /// </summary>
        /// <param name="faq"></param>
        private async Task UnbindProductsAsync(Faq faq)
        {
            await QueryBuilderFactory.Create<ProductToFaq>().DeleteAsync(new { FaqId = faq.Id });
        }
    }
}
