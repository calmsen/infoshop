﻿using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using DomainLogic.Services.Shared;
using DomainLogic.Infrastructure.Attributes;
using DomainLogic.Services.Shared.Providers;

namespace DomainLogic.Services
{
    public class OptionsService : BaseService, IOptionsService
    {
        [Inject]
        public OptionsProvider OptionsProvider { get; set; }

        /// <summary>
        /// Получает все опции
        /// </summary>
        /// <returns></returns>
        public async Task<List<Option>> GetOptions()
        {
            return await OptionsProvider.GetOptionsAsync();
        }

        public async Task<Dictionary<long, Models.Positions>> GetPositionsOfSectonsAsync()
        {
            string positionsOfSectionsAsString = await GetOptionValueByNameAsync("PositionsOfSections");
            if (positionsOfSectionsAsString == null)
                return new Dictionary<long, Positions>();
            return JsonConvert.DeserializeObject<Dictionary<long, Positions>>(positionsOfSectionsAsString);
        }

        /// <summary>
        /// Получает значение опции по ключу
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<string> GetOptionValueByNameAsync(string name)
        {
            return (await QueryBuilderFactory.Create<Option>()
                .FirstOrDefaultAsync(x => x.Name.Equals(name)))?.Value;
        }

        /// <summary>
        /// Получает опцию по ключу
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<Option> GetOptionByNameAsync(string name)
        {
            return await QueryBuilderFactory.Create<Option>()
                .FirstOrDefaultAsync(x => x.Name.Equals(name));
        }

        /// <summary>
        /// Получает или создает  опцию если надо
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public async Task<Option> GetOrCreateOptionAsync(string name, string value)
        {
            Option option = await GetOptionByNameAsync(name);
            if (option == null)
            {
                option = new Option { Name = name, Value = value };
                await CreateOptionAsync(option);
            }
            return option;
        }

        /// <summary>
        /// Создает опцию
        /// </summary>
        /// <param name="option"></param>
        [Transaction]
        public async Task CreateOptionAsync(Option option)
        {
            await QueryBuilderFactory.Create<Option>().UpdateAsync(option);
        }

        /// <summary>
        /// Обновляет опцию
        /// </summary>
        /// <param name="option"></param>
        [Transaction]
        public async Task UpdateOptionAsync(Option option)
        {
            await QueryBuilderFactory.Create<Option>().UpdateAsync(option);
        }
    }
}