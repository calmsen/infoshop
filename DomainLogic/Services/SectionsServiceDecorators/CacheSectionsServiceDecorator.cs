﻿
using DomainLogic.Infrastructure.Extensions;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainLogic.Services.SectionsServiceDecorators
{
    public class CacheSectionsServiceDecorator : BaseSectionsServiceDecorator
    {
        public CacheSectionsServiceDecorator(ISectionsService sectionsService) : base(sectionsService)
        {
        }
        

        private List<Section> _cachedSections;

        private Dictionary<long, List<SectionToImage>> _cachedSectionBanners = new Dictionary<long, List<SectionToImage>>();

        private void ClearCache()
        {
            _cachedSections = null;
            _cachedSectionBanners.Clear();
        }

        /// <summary>
        /// Получает все разделы
        /// </summary>
        /// <returns></returns>
        public override async Task<List<Section>> GetSectionsAsync()
        {
            return await GetCachedSectionsAsync();
        }

        /// <summary>
        /// Получает разделы по списку идентификаторов
        /// </summary>
        /// <param name="sectionIds"></param>
        /// <returns></returns>
        public override async Task<List<Section>> GetSectionsAsync(List<long> sectionIds)
        {
            return (await GetCachedSectionsAsync())
                .Where(x => sectionIds.Contains(x.Id))
                .MapList<Section>();
        }

        /// <summary>
        /// Получает дочерние разделы
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public override async Task<List<Section>> GetSectionChildsAsync(long sectionId)
        {
            var sections = new List<Section>();
            foreach (var s in (await GetCachedSectionsAsync()))
                if (s.ParentId == sectionId)
                    sections.Add(s);
            return sections
                .MapList<Section>();
        }

        /// <summary>
        /// Получает соседние разделы
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<List<Section>> GetSectionSiblingsAsync(long sectionId)
        {
            long parentId = (await GetCachedSectionsAsync()).FirstOrDefault(x => x.Id == sectionId).ParentId;
            var sections = new List<Section>();
            foreach (var s in (await GetCachedSectionsAsync()))
                if (s.ParentId == parentId)
                    sections.Add(s);
            return sections;
        }
        
        /// <summary>
        /// Получает раздел по наименованию
        /// </summary>
        /// <param name="sectionName"></param>
        /// <param name="needIncludeBanner"></param>
        /// <returns></returns>
        public override async Task<Section> GetSectionByNameAsync(string sectionName, bool needIncludeBanner = false)
        {
            Section section = (await GetCachedSectionsAsync()).FirstOrDefault(x => x.Name == sectionName)?.Map<Section>();
            await SetSectionBannesIfNeedAsync(section, needIncludeBanner);
            return section;
        }

        /// <summary>
        /// Получает раздел по идентификатору
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="needIncludeBanner"></param>
        /// <returns></returns>
        public override async Task<Section> GetSectionByIdAsync(long sectionId, bool needIncludeBanner = false)
        {
            Section section = (await GetCachedSectionsAsync()).FirstOrDefault(x => x.Id == sectionId)?.Map<Section>();
            await SetSectionBannesIfNeedAsync(section, needIncludeBanner);
            return section;
        }

        /// <summary>
        /// Проверяет имеет ли раздел дочерние разделы
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public override async Task<bool> HasChildsAsync(long sectionId)
        {
            return (await GetCachedSectionsAsync()).Where(x => x.ParentId == sectionId).Count() > 0;
        }
        
        /// <summary>
        /// Обновляет раздел
        /// </summary>
        /// <param name="section"></param>
        public override async Task EditSectionAsync(Section section)
        {
            await base.EditSectionAsync(section);
            ClearCache();
        }

        /// <summary>
        /// Создает раздел
        /// </summary>
        /// <param name="section"></param>
        public override async Task CreateSectionAsync(Section section)
        {
            await base.CreateSectionAsync(section);
            ClearCache();
        }

        /// <summary>
        /// Удаляет раздел
        /// </summary>
        /// <param name="sectionId"></param>
        public override async Task DeleteSectionAsync(long sectionId)
        {
            await base.DeleteSectionAsync(sectionId);
            ClearCache();

        }

        /// <summary>
        /// Сохраняет позиции разделов
        /// </summary>
        /// <param name="sections"></param>
        public override async Task SaveSectionsPositionsAsync(List<Section> sections)
        {
            await base.SaveSectionsPositionsAsync(sections);
            ClearCache();
        }

        /// <summary>
        /// Отвязывает все товары от раздела
        /// </summary>
        /// <param name="sectionId"></param>
        public override async Task UnbindProductsAsync(long sectionId)
        {
            await base.UnbindProductsAsync(sectionId);
            ClearCache();
        }

        private async Task<List<Section>> GetCachedSectionsAsync()
        {
            if (_cachedSections == null)
                _cachedSections = await base.GetSectionsAsync();
            return _cachedSections;
        }

        /// <summary>
        /// Устанавливает баннеры для раздела
        /// </summary>
        /// <param name="section"></param>
        /// <param name="needIncludeBanner"></param>
        /// <returns></returns>
        private async Task SetSectionBannesIfNeedAsync(Section section, bool needIncludeBanner = false)
        {
            if (section == null)
                return;

            if (!needIncludeBanner)
                return;

            if (!_cachedSectionBanners.ContainsKey(section.Id))
            {
                Section sectionWithBanners = await base.GetSectionByIdAsync(section.Id, true);
                _cachedSectionBanners[section.Id] = sectionWithBanners.SectionsToImages;
            }

            section.SectionsToImages = _cachedSectionBanners[section.Id];
        }
    }
}
