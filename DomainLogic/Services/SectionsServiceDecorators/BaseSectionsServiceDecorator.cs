﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using DomainLogic.Interfaces.Services;
using DomainLogic.Models;

namespace DomainLogic.Services.SectionsServiceDecorators
{
    public abstract class BaseSectionsServiceDecorator : ISectionsService
    {
        private readonly ISectionsService _sectionsService;

        public BaseSectionsServiceDecorator(ISectionsService sectionsService)
        {
            _sectionsService = sectionsService;
        }

        public virtual Task CreateSectionAsync(Section section)
        {
            return _sectionsService.CreateSectionAsync(section);
        }

        public virtual Task DeleteSectionAsync(long sectionId)
        {
            return _sectionsService.DeleteSectionAsync(sectionId);
        }

        public virtual Task EditSectionAsync(Section section)
        {
            return _sectionsService.EditSectionAsync(section);
        }

        public virtual Task<List<Section>> GetDescendenOrSelfSectionsAsync(long pathOfSections)
        {
            return _sectionsService.GetDescendenOrSelfSectionsAsync(pathOfSections);
        }

        public virtual List<Section> GetDescendenSectionsFromList(List<Section> sections, Section section, bool isGetHidden = true)
        {
            return _sectionsService.GetDescendenSectionsFromList(sections, section, isGetHidden);
        }

        public virtual string GetPathOfSectionIdsFromList(List<Section> sections, Section section)
        {
            return _sectionsService.GetPathOfSectionIdsFromList(sections, section);
        }

        public virtual string GetPathOfSectionNamesFromList(List<Section> sections, Section section)
        {
            return _sectionsService.GetPathOfSectionIdsFromList(sections, section);
        }

        public virtual List<Section> GetPathOfSectionsFromList(List<Section> sections, Section section)
        {
            return _sectionsService.GetPathOfSectionsFromList(sections, section);
        }

        public virtual Task<Section> GetSectionByIdAsync(long sectionId, bool needIncludeBanner = false)
        {
            return _sectionsService.GetSectionByIdAsync(sectionId, needIncludeBanner);
        }

        public virtual Section GetSectionByIdFromList(List<Section> sections, long sectionId)
        {
            return _sectionsService.GetSectionByIdFromList(sections, sectionId);
        }

        public virtual Task<Section> GetSectionByNameAsync(string sectionName, bool needIncludeBanner = false)
        {
            return _sectionsService.GetSectionByNameAsync(sectionName, needIncludeBanner);
        }

        public virtual List<Section> GetSectionByQueryFromList(List<Section> sections, string query = "")
        {
            return _sectionsService.GetSectionByQueryFromList(sections, query);
        }

        public virtual Task<List<Section>> GetSectionChildsAsync(long sectionId)
        {
            return _sectionsService.GetSectionChildsAsync(sectionId);
        }

        public virtual List<Section> GetSectionChildsFromList(List<Section> sections, long sectionId)
        {
            return _sectionsService.GetSectionChildsFromList(sections, sectionId);
        }

        public virtual List<long> GetSectionChildsIdsFromList(List<Section> sections, Section section, bool isGetHidden = true)
        {
            return _sectionsService.GetSectionChildsIdsFromList(sections, section, isGetHidden);
        }

        public virtual List<List<Section>> GetSectionsAsPathSiblingsFromList(List<Section> sections, long sectionId = 0, bool isGetChilds = false)
        {
            return _sectionsService.GetSectionsAsPathSiblingsFromList(sections, sectionId, isGetChilds);
        }

        public virtual Task<List<Section>> GetSectionsAsync()
        {
            return _sectionsService.GetSectionsAsync();
        }

        public virtual Task<List<Section>> GetSectionsAsync(List<long> sectionIds)
        {
            return _sectionsService.GetSectionsAsync(sectionIds);
        }

        public virtual Task<List<Section>> GetSectionsWithProductsAsync()
        {
            return _sectionsService.GetSectionsWithProductsAsync();
        }

        public virtual Task<List<SectionTitle>> GetSectionTitlesAsync(List<long> filterIds)
        {
            return _sectionsService.GetSectionTitlesAsync(filterIds);
        }

        public virtual Task<bool> HasChildsAsync(long sectionId)
        {
            return _sectionsService.HasChildsAsync(sectionId);
        }

        public virtual void MapSectionChildsFromList(List<Section> sections, Section section)
        {
            _sectionsService.MapSectionChildsFromList(sections, section);
        }

        public virtual Task SaveSectionsPositionsAsync(List<Section> sections)
        {
            return _sectionsService.SaveSectionsPositionsAsync(sections);
        }

        public virtual List<Section> SectionsToTreeFromList(List<Section> sections, long parentId, bool isGetHidden = true)
        {
            return _sectionsService.SectionsToTreeFromList(sections, parentId, isGetHidden);
        }

        public virtual Task UnbindProductsAsync(long sectionId)
        {
            return _sectionsService.UnbindProductsAsync(sectionId);
        }
    }
}
