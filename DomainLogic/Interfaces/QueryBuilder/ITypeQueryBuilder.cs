﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.QueryBuilder
{
    /// <summary>
    /// Формирует запрос в БД
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IQueryBuilder<TEntity> : IBaseQueryBuilder
    {
        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <returns></returns>
        Task<TEntity> FirstOrDefaultAsync();

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <returns></returns>
        List<TEntity> ToList();

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <returns></returns>
        Task<List<TEntity>> ToListAsync();

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        Task<List<TEntity>> ToListAsync(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="selector"></param>
        /// <returns></returns>
        Task<List<TElement>> ToListAsync<TElement>(Expression<Func<TEntity, TElement>> selector);

        /// <summary>
        /// Строит выражение Select. См. описание у EF метода
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="selector"></param>
        /// <returns></returns>
        IQueryBuilder<TElement> Select<TElement>(Expression<Func<TEntity, TElement>> selector);

        /// <summary>
        /// Строит выражение Join. См. описание у EF метода
        /// </summary>
        /// <typeparam name="TInner"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="inner"></param>
        /// <param name="outerKeySelector"></param>
        /// <param name="innerKeySelector"></param>
        /// <param name="resultSelector"></param>
        /// <returns></returns>
        IQueryBuilder<TElement> Join<TInner, TKey, TElement>(IEnumerable<TInner> inner, Expression<Func<TEntity, TKey>> outerKeySelector,
            Expression<Func<TInner, TKey>> innerKeySelector, Expression<Func<TEntity, TInner, TElement>> resultSelector)
             where TInner : class;

        /// <summary>
        /// Строит выражение Include. См. описание у EF метода
        /// </summary>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        IQueryBuilder<TEntity> Include<TProperty>(Expression<Func<TEntity, TProperty>> path);

        /// <summary>
        /// Строит выражение where. См. описание у EF метода
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IQueryBuilder<TEntity> Where(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Строит выражение where. См. описание у EF метода
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        IQueryBuilder<TEntity> Where(string predicate, params object[] values);

        /// <summary>
        /// Строит выражение OrderBy. См. описание у EF метода
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        IQueryBuilder<TEntity> OrderBy<TKey>(Expression<Func<TEntity, TKey>> keySelector);

        /// <summary>
        /// Строит выражение OrderBy. См. описание у EF метода
        /// </summary>
        /// <param name="ordering"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        IQueryBuilder<TEntity> OrderBy(string ordering, params object[] values);

        /// <summary>
        /// Строит выражение OrderByDescending. См. описание у EF метода
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        IQueryBuilder<TEntity> OrderByDescending<TKey>(Expression<Func<TEntity, TKey>> keySelector);

        /// <summary>
        /// Строит выражение Skip. Количество записей которые нужно пропустить
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        IQueryBuilder<TEntity> Skip(int count);

        /// <summary>
        /// Строит выражение Take. Количество записей которые нужно получить
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        IQueryBuilder<TEntity> Take(int count);

        /// <summary>
        /// Строит выражение Distinct. См. описание у EF метода
        /// </summary>
        /// <returns></returns>
        IQueryBuilder<TEntity> Distinct();

        /// <summary>
        /// Получает количество записей. См. описание у EF метода 
        /// </summary>
        /// <returns></returns>
        Task<int> CountAsync();

        /// <summary>
        /// Получает количество записей. См. описание у EF метода 
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate);
    }

    

    

    
}