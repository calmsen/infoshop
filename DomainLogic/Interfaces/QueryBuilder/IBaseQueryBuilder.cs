﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.QueryBuilder
{
    /// <summary>
    /// Формирует запрос в БД
    /// </summary>
    public interface IBaseQueryBuilder
    {
        /// <summary>
        /// Основной ключ сущности
        /// </summary>
        string PrimaryKey { get; }

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <param name="elementType"></param>
        /// <param name="fields"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<object> FirstOrDefaultAsync(Type elementType, string[] fields = null, object where = null);

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="fields"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<TElement> FirstOrDefaultAsync<TElement>(string[] fields, object where = null);

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<TElement> FirstOrDefaultAsync<TElement>(object where = null);

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <returns></returns>
        Task<TElement> FirstOrDefaultAsync<TElement>();

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <param name="elementType"></param>
        /// <param name="fields"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<List<object>> ToListAsync(Type elementType, string[] fields = null, object where = null);

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="fields"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<List<TElement>> ToListAsync<TElement>(string[] fields, object where = null);

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<List<TElement>> ToListAsync<TElement>(object where = null);

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <returns></returns>
        Task<List<TElement>> ToListAsync<TElement>();

        /// <summary>
        /// Добавляет запись в таблицу, соответсвующей сущности
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        Task InsertAsync(object obj, object where = null);

        /// <summary>
        /// Обновляет запись
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        Task UpdateAsync(object obj, object where = null);

        /// <summary>
        /// Удаляет запись
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        Task DeleteAsync(object where);

        /// <summary>
        /// Выполняет sql запрос
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task<int> ExecuteAsync(string sql, params object[] parameters);

        /// <summary>
        /// Выполняет sql запрос для получения одной записи 
        /// </summary>
        /// <typeparam name="TTargetElement"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task<TTargetElement> ToTargetItemAsync<TTargetElement>(string sql, params object[] parameters);

        /// <summary>
        /// Выполняет sql запрос для получения списка записей
        /// </summary>
        /// <typeparam name="TTargetElement"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task<List<TTargetElement>> ToTargetListAsync<TTargetElement>(string sql, params object[] parameters);

        /// <summary>
        /// Указывает что строку не нужно обрабатывать в sql запросе
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        string DbRaw(string str);
    }
}