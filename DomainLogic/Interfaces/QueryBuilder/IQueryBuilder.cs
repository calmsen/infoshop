﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.QueryBuilder
{
    /// <summary>
    /// Формирует запрос в БД
    /// </summary>
    public interface IQueryBuilder : IBaseQueryBuilder
    {
        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <returns></returns>
        Task<object> FirstOrDefaultAsync();

        /// <summary>
        /// Получает первую запись или null, если ни одной записи нет
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        Task<object> FirstOrDefaultAsync(string predicate, params object[] values);

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <returns></returns>
        Task<List<object>> ToListAsync();

        /// <summary>
        /// Получает список записей
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        Task<List<object>> ToListAsync(string predicate, params object[] values);

        /// <summary>
        /// Строит выражение where. См. описание у EF метода
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        IQueryBuilder Where(string predicate, params object[] values);

        /// <summary>
        /// Строит выражение OrderBy. См. описание у EF метода 
        /// </summary>
        /// <param name="ordering"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        IQueryBuilder OrderBy(string ordering, params object[] values);

        /// <summary>
        /// Строит выражение Skip. Количество записей которые нужно пропустить
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        IQueryBuilder Skip(int count);

        /// <summary>
        /// Строит выражение Take. Количество записей которые нужно получить 
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        IQueryBuilder Take(int count);

        /// <summary>
        /// Получает количество записей. См. описание у EF метода 
        /// </summary>
        /// <returns></returns>
        Task<int> CountAsync();

        /// <summary>
        /// Получает количество записей. См. описание у EF метода
        /// </summary>
        /// <param name="ordering"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        Task<int> CountAsync(string ordering, params object[] values);
    }
}