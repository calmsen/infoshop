﻿using System;

namespace DomainLogic.Interfaces.QueryBuilder
{
    /// <summary>
    /// Фабрика для создания QueryBuilder
    /// </summary>
    public interface IQueryBuilderFactory
    {
        /// <summary>
        /// Создает базовый билдер для формирования запроса
        /// </summary>
        /// <returns></returns>
        IBaseQueryBuilder Create();

        /// <summary>
        /// Создает билдер для формирования запроса
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        IQueryBuilder Create(Type entityType);

        /// <summary>
        /// Создает билдер для формирования запроса
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        IQueryBuilder<TEntity> Create<TEntity>() where TEntity : class;

        /// <summary>
        /// Создает билдер для формирования запроса
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="queryBuilder"></param>
        /// <returns></returns>
        IQueryBuilder<TEntity> Create<TEntity>(IQueryBuilder<TEntity> queryBuilder);
    }
}