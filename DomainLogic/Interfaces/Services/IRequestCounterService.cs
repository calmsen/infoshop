﻿using DomainLogic.Infrastructure.Paging;
using DomainLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface IRequestCounterService
    {
        Task CreateRequestCounterAsync(RequestCounter requestCounter);

        Task<List<RequestCounter>> GetRequestCounterListAsync(Pagination pagination);
    }
}
