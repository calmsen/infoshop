﻿using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface ICommonService
    {
        Task<string> GetCityByIpAsync(string ip);

        Task SendMailAsync(string subject, string body, bool doTest);
    }
}
