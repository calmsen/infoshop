﻿using DomainLogic.Models;
using DomainLogic.Models.Enumerations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface IFaqService
    {
        Task<List<Faq>> GetFaqAsync(long? sectionId);

        Task<List<Faq>> GetFaqAsync(long? sectionId, long? productId);

        Task<List<Faq>> GetFaqAsync(long? sectionId, long? productId, FeedbackThemeEnum theme);

        Task<List<Faq>> GetFaqAsync(long? sectionId, long? productId, FeedbackThemeEnum theme, bool? onlyManager);

        Task<Faq> GetFaqByIdAsync(long id);
        
        Task EditFaqAsync(Faq faq);

        Task CreateFaqAsync(Faq faq);

        Task DeleteFaqAsync(Faq faq);
    }
}
