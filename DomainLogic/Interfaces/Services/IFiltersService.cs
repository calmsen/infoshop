﻿using DomainLogic.Infrastructure.Paging;
using DomainLogic.Models;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface IFiltersService
    {
        Task<Dictionary<long, List<long>>> GetFiltersParamsAsync(NameValueCollection qParams);

        Task<List<FilterParam>> GetFiltersParamsAsListAsync(NameValueCollection qParams, Section section, List<DmFilter> outFilters = null);
        
        Task<List<DmFilter>> GetFiltersAsync();

        Task<List<DmFilter>> GetFiltersByNamesAsync(string[] filterNames);

        Task<List<DmFilter>> GetFiltersForSectionAsync(long sectionId);

        Task<List<DmFilter>> GetFiltersByIdsAsync(List<long> filtersIds, bool needIncludeValues = true);

        Task<List<DmFilter>> SearchFiltersAsync(string query);

        Task<List<DmFilter>> GetFiltersAsync(Pagination pagination, long pathOfSections, string query);
        
        Task<DmFilter> GetFilterByIdAsync(long filterId, bool needIncludeValues = true);

        Task<DmFilter> GetFilterByTitleAsync(string filterTitle, long sectionId);

        Task<DmFilter> GetFilterByNameAsync(string filterName, long sectionId);
        
        Task EditFilterAsync(DmFilter filter);

        Task CreateFilterAsync(DmFilter filter);

        Task DeleteFilterAsync(long filterId);

        Task<DmFilter> CopyFilterAsync(long filterId);

        Task<bool> CheckProductsValuesMultipleAsync(long filterId);
        
        Task<List<FilterValue>> GetFilterValuesAsync(long filterId);

        Task<List<FilterValue>> GetFilterValuesAsync(string filterTitle, long sectionId);

        Task<List<FilterValue>> GetFiltersValuesAsync(List<FilterParam> fParams);

        Task MergeBindedValuesAsync(long valueId, long mainValueId, List<long> sectionIds);

        Task DeleteBindedValuesAsync(long valueId, List<long> sectionIds);

        Task<FilterValue> CopyBindedValuesAsync(long valueId, List<long> sectionIds);

        Task<FilterValue> GetOrCreateFilterValueAsync(long filterId, string value, string valueIn, double valueAsDouble);
    }
}
