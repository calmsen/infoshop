﻿using DomainLogic.Models;
using DomainLogic.Models.Enumerations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{

    public interface IProductSnapshotsService
    {
        Task<List<ProductSnapshot>> GetProductSnapshotsAsync(long productId);

        Task<List<ProductSnapshot>> GetLastProductSnapshotsAsync(List<long> productIds);

        Task<ProductSnapshot> GetProductSnapshotAsync(long snapshotId);

        Task<ProductSnapshot> GetLastProductSnapshotAsync(long productId);

        Task<ProductSnapshot> CreateProductSnapshotAsync(ProductSnapshot originalProductSnapshot, Dictionary<string, object> fields, List<ProductStateEnum> productStates, int userId);

        Task<ProductSnapshot> CreateProductSnapshotAsync(Product product, List<ProductStateEnum> productStates, int userId);
    }
}
