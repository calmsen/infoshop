﻿using DomainLogic.Infrastructure.Paging;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface ISupportArticlesService
    {
        Task<List<SupportArticle>> GetArticlesAsync(Pagination pagination);

        Task<SupportArticle> GetArticleAsync(long articleId);

        Task EditArticleAsync(SupportArticle article);

        Task CreateArticleAsync(SupportArticle article);

        Task DeleteArticleAsync(SupportArticle article);
    }
}
