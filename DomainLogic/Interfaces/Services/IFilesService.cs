﻿using DomainLogic.Infrastructure.Paging;
using DomainLogic.Models;
using DomainLogic.Models.Enumerations;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface IFilesService
    {
        Task<List<DmFile>> GetFilesAsync(FileFilterEnum filters, long productId);

        Task<List<DmFile>> GetFilesAsync(long productId);

        Task<List<DmFile>> SerchFilesAsync(string query, FileFilterEnum filters, Pagination pagination, List<long> sectionsIds);

        Task<DmFile> GetFileByIdAsync(long id);

        Task EditFileAsync(DmFile file, long productId);

        Task CreateFileAsync(DmFile file, long productId);

        Task DeleteFileAsync(DmFile file);

        Task LoadFileAsync(Stream file, string fileName, string folder);
    }
}
