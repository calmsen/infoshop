﻿using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface IProductCounters
    {
        Task UpdateProductCountersAsync();
    }
}
