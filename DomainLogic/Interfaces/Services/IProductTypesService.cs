﻿using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface IProductTypesService
    {
        Task<List<ProductType>> GetTypesAsync();

        Task<ProductType> GetTypeByIdAsync(long typeId);

        Task EditTypeAsync(ProductType type);

        Task CreateTypeAsync(ProductType type);

        Task DeleteTypeAsync(ProductType type);
    }
}
