﻿using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface IOptionsService
    {
        Task<List<Option>> GetOptions();

        Task<Dictionary<long, Positions>> GetPositionsOfSectonsAsync();
        
        Task<string> GetOptionValueByNameAsync(string name);

        Task<Option> GetOptionByNameAsync(string name);

        Task<Option> GetOrCreateOptionAsync(string name, string value);

        Task CreateOptionAsync(Option option);

        Task UpdateOptionAsync(Option option);
    }
}
