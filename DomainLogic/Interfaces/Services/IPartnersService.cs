﻿using DomainLogic.Infrastructure.Paging;
using DomainLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    /// <summary>
    /// Интерфейс сервиса для работы с партнерами
    /// </summary>
    public interface IPartnersService
    {
        /// <summary>
        /// Получает список всех партнеров
        /// </summary>
        /// <returns></returns>
        Task<List<Partner>> GetPartnersAsync();

        /// <summary>
        /// Получает партнеров с указанным количеством элементов на странице
        /// </summary>
        /// <param name="pagination"></param>
        /// <returns></returns>
        Task<List<Partner>> GetPartnersAsync(Pagination pagination);

        /// <summary>
        /// Получает список партнеров по списку идентификаторов
        /// </summary>
        /// <param name="partnerIds"></param>
        /// <returns></returns>
        Task<List<Partner>> GetPartnersByIdsAsync(List<long> partnerIds);

        /// <summary>
        /// Получает информацию о партнере по идентификатору
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        Task<Partner> GetPartnerByIdAsync(long partnerId);

        /// <summary>
        /// Обновляет партнера
        /// </summary>
        /// <param name="partner"></param>
        Task EditPartnerAsync(Partner partner);

        /// <summary>
        /// Создает партнера
        /// </summary>
        /// <param name="partner"></param>
        Task CreatePartnerAsync(Partner partner);

        /// <summary>
        /// Удаляет партнера
        /// </summary>
        /// <param name="partner"></param>
        Task DeletePartnerAsync(Partner partner);
    }
}
