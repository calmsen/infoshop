﻿using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface IResourcesService
    {
        /// <summary>
        /// Получает все ресурсы
        /// </summary>
        /// <returns></returns>
        List<Resource> GetResources();

        /// <summary>
        /// Получает ресурс по ключу
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Resource GetResourceByKey(string key);

        /// <summary>
        ///  Создает ресурс
        /// </summary>
        /// <param name="resource"></param>
        Task CreateResourceAsync(Resource resource);

        /// <summary>
        /// Обновляет ресурс
        /// </summary>
        /// <param name="resource"></param>
        Task UpdateResourceAsync(Resource resource);

        /// <summary>
        /// Создает или обновляет ресурс
        /// </summary>
        /// <param name="resource"></param>
        Task CreateOrUpdateResourceAsync(Resource resource);
    }
}
