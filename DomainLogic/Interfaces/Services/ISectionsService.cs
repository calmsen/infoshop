﻿using DomainLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface ISectionsService
    {
        void MapSectionChildsFromList(List<Section> sections, Section section);

        List<Section> SectionsToTreeFromList(List<Section> sections, long parentId, bool isGetHidden = true);

        List<List<Section>> GetSectionsAsPathSiblingsFromList(List<Section> sections, long sectionId = 0, bool isGetChilds = false);

        List<Section> GetDescendenSectionsFromList(List<Section> sections, Section section, bool isGetHidden = true);

        List<long> GetSectionChildsIdsFromList(List<Section> sections, Section section, bool isGetHidden = true);

        Section GetSectionByIdFromList(List<Section> sections, long sectionId);

        List<Section> GetSectionChildsFromList(List<Section> sections, long sectionId);

        List<Section> GetSectionByQueryFromList(List<Section> sections, string query = "");

        List<Section> GetPathOfSectionsFromList(List<Section> sections, Section section);

        string GetPathOfSectionNamesFromList(List<Section> sections, Section section);

        string GetPathOfSectionIdsFromList(List<Section> sections, Section section);

        Task<List<Section>> GetSectionsAsync();

        Task<List<Section>> GetSectionsAsync(List<long> sectionIds);

        Task<List<Section>> GetSectionChildsAsync(long sectionId);

        Task<List<Section>> GetSectionsWithProductsAsync();

        Task<List<Section>> GetDescendenOrSelfSectionsAsync(long pathOfSections);

        Task<List<SectionTitle>> GetSectionTitlesAsync(List<long> filterIds);

        Task<Section> GetSectionByIdAsync(long sectionId, bool needIncludeBanner = false);

        Task<Section> GetSectionByNameAsync(string sectionName, bool needIncludeBanner = false);

        Task<bool> HasChildsAsync(long sectionId);

        Task EditSectionAsync(Section section);

        Task CreateSectionAsync(Section section);

        Task DeleteSectionAsync(long sectionId);

        Task SaveSectionsPositionsAsync(List<Section> sections);

        Task UnbindProductsAsync(long sectionId);
    }
}
