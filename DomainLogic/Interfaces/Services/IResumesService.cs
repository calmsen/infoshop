﻿using DomainLogic.Models;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface IResumesService
    {
        Task CreateResumeAsync(Resume form);
    }
}
