﻿using DomainLogic.Infrastructure.Paging;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface IPagesService
    {
        Task<List<Page>> GetPagesAsync(Pagination pagination);

        Task<Page> GetPageByIdAsync(long pageId);

        Task<Page> GetPageByNameAsync(string pageName);

        Task EditPageAsync(Page page);

        Task CreatePageAsync(Page page);

        Task DeletePageAsync(Page page);
    }
}
