﻿using DomainLogic.Models;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface IAddressesService
    {
        Task<Address> GetAddressByIpAsync(string ip);
    }
}
