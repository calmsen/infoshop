﻿using DomainLogic.Models;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface IProductCountersService
    {
        Task<ProductCounter> GetAsync(long id);

        Task AddAsync(ProductCounter counter);

        Task UpdateAsync(ProductCounter counter);

        Task AddOrUpdateByProductAsync(long productId, long? cityId);
    }    
}
