﻿using DomainLogic.Infrastructure.Credentials;
using DomainLogic.Interfaces.ImageInterfaces;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface IImagesService
    {
        Task<IEnumerable<object>> SearchAsync(string type, string query, int page);
        Task<T> UploadImageAsync<T>(Stream file);
        Task<object> UploadImageAsync(Type type, Stream file);
        Task<object> UploadImageAsync(string type, Stream file);

        Task<IList<T>> UploadImagesAsync<T>(Stream[] files);
        Task<IList<object>> UploadImagesAsync(Type type, Stream[] files);
        Task<IList<object>> UploadImagesAsync(string type, Stream[] files);

        Task<T> UploadImageAsync<T>(string UrlOrFtpPath);
        Task<object> UploadImageAsync(Type type, string UrlOrFtpPath);
        Task<object> UploadImageAsync(string type, string UrlOrFtpPath);

        Task<IList<T>> UploadImagesAsync<T>(string UrlOrFtpPath);
        Task<IList<object>> UploadImagesAsync(Type type, string UrlOrFtpPath);
        Task<IList<object>> UploadImagesAsync(string type, string UrlOrFtpPath);

        Task<T> UploadImageAsync<T>(HttpCredential settings, string ftpPath);
        Task<object> UploadImageAsync(Type type, HttpCredential settings, string ftpPath);
        Task<object> UploadImageAsync(string type, HttpCredential settings, string ftpPath);

        Task<IList<T>> UploadImagesAsync<T>(HttpCredential settings, string ftpPath);
        Task<IList<object>> UploadImagesAsync(Type type, HttpCredential settings, string ftpPath);
        Task<IList<object>> UploadImagesAsync(string type, HttpCredential settings, string ftpPath);

        Task<T> UploadImageAsync<T>(FtpCredential credential, string ftpPath);
        Task<object> UploadImageAsync(Type type, FtpCredential credential, string ftpPath);
        Task<object> UploadImageAsync(string type, FtpCredential credential, string ftpPath);

        Task<IList<T>> UploadImagesAsync<T>(FtpCredential credential, string ftpPath);
        Task<IList<object>> UploadImagesAsync(Type type, FtpCredential credential, string ftpPath);
        Task<IList<object>> UploadImagesAsync(string type, FtpCredential credential, string ftpPath);

        void ReloadImage(string type, Stream file, long id, int dimension);

        Task UploadImageAsync(HttpCredential settings, string url, string fileNameDst, string folderDst, bool createMini = true, IImageDimension imDim = null, int minDim = 800);
        Task UploadImageAsync(FtpCredential credential, string url, string fileNameDst, string folderDst, bool createMini = true, IImageDimension imDim = null, int minDim = 800);
        void UploadImage(Stream stream, string fileNameDst, string folderDst, bool createMini = true, IImageDimension imDim = null, int minDim = 800);
        Task UploadImageAsync(string url, string fileNameDst, string folderDst, bool createMini = true, IImageDimension imDim = null, int minDim = 800);

        void DrawWatermark(string watermarkImagePath, System.Drawing.Image image);
        //
        Task<object> GetImageByIdAsync(string type, long id);
        Task UpdateImageAsync(string type, object image);
    }
}
