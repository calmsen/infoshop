﻿using DomainLogic.Models;
using DomainLogic.Models.Enumerations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface IVacanciesService
    {
        Task<List<Vacancy>> GetVacanciesAsync();

        Task<List<Vacancy>> GetVacanciesAsync(VacancyStateEnum state);

        Task<Vacancy> GetVacancyAsync(int vacancyId);

        Task EditVacancyAsync(Vacancy vacancy);

        Task CreateVacancyAsync(Vacancy vacancy);

        Task DeleteVacancyAsync(Vacancy vacancy);
    }
}