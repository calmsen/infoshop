﻿using DomainLogic.Infrastructure.Paging;
using DomainLogic.Interfaces.Models;
using DomainLogic.Models;
using DomainLogic.Models.Enumerations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface IFeedbacksService 
    {
        Task<List<Feedback>> GetFeedbacksAsync(Pagination pagination, IFeedbackListFilter filter);

        Task<List<Feedback>> GetMyFeedbacksAsync(string userEmail);

        Task<Feedback> GetFeedbackByIdAsync(long feedbackId);

        Task CreateFeedbackAsync(Feedback feedback);

        Task UpdateFeedbackImagesAsync(Feedback feedback);

        Task ChangeStateAsync(long id, StateEnum state, long userId);

        Task ChangeModelAsync(long? sectionId, long? productId, long feedbackId);

        Task ChangeThemeAsync(FeedbackThemeEnum theme, long feedbackId, int userId);
        
        Task<FeedbackAnswer> GetFeedbackAnswerAsync(long answerId);

        Task<List<FeedbackAnswer>> GetFeedbackAnswersAsync(Pagination pagination, IFeedbackAnswerListFilter filter);

        Task CreateFeedbackAnswerAsync(FeedbackAnswer feedbackAnswer, bool needChangeState, bool needPickUp);
        
        Task<List<FeedbackTag>> SearchTagsAsync(string query);

        Task CreateFeedbackTagAsync(FeedbackTag tag);

        Task DeleteFeedbackTagAsync(long feedbackId, string tagTitle);
        
        Task InviteMessageAsync(long feedbackId, List<string> emails, string message);
    }
}
