﻿using DomainLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface IUsersService
    {
        /// <summary>
        /// Получает пользователей по списку идентификаторов
        /// </summary>
        /// <param name="usersIds"></param>
        /// <returns></returns>
        Task<List<User>> GetUsersByIdsAsync(List<int> usersIds);

        /// <summary>
        /// Получает список пользователей
        /// </summary>
        /// <param name="offset">Количество записей, которые нужно пропустить</param>
        /// <param name="numberOfItems">Число записей, которые надо выбрать</param>
        /// <returns></returns>
        Task<List<User>> GetUsersAsync(int offset, int numberOfItems);

        /// <summary>
        /// Получает количество пользователей в бд
        /// </summary>
        /// <returns></returns>
        Task<int> GetNumberOfUsersAsync();

        Task<User> GetUserByEmailAsync(string email);

        Task CreateUserAsync(User user);

        Task UpdateUserAsync(User user);

        Task UnsubscribeAsync(string email, string hash);

        Task UnsubscribeAsync(int userId);

        Task SubscribeAsync(int userId);

        List<string> GetAllRoles();

        Task<List<string>> GetRolesForUserAsync(string userName);

        Task AddRoleForUserAsync(string userName, string userRole);

        Task RemoveRoleFromUserAsync(string userName, string userRole);

        Task<bool> IsInRoleAsync(string userEmail, string role);

        Task CreateRoleAsync(string roleName);
    }
}
