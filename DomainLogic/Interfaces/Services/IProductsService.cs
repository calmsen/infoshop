﻿using DomainLogic.Infrastructure.Paging;
using DomainLogic.Models;
using DomainLogic.Models.Enumerations;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface IProductsService : IProductCounters
    {
        Task<byte[]> GenerateProductsPresentationAsync(long sectionId);

        Task<List<Product>> GetProductsAsync(Pagination pagination, long sectionId, List<FilterParam> fParams, List<Sort> sorts, int selectType, bool isArchive, double[] price);

        Task<List<Product>> GetProductsAsync(bool? isPublished = false, bool? hasPhoto = false, long sectionId = 0, bool? isActive = false, bool? isArchive = false);

        Task<List<Product>> GetProductsByIdsAsync(List<long> productIds);

        Task<List<Product>> SearchProductsAsync(string q, Pagination pagination);

        Task<List<Product>> SearchProductsAsync(string query);

        Task<Product> GetProductByIdAsync(long productId);

        Task<int> GetProductsAmountAsync(long sectionId);

        Task<int> GetPageForProductAsync(Product product);

        Task<List<Product>> GetProductsForBannerAsync();

        Task<List<Product>> GetHitProductsAsync();

        Task<List<Product>> GetNewProductsAsync();

        Task<List<Product>> GetProductsForAdminAsync(Pagination pagination, long pathOfSections, List<AllowableFilterEnum> filter = null, bool needShowUnactiveProducts = false, string query = "", string sort = null);

        Task<List<Product>> GetProductsByCodesAsync(List<long> codes);

        Task<List<ProductTitle>> GetProductTitlesAsync(long sectionId);

        Task<string> GetProductsAsXmlAsync(long sectionId);

        Task<byte[]> GetProductsInExcelAsync(long sectionId);

        Task<byte[]> GetProductImagesAsZipAsync(long sectionId);

        Task<List<Description>> GetProductDescriptionsAsync(List<long> descriptionIds);

        Task LoadProductAttributesAsync(List<Product> products, List<long> filterIds = null);
        
        Task DeleteImagesAsync(long productId);

        Task EditProductAsync(Product product, int userId = 0);

        Task CreateProductAsync(Product product, int userId = 0);

        Task DeleteProductAsync(long productId);

        Task UpdateProductAsync(Product product);

        Task<Product> CopyProductAsync(long productId, int userId);

        Task CopyAttributesAsync(long productId, long fromProductId);

        Task<Product> CopyImagesAsync(long productId, long fromProductId);
    }
}
