﻿using DomainLogic.Models.Settings;
using DomainLogic.Models;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface ISettingsService
    {
        Task CreateDataOnInitApp();

        Task UpdateDataOnCreateAdmin(User admin);

        void FillSettings(AppSettings settings);
    }
}
