﻿using DomainLogic.Infrastructure.Paging;
using DomainLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface INewsService
    {
        Task<List<Article>> GetNewsAsync(Pagination pagination, bool? active, Sort sort = null);

        Task<Article> GetArticleAsync(long articleId);

        Task EditArticleAsync(Article article);

        Task CreateArticleAsync(Article article);

        Task DeleteArticleAsync(Article article);
    }
}
