﻿using DomainLogic.Infrastructure.Paging;
using DomainLogic.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface IPostsService
    {
        Task<List<Post>> GetPostsAsync(Pagination pagination, bool? activate = null);

        Task<Post> GetPostByIdAsync(long postId);

        Task EditPostAsync(Post post);

        Task CreatePostAsync(Post post);

        Task DeletePostAsync(Post post);
    }
}
