﻿using DomainLogic.Models;
using DomainLogic.Models.Enumerations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.Services
{
    public interface ISrvsService
    {
        Task<List<Service>> GetServicesAsync();

        Task<List<Service>> GetServicesAsync(string city, ServiceTypeEnum type);

        Task<List<string>> GetCitiesForSelectListAsync(string country = null);

        Task<Service> GetServiceAsync(long serviceId);

        Task<string> GetSrvsListAsXmlAsync();

        Task ServiceFillTableAsync(ServiceTypeEnum type);

        Task EditServiceAsync(Service service);

        Task CreateServiceAsync(Service service);

        Task CreateServicesAsync(List<Service> services);

        Task CreateServicesFromFileAsync(string fileName, ServiceTypeEnum type);

        Task CreateOrUpdateServicesAsync(List<Service> services);

        Task DeleteServiceAsync(Service service);

        Task DeleteServicesAsync(ServiceTypeEnum type);        
    }
}
