﻿
namespace DomainLogic.Infrastructure.Utils
{
    public interface IRenderViewsUtils
    {
        /// <summary>
        /// Получает представление
        /// </summary>
        /// <param name="viewPath"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        string RenderPartialToString(string viewPath, object model);

        /// <summary>
        /// Получает представление
        /// </summary>
        /// <param name="controllerName"></param>
        /// <param name="partialViewName"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        string RenderPartialToString(string controllerName, string partialViewName, object model);
    }
}