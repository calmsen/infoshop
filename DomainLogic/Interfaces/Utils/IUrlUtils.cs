﻿namespace DomainLogic.Infrastructure.Utils
{
    /// <summary>
    /// Описывает методы для работы с путями и url
    /// </summary>
    public interface IUrlUtils
    {
        /// <summary>
        /// Получает базовый url
        /// </summary>
        /// <returns></returns>
        string GetBaseUrl();

        /// <summary>
        /// Получает полный путь к файлу
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        string MapPath(string filePath);

        /// <summary>
        /// Генерирует ссылку
        /// </summary>
        /// <param name="action"></param>
        /// <param name="controller"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        string UrlAction(string action, string controller, object parameters = null);
    }
}