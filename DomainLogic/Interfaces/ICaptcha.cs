﻿using System.IO;

namespace DomainLogic.Interfaces
{
    public interface ICaptcha
    {
        string Generate(Stream stream);
    }
}
