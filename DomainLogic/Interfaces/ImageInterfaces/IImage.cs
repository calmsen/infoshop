﻿
namespace DomainLogic.Interfaces.ImageInterfaces
{
    public interface IImage : IImageDimension
    {
        /// <summary>
        /// Идентификатор картинки
        /// </summary>
        long Id { get; set; }

        /// <summary>
        /// Название картинки
        /// </summary>
        string Title { get; set; }

        /// <summary>
        /// Внешняя ссылка на картинку
        /// </summary>
        string ExternalLink { get; set; }
    }
}
