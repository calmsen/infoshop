﻿
namespace DomainLogic.Interfaces.ImageInterfaces
{
    /// <summary>
    /// Размеры картинки
    /// </summary>
    public interface IImageDimension
    {
        /// <summary>
        /// Ширина картинки
        /// </summary>
        int Width { get; set; }

        /// <summary>
        /// Высота картинки
        /// </summary>
        int Height { get; set; }
    }
}
