﻿
namespace DomainLogic.Interfaces.Localization
{
    /// <summary>
    /// Описывает методы для локализации домена
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDomainLocalizer<T> : IDomainLocalizer
    {
        void Localize(T domain);
    }
}
