﻿
namespace DomainLogic.Interfaces.Localization
{
    /// <summary>
    /// Интерфейс необходим чтобы зарегистрировать локализаторы в провайдере локализаторов.
    /// </summary>
    public interface IDomainLocalizer
    {
    }    
}
