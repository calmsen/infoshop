﻿
using DomainLogic.Infrastructure.Localization;
using System;

namespace DomainLogic.Interfaces.Localization
{
    /// <summary>
    /// Содержит агрегатор всех локализаторов
    /// </summary>
    public interface IAggregateLocalizerHolder
    {
        /// <summary>
        /// Агрегатор всех локализаторов
        /// </summary>
        AggregateLocalizer AggregateLocalizer { set; }
    }
}
