﻿
namespace DomainLogic.Interfaces.Localization
{
    /// <summary>
    /// Локализует значение
    /// </summary>
    public interface IValueLocalizer
    {
        /// <summary>
        /// Локализует значение в соответсвии текущей локали или возвращает значение по умолчанию
        /// </summary>
        /// <param name="defaultCultureValue"></param>
        /// <param name="otherCultureValue"></param>
        /// <returns></returns>
        string LocalizedValue(string defaultCultureValue, string otherCultureValue);

        /// <summary>
        /// Локализует значение в соответсвии текущей локали или возвращает значение по умолчанию
        /// </summary>
        /// <param name="defaultCultureValue"></param>
        /// <param name="otherCultureValue"></param>
        /// <returns></returns>
        long? LocalizedValue(long? defaultCultureValue, long? otherCultureValue);
    }
}
