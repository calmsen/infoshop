﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.QueryMappers
{
    /// <summary>
    /// Интерфейс для работы с сущностью User. Методы содержат сырые запросы к БД
    /// </summary>
    public interface IUsersQueryMapper
    {
        /// <summary>
        /// Получает список имен пользователей, имеющие роли на сайте
        /// </summary>
        /// <returns></returns>
        Task<List<string>> GetUserNamesWithRolesAsync();

        /// <summary>
        /// даляет ненужные роли у пользователя
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        Task RemoveUnnecessaryUserRoleAsync(string userName);
    }
}