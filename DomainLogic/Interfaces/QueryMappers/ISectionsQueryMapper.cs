﻿using DomainLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.QueryMappers
{
    /// <summary>
    /// Интерфейс для работы с сущностью Section. Методы содержат сырые запросы к БД
    /// </summary>
    public interface ISectionsQueryMapper
    {
        /// <summary>
        /// Получает всех потомков и себя
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        Task<List<long>> GetDescendantOrSelfSectionsAsync(long sectionId);

        /// <summary>
        /// Получает полный путь из разделам относительно родителя
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        Task<long[]> GetPathOfSectionsByParentIdAsync(long parentId);

        /// <summary>
        /// Получает полный путь по разделам относительно своих соседей
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        Task<long[]> GetPathOfSectionsAsync(long sectionId);

        /// <summary>
        /// Получает заголовки разделов для указанных фильтров
        /// </summary>
        /// <param name="filterIds"></param>
        /// <returns></returns>
        Task<List<SectionTitle>> GetSectionTitlesAsync(List<long> filterIds);
    }
}