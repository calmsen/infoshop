﻿using System.Threading.Tasks;

namespace DomainLogic.Interfaces.QueryMappers
{
    /// <summary>
    /// Интерфейс для работы с сущностью ProductCounter. Методы содержат сырые запросы к БД
    /// </summary>
    public interface IProductCountersQueryMapper
    {
        /// <summary>
        /// Обновляет поле "Количество просмотров" у товаров
        /// </summary>
        Task UpdateProductCountersAsync();
    }
}