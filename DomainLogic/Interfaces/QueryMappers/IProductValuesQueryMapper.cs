﻿using System.Threading.Tasks;

namespace DomainLogic.Interfaces.QueryMappers
{
    /// <summary>
    /// Интерфейс для работы с сущностью ProductValue. Методы содержат сырые запросы к БД
    /// </summary>
    public interface IProductValuesQueryMapper
    {
        /// <summary>
        /// Получает количество значений для указанного фильтра
        /// </summary>
        /// <param name="filterId"></param>
        /// <returns></returns>
        Task<int> GetProductValuesCount(long filterId);

        /// <summary>
        /// Удаляет значения у товаров для указанного фильтра за исключением указаных значений
        /// </summary>
        /// <param name="filterId"></param>
        /// <param name="valuesIds"></param>
        /// <returns></returns>
        Task DeleteProductValues(long filterId, string valuesIds);
    }
}