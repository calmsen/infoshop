﻿using DomainLogic.Infrastructure.Paging;
using DomainLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.QueryMappers
{
    /// <summary>
    /// Интерфейс для работы с сущностью Product. Методы содержат сырые запросы к БД
    /// </summary>
    public interface IProductsQueryMapper
    {
        /// <summary>
        /// Получает список товаров по указанным фильтрам
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="sectionId"></param>
        /// <param name="fParams"></param>
        /// <param name="sorts"></param>
        /// <param name="isArchive"></param>
        /// <param name="price"></param>
        /// <returns></returns>
        Task<List<Product>> GetProductsAsync(Pagination pagination, long sectionId, List<FilterParam> fParams, List<Sort> sorts, int selectType, bool isArchive, double[] price, long filterIdByGroupType, long pathOfSections, List<FilterValue> values);

        /// <summary>
        /// Поиск товаров по строке запроса для определенных разделов
        /// </summary>
        /// <param name="q"></param>
        /// <param name="pagination"></param>
        /// <returns></returns>
        Task<List<Product>> SearchProductsAsync(string q, Pagination pagination);

        /// <summary>
        /// Получает номер страницы в которой находится данный товар
        /// </summary>
        /// <returns></returns>
        Task<int> GetPageForProductAsync(long productId, long sectionId);
    }
}