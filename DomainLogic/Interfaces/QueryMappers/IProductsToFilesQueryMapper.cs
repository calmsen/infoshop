﻿using System.Threading.Tasks;

namespace DomainLogic.Interfaces.QueryMappers
{
    /// <summary>
    /// Интерфейс для работы с сущностью ProductsToFile. Методы содержат сырые запросы к БД
    /// </summary>
    public interface IProductsToFilesQueryMapper
    {
        /// <summary>
        /// Добавляет файлы к товарам в указанном разделе
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="filterValueId"></param>
        /// <returns></returns>
        Task AddFilesWithSameFilterValue(long fileId, long filterValueId);

        /// <summary>
        /// Добавляет файлы к товарам c указанным значением фильтра
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        Task AddFilesWithSameSection(long fileId, long sectionId);
    }
}