﻿using DomainLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.QueryMappers
{
    /// <summary>
    /// Интерфейс для работы с сущностью ProductSnapshot. Методы содержат сырые запросы к БД
    /// </summary>
    public interface IProductSnapshotsQueryMapper
    {
        /// <summary>
        /// Получает последние снимки по списку идентификаторов товаров
        /// </summary>
        /// <param name="productIds"></param>
        /// <returns></returns>
        Task<List<ProductSnapshotInline>> GetLastProductSnapshotsAsync(List<long> productIds);
    }
}