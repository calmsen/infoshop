﻿using System.Threading.Tasks;

namespace DomainLogic.Interfaces.QueryMappers
{
    /// <summary>
    /// Интерфейс для работы с сущностью Attribute. Методы содержат сырые запросы к БД
    /// </summary>
    public interface IAttributesQueryMapper
    {
        /// <summary>
        /// Удаляет атрибуты по идентификатору фильтра
        /// </summary>
        /// <param name="filterId"></param>
        /// <returns></returns>
        Task DeleteAttributes(long filterId);
    }
}