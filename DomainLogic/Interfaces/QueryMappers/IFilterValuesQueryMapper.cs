﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces.QueryMappers
{
    /// <summary>
    /// Интерфейс для работы с сущностью FilterValue. Методы содержат сырые запросы к БД
    /// </summary>
    public interface IFilterValuesQueryMapper
    {
        /// <summary>
        /// Удаляет значения фильтра
        /// </summary>
        /// <param name="filterId"></param>
        /// <param name="valuesIds"></param>
        /// <returns></returns>
        Task DeleteFilterValues(long filterId, string valuesIds);

        /// <summary>
        /// Объединяет значения valueId c mainValueId в определенных разделах
        /// </summary>
        /// <param name="valueId"></param>
        /// <param name="mainValueId"></param>
        /// <param name="sectionIds"></param>
        Task MergeBindedValuesAsync(long valueId, long mainValueId, List<long> sectionIds);

        /// <summary>
        /// Удаляет значения valueId в определенных разделах
        /// </summary>
        /// <param name="valueId"></param>
        /// <param name="sectionIds"></param>
        Task DeleteBindedValuesAsync(long valueId, List<long> sectionIds);

        /// <summary>
        /// Копируем все привязанные значения valueId
        /// </summary>
        /// <param name="valueId"></param>
        /// <param name="sectionIds"></param>
        /// <param name="filterValueCloneId"></param>
        /// <returns></returns>
        Task CopyBindedValuesAsync(long valueId, List<long> sectionIds, long filterValueCloneId);
    }
}