﻿using System;

namespace DomainLogic.Interfaces.Models
{
    /// <summary>
    /// Описывает фильтр списка ответов на сообщения обратной связи
    /// </summary>
    public interface IFeedbackAnswerListFilter
    {
        /// <summary>
        /// Дата от (включительно)
        /// </summary>
        DateTime DateFrom { get; set; }

        /// <summary>
        /// Дата до (включительно)
        /// </summary>
        DateTime DateTo { get; set; }

        /// <summary>
        /// Строка по которой искать ответы на сообщения
        /// </summary>
        string Search { get; set; }
    }
}
