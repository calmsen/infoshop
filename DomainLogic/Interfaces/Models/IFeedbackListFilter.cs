﻿using DomainLogic.Models.Enumerations;
using System;

namespace DomainLogic.Interfaces.Models
{
    /// <summary>
    /// Описывает фильтр списка сообщений обратной связи
    /// </summary>
    public interface IFeedbackListFilter
    {
        /// <summary>
        /// Дата от (включительно)
        /// </summary>
        DateTime DateFrom { get; set; }

        /// <summary>
        /// Дата до (включительно)
        /// </summary>
        DateTime DateTo { get; set; }

        /// <summary>
        /// Тема сообщений
        /// </summary>
        int? Theme { get; set; }

        /// <summary>
        /// Идентификатор раздела, к которым привязаны сообщения
        /// </summary>
        long? SectionId { get; set; }

        /// <summary>
        /// Идентификатор товара, к которым привязаны сообщения
        /// </summary>
        long? ProductId { get; set; }

        /// <summary>
        /// Состояние сообщения
        /// </summary>
        StateEnum State { get; set; }

        /// <summary>
        /// Строка по которой искать сообщения
        /// </summary>
        string Search { get; set; }
    }
}
