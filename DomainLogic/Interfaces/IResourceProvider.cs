﻿using System.Collections.Generic;

namespace DomainLogic.Interfaces
{
    /// <summary>
    /// Предостовляет ресурсы локализации
    /// </summary>
    public interface IResourceProvider
    {
        /// <summary>
        /// Получает ресурс
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        string ReadResource(string key);

        /// <summary>
        /// Получает ресурсы
        /// </summary>
        /// <param name="classKey"></param>
        /// <returns></returns>
        Dictionary<string, string> ReadResources(string classKey);

        /// <summary>
        /// Получает ресурсы  для указанной локали
        /// </summary>
        /// <param name="classKey"></param>
        /// <returns></returns>
        string ReadResourcesAsJson(string classKey);
    }
}
