﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DomainLogic.Interfaces
{
    /// <summary>
    /// Описывает методы для работы с ролями
    /// </summary>
    public interface IRoleManagerWrap
    {
        /// <summary>
        /// Получает все роли
        /// </summary>
        /// <returns></returns>
        List<string> GetAllRoles();

        /// <summary>
        /// Получает роли для пользователя
        /// </summary>
        /// <param name="userEmail"></param>
        /// <returns></returns>
        Task<List<string>> GetRolesForUserAsync(string userEmail);

        /// <summary>
        /// Добавляет роль пользователю
        /// </summary>
        /// <param name="userEmail"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        Task AddRoleForUserAsync(string userEmail, string role);

        /// <summary>
        /// Удаляет роль у пользователя
        /// </summary>
        /// <param name="userEmail"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        Task RemoveRoleFromUserAsync(string userEmail, string role);

        /// <summary>
        /// Проверяет есть указанная роль у пользователя
        /// </summary>
        /// <param name="userEmail"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        Task<bool> IsInRoleAsync(string userEmail, string role);

        /// <summary>
        /// Создает новую роль
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        Task CreateRoleAsync(string roleName);
    }
}
