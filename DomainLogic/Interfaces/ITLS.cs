﻿using DomainLogic.Models.Enumerations;
using System;
using System.Collections.Generic;

namespace DomainLogic.Interfaces
{
    /// <summary>
    /// Интерфейс описывает работу с Thread Local Storage. Реализация вынесена в UI layer, для того чтобы избавится от зависимостей библиотеки System.Web
    /// </summary>
    public interface ITLS
    {
        /// <summary>
        /// Режим использования контекста 
        /// </summary>
        TlsMode Mode { get; set; }

        /// <summary>
        /// Словарь в котором хранятся все данные потока
        /// </summary>
        Dictionary<string, object> Data { get; }

        /// <summary>
        /// Устанавливает значение
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        void Set(string name, object value);

        /// <summary>
        /// Получает значение
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        object Get(string name);

        /// <summary>
        /// Получает значение и преобразует в нужный тип
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        T Get<T>(string name);

        /// <summary>
        /// Проверяет установлено ли значение
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        bool Contains(string name);

        /// <summary>
        /// Очищает весь словарь, при необходимости уничтожает вызывает метод Dispose
        /// </summary>
        void Clear();
    }

    
}
