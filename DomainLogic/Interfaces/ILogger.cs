﻿using System;

namespace DomainLogic.Interfaces
{
    /// <summary>
    /// Интерфейс описывает работу с логированием
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Запись сообщения в лог
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        void Error(string message, Exception innerException);

        /// <summary>
        /// Запись сообщения в лог
        /// </summary>
        /// <param name="message"></param>
        void Error(string message);

        /// <summary>
        /// Запись сообщения в лог
        /// </summary>
        /// <param name="exception"></param>
        void Error(Exception exception);

        /// <summary>
        /// Запись сообщения в лог
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        void Info(string message, params object[] args);
    }
}
